.. _contact:
.. index:: contact, bug report, feature request, information

Announcements - Getting Help - Contact
======================================

Announcement mailing list
-------------------------

This moderated **low traffic** mailing list is intended to announce general news on diviz (such as new releases) and inform you of upcoming events.

You can subscribe to the mailing list by pointing your web browser to the following address:

https://listes.imt-atlantique.fr/wws/subscribe/diviz-announcements

and by entering your email address. You will then receive an email with a link to confirm your subscription.


Bug reports, feature requests
-----------------------------

For bug reports, feature requests etc., there are two options:

- either send an email to support@diviz.org;

- or `create a new issue <https://gitlab.com/decision-deck/diviz/issues/new?issue>`_ on the project's page at GitLab —this requires a (free) account on `GitLab <https://www.gitlab.com>`_.


Contacting us
-------------

You can contact us at: info@diviz.org.

