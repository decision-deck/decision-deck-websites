.. index:: workflow, random

Generation of a random problem
==============================

.. toctree::
   :maxdepth: 2

Short description
-----------------

This workflow demonstrates how to build a random problem (performance table and criteria weights).

Detailed description
--------------------

To build a random problem, the following components are used:

- :ref:`randomPerformanceTable<randomPerformanceTable-PyXMCDA>`,
- :ref:`randomCriteriaWeights<randomCriteriaWeights-RXMCDA>`,
- :ref:`randomCriteria<randomCriteria-PyXMCDA>`,
- :ref:`randomAlternatives<randomAlternatives-PyXMCDA>`.

After the random problem is generated, the alternatives' evaluations are aggregated by a weighted sum.

Screenshot
----------

.. figure:: workflows/workflow-randomProblem-weightedSum.png
   :width: 100%

Download link
-------------

:download:`workflow-randomProblem-weightedSum.dvz</workflows/workflow-randomProblem-weightedSum.dvz>`

Click :ref:`here<tutorial_import>` for a detailed description on how to import the workflow into diviz.
