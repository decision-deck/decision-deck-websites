.. index:: workflows.x2

Example workflows
=================

On this page you can find some example workflows.
Each of them can be imported into diviz and executed as-is (the procedure to import a workflow into diviz is explained here: :ref:`tutorial_import`).

.. note:: For workflows adapted to diviz-x4, see :ref:`workflows.x4`.

.. _workflows.x2.methods:
MCDA Methods
------------

.. toctree::
   :maxdepth: 1

   workflow.method.ACUTAR
   workflow.method.ELECTRE-3
   workflow.method.ELECTRE-TRI
   workflow.method.ELECTRE
   workflow.method.PROMETHEE


.. _workflows.x2.published:
Published Workflows
-------------------

.. toctree::
   :maxdepth: 1

   workflow.DecisionDeckBook-MeyerBigaret
   workflow.KadzinskiLabijakNapieraj2016
   workflow.URPDM2010-BigaretMeyer
   workflow.URPDM2010-PirlotSchmitzMeyer
   workflow.URPDM2010-SpecialIssue


.. _workflows.x2.examples:
Example workflows
-----------------

.. toctree::
   :maxdepth: 1


   workflow.AHP-TOPSIS
   workflow.kappalab.linProgCapaIdent
   workflow.randomProblem.weightedSum
   workflow.weightedSum
