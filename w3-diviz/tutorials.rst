.. index:: tutorial

Tutorials
=========

On this page you can find tutorials for various topics, like:

.. toctree::
   :maxdepth: 1
   :glob:

   tutorial.*
   
You can also download a nice `user guide for diviz (PDF) <_static/ReportDecisionDeck-DEIM-URV.pdf>`_, which presents the implementation of well-known decision aid methods, such as the weighted sum or the Electre III method, allowing a novice user to easily familiarise himself with the software.
