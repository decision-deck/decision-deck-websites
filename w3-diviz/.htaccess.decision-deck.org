# redirect to www.diviz.org
RewriteEngine On
RewriteCond %{SERVER_PORT} ^80$
RewriteRule ^(.*)$ http://www.diviz.org/$1 [R=301,L]
RewriteCond %{SERVER_PORT} ^443$
RewriteRule ^(.*)$ https://www.diviz.org/$1 [R=301,L]
