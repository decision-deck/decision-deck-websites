.. index:: workflow, x4, diviz-x4
.. _workflows.x4:

Example workflows for diviz / XMCDAv4
=====================================

On this page you can find some example workflows that can be imported into :doc:`diviz x4 <diviz-XMCDAv4>`.

Each of them can be imported into diviz x4 and executed as-is (the procedure to import a workflow into diviz is explained here: :ref:`tutorial_import`, it is the same for diviz x2 and diviz x4).

.. _workflows.x4.methods:
MCDA Methods
------------

.. toctree::
   :maxdepth: 1

   workflow.x4.method.ACUTA
   workflow.x4.method.ELECTRE
   workflow.x4.method.ELECTRE-3
   workflow.x4.method.PROMETHEE
   workflow.x4.method.ELECTRE-TRI


.. _workflows.x4.published:
Published Workflows
-------------------

.. toctree::
   :maxdepth: 1

   workflow.x4.DecisionDeckBook-MeyerBigaret
   workflow.x4.URPDM2010-BigaretMeyer
   workflow.x4.URPDM2010-PirlotSchmitzMeyer
   workflow.x4.URPDM2010-SpecialIssue

..
   doc:`workflow.x4.KadzinskiLabijakNapieraj2016`

.. _workflows.x4.examples:
Example workflows
-----------------

.. toctree::
   :maxdepth: 1

   workflow.x4.AHP-TOPSIS
   workflow.x4.weightedSum

.. 
   doc:`workflow.x4.kappalab.linProgCapaIdent`
   doc:`workflow.x4.randomProblem.weightedSum`
