.. index:: changes_v1.12
.. _changes_v1.12:

Changes in diviz v1.12, v1.12.1, v1.12.2 (`Download it! <download.html>`_)
==========================================================================

.. note::

  - RXMCDA computeAlternativesQualification was removed in v1.12 but it should not have been removed: it is back at its place in v1.12.1

  - plotValueFunction was wrongly marked as not available in v1.12.1, due to an error in the build process: v1.12.2 fixes the problem.


Changes:

- The following ITTB programs are updated to version 1.1. It fixes bugs where
  the programs were not working properly when the amount of input data was too
  big (number of alternatives, number of criteria, etc.)

    - alternativesRankingViaQualificationDistillation
    - cutRelation
    - performanceTableFilter
    - plotAlternativesComparisons
    - plotAlternativesValuesPreorder
    - plotAlternativesValues
    - plotCriteriaValuesPreorder
    - plotCriteriaValues
    - plotNumericPerformanceTable

  plotValueFunctions has also been fixed: the service was not associating the
  right value functions to the right criteria when the files "criteria" and
  "criteriaValues" did not contain exactly the same set of criteria, or when
  the order of the criteria in the two XML files was not the same. 
  Additionally, the service failed to produce any graph when the functions
  associated to the criteria do not contain exactly the same number of points.

- New: diviz now remembers which workflows are opened, and reopens them when
  the application restarts.

- New: when selecting an program or a file, the related links are highlighted,
  while other links are more discretly displayed.  It makes it possible to
  immediately see to which elements a program is linked, simply by clicking on
  it.

  .. figure:: images/diviz_v1.12/highlight-before.png
     :width: 100%
     :alt: Image of a workflow before an element is selected
  
     With a lot of programs in a workflow, it is sometimes difficult to see the links between the element

  .. figure:: images/diviz_v1.12/highlight-after.png
     :width: 100%
     :alt: Image of a workflow after an element is selected

     Selecting an element highlights its connections!

- New: When searching a program, the programs' tree now displays only the
  matching programs, instead of simply showing the programs in bold style.
  This should allow a quicker choosing of the searched program.

  .. figure:: images/diviz_v1.12/searching.png
     :alt: The programs' tree when searching for "plot"

     Shown here, all plotting programs

- New: the first line of programs' annotations is now displayed under the
  program's name

  .. figure:: images/diviz_v1.12/programs_annotation.png
     :alt: A program with an annotation displayed under its name
  
     To open the annotation window, double-click on the tiny yellow paper.
     The first line in the annotation is displayed under the program's name.

- Fixed: on Linux, the external browser which can be opened to view results is
  now the same as the system's default web browser (the problem was observed
  when using GNOME).

- Fixed: when deleting a workflow having unsaved changes, the interface was
  asking whether the workflow should be saved after deleting it, and by doing
  so it was cancelling the deletion by saving it again!  Deleting a workflow
  does not trigger the should-we-save-the-workflow-before-closing dialog box
  anymore. Note: the dialog prompting for confirmation is still displayed.

- Fixed: "ghost" execution-in-progress items can now be deleted.  They were
  coming back each time the application was relaunched, no matter how many
  time you delete them (these items are processes in the RUNNING state whose
  processes is not executed anymore but which remain in the server database
  after a crash e.g.).

- Numerous bugs fixed which were caused by the renaming of a worklow: a
  renamed workflow was not correctly identified by the interface anymore until
  the application is restarted, it was not possible to select it in the list
  of opened workflows, deleting its results (resp. closing it) was in fact
  deleting the results in the workflow coming after it in the list
  (resp. closing the workflow coming next).

- Fixed: it was not possible to reset a file's displayed name after it was
  customized.  When reset to the empty string, the displayed name is the
  underlying file name.

- Fixed: ACUTA v1.1 was not marked as inactive: workflows containing it were
  not adapted to use v2.0, but they were not executable anymore since no
  web-service is available anymore for v1.1.  This has been fixed and such
  workflows are now adapted, as expected.

- Fixed: since v1.7, the workflows created by v1.1 or earlier were not
  correctly loaded: some programs, if not all, vanished off the workflow. This
  is now fixed.

