.. index:: workflow, AHP, TOPSIS

AHP-TOPSIS
==========

AHP-criteria with TOPSIS, by Witold Kupś.


Screenshot
----------

.. figure:: workflows/AHP-TOPSIS.png
   :width: 100%

Download link
-------------

:download:`AHP-TOPSIS.dvz</workflows/AHP-TOPSIS.dvz>`

Click :ref:`here<tutorial_import>` for a detailed description on how to import the workflow into diviz.
