.. index:: MCDA, Decision Deck
.. _index:

.. raw:: html

    <div id="title" class="text-center">
      <img class="img-responsive" src="_static/divizlogo2.png" style="max-height:70px;display:block;margin: 20px auto;">
      <h2>diviz</h2>
      <p>diviz is a workbench to design, execute and share complex MCDA algorithms and experiments</p>
    </div>


diviz relies on the `XMCDA web services <//www.decision-deck.org/ws>`_ and the `XMCDA <//www.decision-deck.org/xmcda>`_ standard.

.. raw:: html

  <div class="video">
    <video  width="100%" controls>
      <source src="/_static/diviz_demo.mp4" type="video/mp4">
      <track label="English" kind="subtitles" srclang="en" src="/_static/diviz_demo.vtt" default>
      Your browser does not support the video tag.
    </video>
  </div>

.. raw:: html

    <hr>
    <div style="font-size: x-large; text-align: center;">
      <b style="color: firebrick;">New!</b> Test now the <a href="/diviz-XMCDAv4.html">next version of diviz</a>!
    </div>
    <hr>

Using diviz
-----------

.. toctree::
   :maxdepth: 1

   download
   tutorials
   programs_xmcda-v2/wsindex-1
   programs_xmcda-v4/wsindex-1
   workflows

General information
--------------------------------

.. toctree::
   :maxdepth: 1

   features
   screenshots
   dissemination
   contact

Developer's corner
------------------
.. toctree::
   :maxdepth: 1

   howtos
