.. index:: tutorial, workflow import
.. _tutorial_import:

Import of a workflow in diviz
=============================

.. toctree::
   :maxdepth: 2

Short description of the tutorial
---------------------------------

This tutorial presents you how to import existing workflows into diviz.

Detailed description of the tutorial
------------------------------------

This tutorial focusses on the following aspects:

  - Importing of an existing workflow;
  - Execution of the workflow;
  - Visualisation of the results.

Tutorial
--------

First start by downloading the following workflow file:

  - :download:`bigMix.dvz</workflows/bigMix.dvz>`

Then run diviz. 

After the splash screen you can see an empty workspace (as in Figure :ref:`fig_tuto_diviz1`).
The left panel is used to display the open workflows and their past executions. 
The right pane contains the list of available elementary components. 
The center pane is used to construct and display the workflows. 

.. _fig_tuto_diviz1:

.. figure:: images/tuto_diviz1.png

   *Empy diviz screen*

To load the workflow that you just downloaded, open the workflow menu and click on "Import as new" and select the file that you just downloaded.

Then choose a directory where the data files contained in the workflow should be saved. 

The workflow stored in the file `bigMix.dvz` is then displayed and you can run it or modify it (see Figure :ref:`fig_tuto_diviz2`).

.. _fig_tuto_diviz2:

.. figure:: images/tuto_workflow1.png

   *Loaded workflow*

You can execute it via the **Execution** menu via the **Run** entry.

After the workflow has been successfully executed, you can click on the entry below the name of your workflow in the left panel to view the results in the panel which has appeared under your workflow.


