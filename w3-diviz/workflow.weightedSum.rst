.. index:: workflow, weighted sum

Weighted sum
============

.. toctree::
   :maxdepth: 2

Short description
-----------------

This workflow applies a weighted sum on a performance table.

Detailed description
--------------------

The :ref:`weightedSum<weightedSum-PyXMCDA>` component calculates the weighted sum of the alternatives.  This overall value is then represented graphically via a barchart.

Screenshot
----------

.. figure:: workflows/workflow-weightedSum.png
   :width: 100%

Download link
-------------

:download:`workflow-weightedSum.dvz</workflows/workflow-weightedSum.dvz>`

Click :ref:`here<tutorial_import>` for a detailed description on how to import the workflow into diviz.
