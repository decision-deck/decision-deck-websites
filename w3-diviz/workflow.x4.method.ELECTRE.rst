.. index:: workflow, x4, diviz-x4, ELECTRE

ELECTRE
=======

.. toctree::
   :maxdepth: 2

Short description
-----------------

This workflow demonstrates the construction of an outranking relation according to the ELECTRE [Roy1996]_ methods approaches.

Detailed description
--------------------

This workflow uses, among other components, the :ref:`ELECTRE Concordance<ElectreConcordance-J-MCDA>`, :ref:`ELECTRE Discordances<ElectreDiscordances-J-MCDA>` and :ref:`ELECTRE Outranking<ElectreOutranking-J-MCDA>` components to calculate the outranking relation according to the ELECTRE methods approaches.

The output is then used to plot this relation as a directed graph.

Screenshot
----------

.. figure:: workflows/x4/method.ELECTRE.png
   :width: 100%

Download link
-------------

:download:`method.ELECTRE.dvz</workflows/x4/method.ELECTRE.dvz>`

Click :ref:`here<tutorial_import>` for a detailed description on how to import the workflow into diviz.

Bibliography
------------

.. [Roy1996] Roy B., Multicriteria Methodology for Decision Analysis, Kluwer Academic Publishers, 1996
