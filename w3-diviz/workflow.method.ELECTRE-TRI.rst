.. index:: workflow, ELECTRE, ELECTRE TRI

ELECTRE TRI
===========

.. toctree::
   :maxdepth: 2

Short description
-----------------

This workflow demonstrates the construction of the ELECTRE TRI [Roy1996]_ method.

Detailed description
--------------------

This workflow uses, among other components, the :ref:`ELECTRE TRI Exploitation<ElectreTriExploitation>` component to determine the category to which each alternative belongs according to either a pessimistic, an optimistic or a disjunctive rule. The parameters of the sorting procedure can be tuned by double-clicking on the ElectreTriExploitation component to obtain the parameters window.

Screenshot
----------

.. figure:: workflows/method.ELECTRE-TRI.png
   :width: 100%

Download link
-------------

:download:`method.ELECTRE-TRI.dvz</workflows/method.ELECTRE-TRI.dvz>`

Click :ref:`here<tutorial_import>` for a detailed description on how to import the workflow into diviz.

Bibliography
------------

.. [Roy1996] Roy B., Multicriteria Methodology for Decision Analysis, Kluwer Academic Publishers, 1996
