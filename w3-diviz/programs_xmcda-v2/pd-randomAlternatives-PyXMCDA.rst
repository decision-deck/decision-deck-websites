:orphan:



.. _randomAlternatives-PyXMCDA:

randomAlternatives
==================

:Provider: PyXMCDA
:Version: 1.0

Description
-----------

This web service allows to create a simple list of alternative by simply providing the desired number of alternatives, or a list of alternatives names.

- **Contact:** Thomas Veneziano (thomas.veneziano@uni.lu)



Inputs
------
(For outputs, see :ref:`below <randomAlternatives-PyXMCDA_outputs>`)


- :ref:`nbAlternatives <randomAlternatives-PyXMCDA-nbAlternatives>`
- :ref:`alternativesPrefix <randomAlternatives-PyXMCDA-alternativesPrefix>` *(optional)*
- :ref:`alternativesNames <randomAlternatives-PyXMCDA-alternativesNames>` *(optional)*

.. _randomAlternatives-PyXMCDA-nbAlternatives:

nbAlternatives
~~~~~~~~~~~~~~


Description:
............

Indicates the desired number of alternatives. It must be a strict positive integer.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** nbAlternatives

  *Indicates the desired number of alternatives.*

  - **Constraint description:** The value should be a positive integer.

  - **Type:** integer
  - **Default value:** 2

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                            
                    <methodParameters>
                        <parameter
                             name="nbAlternatives"> <!-- REQUIRED  -->
                            <value>
                                <integer>%1</integer>
                            </value>
                        </parameter>
                    </methodParameters>
                    
                     

----------------------------


.. _randomAlternatives-PyXMCDA-alternativesPrefix:

alternativesPrefix
~~~~~~~~~~~~~~~~~~


Description:
............

Indicates the desired prefix for the name of the alternatives. If not provided, alternatives will be called a1, a2, ... If provided, alternatives will be called prefix1, prefix2, ... Note that it will only be used if you provide a number of alternatives.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** alternativesPrefix

  *Indicates the desired prefix for the alternatives.*

  - **Type:** string
  - **Default value:** "a"

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                            
                    <methodParameters>
                        <parameter
                             name="alternativesPrefix"> <!-- REQUIRED  -->
                            <value>
                                <label>%1</label>
                            </value>
                        </parameter>
                    </methodParameters>
                    
                     

----------------------------


.. _randomAlternatives-PyXMCDA-alternativesNames:

alternativesNames
~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Indicates a list of alternatives names. When supplied, the service ignores parameters nbAlternatives and alternativesPrefix



XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                            
                    <methodParameters>
                        <parameters
                             name="alternativesNames"> <!-- REQUIRED  -->
                            <parameter><value>
                                <label>[...]</label>
                            </value></parameter>
                            [..]
                        </parameter>
                    </methodParameters>
                    
                     

----------------------------



.. _randomAlternatives-PyXMCDA_outputs:

Outputs
-------


- :ref:`alternatives <randomAlternatives-PyXMCDA-alternatives>`
- :ref:`messages <randomAlternatives-PyXMCDA-messages>`

.. _randomAlternatives-PyXMCDA-alternatives:

alternatives
~~~~~~~~~~~~


Description:
............

A list of alternatives with active tags.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _randomAlternatives-PyXMCDA-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
