:orphan:



.. _DEASMAAvalueADDEfficiencies-PUT:

DEASMAAvalueADDEfficiencies
===========================

:Provider: PUT
:Version: 1.0

Description
-----------

Computes efficiency scores for the given DMUs (alternatives) using SMAA-D method and additive Data Envelopment Analysis Model. For given number of buckets and samples, returns a matrix with alternatives in each row and buckets representing efficiency intervals in each column. Single cell indicates how many samples gave efficiency scores of respective alternative in respective bucket.

- **Contact:** Malgorzata Napieraj (napieraj.malgorzata@gmail.com)

- **Reference:** Gouveia M. C., Dias L. C., Antunes C. H., Additive DEA based on MCDA with imprecise information (2008).
			Lahdelma R., Salminen P., Stochastic multicriteria acceptability analysis using the data envelopment model (2004).



Inputs
------
(For outputs, see :ref:`below <DEASMAAvalueADDEfficiencies-PUT_outputs>`)


- :ref:`inputsOutputs <DEASMAAvalueADDEfficiencies-PUT-inputsOutputs>`
- :ref:`units <DEASMAAvalueADDEfficiencies-PUT-units>`
- :ref:`performanceTable <DEASMAAvalueADDEfficiencies-PUT-performanceTable>`
- :ref:`weightsLinearConstraints <DEASMAAvalueADDEfficiencies-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <DEASMAAvalueADDEfficiencies-PUT-methodParameters>`

.. _DEASMAAvalueADDEfficiencies-PUT-inputsOutputs:

inputsOutputs
~~~~~~~~~~~~~


Description:
............

A list of criteria with specified preference direction. List has to contains at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output).



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    <criteria>
                        <criterion>
							<scale>
								<quantitative>
									<preferenceDirection>
										[...]
									</preferenceDirection>
								</quantitative>
							</scale>
                            [...]
                        </criterion>
                        [...]
                    </criteria>

----------------------------


.. _DEASMAAvalueADDEfficiencies-PUT-units:

units
~~~~~


Description:
............

A list of alternatives (DMUs).



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>

----------------------------


.. _DEASMAAvalueADDEfficiencies-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~


Description:
............

A list of alternatives (DMUs) performances.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    <performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>

----------------------------


.. _DEASMAAvalueADDEfficiencies-PUT-weightsLinearConstraints:

weightsLinearConstraints
~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

A list of criteria linear constraints.



XMCDA related:
..............

- **Tag:** criteriaLinearConstraints

- **Code:**

  ::

    <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID>[...]</criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>

----------------------------


.. _DEASMAAvalueADDEfficiencies-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~


Description:
............

"boundariesProvided" indicates if criteria values boundaries are given in criteria input or they have to be computed based on alternatives performances. "samplesNo" represents the number of samples to generate. "intervalsNo", represents the number of buckets which efficiency scores will be assigned to. "transformToUtilites" indicates if given alternatives values are utilities or if they have to be firstly transformed to utilities.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** boundariesProvided

  

  - **Default value:** false
- **Name:** samplesNo

  

  - **Constraint description:** The value should be a positive integer.

  - **Type:** integer
  - **Default value:** 100
- **Name:** intervalsNo

  

  - **Constraint description:** The value should be a positive integer.

  - **Type:** integer
  - **Default value:** 10
- **Name:** transformToUtilities

  

  - **Default value:** true

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    <methodParameters>
							<parameter name="boundariesProvided">
								<value><boolean>%1</boolean></value>
							</parameter>
							<parameter name="samplesNo">
								<value><integer>%2</integer></value>
							</parameter>
							<parameter name="intervalsNo">
								<value><integer>%3</integer></value>
							</parameter>
							<parameter name="transformToUtilities">
								<value><boolean>%4</boolean></value>
							</parameter>
					</methodParameters>

----------------------------



.. _DEASMAAvalueADDEfficiencies-PUT_outputs:

Outputs
-------


- :ref:`efficiencyDistribution <DEASMAAvalueADDEfficiencies-PUT-efficiencyDistribution>`
- :ref:`maxEfficiency <DEASMAAvalueADDEfficiencies-PUT-maxEfficiency>`
- :ref:`minEfficiency <DEASMAAvalueADDEfficiencies-PUT-minEfficiency>`
- :ref:`avgEfficiency <DEASMAAvalueADDEfficiencies-PUT-avgEfficiency>`
- :ref:`messages <DEASMAAvalueADDEfficiencies-PUT-messages>`

.. _DEASMAAvalueADDEfficiencies-PUT-efficiencyDistribution:

efficiencyDistribution
~~~~~~~~~~~~~~~~~~~~~~


Description:
............

A performance table for given alternatives. Single performance consists of attribute criterionID representing certain bucket, and a value representing ratio of efficiency scores in this bucket.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    <performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>Bucket [...]</criterionID>
									<value>
									[...]
									</value>
							</performance>
							[...]
						</alternativePerformances>
					</performanceTable>

----------------------------


.. _DEASMAAvalueADDEfficiencies-PUT-maxEfficiency:

maxEfficiency
~~~~~~~~~~~~~


Description:
............

A list of alternatives with maximum efficiency scores (obtained for given sample).



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    <alternativesValues mcdaConcept="efficiency">
						<alternativeValue>
						  <alternativeID>[...]</alternativeID>
						  <value>
							[...]
						  </value>
						</alternativeValue>
						[...]
					</alternativesValues>

----------------------------


.. _DEASMAAvalueADDEfficiencies-PUT-minEfficiency:

minEfficiency
~~~~~~~~~~~~~


Description:
............

A list of alternatives with computed minimum efficiency scores (obtained for given sample).



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    <alternativesValues mcdaConcept="efficiency">
						<alternativeValue>
						  <alternativeID>[...]</alternativeID>
						  <value>
							[...]
						  </value>
						</alternativeValue>
						[...]
					</alternativesValues>

----------------------------


.. _DEASMAAvalueADDEfficiencies-PUT-avgEfficiency:

avgEfficiency
~~~~~~~~~~~~~


Description:
............

A list of alternatives with average efficiency scores (obtained for given sample).



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    <alternativesValues mcdaConcept="efficiency">
						<alternativeValue>
						  <alternativeID>[...]</alternativeID>
						  <value>
							[...]
						  </value>
						</alternativeValue>
						[...]
					</alternativesValues>

----------------------------


.. _DEASMAAvalueADDEfficiencies-PUT-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
