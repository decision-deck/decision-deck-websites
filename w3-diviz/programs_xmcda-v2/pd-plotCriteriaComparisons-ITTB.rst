:orphan:



.. _plotCriteriaComparisons-ITTB:

plotCriteriaComparisons
=======================

:Provider: ITTB
:Version: 1.0

Description
-----------

This web service generates a graph representing a partial preorder on the criteria. Compared to the web service plotCriteriaComparisons, some parameters are added. The generated graph can be valued. It can also be transitive. Several shapes for the nodes are proposed and colors can be used.

- **Contact:** Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)



Inputs
------
(For outputs, see :ref:`below <plotCriteriaComparisons-ITTB_outputs>`)


- :ref:`criteria <plotCriteriaComparisons-ITTB-criteria>`
- :ref:`criteriaComparisons <plotCriteriaComparisons-ITTB-criteriaComparisons>`
- :ref:`methodGraphOptions <plotCriteriaComparisons-ITTB-methodGraphOptions>`

.. _plotCriteriaComparisons-ITTB-criteria:

criteria
~~~~~~~~


Description:
............

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
                   
                    <criteria>
                        <criterion>
                            <active>[...]</active>
                            [...]
                        </criterion>
                        [...]
                    </criteria>
                    
               

----------------------------


.. _plotCriteriaComparisons-ITTB-criteriaComparisons:

criteriaComparisons
~~~~~~~~~~~~~~~~~~~


Description:
............

A valued relation relative to comparisons of the criteria. A numeric <value> indicates a the valuation for each <pair> of the relation.



XMCDA related:
..............

- **Tag:** criteriaComparisons

- **Code:**

  ::

    
               
                    <criteriaComparisons>
                        <pairs>
                            <pair>
                                <initial>
                                    <criterionID>[...]</criterionID>
                                </initial>
                                <terminal>
                                    <criterionID>[...]</criterionID>
                                </terminal>
                                <value>
                                    <real>[...]</real>
                                </value>
                            </pair>

                            [...]
                        </pairs>
                    </criteriaComparisons>
                    
                    

----------------------------


.. _plotCriteriaComparisons-ITTB-methodGraphOptions:

methodGraphOptions
~~~~~~~~~~~~~~~~~~


Description:
............

Generates a graph taking into account the proposed options.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** Graph type:

  *Choose between true (values appear with their transitions) or false.*

  - **Type:** drop-down list
  - **Possible values:**
      - Arcs and values (XMCDA label : true) 

      - Arcs only (XMCDA label : false) (default)

- **Name:** With transitive reduction?

  

  - **Type:** drop-down list
  - **Possible values:**
      - Yes (XMCDA label : true) 

      - No (XMCDA label : false) (default)

- **Name:** Shape of the nodes?

  *Choose between rectangle, square, ellipse, circle or diamond.*

  - **Type:** drop-down list
  - **Possible values:**
      - Rectangle (XMCDA label : Rectangle) (default)

      - Square (XMCDA label : Square) 

      - Ellipse (XMCDA label : Ellipse) 

      - Circle (XMCDA label : Circle) 

      - Diamond (XMCDA label : Diamond) 

- **Name:** Use colors?

  *The use of colors: true for a colored graph.*

  - **Type:** drop-down list
  - **Possible values:**
      - Yes (XMCDA label : true) 

      - No (XMCDA label : false) (default)

- **Name:** Choose color:

  *String that indicates the color in the generated graph.Choose between "Black", "Red", "Blue", "Green", "Yellow", "Magenta" and "Cyan".*

  - **Type:** drop-down list
  - **Possible values:**
      - Black (XMCDA label : Black) (default)

      - Red (XMCDA label : Red) 

      - Blue (XMCDA label : Blue) 

      - Green (XMCDA label : Green) 

      - Yellow (XMCDA label : Yellow) 

      - Magenta (XMCDA label : Magenta) 

      - Cyan (XMCDA label : Cyan) 


XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                   
					<methodParameters>
						<parameter id="valued_graph" name="Valued graph">
							<value>
            					<label>%1</label>
       						 </value>
						</parameter>
						<parameter id="transitive_reduction" name="Transitive reduction">
							<value>
            					<label>%2</label>
       						 </value>
						</parameter>
						<parameter id="node_shape" name="Node shape">
							<value>
            					<label>%3</label>
       						 </value>
						</parameter>

						<parameter id="use_color" name="Colors in the graph">
							<value>
            					<label>%4</label>
       						 </value>
						</parameter>
						
						<parameter id="selected_color" name="Selected color">
							<value>
            					<label>%5</label>
       						 </value>
						</parameter>
						
					</methodParameters>
				
               

----------------------------



.. _plotCriteriaComparisons-ITTB_outputs:

Outputs
-------


- :ref:`criteriaComparisonsPlot <plotCriteriaComparisons-ITTB-criteriaComparisonsPlot>`
- :ref:`messages <plotCriteriaComparisons-ITTB-messages>`

.. _plotCriteriaComparisons-ITTB-criteriaComparisonsPlot:

criteriaComparisonsPlot
~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

A string containing the base64 representation of the png image of the generated graph.



XMCDA related:
..............

- **Tag:** criterionValue

----------------------------


.. _plotCriteriaComparisons-ITTB-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
