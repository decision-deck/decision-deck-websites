:orphan:



.. _additiveValueFunctionsIdentification-RXMCDA:

additiveValueFunctionsIdentification
====================================

:Provider: RXMCDA
:Version: 1.1

Description
-----------

Identifies an set of piecewise linear additive value functions according to a ranking of the alternatives. If the number of segments is not given, a general additive value function is looked for.

- **Contact:** Helene Schmitz and Patrick Meyer (patrick.meyer@telecom-bretagne.eu)

- **Web page:** None



Inputs
------
(For outputs, see :ref:`below <additiveValueFunctionsIdentification-RXMCDA_outputs>`)


- :ref:`criteria <additiveValueFunctionsIdentification-RXMCDA-criteria>`
- :ref:`alternatives <additiveValueFunctionsIdentification-RXMCDA-alternatives>`
- :ref:`performanceTable <additiveValueFunctionsIdentification-RXMCDA-performanceTable>`
- :ref:`alternativesRanks <additiveValueFunctionsIdentification-RXMCDA-alternativesRanks>`
- :ref:`separationThreshold <additiveValueFunctionsIdentification-RXMCDA-separationThreshold>`
- :ref:`segments <additiveValueFunctionsIdentification-RXMCDA-segments>` *(optional)*

.. _additiveValueFunctionsIdentification-RXMCDA-criteria:

criteria
~~~~~~~~


Description:
............

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
                
                    <criteria>
                        <criterion>
                            <active>[...]</active>
                            [...]
                        </criterion>
                        [...]
                    </criteria>
                    
            

----------------------------


.. _additiveValueFunctionsIdentification-RXMCDA-alternatives:

alternatives
~~~~~~~~~~~~


Description:
............

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
                
                    <alternatives>
                        <alternative>
                            <active>[...]</active>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
                    
            

----------------------------


.. _additiveValueFunctionsIdentification-RXMCDA-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~


Description:
............

A performance table. The evaluations should be numeric values, i.e. <real>, <integer> or <rational>.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _additiveValueFunctionsIdentification-RXMCDA-alternativesRanks:

alternativesRanks
~~~~~~~~~~~~~~~~~


Description:
............

The ranking of the alternatives, the best alternative having the lowest rank.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _additiveValueFunctionsIdentification-RXMCDA-separationThreshold:

separationThreshold
~~~~~~~~~~~~~~~~~~~


Description:
............

Threshold value indicating the minimal difference in terms of the overall value between two neighbor alternatives in the given ranking.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** separationThreshold

  *Threshold value indicating the minimal difference in terms of the overall value between two neighbor alternatives in the given ranking.*

  - **Constraint description:** The value should be a strictly positive float, less than the highest possible overall value.

  - **Type:** float

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                
                    <methodParameters>
                        <parameter
                             name="separationThreshold"> <!-- REQUIRED  -->
                            <value>
                                <real>%1</real>
                            </value>
                        </parameter>
                    </methodParameters>
                    
            

----------------------------


.. _additiveValueFunctionsIdentification-RXMCDA-segments:

segments
~~~~~~~~


Description:
............

The number of segments for the additive value functions. If it is not given, then a general additive value function is searched for.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** numberOfSegments

  *The number of segments for the additive value functions.*

  - **Constraint description:** The value should be a strictly positive integer.

  - **Type:** integer
  - **Default value:** 1

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                
                    <methodParameters>
                        <parameter
                             name="criteriaSegments"> <!-- REQUIRED  -->
                            <value>
                                <integer>%1</integer>
                            </value>
                        </parameter>
                    </methodParameters>
                    
            

----------------------------



.. _additiveValueFunctionsIdentification-RXMCDA_outputs:

Outputs
-------


- :ref:`valueFunctions <additiveValueFunctionsIdentification-RXMCDA-valueFunctions>`
- :ref:`messages <additiveValueFunctionsIdentification-RXMCDA-messages>`

.. _additiveValueFunctionsIdentification-RXMCDA-valueFunctions:

valueFunctions
~~~~~~~~~~~~~~


Description:
............

The value functions of the selected criteria.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
                
					<criteria mcdaConcept="valueFunctions">
						<criterion>
							<criterionID>[...]</criterionID>
							<criterionFunction>
								<points>
									<point>
										<abscissa><real>[...]</real></abscissa>
										<ordinate><real>[...]</real></ordinate>
									</point>
								</points>
							</criterionFunction>
						</criterion>
					</criteria>
				
            

----------------------------


.. _additiveValueFunctionsIdentification-RXMCDA-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
