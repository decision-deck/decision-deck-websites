:orphan:



.. _csvToXMCDA-criteriaValues-PyXMCDA:

csvToXMCDA-criteriaValues
=========================

:Provider: PyXMCDA
:Version: 1.2

Description
-----------

Transforms a file containing criteria values from a comma-separated values (CSV) file to two XMCDA compliant files, containing the corresponding criteria ids and their criteriaValues.

- **Contact:** Sébastien Bigaret (sebastien.bigaret@telecom-bretagne.eu)

- **Web page:** http://github.com/sbigaret/ws-PyXMCDA



Inputs
------
(For outputs, see :ref:`below <csvToXMCDA-criteriaValues-PyXMCDA_outputs>`)


- :ref:`criteriaValues.csv <csvToXMCDA-criteriaValues-PyXMCDA-csv>`

.. _csvToXMCDA-criteriaValues-PyXMCDA-csv:

criteriaValues (csv)
~~~~~~~~~~~~~~~~~~~~


Description:
............

The criteria and their criteria values as a CSV file.

Example:

  ,cost,risk,employment,connection
  weights,1,2,3,4



XMCDA related:
..............

- **Tag:** other

----------------------------



.. _csvToXMCDA-criteriaValues-PyXMCDA_outputs:

Outputs
-------


- :ref:`criteria <csvToXMCDA-criteriaValues-PyXMCDA-criteria>`
- :ref:`criteriaValues <csvToXMCDA-criteriaValues-PyXMCDA-criteriaValues>`
- :ref:`messages <csvToXMCDA-criteriaValues-PyXMCDA-messages>`

.. _csvToXMCDA-criteriaValues-PyXMCDA-criteria:

criteria
~~~~~~~~


Description:
............

The equivalent criteria ids.



XMCDA related:
..............

- **Tag:** criteria

----------------------------


.. _csvToXMCDA-criteriaValues-PyXMCDA-criteriaValues:

criteriaValues
~~~~~~~~~~~~~~


Description:
............

The equivalent criteria values.



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _csvToXMCDA-criteriaValues-PyXMCDA-messages:

messages
~~~~~~~~


Description:
............

Status messages.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
