:orphan:



.. _ElectreConcordanceWithInteractions-PUT:

ElectreConcordanceWithInteractions
==================================

:Provider: PUT
:Version: 0.2.0

Description
-----------

Computes concordance matrix taking into account interactions between criteria. Possible interactions are: 'strengthening', 'weakening' and 'antagonistic'.

This module allows to compute concordance matrix for both for alternatives vs alternatives and alternatives vs profiles comparison - where profiles may be boundary or central (characteristic).

- **Web page:** http://github.com/xor-xor/electre_diviz



Inputs
------
(For outputs, see :ref:`below <ElectreConcordanceWithInteractions-PUT_outputs>`)


- :ref:`criteria <ElectreConcordanceWithInteractions-PUT-input3>`
- :ref:`alternatives <ElectreConcordanceWithInteractions-PUT-input1>`
- :ref:`performance_table <ElectreConcordanceWithInteractions-PUT-input5>`
- :ref:`profiles_performance_table <ElectreConcordanceWithInteractions-PUT-input6>` *(optional)*
- :ref:`interactions <ElectreConcordanceWithInteractions-PUT-input4>`
- :ref:`weights <ElectreConcordanceWithInteractions-PUT-input7>`
- :ref:`classes_profiles <ElectreConcordanceWithInteractions-PUT-input2>` *(optional)*
- :ref:`method_parameters <ElectreConcordanceWithInteractions-PUT-input8>`

.. _ElectreConcordanceWithInteractions-PUT-input3:

criteria
~~~~~~~~


Description:
............

Criteria to consider, possibly with preference and indifference thresholds. Each criterion must have a preference direction specified (min or max). It is worth mentioning that this module allows to define thresholds as constants as well as linear functions.



XMCDA related:
..............

- **Tag:** criteria

----------------------------


.. _ElectreConcordanceWithInteractions-PUT-input1:

alternatives
~~~~~~~~~~~~


Description:
............

Alternatives to consider.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _ElectreConcordanceWithInteractions-PUT-input5:

performance_table
~~~~~~~~~~~~~~~~~


Description:
............

The performance of alternatives.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _ElectreConcordanceWithInteractions-PUT-input6:

profiles_performance_table
~~~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The performance of profiles (boundary or central).



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _ElectreConcordanceWithInteractions-PUT-input4:

interactions
~~~~~~~~~~~~


Description:
............

Declarations of criteria interactions which should be taken into account. Possible interactions are: "strengthening", "weakening" and "antagonistic".

Every such declaration contains a pair of criteria and a numerical value for the interaction coefficient. This value should be negative for "weakening" and positive for "strenghtening" and "antagonistic". This coefficient should be interpreted as follows: the larger its absolute value, the more important the interaction effect.  All interaction coefficients should fulfill positive net balance condition.

The order in which each criterion appears in its interaction definition is significant, i.e. it designates the direction in which certain criterion affects the other, which is especially true when it comes to "antagonistic" interaction (i.e. the second criterion is the affected one).

There are no limits on the number of interactions that can be declared, but keep in mind that it should be rather small to make sense.

It is possible to declare more than one interactions of the same type, although please note that considering the same pair of criteria involved, the "strengthening" and "weakening" interactions are mutually exclusive.



XMCDA related:
..............

- **Tag:** criteriaValues

- **Code:**

  ::

    
        
          <criteriaValues mcdaConcept="criteriaInteractions">
            <criterionValue mcdaConcept="strengthening">
              <criteriaSet>
                <element>
                  <criterionID>[...]</criterionID>
                </element>
                <element>
                  <criterionID>[...]</criterionID>
                </element>
              </criteriaSet>
              <value>
                <real>[...]</real>
              </value>
            </criterionValue>
            <criterionValue mcdaConcept="weakening">
              [...]
            </criterionValue>
            <criterionValue mcdaConcept="antagonistic">
              [...]
            </criterionValue>
          </criteraValues>
        
      

----------------------------


.. _ElectreConcordanceWithInteractions-PUT-input7:

weights
~~~~~~~


Description:
............

Weights of criteria to consider.



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _ElectreConcordanceWithInteractions-PUT-input2:

classes_profiles
~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Definitions of profiles (boundary or central) which should be used for classes (categories) representation.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _ElectreConcordanceWithInteractions-PUT-input8:

method_parameters
~~~~~~~~~~~~~~~~~


Description:
............

A set of parameters provided to tune up the module's operation.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** comparison_with

  *This parameter specifies the type of elements provided for comparison.

Choosing 'boundary_profiles' or 'central_profiles' requires providing inputs 'classes_profiles' and 'profiles_performance_table' as well (which are optional by default).*

  - **Type:** drop-down list
  - **Possible values:**
      - alternatives vs alternatives (XMCDA label : alternatives) (default)

      - alternatives vs boundary profiles (XMCDA label : boundary_profiles) 

      - alternatives vs central (characteristic) profiles (XMCDA label : central_profiles) 

- **Name:** z_function

  *Z(x, y) - function used to capture interaction effects in the ambiguity zone. There are multiple forms that can be chosen for it, but two of them have intuitive and meaningful interpretation: Z(x, y) = min{x, y} Z(x, y) = xy - the last one is prefereable, though.*

  - **Type:** drop-down list
  - **Possible values:**
      - Z(x, y) = xy (XMCDA label : multiplication) (default)

      - Z(x, y) = min{x, y} (XMCDA label : minimum) 


XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
        
        <methodParameters>
          <parameter name="comparison_with">
            <value>
              <label>%1</label>
            </value>
          </parameter>
          <parameter name="z_function">
            <value>
              <label>%2</label>
            </value>
          </parameter>
         </methodParameters>
        
      

----------------------------



.. _ElectreConcordanceWithInteractions-PUT_outputs:

Outputs
-------


- :ref:`concordance <ElectreConcordanceWithInteractions-PUT-output1>`
- :ref:`messages <ElectreConcordanceWithInteractions-PUT-output2>`

.. _ElectreConcordanceWithInteractions-PUT-output1:

concordance
~~~~~~~~~~~


Description:
............

Matrix of aggregated concordance indices computed from the given data.



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------


.. _ElectreConcordanceWithInteractions-PUT-output2:

messages
~~~~~~~~


Description:
............

Messages or errors generated by this module.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
