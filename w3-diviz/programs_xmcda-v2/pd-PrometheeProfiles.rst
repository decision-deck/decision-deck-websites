:orphan:



.. _PrometheeProfiles:

PrometheeProfiles
=================

:Provider: J-MCDA
:Version: 0.2

Description
-----------

 -**Contact:** Olivier Cailloux <olivier.cailloux@ecp.fr>

 -**Web page:** https://sourceforge.net/projects/j-mcda/

 -**Reference:** Promethee methods, Brans & Mareschal, in Multiple Criteria Decision Analysis: State of the Art Surveys.

  Computes the profiles of the given alternatives on the given criteria, a la Promethee.

Inputs
------


alternatives
~~~~~~~~~~~~


Description: 
............

  The alternatives to consider.

XMCDA related:
..............

 -**Tag:** alternatives

----------------------------


criteria
~~~~~~~~


Description: 
............

  The criteria to consider (with preference and indifference thresholds).

XMCDA related:
..............

 -**Tag:** criteria

----------------------------


performances
~~~~~~~~~~~~


Description: 
............

  The performances of the alternatives on the criteria to consider.

XMCDA related:
..............

 -**Tag:** performanceTable

----------------------------



Outputs
-------


promethee_profiles
~~~~~~~~~~~~~~~~~~


Description: 
............

  The profiles of the alternatives computed from the given input data.

XMCDA related:
..............

 -**Tag:** performanceTable

----------------------------


messages
~~~~~~~~


Description: 
............

  A status message.

XMCDA related:
..............

 -**Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_. 


