:orphan:



.. _DEAvalueADDPreferenceRelations-PUT:

DEAvalueADDPreferenceRelations
==============================

:Provider: PUT
:Version: 1.0

Description
-----------

Determines necessary and possible dominance relations for the given DMUs (alternatives) using additive Data Envelopment Analysis Model.

- **Contact:** Malgorzata Napieraj (napieraj.malgorzata@gmail.com)

- **Reference:** Gouveia M. C., Dias L. C., Antunes C. H., Additive DEA based on MCDA with imprecise information (2008).
			Greco S., Mousseau V., Slowinski R., The Possible and the Necessary for Multiple Criteria Group Decision (2009).



Inputs
------
(For outputs, see :ref:`below <DEAvalueADDPreferenceRelations-PUT_outputs>`)


- :ref:`inputsOutputs <DEAvalueADDPreferenceRelations-PUT-inputsOutputs>`
- :ref:`units <DEAvalueADDPreferenceRelations-PUT-units>`
- :ref:`performanceTable <DEAvalueADDPreferenceRelations-PUT-performanceTable>`
- :ref:`weightsLinearConstraints <DEAvalueADDPreferenceRelations-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <DEAvalueADDPreferenceRelations-PUT-methodParameters>`

.. _DEAvalueADDPreferenceRelations-PUT-inputsOutputs:

inputsOutputs
~~~~~~~~~~~~~


Description:
............

A list of criteria with specified preference direction. List has to contains at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output). In addition, minimum and maximum possible value for each critetion can be defined. If not defined, they will be computed from alternatives performances on given criteria.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    <criteria>
                        <criterion>
							<scale>
								<quantitative>
									<preferenceDirection>
										[...]
									</preferenceDirection>
								</quantitative>
							</scale>
							<minimum>[...]</minimum>
							<maximum>[...]</maximum>
                            [...]
                        </criterion>
                        [...]
                    </criteria>

----------------------------


.. _DEAvalueADDPreferenceRelations-PUT-units:

units
~~~~~


Description:
............

A list of alternatives (DMUs).



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>

----------------------------


.. _DEAvalueADDPreferenceRelations-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~


Description:
............

A list of alternatives (DMUs) performances.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    <performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>

----------------------------


.. _DEAvalueADDPreferenceRelations-PUT-weightsLinearConstraints:

weightsLinearConstraints
~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

A list of criteria linear constraints.



XMCDA related:
..............

- **Tag:** criteriaLinearConstraints

- **Code:**

  ::

    <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID>[...]</criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>

----------------------------


.. _DEAvalueADDPreferenceRelations-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~


Description:
............

"boundariesProvided" indicates if criteria values boundaries are given in criteria input or they have to be computed based on alternatives performances. "transformToUtilites" indicates if given alternatives values are utilities or if they have to be firstly transformed to utilities.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** boundariesProvided

  

  - **Default value:** false
- **Name:** transformToUtilities

  

  - **Default value:** true

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    <methodParameters>
							<parameter name="boundariesProvided">
								<value><boolean>%1</boolean></value>
							</parameter>
							<parameter name="transformToUtilities">
								<value><boolean>%2</boolean></value>
							</parameter>
					</methodParameters>

----------------------------



.. _DEAvalueADDPreferenceRelations-PUT_outputs:

Outputs
-------


- :ref:`possibleDominance <DEAvalueADDPreferenceRelations-PUT-possibleDominance>`
- :ref:`necessaryDominance <DEAvalueADDPreferenceRelations-PUT-necessaryDominance>`
- :ref:`messages <DEAvalueADDPreferenceRelations-PUT-messages>`

.. _DEAvalueADDPreferenceRelations-PUT-possibleDominance:

possibleDominance
~~~~~~~~~~~~~~~~~


Description:
............

A list of alternatives pairs being in a possible dominance relation. If list contains pair (A,B), it means that alternative A dominates alternative B.



XMCDA related:
..............

- **Tag:** alternativesComparisons

- **Code:**

  ::

    <alternativesComparisons mcdaConcept="possibleDominance">
						<pairs>
						  <pair>
							<initial>
							  <alternativeID>[...]</alternativeID>
							</initial>
							<terminal>
							  <alternativeID>[...]</alternativeID>
							</terminal>
							<value>
							  [...]
							</value>
						  </pair>	
						  [...]
						</pairs>
					</alternativesComparisons>

----------------------------


.. _DEAvalueADDPreferenceRelations-PUT-necessaryDominance:

necessaryDominance
~~~~~~~~~~~~~~~~~~


Description:
............

A list of alternatives pairs being in a necessary dominance relation. If list contains pair (A,B), it means that alternative A dominates alternative B.



XMCDA related:
..............

- **Tag:** alternativesComparisons

- **Code:**

  ::

    <alternativesComparisons mcdaConcept="necessaryDominance">
						<pairs>
						  <pair>
							<initial>
							  <alternativeID>[...]</alternativeID>
							</initial>
							<terminal>
							  <alternativeID>[...]</alternativeID>
							</terminal>
							<value>
							  [...]
							</value>
						  </pair>	
						  [...]
						</pairs>
					</alternativesComparisons>

----------------------------


.. _DEAvalueADDPreferenceRelations-PUT-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
