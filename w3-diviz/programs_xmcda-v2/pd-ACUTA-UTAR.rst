:orphan:



.. _ACUTA-UTAR:

ACUTA
=====

:Provider: UTAR
:Version: 2.0

Description
-----------

Computes ACUTA method - analytic center for UTA - which provides a set of additive value functions that have a central position in the polyhedron of admissible value functions, given some preference information (preference relation).

- **Contact:** Boris Leistedt (boris.leistedt@gmail.com)



Inputs
------
(For outputs, see :ref:`below <ACUTA-UTAR_outputs>`)


- :ref:`criteria <ACUTA-UTAR-crit>`
- :ref:`alternatives <ACUTA-UTAR-alt>`
- :ref:`performanceTable <ACUTA-UTAR-perfTable>`
- :ref:`preferencesDirections <ACUTA-UTAR-prefDir>`
- :ref:`segments <ACUTA-UTAR-critSeg>`
- :ref:`alternativesRanking <ACUTA-UTAR-altRank>` *(optional)*
- :ref:`alternativesPreferences <ACUTA-UTAR-altComp1>` *(optional)*
- :ref:`alternativesIndifferences <ACUTA-UTAR-altComp2>` *(optional)*
- :ref:`delta <ACUTA-UTAR-delta>` *(optional)*

.. _ACUTA-UTAR-crit:

criteria
~~~~~~~~


Description:
............

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
				
					<criteria>
						<criterion>
							<criterionID>[...]</criterionID>
						</criterion>
					</criteria>
				
			

----------------------------


.. _ACUTA-UTAR-alt:

alternatives
~~~~~~~~~~~~


Description:
............

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
				
					<alternatives>
						<alternative>
							<alternativeID>[...]</alternativeID>
						</alternative>
					</alternatives>
				
			

----------------------------


.. _ACUTA-UTAR-perfTable:

performanceTable
~~~~~~~~~~~~~~~~


Description:
............

Values of criteria for different alternatives. It must contains IDs of both criteria and alternatives previously described.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    
				
					<performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>[...]</criterionID>
								<value>
            							<real>[...]</real>
       						 	</value>
							</performance>
						</alternativePerformances>
					</performanceTable>
				
			

----------------------------


.. _ACUTA-UTAR-prefDir:

preferenceDirections
~~~~~~~~~~~~~~~~~~~~


Description:
............

Optimization direction for the selected criteria (min or max).



XMCDA related:
..............

- **Tag:** criteriaValues

- **Code:**

  ::

    
				
					<criteriaValues>
						<criterionValue>
							<criterionID>[...]</criterionID>
							<value>
            							<label>[...]</label>
       						 	</value>
						</criterionValue>
					</criteriaValues>
				
			

----------------------------


.. _ACUTA-UTAR-critSeg:

criteriaSegments
~~~~~~~~~~~~~~~~


Description:
............

Number of segments in each value function to be constructed by UTA.



XMCDA related:
..............

- **Tag:** criteriaValues

- **Code:**

  ::

    
				
					<criteriaValues>
						<criterionValue>
							<criterionID>[...]</criterionID>
							<value>
            							<integer>[...]</integer>
       						 	</value>
						</criterionValue>
					</criteriaValues>
				
			

----------------------------


.. _ACUTA-UTAR-altRank:

alternativesRank
~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Ranking (preorder) of alternatives, corresponding to pariwize preference and indifference statements.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
				
					<alternativesValues>
						<alternativeValue>
							<alternativeID>[...]</alternativeID>
							<value>
            							<integer>[...]</integer>
       						 	</value>
						</alternativeValue>
					</alternativesValues>
				
			

----------------------------


.. _ACUTA-UTAR-altComp1:

alternativesPreferences
~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Asymmetric part of the preference relation, representing strict preference statements, under the form of paiwise comparisons of alternatives.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
				
				<alternativesComparisons>
					<pairs>
						<pair>
							<initial>
								<alternativeID>[...]</alternativeID>
							</initial>
							<terminal>
								<alternativeID>[...]</alternativeID>
							</terminal>
						</pair>
						[...]
					</pairs>
				</alternativesComparisons>
				                			
			

----------------------------


.. _ACUTA-UTAR-altComp2:

alternativesIndifferences
~~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Symmetric part of the preference relation, representing indifference statements, under the form of paiwise comparisons of alternatives.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
				
				<alternativesComparisons>
					<pairs>
						<pair>
							<initial>
								<alternativeID>[...]</alternativeID>
							</initial>
							<terminal>
								<alternativeID>[...]</alternativeID>
							</terminal>
						</pair>
						[...]
					</pairs>
				</alternativesComparisons>
				                			
			

----------------------------


.. _ACUTA-UTAR-delta:

delta
~~~~~


Description:
............

Optional delta value for UTA - delta is the utility gap between two successive alternatives in the preference ranking.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** delta

  *Delta value for UTA*

  - **Constraint description:** Delta should be positive and small 

  - **Type:** float

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
				
					<methodParameters>
						<parameter>
							<value>
								<real>%1</real>
							</value>
						</parameter>
					</methodParameters>
				
			

----------------------------



.. _ACUTA-UTAR_outputs:

Outputs
-------


- :ref:`valueFunctions <ACUTA-UTAR-valueFunctions>`
- :ref:`message <ACUTA-UTAR-logMessage>`

.. _ACUTA-UTAR-valueFunctions:

valueFunctions
~~~~~~~~~~~~~~


Description:
............

Constructed value functions for the selected criteria and the provided rankings, using ACUTA method.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
				
					<criteria mcdaConcept="criteria">
						<criterion>
							<criterionID>[...]</criterionID>
							<criterionFunction>
								<points>
									<point>
										<abscissa><real>[...]</real></abscissa>
										<ordinate><real>[...]</real></ordinate>
									</point>
								</points>
							</criterionFunction>
						</criterion>
					</criteria>
				
			

----------------------------


.. _ACUTA-UTAR-logMessage:

messages
~~~~~~~~


Description:
............

Log message.



XMCDA related:
..............

- **Tag:** methodMessages

- **Code:**

  ::

    
				
					<methodMessages mcdaConcept="methodMessage">
						<logMessage>
							<text>[...]</text>
						</logMessage>
						<errorMessage>
							<text>[...]</text>
						</errorMessage>
					</methodMessages>
				
			

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
