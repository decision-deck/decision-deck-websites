:orphan:



.. _ImpreciseDEA-ValueAdditive_preferenceRelations-PUT:

ImpreciseDEA-ValueAdditive_preferenceRelations
==============================================

:Provider: PUT
:Version: 1.0

Description
-----------

Computes efficiency scores for the given DMUs (alternatives)  using Additive Data Envelopment Analysis Model with imprecise information.

- **Contact:** Anna Labijak <support@decision-deck.org>

- **Reference:** G. Jahanshahloo, F. H. Lotfi, M. R. Malkhalifeh, and M. A. Namin. A generalized model for data envelopment analysis with interval data. 2009.

- **Reference:** MC. Gouveia et al. Super-efficiency and stability intervals in additive DEA. 2013



Inputs
------
(For outputs, see :ref:`below <ImpreciseDEA-ValueAdditive_preferenceRelations-PUT_outputs>`)


- :ref:`units <ImpreciseDEA-ValueAdditive_preferenceRelations-PUT-units>`
- :ref:`inputsOutputs <ImpreciseDEA-ValueAdditive_preferenceRelations-PUT-inputsOutputs>`
- :ref:`performanceTable <ImpreciseDEA-ValueAdditive_preferenceRelations-PUT-performanceTable>`
- :ref:`maxPerformanceTable <ImpreciseDEA-ValueAdditive_preferenceRelations-PUT-maxPerformanceTable>` *(optional)*
- :ref:`weightsLinearConstraints <ImpreciseDEA-ValueAdditive_preferenceRelations-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <ImpreciseDEA-ValueAdditive_preferenceRelations-PUT-methodParameters>`

.. _ImpreciseDEA-ValueAdditive_preferenceRelations-PUT-units:

units
~~~~~


Description:
............

A list of alternatives (DMUs).



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
        <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
      

----------------------------


.. _ImpreciseDEA-ValueAdditive_preferenceRelations-PUT-inputsOutputs:

inputs/outputs
~~~~~~~~~~~~~~


Description:
............

A list of criteria with specified preference direction. List has to contains at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output).



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
        <criteria>
                        <criterion>
							<scale>
                                [...]
							</scale>
                            [...]
                        </criterion>
                        [...]
                    </criteria>
      

----------------------------


.. _ImpreciseDEA-ValueAdditive_preferenceRelations-PUT-performanceTable:

performance
~~~~~~~~~~~


Description:
............

A list of alternatives (DMUs) minimal performances (or exact performances if intervals should be created by tolerance).



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    
        <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>
      

----------------------------


.. _ImpreciseDEA-ValueAdditive_preferenceRelations-PUT-maxPerformanceTable:

max performance
~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A list of alternatives (DMUs) maximal performances.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    
        <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>
      

----------------------------


.. _ImpreciseDEA-ValueAdditive_preferenceRelations-PUT-weightsLinearConstraints:

weights constraints
~~~~~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

A list of criteria linear constraints.



XMCDA related:
..............

- **Tag:** criteriaLinearConstraints

- **Code:**

  ::

    
        <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID> [...] </criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>
      

----------------------------


.. _ImpreciseDEA-ValueAdditive_preferenceRelations-PUT-methodParameters:

method parameters
~~~~~~~~~~~~~~~~~


Description:
............

Represents method parameters.
          "tolerance" represents the fraction for creating interval data (created interval: [data*(1-tolerance), data*(1+tolerance)]),
          "transformToUtilities" means if data should be tranformed into values from range [0-1],
          "boundariesProvided" means if inputsOutputs file contains information about min and max data for each factor,
          "functionShapeProvided" means if inputsOutputs file contains information about the shapes of value function for given factor.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** tolerance

  

  - **Constraint description:** The value should be non-negative.

  - **Type:** float
  - **Default value:** 0.00
- **Name:** transform to utilities

  

  - **Default value:** true
- **Name:** boundaries provided

  

  - **Default value:** false
- **Name:** function shapes provided

  

  - **Default value:** false

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
	<methodParameters>
		<parameter id="tolerance">
			<value><real>%1</real></value>
		</parameter>
		<parameter id="transformToUtilities">
			<value><boolean>%2</boolean></value>
		</parameter>
		<parameter id="boundariesProvided">
			<value><boolean>%3</boolean></value>
		</parameter>
		<parameter id="functionShapeProvided">
			<value><boolean>%4</boolean></value>
		</parameter>
	</methodParameters>


----------------------------



.. _ImpreciseDEA-ValueAdditive_preferenceRelations-PUT_outputs:

Outputs
-------


- :ref:`necessaryDominance <ImpreciseDEA-ValueAdditive_preferenceRelations-PUT-necessaryDominance>`
- :ref:`possibleDominance <ImpreciseDEA-ValueAdditive_preferenceRelations-PUT-possibleDominance>`
- :ref:`messages <ImpreciseDEA-ValueAdditive_preferenceRelations-PUT-messages>`

.. _ImpreciseDEA-ValueAdditive_preferenceRelations-PUT-necessaryDominance:

necessary dominance
~~~~~~~~~~~~~~~~~~~


Description:
............

A list of pairs of pairs of DMU related with necessary preference relation.



XMCDA related:
..............

- **Tag:** alternativesComparisons

- **Code:**

  ::

    
        <alternativesComparisons>
						<pairs>
              <pair>
                  <initial>
                    <alternativeID>[...]</alternativeID>
                  </initial>
                  <terminal>
                    <alternativeID>[...]</alternativeID>
                  </terminal>
                  <values>
                    <value>
                      <real>1</real>
                    <value>
                  </values>
              </pair>
						  [...]
            </pairs>
            [...]
					</alternativesComparisons>
      

----------------------------


.. _ImpreciseDEA-ValueAdditive_preferenceRelations-PUT-possibleDominance:

possible dominance
~~~~~~~~~~~~~~~~~~


Description:
............

A list of pairs of pairs of DMU related with possible preference relation.



XMCDA related:
..............

- **Tag:** alternativesComparisons

- **Code:**

  ::

    
        <alternativesComparisons>
						<pairs>
              <pair>
                <initial>
                  <alternativeID>[...]</alternativeID>
                </initial>
                <terminal>
                  <alternativeID>[...]</alternativeID>
                </terminal>
                <values>
                  <value>
                    <real>1</real>
                  </value>
                </value>
            </pair>
						[...]
          </pairs>
          [...]
					</alternativesComparisons>
      

----------------------------


.. _ImpreciseDEA-ValueAdditive_preferenceRelations-PUT-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
