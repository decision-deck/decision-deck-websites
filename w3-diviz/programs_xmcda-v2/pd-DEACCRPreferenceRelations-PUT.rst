:orphan:



.. _DEACCRPreferenceRelations-PUT:

DEACCRPreferenceRelations
=========================

:Provider: PUT
:Version: 1.0

Description
-----------

Determines necessary and possible dominance relations for the given DMUs (alternatives) using CCR Data Envelopment Analysis Model.

- **Contact:** Malgorzata Napieraj (napieraj.malgorzata@gmail.com)

- **Reference:** Salo A., Punkka A., Ranking Intervals and Dominance Relations for Ratio-Based Efficiency Analysis (2011).



Inputs
------
(For outputs, see :ref:`below <DEACCRPreferenceRelations-PUT_outputs>`)


- :ref:`inputsOutputs <DEACCRPreferenceRelations-PUT-inputsOutputs>`
- :ref:`units <DEACCRPreferenceRelations-PUT-units>`
- :ref:`performanceTable <DEACCRPreferenceRelations-PUT-performanceTable>`
- :ref:`weightsLinearConstraints <DEACCRPreferenceRelations-PUT-weightsLinearConstraints>` *(optional)*

.. _DEACCRPreferenceRelations-PUT-inputsOutputs:

inputsOutputs
~~~~~~~~~~~~~


Description:
............

A list of criteria with specified preference direction. List has to contains at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output).



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    <criteria>
                        <criterion>
							<scale>
								<quantitative>
									<preferenceDirection>
										[...]
									</preferenceDirection>
								</quantitative>
							</scale>
                            [...]
                        </criterion>
                        [...]
                    </criteria>

----------------------------


.. _DEACCRPreferenceRelations-PUT-units:

units
~~~~~


Description:
............

A list of alternatives (DMUs).



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>

----------------------------


.. _DEACCRPreferenceRelations-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~


Description:
............

A list of alternatives (DMUs) performances.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    <performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>

----------------------------


.. _DEACCRPreferenceRelations-PUT-weightsLinearConstraints:

weightsLinearConstraints
~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

A list of criteria linear constraints.



XMCDA related:
..............

- **Tag:** criteriaLinearConstraints

- **Code:**

  ::

    <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID>[...]</criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>

----------------------------



.. _DEACCRPreferenceRelations-PUT_outputs:

Outputs
-------


- :ref:`possibleDominance <DEACCRPreferenceRelations-PUT-possibleDominance>`
- :ref:`necessaryDominance <DEACCRPreferenceRelations-PUT-necessaryDominance>`
- :ref:`messages <DEACCRPreferenceRelations-PUT-messages>`

.. _DEACCRPreferenceRelations-PUT-possibleDominance:

possibleDominance
~~~~~~~~~~~~~~~~~


Description:
............

A list of alternatives pairs being in a possible dominance relation. If list contains pair (A,B), it means that alternative A dominates alternative B.



XMCDA related:
..............

- **Tag:** alternativesComparisons

- **Code:**

  ::

    <alternativesComparisons mcdaConcept="possibleDominance">
						<pairs>
						  <pair>
							<initial>
							  <alternativeID>[...]</alternativeID>
							</initial>
							<terminal>
							  <alternativeID>[...]</alternativeID>
							</terminal>
							<value>
							  [...]
							</value>
						  </pair>	
						  [...]
						</pairs>
					</alternativesComparisons>

----------------------------


.. _DEACCRPreferenceRelations-PUT-necessaryDominance:

necessaryDominance
~~~~~~~~~~~~~~~~~~


Description:
............

A list of alternatives pairs being in a necessary dominance relation. If list contains pair (A,B), it means that alternative A dominates alternative B.



XMCDA related:
..............

- **Tag:** alternativesComparisons

- **Code:**

  ::

    <alternativesComparisons mcdaConcept="necessaryDominance">
						<pairs>
						  <pair>
							<initial>
							  <alternativeID>[...]</alternativeID>
							</initial>
							<terminal>
							  <alternativeID>[...]</alternativeID>
							</terminal>
							<value>
							  [...]
							</value>
						  </pair>	
						  [...]
						</pairs>
					</alternativesComparisons>

----------------------------


.. _DEACCRPreferenceRelations-PUT-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
