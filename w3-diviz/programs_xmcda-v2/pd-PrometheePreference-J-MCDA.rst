:orphan:



.. _PrometheePreference-J-MCDA:

PrometheePreference
===================

:Provider: J-MCDA
:Version: 0.5.5

Description
-----------

Computes a preference relation, a la Promethee.

- **Contact:** Olivier Cailloux <olivier.cailloux@ecp.fr>

- **Web page:** http://sourceforge.net/projects/j-mcda/

- **Reference:** Cailloux, Olivier. Electre and Promethee MCDA methods as reusable software components. In Proceedings of the 25th Mini-EURO Conference on Uncertainty and Robustness in Planning and Decision Making (URPDM 2010). Coimbra, Portugal, 2010.



Inputs
------
(For outputs, see :ref:`below <PrometheePreference-J-MCDA_outputs>`)


- :ref:`criteria <PrometheePreference-J-MCDA-input1>`
- :ref:`alternatives <PrometheePreference-J-MCDA-input0>` *(optional)*
- :ref:`performances <PrometheePreference-J-MCDA-input3>`
- :ref:`weights <PrometheePreference-J-MCDA-input2>`

.. _PrometheePreference-J-MCDA-input1:

criteria
~~~~~~~~


Description:
............

The criteria to consider, possibly with preference and indifference thresholds. Each one must have a preference direction. Set some criteria as inactive (or remove them) to ignore them.



XMCDA related:
..............

- **Tag:** criteria

----------------------------


.. _PrometheePreference-J-MCDA-input0:

alternatives
~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The alternatives to consider. Set some alternatives as inactive (or remove them) to ignore them.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _PrometheePreference-J-MCDA-input3:

performances
~~~~~~~~~~~~


Description:
............

The performances of the alternatives on the criteria to consider.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _PrometheePreference-J-MCDA-input2:

weights
~~~~~~~


Description:
............

The weights of the criteria to consider.



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------



.. _PrometheePreference-J-MCDA_outputs:

Outputs
-------


- :ref:`preference <PrometheePreference-J-MCDA-output0>`
- :ref:`messages <PrometheePreference-J-MCDA-output1>`

.. _PrometheePreference-J-MCDA-output0:

preference
~~~~~~~~~~


Description:
............

The preference relation computed from the given input data.



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------


.. _PrometheePreference-J-MCDA-output1:

messages
~~~~~~~~


Description:
............

A status message.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
