:orphan:



.. _PrometheeProfiles-J-MCDA:

PrometheeProfiles
=================

:Provider: J-MCDA
:Version: 0.5.5

Description
-----------

Computes the profiles of the given alternatives on the given criteria, a la Promethee.

- **Contact:** Olivier Cailloux <olivier.cailloux@ecp.fr>

- **Web page:** http://sourceforge.net/projects/j-mcda/

- **Reference:** Promethee methods, Brans & Mareschal, in Multiple Criteria Decision Analysis: State of the Art Surveys.



Inputs
------
(For outputs, see :ref:`below <PrometheeProfiles-J-MCDA_outputs>`)


- :ref:`criteria <PrometheeProfiles-J-MCDA-input1>`
- :ref:`alternatives <PrometheeProfiles-J-MCDA-input0>` *(optional)*
- :ref:`performances <PrometheeProfiles-J-MCDA-input2>`

.. _PrometheeProfiles-J-MCDA-input1:

criteria
~~~~~~~~


Description:
............

The criteria to consider, possibly with preference and indifference thresholds. Each one must have a preference direction. Set some criteria as inactive (or remove them) to ignore them.



XMCDA related:
..............

- **Tag:** criteria

----------------------------


.. _PrometheeProfiles-J-MCDA-input0:

alternatives
~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The alternatives to consider. Set some alternatives as inactive (or remove them) to ignore them.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _PrometheeProfiles-J-MCDA-input2:

performances
~~~~~~~~~~~~


Description:
............

The performances of the alternatives on the criteria to consider.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------



.. _PrometheeProfiles-J-MCDA_outputs:

Outputs
-------


- :ref:`promethee_profiles <PrometheeProfiles-J-MCDA-output0>`
- :ref:`messages <PrometheeProfiles-J-MCDA-output1>`

.. _PrometheeProfiles-J-MCDA-output0:

promethee_profiles
~~~~~~~~~~~~~~~~~~


Description:
............

The profiles of the alternatives computed from the given input data.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _PrometheeProfiles-J-MCDA-output1:

messages
~~~~~~~~


Description:
............

A status message.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
