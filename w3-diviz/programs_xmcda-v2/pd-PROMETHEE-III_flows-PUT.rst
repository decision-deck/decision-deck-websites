:orphan:



.. _PROMETHEE-III_flows-PUT:

PROMETHEE-III_flows
===================

:Provider: PUT
:Version: 1.0.0

Description
-----------

This module is calculating ranking using Promethee III method. The result is shown as the table of outranking relationship, where the relationship is a weak preference. The second output presents the limits of the interval for every alternative.

- **Contact:** Magdalena Dzi?cielska <magdalenadziecielska6@gmail.com>

- **Web page:** https://github.com/MagdalenaDziecielska/PrometheeDiviz



Inputs
------
(For outputs, see :ref:`below <PROMETHEE-III_flows-PUT_outputs>`)


- :ref:`alternatives <PROMETHEE-III_flows-PUT-input1>`
- :ref:`preferences <PROMETHEE-III_flows-PUT-input2>`
- :ref:`parameters <PROMETHEE-III_flows-PUT-input3>`

.. _PROMETHEE-III_flows-PUT-input1:

alternatives
~~~~~~~~~~~~


Description:
............

Alternatives to consider.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _PROMETHEE-III_flows-PUT-input2:

preferences
~~~~~~~~~~~


Description:
............

Aggregated preferences.



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------


.. _PROMETHEE-III_flows-PUT-input3:

method parameters
~~~~~~~~~~~~~~~~~


Description:
............

Parameter alpha required in Promethee III flows calculation.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** alpha

  *An alpha parameter.*

  - **Type:** float
  - **Default value:** 0.0

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
        
				<methodParameters>
					<parameter id="alpha">
                        <value>
                            <real>%1</real>
                        </value>
					</parameter>
				</methodParameters>
			
      

----------------------------



.. _PROMETHEE-III_flows-PUT_outputs:

Outputs
-------


- :ref:`ranking <PROMETHEE-III_flows-PUT-output1>`
- :ref:`intervals <PROMETHEE-III_flows-PUT-output2>`
- :ref:`messages <PROMETHEE-III_flows-PUT-output3>`

.. _PROMETHEE-III_flows-PUT-output1:

ranking
~~~~~~~


Description:
............

Result computed from the given data.



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------


.. _PROMETHEE-III_flows-PUT-output2:

intervals
~~~~~~~~~


Description:
............

Flows final intervals.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _PROMETHEE-III_flows-PUT-output3:

messages
~~~~~~~~


Description:
............

Messages or errors generated by this module.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
