:orphan:



.. _ElectreDiscordance-PUT:

ElectreDiscordance
==================

:Provider: PUT
:Version: 0.2.0

Description
-----------

ElectreDiscordance - computes partial (i.e. per-criterion) discordance matrix using procedure which is common to the most methods from the Electre family.

The key feature of this module is its flexibility in terms of the types of elements allowed to compare, i.e. alternatives vs alternatives, alternatives vs boundary profiles and alternatives vs central (characteristic) profiles.

It also brings two new concepts: a 'counter-veto' threshold (cv) and 'pre-veto' threshold (pv) such as: cv >= v >= pv >= p (where 'v' is 'veto' threshold, and 'p' is 'preference' threshold).

- **Web page:** http://github.com/xor-xor/electre_diviz



Inputs
------
(For outputs, see :ref:`below <ElectreDiscordance-PUT_outputs>`)


- :ref:`criteria <ElectreDiscordance-PUT-input3>`
- :ref:`alternatives <ElectreDiscordance-PUT-input1>`
- :ref:`performance_table <ElectreDiscordance-PUT-input4>`
- :ref:`profiles_performance_table <ElectreDiscordance-PUT-input5>` *(optional)*
- :ref:`classes_profiles <ElectreDiscordance-PUT-input2>` *(optional)*
- :ref:`method_parameters <ElectreDiscordance-PUT-input6>`

.. _ElectreDiscordance-PUT-input3:

criteria
~~~~~~~~


Description:
............

Criteria to consider, possibly with preference, veto, pre-veto and counter-veto thresholds. Each criterion must have a preference direction specified (min or max). It is worth mentioning that this module allows to define thresholds as constants as well as linear functions.



XMCDA related:
..............

- **Tag:** criteria

----------------------------


.. _ElectreDiscordance-PUT-input1:

alternatives
~~~~~~~~~~~~


Description:
............

Alternatives to consider.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _ElectreDiscordance-PUT-input4:

performance_table
~~~~~~~~~~~~~~~~~


Description:
............

The performance of alternatives.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _ElectreDiscordance-PUT-input5:

profiles_performance_table
~~~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The performance of profiles (boundary or central).



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _ElectreDiscordance-PUT-input2:

classes_profiles
~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Definitions of profiles (boundary or central) which should be used for classes (categories) representation.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _ElectreDiscordance-PUT-input6:

method_parameters
~~~~~~~~~~~~~~~~~


Description:
............

A set of parameters provided to tune up the module's operation.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** comparison_with

  *This parameter specifies the type of elements provided for comparison.

Choosing 'boundary_profiles' or 'central_profiles' requires providing inputs 'classes_profiles' and 'profiles_performance_table' as well (which are optional by default).*

  - **Type:** drop-down list
  - **Possible values:**
      - alternatives vs alternatives (XMCDA label : alternatives) (default)

      - alternatives vs boundary profiles (XMCDA label : boundary_profiles) 

      - alternatives vs central (characteristic) profiles (XMCDA label : central_profiles) 

- **Name:** use_pre_veto

  *This parameter specifies if you want to take into account another (optional) threshold called 'pre-veto threshold', such as: v >= pv >= p (where 'v' is 'normal' veto threshold, 'pv' is the pre-veto threshold and 'p' is preference threshold).*

  - **Default value:** false

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
        
        <methodParameters>
          <parameter name="comparison_with">
            <value>
              <label>%1</label>
            </value>
          </parameter>
          <parameter name="use_pre_veto">
            <value>
              <boolean>%2</boolean>
            </value>
          </parameter>
        </methodParameters>
        
      

----------------------------



.. _ElectreDiscordance-PUT_outputs:

Outputs
-------


- :ref:`discordance <ElectreDiscordance-PUT-output1>`
- :ref:`counter_veto_crossed <ElectreDiscordance-PUT-output2>`
- :ref:`messages <ElectreDiscordance-PUT-output3>`

.. _ElectreDiscordance-PUT-output1:

discordance
~~~~~~~~~~~


Description:
............

Partial (i.e. per-criterion) discordance indices computed from the given data.



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------


.. _ElectreDiscordance-PUT-output2:

counter_veto_crossed
~~~~~~~~~~~~~~~~~~~~


Description:
............

This input contains information regarding the number of criteria per pair of the alternatives/profiles on which the 'counter-veto' threshold has been crossed. Please note that this output will be created even if 'counter-veto' threshold has not been defined - in such case, all the values will be set to 'false'.



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------


.. _ElectreDiscordance-PUT-output3:

messages
~~~~~~~~


Description:
............

Messages or errors generated by this module.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
