:orphan:



.. _DEASMAAvalueADDRanks-PUT:

DEASMAAvalueADDRanks
====================

:Provider: PUT
:Version: 1.0

Description
-----------

Computes rankings for the given DMUs (alternatives) using SMAA-D method and additive Data Envelopment Analysis Model. For given number of samples  returns a matrix with alternatives in each row and rankings in each column. Single cell indicates how many samples of respective alternative gave respective position in ranking.

- **Contact:** Malgorzata Napieraj (napieraj.malgorzata@gmail.com)

- **Reference:** Gouveia M. C., Dias L. C., Antunes C. H., Additive DEA based on MCDA with imprecise information (2008).
			Lahdelma R., Salminen P., Stochastic multicriteria acceptability analysis using the data envelopment model (2004).



Inputs
------
(For outputs, see :ref:`below <DEASMAAvalueADDRanks-PUT_outputs>`)


- :ref:`inputsOutputs <DEASMAAvalueADDRanks-PUT-inputsOutputs>`
- :ref:`units <DEASMAAvalueADDRanks-PUT-units>`
- :ref:`performanceTable <DEASMAAvalueADDRanks-PUT-performanceTable>`
- :ref:`weightsLinearConstraints <DEASMAAvalueADDRanks-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <DEASMAAvalueADDRanks-PUT-methodParameters>`

.. _DEASMAAvalueADDRanks-PUT-inputsOutputs:

inputsOutputs
~~~~~~~~~~~~~


Description:
............

A list of criteria with specified preference direction. List has to contains at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output).



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    <criteria>
                        <criterion>
							<scale>
								<quantitative>
									<preferenceDirection>
										[...]
									</preferenceDirection>
								</quantitative>
							</scale>
                            [...]
                        </criterion>
                        [...]
                    </criteria>

----------------------------


.. _DEASMAAvalueADDRanks-PUT-units:

units
~~~~~


Description:
............

A list of alternatives (DMUs).



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>

----------------------------


.. _DEASMAAvalueADDRanks-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~


Description:
............

A list of alternatives (DMUs) performances.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    <performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>

----------------------------


.. _DEASMAAvalueADDRanks-PUT-weightsLinearConstraints:

weightsLinearConstraints
~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

A list of criteria linear constraints.



XMCDA related:
..............

- **Tag:** criteriaLinearConstraints

- **Code:**

  ::

    <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID>[...]</criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>

----------------------------


.. _DEASMAAvalueADDRanks-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~


Description:
............

"boundariesProvided" indicates if criteria values boundaries are given in criteria input or they have to be computed based on alternatives performances. "samplesNo" represents the number of samples to generate. "transformToUtilites" indicates if given alternatives values are utilities or if they have to be firstly transformed to utilities.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** boundariesProvided

  

  - **Default value:** false
- **Name:** samplesNo

  

  - **Constraint description:** The value should be a positive integer.

  - **Type:** integer
  - **Default value:** 100
- **Name:** transformToUtilities

  

  - **Default value:** true

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    <methodParameters>
							<parameter name="boundariesProvided">
								<value><boolean>%1</boolean></value>
							</parameter>
							<parameter name="samplesNo">
								<value><integer>%2</integer></value>
							</parameter>
							<parameter name="transformToUtilities">
								<value><boolean>%3</boolean></value>
							</parameter>
					</methodParameters>

----------------------------



.. _DEASMAAvalueADDRanks-PUT_outputs:

Outputs
-------


- :ref:`rankAcceptabilityIndices <DEASMAAvalueADDRanks-PUT-rankAcceptabilityIndices>`
- :ref:`avgRank <DEASMAAvalueADDRanks-PUT-avgRank>`
- :ref:`messages <DEASMAAvalueADDRanks-PUT-messages>`

.. _DEASMAAvalueADDRanks-PUT-rankAcceptabilityIndices:

rankAcceptabilityIndices
~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

A performance table for given alternatives. Single performance consists of attribute criterionID representing certain ranking, and a value representing ratio of samples attaining this ranking.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    <performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>Rank [...]</criterionID>
									<value>
									[...]
									</value>
							</performance>
							[...]
						</alternativePerformances>
					</performanceTable>

----------------------------


.. _DEASMAAvalueADDRanks-PUT-avgRank:

avgRank
~~~~~~~


Description:
............

A list of alternatives with average rank (obtained for given sample).



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    <alternativesValues mcdaConcept="efficiency">
						<alternativeValue>
						  <alternativeID>[...]</alternativeID>
						  <value>
							[...]
						  </value>
						</alternativeValue>
						[...]
					</alternativesValues>

----------------------------


.. _DEASMAAvalueADDRanks-PUT-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
