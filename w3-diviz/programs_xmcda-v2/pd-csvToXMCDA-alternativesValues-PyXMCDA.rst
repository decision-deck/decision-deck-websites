:orphan:



.. _csvToXMCDA-alternativesValues-PyXMCDA:

csvToXMCDA-alternativesValues
=============================

:Provider: PyXMCDA
:Version: 1.0

Description
-----------

Transforms a file containing alternatives values from a comma-separated values (CSV) file to two XMCDA compliant files, containing the corresponding alternatives ids and their alternativesValues.

- **Contact:** Sébastien Bigaret (sebastien.bigaret@telecom-bretagne.eu)

- **Web page:** http://github.com/sbigaret/ws-PyXMCDA



Inputs
------
(For outputs, see :ref:`below <csvToXMCDA-alternativesValues-PyXMCDA_outputs>`)


- :ref:`alternativesValues.csv <csvToXMCDA-alternativesValues-PyXMCDA-csv>`

.. _csvToXMCDA-alternativesValues-PyXMCDA-csv:

alternativesValues (csv)
~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The alternatives and their alternatives values as a CSV file.  The first line is made of two cells, the first one being empty, and the second one will be the content of the attribute "mcdaConcept" in the tag "<alternativesValues>", if supplied.

Example::

    ,ranks
    alt1,1
    alt2,2.7
    alt3,3



XMCDA related:
..............

- **Tag:** other

----------------------------



.. _csvToXMCDA-alternativesValues-PyXMCDA_outputs:

Outputs
-------


- :ref:`alternatives <csvToXMCDA-alternativesValues-PyXMCDA-alternatives>`
- :ref:`alternativesValues <csvToXMCDA-alternativesValues-PyXMCDA-alternativesValues>`
- :ref:`messages <csvToXMCDA-alternativesValues-PyXMCDA-messages>`

.. _csvToXMCDA-alternativesValues-PyXMCDA-alternatives:

alternatives
~~~~~~~~~~~~


Description:
............

The equivalent alternatives ids.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _csvToXMCDA-alternativesValues-PyXMCDA-alternativesValues:

alternativesValues
~~~~~~~~~~~~~~~~~~


Description:
............

The equivalent alternatives values.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _csvToXMCDA-alternativesValues-PyXMCDA-messages:

messages
~~~~~~~~


Description:
............

Status messages.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
