:orphan:



.. _plotAlternativesValuesPreorder-ITTB:

plotAlternativesValuesPreorder
==============================

:Provider: ITTB
:Version: 1.1

Description
-----------

This web service generates a graph representing a preorder on the alternatives, according to numerical values taken by the alternatives (the "best" alternative has the highest value). Compared to the web service plotAlternativesValuesPreorder, some parameters are added. Colors can be used and the title of the graph can be typed. You can choose between an increasing or a decreasing order for the graph. It is also possible to show the name of the alternatives instead of the id, etc. The alternatives' evaluations are supposed to be real or integer numeric values.

- **Contact:** Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)



Inputs
------
(For outputs, see :ref:`below <plotAlternativesValuesPreorder-ITTB_outputs>`)


- :ref:`alternatives <plotAlternativesValuesPreorder-ITTB-alternatives>`
- :ref:`alternativesValues <plotAlternativesValuesPreorder-ITTB-alternativesValues>`
- :ref:`options <plotAlternativesValuesPreorder-ITTB-options>`

.. _plotAlternativesValuesPreorder-ITTB-alternatives:

alternatives
~~~~~~~~~~~~


Description:
............

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
                   
                    <alternatives>
                        <alternative>
                            <active>[...]</active>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
                    
               

----------------------------


.. _plotAlternativesValuesPreorder-ITTB-alternativesValues:

alternativesValues
~~~~~~~~~~~~~~~~~~


Description:
............

A list of <alternativesValue> representing a certain numeric quantity for each alternative, like, e.g., an overall value.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _plotAlternativesValuesPreorder-ITTB-options:

options
~~~~~~~


Description:
............

Options of the plotted graph.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** Chart title

  *Title of the graph.*

  - **Type:** string
- **Name:** Order

  *Increasing or decreasing order w.r.t. the input values.*

  - **Type:** drop-down list
  - **Possible values:**
      - Increasing (XMCDA label : increasing) 

      - Decreasing (XMCDA label : decreasing) (default)

- **Name:** Plot symbols

  *Symbol used in the plot.*

  - **Type:** drop-down list
  - **Possible values:**
      - Rectangle (XMCDA label : Rectangle) (default)

      - Square (XMCDA label : Square) 

      - Ellipse (XMCDA label : Ellipse) 

      - Circle (XMCDA label : Circle) 

      - Diamond (XMCDA label : Diamond) 

- **Name:** Display

  *Display alternatives' names or IDs.*

  - **Type:** drop-down list
  - **Possible values:**
      - Name (XMCDA label : true) 

      - ID (XMCDA label : false) (default)

- **Name:** Colored

  *Colored or bw graph.*

  - **Type:** drop-down list
  - **Possible values:**
      - Yes (XMCDA label : true) 

      - No (XMCDA label : false) (default)

- **Name:** Color

  *Color of the graph. Choose between "Black", "Red", "Blue", "Green", "Yellow", "Magenta" and "Cyan".*

  - **Type:** drop-down list
  - **Possible values:**
      - Black (XMCDA label : Black) (default)

      - Red (XMCDA label : Red) 

      - Blue (XMCDA label : Blue) 

      - Green (XMCDA label : Green) 

      - Yellow (XMCDA label : Yellow) 

      - Magenta (XMCDA label : Magenta) 

      - Cyan (XMCDA label : Cyan) 


XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                   
					<methodParameters>
						  <parameter id="plot_title" name="Plot Title">
							<value>
            					<label>%1</label>
       						 </value>
						</parameter>
						<parameter id="plot_order" name="Plot order">
							<value>
            					<label>%2</label>
       						 </value>
						</parameter>
						<parameter id="node_shape" name="Node shape">
							<value>
            					<label>%3</label>
       						 </value>
						</parameter>
						 <parameter id="show_names" name="Show alternatives names">
							<value>
            					<label>%4</label>
       						 </value>
						</parameter>
						 <parameter id="use_color" name="Colors in the graph">
							<value>
            					<label>%5</label>
       						 </value>
						</parameter>
						 <parameter id="selected_color" name="Selected color">
							<value>
            					<label>%6</label>
       						 </value>
						</parameter >
					</methodParameters>
				
               

----------------------------



.. _plotAlternativesValuesPreorder-ITTB_outputs:

Outputs
-------


- :ref:`alternativesValuesPlot <plotAlternativesValuesPreorder-ITTB-alternativesValuesPlot>`
- :ref:`messages <plotAlternativesValuesPreorder-ITTB-messages>`

.. _plotAlternativesValuesPreorder-ITTB-alternativesValuesPlot:

alternativesValuesPlot
~~~~~~~~~~~~~~~~~~~~~~


Description:
............

A string containing the base64 representation of the png image of the generated graph.



XMCDA related:
..............

- **Tag:** alternativeValue

----------------------------


.. _plotAlternativesValuesPreorder-ITTB-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
