:orphan:



.. _leastSquaresCapaIdent-kappalab:

leastSquaresCapaIdent
=====================

:Provider: kappalab
:Version: 1.0

Description
-----------

Identifies a Mobius capacity by means of an approach grounded on least squares optimization. More precisely, given a set of overall values for each alternative, and possibly additional linear constraints expressing preferences, importance of criteria, etc., this algorithm determines, if it exists, a capacity minimising the sum of squared errors between overall scores as given by the data and the output of the Choquet integral for those data, and compatible with the additional linear constraints. The existence is ensured if no additional constraint is given. The problem is solved using quadratic programming.

- **Contact:** Patrick Meyer (patrick.meyer@telecom-bretagne.eu)

- **Reference:** J-L. Marichal and M. Roubens (2000), Determination of weights of interacting criteria from a reference set, European Journal of Operational Research 124, pages 641-650. 



Inputs
------
(For outputs, see :ref:`below <leastSquaresCapaIdent-kappalab_outputs>`)


- :ref:`criteria <leastSquaresCapaIdent-kappalab-criteria>`
- :ref:`alternatives <leastSquaresCapaIdent-kappalab-alternatives>`
- :ref:`performanceTable <leastSquaresCapaIdent-kappalab-performanceTable>`
- :ref:`shapleyPreorder <leastSquaresCapaIdent-kappalab-shapleyPreorder>` *(optional)*
- :ref:`interactionPreorder <leastSquaresCapaIdent-kappalab-interactionPreorder>` *(optional)*
- :ref:`shapleyInterval <leastSquaresCapaIdent-kappalab-shapleyInterval>` *(optional)*
- :ref:`interactionInterval <leastSquaresCapaIdent-kappalab-interactionInterval>` *(optional)*
- :ref:`overallValues <leastSquaresCapaIdent-kappalab-overallValues>`
- :ref:`kAdditivity <leastSquaresCapaIdent-kappalab-kAdditivity>`

.. _leastSquaresCapaIdent-kappalab-criteria:

criteria
~~~~~~~~


Description:
............

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
                
                    <criteria>
                        <criterion>
                            <active>[...]</active>
                            [...]
                        </criterion>
                        [...]
                    </criteria>
                    
            

----------------------------


.. _leastSquaresCapaIdent-kappalab-alternatives:

alternatives
~~~~~~~~~~~~


Description:
............

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
                
                    <alternatives>
                        <alternative>
                            <active>[...]</active>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
                    
            

----------------------------


.. _leastSquaresCapaIdent-kappalab-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~


Description:
............

A performance table. The evaluations should be numeric values, i.e. <real>, <integer> or <rational>.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _leastSquaresCapaIdent-kappalab-shapleyPreorder:

shapleyPreorder
~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A valued relation on criteria expressing importance constraints on the critera. A numeric <value> indicates a minimal preference threshold for each <pair>. One <pair> represents an affirmation of the type "the Shapley importance index of criterion g1 is greater than the Shapley importance index of criterion g2 with preference threshold delta".



XMCDA related:
..............

- **Tag:** criteriaComparisons

----------------------------


.. _leastSquaresCapaIdent-kappalab-interactionPreorder:

interactionPreorder
~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A valued relation on pairs of criteria expressing constraints on value of the the Shapley interaction index. A numeric <value> indicates a minimal preference threshold for each <pair> of the relation. One <pair> represents a constraint of the type "the Shapley interaction index of the pair (g1,g2) of criteria is greater than the Shapley interaction index of the pair (g3,g4) of criteria with preference threshold delta".



XMCDA related:
..............

- **Tag:** criteriaComparisons

----------------------------


.. _leastSquaresCapaIdent-kappalab-shapleyInterval:

shapleyInterval
~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A list of <criterionValue> containing the constraints relative to the quantitative importance of the criteria. Each <criterionValue> contains an an <interval>. Each <criteriaValue> represents an affirmation of the type "the Shapley importance index of criterion g1 lies in the interval [a,b]".



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _leastSquaresCapaIdent-kappalab-interactionInterval:

interactionInterval
~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A list of <criterionValue> containing the constraints relative to the type and the magnitude of the Shapley interaction index for pairs of criteria. Each <criterionValue> contains an an <interval>. Each <criteriaValue> represents an affirmation of the type "the Shapley interaction index of the pair (g1,g2) of criteria lies in the interval [a,b]".



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _leastSquaresCapaIdent-kappalab-overallValues:

overallValues
~~~~~~~~~~~~~


Description:
............

A list of <alternativeValue> containing the desired overall value of each alternative. The <value> should be a numeric value.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _leastSquaresCapaIdent-kappalab-kAdditivity:

kAdditivity
~~~~~~~~~~~


Description:
............

Indicates the level of k-additivity of the Mobius capacity (the Mobius transform of subsets whose cardinal is superior to k vanishes).



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via the XMCDA file.

- **Name:** kAdditivity

  *Indicates the level of k-additivity of the Mobius capacity (the Mobius transform of subsets whose cardinal is superior to k vanishes).*

  - **Constraint description:** The value should be a positive integer, less than or equal to the number of criteria.

  - **Type:** integer
  - **Default value:** 1

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                
                    <methodParameters>
                        <parameter
                             name="kAdditivity"> <!-- REQUIRED  -->
                            <value>
                                <integer>%1</integer>
                            </value>
                        </parameter>
                    </methodParameters>
                    
            

----------------------------



.. _leastSquaresCapaIdent-kappalab_outputs:

Outputs
-------


- :ref:`mobiusCapacity <leastSquaresCapaIdent-kappalab-mobiusCapacity>`
- :ref:`residuals <leastSquaresCapaIdent-kappalab-residuals>`
- :ref:`messages <leastSquaresCapaIdent-kappalab-messages>`

.. _leastSquaresCapaIdent-kappalab-mobiusCapacity:

mobiusCapacity
~~~~~~~~~~~~~~


Description:
............

The Mobius transform of a capacity.



XMCDA related:
..............

- **Tag:** criteriaValues

- **Code:**

  ::

    
                
                    <criteriaValues mcdaConcept="mobiusCapacity">
                        <criterionValue>
                            <criteriaSet>
                                <element>
                                    <criterionID>[...]</criterionID>
                                </element>
                                [...]
                            </criteriaSet>
                            <value>
                                <real>[...]</real>
                            </value>
                        </criterionValue>
                        [...]
                    </criteriaValues>
                    
            

----------------------------


.. _leastSquaresCapaIdent-kappalab-residuals:

residuals
~~~~~~~~~


Description:
............

A list of <alternativeValue> representing the difference between the provided overall evaluations and those returned by the obtained model, for each alternative.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
                
                    <alternativesValues mcdaConcept="mobiusCapacity">
                        <alternativeValue>
                            [...]
                        </alternativeValue>
                        [...]
                    </alternativesValues>
                    
            

----------------------------


.. _leastSquaresCapaIdent-kappalab-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
