:orphan:



.. _HierarchicalDEA-ValueAdditive_preferenceRelations-PUT:

HierarchicalDEA-ValueAdditive_preferenceRelations
=================================================

:Provider: PUT
:Version: 1.0

Description
-----------

Computes necessary and possible preference relations for the pairs of DMUs (alternatives) using Additive Data Envelopment Analysis Model with hierarchical structure of inputs and outputs.

- **Contact:** 
            Anna Labijak <anna.labijak@cs.put.poznan.pl>
        



Inputs
------
(For outputs, see :ref:`below <HierarchicalDEA-ValueAdditive_preferenceRelations-PUT_outputs>`)


- :ref:`units <HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-units>`
- :ref:`performanceTable <HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-performanceTable>`
- :ref:`inputsOutputs <HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-inputsOutputs>`
- :ref:`hierarchy <HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-hierarchy>`
- :ref:`weightsLinearConstraints <HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-methodParameters>`

.. _HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-units:

units
~~~~~


Description:
............

A list of alternatives (DMUs).



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
                <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
            

----------------------------


.. _HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-performanceTable:

performance
~~~~~~~~~~~


Description:
............

A list of alternatives (DMUs) performances (on leaf hierarchy criteria).



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    
                <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>
            

----------------------------


.. _HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-inputsOutputs:

inputs/outputs
~~~~~~~~~~~~~~


Description:
............

A list of performance criteria (hierarchy leafs) and their preference direction. List has to contain at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output) for each hierarchy category.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
                <criteria>
                        <criterion>
							<scale>
                                [...]
							</scale>
                            [...]
                        </criterion>
                        [...]
                    </criteria>
            

----------------------------


.. _HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-hierarchy:

hierarchy
~~~~~~~~~


Description:
............

The hierarchical structure of criteria.



XMCDA related:
..............

- **Tag:** hierarchy

- **Code:**

  ::

    
                
                <hierarchy>
                    <node>
                        <criterionID>[...]</criterionID>
                        <node>
                            <criterionID>[...]</criterionID>
                            <node>
                                [...]
                            </node>
                            [...]
                        </node>
                        [...]
                    </node>
                </hierarchy>
            

----------------------------


.. _HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-weightsLinearConstraints:

weights constraints
~~~~~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

A list of hierarchy criteria linear constraints.



XMCDA related:
..............

- **Tag:** criteriaLinearConstraints

- **Code:**

  ::

    
                <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID> [...] </criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>
            

----------------------------


.. _HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-methodParameters:

parameters
~~~~~~~~~~


Description:
............

Represents parameters.
            "hierarchy node" is the ID of the hierarchy criterion for which the analysis should be performed;
            "transformToUtilities" means if data should be tranformed into values from range [0-1];
            "boundariesProvided" means if inputsOutputs file contains information about min and max data for each factor.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** hierarchy node

  

  - **Type:** string
  - **Default value:** "root"
- **Name:** transform to utilities

  

  - **Default value:** true
- **Name:** boundaries provided

  

  - **Default value:** false

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                
	<methodParameters>
		<parameter id="hierarchyNode">
            <value><label>%1</label></value>
        </parameter>
		<parameter id="transformToUtilities">
			<value><boolean>%2</boolean></value>
		</parameter>
		<parameter id="boundariesProvided">
			<value><boolean>%3</boolean></value>
		</parameter>
	</methodParameters>

            

----------------------------



.. _HierarchicalDEA-ValueAdditive_preferenceRelations-PUT_outputs:

Outputs
-------


- :ref:`necessaryDominance <HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-necessaryDominance>`
- :ref:`possibleDominance <HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-possibleDominance>`
- :ref:`messages <HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-messages>`

.. _HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-necessaryDominance:

necessary dominance
~~~~~~~~~~~~~~~~~~~


Description:
............

A list of pairs of DMU related with necessary preference relation.



XMCDA related:
..............

- **Tag:** alternativesComparisons

- **Code:**

  ::

    
                <alternativesComparisons>
						<pairs>
              <pair>
                  <initial>
                    <alternativeID>[...]</alternativeID>
                  </initial>
                  <terminal>
                    <alternativeID>[...]</alternativeID>
                  </terminal>
                  <values>
                    <value>
                      <real>1</real>
                    <value>
                  </values>
              </pair>
						  [...]
            </pairs>
            [...]
					</alternativesComparisons>
            

----------------------------


.. _HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-possibleDominance:

possible dominance
~~~~~~~~~~~~~~~~~~


Description:
............

A list of pairs of DMU related with possible preference relation.



XMCDA related:
..............

- **Tag:** alternativesComparisons

- **Code:**

  ::

    
                <alternativesComparisons>
						<pairs>
              <pair>
                <initial>
                  <alternativeID>[...]</alternativeID>
                </initial>
                <terminal>
                  <alternativeID>[...]</alternativeID>
                </terminal>
                <values>
                  <value>
                    <real>1</real>
                  </value>
                </value>
            </pair>
						[...]
          </pairs>
          [...]
					</alternativesComparisons>
            

----------------------------


.. _HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
