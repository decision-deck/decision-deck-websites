:orphan:



.. _PROMETHEE-FlowSort-GDSS_assignments-PUT:

PROMETHEE-FlowSort-GDSS_assignments
===================================

:Provider: PUT
:Version: 1.0.0

Description
-----------

Computes group class assignment for given data using FlowSortGDSS method.

- **Contact:** Maciej Uniejewski <maciej.uniejewski@gmail.com>

- **Web page:** https://github.com/maciej7777/PrometheeDiviz



Inputs
------
(For outputs, see :ref:`below <PROMETHEE-FlowSort-GDSS_assignments-PUT_outputs>`)


- :ref:`criteria <PROMETHEE-FlowSort-GDSS_assignments-PUT-input14>`
- :ref:`alternatives <PROMETHEE-FlowSort-GDSS_assignments-PUT-input1>`
- :ref:`categories <PROMETHEE-FlowSort-GDSS_assignments-PUT-input2>`
- :ref:`performance_table1 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input25>`
- :ref:`preferences1 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input35>`
- :ref:`flows1 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input15>`
- :ref:`categories_profiles1 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input3>`
- :ref:`performance_table2 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input26>`
- :ref:`preferences2 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input36>`
- :ref:`flows2 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input16>`
- :ref:`categories_profiles2 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input4>`
- :ref:`performance_table3 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input27>` *(optional)*
- :ref:`preferences3 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input37>` *(optional)*
- :ref:`flows3 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input17>` *(optional)*
- :ref:`categories_profiles3 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input5>` *(optional)*
- :ref:`performance_table4 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input28>` *(optional)*
- :ref:`preferences4 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input38>` *(optional)*
- :ref:`flows4 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input18>` *(optional)*
- :ref:`categories_profiles4 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input6>` *(optional)*
- :ref:`performance_table5 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input29>` *(optional)*
- :ref:`preferences5 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input39>` *(optional)*
- :ref:`flows5 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input19>` *(optional)*
- :ref:`categories_profiles5 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input7>` *(optional)*
- :ref:`performance_table6 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input30>` *(optional)*
- :ref:`preferences6 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input40>` *(optional)*
- :ref:`flows6 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input20>` *(optional)*
- :ref:`categories_profiles6 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input8>` *(optional)*
- :ref:`performance_table7 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input31>` *(optional)*
- :ref:`preferences7 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input41>` *(optional)*
- :ref:`flows7 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input21>` *(optional)*
- :ref:`categories_profiles7 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input9>` *(optional)*
- :ref:`performance_table8 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input32>` *(optional)*
- :ref:`preferences8 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input42>` *(optional)*
- :ref:`flows8 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input22>` *(optional)*
- :ref:`categories_profiles8 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input10>` *(optional)*
- :ref:`performance_table9 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input33>` *(optional)*
- :ref:`preferences9 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input43>` *(optional)*
- :ref:`flows9 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input23>` *(optional)*
- :ref:`categories_profiles9 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input11>` *(optional)*
- :ref:`performance_table10 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input34>` *(optional)*
- :ref:`preferences10 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input44>` *(optional)*
- :ref:`flows10 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input24>` *(optional)*
- :ref:`categories_profiles10 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input12>` *(optional)*
- :ref:`profiles_flows <PROMETHEE-FlowSort-GDSS_assignments-PUT-input45>`
- :ref:`categories_values <PROMETHEE-FlowSort-GDSS_assignments-PUT-input13>`
- :ref:`method_parameters <PROMETHEE-FlowSort-GDSS_assignments-PUT-input46>`

.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input14:

criteria
~~~~~~~~


Description:
............

Criteria to consider and their scales



XMCDA related:
..............

- **Tag:** criteria

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input1:

alternatives
~~~~~~~~~~~~


Description:
............

Alternatives to consider.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input2:

categories
~~~~~~~~~~


Description:
............

Definitions of categories.



XMCDA related:
..............

- **Tag:** categories

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input25:

profiles performances 1
~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The performances of profiles for decision maker 1.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input35:

preferences 1
~~~~~~~~~~~~~


Description:
............

The preference matrix computed for profiles and alternatives given by decision maker 1.



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input15:

alternatives flows 1
~~~~~~~~~~~~~~~~~~~~


Description:
............

Normalised flows of given alternatives for decision maker 1.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input3:

categories profiles 1
~~~~~~~~~~~~~~~~~~~~~


Description:
............

Definitions of boundary profiles for decision maker 1, which should be used for classes (categories) representation.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input26:

profiles performances 2
~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The performances of profiles for decision maker 2.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input36:

preferences 2
~~~~~~~~~~~~~


Description:
............

The preference matrix computed for profiles and alternatives given by decision maker 2.



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input16:

alternatives flows 2
~~~~~~~~~~~~~~~~~~~~


Description:
............

Normalised flows of given alternatives for decision maker 2.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input4:

categories profiles 2
~~~~~~~~~~~~~~~~~~~~~


Description:
............

Definitions of boundary profiles for decision maker 2, which should be used for classes (categories) representation.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input27:

profiles performances 3
~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The performances of profiles for decision maker 3.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input37:

preferences 3
~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The preference matrix computed for profiles and alternatives given by decision maker 3.



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input17:

alternatives flows 3
~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Normalised flows of given alternatives for decision maker 3.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input5:

categories profiles 3
~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Definitions of boundary profiles for decision maker 3, which should be used for classes (categories) representation.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input28:

profiles performances 4
~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The performances of profiles for decision maker 4.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input38:

preferences 4
~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The preference matrix computed for profiles and alternatives given by decision maker 4.



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input18:

alternatives flows 4
~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Normalised flows of given alternatives for decision maker 4.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input6:

categories profiles 4
~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Definitions of boundary profiles for decision maker 4, which should be used for classes (categories) representation.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input29:

profiles performances 5
~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The performances of profiles for decision maker 5.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input39:

preferences 5
~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The preference matrix computed for profiles and alternatives given by decision maker 5.



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input19:

alternatives flows 5
~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Normalised flows of given alternatives for decision maker 5.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input7:

categories profiles 5
~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Definitions of boundary profiles for decision maker 5, which should be used for classes (categories) representation.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input30:

profiles performances 6
~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The performances of profiles for decision maker 6.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input40:

preferences 6
~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The preference matrix computed for profiles and alternatives given by decision maker 6.



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input20:

alternatives flows 6
~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Normalised flows of given alternatives for decision maker 6.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input8:

categories profiles 6
~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Definitions of boundary profiles for decision maker 6, which should be used for classes (categories) representation.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input31:

profiles performances 7
~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The performances of profiles for decision maker 7.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input41:

preferences 7
~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The preference matrix computed for profiles and alternatives given by decision maker 7.



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input21:

alternatives flows 7
~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Normalised flows of given alternatives for decision maker 7.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input9:

categories profiles 7
~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Definitions of boundary profiles for decision maker 7, which should be used for classes (categories) representation.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input32:

profiles performances 8
~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The performances of profiles for decision maker 8.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input42:

preferences 8
~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The preference matrix computed for profiles and alternatives given by decision maker 8.



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input22:

alternatives flows 8
~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Normalised flows of given alternatives for decision maker 8.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input10:

categories profiles 8
~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Definitions of boundary profiles for decision maker 8, which should be used for classes (categories) representation.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input33:

profiles performances 9
~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The performances of profiles for decision maker 9.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input43:

preferences 9
~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The preference matrix computed for profiles and alternatives given by decision maker 9.



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input23:

alternatives flows 9
~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Normalised flows of given alternatives for decision maker 9.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input11:

categories profiles 9
~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Definitions of boundary profiles for decision maker 9, which should be used for classes (categories) representation.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input34:

profiles performances 10
~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The performances of profiles for decision maker 10.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input44:

preferences 10
~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The preference matrix computed for profiles and alternatives given by decision maker 10.



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input24:

alternatives flows 10
~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Normalised flows of given alternatives for decision maker 10.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input12:

categories profiles 10
~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Definitions of boundary profiles for decision maker 10, which should be used for classes (categories) representation.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input45:

profiles flows
~~~~~~~~~~~~~~


Description:
............

Normalised flows of given profiles counted for all decision makers together.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input13:

categories marks
~~~~~~~~~~~~~~~~


Description:
............

Marks of categories (higher mark means better category). Each category need to have unique mark from 1 to C, where C is a number of categories.



XMCDA related:
..............

- **Tag:** categoriesValues

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input46:

method parameters
~~~~~~~~~~~~~~~~~


Description:
............

A set of parameters provided to tune up the module's operation.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** comparison with profiles

  *Information if we are using central or boundary profiles.*

  - **Type:** drop-down list
  - **Possible values:**
      - central profiles (XMCDA label : central) (default)

      - boundary profiles (XMCDA label : bounding) 

- **Name:** assign to a better class

  *Information if alternative will be assigned to a better class when the assignment is not clear. Works only if cutPoint does unclear output for alternative.*

  - **Default value:** true
- **Name:** decision maker 1 weight

  *Weight of decision maker 1.*

  - **Constraint description:** A float value.

  - **Type:** float
  - **Default value:** 0.0
- **Name:** decision maker 2 weight

  *Weight of decision maker 2.*

  - **Constraint description:** A float value.

  - **Type:** float
  - **Default value:** 0.0
- **Name:** decision maker 3 weight

  *Weight of decision maker 3.*

  - **Constraint description:** A float value. Choose 0.0 when decision maker does not exist.

  - **Type:** float
  - **Default value:** 0.0
- **Name:** decision maker 4 weight

  *Weight of decision maker 4.*

  - **Constraint description:** A float value. Choose 0.0 when decision maker does not exist.

  - **Type:** float
  - **Default value:** 0.0
- **Name:** decision maker 5 weight

  *Weight of decision maker 5.*

  - **Constraint description:** A float value. Choose 0.0 when decision maker does not exist.

  - **Type:** float
  - **Default value:** 0.0
- **Name:** decision maker 6 weight

  *Weight of decision maker 6.*

  - **Constraint description:** A float value. Choose 0.0 when decision maker does not exist.

  - **Type:** float
  - **Default value:** 0.0
- **Name:** decision maker 7 weight

  *Weight of decision maker 7.*

  - **Constraint description:** A float value. Choose 0.0 when decision maker does not exist.

  - **Type:** float
  - **Default value:** 0.0
- **Name:** decision maker 8 weight

  *Weight of decision maker 8.*

  - **Constraint description:** A float value. Choose 0.0 when decision maker does not exist.

  - **Type:** float
  - **Default value:** 0.0
- **Name:** decision maker 9 weight

  *Weight of decision maker 9.*

  - **Constraint description:** A float value. Choose 0.0 when decision maker does not exist.

  - **Type:** float
  - **Default value:** 0.0
- **Name:** decision maker 10 weight

  *Weight of decision maker 10.*

  - **Constraint description:** A float value. Choose 0.0 when decision maker does not exist.

  - **Type:** float
  - **Default value:** 0.0

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    

                <methodParameters>
                    %1
                    %2
                    %3
                    %4
                    %5
                    %6
                    %7
                    %8
                    %9
                    %10
                </methodParameters>

            

----------------------------



.. _PROMETHEE-FlowSort-GDSS_assignments-PUT_outputs:

Outputs
-------


- :ref:`first_step_assignments <PROMETHEE-FlowSort-GDSS_assignments-PUT-output1>`
- :ref:`final_assignments <PROMETHEE-FlowSort-GDSS_assignments-PUT-output2>`
- :ref:`messages <PROMETHEE-FlowSort-GDSS_assignments-PUT-output3>`

.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-output1:

first step assignments
~~~~~~~~~~~~~~~~~~~~~~


Description:
............

Assignments made in a first step of FlowSortGDSS process. They shows for which alternatives decision makers were not able to make an unanimous assignment.



XMCDA related:
..............

- **Tag:** alternativesAffectations

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-output2:

final assignments
~~~~~~~~~~~~~~~~~


Description:
............

Final assignments made in a FlowSortGDSS process.



XMCDA related:
..............

- **Tag:** alternativesAffectations

----------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-output3:

messages
~~~~~~~~


Description:
............

Messages or errors generated by this module.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
