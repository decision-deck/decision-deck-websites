:orphan:



.. _PROMETHEE_preference-reinforcedPreference-PUT:

PrometheePreferenceReinforcedPreference
=======================================

:Provider: PUT
:Version: 1.0.0

Description
-----------

Computes aggregated preference indices with reinforced preference effect

This module is an extended version of 'PrometheePreference' - it brings the concept of 'reinforced_preference', which boils down to the new threshold of the same name and a new input file where the 'reinforcement factors' are defined (one for each criterion where 'reinforced_preference' threshold is present).

- **Web page:** https://github.com/Yamadads/PrometheeDiviz

- **Reference:** Bernard Roy and Roman Slowinski; Handling effects of reinforced preference and counter-veto in credibility of outranking; European Journal of Operational Research 188(1):185?190; 2008; doi:10.1016/j.ejor.2007.04.005



Inputs
------
(For outputs, see :ref:`below <PROMETHEE_preference-reinforcedPreference-PUT_outputs>`)


- :ref:`criteria <PROMETHEE_preference-reinforcedPreference-PUT-input3>`
- :ref:`alternatives <PROMETHEE_preference-reinforcedPreference-PUT-input1>`
- :ref:`performance_table <PROMETHEE_preference-reinforcedPreference-PUT-input4>`
- :ref:`profiles_performance_table <PROMETHEE_preference-reinforcedPreference-PUT-input5>` *(optional)*
- :ref:`weights <PROMETHEE_preference-reinforcedPreference-PUT-input6>`
- :ref:`generalised_criteria <PROMETHEE_preference-reinforcedPreference-PUT-input7>` *(optional)*
- :ref:`reinforcement_factors <PROMETHEE_preference-reinforcedPreference-PUT-input8>`
- :ref:`categories_profiles <PROMETHEE_preference-reinforcedPreference-PUT-input2>` *(optional)*
- :ref:`method_parameters <PROMETHEE_preference-reinforcedPreference-PUT-input9>`

.. _PROMETHEE_preference-reinforcedPreference-PUT-input3:

criteria
~~~~~~~~


Description:
............

Criteria to consider, possibly with 'preference', 'indifference' and 'reinforced preference' thresholds (see also 'reinforcement_factors' input below). Each criterion must have a preference direction specified (min or max). It is worth mentioning that this module allows to define thresholds as constants as well as linear functions.



XMCDA related:
..............

- **Tag:** criteria

----------------------------


.. _PROMETHEE_preference-reinforcedPreference-PUT-input1:

alternatives
~~~~~~~~~~~~


Description:
............

Alternatives to consider.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _PROMETHEE_preference-reinforcedPreference-PUT-input4:

performance table
~~~~~~~~~~~~~~~~~


Description:
............

The performance of alternatives.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _PROMETHEE_preference-reinforcedPreference-PUT-input5:

profiles performance table
~~~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The performance of profiles (boundary or central).



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _PROMETHEE_preference-reinforcedPreference-PUT-input6:

weights
~~~~~~~


Description:
............

Weights of criteria to consider.



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _PROMETHEE_preference-reinforcedPreference-PUT-input7:

generalised criteria
~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

ID number of predefined preference function specified for each criterion.



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _PROMETHEE_preference-reinforcedPreference-PUT-input8:

reinforcement factors
~~~~~~~~~~~~~~~~~~~~~


Description:
............

Definitions of so-called 'reinforcement factors', one per each criterion for which 'reinforcement threshold' has been defined. For more regarding these concepts see the paper from 'Reference' section.

This input is mandatory, if you don't need the concept of reinforced preference please use 'PrometheePreference'.



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _PROMETHEE_preference-reinforcedPreference-PUT-input2:

categories profiles
~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Definitions of central or boundary profiles connected with classes (categories)



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _PROMETHEE_preference-reinforcedPreference-PUT-input9:

method parameters
~~~~~~~~~~~~~~~~~


Description:
............

First parameter specifies the type of elements provided for comparison.

Choosing 'boundary_profiles' or 'central_profiles' requires providing inputs 'classes_profiles' and 'profiles_performance_table' as well (which are optional by default).

Second parameter specifies the type of function used for comparison of each criterion.
Choosing 'specified' requires providing inputs "generalised_criterion" which is optional by default.
Choosing some of numbers sets same function for all criteria.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** comparison with

  

  - **Type:** drop-down list
  - **Possible values:**
      - alternatives vs alternatives (XMCDA label : alternatives) (default)

      - alternatives vs boundary profiles (XMCDA label : boundary_profiles) 

      - alternatives vs central (characteristic) profiles (XMCDA label : central_profiles) 

- **Name:** generalised criterion

  

  - **Type:** drop-down list
  - **Possible values:**
      - Each criterion needs its own function (XMCDA label : specified) (default)

      - Usual Criterion (XMCDA label : usual) 

      - U-Shape Criterion, needs indifference threshold specified in criterion. (XMCDA label : u-shape) 

      - V-Shape Criterion, needs threshold of strict preference specified in criterion. (XMCDA label : v-shape) 

      - Level Criterion, needs both indifference and strict preference thresholds specified in criterion. (XMCDA label : level) 

      - V-Shape with Indifference Criterion, needs both indifference and strict preference thresholds specified in criterion. (XMCDA label : v-shape-in) 


XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
        
        <methodParameters>
          <parameter name="comparison_with">
            <value>
              <label>%1</label>
            </value>
          </parameter>
          <parameter name="generalised_criterion">
            <value>
              <label>%2</label>
            </value>
          </parameter>
        </methodParameters>
        
      

----------------------------



.. _PROMETHEE_preference-reinforcedPreference-PUT_outputs:

Outputs
-------


- :ref:`preferences <PROMETHEE_preference-reinforcedPreference-PUT-output1>`
- :ref:`partial_preferences <PROMETHEE_preference-reinforcedPreference-PUT-output2>`
- :ref:`messages <PROMETHEE_preference-reinforcedPreference-PUT-output3>`

.. _PROMETHEE_preference-reinforcedPreference-PUT-output1:

preferences
~~~~~~~~~~~


Description:
............

Aggregated preference matrix computed from the given data. This matrix aggregates partial preference indices from all criteria into single preference index per pair of alternatives or alternatives/profiles.



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------


.. _PROMETHEE_preference-reinforcedPreference-PUT-output2:

partial preferences
~~~~~~~~~~~~~~~~~~~


Description:
............

Preference matrix computed from the given data. This matrix contains partial preference indices for all criteria and all pairs of alternatives or alternatives/profiles.



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------


.. _PROMETHEE_preference-reinforcedPreference-PUT-output3:

messages
~~~~~~~~


Description:
............

Messages or errors generated by this module.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
