:orphan:



.. _csvToXMCDA-performanceTable-PyXMCDA:

csvToXMCDA-performanceTable
===========================

:Provider: PyXMCDA
:Version: 1.0

Description
-----------

Transforms a file containing a performance table from a comma-separated values (CSV) file to three XMCDA compliant files, containing the corresponding criteria ids, alternatives' ids and the performance table.

- **Contact:** Sébastien Bigaret (sebastien.bigaret@telecom-bretagne.eu)

- **Web page:** http://github.com/sbigaret/ws-PyXMCDA



Inputs
------
(For outputs, see :ref:`below <csvToXMCDA-performanceTable-PyXMCDA_outputs>`)


- :ref:`performanceTable.csv <csvToXMCDA-performanceTable-PyXMCDA-input0>`

.. _csvToXMCDA-performanceTable-PyXMCDA-input0:

performanceTable (csv)
~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The performance table as a CSV file.

Example:

  ,cost,risk,employment,connection
  a11,17537,28.3,34.8,2.33
  a03,16973,29,34.9,2.66



XMCDA related:
..............

- **Tag:** other

----------------------------



.. _csvToXMCDA-performanceTable-PyXMCDA_outputs:

Outputs
-------


- :ref:`criteria <csvToXMCDA-performanceTable-PyXMCDA-output1>`
- :ref:`alternatives <csvToXMCDA-performanceTable-PyXMCDA-output0>`
- :ref:`performanceTable <csvToXMCDA-performanceTable-PyXMCDA-output2>`
- :ref:`messages <csvToXMCDA-performanceTable-PyXMCDA-output3>`

.. _csvToXMCDA-performanceTable-PyXMCDA-output1:

criteria
~~~~~~~~


Description:
............

The equivalent criteria ids.



XMCDA related:
..............

- **Tag:** criteria

----------------------------


.. _csvToXMCDA-performanceTable-PyXMCDA-output0:

alternatives
~~~~~~~~~~~~


Description:
............

The equivalent alternative ids.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _csvToXMCDA-performanceTable-PyXMCDA-output2:

performanceTable
~~~~~~~~~~~~~~~~


Description:
............

The equivalent performances.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _csvToXMCDA-performanceTable-PyXMCDA-output3:

messages
~~~~~~~~


Description:
............

Status messages.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
