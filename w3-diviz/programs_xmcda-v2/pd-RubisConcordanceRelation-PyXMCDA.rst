:orphan:



.. _RubisConcordanceRelation-PyXMCDA:

RubisConcordanceRelation
========================

:Provider: PyXMCDA
:Version: 1.0

Description
-----------

This web service allows to compute a concordance relation as defined in the Rubis methodology.

- **Contact:** Thomas Veneziano (thomas.veneziano@uni.lu)

- **Reference:** R. Bisdorff, P. Meyer, M. Roubens, Rubis: a bipolar-valued outranking method for the best choice decision problem, 4OR, 6 (2), June 2008, Springer (doi:10.1007/s10288-007-0045-5).



Inputs
------
(For outputs, see :ref:`below <RubisConcordanceRelation-PyXMCDA_outputs>`)


- :ref:`criteria <RubisConcordanceRelation-PyXMCDA-criteria>`
- :ref:`alternatives <RubisConcordanceRelation-PyXMCDA-alternatives>`
- :ref:`performanceTable <RubisConcordanceRelation-PyXMCDA-performanceTable>`
- :ref:`criteriaWeights <RubisConcordanceRelation-PyXMCDA-criteriaWeights>`
- :ref:`valuationDomain <RubisConcordanceRelation-PyXMCDA-valuationDomain>` *(optional)*

.. _RubisConcordanceRelation-PyXMCDA-criteria:

criteria
~~~~~~~~


Description:
............

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.
                             Using thresholds is optional, only the constant ones with mcdaConcept equals to "indifference", "preference" or "veto" will be considered.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
                            
                    <criteria>
                        <criterion>
                            <active>[...]</active>
                            <thresholds>
                            	<threshold
                            		mcdaConcept="indifference"><!-- REQUIRED, must be indifference, preference or veto  -->
                            		<constant><real>[...]</real></constant>
                            	</threshold>
                             </thresholds>
                             [...]
                        </criterion>
                        [...]
                    </criteria>
                    
                     

----------------------------


.. _RubisConcordanceRelation-PyXMCDA-alternatives:

alternatives
~~~~~~~~~~~~


Description:
............

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
                            
                    <alternatives>
                        <alternative>
                            <active>[...]</active>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
                    
                     

----------------------------


.. _RubisConcordanceRelation-PyXMCDA-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~


Description:
............

A performance table. The evaluations should be numeric values, i.e. <real>, <integer> or <rational>.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _RubisConcordanceRelation-PyXMCDA-criteriaWeights:

criteriaWeights
~~~~~~~~~~~~~~~


Description:
............

The set of criteria weights.



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _RubisConcordanceRelation-PyXMCDA-valuationDomain:

valuationDomain
~~~~~~~~~~~~~~~


Description:
............

Indicates the minimal and the maximal values of the valuation domain for computing the concordance relation. By default the valuation domain is {0,0.5,1}. The median indetermination value is computed as the average of the minimal and the maximal values.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** min

  *Indicates the minimal value of the valuation domain.*

  - **Type:** float
- **Name:** max

  *Indicates the maximal value of the valuation domain (which should be greater than the minimal value).*

  - **Type:** float

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                            
                    <methodParameters
                             name="valuationDomain"> <!-- REQUIRED  -->
                        <parameter
                             name="min"> <!-- REQUIRED  -->
                            <value>
                                <integer>%1</integer>
                            </value>
                        </parameter>
                        <parameter
                             name="max"> <!-- REQUIRED  -->
                            <value>
                                <integer>%2</integer>
                            </value>
                        </parameter>
                    </methodParameters>
                    
                     

----------------------------



.. _RubisConcordanceRelation-PyXMCDA_outputs:

Outputs
-------


- :ref:`alternativesComparisons <RubisConcordanceRelation-PyXMCDA-alternativesComparisons>`
- :ref:`messages <RubisConcordanceRelation-PyXMCDA-messages>`

.. _RubisConcordanceRelation-PyXMCDA-alternativesComparisons:

concordanceRelation
~~~~~~~~~~~~~~~~~~~


Description:
............

The concordance relation.



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------


.. _RubisConcordanceRelation-PyXMCDA-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
