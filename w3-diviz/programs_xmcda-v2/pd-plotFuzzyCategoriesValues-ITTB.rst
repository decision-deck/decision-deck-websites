:orphan:



.. _plotFuzzyCategoriesValues-ITTB:

plotFuzzyCategoriesValues
=========================

:Provider: ITTB
:Version: 1.0

Description
-----------

This web service allows to plot fuzzy sets. There are some options to take into account: title, X-axis label, Y-axis label and the use of colors.

- **Contact:** Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)



Inputs
------
(For outputs, see :ref:`below <plotFuzzyCategoriesValues-ITTB_outputs>`)


- :ref:`fuzzyCategoriesValues <plotFuzzyCategoriesValues-ITTB-fuzzyCategoriesValues>`
- :ref:`options <plotFuzzyCategoriesValues-ITTB-options>`

.. _plotFuzzyCategoriesValues-ITTB-fuzzyCategoriesValues:

fuzzyCategoriesValues
~~~~~~~~~~~~~~~~~~~~~


Description:
............

A set of fuzzy numbers: trapezoidal or triangular. The abscissa and ordinate evaluations should be only real or integer numeric values, i.e. <real> or <integer>..



XMCDA related:
..............

- **Tag:** categoriesValues

- **Code:**

  ::

    
                            
                   <categoriesValues>
		              <categoryValue>
			             <values>
			                 <value id=[...] name=[...]>
                	           <fuzzyNumber>
                	 	             <trapezoidal>
                	 		                <point1>
	                	 		               <abscissa><real>[...]</real></abscissa>
	                	 		               <ordinate><integer>[...]</integer></ordinate>
	                	 	                </point1>
	                	 	                <point2>
	                	 		               <abscissa><real>[...]</real></abscissa>
	                	 		               <ordinate><real>[...]</real></ordinate>
	                	 	                </point2>
	                	 	                <point3>
	                	 		               <abscissa><integer>[...]</integer></abscissa>
	                	 		               <ordinate><integer>[...]</integer></ordinate>
	                	 	                </point3>
	                	 	                <point4>
		                	 	               <abscissa><integer>[...]</integer></abscissa>
		                	 	               <ordinate><real>[...]</real></ordinate>
	                	 	                </point4>
                	 	          </trapezoidal>
                	           </fuzzyNumber>
                	         </value>
			                 <value id=[...] name=[...]>
                	           <fuzzyNumber>
                	 	             <triangular>
                	 		                <point1>
	                	 		               <abscissa><real>[...]</real></abscissa>
	                	 		               <ordinate><real>[...]</real></ordinate>
	                	 	                </point1>
	                	 	                <point2>
	                	 		               <abscissa><integer>[...]</integer></abscissa>
	                	 		               <ordinate><real>[...]</real></ordinate>
	                	 	                </point2>
	                	 	                <point3>
	                	 		               <abscissa><integer>[...]</integer></abscissa>
	                	 		               <ordinate><integer>[...]</integer></ordinate>
	                	 	                </point3>
                	 	          </triangular>
                	           </fuzzyNumber>
                            </value>
                            [...]
			             </values>
                      </categoryValue>
	              </categoriesValues>
                    
                     

----------------------------


.. _plotFuzzyCategoriesValues-ITTB-options:

options
~~~~~~~


Description:
............

None



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** Chart title:

  *String for the title of the plot. The default value is an empty field.*

  - **Type:** string
- **Name:** X-axis label:

  *String for the horizontal axis-label.The default value is an empty field.*

  - **Type:** string
- **Name:** Y axis label:

  *String for the vertical axis-label.The default value is an empty field.*

  - **Type:** string
- **Name:** Use Colors?

  *The use of colors: true for colored plot.*

  - **Type:** drop-down list
  - **Possible values:**
      - Yes (XMCDA label : true) 

      - No (XMCDA label : false) (default)


XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                   
					<methodParameters>	
						  <parameter id= "chart_title" name="Chart title">
							<value>
            					<label>%1</label>
       						 </value>
						</parameter>
						<parameter id="x_axis" name="X-axis label">
							<value>
            					<label>%2</label>
       						 </value>
						</parameter >
						<parameter id="y_axis" name="Y-axis label">
							<value>
            					<label>%3</label>
       						 </value>
						</parameter>
						 <parameter id="use_color" name="Use colors">
							<value>
            					<label>%4</label>
       						 </value>
						</parameter>
					</methodParameters>
				
               

----------------------------



.. _plotFuzzyCategoriesValues-ITTB_outputs:

Outputs
-------


- :ref:`fuzzyCategoriesValuesPlot <plotFuzzyCategoriesValues-ITTB-fuzzyCategoriesValuesPlot>`
- :ref:`messages <plotFuzzyCategoriesValues-ITTB-messages>`

.. _plotFuzzyCategoriesValues-ITTB-fuzzyCategoriesValuesPlot:

fuzzyCategoriesValuesPlot
~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

A string containing the base64 representation of the png image of the generated plot.



XMCDA related:
..............

- **Tag:** categoriesValues

----------------------------


.. _plotFuzzyCategoriesValues-ITTB-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
