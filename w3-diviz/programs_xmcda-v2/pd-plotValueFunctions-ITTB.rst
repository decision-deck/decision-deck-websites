:orphan:



.. _plotValueFunctions-ITTB:

plotValueFunctions
==================

:Provider: ITTB
:Version: 1.1

Description
-----------

This web service allows to plot utility functions. Compared to the web service plotValueFunctions, some parameters are added. Colors can be used. You can specify how to display the utility functions: by line, by column or by square. A linear interpolation can be processed in order to connect the different points in the generated utility function.

- **Contact:** Sébastien Bigaret (sebastien.bigaret@telecom-bretagne.eu)



Inputs
------
(For outputs, see :ref:`below <plotValueFunctions-ITTB_outputs>`)


- :ref:`criteria <plotValueFunctions-ITTB-criteria>` *(optional)*
- :ref:`valueFunctions <plotValueFunctions-ITTB-valueFunctions>`
- :ref:`methodPlotOptions <plotValueFunctions-ITTB-methodPlotOptions>`

.. _plotValueFunctions-ITTB-criteria:

criteria
~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
                            
                    <criteria>
                        <criterion>
                            <active>[...]</active>
                            [...]
                        </criterion>
                        [...]
                    </criteria>
                    
                     

----------------------------


.. _plotValueFunctions-ITTB-valueFunctions:

valueFunctions
~~~~~~~~~~~~~~


Description:
............

Values of utilities of chosen criteria abscissa - Utility functions



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
                   
					<criteria>
						<criterion>
							<criterionID>[...]</criterionID>
							<criterionFunction>
								<points>
									<point>
										<abscissa><real>[...]</real></abscissa>
										<ordinate><real>[...]</real></ordinate>
									</point>
								</points>
							</criterionFunction>
						</criterion>
					</criteria>
				
               

----------------------------


.. _plotValueFunctions-ITTB-methodPlotOptions:

methodPlotOptions
~~~~~~~~~~~~~~~~~


Description:
............

None



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** Unique or multiple plot(s)?

  *In a unique plot, only one image is generated containing all the utility functions. Multiple plots can be obtained. The default value is true.*

  - **Type:** drop-down list
  - **Possible values:**
      - Unique (XMCDA label : true) (default)

      - Multiple (XMCDA label : false) 

- **Name:** Plots arrangement

  *In the case of a unique plot, you can specify how to display the utility functions: by line, by column or by square. The default value is by column.*

  - **Type:** drop-down list
  - **Possible values:**
      - Column (XMCDA label : column) (default)

      - Line (XMCDA label : line) 

      - Grid (XMCDA label : grid) 

- **Name:** Linear interpolation ?

  *Linear interpolation means that lines connect the points in each utility function. The default value is true.*

  - **Type:** drop-down list
  - **Possible values:**
      - Yes (XMCDA label : true) (default)

      - No (XMCDA label : false) 

- **Name:** Add vertical bars ?

  *Without linear interpolation, you can add vertical lines so that points are more visible. The default value is false.*

  - **Type:** drop-down list
  - **Possible values:**
      - Yes (XMCDA label : true) 

      - No (XMCDA label : false) (default)

- **Name:** Use Colors?

  *The use of colors: true for colored functions.*

  - **Type:** drop-down list
  - **Possible values:**
      - Yes (XMCDA label : true) 

      - No (XMCDA label : false) (default)

- **Name:** Choose color:

  *Choose between "Black", "Red", "Blue", "Green", "Yellow", "Magenta" and "Cyan".*

  - **Type:** drop-down list
  - **Possible values:**
      - Black (XMCDA label : Black) (default)

      - Red (XMCDA label : Red) 

      - Blue (XMCDA label : Blue) 

      - Green (XMCDA label : Green) 

      - Yellow (XMCDA label : Yellow) 

      - Magenta (XMCDA label : Magenta) 

      - Cyan (XMCDA label : Cyan) 


XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                   
					<methodParameters>	
						  <parameter id="unique_plot" name="Unique plot">
							<value>
            					<label>%1</label>
       						 </value>
						</parameter>
						 <parameter id="plots_display" name="Plots' display">
							<value>
            					<label>%2</label>
       						 </value>
						</parameter>
						<parameter id="linear_interpolation" name="Linear interpolation">
							<value>
            					<label>%3</label>
       						 </value>
						</parameter>
						<parameter id="vertical_lines" name="Vertical lines">
							<value>
            					<label>%4</label>
       						 </value>
						</parameter>	
						 <parameter id="use_color" name="Colors in the plots">
							<value>
            					<label>%5</label>
       						 </value>
						</parameter>
						 <parameter id="selected_color" name="Selected color">
							<value>
            					<label>%6</label>
       						 </value>
       						 </parameter>
					</methodParameters>
				
               

----------------------------



.. _plotValueFunctions-ITTB_outputs:

Outputs
-------


- :ref:`valueFunctionsPlot <plotValueFunctions-ITTB-valueFunctionsPlot>`
- :ref:`messages <plotValueFunctions-ITTB-messages>`

.. _plotValueFunctions-ITTB-valueFunctionsPlot:

valueFunctionsPlot
~~~~~~~~~~~~~~~~~~


Description:
............

A string containing the base64 representation of the png image of the generated multi-subplot.



XMCDA related:
..............

- **Tag:** criterionValue

----------------------------


.. _plotValueFunctions-ITTB-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
