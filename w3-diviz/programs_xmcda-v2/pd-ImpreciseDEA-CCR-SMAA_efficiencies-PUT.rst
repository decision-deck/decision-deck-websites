:orphan:



.. _ImpreciseDEA-CCR-SMAA_efficiencies-PUT:

ImpreciseDEA-CCR-SMAA_efficiencies
==================================

:Provider: PUT
:Version: 1.0

Description
-----------

Computes efficiency scores for the given DMUs (alternatives) using SMAA-D method and CCR Data Envelopment Analysis Model qith imprecise data. For given number of buckets and samples, returns a matrix with alternatives in each row and buckets representing efficiency intervals in each column. Single cell indicates how many samples gave efficiency scores of respective alternative in respective bucket.

- **Contact:** Anna Labijak <support@decision-deck.org>



Inputs
------
(For outputs, see :ref:`below <ImpreciseDEA-CCR-SMAA_efficiencies-PUT_outputs>`)


- :ref:`units <ImpreciseDEA-CCR-SMAA_efficiencies-PUT-units>`
- :ref:`inputsOutputs <ImpreciseDEA-CCR-SMAA_efficiencies-PUT-inputsOutputs>`
- :ref:`performanceTable <ImpreciseDEA-CCR-SMAA_efficiencies-PUT-performanceTable>`
- :ref:`maxPerformanceTable <ImpreciseDEA-CCR-SMAA_efficiencies-PUT-maxPerformanceTable>` *(optional)*
- :ref:`weightsLinearConstraints <ImpreciseDEA-CCR-SMAA_efficiencies-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <ImpreciseDEA-CCR-SMAA_efficiencies-PUT-methodParameters>`

.. _ImpreciseDEA-CCR-SMAA_efficiencies-PUT-units:

units
~~~~~


Description:
............

A list of alternatives (DMUs).



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>

----------------------------


.. _ImpreciseDEA-CCR-SMAA_efficiencies-PUT-inputsOutputs:

inputs/outputs
~~~~~~~~~~~~~~


Description:
............

A list of criteria with specified preference direction. List has to contains at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output).



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    <criteria>
                        <criterion>
							<scale>
								<quantitative>
									<preferenceDirection>
										[...]
									</preferenceDirection>
								</quantitative>
							</scale>
                            [...]
                        </criterion>
                        [...]
                    </criteria>

----------------------------


.. _ImpreciseDEA-CCR-SMAA_efficiencies-PUT-performanceTable:

performance
~~~~~~~~~~~


Description:
............

A list of alternatives (DMUs) performances.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>

----------------------------


.. _ImpreciseDEA-CCR-SMAA_efficiencies-PUT-maxPerformanceTable:

max performance
~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A list of maximal alternatives (DMUs) performances.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>

----------------------------


.. _ImpreciseDEA-CCR-SMAA_efficiencies-PUT-weightsLinearConstraints:

weights constraints
~~~~~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

A list of criteria linear constraints.



XMCDA related:
..............

- **Tag:** criteriaLinearConstraints

- **Code:**

  ::

    <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID> [...] </criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>

----------------------------


.. _ImpreciseDEA-CCR-SMAA_efficiencies-PUT-methodParameters:

method parameters
~~~~~~~~~~~~~~~~~


Description:
............

"samplesNb" represents the number of samples to generate; "intervalsNo" represents the number of buckets which efficiency scores will be assigned to.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** number of samples

  

  - **Constraint description:** The value should be a positive integer.

  - **Type:** integer
  - **Default value:** 100
- **Name:** number of buckets

  

  - **Constraint description:** The value should be a positive integer.

  - **Type:** integer
  - **Default value:** 10
- **Name:** tolerance

  

  - **Constraint description:** The value should be a non-negative number.

  - **Type:** float
  - **Default value:** 0.0

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
    <methodParameters>
        <parameter id="samplesNb">
            <value><integer>%1</integer></value>
        </parameter>
        <parameter id="intervalsNb">
            <value><integer>%2</integer></value>
        </parameter>
        <parameter id="tolerance">
            <value><real>%3</real></value>
        </parameter>
    </methodParameters>

----------------------------



.. _ImpreciseDEA-CCR-SMAA_efficiencies-PUT_outputs:

Outputs
-------


- :ref:`efficiencyDistribution <ImpreciseDEA-CCR-SMAA_efficiencies-PUT-efficiencyDistribution>`
- :ref:`maxEfficiency <ImpreciseDEA-CCR-SMAA_efficiencies-PUT-maxEfficiency>`
- :ref:`minEfficiency <ImpreciseDEA-CCR-SMAA_efficiencies-PUT-minEfficiency>`
- :ref:`avgEfficiency <ImpreciseDEA-CCR-SMAA_efficiencies-PUT-avgEfficiency>`
- :ref:`messages <ImpreciseDEA-CCR-SMAA_efficiencies-PUT-messages>`

.. _ImpreciseDEA-CCR-SMAA_efficiencies-PUT-efficiencyDistribution:

efficiency distribution
~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

A performance table for given alternatives. Single performance consists of attribute criterionID representing certain bucket, and a value representing the ratio of efficiency scores in this bucket.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID> Bucket [...]</criterionID>
									<value>
									[...]
									</value>
							</performance>
							[...]
						</alternativePerformances>
					</performanceTable>

----------------------------


.. _ImpreciseDEA-CCR-SMAA_efficiencies-PUT-maxEfficiency:

max efficiency
~~~~~~~~~~~~~~


Description:
............

A list of alternatives with maximum efficiency scores (obtained for given sample).



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
         <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
						  <values>
                <value>
							  [...]
						    </value>
              </value>
						</alternativeValue>
						[...]
					</alternativesValues>

----------------------------


.. _ImpreciseDEA-CCR-SMAA_efficiencies-PUT-minEfficiency:

min efficiency
~~~~~~~~~~~~~~


Description:
............

A list of alternatives with computed minimum efficiency scores (obtained for given sample).



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
         <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
						  <values>
                <value>
							  [...]
						    <value>
              </values>
						</alternativeValue>
						[...]
					</alternativesValues>

----------------------------


.. _ImpreciseDEA-CCR-SMAA_efficiencies-PUT-avgEfficiency:

average efficiency
~~~~~~~~~~~~~~~~~~


Description:
............

A list of alternatives with average efficiency scores (obtained for given sample).



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
         <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
						  <values>
                <value>
							  [...]
                </value>
						  </values>
						</alternativeValue>
						[...]
					</alternativesValues>

----------------------------


.. _ImpreciseDEA-CCR-SMAA_efficiencies-PUT-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
