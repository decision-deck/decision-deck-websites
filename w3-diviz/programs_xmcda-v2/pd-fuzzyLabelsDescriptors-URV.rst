:orphan:



.. _fuzzyLabelsDescriptors-URV:

defuzzification
===============

:Provider: URV
:Version: 1.0

Description
-----------

Two types of uncertainty in fuzzy sets are recognized: (1) specificity, related to the measurement of imprecision, which is based on the cardinality of the set, and (2) fuzziness, or entropy, which measures the vagueness of the set as a result of having imprecise boundaries. Specificity and fuzziness refer to two different characteristics of fuzzy sets. Specificity (or its counterpart, non-specificity) measures the degree of truth of the sentence: Containing just one element. Fuzziness measures the difference from a crisp set.

- **Contact:** Aida Valls <aida.valls@urv.cat>

- **Reference:** R.R. Yager, On Ordered Weighted Averaging Aggregation, IEEE Transactions on Systems, Man and Cybernetics, 18, pp. 119-145 (1988)



Inputs
------
(For outputs, see :ref:`below <fuzzyLabelsDescriptors-URV_outputs>`)


- :ref:`fuzzyNumbers <fuzzyLabelsDescriptors-URV-input0>`

.. _fuzzyLabelsDescriptors-URV-input0:

fuzzy numbers
~~~~~~~~~~~~~


Description:
............

Definition of a fuzzy variable as a list of labels (trapezoidal)



XMCDA related:
..............

- **Tag:** categoriesValues

- **Code:**

  ::

    
                    
			<categoriesValue>
			<categoryValue>
			   <values>
				<value id="xxx" name="xxxxxxxx">
						<fuzzyNumber>
							<trapezoidal>
								<point1>
									<abscissa>[...]</abscissa>
									<ordinate>[...]</ordinate>
								</point1>
								[...]
								<point4>
									[...]
								</point4>
							</trapezoidal>
						</fuzzyNumber>
				</value>
			    	[...]
        	            </values>
			</categoryValue>
			</categoriesValue>
                    
                     

----------------------------



.. _fuzzyLabelsDescriptors-URV_outputs:

Outputs
-------


- :ref:`SpecificityResults <fuzzyLabelsDescriptors-URV-output0>`
- :ref:`FuzzinessResults <fuzzyLabelsDescriptors-URV-output1>`
- :ref:`messages <fuzzyLabelsDescriptors-URV-output2>`

.. _fuzzyLabelsDescriptors-URV-output0:

specificity
~~~~~~~~~~~


Description:
............

Specificity value for each label of the fuzzy variable.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _fuzzyLabelsDescriptors-URV-output1:

fuzziness
~~~~~~~~~


Description:
............

Fuzziness value for each label of the fuzzy variable.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _fuzzyLabelsDescriptors-URV-output2:

messages
~~~~~~~~


Description:
............

A status message.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
