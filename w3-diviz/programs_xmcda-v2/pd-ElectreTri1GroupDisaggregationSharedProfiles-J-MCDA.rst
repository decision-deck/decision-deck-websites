:orphan:



.. _ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA:

ElectreTri1GroupDisaggregationSharedProfiles
============================================

:Provider: J-MCDA
:Version: 0.5.5

Description
-----------

Finds electre tri like models with shared profiles and individual weights that matches given group assignments.

- **Contact:** Olivier Cailloux <olivier.cailloux@ecp.fr>

- **Web page:** http://sourceforge.net/projects/j-mcda/

- **Reference:** Cailloux, O., Meyer, P. & Mousseau, V., 2012. Eliciting ELECTRE TRI category limits for a group of decision makers. European Journal of Operational Research. Available at: http://www.sciencedirect.com/science/article/pii/S0377221712003906?v=s5.



Inputs
------
(For outputs, see :ref:`below <ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA_outputs>`)


- :ref:`criteria <ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-criteria>`
- :ref:`alternatives <ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-alternatives>` *(optional)*
- :ref:`performances_alternatives <ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-performances_alternatives>`
- :ref:`set_profiles <ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-set_profiles>` *(optional)*
- :ref:`examples <ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-examples>`
- :ref:`categories_profiles <ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-categories_profiles>`
- :ref:`use_vetoes <ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-use_vetoes>` *(optional)*
- :ref:`min_weights <ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-min_weights>` *(optional)*

.. _ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-criteria:

criteria
~~~~~~~~


Description:
............

The criteria to consider. Each one must have a preference direction. No preference or indifference thresholds may be included. Set some criteria as inactive (or remove them) to ignore them. Veto thresholds may be included: these will be considered as constraints on the preference model.



XMCDA related:
..............

- **Tag:** criteria

----------------------------


.. _ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-alternatives:

alternatives
~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The alternatives to consider. Set some alternatives as inactive (or remove them) to ignore them.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-performances_alternatives:

alternatives performances
~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The performances of the example alternatives.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-set_profiles:

set profiles
~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Some performances for the profiles of the resulting model may be constrained here. The values will not necessarily be considered as is but the resulting model will satisfy the ordering implied by the constraints.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-examples:

group example assignments
~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The example assignments. Use one tag per decision maker. Use the name attribute to indicate the corresponding decision maker.



XMCDA related:
..............

- **Tag:** alternativesAffectations

----------------------------


.. _ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-categories_profiles:

categories_profiles
~~~~~~~~~~~~~~~~~~~


Description:
............

The profiles and the categories names in which alternatives are to be sorted.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-use_vetoes:

use vetoes
~~~~~~~~~~


Description:
............

Whether to accept using veto thresholds in the resulting preference model. Must be true when veto threshold values are set as input.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** use vetoes

  


XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    

      <methodParameters>
         <parameter>
               <value>
                  <boolean>%1</boolean>
               </value>
         </parameter>
      </methodParameters>

			

----------------------------


.. _ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-min_weights:

minimum weights
~~~~~~~~~~~~~~~


Description:
............

The minimum value accepted for a weight. Default is zero.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** minimum weights

  

  - **Type:** float

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    

      <methodParameters>
         <parameter>
               <value>
                  <real>%1</real>
               </value>
         </parameter>
      </methodParameters>

			

----------------------------



.. _ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA_outputs:

Outputs
-------


- :ref:`veto_thresholds <ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-veto_thresholds>`
- :ref:`coalitions <ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-coalitions>`
- :ref:`performances_profiles <ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-performances_profiles>`
- :ref:`messages <ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-messages>`
- :ref:`result_status <ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-result_status>`

.. _ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-veto_thresholds:

veto thresholds
~~~~~~~~~~~~~~~


Description:
............

The resulting veto thresholds.



XMCDA related:
..............

- **Tag:** criteria

----------------------------


.. _ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-coalitions:

group coalitions
~~~~~~~~~~~~~~~~


Description:
............

The resulting group coalitions.



XMCDA related:
..............

- **Tag:** criteriaSets

----------------------------


.. _ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-performances_profiles:

profiles performances
~~~~~~~~~~~~~~~~~~~~~


Description:
............

The performances of the shared profiles.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-messages:

messages
~~~~~~~~


Description:
............

A status message to indicate if everything went correctly or if an error happened during the execution. Note that if the service finds that the problem is infeasible, this is not considered as an error.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------


.. _ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-result_status:

result status
~~~~~~~~~~~~~


Description:
............

The result status: if feasible, then one solution was found, otherwise the status indicates what happened (e.g. the program is infeasible).



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
