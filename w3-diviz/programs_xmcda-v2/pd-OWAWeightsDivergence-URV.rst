:orphan:



.. _OWAWeightsDivergence-URV:

OWAWeightsDivergence
====================

:Provider: URV
:Version: 1.0

Description
-----------

Divergence of the weights given to the Ordered Weighted Average operator (OWA)

 The Ordered Weighted Averaging operators, commonly called OWA operators, provide a parameterized class of mean type aggregation operators.
 OWA was defined by R.R. Yager in 1988.
 The main characteristic of this aggregation operator is the fact that the weights are associated to the values instead to criteria.
 In this way, we can define different aggregation policies according to the importance that we associate to high and low performance scores.
 There exist different measures to characterise a set of weights associated to an OWA operator.
 In this module we implement the DIVERGENCE measure.

- **Contact:** Aida Valls <aida.valls@urv.cat>

- **Reference:** 
		R.R. Yager, On Ordered Weighted Averaging Aggregation, IEEE Transactions on Systems, Man and Cybernetics, 18, pp. 119-145 (1988)
		



Inputs
------
(For outputs, see :ref:`below <OWAWeightsDivergence-URV_outputs>`)


- :ref:`criteria <OWAWeightsDivergence-URV-input1>`
- :ref:`weights <OWAWeightsDivergence-URV-input2>`

.. _OWAWeightsDivergence-URV-input1:

criteria
~~~~~~~~


Description:
............

A list of criteria. Criteria are always active.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
                            
                    <criteria>
                        <criterion>
                            <active>[...]</active>
                            [...]
                        </criterion>
                        [...]
                    </criteria>
                    
                     

----------------------------


.. _OWAWeightsDivergence-URV-input2:

weights
~~~~~~~


Description:
............

The weights are associated to the values of the alteranatives. Assuming that the values
on the alternative will be sorted from the best to the worst (e.g. 7, 5, 5, 3, 1), the list of weights must be ordered
according to the importance that is given to the values, from the highest to the lowest.
For example a list of weights as (0.5, 0.5, 0, 0, 0) is ignoring the 3 lowest values, and making an average of the
two highest ones. A list like (0, 0, 1, 0 ,0 ) is calculating the median, while (0, 0, 0, 0, 1) is taking the minimum.
Notice that the sum of weights is required to be 1.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
		  
			<alternativesValues>
			  <alternativeValue>
				<values>
					<value>
						<real>[...]</real>
					</value>
					<value>
						<integer>[...]</integer>
					</value>
					[...]
			  </alternativeValue>
			</alternativesValues>
		  
		  

----------------------------



.. _OWAWeightsDivergence-URV_outputs:

Outputs
-------


- :ref:`owaMeasures <OWAWeightsDivergence-URV-output0>`
- :ref:`messages <OWAWeightsDivergence-URV-output1>`

.. _OWAWeightsDivergence-URV-output0:

owaMeasures
~~~~~~~~~~~


Description:
............

Result obtained from the analysis of the OWA weights.



XMCDA related:
..............

- **Tag:** criterionValue

----------------------------


.. _OWAWeightsDivergence-URV-output1:

messages
~~~~~~~~


Description:
............

A status message.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
