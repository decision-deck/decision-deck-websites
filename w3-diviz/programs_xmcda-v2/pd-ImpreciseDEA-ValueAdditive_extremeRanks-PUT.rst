:orphan:



.. _ImpreciseDEA-ValueAdditive_extremeRanks-PUT:

ImpreciseDEA-ValueAdditive_extremeRanks
=======================================

:Provider: PUT
:Version: 1.0

Description
-----------

Computes efficiency scores for the given DMUs (alternatives)  using Additive Data Envelopment Analysis Model with imprecise information.

- **Contact:** Anna Labijak <support@decision-deck.org>

- **Reference:** G. Jahanshahloo, F. H. Lotfi, M. R. Malkhalifeh, and M. A. Namin. A generalized model for data envelopment analysis with interval data. 2009.

- **Reference:** MC. Gouveia et al. Super-efficiency and stability intervals in additive DEA. 2013



Inputs
------
(For outputs, see :ref:`below <ImpreciseDEA-ValueAdditive_extremeRanks-PUT_outputs>`)


- :ref:`units <ImpreciseDEA-ValueAdditive_extremeRanks-PUT-units>`
- :ref:`inputsOutputs <ImpreciseDEA-ValueAdditive_extremeRanks-PUT-inputsOutputs>`
- :ref:`performanceTable <ImpreciseDEA-ValueAdditive_extremeRanks-PUT-performanceTable>`
- :ref:`maxPerformanceTable <ImpreciseDEA-ValueAdditive_extremeRanks-PUT-maxPerformanceTable>` *(optional)*
- :ref:`weightsLinearConstraints <ImpreciseDEA-ValueAdditive_extremeRanks-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <ImpreciseDEA-ValueAdditive_extremeRanks-PUT-methodParameters>`

.. _ImpreciseDEA-ValueAdditive_extremeRanks-PUT-units:

units
~~~~~


Description:
............

A list of alternatives (DMUs).



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
        <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
      

----------------------------


.. _ImpreciseDEA-ValueAdditive_extremeRanks-PUT-inputsOutputs:

inputs/outputs
~~~~~~~~~~~~~~


Description:
............

A list of criteria with specified preference direction. List has to contains at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output).



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
        <criteria>
                        <criterion>
							<scale>
                                [...]
							</scale>
                            [...]
                        </criterion>
                        [...]
                    </criteria>
      

----------------------------


.. _ImpreciseDEA-ValueAdditive_extremeRanks-PUT-performanceTable:

performance
~~~~~~~~~~~


Description:
............

A list of alternatives (DMUs) minimal performances (or exact performances if intervals should be created by tolerance).



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    
        <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>
      

----------------------------


.. _ImpreciseDEA-ValueAdditive_extremeRanks-PUT-maxPerformanceTable:

max performance
~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A list of alternatives (DMUs) maximal performances.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    
        <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>
      

----------------------------


.. _ImpreciseDEA-ValueAdditive_extremeRanks-PUT-weightsLinearConstraints:

weights constraints
~~~~~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

A list of criteria linear constraints.



XMCDA related:
..............

- **Tag:** criteriaLinearConstraints

- **Code:**

  ::

    
        <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID> [...] </criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>
      

----------------------------


.. _ImpreciseDEA-ValueAdditive_extremeRanks-PUT-methodParameters:

method parameters
~~~~~~~~~~~~~~~~~


Description:
............

Represents method parameters.
          "tolerance" represents the fraction for creating interval data (created interval: [data*(1-tolerance), data*(1+tolerance)]),
          "transformToUtilities" means if data should be tranformed into values from range [0-1],
          "boundariesProvided" means if inputsOutputs file contains information about min and max data for each factor,
          "functionShapeProvided" means if inputsOutputs file contains information about the shapes of value function for given factor.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** tolerance

  

  - **Constraint description:** The value should be non-negative.

  - **Type:** float
  - **Default value:** 0.00
- **Name:** transform to utilities

  

  - **Default value:** true
- **Name:** boundaries provided

  

  - **Default value:** false
- **Name:** function shapes provided

  

  - **Default value:** false

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
    <methodParameters>
        <parameter id="tolerance">
            <value><real>%1</real></value>
        </parameter>
        <parameter id="transformToUtilities">
            <value><boolean>%2</boolean></value>
        </parameter>
        <parameter id="boundariesProvided">
            <value><boolean>%3</boolean></value>
        </parameter>
        <parameter id="functionShapeProvided">
            <value><boolean>%4</boolean></value>
        </parameter>
    </methodParameters>


----------------------------



.. _ImpreciseDEA-ValueAdditive_extremeRanks-PUT_outputs:

Outputs
-------


- :ref:`bestRank <ImpreciseDEA-ValueAdditive_extremeRanks-PUT-bestRank>`
- :ref:`worstRank <ImpreciseDEA-ValueAdditive_extremeRanks-PUT-worstRank>`
- :ref:`messages <ImpreciseDEA-ValueAdditive_extremeRanks-PUT-messages>`

.. _ImpreciseDEA-ValueAdditive_extremeRanks-PUT-bestRank:

best rank
~~~~~~~~~


Description:
............

A list of alternatives with computed best rank for each of them.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
        <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
						  <values>
                <value>
							  [...]
						    </value>
              </values>
						</alternativeValue>
						[...]
					</alternativesValues>
      

----------------------------


.. _ImpreciseDEA-ValueAdditive_extremeRanks-PUT-worstRank:

worst rank
~~~~~~~~~~


Description:
............

A list of alternatives with computed worst rank for each of them.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
        <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
						  <values>
							  <value>
                [...]
                <value>
						  </values>
						</alternativeValue>
						[...]
					</alternativesValues>
      

----------------------------


.. _ImpreciseDEA-ValueAdditive_extremeRanks-PUT-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
