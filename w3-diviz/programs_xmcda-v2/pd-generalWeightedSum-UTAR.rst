:orphan:


.. _generalWeightedSum-UTAR:

generalWeightedSum
==================

:Provider: UTAR
:Version: 1.0

Description
-----------

 -**Contact:** Boris Leistedt (boris.leistedt@gmail.com)

  Computes the (possibly normalised) weighted sum of alternatives' evaluations, their average value or simply their sum. The aggregation operator is chosen in the parameter window. 

Inputs
------


alternatives
~~~~~~~~~~~~


Description: 
............

  
					A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active. 
				

XMCDA related:
..............

 -**Tag:** alternatives

 -**Code:**

  ::

   
				
					<alternatives>
						<alternative>
							<alternativeID> %1 </alternativeID>
						</alternative>
					</alternatives>
				
			

----------------------------


criteria
~~~~~~~~


Description: 
............

  
					A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.
				

XMCDA related:
..............

 -**Tag:** criteria

 -**Code:**

  ::

   
				
					<criteria>
						<criterion>
							<criterionID> %1 </criterionID>
						</criterion>
					</criteria>
				
			

----------------------------


performanceTable
~~~~~~~~~~~~~~~~


Description: 
............

  
					A performance table. The evaluations should be numeric values, i.e. <real>, <integer> or <rational>. It must contains IDs of both criteria and alternatives described previously.
				

XMCDA related:
..............

 -**Tag:** performanceTable

 -**Code:**

  ::

   
				
					<performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>[...]</criterionID>
								<value>
            								<real>[...]</real>
       						 		</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>
				
			

----------------------------


weights
~~~~~~~

 -**Optional:** True

Description: 
............

  
					Containing the optional weights for criteria sum.
				

XMCDA related:
..............

 -**Tag:** criteriaValues

 -**Code:**

  ::

   
				
					<criteriaValues>
						<criterionValue>
							<criterionID>[...]</criterionID>
							<value>
								<real>[...]</real>
							</value>
						</criterionValue>
						[...]
					</criteriaValues>
				
			

----------------------------



Outputs
-------


alternativesValues
~~~~~~~~~~~~~~~~~~


Description: 
............

  
					Values (or utility) for different alternatives
				

XMCDA related:
..............

 -**Tag:** alternativesValues

 -**Code:**

  ::

   
				
					<alternativesValues> 
				    	<alternativeValue>
	                       	<alternativeID>[...]</alternativeID>
                        	<value>
	                          	<real>[...]</real>
                        	</value>
                    	</alternativeValue>
                    </alternativesValues>
                
			

----------------------------


messages
~~~~~~~~


Description: 
............

  
					logMessage
				

XMCDA related:
..............

 -**Tag:** methodMessages

 -**Code:**

  ::

   
				
					<methodMessages mcdaConcept="methodMessage">
						<logMessage>
							<text> [...]</text>
						</logMessage>
						<errorMessage>
							<text> [...]</text>
						</errorMessage>
					</methodMessages>
				
			

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_. 


