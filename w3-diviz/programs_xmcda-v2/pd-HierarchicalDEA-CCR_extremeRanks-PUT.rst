:orphan:



.. _HierarchicalDEA-CCR_extremeRanks-PUT:

HierarchicalDEA-CCR_extremeRanks
================================

:Provider: PUT
:Version: 1.0

Description
-----------

Computes the extreme efficiency ranks for the given DMUs (alternatives) using CCR Data Envelopment Analysis Model with hierarchical structure of outputs.

- **Contact:** 
            Anna Labijak <anna.labijak@cs.put.poznan.pl>
        



Inputs
------
(For outputs, see :ref:`below <HierarchicalDEA-CCR_extremeRanks-PUT_outputs>`)


- :ref:`units <HierarchicalDEA-CCR_extremeRanks-PUT-units>`
- :ref:`performanceTable <HierarchicalDEA-CCR_extremeRanks-PUT-performanceTable>`
- :ref:`hierarchy <HierarchicalDEA-CCR_extremeRanks-PUT-hierarchy>`
- :ref:`weightsLinearConstraints <HierarchicalDEA-CCR_extremeRanks-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <HierarchicalDEA-CCR_extremeRanks-PUT-methodParameters>`

.. _HierarchicalDEA-CCR_extremeRanks-PUT-units:

units
~~~~~


Description:
............

A list of alternatives (DMUs).



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
                <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
            

----------------------------


.. _HierarchicalDEA-CCR_extremeRanks-PUT-performanceTable:

performance
~~~~~~~~~~~


Description:
............

A list of alternatives (DMUs) performances (on leaf hierarchy criteria).



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    
                <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>
            

----------------------------


.. _HierarchicalDEA-CCR_extremeRanks-PUT-hierarchy:

hierarchy
~~~~~~~~~


Description:
............

The hierarchical structure of criteria.



XMCDA related:
..............

- **Tag:** hierarchy

- **Code:**

  ::

    
                
                <hierarchy>
                    <node>
                        <criterionID>[...]</criterionID>
                        <node>
                            <criterionID>[...]</criterionID>
                            <node>
                                [...]
                            </node>
                            [...]
                        </node>
                        [...]
                    </node>
                </hierarchy>
            

----------------------------


.. _HierarchicalDEA-CCR_extremeRanks-PUT-weightsLinearConstraints:

weights constraints
~~~~~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

A list of hierarchy criteria linear constraints.



XMCDA related:
..............

- **Tag:** criteriaLinearConstraints

- **Code:**

  ::

    
                <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID> [...] </criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>
            

----------------------------


.. _HierarchicalDEA-CCR_extremeRanks-PUT-methodParameters:

parameters
~~~~~~~~~~


Description:
............

Represents parameters (hierarchyNode).



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** hierarchyNode

  *ID of the hierarchy criterion for which the analysis should be performed.*

  - **Type:** string
  - **Default value:** "root"

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                
    <methodParameters>
        <parameter id="hierarchyNode">
            <value><label>%1</label></value>
        </parameter>
    </methodParameters>
            

----------------------------



.. _HierarchicalDEA-CCR_extremeRanks-PUT_outputs:

Outputs
-------


- :ref:`bestRank <HierarchicalDEA-CCR_extremeRanks-PUT-bestRank>`
- :ref:`worstRank <HierarchicalDEA-CCR_extremeRanks-PUT-worstRank>`
- :ref:`messages <HierarchicalDEA-CCR_extremeRanks-PUT-messages>`

.. _HierarchicalDEA-CCR_extremeRanks-PUT-bestRank:

best rank
~~~~~~~~~


Description:
............

A list of alternatives with computed best rank for each of them.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
                
         <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
						  <values>
                <value>
							  [...]
						    </value>
              </values>
						</alternativeValue>
						[...]
					</alternativesValues>
            

----------------------------


.. _HierarchicalDEA-CCR_extremeRanks-PUT-worstRank:

worst rank
~~~~~~~~~~


Description:
............

A list of alternatives with computed worst rank for each of them.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
                
         <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
						    <values>
                  <value>
							    [...]
						      </value>
                </values>
						</alternativeValue>
						[...]
					</alternativesValues>
            

----------------------------


.. _HierarchicalDEA-CCR_extremeRanks-PUT-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
