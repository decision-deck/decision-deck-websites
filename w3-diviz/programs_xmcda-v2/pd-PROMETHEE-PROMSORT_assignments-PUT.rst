:orphan:



.. _PROMETHEE-PROMSORT_assignments-PUT:

PROMETHEE-PROMSORT_assignments
==============================

:Provider: PUT
:Version: 1.0.0

Description
-----------

Computes class assignment for given alternatives using PromSort method. This method consists of two separated steps. In the first step alternatives are being assigned to categories basing on their relations with boundary profiles. This relations are computed using positive and negative flows. After first step alternative can be assigned to one or two categories. In the second step final assignment is calculated for each alternative assigned to two categories as a distance function between this alternative and alternatives assigned in first step to exactly one category. For other alternatives assignment from first step is final.

- **Contact:** Maciej Uniejewski <maciej.uniejewski@gmail.com>

- **Web page:** https://github.com/maciej7777/PrometheeDiviz



Inputs
------
(For outputs, see :ref:`below <PROMETHEE-PROMSORT_assignments-PUT_outputs>`)


- :ref:`criteria <PROMETHEE-PROMSORT_assignments-PUT-input6>`
- :ref:`alternatives <PROMETHEE-PROMSORT_assignments-PUT-input1>`
- :ref:`categories <PROMETHEE-PROMSORT_assignments-PUT-input2>`
- :ref:`performance_table <PROMETHEE-PROMSORT_assignments-PUT-input5>`
- :ref:`positive_flows <PROMETHEE-PROMSORT_assignments-PUT-input7>`
- :ref:`negative_flows <PROMETHEE-PROMSORT_assignments-PUT-input8>`
- :ref:`categories_profiles <PROMETHEE-PROMSORT_assignments-PUT-input3>`
- :ref:`categories_values <PROMETHEE-PROMSORT_assignments-PUT-input4>`
- :ref:`method_parameters <PROMETHEE-PROMSORT_assignments-PUT-input9>`

.. _PROMETHEE-PROMSORT_assignments-PUT-input6:

criteria
~~~~~~~~


Description:
............

Definitions of criteria, their scales and preference thresholds.



XMCDA related:
..............

- **Tag:** criteria

----------------------------


.. _PROMETHEE-PROMSORT_assignments-PUT-input1:

alternatives
~~~~~~~~~~~~


Description:
............

Alternatives to consider.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _PROMETHEE-PROMSORT_assignments-PUT-input2:

categories
~~~~~~~~~~


Description:
............

Definitions of categories.



XMCDA related:
..............

- **Tag:** categories

----------------------------


.. _PROMETHEE-PROMSORT_assignments-PUT-input5:

profiles performances
~~~~~~~~~~~~~~~~~~~~~


Description:
............

The performances of boundary profiles.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _PROMETHEE-PROMSORT_assignments-PUT-input7:

positive flows
~~~~~~~~~~~~~~


Description:
............

Positive flows of given alternatives and boundary profiles.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _PROMETHEE-PROMSORT_assignments-PUT-input8:

negative flows
~~~~~~~~~~~~~~


Description:
............

Negative flows of given alternatives and boundary profiles.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _PROMETHEE-PROMSORT_assignments-PUT-input3:

categories profiles
~~~~~~~~~~~~~~~~~~~


Description:
............

Definitions of boundary profiles which should be used for classes (categories) representation.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _PROMETHEE-PROMSORT_assignments-PUT-input4:

categories marks
~~~~~~~~~~~~~~~~


Description:
............

Marks of categories (higher mark means better category). Each category need to have unique mark from 1 to C, where C is a number of categories.



XMCDA related:
..............

- **Tag:** categoriesValues

----------------------------


.. _PROMETHEE-PROMSORT_assignments-PUT-input9:

method parameters
~~~~~~~~~~~~~~~~~


Description:
............

A set of parameters provided to tune up the module's operation.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** cut point

  *Parameter used in a second phase of PromSort method. Determines the impact of assigning alternatives to better/worse categories.*

  - **Constraint description:** The value should be between -1.0 and 1.0 (both included).

  - **Type:** float
- **Name:** assign to a better class

  *Information if alternative will be assigned to a better class when the assignment is not clear. Works only if cutPoint does unclear output for alternative.*

  - **Default value:** true

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                
                <methodParameters>
                    <parameter id="cutPoint">
                        <value>
                            <real>%1</real>
                        </value>
                    </parameter>
                    <parameter id="assignToABetterClass">
                        <value>
                            <boolean>%2</boolean>
                        </value>
                    </parameter>
                </methodParameters>
	        
            

----------------------------



.. _PROMETHEE-PROMSORT_assignments-PUT_outputs:

Outputs
-------


- :ref:`first_step_assignments <PROMETHEE-PROMSORT_assignments-PUT-output1>`
- :ref:`final_assignments <PROMETHEE-PROMSORT_assignments-PUT-output2>`
- :ref:`messages <PROMETHEE-PROMSORT_assignments-PUT-output3>`

.. _PROMETHEE-PROMSORT_assignments-PUT-output1:

first step assignments
~~~~~~~~~~~~~~~~~~~~~~


Description:
............

Assignments made in a first step of PromSort method. The assignment is imprecise, as some of alternatives can need next steps of PromSort method to return the final (precise) assignment.



XMCDA related:
..............

- **Tag:** alternativesAffectations

----------------------------


.. _PROMETHEE-PROMSORT_assignments-PUT-output2:

final assignments
~~~~~~~~~~~~~~~~~


Description:
............

Final assignments made in a PromSort method.



XMCDA related:
..............

- **Tag:** alternativesAffectations

----------------------------


.. _PROMETHEE-PROMSORT_assignments-PUT-output3:

messages
~~~~~~~~


Description:
............

Messages or errors generated by this module.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
