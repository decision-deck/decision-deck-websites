:orphan:



.. _ElectreCredibilityWithCounterVeto-PUT:

ElectreCredibilityWithCounterVeto
=================================

:Provider: PUT
:Version: 0.1.0

Description
-----------

ElectreCredibilityWithCounterVeto - computes credibility matrix using procedure which is common to the most methods from the Electre family.

This module is an extended version of 'ElectreCredibility' in that it is designed to work with the 'counter-veto' concept - i.e. it requires an additional input file ('counter_veto_crossed.xml') produced by 'ElectreDiscordance' module, which contains the information for which pairs and on which criteria the 'counter-veto' threshold has been crossed.

Please note that unlike 'ElectreCredibility', this module can accept discordance indices only in non-aggregated form (i.e. one index per criterion).

- **Web page:** http://github.com/xor-xor/electre_diviz

- **Reference:** Bernard Roy and Roman Słowiński; Handling effects of reinforced preference and counter-veto in credibility of outranking; European Journal of Operational Research 188(1):185–190; 2008; doi:10.1016/j.ejor.2007.04.005



Inputs
------
(For outputs, see :ref:`below <ElectreCredibilityWithCounterVeto-PUT_outputs>`)


- :ref:`alternatives <ElectreCredibilityWithCounterVeto-PUT-input1>`
- :ref:`concordance <ElectreCredibilityWithCounterVeto-PUT-input3>`
- :ref:`counter_veto_crossed <ElectreCredibilityWithCounterVeto-PUT-input4>`
- :ref:`discordance <ElectreCredibilityWithCounterVeto-PUT-input5>`
- :ref:`classes_profiles <ElectreCredibilityWithCounterVeto-PUT-input2>` *(optional)*
- :ref:`method_parameters <ElectreCredibilityWithCounterVeto-PUT-input6>`

.. _ElectreCredibilityWithCounterVeto-PUT-input1:

alternatives
~~~~~~~~~~~~


Description:
............

Alternatives to consider.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _ElectreCredibilityWithCounterVeto-PUT-input3:

concordance
~~~~~~~~~~~


Description:
............

Concordance matrix with aggregated concordance indices (i.e. one index per pair of alternatives or alternatives/profiles).



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------


.. _ElectreCredibilityWithCounterVeto-PUT-input4:

counter_veto_crossed
~~~~~~~~~~~~~~~~~~~~


Description:
............

This input contains the information (boolean values) for which pairs of variants and on which criteria the 'counter-veto' threshold has been crossed.



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------


.. _ElectreCredibilityWithCounterVeto-PUT-input5:

discordance
~~~~~~~~~~~


Description:
............

Discordance indices provided in non-aggregated ('partial' or 'per-criterion') form.



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------


.. _ElectreCredibilityWithCounterVeto-PUT-input2:

classes_profiles
~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Definitions of profiles (boundary or central) which should be used for classes (categories) representation.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _ElectreCredibilityWithCounterVeto-PUT-input6:

method_parameters
~~~~~~~~~~~~~~~~~


Description:
............

A set of parameters provided to tune up the module's operation.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** comparison_with

  *This parameter specifies the type of elements provided for comparison.

Choosing 'boundary_profiles' or 'central_profiles' requires providing inputs 'classes_profiles' and 'profiles_performance_table' as well (which are optional by default).*

  - **Type:** drop-down list
  - **Possible values:**
      - alternatives vs alternatives (XMCDA label : alternatives) (default)

      - alternatives vs boundary profiles (XMCDA label : boundary_profiles) 

      - alternatives vs central (characteristic) profiles (XMCDA label : central_profiles) 

- **Name:** with_denominator

  *This parameter specifies if you want to take into account only sufficiently great discordance (by using '1 - d_j(a, b) / 1 - C(a, b)' instead of '1 - d_j(a, b)' in credibility's equation).

In case of selecting this option, only discordance indices for which d_j(a, b) > C(a, b) holds are taken into account.*

  - **Default value:** true
- **Name:** only_max_discordance

  *Use only maximum discordance index in discordance aggregation part of credibility's equation (i.e. '1 - max(d_j(a, b))'). When 'with_denominator' is set to 'true', this parameter is ignored.*

  - **Default value:** false

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
        
        <methodParameters>
          <parameter name="comparison_with">
            <value>
              <label>%1</label>
            </value>
          </parameter>
          <parameter name="with_denominator">
            <value>
              <boolean>%2</boolean>
            </value>
          </parameter>
          <parameter name="only_max_discordance">
            <value>
              <boolean>%3</boolean>
            </value>
          </parameter>
        </methodParameters>
        
      

----------------------------



.. _ElectreCredibilityWithCounterVeto-PUT_outputs:

Outputs
-------


- :ref:`credibility <ElectreCredibilityWithCounterVeto-PUT-output1>`
- :ref:`messages <ElectreCredibilityWithCounterVeto-PUT-output2>`

.. _ElectreCredibilityWithCounterVeto-PUT-output1:

credibility
~~~~~~~~~~~


Description:
............

Credibility matrix computed from the given data.



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------


.. _ElectreCredibilityWithCounterVeto-PUT-output2:

messages
~~~~~~~~


Description:
............

Messages or errors generated by this module.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
