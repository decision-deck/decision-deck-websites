:orphan:



.. _RubisOutrankingRelation-PyXMCDA:

RubisOutrankingRelation
=======================

:Provider: PyXMCDA
:Version: 1.1

Description
-----------

This web service allows to compute an outranking relation as defined in the Rubis methodology.

- **Contact:** Thomas Veneziano (thomas.veneziano@uni.lu)

- **Reference:** R. Bisdorff, P. Meyer, M. Roubens, Rubis: a bipolar-valued outranking method for the best choice decision problem, 4OR, 6 (2), June 2008, Springer (doi:10.1007/s10288-007-0045-5).



Inputs
------
(For outputs, see :ref:`below <RubisOutrankingRelation-PyXMCDA_outputs>`)


- :ref:`criteria <RubisOutrankingRelation-PyXMCDA-criteria>`
- :ref:`alternatives <RubisOutrankingRelation-PyXMCDA-alternatives>`
- :ref:`performanceTable <RubisOutrankingRelation-PyXMCDA-performanceTable>`
- :ref:`criteriaWeights <RubisOutrankingRelation-PyXMCDA-criteriaWeights>`
- :ref:`valuationDomain <RubisOutrankingRelation-PyXMCDA-valuationDomain>` *(optional)*

.. _RubisOutrankingRelation-PyXMCDA-criteria:

criteria
~~~~~~~~


Description:
............

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.
                             Using thresholds is optional, only the constant ones with mcdaConcept equals to "indifference", "preference" or "veto" will be considered.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
                            
                    <criteria>
                        <criterion>
                            <active>[...]</active>
                            <thresholds>
                            	<threshold
                            		mcdaConcept="indifference"><!-- REQUIRED, must be indifference, preference or veto  -->
                            		<constant><real>[...]</real></constant>
                            	</threshold>
                             </thresholds>
                             [...]
                        </criterion>
                        [...]
                    </criteria>
                    
                     

----------------------------


.. _RubisOutrankingRelation-PyXMCDA-alternatives:

alternatives
~~~~~~~~~~~~


Description:
............

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
                            
                    <alternatives>
                        <alternative>
                            <active>[...]</active>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
                    
                     

----------------------------


.. _RubisOutrankingRelation-PyXMCDA-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~


Description:
............

A performance table. The evaluations should be numeric values, i.e. <real>, <integer> or <rational>.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _RubisOutrankingRelation-PyXMCDA-criteriaWeights:

criteriaWeights
~~~~~~~~~~~~~~~


Description:
............

The set of criteria weights.



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _RubisOutrankingRelation-PyXMCDA-valuationDomain:

valuationDomain
~~~~~~~~~~~~~~~


Description:
............

Indicates the minimal and the maximal values of the valuation domain for computing the outranking relation. By default the valuation domain is {0,0.5,1}. The median indetermination value is computed as the average of the minimal and the maximal values.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** min

  *Indicates the minimal value of the valuation domain.*

  - **Type:** float
  - **Default value:** 0
- **Name:** max

  *Indicates the maximal value of the valuation domain (which should be greater than the minimal value).*

  - **Type:** float
  - **Default value:** 1

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                            
                    <methodParameters
                             name="valuationDomain"> <!-- REQUIRED  -->
                        <parameter
                             name="min"> <!-- REQUIRED  -->
                            <value>
                                <integer>%1</integer>
                            </value>
                        </parameter>
                        <parameter
                             name="max"> <!-- REQUIRED  -->
                            <value>
                                <integer>%2</integer>
                            </value>
                        </parameter>
                    </methodParameters>
                    
                     

----------------------------



.. _RubisOutrankingRelation-PyXMCDA_outputs:

Outputs
-------


- :ref:`alternativesComparisons <RubisOutrankingRelation-PyXMCDA-alternativesComparisons>`
- :ref:`messages <RubisOutrankingRelation-PyXMCDA-messages>`

.. _RubisOutrankingRelation-PyXMCDA-alternativesComparisons:

outrankingRelation
~~~~~~~~~~~~~~~~~~


Description:
............

The ogniknartu relation.



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------


.. _RubisOutrankingRelation-PyXMCDA-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
