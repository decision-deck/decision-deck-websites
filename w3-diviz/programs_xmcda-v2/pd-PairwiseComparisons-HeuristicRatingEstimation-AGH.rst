:orphan:



.. _PairwiseComparisons-HeuristicRatingEstimation-AGH:

PairwiseComparisons-HeuristicRatingEstimation
=============================================

:Provider: AGH
:Version: 1.0

Description
-----------

Computes HRE (Heuristic rating estimation) method - approach proposes a new way of using pairwise comparisons matrix.
Source code: https://github.com/djurasze/HRE-algo.
More info about algorithm: https://link.springer.com/article/10.1007/s10898-014-0253-4 and https://arxiv.org/pdf/1309.0386.pdf
For XMCDA input data version preferable is 3.0.0 but algorithm also support version 2.2.2

- **Contact:** Dominik Juraszek (djurasze@gmail.com)



Inputs
------
(For outputs, see :ref:`below <PairwiseComparisons-HeuristicRatingEstimation-AGH_outputs>`)


- :ref:`alternatives <PairwiseComparisons-HeuristicRatingEstimation-AGH-alternatives>` *(optional)*
- :ref:`knownRankings <PairwiseComparisons-HeuristicRatingEstimation-AGH-alternativesValues>` *(optional)*
- :ref:`alternativesMatrix <PairwiseComparisons-HeuristicRatingEstimation-AGH-alternativesMatrix>`
- :ref:`parameters <PairwiseComparisons-HeuristicRatingEstimation-AGH-parameters>`

.. _PairwiseComparisons-HeuristicRatingEstimation-AGH-alternatives:

alternatives
~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Alternatives that will be ranked. By default (no active tag), alternatives are considered as active. If absent, the program behaves as if it was supplied with all alternatives being active



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
                
    <alternatives>
        <alternative id="..."></alternative>
        [...]
    </alternatives>

            

----------------------------


.. _PairwiseComparisons-HeuristicRatingEstimation-AGH-alternativesValues:

known rankings
~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Initially known rankings for alternatives.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
                
    <alternativesValues>
        <alternativeValue>
            <alternativeID> [...] </alternativeID>
            <values>
                <value><real> [...] </real></value>
            </values>
        </alternativeValue>
        [...]
    </alternativesValues>

            

----------------------------


.. _PairwiseComparisons-HeuristicRatingEstimation-AGH-alternativesMatrix:

pairwise comparisons matrix
~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

Partial assessments pairs over the set of alternatives.



XMCDA related:
..............

- **Tag:** alternativesMatrix

- **Code:**

  ::

    
                
    <alternativesMatrix>
        <row>
            <alternativeID>[...]</alternativeID>
            <column>
                <alternativeID>[...]</alternativeID>
                <values>
                    <value><real>[...]</real></value>
                </values>
            </column>
            [...]
        </row>
        [...]
    </alternativesMatrix>

            

----------------------------


.. _PairwiseComparisons-HeuristicRatingEstimation-AGH-parameters:

parameters
~~~~~~~~~~


Description:
............

A set of method parameters to tune the algorithm:



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** Try reciprocity restoration procedure for pairwise comparisons matrix?

  *Turns on Heuristics of reciprocity restoration. This is a boolean. The default value is false.*

  - **Default value:** false
- **Name:** Normalize results?

  *Turns on results normalization. This is a boolean. The default value is true.*

  - **Default value:** true
- **Name:** Calculation method

  *This parameter specifies the type of calculation method.*

  - **Type:** drop-down list
  - **Possible values:**
      - Arithmetic (XMCDA label : Arithmetic) (default)

      - Geometric (XMCDA label : Geometric) 

      - Minimizing estimation error with arithmetic approach (XMCDA label : MinimizingEstimationError) 

- **Name:** Try minimal absolute estimation error?

  *Tries to find solution with smallest error. This method should be used with 'Arithmetic' calculation method and with specified MaxIterations. In practice, it should be small enough (even two or three iterations may be useful). This is a boolean. The default value is false.*

  - **Default value:** false
- **Name:** Maximum iterations

  *Defines number of maximum iterations for algorithm. This is a integer. The default value is 100.*

  - **Constraint description:** Iterations should be positive

  - **Type:** integer
  - **Default value:** 100

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                

    <methodParameters>
        <parameter id="TryToReciprocalMatrix">
            <value>
                <boolean>%1</boolean>
            </value>
        </parameter>

        <parameter id="NormalizeResults">
            <value>
                <boolean>%2</boolean>
            </value>
        </parameter>

        <parameter id="CalculationMethod">
            <value>
                <label>%3</label>
            </value>
        </parameter>

        <parameter id="WithSmallestError">
            <value>
                <boolean>%4</boolean>
            </value>
        </parameter>

         <parameter id="MaxIterations">
            <value>
                <integer>%5</integer>
            </value>
        </parameter>
    </methodParameters>


            

----------------------------



.. _PairwiseComparisons-HeuristicRatingEstimation-AGH_outputs:

Outputs
-------


- :ref:`alternativesValues <PairwiseComparisons-HeuristicRatingEstimation-AGH-alternativesValues_out>`
- :ref:`messages <PairwiseComparisons-HeuristicRatingEstimation-AGH-logMessage>`

.. _PairwiseComparisons-HeuristicRatingEstimation-AGH-alternativesValues_out:

computed rankings
~~~~~~~~~~~~~~~~~


Description:
............

Calculated rankings for all alternatives.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
                
    <alternativesValues>
        <alternativeValue>
            <alternativeID> [...] </alternativeID>
            <values>
                <value><real> [...] </real></value>
            </values>
        </alternativeValue>
        [...]
    </alternativesValues>

            

----------------------------


.. _PairwiseComparisons-HeuristicRatingEstimation-AGH-logMessage:

messages
~~~~~~~~


Description:
............

Log message about program status. This can be 'ok' when algorithm finish successfully or 'error' when something goes wrong.



XMCDA related:
..............

- **Tag:** methodMessages

- **Code:**

  ::

    
                
    <methodMessages>
        <message>
            <text> [...] </text>
        </message>
    </methodMessages>

            

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
