:orphan:



.. _UTA-ITTB:

UTA
===

:Provider: ITTB
:Version: 1.0

Description
-----------

Computes UTA method and if necessary uses post-optimality analysis.

- **Contact:** Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)



Inputs
------
(For outputs, see :ref:`below <UTA-ITTB_outputs>`)


- :ref:`criteria <UTA-ITTB-criteria>`
- :ref:`alternatives <UTA-ITTB-alternatives>`
- :ref:`performanceTable <UTA-ITTB-performanceTable>`
- :ref:`criteriaSegments <UTA-ITTB-criteriaSegments>`
- :ref:`alternativesRanks <UTA-ITTB-alternativesRanks>`
- :ref:`methodParameters <UTA-ITTB-methodParameters>` *(optional)*

.. _UTA-ITTB-criteria:

criteria
~~~~~~~~


Description:
............

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active. Optimization direction for the selected criteria is provided (min or max).



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
				
					<criteria>
						<criterion>
							<criterionID>[...]</criterionID>
							<scale>
								<quantitative>
									<preferenceDirection>[...]</preferenceDirection>
								</quantitative>
							</scale>
						</criterion>
					</criteria>
				
			

----------------------------


.. _UTA-ITTB-alternatives:

alternatives
~~~~~~~~~~~~


Description:
............

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
				
					<alternatives>
						<alternative>
							<alternativeID>[...]</alternativeID>
						</alternative>
					</alternatives>
				
			

----------------------------


.. _UTA-ITTB-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~


Description:
............

Values of criteria for different alternatives. It must contains IDs of both criteria and alternatives described previously.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    
				
					<performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>[...]</criterionID>
								<value>
            							<real>[...]</real>
       						 	</value>
							</performance>
						</alternativePerformances>
					</performanceTable>
				
			

----------------------------


.. _UTA-ITTB-criteriaSegments:

criteriaSegments
~~~~~~~~~~~~~~~~


Description:
............

Number of segments in each value function to be constructed by UTA.



XMCDA related:
..............

- **Tag:** criteriaValues

- **Code:**

  ::

    
				
					<criteriaValues>
						<criterionValue>
							<criterionID>[...]</criterionID>
							<value>
            							<integer>[...]</integer>
       						 	</value>
						</criterionValue>
					</criteriaValues>
				
			

----------------------------


.. _UTA-ITTB-alternativesRanks:

alternativesRanks
~~~~~~~~~~~~~~~~~


Description:
............

Ranking (preorder) of alternatives, corresponding to pariwize preference and indifference statements



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
				
					<alternativesValues>
						<alternativeValue>
							<alternativeID>[...]</alternativeID>
							<value>
            							<integer>[...]</integer>
       						 	</value>
						</alternativeValue>
					</alternativesValues>
				
			

----------------------------


.. _UTA-ITTB-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~


Description:
............

- **Reference:** E. Jacquet-Lagreze; J. Siskos. Assessing a set of additive utility functions for multicriteria decision-making: the UTA method.European Journal of Operational Research (June 1982), 10 (2), pg. 151-164.

Post-optimality method : choose between "Yes" or "No".



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** Post-optimality analysis:

  *The implementation or not of post optimality analysis.*

  - **Type:** drop-down list
  - **Possible values:**
      - Yes (XMCDA label : true) 

      - No (XMCDA label : false) (default)


XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
				
					<methodParameters>
					<parameter id="post_optimality" name="Post optimality analysis">
							<value>
            					<boolean>%1</boolean>
       						 </value>
						</parameter>
					</methodParameters>
				
			

----------------------------



.. _UTA-ITTB_outputs:

Outputs
-------


- :ref:`valueFunctions <UTA-ITTB-valueFunctions>`
- :ref:`messages <UTA-ITTB-messages>`

.. _UTA-ITTB-valueFunctions:

valueFunctions
~~~~~~~~~~~~~~


Description:
............

Constructed value functions for the selected criteria.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
				
					<criteria mcdaConcept="criteria">
						<criterion>
							<criterionID>[...]</criterionID>
							<criterionFunction>
								<points>
									<point>
										<abscissa><real>[...]</real></abscissa>
										<ordinate><real>[...]</real></ordinate>
									</point>
								</points>
							</criterionFunction>
						</criterion>
					</criteria>
				
			

----------------------------


.. _UTA-ITTB-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
