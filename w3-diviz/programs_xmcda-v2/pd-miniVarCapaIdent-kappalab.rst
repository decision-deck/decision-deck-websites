:orphan:



.. _miniVarCapaIdent-kappalab:

miniVarCapaIdent
================

:Provider: kappalab
:Version: 1.0

Description
-----------

Identifies a Mobius capacity by means of an approach using a maximum like quadratic entropy principle, which is equivalent to the minimization of the variance. More precisely, this function determines, if it exists, the minimum variance capacity compatible with a set of linear constraints. The problem is solved using strictly convex quadratic programming.

- **Contact:** Patrick Meyer (patrick.meyer@telecom-bretagne.eu)

- **Reference:** I. Kojadinovic (2005), Minimum variance capacity identification, European Journal of Operational Research, in press. 



Inputs
------
(For outputs, see :ref:`below <miniVarCapaIdent-kappalab_outputs>`)


- :ref:`criteria <miniVarCapaIdent-kappalab-criteria>`
- :ref:`alternatives <miniVarCapaIdent-kappalab-alternatives>`
- :ref:`performanceTable <miniVarCapaIdent-kappalab-performanceTable>`
- :ref:`shapleyPreorder <miniVarCapaIdent-kappalab-shapleyPreorder>` *(optional)*
- :ref:`interactionPreorder <miniVarCapaIdent-kappalab-interactionPreorder>` *(optional)*
- :ref:`shapleyInterval <miniVarCapaIdent-kappalab-shapleyInterval>` *(optional)*
- :ref:`interactionInterval <miniVarCapaIdent-kappalab-interactionInterval>` *(optional)*
- :ref:`alternativesPreorder <miniVarCapaIdent-kappalab-alternativesPreorder>`
- :ref:`kAdditivity <miniVarCapaIdent-kappalab-kAdditivity>`

.. _miniVarCapaIdent-kappalab-criteria:

criteria
~~~~~~~~


Description:
............

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
                
                    <criteria>
                        <criterion>
                            <active>[...]</active>
                            [...]
                        </criterion>
                        [...]
                    </criteria>
                    
            

----------------------------


.. _miniVarCapaIdent-kappalab-alternatives:

alternatives
~~~~~~~~~~~~


Description:
............

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
                
                    <alternatives>
                        <alternative>
                            <active>[...]</active>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
                    
            

----------------------------


.. _miniVarCapaIdent-kappalab-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~


Description:
............

A performance table. The evaluations should be numeric values, i.e. <real>, <integer> or <rational>.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _miniVarCapaIdent-kappalab-shapleyPreorder:

shapleyPreorder
~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A valued relation on criteria expressing importance constraints on the critera. A numeric <value> indicates a minimal preference threshold for each <pair>. One <pair> represents an affirmation of the type "the Shapley importance index of criterion g1 is greater than the Shapley importance index of criterion g2 with preference threshold delta".



XMCDA related:
..............

- **Tag:** criteriaComparisons

----------------------------


.. _miniVarCapaIdent-kappalab-interactionPreorder:

interactionPreorder
~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A valued relation on pairs of criteria expressing constraints on value of the the Shapley interaction index. A numeric <value> indicates a minimal preference threshold for each <pair> of the relation. One <pair> represents a constraint of the type "the Shapley interaction index of the pair (g1,g2) of criteria is greater than the Shapley interaction index of the pair (g3,g4) of criteria with preference threshold delta".



XMCDA related:
..............

- **Tag:** criteriaComparisons

----------------------------


.. _miniVarCapaIdent-kappalab-shapleyInterval:

shapleyInterval
~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A list of <criterionValue> containing the constraints relative to the quantitative importance of the criteria. Each <criterionValue> contains an an <interval>. Each <criteriaValue> represents an affirmation of the type "the Shapley importance index of criterion g1 lies in the interval [a,b]".



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _miniVarCapaIdent-kappalab-interactionInterval:

interactionInterval
~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A list of <criterionValue> containing the constraints relative to the type and the magnitude of the Shapley interaction index for pairs of criteria. Each <criterionValue> contains an an <interval>. Each <criteriaValue> represents an affirmation of the type "the Shapley interaction index of the pair (g1,g2) of criteria lies in the interval [a,b]".



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _miniVarCapaIdent-kappalab-alternativesPreorder:

alternativesPreorder
~~~~~~~~~~~~~~~~~~~~


Description:
............

A valued relation relative to the preorder of the alternatives. A numeric <value> indicates a minimal preference threshold for each <pair> of the relation. One <pair> represents a constraint of the type "alternative a is preferred to alternative b with preference threshold delta".



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------


.. _miniVarCapaIdent-kappalab-kAdditivity:

kAdditivity
~~~~~~~~~~~


Description:
............

Indicates the level of k-additivity of the Mobius capacity (the Mobius transform of subsets whose cardinal is superior to k vanishes).



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via the XMCDA file.

- **Name:** kAdditivity

  *Indicates the level of k-additivity of the Mobius capacity (the Mobius transform of subsets whose cardinal is superior to k vanishes).*

  - **Constraint description:** The value should be a positive integer, less than or equal to the number of criteria.

  - **Type:** integer
  - **Default value:** 1

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                
                    <methodParameters>
                        <parameter
                             name="kAdditivity"> <!-- REQUIRED  -->
                            <value>
                                <integer>%1</integer>
                            </value>
                        </parameter>
                    </methodParameters>
                    
            

----------------------------



.. _miniVarCapaIdent-kappalab_outputs:

Outputs
-------


- :ref:`mobiusCapacity <miniVarCapaIdent-kappalab-mobiusCapacity>`
- :ref:`messages <miniVarCapaIdent-kappalab-messages>`

.. _miniVarCapaIdent-kappalab-mobiusCapacity:

mobiusCapacity
~~~~~~~~~~~~~~


Description:
............

The Mobius transform of a capacity.



XMCDA related:
..............

- **Tag:** criteriaValues

- **Code:**

  ::

    
                
                    <criteriaValues mcdaConcept="mobiusCapacity">
                        <criterionValue>
                            <criteriaSet>
                                <element>
                                    <criterionID>[...]</criterionID>
                                </element>
                                [...]
                            </criteriaSet>
                            <value>
                                <real>[...]</real>
                            </value>
                        </criterionValue>
                        [...]
                    </criteriaValues>
                    
            

----------------------------


.. _miniVarCapaIdent-kappalab-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
