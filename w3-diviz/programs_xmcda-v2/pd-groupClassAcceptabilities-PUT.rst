:orphan:



.. _groupClassAcceptabilities-PUT:

groupClassAcceptabilities
=========================

:Provider: PUT
:Version: 1.0.0

Description
-----------

Module for calculation group class acceptabilities basing on the assignments to classes of each decision maker.

- **Contact:** Magdalena Dziecielska <magdalenadziecielska6@gmail.com>

- **Web page:** https://github.com/MagdalenaDziecielska/PrometheeDiviz

- **Reference:** 
      S. Damart, L.C. Dias and V. Mousseau; Supporting groups in sorting
      decisions: Methodology and use of a multi-criteria aggregation/disaggregation DSS.;
      Decision Support Systems 43 (2007) 1464?1475
    



Inputs
------
(For outputs, see :ref:`below <groupClassAcceptabilities-PUT_outputs>`)


- :ref:`alternatives <groupClassAcceptabilities-PUT-input1>`
- :ref:`assignments_1 <groupClassAcceptabilities-PUT-input2>`
- :ref:`assignments_2 <groupClassAcceptabilities-PUT-input3>`
- :ref:`assignments_3 <groupClassAcceptabilities-PUT-input4>` *(optional)*
- :ref:`assignments_4 <groupClassAcceptabilities-PUT-input5>` *(optional)*
- :ref:`assignments_5 <groupClassAcceptabilities-PUT-input6>` *(optional)*
- :ref:`assignments_6 <groupClassAcceptabilities-PUT-input7>` *(optional)*
- :ref:`assignments_7 <groupClassAcceptabilities-PUT-input8>` *(optional)*
- :ref:`assignments_8 <groupClassAcceptabilities-PUT-input9>` *(optional)*
- :ref:`assignments_9 <groupClassAcceptabilities-PUT-input10>` *(optional)*
- :ref:`assignments_10 <groupClassAcceptabilities-PUT-input11>` *(optional)*
- :ref:`categories <groupClassAcceptabilities-PUT-input12>` *(optional)*
- :ref:`categories_values <groupClassAcceptabilities-PUT-input13>`

.. _groupClassAcceptabilities-PUT-input1:

alternatives
~~~~~~~~~~~~


Description:
............

Alternatives to consider.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _groupClassAcceptabilities-PUT-input2:

assignments 1
~~~~~~~~~~~~~


Description:
............

Assignments of each alternative to the categories interval as an upper and lower bound for decision maker 1.



XMCDA related:
..............

- **Tag:** alternativesAffectations

----------------------------


.. _groupClassAcceptabilities-PUT-input3:

assignments 2
~~~~~~~~~~~~~


Description:
............

Assignments of each alternative to the categories interval as an upper and lower bound for decision maker 2.



XMCDA related:
..............

- **Tag:** alternativesAffectations

----------------------------


.. _groupClassAcceptabilities-PUT-input4:

assignments 3
~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Assignments of each alternative to the categories interval as an upper and lower bound for decision maker 3.



XMCDA related:
..............

- **Tag:** alternativesAffectations

----------------------------


.. _groupClassAcceptabilities-PUT-input5:

assignments 4
~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Assignments of each alternative to the categories interval as an upper and lower bound for decision maker 4.



XMCDA related:
..............

- **Tag:** alternativesAffectations

----------------------------


.. _groupClassAcceptabilities-PUT-input6:

assignments 5
~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Assignments of each alternative to the categories interval as an upper and lower bound for decision maker 5.



XMCDA related:
..............

- **Tag:** alternativesAffectations

----------------------------


.. _groupClassAcceptabilities-PUT-input7:

assignments 6
~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Assignments of each alternative to the categories interval as an upper and lower bound for decision maker 6.



XMCDA related:
..............

- **Tag:** alternativesAffectations

----------------------------


.. _groupClassAcceptabilities-PUT-input8:

assignments 7
~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Assignments of each alternative to the categories interval as an upper and lower bound for decision maker 7.



XMCDA related:
..............

- **Tag:** alternativesAffectations

----------------------------


.. _groupClassAcceptabilities-PUT-input9:

assignments 8
~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Assignments of each alternative to the categories interval as an upper and lower bound for decision maker 8.



XMCDA related:
..............

- **Tag:** alternativesAffectations

----------------------------


.. _groupClassAcceptabilities-PUT-input10:

assignments 9
~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Assignments of each alternative to the categories interval as an upper and lower bound for decision maker 9.



XMCDA related:
..............

- **Tag:** alternativesAffectations

----------------------------


.. _groupClassAcceptabilities-PUT-input11:

assignments 10
~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Assignments of each alternative to the categories interval as an upper and lower bound for decision maker 10.



XMCDA related:
..............

- **Tag:** alternativesAffectations

----------------------------


.. _groupClassAcceptabilities-PUT-input12:

categories
~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Definitions of categories.



XMCDA related:
..............

- **Tag:** categoriesValues

----------------------------


.. _groupClassAcceptabilities-PUT-input13:

categories values
~~~~~~~~~~~~~~~~~


Description:
............

Definitions of categories and their marks (higher mark means better category).



XMCDA related:
..............

- **Tag:** categoriesValues

----------------------------



.. _groupClassAcceptabilities-PUT_outputs:

Outputs
-------


- :ref:`alternatives_support <groupClassAcceptabilities-PUT-output1>`
- :ref:`unimodal_alternatives_support <groupClassAcceptabilities-PUT-output2>`
- :ref:`messages <groupClassAcceptabilities-PUT-output3>`

.. _groupClassAcceptabilities-PUT-output1:

alternatives support
~~~~~~~~~~~~~~~~~~~~


Description:
............

Alternatives support represented as percentage value



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _groupClassAcceptabilities-PUT-output2:

unimodal alternatives support
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

Unimodal alternatives support represented as percentage value



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _groupClassAcceptabilities-PUT-output3:

messages
~~~~~~~~


Description:
............

Messages or errors generated by this module.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
