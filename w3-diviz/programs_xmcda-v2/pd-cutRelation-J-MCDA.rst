:orphan:



.. _cutRelation-J-MCDA:

cutRelation
===========

:Provider: J-MCDA
:Version: 0.5.5

Description
-----------

Cuts a fuzzy relation (on alternatives) at a given threshold and produces a binary relation.

- **Contact:** Olivier Cailloux <olivier.cailloux@ecp.fr>

- **Web page:** http://sourceforge.net/projects/j-mcda/

- **Reference:** Cailloux, Olivier. Electre and Promethee MCDA methods as reusable software components. In Proceedings of the 25th Mini-EURO Conference on Uncertainty and Robustness in Planning and Decision Making (URPDM 2010). Coimbra, Portugal, 2010.



Inputs
------
(For outputs, see :ref:`below <cutRelation-J-MCDA_outputs>`)


- :ref:`relation <cutRelation-J-MCDA-input0>`
- :ref:`alternatives <cutRelation-J-MCDA-input1>` *(optional)*
- :ref:`cut_threshold <cutRelation-J-MCDA-input2>`

.. _cutRelation-J-MCDA-input0:

relation
~~~~~~~~


Description:
............

The fuzzy relation to cut.



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------


.. _cutRelation-J-MCDA-input1:

alternatives
~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The alternatives to consider. Set some alternatives as inactive (or remove them) to ignore them.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _cutRelation-J-MCDA-input2:

cut_threshold
~~~~~~~~~~~~~


Description:
............

The threshold specifying where to cut the relation.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** cut_threshold

  

  - **Type:** float

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    

      <methodParameters>
         <parameter>
               <value>
                  <real>%1</real>
               </value>
         </parameter>
      </methodParameters>

			

----------------------------



.. _cutRelation-J-MCDA_outputs:

Outputs
-------


- :ref:`binary_relation <cutRelation-J-MCDA-output0>`
- :ref:`messages <cutRelation-J-MCDA-output1>`

.. _cutRelation-J-MCDA-output0:

binary_relation
~~~~~~~~~~~~~~~


Description:
............

The binary relation resulting from the cut.



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------


.. _cutRelation-J-MCDA-output1:

messages
~~~~~~~~


Description:
............

A status message.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
