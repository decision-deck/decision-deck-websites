:orphan:



.. _UTASTAR-UTAR:

UTASTAR
=======

:Provider: UTAR
:Version: 1.1

Description
-----------

Computes UTASTAR method and if necessary uses post-optimality analysis among three well-known post-optimality methods

- **Contact:** Boris Leistedt (boris.leistedt@gmail.com)



Inputs
------
(For outputs, see :ref:`below <UTASTAR-UTAR_outputs>`)


- :ref:`criteria <UTASTAR-UTAR-crit>`
- :ref:`alternatives <UTASTAR-UTAR-alternatives>`
- :ref:`performanceTable <UTASTAR-UTAR-perfTable>`
- :ref:`preferencesDirections <UTASTAR-UTAR-prefDir>`
- :ref:`segments <UTASTAR-UTAR-critSeg>`
- :ref:`alternativesRanking <UTASTAR-UTAR-altRank>` *(optional)*
- :ref:`alternativesPreferences <UTASTAR-UTAR-alternativesPreferences>` *(optional)*
- :ref:`alternativesIndifferences <UTASTAR-UTAR-alternativesIndifferences>` *(optional)*
- :ref:`method <UTASTAR-UTAR-method>` *(optional)*

.. _UTASTAR-UTAR-crit:

criteria
~~~~~~~~


Description:
............

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
				
					<criteria>
						<criterion>
							<criterionID>[...]</criterionID>
						</criterion>
					</criteria>
				
			

----------------------------


.. _UTASTAR-UTAR-alternatives:

alternatives
~~~~~~~~~~~~


Description:
............

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
				
					<alternatives>
						<alternative>
							<alternativeID>[...]</alternativeID>
						</alternative>
					</alternatives>
				
			

----------------------------


.. _UTASTAR-UTAR-perfTable:

performanceTable
~~~~~~~~~~~~~~~~


Description:
............

Values of criteria for different alternatives. It must contains IDs of both criteria and alternatives described previously.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    
				
					<performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>[...]</criterionID>
								<value>
            							<real>[...]</real>
       						 	</value>
							</performance>
						</alternativePerformances>
					</performanceTable>
				
			

----------------------------


.. _UTASTAR-UTAR-prefDir:

preferenceDirections
~~~~~~~~~~~~~~~~~~~~


Description:
............

Optimization direction for the selected criteria (min or max).



XMCDA related:
..............

- **Tag:** criteriaValues

- **Code:**

  ::

    
				
					<criteriaValues>
						<criterionValue>
							<criterionID>[...]</criterionID>
							<value>
            							<label>[...]</label>
       						 	</value>
						</criterionValue>
					</criteriaValues>
				
			

----------------------------


.. _UTASTAR-UTAR-critSeg:

criteriaSegments
~~~~~~~~~~~~~~~~


Description:
............

Number of segments in each value function to be constructed by UTA.



XMCDA related:
..............

- **Tag:** criteriaValues

- **Code:**

  ::

    
				
					<criteriaValues>
						<criterionValue>
							<criterionID>[...]</criterionID>
							<value>
            							<integer>[...]</integer>
       						 	</value>
						</criterionValue>
					</criteriaValues>
				
			

----------------------------


.. _UTASTAR-UTAR-altRank:

alternativesRank
~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Ranking (preorder) of alternatives, corresponding to pariwize preference and indifference statements



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
				
					<alternativesValues>
						<alternativeValue>
							<alternativeID>[...]</alternativeID>
							<value>
            							<integer>[...]</integer>
       						 	</value>
						</alternativeValue>
					</alternativesValues>
				
			

----------------------------


.. _UTASTAR-UTAR-alternativesPreferences:

alternativesPreferences
~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Asymmetric part of the preference relation, representing strict preference statements, under the form of paiwise comparisons of alternatives.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
				
				<alternativesComparisons>
					<pairs>
						<pair>
							<initial>
								<alternativeID>[...]</alternativeID>
							</initial>
							<terminal>
								<alternativeID>[...]</alternativeID>
							</terminal>
						</pair>
						[...]
					</pairs>
				</alternativesComparisons>
				                			
			

----------------------------


.. _UTASTAR-UTAR-alternativesIndifferences:

alternativesIndifferences
~~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Symmetric part of the preference relation, representing indifference statements, under the form of paiwise comparisons of alternatives.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
				
				<alternativesComparisons>
					<pairs>
						<pair>
							<initial>
								<alternativeID>[...]</alternativeID>
							</initial>
							<terminal>
								<alternativeID>[...]</alternativeID>
							</terminal>
						</pair>
						[...]
					</pairs>
				</alternativesComparisons>
				                			
			

----------------------------


.. _UTASTAR-UTAR-method:

method
~~~~~~


Description:
............

Post-optimality method : if required, please choose between "ac" (analytic center), "mv" (mean value, geometric center), and "md" (utamp, delta maximization).



GUI information:
................


- **Name:** Post-optimality method

  

  - **Type:** drop-down list
  - **Possible values:**
      - None (XMCDA label : none) (default)

      - Analytic center (XMCDA label : ac) 

      - Mean value (XMCDA label : mv) 

      - Max delta (XMCDA label : md) 


XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
				
					<methodParameters>
						<parameter>
							<value>
            					<label>%1</label>
       						 </value>
						</parameter>
					</methodParameters>
				
			

----------------------------



.. _UTASTAR-UTAR_outputs:

Outputs
-------


- :ref:`valueFunctions <UTASTAR-UTAR-valueFunctions>`
- :ref:`message <UTASTAR-UTAR-logMessage>`

.. _UTASTAR-UTAR-valueFunctions:

valueFunctions
~~~~~~~~~~~~~~


Description:
............

Constructed value functions for the selected criteria and the provided rankings, using ACUTA method.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
				
					<criteria mcdaConcept="criteria">
						<criterion>
							<criterionID>[...]</criterionID>
							<criterionFunction>
								<points>
									<point>
										<abscissa><real>[...]</real></abscissa>
										<ordinate><real>[...]</real></ordinate>
									</point>
								</points>
							</criterionFunction>
						</criterion>
					</criteria>
				
			

----------------------------


.. _UTASTAR-UTAR-logMessage:

message
~~~~~~~


Description:
............

logMessage



XMCDA related:
..............

- **Tag:** methodMessages

- **Code:**

  ::

    
				
					<methodMessages mcdaConcept="methodMessage">
						<logMessage>
							<text>[...]</text>
						</logMessage>
						<errorMessage>
							<text>[...]</text>
						</errorMessage>
					</methodMessages>
				
			

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
