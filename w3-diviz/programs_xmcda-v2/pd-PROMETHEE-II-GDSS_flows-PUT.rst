:orphan:



.. _PROMETHEE-II-GDSS_flows-PUT:

PROMETHEE-II-GDSS_flows
=======================

:Provider: PUT
:Version: 1.0.0

Description
-----------

Module for calculation PROMETHEE group ranking as a weighted sum of flows from every decision maker for each alternative.

- **Contact:** Magdalena Dziecielska <magdalenadziecielska6@gmail.com>

- **Web page:** https://github.com/MagdalenaDziecielska/PrometheeDiviz

- **Reference:** C. Macharis, J.-P. Brans and B. Mareschal, 1998. The GDSS PROMETHEE Procedure. Journal of Decision Systems, Vol. 7-SI/1998, 283-307.



Inputs
------
(For outputs, see :ref:`below <PROMETHEE-II-GDSS_flows-PUT_outputs>`)


- :ref:`alternatives <PROMETHEE-II-GDSS_flows-PUT-input1>`
- :ref:`flows_1 <PROMETHEE-II-GDSS_flows-PUT-flows_1>`
- :ref:`flows_2 <PROMETHEE-II-GDSS_flows-PUT-flows_2>`
- :ref:`flows_3 <PROMETHEE-II-GDSS_flows-PUT-flows_3>` *(optional)*
- :ref:`flows_4 <PROMETHEE-II-GDSS_flows-PUT-flows_4>` *(optional)*
- :ref:`flows_5 <PROMETHEE-II-GDSS_flows-PUT-flows_5>` *(optional)*
- :ref:`flows_6 <PROMETHEE-II-GDSS_flows-PUT-flows_6>` *(optional)*
- :ref:`flows_7 <PROMETHEE-II-GDSS_flows-PUT-flows_7>` *(optional)*
- :ref:`flows_8 <PROMETHEE-II-GDSS_flows-PUT-flows_8>` *(optional)*
- :ref:`flows_9 <PROMETHEE-II-GDSS_flows-PUT-flows_9>` *(optional)*
- :ref:`flows_10 <PROMETHEE-II-GDSS_flows-PUT-flows_10>` *(optional)*
- :ref:`nbDM <PROMETHEE-II-GDSS_flows-PUT-nbDM>`
- :ref:`method_parameters <PROMETHEE-II-GDSS_flows-PUT-method_parameters>`

.. _PROMETHEE-II-GDSS_flows-PUT-input1:

alternatives
~~~~~~~~~~~~


Description:
............

Alternatives to consider.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _PROMETHEE-II-GDSS_flows-PUT-flows_1:

flows 1
~~~~~~~


Description:
............

Flows for decision maker 1.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _PROMETHEE-II-GDSS_flows-PUT-flows_2:

flows 2
~~~~~~~


Description:
............

Flows for decision maker 2.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _PROMETHEE-II-GDSS_flows-PUT-flows_3:

flows 3
~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Flows for decision maker 3.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _PROMETHEE-II-GDSS_flows-PUT-flows_4:

flows 4
~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Flows for decision maker 4.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _PROMETHEE-II-GDSS_flows-PUT-flows_5:

flows 5
~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Flows for decision maker 5.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _PROMETHEE-II-GDSS_flows-PUT-flows_6:

flows 6
~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Flows for decision maker 6.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _PROMETHEE-II-GDSS_flows-PUT-flows_7:

flows 7
~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Flows for decision maker 7.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _PROMETHEE-II-GDSS_flows-PUT-flows_8:

flows 8
~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Flows for decision maker 8.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _PROMETHEE-II-GDSS_flows-PUT-flows_9:

flows 9
~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Flows for decision maker 9.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _PROMETHEE-II-GDSS_flows-PUT-flows_10:

flows 10
~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Flows for decision maker 10.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _PROMETHEE-II-GDSS_flows-PUT-nbDM:

Number of decision makers
~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

Number of decision makers (2-10)



GUI information:
................


- **Name:** Number of decision makers

  

  - **Constraint description:** An integer value between 2 and 10 (inclusive).

  - **Type:** integer
  - **Default value:** 2

XMCDA related:
..............

- **Tag:** parameter

- **Code:**

  ::

    %1

----------------------------


.. _PROMETHEE-II-GDSS_flows-PUT-method_parameters:

method parameters
~~~~~~~~~~~~~~~~~


Description:
............

A set of parameters provided to tune up the module's operation.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** decision maker 1 weight

  

  - **Constraint description:** An integer value.

  - **Type:** float
  - **Default value:** 0.0
- **Name:** decision maker 2 weight

  

  - **Constraint description:** An integer value.

  - **Type:** float
  - **Default value:** 0.0
- **Name:** decision maker 3 weight

  

  - **Constraint description:** An integer value. Choose 0.0 when decision maker does not exist.

  - **Type:** float
  - **Default value:** 0.0
- **Name:** decision maker 4 weight

  

  - **Constraint description:** An integer value. Choose 0.0 when decision maker does not exist.

  - **Type:** float
  - **Default value:** 0.0
- **Name:** decision maker 5 weight

  

  - **Constraint description:** An integer value. Choose 0.0 when decision maker does not exist.

  - **Type:** float
  - **Default value:** 0.0
- **Name:** decision maker 6 weight

  

  - **Constraint description:** An integer value. Choose 0.0 when decision maker does not exist.

  - **Type:** float
  - **Default value:** 0.0
- **Name:** decision maker 7 weight

  

  - **Constraint description:** An integer value. Choose 0.0 when decision maker does not exist.

  - **Type:** float
  - **Default value:** 0.0
- **Name:** decision maker 8 weight

  

  - **Constraint description:** An integer value. Choose 0.0 when decision maker does not exist.

  - **Type:** float
  - **Default value:** 0.0
- **Name:** decision maker 9 weight

  

  - **Constraint description:** An integer value. Choose 0.0 when decision maker does not exist.

  - **Type:** float
  - **Default value:** 0.0
- **Name:** decision maker 10 weight

  

  - **Constraint description:** An integer value. Choose 0.0 when decision maker does not exist.

  - **Type:** float
  - **Default value:** 0.0

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    

                <methodParameters>
                    %1
                    %2
                    %3
                    %4
                    %5
                    %6
                    %7
                    %8
                    %9
                    %10
                </methodParameters>

      

----------------------------



.. _PROMETHEE-II-GDSS_flows-PUT_outputs:

Outputs
-------


- :ref:`aggregated_flows <PROMETHEE-II-GDSS_flows-PUT-output1>`
- :ref:`ranking <PROMETHEE-II-GDSS_flows-PUT-output2>`
- :ref:`messages <PROMETHEE-II-GDSS_flows-PUT-output3>`

.. _PROMETHEE-II-GDSS_flows-PUT-output1:

aggregated flows
~~~~~~~~~~~~~~~~


Description:
............

Aggregated flows and weights for decision makers.



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------


.. _PROMETHEE-II-GDSS_flows-PUT-output2:

ranking
~~~~~~~


Description:
............

Final group ranking



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------


.. _PROMETHEE-II-GDSS_flows-PUT-output3:

messages
~~~~~~~~


Description:
............

Messages or errors generated by this module.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
