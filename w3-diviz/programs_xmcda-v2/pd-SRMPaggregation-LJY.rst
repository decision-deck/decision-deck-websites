:orphan:



.. _SRMPaggregation-LJY:

S-RMP Aggregation
=================

:Provider: LJY
:Version: 1.2

Description
-----------

Execute the S-RMP model based aggregation process for a series of alternatives with a given set of model parameters and create the ranking list of alternatives.

- **Contact:** Jinyan Liu (jinyan.liu@ecp.fr)

- **Web page:** http://www.lgi.ecp.fr/pmwiki.php/PagesPerso/JinyanLiu

- **Reference:** Antoine Rolland. Reference-based preferences aggregation procedures in multi-criteria decision making. European Journal of Operational Research, pages 479–486, 2013.
		

- **Reference:** Jun Zheng. Preference elicitation for aggregation models based on reference points: algorithms and procedures. These PhD. May, 2012.



Inputs
------
(For outputs, see :ref:`below <SRMPaggregation-LJY_outputs>`)


- :ref:`criteria <SRMPaggregation-LJY-inc>`
- :ref:`alternatives <SRMPaggregation-LJY-ina>`
- :ref:`performanceTable <SRMPaggregation-LJY-inperf>`
- :ref:`profileConfigs <SRMPaggregation-LJY-inrp>`
- :ref:`weights <SRMPaggregation-LJY-inw>`
- :ref:`lexicography <SRMPaggregation-LJY-inlexi>`

.. _SRMPaggregation-LJY-inc:

criteria
~~~~~~~~


Description:
............

A list of criteria involved in the ranking problem. For each criterion, the preference direction should be provided. Besides, the maximum and minimum should also be included.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    <criteria> <criterion id="[...]" name="[...]"> </criterion> </criteria>

----------------------------


.. _SRMPaggregation-LJY-ina:

alternatives
~~~~~~~~~~~~


Description:
............

A complete list of alternatives to be considered in the ranking problem.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
				<alternatives> <alternative id="[...]" name="[...]"/> </alternatives>
			

----------------------------


.. _SRMPaggregation-LJY-inperf:

performance table
~~~~~~~~~~~~~~~~~


Description:
............

Values of criteria for each alternatives defined in the alternatives list.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _SRMPaggregation-LJY-inrp:

reference points
~~~~~~~~~~~~~~~~


Description:
............

A set of reference points (special alternatives) with their performance values on each criteria.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _SRMPaggregation-LJY-inw:

criteria weights
~~~~~~~~~~~~~~~~


Description:
............

The weights of criteria. For each criteria, the value of weight should strictly positive. The sum of all criteria weights should be normalized to 1.



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _SRMPaggregation-LJY-inlexi:

lexicographic order
~~~~~~~~~~~~~~~~~~~


Description:
............

the lexicographic order of the reference points we use during aggregation.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------



.. _SRMPaggregation-LJY_outputs:

Outputs
-------


- :ref:`preorder <SRMPaggregation-LJY-out>`
- :ref:`messages <SRMPaggregation-LJY-msg>`

.. _SRMPaggregation-LJY-out:

preorder
~~~~~~~~


Description:
............

A ranking list of alternatives.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    

			

----------------------------


.. _SRMPaggregation-LJY-msg:

messages
~~~~~~~~


Description:
............

Some status messages.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
