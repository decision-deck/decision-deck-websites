:orphan:



.. _defuzzificationCOM-URV:

defuzzificationCOM
==================

:Provider: URV
:Version: 1.0

Description
-----------

Implementation of a defuzzification of set of fuzzy labels according to the COM method (Center of Maximum). The method consists in returning the point in the center of the interval where the membership function has its maximum value.
Given a set of alternatives that are associated to a linguistic fuzzy term (alternativeValues file) a translation to the corresponding numerical value is made.

- **Contact:** Aida Valls <aida.valls@urv.cat>



Inputs
------
(For outputs, see :ref:`below <defuzzificationCOM-URV_outputs>`)


- :ref:`linguisticScores <defuzzificationCOM-URV-input0>`
- :ref:`fuzzyNumbers <defuzzificationCOM-URV-input1>`

.. _defuzzificationCOM-URV-input0:

linguistic scores
~~~~~~~~~~~~~~~~~


Description:
............

Linguistic label describing the performance of each alternative



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
		  
			<alternativesValues>
			  <alternativeValue>
                                <alternativeID>
                                        identifier
                                </alernativeID>
				<value>
					<label> ... </label>
				</value>
			  </alternativeValue>
			
                          [... list of other alternatives with id and label ]

			</alternativesValues>
		  
		  

----------------------------


.. _defuzzificationCOM-URV-input1:

fuzzy numbers
~~~~~~~~~~~~~


Description:
............

Definition of a fuzzy variable as a list of labels (trapezoidal)



XMCDA related:
..............

- **Tag:** categoriesValues

- **Code:**

  ::

    
                    
			<categoriesValue>
			<categoryValue>
			   <values>
				<value id="xxx" name="xxxxxxxx">
						<fuzzyNumber>
							<trapezoidal>
								<point1>
									<abscissa>[...]</abscissa>
									<ordinate>[...]</ordinate>
								</point1>
								[...]
								<point4>
									[...]
								</point4>
							</trapezoidal>
						</fuzzyNumber>
				</value>
			    	[...]
        	            </values>
			</categoryValue>
			</categoriesValue>
                    
                     

----------------------------



.. _defuzzificationCOM-URV_outputs:

Outputs
-------


- :ref:`alternativesValues <defuzzificationCOM-URV-output0>`
- :ref:`messages <defuzzificationCOM-URV-output1>`

.. _defuzzificationCOM-URV-output0:

numerical values
~~~~~~~~~~~~~~~~


Description:
............

Numerical score for each alternative according to the input fuzzy label.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _defuzzificationCOM-URV-output1:

messages
~~~~~~~~


Description:
............

A status message.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
