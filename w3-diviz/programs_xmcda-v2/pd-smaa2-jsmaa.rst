:orphan:



.. _smaa2-jsmaa:

smaa2
=====

:Provider: jsmaa
:Version: 0.2

Description
-----------

JSMAA adapter for DD. Currently implements only SMAA-2 without preference information and only cardinal interval measurements of reals.

- **Contact:** Tommi Tervonen (t.p.tervonen@rug.nl)

- **Web page:** http://www.smaa.fi



Inputs
------
(For outputs, see :ref:`below <smaa2-jsmaa_outputs>`)


- :ref:`criteria <smaa2-jsmaa-criteria>`
- :ref:`alternatives <smaa2-jsmaa-alternatives>`
- :ref:`performanceTable <smaa2-jsmaa-performanceTable>`

.. _smaa2-jsmaa-criteria:

criteria
~~~~~~~~


Description:
............

A list of criteria.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
               
                   <criteria>
                       <criterion>
                           [...]
                       </criterion>
                       [...]
                   </criteria>
                   
           

----------------------------


.. _smaa2-jsmaa-alternatives:

alternatives
~~~~~~~~~~~~


Description:
............

A list of alternatives.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
               
                   <alternatives>
                       <alternative>
                           [...]
                       </alternative>
                       [...]
                   </alternatives>
                   
           

----------------------------


.. _smaa2-jsmaa-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~


Description:
............

A performance table. The evaluations should be numeric values, i.e. <real>, <integer> or <rational>.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------



.. _smaa2-jsmaa_outputs:

Outputs
-------


- :ref:`confidenceFactors <smaa2-jsmaa-confidenceFactors>`
- :ref:`rankAcceptabilities <smaa2-jsmaa-rankAcceptabilities>`
- :ref:`centralWeights <smaa2-jsmaa-centralWeights>`
- :ref:`messages <smaa2-jsmaa-messages>`

.. _smaa2-jsmaa-confidenceFactors:

confidenceFactors
~~~~~~~~~~~~~~~~~


Description:
............

Confidence factors of SMAA-2 computation.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _smaa2-jsmaa-rankAcceptabilities:

rankAcceptabilities
~~~~~~~~~~~~~~~~~~~


Description:
............

Rank acceptabilities of SMAA-2 computation.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _smaa2-jsmaa-centralWeights:

centralWeights
~~~~~~~~~~~~~~


Description:
............

Central weights of SMAA-2 computation.



XMCDA related:
..............

- **Tag:** other

----------------------------


.. _smaa2-jsmaa-messages:

messages
~~~~~~~~


Description:
............

Output of messages. Currently always outputs execution succesful :)



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
