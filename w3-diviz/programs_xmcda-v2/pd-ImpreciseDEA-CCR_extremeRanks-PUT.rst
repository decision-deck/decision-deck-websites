:orphan:



.. _ImpreciseDEA-CCR_extremeRanks-PUT:

ImpreciseDEA-CCR_extremeRanks
=============================

:Provider: PUT
:Version: 1.0

Description
-----------

Computes extrene efficiency ranks for the given DMUs (alternatives) using CCR Data Envelopment Analysis Model.

- **Contact:** Anna Labijak <support@decision-deck.org>



Inputs
------
(For outputs, see :ref:`below <ImpreciseDEA-CCR_extremeRanks-PUT_outputs>`)


- :ref:`units <ImpreciseDEA-CCR_extremeRanks-PUT-units>`
- :ref:`inputsOutputs <ImpreciseDEA-CCR_extremeRanks-PUT-inputsOutputs>`
- :ref:`performanceTable <ImpreciseDEA-CCR_extremeRanks-PUT-performanceTable>`
- :ref:`maxPerformanceTable <ImpreciseDEA-CCR_extremeRanks-PUT-maxPerformanceTable>` *(optional)*
- :ref:`weightsLinearConstraints <ImpreciseDEA-CCR_extremeRanks-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <ImpreciseDEA-CCR_extremeRanks-PUT-methodParameters>`

.. _ImpreciseDEA-CCR_extremeRanks-PUT-units:

units
~~~~~


Description:
............

A list of alternatives (DMUs).



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
        <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
      

----------------------------


.. _ImpreciseDEA-CCR_extremeRanks-PUT-inputsOutputs:

inputs/outputs
~~~~~~~~~~~~~~


Description:
............

A list of criteria with specified preference direction. List has to contains at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output).



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
        <criteria>
                        <criterion>
							<scale>								
                                [...]
							</scale>
                            [...]
                        </criterion>
                        [...]
                    </criteria>
      

----------------------------


.. _ImpreciseDEA-CCR_extremeRanks-PUT-performanceTable:

performance
~~~~~~~~~~~


Description:
............

A list of alternatives (DMUs) minimal performances (or exact performances if intervals should be created by tolerance).



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    
        <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>
      

----------------------------


.. _ImpreciseDEA-CCR_extremeRanks-PUT-maxPerformanceTable:

max performance
~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A list of alternatives (DMUs) maximal performances.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    
        <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>
      

----------------------------


.. _ImpreciseDEA-CCR_extremeRanks-PUT-weightsLinearConstraints:

weights constraints
~~~~~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

A list of criteria linear constraints.



XMCDA related:
..............

- **Tag:** criteriaLinearConstraints

- **Code:**

  ::

    
        <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID> [...] </criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>
      

----------------------------


.. _ImpreciseDEA-CCR_extremeRanks-PUT-methodParameters:

method parameters
~~~~~~~~~~~~~~~~~


Description:
............

Represents method parameters (tolerance).



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** tolerance

  

  - **Constraint description:** The value should be a non-negative number.

  - **Type:** float
  - **Default value:** 0.00

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
        
    <methodParameters>
        <parameter id="tolerance">
            <value><real>%1</real></value>
        </parameter>
    </methodParameters>
      

----------------------------



.. _ImpreciseDEA-CCR_extremeRanks-PUT_outputs:

Outputs
-------


- :ref:`bestRank <ImpreciseDEA-CCR_extremeRanks-PUT-bestRank>`
- :ref:`worstRank <ImpreciseDEA-CCR_extremeRanks-PUT-worstRank>`
- :ref:`messages <ImpreciseDEA-CCR_extremeRanks-PUT-messages>`

.. _ImpreciseDEA-CCR_extremeRanks-PUT-bestRank:

best rank
~~~~~~~~~


Description:
............

A list of alternatives with computed best rank for each of them.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
        
         <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
						  <values>
                <value>
							  [...]
						    </value>
              </values>
						</alternativeValue>
						[...]
					</alternativesValues>
      

----------------------------


.. _ImpreciseDEA-CCR_extremeRanks-PUT-worstRank:

worst rank
~~~~~~~~~~


Description:
............

A list of alternatives with computed worst rank for each of them.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
        
         <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
						    <values>
                  <value>
							    [...]
						      </value>
                </values>
						</alternativeValue>
						[...]
					</alternativesValues>
      

----------------------------


.. _ImpreciseDEA-CCR_extremeRanks-PUT-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
