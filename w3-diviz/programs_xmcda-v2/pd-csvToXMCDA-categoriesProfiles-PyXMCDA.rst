:orphan:



.. _csvToXMCDA-categoriesProfiles-PyXMCDA:

csvToXMCDA-categoriesProfiles
=============================

:Provider: PyXMCDA
:Version: 1.0

Description
-----------

Transforms a file containing categories profiles from a comma-separated values (CSV) file to XMCDA compliant file, containing the alternatives ids with their limits (lowerCategory and upperCategory).

- **Contact:** Sébastien Bigaret (sebastien.bigaret@telecom-bretagne.eu)

- **Web page:** http://github.com/sbigaret/ws-PyXMCDA



Inputs
------
(For outputs, see :ref:`below <csvToXMCDA-categoriesProfiles-PyXMCDA_outputs>`)


- :ref:`categoriesProfiles.csv <csvToXMCDA-categoriesProfiles-PyXMCDA-categoriesProfiles_csv>`

.. _csvToXMCDA-categoriesProfiles-PyXMCDA-categoriesProfiles_csv:

categoriesProfiles (csv)
~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The categories profiles as a CSV file.

Example::

  pMG,Medium,Good
  pMB,Bad,Medium



XMCDA related:
..............

- **Tag:** other

----------------------------



.. _csvToXMCDA-categoriesProfiles-PyXMCDA_outputs:

Outputs
-------


- :ref:`categoriesProfiles <csvToXMCDA-categoriesProfiles-PyXMCDA-categoriesProfiles>`
- :ref:`messages <csvToXMCDA-categoriesProfiles-PyXMCDA-messages>`

.. _csvToXMCDA-categoriesProfiles-PyXMCDA-categoriesProfiles:

categoriesProfiles
~~~~~~~~~~~~~~~~~~


Description:
............

The equivalent categories profiles.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _csvToXMCDA-categoriesProfiles-PyXMCDA-messages:

messages
~~~~~~~~


Description:
............

Status messages.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
