:orphan:



.. _plotStarGraphPerformanceTable-ITTB:

plotStarGraphPerformanceTable
=============================

:Provider: ITTB
:Version: 1.0

Description
-----------

This web service generates, for each alternative, a plot representing the performance table as a  star graph. Colors can be used. You can specify how to display the star graphs: by line, by column or in a grid. The star graphs can also be ordered by name or by id (of the alternatives).

- **Contact:** Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)



Inputs
------
(For outputs, see :ref:`below <plotStarGraphPerformanceTable-ITTB_outputs>`)


- :ref:`criteria <plotStarGraphPerformanceTable-ITTB-criteria>`
- :ref:`alternatives <plotStarGraphPerformanceTable-ITTB-alternatives>`
- :ref:`performanceTable <plotStarGraphPerformanceTable-ITTB-performanceTable>`
- :ref:`options <plotStarGraphPerformanceTable-ITTB-options>`

.. _plotStarGraphPerformanceTable-ITTB-criteria:

criteria
~~~~~~~~


Description:
............

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active. Preference direction for the selected criteria can be provided (min or max). In this web service, the default value is set to max.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
                   
                    <criteria>
                        <criterion>
                            <active>[...]</active>
                            <scale>
								<quantitative>
									<preferenceDirection>[...]</preferenceDirection>
								</quantitative>
							</scale>
                        </criterion>
                        [...]
                    </criteria>
                    
               

----------------------------


.. _plotStarGraphPerformanceTable-ITTB-alternatives:

alternatives
~~~~~~~~~~~~


Description:
............

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
                   
                    <alternatives>
                        <alternative>
                            <active>[...]</active>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
                    
               

----------------------------


.. _plotStarGraphPerformanceTable-ITTB-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~


Description:
............

A performance table. The evaluations should be only real or integer numeric values, i.e. <real> or <integer>.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _plotStarGraphPerformanceTable-ITTB-options:

options
~~~~~~~


Description:
............

None



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** Use preference directions?

  *Taking (or not) into account preference directions in the generated star graphs.*

  - **Type:** drop-down list
  - **Possible values:**
      - Yes (XMCDA label : true) 

      - No (XMCDA label : false) (default)

- **Name:** Unique or multiple plot(s)?

  *In a unique plot, only one image is generated containing all star graphs. Multiple plots can be obtained. The default value is true.*

  - **Type:** drop-down list
  - **Possible values:**
      - Unique (XMCDA label : true) (default)

      - Multiple (XMCDA label : false) 

- **Name:** Plots arrangement

  *In the case of a unique plot, you can specify how to display the star graphs: by line, by column or in a grid. The default value is by column.*

  - **Type:** drop-down list
  - **Possible values:**
      - Column (XMCDA label : column) (default)

      - Line (XMCDA label : line) 

      - Grid (XMCDA label : grid) 

- **Name:** Order abscissa by:

  *Choose between "name" or "id".*

  - **Type:** drop-down list
  - **Possible values:**
      - name (XMCDA label : name) 

      - id (XMCDA label : id) (default)

- **Name:** order

  *The parameter which says if the star graphs are sorted out in the alphabetical order or its inverse, according to the title.*

  - **Type:** drop-down list
  - **Possible values:**
      - Ascending order (XMCDA label : increasing) (default)

      - Descending order (XMCDA label : decreasing) 

- **Name:** Use Colors?

  *The use of colors: true for colored star graphs.*

  - **Type:** drop-down list
  - **Possible values:**
      - Yes (XMCDA label : true) 

      - No (XMCDA label : false) (default)

- **Name:** Choose color:

  *Choose between "Black", "Red", "Blue", "Green", "Yellow", "Magenta" and "Cyan".*

  - **Type:** drop-down list
  - **Possible values:**
      - Black (XMCDA label : black) (default)

      - Red (XMCDA label : red) 

      - Blue (XMCDA label : blue) 

      - Green (XMCDA label : green) 

      - Yellow (XMCDA label : yellow) 

      - Magenta (XMCDA label : magenta) 

      - Cyan (XMCDA label : cyan) 


XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                   
					<methodParameters>	
						
						  <parameter id="preference_direction" name="Use preference direction">
							<value>
            					<label>%1</label>
       						 </value>
						</parameter>
						  <parameter id="unique_plot" name="Unique plot">
							<value>
            					<label>%2</label>
       						 </value>
						</parameter>
						 <parameter id="plots_display" name="Plots' display">
							<value>
            					<label>%3</label>
       						 </value>
						</parameter>
                        <parameter id="order_by" name="Order by">
							<value>
            					<label>%4</label>
       						 </value>
						</parameter>
                        <parameter id="order" name="order">
							<value>
            					<label>%5</label>
       						 </value>
						</parameter>						
						 <parameter id="use_color" name="Colors in the plots">
							<value>
            					<label>%6</label>
       						 </value>
						</parameter>
						 <parameter id="selected_color" name="Selected color">
							<value>
            					<label>%7</label>
       						 </value>
       						 </parameter>
					</methodParameters>
				
               

----------------------------



.. _plotStarGraphPerformanceTable-ITTB_outputs:

Outputs
-------


- :ref:`starGraph <plotStarGraphPerformanceTable-ITTB-starGraph>`
- :ref:`messages <plotStarGraphPerformanceTable-ITTB-messages>`

.. _plotStarGraphPerformanceTable-ITTB-starGraph:

starGraph
~~~~~~~~~


Description:
............

A string containing the base64 representation of the png image of the generated plot.



XMCDA related:
..............

- **Tag:** alternativeValue

----------------------------


.. _plotStarGraphPerformanceTable-ITTB-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
