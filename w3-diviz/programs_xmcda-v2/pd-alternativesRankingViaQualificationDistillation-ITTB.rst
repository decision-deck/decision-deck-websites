:orphan:



.. _alternativesRankingViaQualificationDistillation-ITTB:

alternativesRankingViaQualificationDistillation
===============================================

:Provider: ITTB
:Version: 1.1

Description
-----------

This web service computes rankings on the alternatives by distillation of alternatives' qualification. Compared to the web service alternativesRankingViaQualificationDistillation, this web service requires a non-valued or a 0-1 valued outranking relation as input. The partial ranking (intersectionDistillation) is obtained by taking the intersection of upwards and downwards distillation preorders (which are also provided).

- **Contact:** Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)



Inputs
------
(For outputs, see :ref:`below <alternativesRankingViaQualificationDistillation-ITTB_outputs>`)


- :ref:`alternatives <alternativesRankingViaQualificationDistillation-ITTB-alternatives>`
- :ref:`outrankingRelation <alternativesRankingViaQualificationDistillation-ITTB-outrankingRelation>`

.. _alternativesRankingViaQualificationDistillation-ITTB-alternatives:

alternatives
~~~~~~~~~~~~


Description:
............

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
				
                    <alternatives>
                        <alternative>
                            <active>[...]</active>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
                    
			

----------------------------


.. _alternativesRankingViaQualificationDistillation-ITTB-outrankingRelation:

outrankingRelation
~~~~~~~~~~~~~~~~~~


Description:
............

A non-valued relation or a 0-1 valued outranking relation. A numeric <value> indicates a the valuation for each <pair> of the relation.



XMCDA related:
..............

- **Tag:** alternativesComparisons

- **Code:**

  ::

    
				
                    <alternativesComparisons>
                        <pairs>
                            <pair>
                                <initial>
                                    <alternativeID>[...]</alternativeID>
                                </initial>
                                <terminal>
                                    <alternativeID>[...]</alternativeID>
                                </terminal>
                                <value>
                                    <real>[...]</real>
                                </value>
                            </pair>

                            [...]
                        </pairs>
                    </alternativesComparisons>
                    
			

----------------------------



.. _alternativesRankingViaQualificationDistillation-ITTB_outputs:

Outputs
-------


- :ref:`intersectionDistillation <alternativesRankingViaQualificationDistillation-ITTB-intersectionDistillation>`
- :ref:`downwardsDistillation <alternativesRankingViaQualificationDistillation-ITTB-downwardsDistillation>`
- :ref:`upwardsDistillation <alternativesRankingViaQualificationDistillation-ITTB-upwardsDistillation>`
- :ref:`messages <alternativesRankingViaQualificationDistillation-ITTB-messages>`

.. _alternativesRankingViaQualificationDistillation-ITTB-intersectionDistillation:

intersectionDistillation
~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

An <alternativesComparisons> containing the partial preorder obtained by taking the intersection of the downwards and upwards distillation preorders.



XMCDA related:
..............

- **Tag:** alternativesComparisons

- **Code:**

  ::

    
				
                    <alternativesComparisons mcdaConcept="Intersection distillation">
                        <pairs>
                            <pair>
                                <initial>
                                    <alternativeID>[...]</alternativeID>
                                </initial>
                                <terminal>
                                    <alternativeID>[...]</alternativeID>
                                </terminal>
                            </pair>
                            [...]
                        </pairs>
                    </alternativesComparisons>
                    
			

----------------------------


.. _alternativesRankingViaQualificationDistillation-ITTB-downwardsDistillation:

downwardsDistillation
~~~~~~~~~~~~~~~~~~~~~


Description:
............

Alternatives' ranks in the downwards distillation preorder.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
				
					<alternativesValues mcdaConcept="Downwards distillation">
				    	<alternativeValue>
	                       	<alternativeID>[...]</alternativeID>
                        	<value>
	                          	<real>[...]</real>
                        	</value>
                    	</alternativeValue>
                    </alternativesValues>
                
			

----------------------------


.. _alternativesRankingViaQualificationDistillation-ITTB-upwardsDistillation:

upwardsDistillation
~~~~~~~~~~~~~~~~~~~


Description:
............

Alternatives' ranks in the upwards distillation preorder.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
				
					<alternativesValues mcdaConcept="Upwards distillation">
				    	<alternativeValue>
	                       	<alternativeID>[...]</alternativeID>
                        	<value>
	                          	<real>[...]</real>
                        	</value>
                    	</alternativeValue>
                    </alternativesValues>
                
			

----------------------------


.. _alternativesRankingViaQualificationDistillation-ITTB-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
