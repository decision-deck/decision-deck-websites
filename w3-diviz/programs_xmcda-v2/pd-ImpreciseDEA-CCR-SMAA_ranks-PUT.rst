:orphan:



.. _ImpreciseDEA-CCR-SMAA_ranks-PUT:

ImpreciseDEA-CCR-SMAA_ranks
===========================

:Provider: PUT
:Version: 1.0

Description
-----------

Computes ranks for the given DMUs (alternatives) using SMAA-D method and CCR Data Envelopment Analysis Model with imprecise data. For given number of samples  returns a matrix with alternatives in each row and rankings in each column. Single cell indicates how many samples of respective alternative gave respective position in ranking.

- **Contact:** Anna Labijak <support@decision-deck.org>



Inputs
------
(For outputs, see :ref:`below <ImpreciseDEA-CCR-SMAA_ranks-PUT_outputs>`)


- :ref:`units <ImpreciseDEA-CCR-SMAA_ranks-PUT-units>`
- :ref:`inputsOutputs <ImpreciseDEA-CCR-SMAA_ranks-PUT-inputsOutputs>`
- :ref:`performanceTable <ImpreciseDEA-CCR-SMAA_ranks-PUT-performanceTable>`
- :ref:`maxPerformanceTable <ImpreciseDEA-CCR-SMAA_ranks-PUT-maxPerformanceTable>` *(optional)*
- :ref:`weightsLinearConstraints <ImpreciseDEA-CCR-SMAA_ranks-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <ImpreciseDEA-CCR-SMAA_ranks-PUT-methodParameters>`

.. _ImpreciseDEA-CCR-SMAA_ranks-PUT-units:

units
~~~~~


Description:
............

A list of alternatives (DMUs).



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>

----------------------------


.. _ImpreciseDEA-CCR-SMAA_ranks-PUT-inputsOutputs:

inputs/outputs
~~~~~~~~~~~~~~


Description:
............

A list of criteria with specified preference direction. List has to contains at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output).



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    <criteria>
                        <criterion>
							<scale>
								<quantitative>
									<preferenceDirection>
										[...]
									</preferenceDirection>
								</quantitative>
							</scale>
                            [...]
                        </criterion>
                        [...]
                    </criteria>

----------------------------


.. _ImpreciseDEA-CCR-SMAA_ranks-PUT-performanceTable:

performance
~~~~~~~~~~~


Description:
............

A list of alternatives (DMUs) performances.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>

----------------------------


.. _ImpreciseDEA-CCR-SMAA_ranks-PUT-maxPerformanceTable:

max performance
~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A list of alternatives (DMUs) maximal performances.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>

----------------------------


.. _ImpreciseDEA-CCR-SMAA_ranks-PUT-weightsLinearConstraints:

weights constraints
~~~~~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

A list of criteria linear constraints.



XMCDA related:
..............

- **Tag:** criteriaLinearConstraints

- **Code:**

  ::

    <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID> [...] </criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>

----------------------------


.. _ImpreciseDEA-CCR-SMAA_ranks-PUT-methodParameters:

method parameters
~~~~~~~~~~~~~~~~~


Description:
............

"samplesNb" represents the number of samples to generate.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** number of samples

  

  - **Constraint description:** The value should be a positive integer.

  - **Type:** integer
  - **Default value:** 100
- **Name:** tolerance

  

  - **Constraint description:** The value should be a non-negative number.

  - **Type:** float
  - **Default value:** 0.0

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
         <methodParameters>
            <parameter id="samplesNb">
                <value><integer>%1</integer></value>
            </parameter>
            <parameter id="tolerance">
                <value><real>%2</real></value>
            </parameter>
        </methodParameters>

----------------------------



.. _ImpreciseDEA-CCR-SMAA_ranks-PUT_outputs:

Outputs
-------


- :ref:`rankAcceptabilityIndices <ImpreciseDEA-CCR-SMAA_ranks-PUT-rankAcceptabilityIndices>`
- :ref:`avgRank <ImpreciseDEA-CCR-SMAA_ranks-PUT-avgRank>`
- :ref:`messages <ImpreciseDEA-CCR-SMAA_ranks-PUT-messages>`

.. _ImpreciseDEA-CCR-SMAA_ranks-PUT-rankAcceptabilityIndices:

rank acceptability indices
~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

A performance table for given alternatives. Single performance consists of attribute criterionID representing certain ranking, and a value representing ratio of samples attaining this ranking.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID> Rank [...]</criterionID>
									<value>
									[...]
									</value>
							</performance>
							[...]
						</alternativePerformances>
					</performanceTable>

----------------------------


.. _ImpreciseDEA-CCR-SMAA_ranks-PUT-avgRank:

avgerage rank
~~~~~~~~~~~~~


Description:
............

A list of alternatives with average rank (obtained for given sample).



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
						  <values>
							  <value>
                [...]
                </value>
						  </values>
						</alternativeValue>
						[...]
					</alternativesValues>

----------------------------


.. _ImpreciseDEA-CCR-SMAA_ranks-PUT-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
