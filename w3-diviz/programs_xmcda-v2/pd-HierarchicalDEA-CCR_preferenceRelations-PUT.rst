:orphan:



.. _HierarchicalDEA-CCR_preferenceRelations-PUT:

HierarchicalDEA-CCR_preferenceRelations
=======================================

:Provider: PUT
:Version: 1.0

Description
-----------

Computes necessary and possible preference relations for the pairs of DMUs (alternatives) using CCR Data Envelopment Analysis Model with hierarchical structure of outputs.

- **Contact:** 
            Anna Labijak <anna.labijak@cs.put.poznan.pl>
        



Inputs
------
(For outputs, see :ref:`below <HierarchicalDEA-CCR_preferenceRelations-PUT_outputs>`)


- :ref:`units <HierarchicalDEA-CCR_preferenceRelations-PUT-units>`
- :ref:`performanceTable <HierarchicalDEA-CCR_preferenceRelations-PUT-performanceTable>`
- :ref:`hierarchy <HierarchicalDEA-CCR_preferenceRelations-PUT-hierarchy>`
- :ref:`weightsLinearConstraints <HierarchicalDEA-CCR_preferenceRelations-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <HierarchicalDEA-CCR_preferenceRelations-PUT-methodParameters>`

.. _HierarchicalDEA-CCR_preferenceRelations-PUT-units:

units
~~~~~


Description:
............

A list of alternatives (DMUs).



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
                <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
            

----------------------------


.. _HierarchicalDEA-CCR_preferenceRelations-PUT-performanceTable:

performance
~~~~~~~~~~~


Description:
............

A list of alternatives (DMUs) performances (on leaf hierarchy criteria).



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    
                <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>
            

----------------------------


.. _HierarchicalDEA-CCR_preferenceRelations-PUT-hierarchy:

hierarchy
~~~~~~~~~


Description:
............

The hierarchical structure of criteria.



XMCDA related:
..............

- **Tag:** hierarchy

- **Code:**

  ::

    
                
                <hierarchy>
                    <node>
                        <criterionID>[...]</criterionID>
                        <node>
                            <criterionID>[...]</criterionID>
                            <node>
                                [...]
                            </node>
                            [...]
                        </node>
                        [...]
                    </node>
                </hierarchy>
            

----------------------------


.. _HierarchicalDEA-CCR_preferenceRelations-PUT-weightsLinearConstraints:

weights constraints
~~~~~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

A list of hierarchy criteria linear constraints.



XMCDA related:
..............

- **Tag:** criteriaLinearConstraints

- **Code:**

  ::

    
                <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID> [...] </criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>
            

----------------------------


.. _HierarchicalDEA-CCR_preferenceRelations-PUT-methodParameters:

parameters
~~~~~~~~~~


Description:
............

Represents parameters (hierarchyNode).



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** hierarchy node

  *ID of the hierarchy criterion for which the analysis should be performed.*

  - **Type:** string
  - **Default value:** "root"

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                
    <methodParameters>
        <parameter id="hierarchyNode">
            <value><label>%1</label></value>
        </parameter>
    </methodParameters>
            

----------------------------



.. _HierarchicalDEA-CCR_preferenceRelations-PUT_outputs:

Outputs
-------


- :ref:`necessaryDominance <HierarchicalDEA-CCR_preferenceRelations-PUT-necessaryDominance>`
- :ref:`possibleDominance <HierarchicalDEA-CCR_preferenceRelations-PUT-possibleDominance>`
- :ref:`messages <HierarchicalDEA-CCR_preferenceRelations-PUT-messages>`

.. _HierarchicalDEA-CCR_preferenceRelations-PUT-necessaryDominance:

necessary dominance
~~~~~~~~~~~~~~~~~~~


Description:
............

A list of pairs of DMUs related with necessary preference relation.



XMCDA related:
..............

- **Tag:** alternativesComparisons

- **Code:**

  ::

    
                
         <alternativesComparisons>
						<pairs>
              <pair>
                  <initial>
                    <alternativeID>[...]</alternativeID>
                  </initial>
                  <terminal>
                    <alternativeID>[...]</alternativeID>
                  </terminal>
                  <values>
                    <value>
                      <real>1</real>
                    </value>
                  </values>
              </pair>
						  [...]
            </pairs>
            [...]
					</alternativesComparisons>
            

----------------------------


.. _HierarchicalDEA-CCR_preferenceRelations-PUT-possibleDominance:

possible dominance
~~~~~~~~~~~~~~~~~~


Description:
............

A list of pairs of DMUs related with possible preference relation.



XMCDA related:
..............

- **Tag:** alternativesComparisons

- **Code:**

  ::

    
                
         <alternativesComparisons>
						<pairs>
              <pair>
                  <initial>
                    <alternativeID>[...]</alternativeID>
                  </initial>
                  <terminal>
                    <alternativeID>[...]</alternativeID>
                  </terminal>
                  <values>
                    <value>
                      <real>1</real>
                    </value>
                  </values>
              </pair>
						  [...]
            </pairs>
            [...]
					</alternativesComparisons>
            

----------------------------


.. _HierarchicalDEA-CCR_preferenceRelations-PUT-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
