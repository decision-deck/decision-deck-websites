:orphan:



.. _plotAlternativesValues-ITTB:

plotAlternativesValues
======================

:Provider: ITTB
:Version: 1.1

Description
-----------

This web service generates a barplot or a pie plot representing a numeric quantity for each alternative, like, e.g., an importance value. Compared to the web service plotAlternativesValues, some parameters are added. Colors can be used and the title of the plot can be typed. In the case of a bar chart, the axis-labels can also be typed. The alternatives' evaluations are supposed to be real or integer numeric values.

- **Contact:** Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)



Inputs
------
(For outputs, see :ref:`below <plotAlternativesValues-ITTB_outputs>`)


- :ref:`alternatives <plotAlternativesValues-ITTB-alternatives>`
- :ref:`alternativesValues <plotAlternativesValues-ITTB-alternativesValues>`
- :ref:`methodPlotOptions <plotAlternativesValues-ITTB-methodPlotOptions>`

.. _plotAlternativesValues-ITTB-alternatives:

alternatives
~~~~~~~~~~~~


Description:
............

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
                   
                    <alternatives>
                        <alternative>
                            <active>[...]</active>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
                    
               

----------------------------


.. _plotAlternativesValues-ITTB-alternativesValues:

alternativesValues
~~~~~~~~~~~~~~~~~~


Description:
............

A list of <alternativesValue> representing a certain numeric quantity for each alternative, like, e.g., an overall value.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _plotAlternativesValues-ITTB-methodPlotOptions:

methodPlotOptions
~~~~~~~~~~~~~~~~~


Description:
............

Plot type method: choose between "Bar chart" and "Pie chart". The default plot is a bar chart.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** Chart type:

  *Type of the plot: choose between "Bar chart" and "Pie chart". The default plot is a bar chart.*

  - **Type:** drop-down list
  - **Possible values:**
      - bar chart (XMCDA label : barChart) (default)

      - pie chart (XMCDA label : pieChart) 

- **Name:** Order by:

  *Choose between "name", "id" or "values".*

  - **Type:** drop-down list
  - **Possible values:**
      - name (XMCDA label : name) 

      - id (XMCDA label : id) 

      - values (XMCDA label : values) (default)

- **Name:** Order:

  *The parameter which says if the highest or lowest value is to be placed first.*

  - **Type:** drop-down list
  - **Possible values:**
      - increasing (XMCDA label : increasing) (default)

      - decreasing (XMCDA label : decreasing) 

- **Name:** Colors:

  *The use of colors: true for a colored plot.*

  - **Type:** drop-down list
  - **Possible values:**
      - gradient (XMCDA label : true) 

      - black and white (XMCDA label : false) (default)

- **Name:** Initial color:

  *String that indicates the initial color in the generated barplot or pieplot.Choose between "Black", "Red", "Blue", "Green", "Yellow", "Magenta" and "Cyan".*

  - **Type:** drop-down list
  - **Possible values:**
      - black (XMCDA label : black) (default)

      - red (XMCDA label : red) 

      - blue (XMCDA label : blue) 

      - green (XMCDA label : green) 

      - yellow (XMCDA label : yellow) 

      - magenta (XMCDA label : magenta) 

      - cyan (XMCDA label : cyan) 

- **Name:** Final color:

  *String that indicates the final color in the generated barplot or pieplot. choose between "Black","White", "Red", "Blue", "Green", "Yellow", "Magenta" and "Cyan".*

  - **Type:** drop-down list
  - **Possible values:**
      - black (XMCDA label : black) (default)

      - red (XMCDA label : red) 

      - blue (XMCDA label : blue) 

      - green (XMCDA label : green) 

      - yellow (XMCDA label : yellow) 

      - magenta (XMCDA label : magenta) 

      - cyan (XMCDA label : cyan) 

- **Name:** Chart title:

  *String for the title of the plot. The default value is an empty field.*

  - **Type:** string
- **Name:** X axis label:

  *String for the horizontal axis-label.The default value is an empty field.*

  - **Type:** string
- **Name:** Y axis label:

  *String for the vertical axis-label.The default value is an empty field.*

  - **Type:** string

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                   
					<methodParameters>
						<parameter id="chart_type" name="Chart type">
							<value>
            					<label>%1</label>
       						 </value>
						</parameter>
						<parameter id="order_by" name="Order by">
							<value>
            					<label>%2</label>
       						 </value>
						</parameter>
						<parameter id="order" name="Order">
							<value>
            					<label>%3</label>
       						 </value>
						</parameter>
						<parameter id="use_color" name="Colors in the chart">
							<value>
            					<label>%4</label>
       						 </value>
						</parameter>
						<parameter id="initial_color" name="Initial color">
							<value>
            					<label>%5</label>
       						 </value>
						</parameter>
						<parameter id="final_color" name="Final color">
							<value>
            					<label>%6</label>
       						 </value>
						</parameter>
						<parameter id="chart_title" name="Chart title">
							<value>
            					<label>%7</label>
       						 </value>
						</parameter>
						<parameter id="domain_axis" name="Domain axis label">
							<value>
            					<label>%8</label>
       						 </value>
						</parameter >
						<parameter id="range_axis" name="Range axis label">
							<value>
            					<label>%9</label>
       						 </value>
						</parameter>
					</methodParameters>
				
               

----------------------------



.. _plotAlternativesValues-ITTB_outputs:

Outputs
-------


- :ref:`alternativesValuesPlot <plotAlternativesValues-ITTB-alternativesValuesPlot>`
- :ref:`messages <plotAlternativesValues-ITTB-messages>`

.. _plotAlternativesValues-ITTB-alternativesValuesPlot:

alternativesValuesPlot
~~~~~~~~~~~~~~~~~~~~~~


Description:
............

A string containing the base64 representation of the png image of the generated barplot or pieplot .



XMCDA related:
..............

- **Tag:** alternativeValue

----------------------------


.. _plotAlternativesValues-ITTB-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
