:orphan:



.. _BWM-weights-PUT:

BWM-weights
===========

:Provider: PUT
:Version: 1.0.0

Description
-----------

Computes criteria weights using BWM method.

- **Contact:** Krzysztof Martyn <krzysztof.martyn@wp.pl>

- **Web page:** https://bitbucket.org/Krzysztof_Martyn/bwm



Inputs
------
(For outputs, see :ref:`below <BWM-weights-PUT_outputs>`)


- :ref:`criteria <BWM-weights-PUT-input1>`
- :ref:`best_to_others <BWM-weights-PUT-input2>`
- :ref:`others_to_worst <BWM-weights-PUT-input3>`
- :ref:`parameters <BWM-weights-PUT-input4>`

.. _BWM-weights-PUT-input1:

criteria
~~~~~~~~


Description:
............

Criteria for comparison



XMCDA related:
..............

- **Tag:** criteria

----------------------------


.. _BWM-weights-PUT-input2:

best_to_others
~~~~~~~~~~~~~~


Description:
............

Preference of the best criterion over all the other criteria.



XMCDA related:
..............

- **Tag:** criteriaComparisons

----------------------------


.. _BWM-weights-PUT-input3:

others_to_worst
~~~~~~~~~~~~~~~


Description:
............

Preference of all the criteria over the worst criterion.



XMCDA related:
..............

- **Tag:** criteriaComparisons

----------------------------


.. _BWM-weights-PUT-input4:

parameters
~~~~~~~~~~


Description:
............

Parameter input type specifies whether the given preferences are integer or real.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** input type

  

  - **Type:** drop-down list
  - **Possible values:**
      - Integer (XMCDA label : integer) (default)

      - Real (XMCDA label : real) 


XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
        
		<methodParameters>
			<parameter id="input_type" name="input_type">
				<value>
					<label>%1</label>
				</value>
			</parameter>
		</methodParameters>

      

----------------------------



.. _BWM-weights-PUT_outputs:

Outputs
-------


- :ref:`weights <BWM-weights-PUT-output1>`
- :ref:`messages <BWM-weights-PUT-output2>`

.. _BWM-weights-PUT-output1:

weights
~~~~~~~


Description:
............

Weights for criteria



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _BWM-weights-PUT-output2:

messages
~~~~~~~~


Description:
............

Messages or errors generated by this module. It also contains the minimum value of ksi found by the solver, and the value of Consistency Ratio of result.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
