:orphan:



.. _ElectreDistillation-PUT:

ElectreDistillation
===================

:Provider: PUT
:Version: 0.2.0

Description
-----------

This module performs distillation (upwards/downwards)

- **Web page:** https://github.com/MTomczyk/ElectreDiviz



Inputs
------
(For outputs, see :ref:`below <ElectreDistillation-PUT_outputs>`)


- :ref:`alternatives <ElectreDistillation-PUT-input1>`
- :ref:`credibility <ElectreDistillation-PUT-input2>`
- :ref:`method_parameters <ElectreDistillation-PUT-input3>`

.. _ElectreDistillation-PUT-input1:

alternatives
~~~~~~~~~~~~


Description:
............

Alternatives to consider



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _ElectreDistillation-PUT-input2:

credibility
~~~~~~~~~~~


Description:
............

Credibility relation



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------


.. _ElectreDistillation-PUT-input3:

method_parameters
~~~~~~~~~~~~~~~~~


Description:
............

A set of parameters provided to tune up the module's operation



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** direction

  *Parameters of method*

  - **Type:** drop-down list
  - **Possible values:**
      - Downwards distillation (XMCDA label : downwards) (default)

      - Upwards distillation (XMCDA label : upwards) 

- **Name:** alpha

  *Used in credibility bounds calculations (alpha x cred. + beta)*

  - **Type:** float
  - **Default value:** -0.15
- **Name:** beta

  *Used in credibility bounds calculations (alpha x cred. + beta)*

  - **Type:** float
  - **Default value:** 0.3

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
            
                <methodParameters>
                    <parameter name="direction">
                    <value>
                        <label>%1</label>
                    </value>
                </parameter>
                <parameter name="alpha">
                    <value>
                        <real>%2</real>
                    </value>
                </parameter>
                <parameter name="beta">
                <value>
                    <real>%3</real>
                </value>
                </parameter>
                </methodParameters>
            
            

----------------------------



.. _ElectreDistillation-PUT_outputs:

Outputs
-------


- :ref:`ranking <ElectreDistillation-PUT-output1>`
- :ref:`messages <ElectreDistillation-PUT-output2>`

.. _ElectreDistillation-PUT-output1:

ranking
~~~~~~~


Description:
............

Result of distillation



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _ElectreDistillation-PUT-output2:

messages
~~~~~~~~


Description:
............

Messages or errors generated by this module.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
