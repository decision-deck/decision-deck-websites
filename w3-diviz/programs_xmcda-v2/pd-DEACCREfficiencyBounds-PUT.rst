:orphan:



.. _DEACCREfficiencyBounds-PUT:

DEACCREfficiencyBounds
======================

:Provider: PUT
:Version: 1.0

Description
-----------

Computes efficiency bounds scores for the given DMUs (alternatives) using CCR Data Envelopment Analysis Model.

- **Contact:** Malgorzata Napieraj (napieraj.malgorzata@gmail.com)

- **Reference:** Cooper W., Seiford L., Tone K., Data Envelopment Analysis: A Comprehensive Text with Models, Applications, References and DEA-Solver (2007).



Inputs
------
(For outputs, see :ref:`below <DEACCREfficiencyBounds-PUT_outputs>`)


- :ref:`inputsOutputs <DEACCREfficiencyBounds-PUT-inputsOutputs>`
- :ref:`units <DEACCREfficiencyBounds-PUT-units>`
- :ref:`performanceTable <DEACCREfficiencyBounds-PUT-performanceTable>`
- :ref:`weightsLinearConstraints <DEACCREfficiencyBounds-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <DEACCREfficiencyBounds-PUT-methodParameters>`

.. _DEACCREfficiencyBounds-PUT-inputsOutputs:

inputsOutputs
~~~~~~~~~~~~~


Description:
............

A list of criteria with specified preference direction. List has to contains at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output).



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    <criteria>
                        <criterion>
							<scale>
								<quantitative>
									<preferenceDirection>
										[...]
									</preferenceDirection>
								</quantitative>
							</scale>
                            [...]
                        </criterion>
                        [...]
                    </criteria>

----------------------------


.. _DEACCREfficiencyBounds-PUT-units:

units
~~~~~


Description:
............

A list of alternatives (DMUs).



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>

----------------------------


.. _DEACCREfficiencyBounds-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~


Description:
............

A list of alternatives (DMUs) performances.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    <performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>

----------------------------


.. _DEACCREfficiencyBounds-PUT-weightsLinearConstraints:

weightsLinearConstraints
~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

A list of criteria linear constraints.



XMCDA related:
..............

- **Tag:** criteriaLinearConstraints

- **Code:**

  ::

    <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID>[...]</criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>

----------------------------


.. _DEACCREfficiencyBounds-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~


Description:
............

Determines if the subject alternative should be in the relative group.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** includeSubject

  *Determines if the subject alternative should be in the relative group.*

  - **Default value:** false

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    <methodParameters>
							<parameter name="includeSubject">
								<value><boolean>%1</boolean></value>
							</parameter>
					</methodParameters>

----------------------------



.. _DEACCREfficiencyBounds-PUT_outputs:

Outputs
-------


- :ref:`minEfficiencyOverLeastEfficient <DEACCREfficiencyBounds-PUT-minEfficiencyOverLeastEfficient>`
- :ref:`maxEfficiencyOverLeastEfficient <DEACCREfficiencyBounds-PUT-maxEfficiencyOverLeastEfficient>`
- :ref:`minEfficiencyOverMostEfficient <DEACCREfficiencyBounds-PUT-minEfficiencyOverMostEfficient>`
- :ref:`maxEfficiencyOverMostEfficient <DEACCREfficiencyBounds-PUT-maxEfficiencyOverMostEfficient>`
- :ref:`messages <DEACCREfficiencyBounds-PUT-messages>`

.. _DEACCREfficiencyBounds-PUT-minEfficiencyOverLeastEfficient:

minEfficiencyOverLeastEfficient
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

A list of alternatives with computed minimum efficiency scores relative to the least efficient alternative in the group.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    <alternativesValues>
						<alternativeValue>
						  <alternativeID>[...]</alternativeID>
						  <value>
							[...]
						  </value>
						</alternativeValue>
						[...]
					</alternativesValues>

----------------------------


.. _DEACCREfficiencyBounds-PUT-maxEfficiencyOverLeastEfficient:

maxEfficiencyOverLeastEfficient
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

A list of alternatives with computed maximum efficiency scores relative to the least efficient alternative in the group.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    <alternativesValues>
						<alternativeValue>
						  <alternativeID>[...]</alternativeID>
						  <value>
							[...]
						  </value>
						</alternativeValue>
						[...]
					</alternativesValues>

----------------------------


.. _DEACCREfficiencyBounds-PUT-minEfficiencyOverMostEfficient:

minEfficiencyOverMostEfficient
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

A list of alternatives with computed minimum efficiency scores relative to the most efficient alternative in the group.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    <alternativesValues>
						<alternativeValue>
						  <alternativeID>[...]</alternativeID>
						  <value>
							[...]
						  </value>
						</alternativeValue>
						[...]
					</alternativesValues>

----------------------------


.. _DEACCREfficiencyBounds-PUT-maxEfficiencyOverMostEfficient:

maxEfficiencyOverMostEfficient
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

A list of alternatives with computed maximum efficiency scores relative to the most efficient alternative in the group.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    <alternativesValues>
						<alternativeValue>
						  <alternativeID>[...]</alternativeID>
						  <value>
							[...]
						  </value>
						</alternativeValue>
						[...]
					</alternativesValues>

----------------------------


.. _DEACCREfficiencyBounds-PUT-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
