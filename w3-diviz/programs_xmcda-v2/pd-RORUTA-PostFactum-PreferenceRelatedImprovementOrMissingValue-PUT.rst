:orphan:



.. _RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue-PUT:

RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue
============================================================

:Provider: PUT
:Version: 1.0

Description
-----------

Depending on the need, for a given not satisified preference relation between two alternatives: "a is preferred to b", the module finds one of the two possible types of values. These are the minimal improvement of the performance of  alternative a or the minimal missing value of the comprehensive score of this alternative. In the first case, the procedure finds a maximal greater than one value, that multiplied by the performances on chosen criteria allows to attain a truth of mentioned preference relation. In the other case, it finds the minimal value that need to be added to the comprehensive score of this alternative in order to attain the truth of this relation. In both cases it is possible to consider the truth of mentioned relation for both all or at least one value function.

- **Contact:** Pawel Rychly (pawelrychly@gmail.com).



Inputs
------
(For outputs, see :ref:`below <RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue-PUT_outputs>`)


- :ref:`criteria <RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue-PUT-criteria>`
- :ref:`selected-criteria <RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue-PUT-selected-criteria>` *(optional)*
- :ref:`alternatives <RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue-PUT-alternatives>`
- :ref:`performances <RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue-PUT-performances>`
- :ref:`characteristic-points <RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue-PUT-characteristic-points>` *(optional)*
- :ref:`target <RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue-PUT-target>`
- :ref:`preferences <RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue-PUT-preferences>` *(optional)*
- :ref:`intensities-of-preferences <RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue-PUT-intensities-of-preferences>` *(optional)*
- :ref:`rank-related-requirements <RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue-PUT-rank-related-requirements>` *(optional)*
- :ref:`parameters <RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue-PUT-parameters>`

.. _RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue-PUT-criteria:

criteria
~~~~~~~~


Description:
............

A list of all considered criteria. The input value should be a valid XMCDA document whose main tag is criteria.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
                
                    <criteria>
                        <criterion id="%1" name="%1"></criterion>
                        [...]
                    </criteria>
                    
            

----------------------------


.. _RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue-PUT-selected-criteria:

selected criteria
~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A set of ids of the criteria that should be taken into account for modification of the performances. This parameter is used only if the modification of performances is searched.



XMCDA related:
..............

- **Tag:** criteriaSets

- **Code:**

  ::

    
                
                  <criteriaSets>
                     <criteriaSet>
                        <element><criterionID>[...]</criterionID></element>
                        [...]
                     </criteriaSet>
                  </criteriaSets>
                    
            

----------------------------


.. _RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue-PUT-alternatives:

alternatives
~~~~~~~~~~~~


Description:
............

The list of all considered alternatives. The input value should be a valid XMCDA document whose main tag is alternatives. Each alternative may be described using two attributes: id and name. While the first one denotes a machine readable name, the second represents a human readable name.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
                
                    <alternatives>
                        <alternative id="%1" name="%2" />
                        [...]
                    </alternatives>
                    
            

----------------------------


.. _RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue-PUT-performances:

performances
~~~~~~~~~~~~


Description:
............

Description of evaluation of alternatives on different criteria. It is required to provide the IDs of both criteria and alternatives described previously. The input value should be provided as a valid XMCDA document whose main tag is performanceTable



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    
                
                    <performanceTable>
                        <alternativePerformances>
                            <alternativeID>%1</alternativeID>
                            <performance>
                                <criterionID>%2</criterionID>
                                <value>
                                    <real>%3</real>
                                </value>
                            </performance>
                            [...]
                        </alternativePerformances>
                        [...]
                    </performanceTable>
                    
            

----------------------------


.. _RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue-PUT-characteristic-points:

characteristic points
~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A set of values associated with the criteria. This input allows to determine what type of value function should be used for the particular criterion. For each criterion that has an associated greater than one value, a piecewise linear value function is used. In this case, the mentioned value denotes a number of characteristic points of this value function. For the criteria that are not listed in this file, or for these for which the provided values are lower than two uses a general value function. The input value should be provided as a valid XMCDA document whose main tag is criteriaValues. Each element should contain both an id of the criterion, and value tag.



XMCDA related:
..............

- **Tag:** criteriaValues

- **Code:**

  ::

    
                
                     <criteriaValues>
                        <criterionValue>
                            <criterionID>%1</criterionID>
                            <value>
                                <integer>%2</integer>
                            </value>
                        </criterionValue>
                        [...]
                    </criteriaValues>
                    
            

----------------------------


.. _RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue-PUT-target:

target
~~~~~~


Description:
............

Description of the target to be achieved. it has a form of the weak preference relation. The input value should be provided as a valid XMCDA document whose main tag is alternativesComparisons. It should contain only one pair of alternative ids that distinguish the weak preference relation between two alternatives. It may be interpreted as "The first alternative should be weakly preffered to the second one.



XMCDA related:
..............

- **Tag:** alternativesComparisons

- **Code:**

  ::

    
                
                    <alternativesComparisons>
                        <pairs>
                            <pair>
                                <initial>
                                    <alternativeID>%1</alternativeID>
                                </initial>
                                <terminal>
                                    <alternativeID>%2</alternativeID>
                                </terminal>
                            </pair>
                        </pairs>
                    </alternativesComparisons>
                    
            

----------------------------


.. _RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue-PUT-preferences:

preferences
~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Set of pairwise comparisons of reference alternatives. For a pair of alternatives three types of comparisons are supported. These are the strict preference, weak preference, and indifference. The input value should be provided as a valid XMCDA document whose main tag is alternativesComparisons. For each type of comparison, a separate alternativesComparisons tag should be used. Within these groups a mentioned types are denoted using a comparisonType tag by respectively strict, weak, and indif label. Comparisons should be provided as pairs of alternatives ids.



XMCDA related:
..............

- **Tag:** alternativesComparisons

- **Code:**

  ::

    
                
                    <alternativesComparisons>
                        <comparisonType>
                            %1<!-- type of preference: strong, weak, or indif -->
                        </comparisonType>
                        <pairs>
                            <pair>
                                <initial>
                                    <alternativeID>%2</alternativeID>
                                </initial>
                                <terminal>
                                    <alternativeID>%3</alternativeID>
                                </terminal>
                            </pair>
                            [...]
                        </pairs>
                    </alternativesComparisons>
                    [...]
                    
            

----------------------------


.. _RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue-PUT-intensities-of-preferences:

intensities-of-preferences
~~~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Set of comparisons of intensities of preference. For a pair of preference relations three types of comparisons are supported. These are the strict preference, weak, preference, and indifference. The input value should be provided as a valid XMCDA document whose main tag is alternativesComparisons. For each type of comparison, a separate alternativesComparisons tag should be used. Within these groups  aforementioned types are denoted using a comparisonType tag by respectively strict, weak, and indif label. Comparisons should be provided as pairs of two elementary sets of alternatives ids. The following form is expected:



XMCDA related:
..............

- **Tag:** alternativesComparisons

- **Code:**

  ::

    
                
                    <alternativesComparisons>
                        <comparisonType>%1</comparisonType>
                        <pairs>
                            <pair>
                                <initial>
                                    <alternativesSet>
                                        <element>
                                            <alternativeID>%2</alternativeID>
                                        </element>
                                        <element>
                                            <alternativeID>%3</alternativeID>
                                        </element>
                                    </alternativesSet>
                                </initial>
                                <terminal>
                                    <alternativesSet>
                                        <element>
                                            <alternativeID>%4</alternativeID>
                                        </element>
                                        <element>
                                            <alternativeID>%5</alternativeID>
                                        </element>
                                    </alternativesSet>
                                </terminal>
                            </pair>
                            [...]
                        </pairs>
                    </alternativesComparisons>
                    [...]
                    
            

----------------------------


.. _RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue-PUT-rank-related-requirements:

rank-related-requirements
~~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Set of rank-related requirements. In other words it is a set of  ranges of possible positions in the final ranking for a chosen alternatives. The input value should be provided as a valid XMCDA document whose main tag is alternativesValues. Each requirement should contain both an id of the reffered alternative and a pair of values that denote the desired range. These information should be provided within a separate alternativesValue tag.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
                
                    <alternativesValues>
                        <alternativeValue>
                            <alternativeID>%1</alternativeID>
                            <value>
                                <interval>
                                    <lowerBound><integer>%2</integer></lowerBound>
                                    <upperBound><integer>%3</integer></upperBound>
                                </interval>
                            </value>
                        </alternativeValue>
                        [...]
                    </alternativesValues>
                    
            

----------------------------


.. _RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue-PUT-parameters:

parameters
~~~~~~~~~~


Description:
............

Method parameters
                strict %1 - Single boolean value. Determines whether to use sctrictly increasing (true) or monotonously increasing (false) value functions.
                type_of_result %2 - Determines whether analyse the modification of performances or comprehensive score.
                precision %3 - A float value between 0 and 1 (bounds excluded). It describes the precision of expected result.
                possible_or_necessary %4 - One of the two label values (possible or necessary). This parameter determines does target should be satisfied by one (possible) or all(necessary) compatible value functions.



GUI information:
................


- **Name:** Use strictly increasing value functions?

  *Single boolean value. Determines whether to use sctrictly increasing (true) or monotonously increasing (false) value functions.*

  - **Default value:** false
- **Name:** type of result

  *Type of searched value*

  - **Type:** drop-down list
  - **Possible values:**
      - improvement (XMCDA label : improvement) (default)

      - missing-value (XMCDA label : missing-value) 

- **Name:** precision of the result

  

  - **Constraint description:** A float value between 0 and 1 (bounds excluded). It denotes the expected precision of the result.

  - **Type:** float
  - **Default value:** 0.005
- **Name:** Whether the target should be achieved possibly or necessarily?

  *One of the two label values (possible or necessary). This parameter determines does target should be satisfied by one (possible) or all(necessary) value functions*

  - **Type:** drop-down list
  - **Possible values:**
      - possible (XMCDA label : possible) 

      - necessary (XMCDA label : necessary) (default)


XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
            
                 <methodParameters>
                    <parameter name="strict">
                        <value>
                            <boolean>%1</boolean>
                        </value>
                    </parameter>
                    <parameter name="type_of_result">
                        <value><label>%2</label></value>
                    </parameter>
                    <parameter name="precision">
                        <value>
                            <real>%3</real>
                        </value>
                    </parameter>
                    <parameter name="possible_or_necessary">
                        <value><label>%4</label></value>
                    </parameter>
                </methodParameters>
            
            

----------------------------



.. _RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue-PUT_outputs:

Outputs
-------


- :ref:`result <RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue-PUT-result>`
- :ref:`messages <RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue-PUT-messages>`

.. _RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue-PUT-result:

result
~~~~~~


Description:
............

A real value associated with the id of the considered alternative. Depending on the selected option, it may denote the improvement of performances of this alternative or the missing value of its comprehensive score. The output value should be provided as a valid XMCDA document whose main tag is alternativesValues.



XMCDA related:
..............

- **Tag:** alternativeValue

- **Code:**

  ::

    
                
                    <alternativeValue>
                        <alternativeID>[...]</alternativeID>
                        <value><real>[...]</real></value>
                    </alternativeValue>
                    
            

----------------------------


.. _RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue-PUT-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
