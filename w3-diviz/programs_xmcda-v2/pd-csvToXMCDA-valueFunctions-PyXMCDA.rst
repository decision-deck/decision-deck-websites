:orphan:



.. _csvToXMCDA-valueFunctions-PyXMCDA:

csvToXMCDA-valueFunctions
=========================

:Provider: PyXMCDA
:Version: 1.0

Description
-----------

Transforms a file containing value functions from a comma-separated values (CSV) file to a XMCDA compliant file, containing the criteria ids with their names and criterion functions (points).

- **Contact:** Sébastien Bigaret (sebastien.bigaret@telecom-bretagne.eu)

- **Web page:** http://github.com/sbigaret/ws-PyXMCDA



Inputs
------
(For outputs, see :ref:`below <csvToXMCDA-valueFunctions-PyXMCDA_outputs>`)


- :ref:`valueFunctions.csv <csvToXMCDA-valueFunctions-PyXMCDA-valueFunctions_csv>`

.. _csvToXMCDA-valueFunctions-PyXMCDA-valueFunctions_csv:

valueFunctions (csv)
~~~~~~~~~~~~~~~~~~~~


Description:
............

The value functions as a CSV file.

Example:

  g1,cost,0,0
  ,,21334,1
  g2,Acceleration,0,0
  ,,30.8,1
  g3,PickUp,0,0
  ,,41.6,1
  g4,Brakes,0,0
  ,,2.66,1
  g5,RoadHold,0,0
  ,,3.25,1



XMCDA related:
..............

- **Tag:** other

----------------------------



.. _csvToXMCDA-valueFunctions-PyXMCDA_outputs:

Outputs
-------


- :ref:`valueFunctions <csvToXMCDA-valueFunctions-PyXMCDA-valueFunctions>`
- :ref:`messages <csvToXMCDA-valueFunctions-PyXMCDA-messages>`

.. _csvToXMCDA-valueFunctions-PyXMCDA-valueFunctions:

valueFunctions
~~~~~~~~~~~~~~


Description:
............

The equivalent value functions.



XMCDA related:
..............

- **Tag:** criteria

----------------------------


.. _csvToXMCDA-valueFunctions-PyXMCDA-messages:

messages
~~~~~~~~


Description:
............

Status messages.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
