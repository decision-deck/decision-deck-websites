:orphan:



.. _DEASMAACCRPreferenceRelations-PUT:

DEASMAACCRPreferenceRelations
=============================

:Provider: PUT
:Version: 1.0

Description
-----------

Determines dominance relations for the given DMUs (alternatives) using SMAA-D method and CCR Data Envelopment Analysis Model. For given number of samples  returns a matrix with alternatives in each row and column. Single cell indicates how many samples of alternative in a row dominates alternative in a column.

- **Contact:** Malgorzata Napieraj (napieraj.malgorzata@gmail.com)

- **Reference:** Cooper W., Seiford L., Tone K., Data Envelopment Analysis: A Comprehensive Text with Models, Applications, References and DEA-Solver (2007).

- **Reference:** Lahdelma R., Salminen P., Stochastic multicriteria acceptability analysis using the data envelopment model (2004).



Inputs
------
(For outputs, see :ref:`below <DEASMAACCRPreferenceRelations-PUT_outputs>`)


- :ref:`inputsOutputs <DEASMAACCRPreferenceRelations-PUT-inputsOutputs>`
- :ref:`units <DEASMAACCRPreferenceRelations-PUT-units>`
- :ref:`performanceTable <DEASMAACCRPreferenceRelations-PUT-performanceTable>`
- :ref:`weightsLinearConstraints <DEASMAACCRPreferenceRelations-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <DEASMAACCRPreferenceRelations-PUT-methodParameters>`

.. _DEASMAACCRPreferenceRelations-PUT-inputsOutputs:

inputsOutputs
~~~~~~~~~~~~~


Description:
............

A list of criteria with specified preference direction. List has to contains at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output).



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    <criteria>
                        <criterion>
							<scale>
								<quantitative>
									<preferenceDirection>
										[...]
									</preferenceDirection>
								</quantitative>
							</scale>
                            [...]
                        </criterion>
                        [...]
                    </criteria>

----------------------------


.. _DEASMAACCRPreferenceRelations-PUT-units:

units
~~~~~


Description:
............

A list of alternatives (DMUs).



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>

----------------------------


.. _DEASMAACCRPreferenceRelations-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~


Description:
............

A list of alternatives (DMUs) performances.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    <performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>

----------------------------


.. _DEASMAACCRPreferenceRelations-PUT-weightsLinearConstraints:

weightsLinearConstraints
~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

A list of criteria linear constraints.



XMCDA related:
..............

- **Tag:** criteriaLinearConstraints

- **Code:**

  ::

    <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID>[...]</criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>

----------------------------


.. _DEASMAACCRPreferenceRelations-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~


Description:
............

"samplesNo" represents the number of samples to generate.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** samplesNo

  

  - **Constraint description:** The value should be a positive integer.

  - **Type:** integer
  - **Default value:** 100

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    <methodParameters>
							<parameter name="samplesNo">
								<value><integer>%1</integer></value>
							</parameter>
					</methodParameters>

----------------------------



.. _DEASMAACCRPreferenceRelations-PUT_outputs:

Outputs
-------


- :ref:`pairwiseOutrankingIndices <DEASMAACCRPreferenceRelations-PUT-pairwiseOutrankingIndices>`
- :ref:`messages <DEASMAACCRPreferenceRelations-PUT-messages>`

.. _DEASMAACCRPreferenceRelations-PUT-pairwiseOutrankingIndices:

pairwiseOutrankingIndices
~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

A performance table for given alternatives. Single performance consists of attribute criterionID representing dominated alternative, and a value representing ratio of samples dominating this alternative.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    <performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>geq [...]</criterionID>
									<value>
									[...]
									</value>
							</performance>
							[...]
						</alternativePerformances>
					</performanceTable>

----------------------------


.. _DEASMAACCRPreferenceRelations-PUT-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
