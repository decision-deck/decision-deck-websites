:orphan:



.. _cutRelation-ITTB:

cutRelation
===========

:Provider: ITTB
:Version: 1.1

Description
-----------

This web service cuts a fuzzy relation (on alternatives) at a given threshold and produces a relation. Compared to the web service cutRelation, the produced relation is not necessary binary. In fact, some parameters are added. You can choose between classical cut and bipolar cut. The cut level is specified and an output relation is generated. In the case of classical cut, the output relation can be crisp output, binary output or other binary output. When a bipolar cut is taken into account, the output relation can be a bipolar output or other bipolar output.

- **Contact:** Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)



Inputs
------
(For outputs, see :ref:`below <cutRelation-ITTB_outputs>`)


- :ref:`options <cutRelation-ITTB-options>`
- :ref:`alternatives <cutRelation-ITTB-alternatives>`
- :ref:`relation <cutRelation-ITTB-relation>`

.. _cutRelation-ITTB-options:

options
~~~~~~~


Description:
............

Generates a graph taking into account the proposed options.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** Cut type:

  *Choose between classical or bipolar.*

  - **Type:** drop-down list
  - **Possible values:**
      - classical (XMCDA label : classical) (default)

      - bipolar (XMCDA label : bipolar)

- **Name:** Cut threshold:

  *Float for the cut level. The default value is zero.*

  - **Type:** float
  - **Default value:** 0

- **Name:** Classical output:

  *Choose between crisp, binary or other binary output.*

  - **Type:** drop-down list
  - **Possible values:**
      - crisp (XMCDA label : crisp)

      - 0-1 valued (XMCDA label : classical_binary) (default)

      - other valued (XMCDA label : other_binary)

- **Name:** Bipolar output:

  *Choose between bipolar or other bipolar output.*

  - **Type:** drop-down list
  - **Possible values:**
      - -1-0-1 valued (XMCDA label : classical_bipolar) (default)

      - other valued (XMCDA label : other_bipolar)

- **Name:** True:

  *Float for the true value. The default value is 1.*

  - **Type:** float
  - **Default value:** 1

- **Name:** False:

  *Float for the false value. The default value is an 0.*

  - **Type:** float
  - **Default value:** 0

- **Name:** Indeterminate:

  *Float for the indeterminate value. The default value is 0.5.*

  - **Type:** float
  - **Default value:** 0.5

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::



					<methodParameters>
						 <parameter id= "cut_type" name="Cut type">
							<value>
            					<label>%1</label>
       						 </value>
						</parameter>
						<parameter id= "cut_threshold" name="Cut threshold">
							<value>
            					<real>%2</real>
       						 </value>
						</parameter>
						<parameter id= "classical_output" name="Classical output">
							<value>
            					<label>%3</label>
       						 </value>
						</parameter>
						<parameter id= "bipolar_output" name="Bipolar output">
							<value>
            					<label>%4</label>
       						 </value>
						</parameter>
						 <parameter id= "true" name="True">
							<value>
            					<real>%5</real>
       						 </value>
						</parameter>
						 <parameter id= "false" name="False">
							<value>
            					<real>%6</real>
       						 </value>
						</parameter>
						 <parameter id= "indeterminate" name="Indeterminate">
							<value>
            					<real>%7</real>
       						 </value>
						</parameter>
					</methodParameters>



----------------------------


.. _cutRelation-ITTB-alternatives:

alternatives
~~~~~~~~~~~~


Description:
............

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::



                    <alternatives>
                        <alternative>
                            <active>[...]</active>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>



----------------------------


.. _cutRelation-ITTB-relation:

relation
~~~~~~~~


Description:
............

A valued relation relative to comparisons of the alternatives. A numeric <value> indicates a the valuation for each <pair> of the relation.



XMCDA related:
..............

- **Tag:** alternativesComparisons

- **Code:**

  ::



                    <alternativesComparisons>
                        <pairs>
                            <pair>
                                <initial>
                                    <alternativeID>[...]</alternativeID>
                                </initial>
                                <terminal>
                                    <alternativeID>[...]</alternativeID>
                                </terminal>
                                <value>
                                    <real>[...]</real>
                                </value>
                            </pair>

                            [...]
                        </pairs>
                    </alternativesComparisons>



----------------------------



.. _cutRelation-ITTB_outputs:

Outputs
-------


- :ref:`output_relation <cutRelation-ITTB-outputRelation>`
- :ref:`messages <cutRelation-ITTB-messages>`

.. _cutRelation-ITTB-outputRelation:

output_relation
~~~~~~~~~~~~~~~


Description:
............

The relation resulting from the cut.



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------


.. _cutRelation-ITTB-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
