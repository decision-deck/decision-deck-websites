:orphan:



.. _HierarchicalDEA-CCR-SMAA_ranks-PUT:

HierarchicalDEA-CCR-SMAA_ranks
==============================

:Provider: PUT
:Version: 1.0

Description
-----------

Computes ranks for the given DMUs (alternatives) using SMAA-D method and CCR Data Envelopment Analysis Model with hierarchical structure of outputs. For given number of samples  returns a matrix with alternatives in each row and rankings in each column. Single cell indicates how many samples of respective alternative gave respective position in ranking.

- **Contact:** 
            Anna Labijak <anna.labijak@cs.put.poznan.pl>
        



Inputs
------
(For outputs, see :ref:`below <HierarchicalDEA-CCR-SMAA_ranks-PUT_outputs>`)


- :ref:`units <HierarchicalDEA-CCR-SMAA_ranks-PUT-units>`
- :ref:`performanceTable <HierarchicalDEA-CCR-SMAA_ranks-PUT-performanceTable>`
- :ref:`hierarchy <HierarchicalDEA-CCR-SMAA_ranks-PUT-hierarchy>`
- :ref:`weightsLinearConstraints <HierarchicalDEA-CCR-SMAA_ranks-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <HierarchicalDEA-CCR-SMAA_ranks-PUT-methodParameters>`

.. _HierarchicalDEA-CCR-SMAA_ranks-PUT-units:

units
~~~~~


Description:
............

A list of alternatives (DMUs).



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
                <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
            

----------------------------


.. _HierarchicalDEA-CCR-SMAA_ranks-PUT-performanceTable:

performance
~~~~~~~~~~~


Description:
............

A list of alternatives (DMUs) performances (on leaf hierarchy criteria).



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    
                <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>
            

----------------------------


.. _HierarchicalDEA-CCR-SMAA_ranks-PUT-hierarchy:

hierarchy
~~~~~~~~~


Description:
............

The hierarchical structure of criteria.



XMCDA related:
..............

- **Tag:** hierarchy

- **Code:**

  ::

    
                
                <hierarchy>
                    <node>
                        <criterionID>[...]</criterionID>
                        <node>
                            <criterionID>[...]</criterionID>
                            <node>
                                [...]
                            </node>
                            [...]
                        </node>
                        [...]
                    </node>
                </hierarchy>
            

----------------------------


.. _HierarchicalDEA-CCR-SMAA_ranks-PUT-weightsLinearConstraints:

weights constraints
~~~~~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

A list of hierarchy criteria linear constraints.



XMCDA related:
..............

- **Tag:** criteriaLinearConstraints

- **Code:**

  ::

    
                <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID> [...] </criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>
            

----------------------------


.. _HierarchicalDEA-CCR-SMAA_ranks-PUT-methodParameters:

parameters
~~~~~~~~~~


Description:
............

"number of samples" represents the number of samples to generate; "hierarchy node" is the ID of the hierarchy criterion for which the analysis should be performed.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** number of samples

  

  - **Constraint description:** The value should be a positive integer.

  - **Type:** integer
  - **Default value:** 100
- **Name:** hierarchy node

  

  - **Type:** string
  - **Default value:** "root"
- **Name:** random seed (-1 for default time-based seed)

  

  - **Constraint description:** The value should be a non-negative integer or -1 if no constant seed required.

  - **Type:** integer
  - **Default value:** -1

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                
         <methodParameters>
            <parameter id="samplesNb">
                <value><integer>%1</integer></value>
            </parameter>
            <parameter id="hierarchyNode">
                <value><label>%3</label></value>
            </parameter>
            <parameter id="randomSeed">
                <value><integer>%4</integer></value>
            </parameter>
        </methodParameters>
            

----------------------------



.. _HierarchicalDEA-CCR-SMAA_ranks-PUT_outputs:

Outputs
-------


- :ref:`rankAcceptabilityIndices <HierarchicalDEA-CCR-SMAA_ranks-PUT-rankAcceptabilityIndices>`
- :ref:`avgRank <HierarchicalDEA-CCR-SMAA_ranks-PUT-avgRank>`
- :ref:`messages <HierarchicalDEA-CCR-SMAA_ranks-PUT-messages>`

.. _HierarchicalDEA-CCR-SMAA_ranks-PUT-rankAcceptabilityIndices:

rank acceptability indices
~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

A performance table for given alternatives. Single performance consists of attribute criterionID representing certain ranking, and a value representing ratio of samples attaining this ranking.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    
                <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID> Rank [...]</criterionID>
									<value>
									[...]
									</value>
							</performance>
							[...]
						</alternativePerformances>
					</performanceTable>
            

----------------------------


.. _HierarchicalDEA-CCR-SMAA_ranks-PUT-avgRank:

average rank
~~~~~~~~~~~~


Description:
............

A list of alternatives with average rank (obtained with sampling).



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
                <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
						  <values>
							  <value>
                [...]
                </value>
						  </values>
						</alternativeValue>
						[...]
					</alternativesValues>
            

----------------------------


.. _HierarchicalDEA-CCR-SMAA_ranks-PUT-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
