:orphan:



.. _ElectreCredibility-PUT:

ElectreCredibility
==================

:Provider: PUT
:Version: 0.2.0

Description
-----------

ElectreCredibility - computes credibility matrix using procedure which is common to the most methods from the Electre family.

The key feature of this module is its flexibility in terms of the types of elements accepted as input - concordance or discordance may be a result of the following comparisons: alternatives vs alternatives, alternatives vs boundary profiles and alternatives vs central (characteristic) profiles, and discordance may be provided both in non-aggregated ('partial') and aggregated form (think of discordances in Electre TRI and Electre Is methods).

- **Web page:** http://github.com/xor-xor/electre_diviz



Inputs
------
(For outputs, see :ref:`below <ElectreCredibility-PUT_outputs>`)


- :ref:`alternatives <ElectreCredibility-PUT-input1>`
- :ref:`concordance <ElectreCredibility-PUT-input3>`
- :ref:`discordance <ElectreCredibility-PUT-input4>`
- :ref:`classes_profiles <ElectreCredibility-PUT-input2>` *(optional)*
- :ref:`method_parameters <ElectreCredibility-PUT-input5>`

.. _ElectreCredibility-PUT-input1:

alternatives
~~~~~~~~~~~~


Description:
............

Alternatives to consider.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _ElectreCredibility-PUT-input3:

concordance
~~~~~~~~~~~


Description:
............

Concordance matrix with aggregated concordance indices (i.e. one index per pair of alternatives or alternatives/profiles).



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------


.. _ElectreCredibility-PUT-input4:

discordance
~~~~~~~~~~~


Description:
............

Discordance indices provided in non-aggregated ('partial') or aggregated form. In the former case, 'with_denominator' parameter (see below) should be set to 'true'.



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------


.. _ElectreCredibility-PUT-input2:

classes_profiles
~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Definitions of profiles (boundary or central) which should be used for classes (categories) representation.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _ElectreCredibility-PUT-input5:

method_parameters
~~~~~~~~~~~~~~~~~


Description:
............

A set of parameters provided to tune up the module's operation.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** comparison_with

  *This parameter specifies the type of elements provided for comparison.

Choosing 'boundary_profiles' or 'central_profiles' requires providing inputs 'classes_profiles' and 'profiles_performance_table' as well (which are optional by default).*

  - **Type:** drop-down list
  - **Possible values:**
      - alternatives vs alternatives (XMCDA label : alternatives) (default)

      - alternatives vs boundary profiles (XMCDA label : boundary_profiles) 

      - alternatives vs central (characteristic) profiles (XMCDA label : central_profiles) 

- **Name:** with_denominator

  *This parameter specifies if you want to take into account only sufficiently great discordance (by using '1 - d_j(a, b) / 1 - C(a, b)' instead of '1 - d_j(a, b)' in credibility's equation).

In case of selecting this option, only discordance indices for which d_j(a, b) > C(a, b) holds are taken into account.*

  - **Default value:** true
- **Name:** only_max_discordance

  *Use only maximum discordance index in discordance aggregation part of credibility's equation (i.e. '1 - max(d_j(a, b))'). When 'with_denominator' is set to 'true', this parameter is ignored.*

  - **Default value:** false
- **Name:** use_partials

  *This parameter specifies whether the discordance indices are provided in non-aggregated form.*

  - **Default value:** true

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
        
        <methodParameters>
          <parameter name="comparison_with">
            <value>
              <label>%1</label>
            </value>
          </parameter>
          <parameter name="with_denominator">
            <value>
              <boolean>%2</boolean>
            </value>
          </parameter>
          <parameter name="only_max_discordance">
            <value>
              <boolean>%3</boolean>
            </value>
          </parameter>
          <parameter name="use_partials">
            <value>
              <boolean>%4</boolean>
            </value>
          </parameter>
        </methodParameters>
        
      

----------------------------



.. _ElectreCredibility-PUT_outputs:

Outputs
-------


- :ref:`credibility <ElectreCredibility-PUT-output1>`
- :ref:`messages <ElectreCredibility-PUT-output2>`

.. _ElectreCredibility-PUT-output1:

credibility
~~~~~~~~~~~


Description:
............

Credibility matrix computed from the given data.



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------


.. _ElectreCredibility-PUT-output2:

messages
~~~~~~~~


Description:
............

Messages or errors generated by this module.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
