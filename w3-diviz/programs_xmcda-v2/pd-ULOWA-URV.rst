:orphan:



.. _ULOWA-URV:

ULOWA
=====

:Provider: URV
:Version: 1.1

Description
-----------

This module implements the ULOWA aggregation operator: Unbalanced Linguistic Ordered Weighted Average. Aggregation operators for linguistic variables usually assume uniform and symmetrical distribution of the linguistic terms that define the variable. However, there are some problems where an unbalanced set of linguistic terms is more appropriate to describe the objects. ULOWA accepts a set of linguistic labels defined with unbalanced fuzzy sets. The fuzzy sets must define a fuzzy partition on the set of reference values. They can be defined by trapezoidal or triangular membership functions.

- **Contact:** Lucas Marin <lucas.marin@urv.cat>, Aida Valls <aida.valls@urv.cat>, Jordi Canals <jordi.canals@estudiants.urv.cat>

- **Reference:** 
			D. Isern, L. Marin, A. Valls, A. Moreno, The Unbalanced Linguistic Ordered Weighted Averaging Operator, In: Proc. IEEE International Conference on Fuzzy Systems, FUZZ-IEEE 2010, IEEE Computer Society, Barcelona, Catalonia, 2010, pp. 3063-3070.
		



Inputs
------
(For outputs, see :ref:`below <ULOWA-URV_outputs>`)


- :ref:`criteria <ULOWA-URV-input1>`
- :ref:`alternatives <ULOWA-URV-input0>`
- :ref:`performanceTable <ULOWA-URV-input3>`
- :ref:`weights <ULOWA-URV-input2>`
- :ref:`fuzzyNumbers <ULOWA-URV-input4>`

.. _ULOWA-URV-input1:

criteria
~~~~~~~~


Description:
............

A list of linguistic criteria. Criteria are always active.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
                            
                    <criteria>
                        <criterion>
                            <active>[...]</active>
                            [...]
                        </criterion>
                        [...]
                    </criteria>
                    
                     

----------------------------


.. _ULOWA-URV-input0:

alternatives
~~~~~~~~~~~~


Description:
............

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false).
By default (no <active> tag), alternatives are considered as active.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
				
                    <alternatives>
                        <alternative>
                            <active>[...]</active>
                            [...]
                        </alternative>
						[...]
					</alternatives>
				
			

----------------------------


.. _ULOWA-URV-input3:

performanceTable
~~~~~~~~~~~~~~~~


Description:
............

The performance table will contain only string values, which correspond to the identifiers of the labels defined in the domain of the linguistic variable.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    
		
		  
			<performanceTable>
				<alternativePerformances>
					<alternativeID>[...]</alternativeID>
					<performance>
						<criterionID>[...]</criterionID>
						<value>
							<label>[...]</label>
						</value>
					</performance>
					[...]
				</alternativePerformances>
				[...]
			</performanceTable>
		  
		  

----------------------------


.. _ULOWA-URV-input2:

weights
~~~~~~~


Description:
............

The weights operate in the same way as the classical OWA operator. They are associated to the values of the alternatives rather than to the criteria. In this way they can define different aggregation policies. Assuming that the values on the alternative will be sorted from the best to the worst (e.g. perfect, good, good, medium, low), the list of weights must be ordered
according to the importance that is given to the values, from the highest to the lowest.
For example a list of weights as (0.5, 0.5, 0, 0, 0) is ignoring the 3 lowest values, and making an average of the
two highest ones. A list like (0, 0, 1, 0 ,0 ) is calculating the median, while (0, 0, 0, 0, 1) is taking the minimum.
Notice that the sum of weights is required to be 1.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
		  
			<alternativesValues>
			  <alternativeValue>
				<values>
					<value>
						<real>[...]</real>
					</value>
					<value>
						<integer>[...]</integer>
					</value>
					[...]
			  </alternativeValue>
			</alternativesValues>
		  
		  

----------------------------


.. _ULOWA-URV-input4:

fuzzyNumbers
~~~~~~~~~~~~


Description:
............

This file contains the definition of the fuzzy sets associated to the linguistic variable that is used for all the criteria. The semantics of the linguistic labels are given by a trapezoidal membership function, that is represented with a tuple of 4 pairs values ((x1,y1),(x2,y2),(x3,y3),(x4,y4). The first value corresponds to the abscissa point x, and the second to the ordinate value, y. If x2=x3 the fuzzy set A is triangular.

The values must be ordered on the X axis, so that x1 < x2 < x3 < x4.

The ordinates correspond to the membership degree for each point, so they must be concave function. The maximum value of y is 1.

Notice that the ULOWA operator does not use the ordinate value.

For each label in the linguistic domain (categoriesValues list), a fuzzy set must be defined.

The labels must be ordered from the worst to the best performance (fi. Low, Medium, High, Perfect).



XMCDA related:
..............

- **Tag:** categoriesValues

- **Code:**

  ::

    
		  
				<categoriesValues>
					<categoryValue>
						<values>
							<value id="...">
			                	<fuzzyNumber>
			                	 	<trapezoidal>
				                	 	<point1>
				                	 		<abscissa><real>...</real></abscissa>
				                	 		<ordinate><real>...</real></ordinate>
				                	 	</point1>
				                	 	[...]
			                	 	</trapezoidal>
			                	</fuzzyNumber>
			                </value>
			                [...]
			            </values>
       				</categoryValue>
				</categoriesValues>		  
			

----------------------------



.. _ULOWA-URV_outputs:

Outputs
-------


- :ref:`alternativesValues <ULOWA-URV-output0>`
- :ref:`messages <ULOWA-URV-output1>`

.. _ULOWA-URV-output0:

alternativesValues
~~~~~~~~~~~~~~~~~~


Description:
............

Result obtained from the ULOWA aggregation on each alternative. It will be a linguistic label belonging to the domain defined in the fuzzyNumbers input file.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _ULOWA-URV-output1:

messages
~~~~~~~~


Description:
............

A status message.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
