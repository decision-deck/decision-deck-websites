.. index:: tutorial, workflow creation
.. _tutorial1:

Basic usage of diviz
====================

.. toctree::
   :maxdepth: 2

Short description of the tutorial
---------------------------------

This tutorial shows you how to create a basic workflow and run it.

Detailed description of the tutorial
------------------------------------

This tutorial focusses on the following aspects:

  - Loading of XMCDA data;
  - Creation of the workflow;
  - Execution of the workflow;
  - Visualisation of the results.

Tutorial
--------

First start by downloading the following data files which respect the XMCDA standard:

  - :download:`alternatives.xml</XMCDAData/alternatives.xml>` (contains a list of alternatives IDs)
  - :download:`criteria.xml</XMCDAData/criteria.xml>` (contains a list of criteria IDs)
  - :download:`performanceTable.xml</XMCDAData/performanceTable.xml>` (contains a performance table)

Then run diviz. 

After the splash screen you can see an empty workspace. 
The left panel is used to display the open workflows and their past executions. 
The right pane contains the list of available elementary components. 
The center pane is used to construct and display the workflows.

.. figure:: images/tuto_diviz1.png

   *Empy diviz screen*

To create a new workflow, open the workflow menu and click on new. Choose a name for your workflow (e.g., tutorial). 

From the right panel, drag and drop a File(s) element into the central panel. 
A dialog window will open and ask you to choose a file. 
Choose the file ``alternatives.xml`` that you downloaded earlier.

Repeat this process for the remaining two files ``criteria.xml`` and ``performanceTable.xml``

You should now have a screen which looks similar to the following picture.

.. figure:: images/tuto_diviz2.png 

   *3 files have been loaded into diviz*

You can now add an elementary component from the right panel which is called **plotNumericPerformanceTable** (under the category *Visualisation components*). Simply select it and drag and drop it to the central panel. 

You can now connect the data files to the right entries on the newly created component:

  - ``criteria.xml`` to the entry called **criteria**;
  - ``alternatives.xml`` to the entry called **alternatives**;
  - ``performanceTable.xml`` to the entry called **performanceTable**.

Simply do that by clicking on the bullet of a data file and draging your mouse to the corresponding entry of the component. 

You should now have a screen which looks similar to the following picture.

.. figure:: images/tuto_diviz3.png

   *A component has been added to the workspace, and connected to the data*

Congratulations! You have now created your first workflow in diviz.

It is time now to execute the workflow and have a look at the output of the elementary component. You can do so via the **Execution** menu via the **Run** entry.

After the workflow has been successfully executed, you can click on the entry below the name of your workflow in the left panel to view the results. Your screen should look like the following image.

.. figure:: images/tuto_diviz4.png

   *The result of the execution of your first workflow*

You can now click on the little *file* icons in the workflow to view the various files in the visualisation panel which has appeared under the workflow. In particular the output of the elementary component **plotRealPerformanceTable** is a barchart of the performance table. 


