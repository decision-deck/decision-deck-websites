.. index:: HOWTO

How to submit a new program for diviz?
======================================

Imagine that you have a program and you would like to make it available both through Decision Deck web-services' architecture and the diviz software. 

In order to see what kind of information is required, we suggest to go to the developpers' `corner <//www.decision-deck.org/ws/howtos.html>`_ of the XMCDA web services `website <//www.decision-deck.org/ws/index.html>`_.

