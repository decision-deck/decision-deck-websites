.. index:: tutorial, local Program
.. _tutorial_localProgram:

Call local programs in diviz
============================

.. toctree::
   :maxdepth: 2

Short description of the tutorial
---------------------------------

In this tutorial we show how to call a local program in diviz. 

Detailed description of the tutorial
------------------------------------

This tutorial focusses on the following aspects:

  - Creating a *local program* box in diviz;
  - Configuring this program and executing it;
  - Examples of other local executions.

Tutorial
--------

We suppose for this tutorial that you are running diviz on a Linux computer and that you have an sh-compatible command language interpreter installed. 

First you need to run diviz. In the list of available programs (on your right hand side) you can identify the program called **generic_program**. 

Create a new empty workflow (see this :ref:`tutorial<tutorial1>` to learn how to do this) and drag and drop **generic_program** in the workspace. You should now have a program called **generic_program-1** in your workspace with one input (``infile1``) and one output (``outfile1``). 

If you double-click on **generic_program-1**, its properties window will open. There you can specify the number of inputs and outputs you need for your program. For this example let us stay with one input and one output file. 

For this tutorial, the task of **generic_program-1** should be to add an XML comment at the end of an input XMCDA file and to output the result in an XMCDA file.

  In an MCDA context this might be a useless task. However it allows us to easily present the main features of the local executions in diviz in a very easy and intuitive way. A more complete and MCDA-related tutorial for local programs in R can be found :ref:`here<tutorial_RScript>`.

To do this in **bash** we would probably write something like: 

  ::
  
     cp infile outfile
     echo '<!--This is a comment.-->' >> outfile

To do this in diviz in the workflow that you just created, in the properties window of **generic_program-1**, in the **cmdline** field enter : 

  ::
  
     sh -s infile1 outfile1

This defines the command line which should be used by **generic_program-1**. To be preciser, **sh** will read commands from the standard input (-s argument) and the two arguments **infile1** and **outfile1** will be sent to the interpreter. 

In order to specify the commands which **sh** should read from the standard input, we can check the **Use a script** option and enter the commands to be executed in the textbox which has just appeared:

  ::
  
     cp $1 $2
     echo '<!--I have been written by a local script.-->' >> $2
     
To check that the program that you just created works as desired, connect an input file (for example :download:`alternatives.xml</XMCDAData/alternatives.xml>`) to **generic_program-1** and execute the workflow (see this :ref:`tutorial<tutorial1>` to learn how to do this). The last line of the output file produced by **generic_program-1** should be the XML comment ``<!--I have been written by a local script.-->`` (to check this, click on the output file, and then in the *results* window click on **XML view** and scroll down to the bottom of the file).
 
Further examples
****************

The following examples all number the lines of the input file. Again, in practice this might not make much sense in the MCDA context, but it is a simple example which demonstrates the use of scripts in diviz. We show how to do this in **Python**, via a **Shell** script, and **R**.

Python
######

*cmdline*

  ::
     
     python - infile1 outfile1

*script*     

  ::
    
    import sys
    print "argv: ", sys.argv
    # here, sys.argv[0] is '-'
    input=sys.argv[1]
    output=sys.argv[2]

    idx=0
    output=open(output, 'w')
    for line in open(input):
      output.write("%i: %s"%(idx, line))
      idx += 1
    output.close()

*Remarks*

 - the cmdline launches python on the script file;
 - the script reads its arguments from the command-line; another solution would be to hard-code the names "infile1" and "outfile1" in the script itself.

Shell
#####


*cmdline* 

  ::
    
     sh -s infile1 outfile1
     
*script*

  ::
     
      for n in $(seq 0 $#); do echo \$$n=${!n}; done
      cat -n $1 > $2

*Remarks*

  The same thing can be done without a script, using the cmdline: sh -c "cat -n infile1 > outfile1"

R
#

*cmdline*

  ::
     
     R --slave --vanilla --args infile1 outfile1

*script*

  ::
     
     args=commandArgs(trailingOnly=TRUE)
     lines=readLines(args[1], n=-1)
     idx=0
     for (line in lines)
     {
       idx=idx+1
       cat(idx, "\t", line, "\n", file=args[2], append=TRUE)
     }


*Remarks*

  If you prefer, you can also pass the path to the script file as a command-line argument. In this case, the command-line becomes:

  ::
     
     R --slave --vanilla --file=/path/to/script.R --args infile1 outfile2'

  and the file /path/to/script.R contains the same script as above. Since there is no need to 'Use a script' anymore, this option should be unchecked.

  It is also possible to provide the script as a file in your workspace which is connected to the **generic_program-1** box. To do so, you need to check the option 'Provide the script as a file', which adds an input called ``script`` to the **generic_program-1** box. You just have to connect the file representing your script to this entry. The command-line now becomes: 
  
  ::
     
     R --slave --vanilla --file=script --args infile1 outfile2'
  
  This has a big advantage, as it allows to easily share your script with the workflow (workflow menu, export workflow with input files item). 



