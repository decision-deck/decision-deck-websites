:orphan:



.. _ElectreCrispOutrankingAggregation-PUT:

ElectreCrispOutrankingAggregation
=================================

:Provider: PUT
:Version: 0.2.0

Description
-----------

Computes outranking relation as an aggregation of concordance and discordance binary relations. The relation is true (1) for a pair of alternatives when there is a concordance (1) for A > B and no discordance againts A > B (0).

- **Web page:** https://github.com/MTomczyk/ElectreDiviz



Inputs
------
(For outputs, see :ref:`below <ElectreCrispOutrankingAggregation-PUT_outputs>`)


- :ref:`alternatives <ElectreCrispOutrankingAggregation-PUT-input1>`
- :ref:`profiles <ElectreCrispOutrankingAggregation-PUT-input4>` *(optional)*
- :ref:`concordance <ElectreCrispOutrankingAggregation-PUT-input2>`
- :ref:`discordance <ElectreCrispOutrankingAggregation-PUT-input3>`
- :ref:`method_parameters <ElectreCrispOutrankingAggregation-PUT-input5>`

.. _ElectreCrispOutrankingAggregation-PUT-input1:

alternatives
~~~~~~~~~~~~


Description:
............

Alternatives to consider



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _ElectreCrispOutrankingAggregation-PUT-input4:

classes_profiles
~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Profiles to consider



XMCDA related:
..............

- **Tag:** categories

----------------------------


.. _ElectreCrispOutrankingAggregation-PUT-input2:

concordance_binary
~~~~~~~~~~~~~~~~~~


Description:
............

Concordance binary relation



XMCDA related:
..............

- **Tag:** alternativesMatrix

----------------------------


.. _ElectreCrispOutrankingAggregation-PUT-input3:

discordance_binary
~~~~~~~~~~~~~~~~~~


Description:
............

Discordance binary relation (comparing with alternatives or profiles)



XMCDA related:
..............

- **Tag:** alternativesMatrix

----------------------------


.. _ElectreCrispOutrankingAggregation-PUT-input5:

method_parameters
~~~~~~~~~~~~~~~~~


Description:
............

A set of parameters provided to tune up the module's operation



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** comparison_with

  *Alternatives or Profiles*

  - **Type:** drop-down list
  - **Possible values:**
      - alternatives vs alternatives (XMCDA label : alternatives) (default)

      - alternatives vs boundary profiles (XMCDA label : profiles) 

      - alternatives vs central (characteristic) profiles (XMCDA label : profiles) 


XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
<programParameters>
	<parameter name="comparison_with">
		<values>
			<value>
				<label>%1</label>
			</value>
		</values>
	</parameter>
</programParameters>


----------------------------



.. _ElectreCrispOutrankingAggregation-PUT_outputs:

Outputs
-------


- :ref:`outranking <ElectreCrispOutrankingAggregation-PUT-output1>`
- :ref:`messages <ElectreCrispOutrankingAggregation-PUT-output2>`

.. _ElectreCrispOutrankingAggregation-PUT-output1:

outranking
~~~~~~~~~~


Description:
............

Computed outranking relation



XMCDA related:
..............

- **Tag:** alternativesMatrix

----------------------------


.. _ElectreCrispOutrankingAggregation-PUT-output2:

messages
~~~~~~~~


Description:
............

Messages or errors generated by this module.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
