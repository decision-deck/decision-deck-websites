:orphan:



.. _RORUTADIS-PossibleAndNecessaryAssignments-PUT:

RORUTADIS-PossibleAndNecessaryAssignments
=========================================

:Provider: PUT
:Version: 0.1

Description
-----------

Calculates possible and necessary assignments of alternatives to classes (categories) using Robust Ordinal Regression for value-based sorting. It is possible to provide an additional optional preference information: example alternatives assignments, assignment pairwise comparisons and desired class cardinalities. Service developed by Krzysztof Ciomek (Poznan University of Technology, under supervision of Milosz Kadzinski).

- **Contact:** 
			Krzysztof Ciomek (k.ciomek@gmail.com),
			Milosz Kadzinski (milosz.kadzinski@cs.put.poznan.pl)
		

- **Web page:** https://github.com/kciomek/rorutadis

- **Reference:** None



Inputs
------
(For outputs, see :ref:`below <RORUTADIS-PossibleAndNecessaryAssignments-PUT_outputs>`)


- :ref:`criteria <RORUTADIS-PossibleAndNecessaryAssignments-PUT-criteria>`
- :ref:`alternatives <RORUTADIS-PossibleAndNecessaryAssignments-PUT-alternatives>`
- :ref:`categories <RORUTADIS-PossibleAndNecessaryAssignments-PUT-categories>`
- :ref:`performanceTable <RORUTADIS-PossibleAndNecessaryAssignments-PUT-performanceTable>`
- :ref:`assignmentExamples <RORUTADIS-PossibleAndNecessaryAssignments-PUT-assignmentExamples>` *(optional)*
- :ref:`assignmentComparisons <RORUTADIS-PossibleAndNecessaryAssignments-PUT-assignmentComparisons>` *(optional)*
- :ref:`categoriesCardinalities <RORUTADIS-PossibleAndNecessaryAssignments-PUT-categoriesCardinalities>` *(optional)*
- :ref:`strictlyMonotonicValueFunctions <RORUTADIS-PossibleAndNecessaryAssignments-PUT-strictlyMonotonicValueFunctions>`

.. _RORUTADIS-PossibleAndNecessaryAssignments-PUT-criteria:

criteria
~~~~~~~~


Description:
............

A list of criteria (<criteria> tag) with information about preference direction (<criteriaValues mcdaConcept="preferenceDirection">, 0 - gain, 1 - cost) and number of characteristic points (<criteriaValues mcdaConcept="numberOfCharacteristicPoints">, 0 for the most general marginal utility function or integer grater or equal to 2) of each criterion.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
				
					<criteria>
						<criterion id="[...]" />
						[...]
					</criteria>

					<criteriaValues mcdaConcept="preferenceDirection">
						<criterionValue>
							<criterionID>[...]</criterionID>
							<value><integer>[...]</integer></value>
						</criterionValue>
						[...]
					</criteriaValues>

					<criteriaValues mcdaConcept="numberOfCharacteristicPoints">
						<criterionValue>
							<criterionID>[...]</criterionID>
							<value><integer>[0|integer greater or equal to 2]</integer></value>
						</criterionValue>
						[...]
					</criteriaValues>
				
			

----------------------------


.. _RORUTADIS-PossibleAndNecessaryAssignments-PUT-alternatives:

alternatives
~~~~~~~~~~~~


Description:
............

A list of alternatives.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
				
					<alternatives>
                        <alternative id="[...]">
                            <active>[...]</active>
                        </alternative>
                        [...]
                    </alternatives>
				
			

----------------------------


.. _RORUTADIS-PossibleAndNecessaryAssignments-PUT-categories:

categories
~~~~~~~~~~


Description:
............

A list of categories (classes). List must be sorted from the worst category to the best.



XMCDA related:
..............

- **Tag:** categories

- **Code:**

  ::

    
				
					<categories>
                        <category id="[...]" />
                        [...]
                    </categories>
				
			

----------------------------


.. _RORUTADIS-PossibleAndNecessaryAssignments-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~


Description:
............

The performances of the alternatives.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _RORUTADIS-PossibleAndNecessaryAssignments-PUT-assignmentExamples:

assignmentExamples
~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A list of assignment examples of alternatives to intervals of categories (classes) or to a specific category (class).



XMCDA related:
..............

- **Tag:** alternativesAffectations

- **Code:**

  ::

    
				
					<alternativesAffectations>
						<alternativeAffectation>
							<alternativeID>[...]</alternativeID>
							<categoryID>[...]</categoryID>
						</alternativeAffectation>
						[...]
						<alternativeAffectation>
							<alternativeID>[...]</alternativeID>
							<categoriesInterval>
								<lowerBound>
									<categoryID>[...]</categoryID>
								</lowerBound>
								<upperBound>
									<categoryID>[...]</categoryID>
								</upperBound>
							</categoriesInterval>
						</alternativeAffectation>
						[...]
						<alternativeAffectation>
							<alternativeID>[...]</alternativeID>
							<categoriesSet>
								<categoryID>[...]</categoryID>
								[...]
							</categoriesSet>
						</alternativeAffectation>
						[...]
					</alternativesAffectations>
				
			

----------------------------


.. _RORUTADIS-PossibleAndNecessaryAssignments-PUT-assignmentComparisons:

assignmentComparisons
~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Two lists of assignment pairwise comparisons. A comparison from list with attribute mcdaConcept="atLeastAsGoodAs" indicates that some alternative should be assigned to class at least as good as class of some other alternative (k = 0) or at least better by k classes (k > 0). A comparison from list with attribute mcdaConcept="atMostAsGoodAs" indicates that some alternative should be assigned to class at most better by k classes (k > 0) then some other alternative.



XMCDA related:
..............

- **Tag:** alternativesComparisons

- **Code:**

  ::

    
				
					<alternativesComparisons mcdaConcept="atLeastAsGoodAs">
						<pairs>
							<pair>
								<initial><alternativeID>[...]</alternativeID></initial>
								<terminal><alternativeID>[...]</alternativeID></terminal>
								<value><integer>k</integer></value>
							</pair>
							[...]
						</pairs>
					</alternativesComparisons>

					<alternativesComparisons mcdaConcept="atMostAsGoodAs">
						<pairs>
							[...]
						</pairs>
					</alternativesComparisons>
				
			

----------------------------


.. _RORUTADIS-PossibleAndNecessaryAssignments-PUT-categoriesCardinalities:

categoriesCardinalities
~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A list of category (class) cardinality constraints. It allows to define minimal and/or maximal desired category (class) cardinalities.



XMCDA related:
..............

- **Tag:** categoriesValues

- **Code:**

  ::

    
				
					<categoriesValues>
						<categoryValue>
							<categoryID>[...]</categoryID>
							<value>
								<interval>
									<lowerBound><integer>[...]</integer></lowerBound>
									<upperBound><integer>[...]</integer></upperBound>
								</interval>
							</value>
						</categoryValue>
						[...]
					</categoriesValues>
				
			

----------------------------


.. _RORUTADIS-PossibleAndNecessaryAssignments-PUT-strictlyMonotonicValueFunctions:

strictlyMonotonicValueFunctions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

Whether marginal value functions strictly monotonic (true) or weakly monotonic (false).



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** strictlyMonotonicValueFunctions

  *Whether marginal value functions strictly monotonic or not.*

  - **Default value:** false

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                
                    <methodParameters>
                        <parameter name="strictlyMonotonicValueFunctions">
                            <value>
                                <boolean>%1</boolean>
                            </value>
                        </parameter>
                    </methodParameters>
                
            

----------------------------



.. _RORUTADIS-PossibleAndNecessaryAssignments-PUT_outputs:

Outputs
-------


- :ref:`possibleAssignments <RORUTADIS-PossibleAndNecessaryAssignments-PUT-possibleAssignments>`
- :ref:`necessaryAssignments <RORUTADIS-PossibleAndNecessaryAssignments-PUT-necessaryAssignments>`
- :ref:`messages <RORUTADIS-PossibleAndNecessaryAssignments-PUT-messages>`

.. _RORUTADIS-PossibleAndNecessaryAssignments-PUT-possibleAssignments:

possibleAssignments
~~~~~~~~~~~~~~~~~~~


Description:
............

Possible assignments.



XMCDA related:
..............

- **Tag:** alternativesAffectations

- **Code:**

  ::

    
				
					<alternativesAffectations>
						<alternativeAffectation>
							<alternativeID>[...]</alternativeID>
							<categoryID>[...]</categoryID>
						</alternativeAffectation>
						[...]
						<alternativeAffectation>
							<alternativeID>[...]</alternativeID>
							<categoriesInterval>
								<lowerBound>
									<categoryID>[...]</categoryID>
								</lowerBound>
								<upperBound>
									<categoryID>[...]</categoryID>
								</upperBound>
							</categoriesInterval>
						</alternativeAffectation>
						[...]
						<alternativeAffectation>
							<alternativeID>[...]</alternativeID>
							<categoriesSet>
								<categoryID>[...]</categoryID>
								[...]
							</categoriesSet>
						</alternativeAffectation>
						[...]
					</alternativesAffectations>
				
			

----------------------------


.. _RORUTADIS-PossibleAndNecessaryAssignments-PUT-necessaryAssignments:

necessaryAssignments
~~~~~~~~~~~~~~~~~~~~


Description:
............

Necessary assignments.



XMCDA related:
..............

- **Tag:** alternativesAffectations

- **Code:**

  ::

    
				
					<alternativesAffectations>
						<alternativeAffectation>
							<alternativeID>[...]</alternativeID>
							<categoryID>[...]</categoryID>
						</alternativeAffectation>
						[...]
						<alternativeAffectation>
							<alternativeID>[...]</alternativeID>
							<categoriesInterval>
								<lowerBound>
									<categoryID>[...]</categoryID>
								</lowerBound>
								<upperBound>
									<categoryID>[...]</categoryID>
								</upperBound>
							</categoriesInterval>
						</alternativeAffectation>
						[...]
					</alternativesAffectations>
				
			

----------------------------


.. _RORUTADIS-PossibleAndNecessaryAssignments-PUT-messages:

messages
~~~~~~~~


Description:
............

Messages generated by the program.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
