:orphan:



.. _RORUTA-PairwiseOutrankingIndices-PUT:

RORUTA-PairwiseOutrankingIndices
================================

:Provider: PUT
:Version: 1.1

Description
-----------

The procedure finds pairwise outranking indices matrix for a given problem.

- **Contact:** Pawel Rychly (pawelrychly@gmail.com).



Inputs
------
(For outputs, see :ref:`below <RORUTA-PairwiseOutrankingIndices-PUT_outputs>`)


- :ref:`criteria <RORUTA-PairwiseOutrankingIndices-PUT-criteria>`
- :ref:`alternatives <RORUTA-PairwiseOutrankingIndices-PUT-alternatives>`
- :ref:`performances <RORUTA-PairwiseOutrankingIndices-PUT-performances>`
- :ref:`characteristic-points <RORUTA-PairwiseOutrankingIndices-PUT-characteristic-points>` *(optional)*
- :ref:`criteria-preference-directions <RORUTA-PairwiseOutrankingIndices-PUT-criteria-preference-directions>` *(optional)*
- :ref:`preferences <RORUTA-PairwiseOutrankingIndices-PUT-preferences>` *(optional)*
- :ref:`intensities-of-preferences <RORUTA-PairwiseOutrankingIndices-PUT-intensities-of-preferences>` *(optional)*
- :ref:`rank-related-requirements <RORUTA-PairwiseOutrankingIndices-PUT-rank-related-requirements>` *(optional)*
- :ref:`parameters <RORUTA-PairwiseOutrankingIndices-PUT-parameters>`

.. _RORUTA-PairwiseOutrankingIndices-PUT-criteria:

criteria
~~~~~~~~


Description:
............

A list of all considered criteria. The input value should be a valid XMCDA document whose main tag is criteria.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
                
                    <criteria>
                        <criterion id="%1" name="%1"></criterion>
                        [...]
                    </criteria>
                    
            

----------------------------


.. _RORUTA-PairwiseOutrankingIndices-PUT-alternatives:

alternatives
~~~~~~~~~~~~


Description:
............

The list of all considered alternatives. The input value should be a valid XMCDA document whose main tag is alternatives. Each alternative may be described using two attributes: id and name. While the first one denotes a machine readable name, the second represents a human readable name.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
                
                    <alternatives>
                        <alternative id="%1" name="%2" />
                        [...]
                    </alternatives>
                    
            

----------------------------


.. _RORUTA-PairwiseOutrankingIndices-PUT-performances:

performances
~~~~~~~~~~~~


Description:
............

Description of evaluation of alternatives on different criteria. It is required to provide the IDs of both criteria and alternatives described previously. The input value should be provided as a valid XMCDA document whose main tag is performanceTable



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    
                
                    <performanceTable>
                        <alternativePerformances>
                            <alternativeID>%1</alternativeID>
                            <performance>
                                <criterionID>%2</criterionID>
                                <value>
                                    <real>%3</real>
                                </value>
                            </performance>
                            [...]
                        </alternativePerformances>
                        [...]
                    </performanceTable>
                    
            

----------------------------


.. _RORUTA-PairwiseOutrankingIndices-PUT-characteristic-points:

characteristic points
~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A set of values associated with the criteria. This input allows to determine what type of value function should be used for the particular criterion. For each criterion that has an associated greater than one value, a piecewise linear value function is used. In this case, the mentioned value denotes a number of characteristic points of this value function. For the criteria that are not listed in this file, or for these for which the provided values are lower than two uses a general value function. The input value should be provided as a valid XMCDA document whose main tag is criteriaValues. Each element should contain both an id of the criterion, and value tag.



XMCDA related:
..............

- **Tag:** criteriaValues

- **Code:**

  ::

    
                
                     <criteriaValues>
                        <criterionValue>
                            <criterionID>%1</criterionID>
                            <value>
                                <integer>%2</integer>
                            </value>
                        </criterionValue>
                        [...]
                    </criteriaValues>
                    
            

----------------------------


.. _RORUTA-PairwiseOutrankingIndices-PUT-criteria-preference-directions:

criteria preference directions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A set of values associated with criteria that determine their preference direction (0 - gain, 1 - cost).



XMCDA related:
..............

- **Tag:** criteriaValues

- **Code:**

  ::

    
                
                     <criteriaValues mcdaConcept="preferenceDirection">
                        <criterionValue>
                            <criterionID>%1</criterionID>
                            <value>
                                <integer>%2</integer>
                            </value>
                        </criterionValue>
                        [...]
                    </criteriaValues>
                    
            

----------------------------


.. _RORUTA-PairwiseOutrankingIndices-PUT-preferences:

preferences
~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Set of pairwise comparisons of reference alternatives. For a pair of alternatives three types of comparisons are supported. These are the strict preference, weak preference, and indifference. The input value should be provided as a valid XMCDA document whose main tag is alternativesComparisons. For each type of comparison, a separate alternativesComparisons tag should be used. Within these groups a mentioned types are denoted using a comparisonType tag by respectively strict, weak, and indif label. Comparisons should be provided as pairs of alternatives ids.



XMCDA related:
..............

- **Tag:** alternativesComparisons

- **Code:**

  ::

    
                
                    <alternativesComparisons>
                        <comparisonType>
                            %1<!-- type of preference: strong, weak, or indif -->
                        </comparisonType>
                        <pairs>
                            <pair>
                                <initial>
                                    <alternativeID>%2</alternativeID>
                                </initial>
                                <terminal>
                                    <alternativeID>%3</alternativeID>
                                </terminal>
                            </pair>
                            [...]
                        </pairs>
                    </alternativesComparisons>
                    [...]
                    
            

----------------------------


.. _RORUTA-PairwiseOutrankingIndices-PUT-intensities-of-preferences:

intensities-of-preferences
~~~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Set of comparisons of intensities of preference. For a pair of preference relations three types of comparisons are supported. These are the strict preference, weak, preference, and indifference. The input value should be provided as a valid XMCDA document whose main tag is alternativesComparisons. For each type of comparison, a separate alternativesComparisons tag should be used. Within these groups  aforementioned types are denoted using a comparisonType tag by respectively strict, weak, and indif label. Comparisons should be provided as pairs of two elementary sets of alternatives ids. The following form is expected:



XMCDA related:
..............

- **Tag:** alternativesComparisons

- **Code:**

  ::

    
                
                    <alternativesComparisons>
                        <comparisonType>%1</comparisonType>
                        <pairs>
                            <pair>
                                <initial>
                                    <alternativesSet>
                                        <element>
                                            <alternativeID>%2</alternativeID>
                                        </element>
                                        <element>
                                            <alternativeID>%3</alternativeID>
                                        </element>
                                    </alternativesSet>
                                </initial>
                                <terminal>
                                    <alternativesSet>
                                        <element>
                                            <alternativeID>%4</alternativeID>
                                        </element>
                                        <element>
                                            <alternativeID>%5</alternativeID>
                                        </element>
                                    </alternativesSet>
                                </terminal>
                            </pair>
                            [...]
                        </pairs>
                    </alternativesComparisons>
                    [...]
                    
            

----------------------------


.. _RORUTA-PairwiseOutrankingIndices-PUT-rank-related-requirements:

rank-related-requirements
~~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Set of rank-related requirements. In other words it is a set of  ranges of possible positions in the final ranking for a chosen alternatives. The input value should be provided as a valid XMCDA document whose main tag is alternativesValues. Each requirement should contain both an id of the reffered alternative and a pair of values that denote the desired range. These information should be provided within a separate alternativesValue tag.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
                
                    <alternativesValues>
                        <alternativeValue>
                            <alternativeID>%1</alternativeID>
                            <value>
                                <interval>
                                    <lowerBound><integer>%2</integer></lowerBound>
                                    <upperBound><integer>%3</integer></upperBound>
                                </interval>
                            </value>
                        </alternativeValue>
                        [...]
                    </alternativesValues>
                    
            

----------------------------


.. _RORUTA-PairwiseOutrankingIndices-PUT-parameters:

parameters
~~~~~~~~~~


Description:
............

Method parameters
                    strict %1 - Single boolean value. Determines whether to use sctrictly increasing (true) or monotonously increasing (false) value functions
                    number of samples %2 - Number of samples used to generate result



GUI information:
................


- **Name:** Use strictly increasing value functions?

  *Single boolean value. Determines whether to use sctrictly increasing (true) or monotonously increasing (false) value functions.*

  - **Default value:** false
- **Name:** Number of samples that are used to generate a result

  

  - **Constraint description:** An integer greater than zero value that denotes a number of samples generated by an algorithm.

  - **Type:** integer
  - **Default value:** 100

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                
                     <methodParameters>
                        <parameter name="strict">
                            <value>
                                <boolean>%1</boolean>
                            </value>
                        </parameter>
                        <parameter name="number-of-samples">
                            <value>
                                <integer>%2</integer>
                            </value>
                        </parameter>
                    </methodParameters>
                    
            

----------------------------



.. _RORUTA-PairwiseOutrankingIndices-PUT_outputs:

Outputs
-------


- :ref:`pairwise-outranking-indices <RORUTA-PairwiseOutrankingIndices-PUT-pairwise-outranking-indices>`
- :ref:`messages <RORUTA-PairwiseOutrankingIndices-PUT-messages>`

.. _RORUTA-PairwiseOutrankingIndices-PUT-pairwise-outranking-indices:

pairwise outranking indices
~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

A list of all pairwise outranking indices. The returned value is an XMCDA document whose main tag is alternativesComparisons.



XMCDA related:
..............

- **Tag:** alternativesComparisons

- **Code:**

  ::

    
                
                    <alternativesComparisons>
                        <pairs>
                          <pair>
                            <initial>
                              <alternativeID>[...]</alternativeID>
                            </initial>
                            <terminal>
                              <alternativeID>[...]</alternativeID>
                            </terminal>
                            <value>
                                <real>[...]</real>
                            </value>
                          </pair>
                          [...]
                      </pairs>
                    </alternativesComparisons>
                    
            

----------------------------


.. _RORUTA-PairwiseOutrankingIndices-PUT-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
