:orphan:



.. _plotCriteriaValuesPreorder-ITTB:

plotCriteriaValuesPreorder
==========================

:Provider: ITTB
:Version: 2.0

Description
-----------

This web service generates a graph representing a preorder on the criteria, according to numerical values taken by the criteria (the "best" criteria has the highest value). Colors can be used and the graph can have a title. You can choose between an increasing or a decreasing order for the graph. It is also possible to show the name of the criteria instead of the id, etc. The criteria evaluations are supposed to be real or integer numeric values.

- **Contact:** Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)



Inputs
------
(For outputs, see :ref:`below <plotCriteriaValuesPreorder-ITTB_outputs>`)


- :ref:`criteria <plotCriteriaValuesPreorder-ITTB-criteria>`
- :ref:`criteriaValues <plotCriteriaValuesPreorder-ITTB-criteriaValues>`
- :ref:`parameters <plotCriteriaValuesPreorder-ITTB-parameters>`

.. _plotCriteriaValuesPreorder-ITTB-criteria:

criteria
~~~~~~~~


Description:
............

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
	<criteria>
		<criterion>
			<active>[...]</active>
			[...]
		</criterion>
		[...]
	</criteria>


----------------------------


.. _plotCriteriaValuesPreorder-ITTB-criteriaValues:

criteriaValues
~~~~~~~~~~~~~~


Description:
............

A list of <criterionValue> representing a certain numeric quantity for each criterion, like, e.g., an importance value.



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _plotCriteriaValuesPreorder-ITTB-parameters:

parameters
~~~~~~~~~~


Description:
............

Generates a graph taking into account the proposed options.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** Chart title:

  *String for the title of the graph. The default value is an empty field.*

  - **Type:** string
  - **Default value:** ""
- **Name:** Order:

  *Increasing or decreasing. The default value is decreasing.*

  - **Type:** drop-down list
  - **Possible values:**
      - Ascending (XMCDA label : increasing) 

      - Descending (XMCDA label : decreasing) (default)

- **Name:** Shape of the nodes?

  *Choose between rectangle, square, ellipse, circle or diamond.*

  - **Type:** drop-down list
  - **Possible values:**
      - Rectangle (XMCDA label : Rectangle) (default)

      - Square (XMCDA label : Square) 

      - Ellipse (XMCDA label : Ellipse) 

      - Circle (XMCDA label : Circle) 

      - Diamond (XMCDA label : Diamond) 

- **Name:** Criterion name or id?

  *Display criteria names ot IDs.*

  - **Type:** drop-down list
  - **Possible values:**
      - name (XMCDA label : true) 

      - id (XMCDA label : false) (default)

- **Name:** Use colors?

  *The use of colors: true for a colored graph.*

  - **Type:** drop-down list
  - **Possible values:**
      - Yes (XMCDA label : true) 

      - No (XMCDA label : false) (default)

- **Name:** Choose color:

  *String that indicates the color of the graph.Choose between "Black", "Red", "Blue", "Green", "Yellow", "Magenta" and "Cyan".*

  - **Type:** drop-down list
  - **Possible values:**
      - Black (XMCDA label : Black) (default)

      - Red (XMCDA label : Red) 

      - Blue (XMCDA label : Blue) 

      - Green (XMCDA label : Green) 

      - Yellow (XMCDA label : Yellow) 

      - Magenta (XMCDA label : Magenta) 

      - Cyan (XMCDA label : Cyan) 


XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
	<programParameters>
		  <parameter id="plot_title" name="Plot Title">
			<values>
				<value>
					<label>%1</label>
				</value>
			</values>
		</parameter>
		<parameter id="plot_order" name="Plot order">
			<values>
				<value>
					<label>%2</label>
				</value>
			</values>
		</parameter>
		<parameter id="node_shape" name="Node shape">
			<values>
				<value>
					<label>%3</label>
				</value>
			</values>
		</parameter>
		<parameter id="show_names" name="Show criteria names">
			<values>
				<value>
					<label>%4</label>
				</value>
			</values>
		</parameter>
		<parameter id="use_color" name="Colors in the graph">
			<values>
				<value>
					<label>%5</label>
				</value>
			</values>
		</parameter>
		<parameter id="selected_color" name="Selected color">
			<values>
				<value>
					<label>%6</label>
				</value>
			</values>
		</parameter >
	</programParameters>


----------------------------



.. _plotCriteriaValuesPreorder-ITTB_outputs:

Outputs
-------


- :ref:`criteriaValuesPreorder.dot <plotCriteriaValuesPreorder-ITTB-dot>`
- :ref:`criteriaValuesPreorder.png <plotCriteriaValuesPreorder-ITTB-png>`
- :ref:`messages <plotCriteriaValuesPreorder-ITTB-messages>`

.. _plotCriteriaValuesPreorder-ITTB-dot:

criteria value spreorder (dot)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The dot file used to generate the PNG image.



XMCDA related:
..............

- **Tag:** other

----------------------------


.. _plotCriteriaValuesPreorder-ITTB-png:

criteria value spreorder (png)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The preorder as a PNG image.



XMCDA related:
..............

- **Tag:** other

----------------------------


.. _plotCriteriaValuesPreorder-ITTB-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
