:orphan:



.. _PROMETHEE_veto-PUT:

PrometheeVeto
=============

:Provider: PUT
:Version: 1.0.0

Description
-----------

Computes veto indices which
The key feature of this module is its flexibility in terms of the types of
elements allowed to compare, i.e. alternatives vs alternatives, alternatives vs
boundary profiles and alternatives vs central (characteristic) profiles.

- **Web page:** https://github.com/Yamadads/PrometheeDiviz



Inputs
------
(For outputs, see :ref:`below <PROMETHEE_veto-PUT_outputs>`)


- :ref:`criteria <PROMETHEE_veto-PUT-input1>`
- :ref:`alternatives <PROMETHEE_veto-PUT-input2>`
- :ref:`performance_table <PROMETHEE_veto-PUT-input3>`
- :ref:`profiles_performance_table <PROMETHEE_veto-PUT-input4>` *(optional)*
- :ref:`criteria_scales <PROMETHEE_veto-PUT-input5>`
- :ref:`criteria_thresholds <PROMETHEE_veto-PUT-input6>`
- :ref:`weights <PROMETHEE_veto-PUT-input7>` *(optional)*
- :ref:`categories_profiles <PROMETHEE_veto-PUT-input8>` *(optional)*
- :ref:`method_parameters <PROMETHEE_veto-PUT-input9>`

.. _PROMETHEE_veto-PUT-input1:

criteria
~~~~~~~~


Description:
............

Criteria to consider.



XMCDA related:
..............

- **Tag:** criteria

----------------------------


.. _PROMETHEE_veto-PUT-input2:

alternatives
~~~~~~~~~~~~


Description:
............

Alternatives to consider.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _PROMETHEE_veto-PUT-input3:

performance table
~~~~~~~~~~~~~~~~~


Description:
............

The performance of alternatives.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _PROMETHEE_veto-PUT-input4:

profiles performance table
~~~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The performance of profiles (boundary or central).



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _PROMETHEE_veto-PUT-input5:

criteria scales
~~~~~~~~~~~~~~~


Description:
............

Preference direction (min or max) specified for each criterion.



XMCDA related:
..............

- **Tag:** criteriaScales

----------------------------


.. _PROMETHEE_veto-PUT-input6:

criteria thresholds
~~~~~~~~~~~~~~~~~~~


Description:
............

Preference, indifference and sigma thresholds for criteria (as constants as well as linear functions). Gaussian function needs inflection point (sigma), rest of functions need preference or indifference thresholds.



XMCDA related:
..............

- **Tag:** criteriaThresholds

----------------------------


.. _PROMETHEE_veto-PUT-input7:

weights
~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Weights of criteria to consider.



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _PROMETHEE_veto-PUT-input8:

categories profiles
~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Definitions of central or boundary profiles connected with classes (categories)



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _PROMETHEE_veto-PUT-input9:

method parameters
~~~~~~~~~~~~~~~~~


Description:
............

First parameter specifies the type of elements provided for comparison.

Choosing 'boundary_profiles' or 'central_profiles' requires providing inputs 'classes_profiles' and 'profiles_performance_table' as well (which are optional by default).

Second parameter specifies the type of function used for comparison of each criterion.
Choosing 'specified' requires providing inputs "generalised_criterion" which is optional by default.
Choosing some of numbers sets same function for all criteria.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** comparison with

  

  - **Type:** drop-down list
  - **Possible values:**
      - alternatives vs alternatives (XMCDA label : alternatives) (default)

      - alternatives vs boundary profiles (XMCDA label : boundary_profiles) 

      - alternatives vs central (characteristic) profiles (XMCDA label : central_profiles) 

- **Name:** weights specified

  

  - **Type:** drop-down list
  - **Possible values:**
      - Total veto coeficient is weighted average of veto on each criterion (XMCDA label : specified) (default)

      - Weights not specified, if veto on one criterion then veto equals 1.0 (XMCDA label : not_specified) 


XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
        
        <programParameters>
		<parameter id="comparison_with" name="comparison_with">
			<values>
				<value>
					<label>%1</label>
				</value>
			</values>
		</parameter>
		<parameter id="weights_specified" name="weights_specified">
			<values>
				<value>
					<label>%2</label>
				</value>
			</values>
		</parameter>
		</programParameters>
        
      

----------------------------



.. _PROMETHEE_veto-PUT_outputs:

Outputs
-------


- :ref:`veto <PROMETHEE_veto-PUT-output1>`
- :ref:`partial_veto <PROMETHEE_veto-PUT-output2>`
- :ref:`messages <PROMETHEE_veto-PUT-output3>`

.. _PROMETHEE_veto-PUT-output1:

veto
~~~~


Description:
............

Aggregated veto matrix computed from the given data. This matrix aggregates partial veto indices from all criteria into single veto index per pair of alternatives or alternatives/profiles.



XMCDA related:
..............

- **Tag:** alternativesMatrix

----------------------------


.. _PROMETHEE_veto-PUT-output2:

partial veto
~~~~~~~~~~~~


Description:
............

Veto matrix computed from the given data. This matrix shows partial veto indices for all criteria, for all pairs of alternatives or alternatives/profiles.



XMCDA related:
..............

- **Tag:** alternativesMatrix

----------------------------


.. _PROMETHEE_veto-PUT-output3:

messages
~~~~~~~~


Description:
............

Messages or errors generated by this module.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
