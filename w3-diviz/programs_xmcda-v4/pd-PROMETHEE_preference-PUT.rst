:orphan:



.. _PROMETHEE_preference-PUT:

PrometheePreference
===================

:Provider: PUT
:Version: 1.0.0

Description
-----------

Computes aggregated preference indices
The key feature of this module is its flexibility in terms of the types of
elements allowed to compare, i.e. alternatives vs alternatives, alternatives vs
boundary profiles and alternatives vs central (characteristic) profiles.
Each criterion can have its own preference function (one of six predefined functions).

- **Web page:** https://github.com/Yamadads/PrometheeDiviz



Inputs
------
(For outputs, see :ref:`below <PROMETHEE_preference-PUT_outputs>`)


- :ref:`criteria <PROMETHEE_preference-PUT-input1>`
- :ref:`alternatives <PROMETHEE_preference-PUT-input2>`
- :ref:`performance_table <PROMETHEE_preference-PUT-input3>`
- :ref:`profiles_performance_table <PROMETHEE_preference-PUT-input4>` *(optional)*
- :ref:`criteria_scales <PROMETHEE_preference-PUT-input5>`
- :ref:`criteria_thresholds <PROMETHEE_preference-PUT-input6>`
- :ref:`weights <PROMETHEE_preference-PUT-input7>`
- :ref:`generalised_criteria <PROMETHEE_preference-PUT-input8>` *(optional)*
- :ref:`categories_profiles <PROMETHEE_preference-PUT-input9>` *(optional)*
- :ref:`method_parameters <PROMETHEE_preference-PUT-input10>`

.. _PROMETHEE_preference-PUT-input1:

criteria
~~~~~~~~


Description:
............

Criteria to consider.



XMCDA related:
..............

- **Tag:** criteria

----------------------------


.. _PROMETHEE_preference-PUT-input2:

alternatives
~~~~~~~~~~~~


Description:
............

Alternatives to consider.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _PROMETHEE_preference-PUT-input3:

performance table
~~~~~~~~~~~~~~~~~


Description:
............

The performance of alternatives.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _PROMETHEE_preference-PUT-input4:

profiles performance table
~~~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The performance of profiles (boundary or central).



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _PROMETHEE_preference-PUT-input5:

criteria scales
~~~~~~~~~~~~~~~


Description:
............

Preference direction (min or max) specified for each criterion.



XMCDA related:
..............

- **Tag:** criteriaScales

----------------------------


.. _PROMETHEE_preference-PUT-input6:

criteria thresholds
~~~~~~~~~~~~~~~~~~~


Description:
............

Preference, indifference and sigma thresholds for criteria (as constants as well as linear functions). Gaussian function needs inflection point (sigma), rest of functions need preference or indifference thresholds.



XMCDA related:
..............

- **Tag:** criteriaThresholds

----------------------------


.. _PROMETHEE_preference-PUT-input7:

weights
~~~~~~~


Description:
............

Weights of criteria to consider.



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _PROMETHEE_preference-PUT-input8:

generalised criteria
~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

ID number of predefined preference function specified for each criterion.



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _PROMETHEE_preference-PUT-input9:

categories profiles
~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Definitions of central or boundary profiles connected with classes (categories)



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _PROMETHEE_preference-PUT-input10:

method parameters
~~~~~~~~~~~~~~~~~


Description:
............

First parameter specifies the type of elements provided for comparison.

Choosing 'boundary_profiles' or 'central_profiles' requires providing inputs 'classes_profiles' and 'profiles_performance_table' as well (which are optional by default).

Second parameter specifies the type of function used for comparison of each criterion.
Choosing 'specified' requires providing inputs "generalised_criterion" which is optional by default.
Choosing some of numbers sets same function for all criteria.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** comparison with

  

  - **Type:** drop-down list
  - **Possible values:**
      - alternatives vs alternatives (XMCDA label : alternatives) (default)

      - alternatives vs boundary profiles (XMCDA label : boundary_profiles) 

      - alternatives vs central (characteristic) profiles (XMCDA label : central_profiles) 

- **Name:** generalised criterion

  

  - **Type:** drop-down list
  - **Possible values:**
      - Each criterion needs its own function (XMCDA label : specified) (default)

      - Usual Criterion (XMCDA label : usual) 

      - U-Shape Criterion, needs indifference threshold specified in criterion. (XMCDA label : u-shape) 

      - V-Shape Criterion, needs threshold of strict preference specified in criterion. (XMCDA label : v-shape) 

      - Level Criterion, needs both indifference and strict preference thresholds specified in criterion. (XMCDA label : level) 

      - V-Shape with Indifference Criterion, needs both indifference and strict preference thresholds specified in criterion. (XMCDA label : v-shape-ind) 

      - Gaussian Criterion, needs the inflection point of the preference function specified in criterion. (XMCDA label : gaussian) 


XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
        
		<programParameters>
			<parameter id="comparison_with" name="comparison_with">
				<values>
					<value>
						<label>%1</label>
					</value>
				</values>
			</parameter>
			<parameter id="generalised_criterion" name="generalised_criterion">
				<values>
					<value>
						<label>%2</label>
					</value>
				</values>
			</parameter>
		</programParameters>
        
      

----------------------------



.. _PROMETHEE_preference-PUT_outputs:

Outputs
-------


- :ref:`preferences <PROMETHEE_preference-PUT-output1>`
- :ref:`partial_preferences <PROMETHEE_preference-PUT-output2>`
- :ref:`messages <PROMETHEE_preference-PUT-output3>`

.. _PROMETHEE_preference-PUT-output1:

preferences
~~~~~~~~~~~


Description:
............

Aggregated preference matrix computed from the given data. This matrix aggregates partial preference indices from all criteria into single preference index per pair of alternatives or alternatives/profiles.



XMCDA related:
..............

- **Tag:** alternativesMatrix

----------------------------


.. _PROMETHEE_preference-PUT-output2:

partial preferences
~~~~~~~~~~~~~~~~~~~


Description:
............

Preference matrix computed from the given data. This matrix contains partial preference indices for all criteria and all pairs of alternatives or alternatives/profiles.



XMCDA related:
..............

- **Tag:** alternativesMatrix

----------------------------


.. _PROMETHEE_preference-PUT-output3:

messages
~~~~~~~~


Description:
............

Messages or errors generated by this module.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
