:orphan:



.. _RORUTA-PreferentialReductsForNecessaryRelationsHierarchical-PUT:

RORUTA-PreferentialReductsForNecessaryRelationsHierarchical
===========================================================

:Provider: PUT
:Version: 1.0

Description
-----------

The module finds all preferential reducts for the necessary relations. In other words, for each necessary weak preference relation, it finds all minimal sets of pairwise comparisons that imply this relation. Function supports a hierarchical decomposition of the problem.

- **Contact:** Pawel Rychly (pawelrychly@gmail.com).



Inputs
------
(For outputs, see :ref:`below <RORUTA-PreferentialReductsForNecessaryRelationsHierarchical-PUT_outputs>`)


- :ref:`criteria <RORUTA-PreferentialReductsForNecessaryRelationsHierarchical-PUT-criteria>`
- :ref:`alternatives <RORUTA-PreferentialReductsForNecessaryRelationsHierarchical-PUT-alternatives>`
- :ref:`hierarchy-of-criteria <RORUTA-PreferentialReductsForNecessaryRelationsHierarchical-PUT-hierarchy-of-criteria>`
- :ref:`performances <RORUTA-PreferentialReductsForNecessaryRelationsHierarchical-PUT-performances>`
- :ref:`characteristic-points <RORUTA-PreferentialReductsForNecessaryRelationsHierarchical-PUT-characteristic-points>` *(optional)*
- :ref:`criteria-preference-directions <RORUTA-PreferentialReductsForNecessaryRelationsHierarchical-PUT-criteria-preference-directions>` *(optional)*
- :ref:`necessary-relations-hierarchical <RORUTA-PreferentialReductsForNecessaryRelationsHierarchical-PUT-necessary-relations-hierarchical>`
- :ref:`preferences <RORUTA-PreferentialReductsForNecessaryRelationsHierarchical-PUT-preferences>` *(optional)*
- :ref:`parameters <RORUTA-PreferentialReductsForNecessaryRelationsHierarchical-PUT-parameters>`

.. _RORUTA-PreferentialReductsForNecessaryRelationsHierarchical-PUT-criteria:

criteria
~~~~~~~~


Description:
............

A list of all considered criteria. The input value should be a valid XMCDA document whose main tag is criteria.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
                
                    <criteria>
                        <criterion id="%1" name="%1"></criterion>
                        [...]
                    </criteria>
                    
            

----------------------------


.. _RORUTA-PreferentialReductsForNecessaryRelationsHierarchical-PUT-alternatives:

alternatives
~~~~~~~~~~~~


Description:
............

The list of all considered alternatives. The input value should be a valid XMCDA document whose main tag is alternatives. Each alternative may be described using two attributes: id and name. While the first one denotes a machine readable name, the second represents a human readable name.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
                
                    <alternatives>
                        <alternative id="%1" name="%2" />
                        [...]
                    </alternatives>
                    
            

----------------------------


.. _RORUTA-PreferentialReductsForNecessaryRelationsHierarchical-PUT-hierarchy-of-criteria:

hierarchy of criteria
~~~~~~~~~~~~~~~~~~~~~


Description:
............

Description of the hierarchical structure of criteria. Each node of this hierarchy needs to have a unique id attribute. The most nested nodes, should contain a set of criteria. The input value should be provided as a valid XMCDA document whose main tag is hierarchy



XMCDA related:
..............

- **Tag:** hierarchy

- **Code:**

  ::

    
                
                    <hierarchy>
                        <node id="nodes">
                            <node id="nodes1">
                                <criteriaSet>
                                    <element><criterionID>%1</criterionID></element> [...]
                                </criteriaSet>
                            </node>
                            [...]
                        </node>
                        [...]
                    </hierarchy>
                    
            

----------------------------


.. _RORUTA-PreferentialReductsForNecessaryRelationsHierarchical-PUT-performances:

performances
~~~~~~~~~~~~


Description:
............

Description of evaluation of alternatives on different criteria. It is required to provide the IDs of both criteria and alternatives described previously. The input value should be provided as a valid XMCDA document whose main tag is performanceTable



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    
                
                    <performanceTable>
                        <alternativePerformances>
                            <alternativeID>%1</alternativeID>
                            <performance>
                                <criterionID>%2</criterionID>
                                <value>
                                    <real>%3</real>
                                </value>
                            </performance>
                            [...]
                        </alternativePerformances>
                        [...]
                    </performanceTable>
                    
            

----------------------------


.. _RORUTA-PreferentialReductsForNecessaryRelationsHierarchical-PUT-characteristic-points:

characteristic points
~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A set of values associated with the criteria. This input allows to determine what type of value function should be used for the particular criterion. For each criterion that has an associated greater than one value, a piecewise linear value function is used. In this case, the mentioned value denotes a number of characteristic points of this value function. For the criteria that are not listed in this file, or for these for which the provided values are lower than two uses a general value function. The input value should be provided as a valid XMCDA document whose main tag is criteriaValues. Each element should contain both an id of the criterion, and value tag.



XMCDA related:
..............

- **Tag:** criteriaValues

- **Code:**

  ::

    
                
                     <criteriaValues>
                        <criterionValue>
                            <criterionID>%1</criterionID>
                            <value>
                                <integer>%2</integer>
                            </value>
                        </criterionValue>
                        [...]
                    </criteriaValues>
                    
            

----------------------------


.. _RORUTA-PreferentialReductsForNecessaryRelationsHierarchical-PUT-criteria-preference-directions:

criteria preference directions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A set of values associated with criteria that determine their preference direction (0 - gain, 1 - cost).



XMCDA related:
..............

- **Tag:** criteriaValues

- **Code:**

  ::

    
                
                     <criteriaValues mcdaConcept="preferenceDirection">
                        <criterionValue>
                            <criterionID>%1</criterionID>
                            <value>
                                <integer>%2</integer>
                            </value>
                        </criterionValue>
                        [...]
                    </criteriaValues>
                    
            

----------------------------


.. _RORUTA-PreferentialReductsForNecessaryRelationsHierarchical-PUT-necessary-relations-hierarchical:

necessary relations hierarchical
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

A list of all necessary weak preference relations on the set of alternatives. The input value should be a valid XMCDA document whose main tag is alternativesComparisons. Each relation should be denoted as a pair of alternativesID. Each alternativesComparisons tag should describe an another node of the hierarchy tree marked in its id attribute.



XMCDA related:
..............

- **Tag:** alternativesComparisons

- **Code:**

  ::

    
                
                <alternativesComparisons id=%1>
                    <pairs>
                      <pair>
                        <initial>
                          <alternativeID>%2</alternativeID>
                        </initial>
                        <terminal>
                          <alternativeID>%3</alternativeID>
                        </terminal>
                      </pair>
                      [...]
                    </pairs>
                </alternativesComparisons>
                
            

----------------------------


.. _RORUTA-PreferentialReductsForNecessaryRelationsHierarchical-PUT-preferences:

preferences
~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Set of pairwise comparisons of reference alternatives. For a pair of alternatives three types of comparisons are supported. These are the strict preference, weak preference, and indifference. Values linked to pairs indicate  ids of nodes in the hierarchy of criteria tree. If value is not given or if it is equal to 0 pairwise comparison is assumed to concern for the whole set of criteria. Otherwise, the preference relation applies only to a particular node. The input value should be provided as a valid XMCDA document whose main tag is alternativesComparisons. For each type of comparison, a separate alternativesComparisons tag should be used. Within these groups a mentioned types are denoted using a comparisonType tag by respectively strict, weak, and indif label. Comparisons should be provided as pairs of alternatives ids.



XMCDA related:
..............

- **Tag:** alternativesComparisons

- **Code:**

  ::

    
                
                    <alternativesComparisons>
                        <comparisonType>
                            %1<!-- type of preference: strong, weak, or indif -->
                        </comparisonType>
                        <pairs>
                            <pair>
                                <initial>
                                    <alternativeID>%2</alternativeID>
                                </initial>
                                <terminal>
                                    <alternativeID>%3</alternativeID>
                                </terminal>
                                <value>
                                    <label>%4</label>
                                </value>
                            </pair>
                            [...]
                        </pairs>
                    </alternativesComparisons>
                    
            

----------------------------


.. _RORUTA-PreferentialReductsForNecessaryRelationsHierarchical-PUT-parameters:

parameters
~~~~~~~~~~


Description:
............

Single boolean value. Determines whether to use sctrictly increasing (true) or monotonously increasing (false) value functions



GUI information:
................


- **Name:** Use strictly increasing value functions?

  

  - **Default value:** false

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                
                     <methodParameters>
                        <parameter name="strict">
                            <value>
                                <boolean>%1</boolean>
                            </value>
                        </parameter>
                    </methodParameters>
                    
            

----------------------------



.. _RORUTA-PreferentialReductsForNecessaryRelationsHierarchical-PUT_outputs:

Outputs
-------


- :ref:`reducts-by-necessary-relations-hierarchical <RORUTA-PreferentialReductsForNecessaryRelationsHierarchical-PUT-reducts-by-necessary-relations-hierarchical>`
- :ref:`messages <RORUTA-PreferentialReductsForNecessaryRelationsHierarchical-PUT-messages>`

.. _RORUTA-PreferentialReductsForNecessaryRelationsHierarchical-PUT-reducts-by-necessary-relations-hierarchical:

reducts by necessary relations hierarchical
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

list of all necessary relations and underlying preferential reducts. The returned value is a valid XMCDA document whose main tag is the alternativesComparisons. Each element of this data, contains a pair of alternatives ids related by the necessary relation. Each necessary relation has an associated set of string values that describe a different possible minimal sets of pairwise comparisons which imply it. Each of these values is a comma separated list of relations. Each comparison is described as a pair of ids separated by one of the three possible labels: strong, weak, and indif. These abbreviations denote respectively a strong preference, weak preference or indifference between mentioned alternatives. Each alternativesComparisons group describes an another node of hierarchy tree marked in its id attribute.



XMCDA related:
..............

- **Tag:** alternativesComparisons

- **Code:**

  ::

    
                
                    <alternativesComparisons id="%1">
                        <pairs>
                          <pair>
                            <initial>
                              <alternativeID>%1</alternativeID>
                            </initial>
                            <terminal>
                              <alternativeID>%2</alternativeID>
                            </terminal>
                            <values>
                                <value><label>%3</label></value>
                                [...]
                            </values>
                          </pair>
                          [...]
                      </pairs>
                    </alternativesComparisons>
                    
            

----------------------------


.. _RORUTA-PreferentialReductsForNecessaryRelationsHierarchical-PUT-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
