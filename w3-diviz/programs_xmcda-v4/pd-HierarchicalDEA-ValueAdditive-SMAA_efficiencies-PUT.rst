:orphan:



.. _HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT:

HierarchicalDEA-ValueAdditive-SMAA_efficiencies
===============================================

:Provider: PUT
:Version: 1.0

Description
-----------

Computes efficiency scores for the given DMUs (alternatives) using SMAA-D method and A Data Envelopment Analysis Model with hierarchical structure of outputs. For given number of buckets and samples, returns a matrix with alternatives in each row and buckets representing efficiency intervals in each column. Single cell indicates how many samples gave efficiency scores of respective alternative in respective bucket.

- **Contact:** 
            Anna Labijak <anna.labijak@cs.put.poznan.pl>
        



Inputs
------
(For outputs, see :ref:`below <HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT_outputs>`)


- :ref:`units <HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-units>`
- :ref:`inputsOutputsScales <HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-inputsOutputsScales>`
- :ref:`performanceTable <HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-performanceTable>`
- :ref:`hierarchy <HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-hierarchy>`
- :ref:`weightsLinearConstraints <HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-methodParameters>`

.. _HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-units:

units
~~~~~


Description:
............

A list of alternatives (DMUs).



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
                <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
            

----------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-inputsOutputsScales:

inputs/outputs scales
~~~~~~~~~~~~~~~~~~~~~


Description:
............

Information about inputs and outpus (leaf criteria) scales (preference directions) and optionally about boundaries



XMCDA related:
..............

- **Tag:** criteriaScales

- **Code:**

  ::

    
                
<criteriaScales>
    <criterionScale>
      <criterionID>[...]</criterionID>
      <scales>
        <scale>
          [...]
        </scale>
      </scales>
    </criterionScale>
    [...]
</criteriaScales>
            

----------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-performanceTable:

performance
~~~~~~~~~~~


Description:
............

A list of alternatives (DMUs) performances (on leaf hierarchy criteria).



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    
                <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<values><value>[...]</value></values>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>
            

----------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-hierarchy:

hierarchy
~~~~~~~~~


Description:
............

The hierarchical structure of criteria.



XMCDA related:
..............

- **Tag:** criteriaHierarchy

- **Code:**

  ::

    
                <criteriaHierarchy>
						<nodes>
                            <node>
                                <criterionID>[...]</criterionID>
                                <nodes>
                                    <node>
                                        <criterionID>[...]</criterionID>
                                        [...]
                                    </node>
                                    [...]
                                </nodes>
                            </node>
                        <nodes>
					</criteriaHierarchy>
            

----------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-weightsLinearConstraints:

weights constraints
~~~~~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

A list of hierarchy criteria linear constraints.



XMCDA related:
..............

- **Tag:** criteriaLinearConstraints

- **Code:**

  ::

    
                
            <criteriaLinearConstraints>
                <constraints>
                    <constraint>
                        <elements>
                            <element>
                                <criterionID> [...] </criterionID>
                                <coefficient>
                                    [...]
                                </coefficient>
                            </element>
                            [...]
                        </elements>
                        <operator>[...]</operator>
                        <rhs>
                            [...]
                        </rhs>
                    </constraint>
                    [...]
                </constraints>
            </criteriaLinearConstraints>
            

----------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-methodParameters:

parameters
~~~~~~~~~~


Description:
............

Represents method parameters.
                "number of samples" represents the number of samples to generate;
                "number of buckets" represents the number of buckets which efficiency scores will be assigned to;
                "hierarchy node" is the ID of the hierarchy criterion for which the analysis should be performed;
                "transformToUtilities" means if data should be tranformed into values from range [0-1];
                "boundariesProvided" means if inputsOutputs file contains information about min and max data for each factor.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** number of samples

  

  - **Constraint description:** The value should be a positive integer.

  - **Type:** integer
  - **Default value:** 100
- **Name:** number of buckets

  

  - **Constraint description:** The value should be a positive integer.

  - **Type:** integer
  - **Default value:** 10
- **Name:** hierarchy node

  

  - **Type:** string
  - **Default value:** "root"
- **Name:** transform to utilities

  

  - **Default value:** true
- **Name:** boundaries provided

  

  - **Default value:** false
- **Name:** random seed (-1 for default time-based seed)

  

  - **Constraint description:** The value should be a non-negative integer or -1 if no constant seed required.

  - **Type:** integer
  - **Default value:** -1

XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
                
    <programParameters>
        <parameter id="samplesNb">
            <values>
                <value><integer>%1</integer></value>
            </values>
        </parameter>
        <parameter id="intervalsNb">
            <values>
                <value><integer>%2</integer></value>
            </values>
        </parameter>
        <parameter id="hierarchyNode">
            <values>
                <value><label>%3</label></value>
            </values>
        </parameter>
        <parameter id="transformToUtilities">
      <values>
        <value><boolean>%4</boolean></value>
      </values>
		</parameter>
		<parameter id="boundariesProvided">
      <values>
        <value><boolean>%5</boolean></value>
      </values>
		</parameter>
      <parameter id="randomSeed">
            <values>
                <value><integer>%6</integer></value>
            </values>
        </parameter>
    </programParameters>
            

----------------------------



.. _HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT_outputs:

Outputs
-------


- :ref:`efficiencyDistribution <HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-efficiencyDistribution>`
- :ref:`maxEfficiency <HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-maxEfficiency>`
- :ref:`minEfficiency <HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-minEfficiency>`
- :ref:`avgEfficiency <HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-avgEfficiency>`
- :ref:`messages <HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-messages>`

.. _HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-efficiencyDistribution:

efficiency distribution
~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

A performance table for given alternatives. Single performance consists of attribute criterionID representing certain bucket, and a value representing the ratio of efficiency scores in this bucket.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    
                <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>[...]</criterionID>
									<values>
                                        <value>[...]</value>
                                    </values>
							</performance>
							[...]
						</alternativePerformances>
                        [...]
					</performanceTable>
            

----------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-maxEfficiency:

max efficiency
~~~~~~~~~~~~~~


Description:
............

A list of alternatives with maximum efficiency scores (obtained with sampling).



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
                <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
                          <values>
                                <value>
                                    [...]
                                </value>
                          </values>
						</alternativeValue>
						[...]
					</alternativesValues>
            

----------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-minEfficiency:

min efficiency
~~~~~~~~~~~~~~


Description:
............

A list of alternatives with computed minimum efficiency scores (obtained with sampling).



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
                <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
                          <values>
                                <value>
                                    [...]
                                </value>
                          </values>
						</alternativeValue>
						[...]
					</alternativesValues>
            

----------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-avgEfficiency:

average efficiency
~~~~~~~~~~~~~~~~~~


Description:
............

A list of alternatives with average efficiency scores (obtained with sampling).



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
                <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
                          <values>
                                <value>
                                    [...]
                                </value>
                          </values>
						</alternativeValue>
						[...]
					</alternativesValues>
            

----------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
