:orphan:



.. _RORUTA-RepresentativeValueFunctionHierarchical-PUT:

RORUTA-RepresentativeValueFunctionHierarchical
==============================================

:Provider: PUT
:Version: 1.0

Description
-----------

The procedure finds a representative value function for a given set of necessary relations. It supports the hierarchical decomposition of the problem.

- **Contact:** Pawel Rychly (pawelrychly@gmail.com).



Inputs
------
(For outputs, see :ref:`below <RORUTA-RepresentativeValueFunctionHierarchical-PUT_outputs>`)


- :ref:`criteria <RORUTA-RepresentativeValueFunctionHierarchical-PUT-criteria>`
- :ref:`alternatives <RORUTA-RepresentativeValueFunctionHierarchical-PUT-alternatives>`
- :ref:`hierarchy-of-criteria <RORUTA-RepresentativeValueFunctionHierarchical-PUT-hierarchy-of-criteria>`
- :ref:`performances <RORUTA-RepresentativeValueFunctionHierarchical-PUT-performances>`
- :ref:`characteristic-points <RORUTA-RepresentativeValueFunctionHierarchical-PUT-characteristic-points>` *(optional)*
- :ref:`criteria-preference-directions <RORUTA-RepresentativeValueFunctionHierarchical-PUT-criteria-preference-directions>` *(optional)*
- :ref:`necessary-relations-hierarchical <RORUTA-RepresentativeValueFunctionHierarchical-PUT-necessary-relations-hierarchical>`
- :ref:`preferences <RORUTA-RepresentativeValueFunctionHierarchical-PUT-preferences>` *(optional)*
- :ref:`intensities-of-preferences <RORUTA-RepresentativeValueFunctionHierarchical-PUT-intensities-of-preferences>` *(optional)*
- :ref:`rank-related-requirements <RORUTA-RepresentativeValueFunctionHierarchical-PUT-rank-related-requirements>` *(optional)*
- :ref:`parameters <RORUTA-RepresentativeValueFunctionHierarchical-PUT-parameters>`

.. _RORUTA-RepresentativeValueFunctionHierarchical-PUT-criteria:

criteria
~~~~~~~~


Description:
............

A list of all considered criteria. The input value should be a valid XMCDA document whose main tag is criteria.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
                
                    <criteria>
                        <criterion id="%1" name="%1"></criterion>
                        [...]
                    </criteria>
                    
            

----------------------------


.. _RORUTA-RepresentativeValueFunctionHierarchical-PUT-alternatives:

alternatives
~~~~~~~~~~~~


Description:
............

The list of all considered alternatives. The input value should be a valid XMCDA document whose main tag is alternatives. Each alternative may be described using two attributes: id and name. While the first one denotes a machine readable name, the second represents a human readable name.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
                
                    <alternatives>
                        <alternative id="%1" name="%2" />
                        [...]
                    </alternatives>
                    
            

----------------------------


.. _RORUTA-RepresentativeValueFunctionHierarchical-PUT-hierarchy-of-criteria:

hierarchy of criteria
~~~~~~~~~~~~~~~~~~~~~


Description:
............

Description of the hierarchical structure of criteria. Each node of this hierarchy needs to have a unique id attribute. The most nested nodes, should contain a set of criteria. The input value should be provided as a valid XMCDA document whose main tag is hierarchy



XMCDA related:
..............

- **Tag:** hierarchy

- **Code:**

  ::

    
                
                    <hierarchy>
                        <node id="nodes">
                            <node id="nodes1">
                                <criteriaSet>
                                    <element><criterionID>%1</criterionID></element> [...]
                                </criteriaSet>
                            </node>
                            [...]
                        </node>
                        [...]
                    </hierarchy>
                    
            

----------------------------


.. _RORUTA-RepresentativeValueFunctionHierarchical-PUT-performances:

performances
~~~~~~~~~~~~


Description:
............

Description of evaluation of alternatives on different criteria. It is required to provide the IDs of both criteria and alternatives described previously. The input value should be provided as a valid XMCDA document whose main tag is performanceTable



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    
                
                    <performanceTable>
                        <alternativePerformances>
                            <alternativeID>%1</alternativeID>
                            <performance>
                                <criterionID>%2</criterionID>
                                <value>
                                    <real>%3</real>
                                </value>
                            </performance>
                            [...]
                        </alternativePerformances>
                        [...]
                    </performanceTable>
                    
            

----------------------------


.. _RORUTA-RepresentativeValueFunctionHierarchical-PUT-characteristic-points:

characteristic points
~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A set of values associated with the criteria. This input allows to determine what type of value function should be used for the particular criterion. For each criterion that has an associated greater than one value, a piecewise linear value function is used. In this case, the mentioned value denotes a number of characteristic points of this value function. For the criteria that are not listed in this file, or for these for which the provided values are lower than two uses a general value function. The input value should be provided as a valid XMCDA document whose main tag is criteriaValues. Each element should contain both an id of the criterion, and value tag.



XMCDA related:
..............

- **Tag:** criteriaValues

- **Code:**

  ::

    
                
                     <criteriaValues>
                        <criterionValue>
                            <criterionID>%1</criterionID>
                            <value>
                                <integer>%2</integer>
                            </value>
                        </criterionValue>
                        [...]
                    </criteriaValues>
                    
            

----------------------------


.. _RORUTA-RepresentativeValueFunctionHierarchical-PUT-criteria-preference-directions:

criteria preference directions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A set of values associated with criteria that determine their preference direction (0 - gain, 1 - cost).



XMCDA related:
..............

- **Tag:** criteriaValues

- **Code:**

  ::

    
                
                     <criteriaValues mcdaConcept="preferenceDirection">
                        <criterionValue>
                            <criterionID>%1</criterionID>
                            <value>
                                <integer>%2</integer>
                            </value>
                        </criterionValue>
                        [...]
                    </criteriaValues>
                    
            

----------------------------


.. _RORUTA-RepresentativeValueFunctionHierarchical-PUT-necessary-relations-hierarchical:

necessary relations hierarchical
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

A list of all necessary weak preference relations on the set of alternatives. The input value should be a valid XMCDA document whose main tag is alternativesComparisons. Each relation should be denoted as a pair of alternativesID. Each alternativesComparisons tag should describe an another node of the hierarchy tree marked in its id attribute.



XMCDA related:
..............

- **Tag:** alternativesComparisons

- **Code:**

  ::

    
                
                <alternativesComparisons id=%1>
                    <pairs>
                      <pair>
                        <initial>
                          <alternativeID>%2</alternativeID>
                        </initial>
                        <terminal>
                          <alternativeID>%3</alternativeID>
                        </terminal>
                      </pair>
                      [...]
                    </pairs>
                </alternativesComparisons>
                
            

----------------------------


.. _RORUTA-RepresentativeValueFunctionHierarchical-PUT-preferences:

preferences
~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Set of pairwise comparisons of reference alternatives. For a pair of alternatives three types of comparisons are supported. These are the strict preference, weak preference, and indifference. Values linked to pairs indicate  ids of nodes in the hierarchy of criteria tree. If value is not given or if it is equal to 0 pairwise comparison is assumed to concern for the whole set of criteria. Otherwise, the preference relation applies only to a particular node. The input value should be provided as a valid XMCDA document whose main tag is alternativesComparisons. For each type of comparison, a separate alternativesComparisons tag should be used. Within these groups a mentioned types are denoted using a comparisonType tag by respectively strict, weak, and indif label. Comparisons should be provided as pairs of alternatives ids.



XMCDA related:
..............

- **Tag:** alternativesComparisons

- **Code:**

  ::

    
                
                    <alternativesComparisons>
                        <comparisonType>
                            %1<!-- type of preference: strong, weak, or indif -->
                        </comparisonType>
                        <pairs>
                            <pair>
                                <initial>
                                    <alternativeID>%2</alternativeID>
                                </initial>
                                <terminal>
                                    <alternativeID>%3</alternativeID>
                                </terminal>
                                <value>
                                    <label>%4</label>
                                </value>
                            </pair>
                            [...]
                        </pairs>
                    </alternativesComparisons>
                    
            

----------------------------


.. _RORUTA-RepresentativeValueFunctionHierarchical-PUT-intensities-of-preferences:

intensities-of-preferences
~~~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Set of comparisons of intensities of preference. For a pair of preference relations three types of comparisons are supported. These are the strict preference, weak preference, and indifference. Values linked to pairs, determine ids of nodes in the hierarchy of criteria tree. If value is not given or if it is equal to 0 intensity of preference is assumed to concern for the whole set of criteria. Otherwise, the statement applies only to a particular node. The input value should be provided as a valid XMCDA document whose main tag is alternativesComparisons. For each type of comparison, a separate alternativesComparisons tag should be used. Within these groups aforementioned types are denoted using a comparisonType tag by respectively strict, weak, and indif label. Comparisons should be provided as pairs of two elementary sets of alternatives ids. The following form is expected:



XMCDA related:
..............

- **Tag:** alternativesComparisons

- **Code:**

  ::

    
                
                    <alternativesComparisons>
                        <comparisonType>
                            %1<!-- type of preference: strong, weak, or indif -->
                        </comparisonType>
                        <pairs>
                            <pair>
                                <initial>
                                    <alternativesSet>
                                        <element>
                                            <alternativeID>%2</alternativeID>
                                        </element>
                                        <element>
                                            <alternativeID>%3</alternativeID>
                                        </element>
                                    </alternativesSet>
                                </initial>
                                <terminal>
                                    <alternativesSet>
                                        <element>
                                            <alternativeID>%4</alternativeID>
                                        </element>
                                        <element>
                                            <alternativeID>%5</alternativeID>
                                        </element>
                                    </alternativesSet>
                                </terminal>
                                <value>
                                    <label>%6</label>
                                </value>
                            </pair>
                        </pairs>
                    </alternativesComparisons>
                    
            

----------------------------


.. _RORUTA-RepresentativeValueFunctionHierarchical-PUT-rank-related-requirements:

rank-related-requirements
~~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Set of rank-related requirements. In other words it is a set of  ranges of possible positions in the final ranking for a chosen alternatives. The label values linked to the alternatives, determines an ids of nodes in the hierarchy of criteria tree. If value is not given or if it is equal to 0 rank related requirement is assumed to concern for the whole set of criteria, Otherwise, the preference relation applies only for a particular node. The input value should be provided as a valid XMCDA document whose main tag is alternativesValues. Each requirement should contain both an id of the reffered alternative and pair of values that denote the desired range. This information should be provided within a separate alternativesValue tag.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
                
                    <alternativesValues>
                        <alternativeValue>
                            <alternativeID>%1</alternativeID>
                            <value>
                                <interval>
                                    <lowerBound><integer>%2</integer></lowerBound>
                                    <upperBound><integer>%3</integer></upperBound>
                                </interval>
                            </value>
                            <value>
                                <label>%4</label>
                            </value>
                        </alternativeValue>
                    </alternativesValues>
                    
            

----------------------------


.. _RORUTA-RepresentativeValueFunctionHierarchical-PUT-parameters:

parameters
~~~~~~~~~~


Description:
............

Method parameters
                    strict %1 - Single boolean value. Determines whether to use sctrictly increasing (true) or monotonously increasing (false) value functions
                    compromise %2 -A boolean parameter that determines does the compromise method should be used.



GUI information:
................


- **Name:** Use strictly increasing value functions?

  *Single boolean value. Determines whether to use sctrictly increasing (true) or monotonously increasing (false) value functions.*

  - **Default value:** false
- **Name:** Use a compromise method?

  *This parameter defines does the compromise method is used.*

  - **Default value:** false

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                
                     <methodParameters>
                         <parameter name="strict">
                            <value>
                                <boolean>%1</boolean>
                            </value>
                        </parameter>
                        <parameter name="compromise">
                            <value>
                                <boolean>%2</boolean>
                            </value>
                        </parameter>
                     </methodParameters>
                    
            

----------------------------



.. _RORUTA-RepresentativeValueFunctionHierarchical-PUT_outputs:

Outputs
-------


- :ref:`representative-value-function-hierarchical <RORUTA-RepresentativeValueFunctionHierarchical-PUT-representative-value-function-hierarchical>`
- :ref:`messages <RORUTA-RepresentativeValueFunctionHierarchical-PUT-messages>`

.. _RORUTA-RepresentativeValueFunctionHierarchical-PUT-representative-value-function-hierarchical:

representative value function hierarchical
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The lists of characteristic points of the representative value function that are provided for each criterion. The returned value is an XMCDA document which may contain a number of the criteria tags. Each node of hierarchy is described using another criteria tag. Its id attribut denotes the id of described node. Each group contains a description of characteristic points on all considered criteria. The id attribute of each criterion tag denotes the id of described criterion. Each function is described using a criterionFunction tag.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
                
                    <criteria id="%1">
                        <criterion id="%2">
                            <criterionFunction>
                                <points>
                                    <point>
                                        <abscissa><real>[...]</real></abscissa>
                                        <ordinate><real>[...]</real></ordinate>
                                    </point>
                                    [...]
                                </points>
                            </criterionFunction>
                        </criterion>
                        [...]
                    </criteria>
                    [...]
                    
            

----------------------------


.. _RORUTA-RepresentativeValueFunctionHierarchical-PUT-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
