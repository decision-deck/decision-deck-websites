:orphan:



.. _plotCriteriaComparisons-RXMCDA:

plotCriteriaComparisons
=======================

:Provider: RXMCDA
:Version: 1.0

Description
-----------

Generates a graph representing a partial preorder on the criteria.

Please note: this program is deprecated and it is replaced by plotCriteriaComparisons in package ITTB.

- **Contact:** Patrick Meyer (patrick.meyer@telecom-bretagne.eu)



Inputs
------
(For outputs, see :ref:`below <plotCriteriaComparisons-RXMCDA_outputs>`)


- :ref:`criteria <plotCriteriaComparisons-RXMCDA-criteria>`
- :ref:`criteriaComparisons <plotCriteriaComparisons-RXMCDA-criteriaComparisons>`
- :ref:`cutLevel <plotCriteriaComparisons-RXMCDA-cutLevel>` *(optional)*

.. _plotCriteriaComparisons-RXMCDA-criteria:

criteria
~~~~~~~~


Description:
............

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
                
                    <criteria>
                        <criterion>
                            <active>[...]</active>
                            [...]
                        </criterion>
                        [...]
                    </criteria>
                    
            

----------------------------


.. _plotCriteriaComparisons-RXMCDA-criteriaComparisons:

criteriaComparisons
~~~~~~~~~~~~~~~~~~~


Description:
............

A valued relation relative to the partial preorder of the criteria. A numeric <value> indicates a the valuation for each <pair> of the relation.



XMCDA related:
..............

- **Tag:** criteriaComparisons

----------------------------


.. _plotCriteriaComparisons-RXMCDA-cutLevel:

cutLevel
~~~~~~~~


Description:
............

A real value indicating above which level the valued relation (comparisons) should be considered as validated.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** cutLevel

  *A real value indicating above which level the valued relation (comparisons) should be considered as validated.*

  - **Type:** float

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                
                    <methodParameters>
                        <parameter
                             name="cutlevel"> <!-- REQUIRED  -->
                            <value>
                                <real>%1</real>
                            </value>
                        </parameter>
                    </methodParameters>
                    
            

----------------------------



.. _plotCriteriaComparisons-RXMCDA_outputs:

Outputs
-------


- :ref:`criteriaComparisonsPlot <plotCriteriaComparisons-RXMCDA-criteriaComparisonsPlot>`
- :ref:`messages <plotCriteriaComparisons-RXMCDA-messages>`

.. _plotCriteriaComparisons-RXMCDA-criteriaComparisonsPlot:

criteriaComparisonsPlot
~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

A string containing the base64 representation of the png image of the graph generated by the R statistical software.



XMCDA related:
..............

- **Tag:** criterionValue

----------------------------


.. _plotCriteriaComparisons-RXMCDA-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
