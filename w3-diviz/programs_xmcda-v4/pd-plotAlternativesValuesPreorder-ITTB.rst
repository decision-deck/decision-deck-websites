:orphan:



.. _plotAlternativesValuesPreorder-ITTB:

plotAlternativesValuesPreorder
==============================

:Provider: ITTB
:Version: 2.0

Description
-----------

This web service generates a graph representing a preorder on the alternatives, according to numerical values taken by the alternatives (the "best" alternative has the highest value). Compared to the web service plotAlternativesValuesPreorder, some parameters are added. Colors can be used and the title of the graph can be typed. You can choose between an increasing or a decreasing order for the graph. It is also possible to show the name of the alternatives instead of the id, etc. The alternatives' evaluations are supposed to be real or integer numeric values.

- **Contact:** Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)



Inputs
------
(For outputs, see :ref:`below <plotAlternativesValuesPreorder-ITTB_outputs>`)


- :ref:`alternatives <plotAlternativesValuesPreorder-ITTB-alternatives>`
- :ref:`alternativesValues <plotAlternativesValuesPreorder-ITTB-alternativesValues>`
- :ref:`parameters <plotAlternativesValuesPreorder-ITTB-parameters>`

.. _plotAlternativesValuesPreorder-ITTB-alternatives:

alternatives
~~~~~~~~~~~~


Description:
............

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
	<alternatives>
		<alternative>
			<active>[...]</active>
			[...]
		</alternative>
		[...]
	</alternatives>


----------------------------


.. _plotAlternativesValuesPreorder-ITTB-alternativesValues:

alternativesValues
~~~~~~~~~~~~~~~~~~


Description:
............

A list of <alternativesValue> representing a certain numeric quantity for each alternative, like, e.g., an overall value.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _plotAlternativesValuesPreorder-ITTB-parameters:

parameters
~~~~~~~~~~


Description:
............

Generates a graph taking into account the proposed options.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** Chart title:

  *String for the title of the graph. The default value is an empty field.*

  - **Type:** string
  - **Default value:** ""
- **Name:** Order:

  *Increasing or decreasing. The default value is decreasing.*

  - **Type:** drop-down list
  - **Possible values:**
      - Ascending (XMCDA label : increasing) 

      - Descending (XMCDA label : decreasing) (default)

- **Name:** Shape of the nodes?

  *Choose between rectangle, square, ellipse, circle or diamond.*

  - **Type:** drop-down list
  - **Possible values:**
      - Rectangle (XMCDA label : Rectangle) (default)

      - Square (XMCDA label : Square) 

      - Ellipse (XMCDA label : Ellipse) 

      - Circle (XMCDA label : Circle) 

      - Diamond (XMCDA label : Diamond) 

- **Name:** Alternative name or id?

  *Display alternatives' names or IDs.*

  - **Type:** drop-down list
  - **Possible values:**
      - Display name (XMCDA label : true) 

      - Display id (XMCDA label : false) (default)

- **Name:** Use colors?

  *The use of colors: true for a colored graph.*

  - **Type:** drop-down list
  - **Possible values:**
      - Yes (XMCDA label : true) 

      - No (XMCDA label : false) (default)

- **Name:** Choose color:

  *String that indicates the color of the graph.Choose between "Black", "Red", "Blue", "Green", "Yellow", "Magenta" and "Cyan".*

  - **Type:** drop-down list
  - **Possible values:**
      - Black (XMCDA label : Black) (default)

      - Red (XMCDA label : Red) 

      - Blue (XMCDA label : Blue) 

      - Green (XMCDA label : Green) 

      - Yellow (XMCDA label : Yellow) 

      - Magenta (XMCDA label : Magenta) 

      - Cyan (XMCDA label : Cyan) 

      - White (XMCDA label : White) 


XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
	<programParameters>
		<parameter id="plot_title" name="Plot Title">
			<values>
				<value>
					<label>%1</label>
				</value>
			</values>
		</parameter>
		<parameter id="plot_order" name="Plot order">
			<values>
				<value>
					<label>%2</label>
				</value>
			</values>
		</parameter>
		<parameter id="node_shape" name="Node shape">
			<values>
				<value>
					<label>%3</label>
				</value>
			</values>
		</parameter>
		<parameter id="show_names" name="Show alternatives names">
			<values>
				<value>
					<label>%4</label>
				</value>
			</values>
		</parameter>
		<parameter id="use_color" name="Colors in the graph">
			<values>
				<value>
					<label>%5</label>
				</value>
			</values>
		</parameter>
		<parameter id="selected_color" name="Selected color">
			<values>
				<value>
					<label>%6</label>
				</value>
			</values>
		</parameter >
	</programParameters>


----------------------------



.. _plotAlternativesValuesPreorder-ITTB_outputs:

Outputs
-------


- :ref:`alternativesValuesPreorder.dot <plotAlternativesValuesPreorder-ITTB-dot>`
- :ref:`alternativesValuesPreorder.png <plotAlternativesValuesPreorder-ITTB-png>`
- :ref:`messages <plotAlternativesValuesPreorder-ITTB-messages>`

.. _plotAlternativesValuesPreorder-ITTB-dot:

preorder (dot)
~~~~~~~~~~~~~~


Description:
............

The dot file used to generate the PNG image.



XMCDA related:
..............

- **Tag:** other

----------------------------


.. _plotAlternativesValuesPreorder-ITTB-png:

preorder (png)
~~~~~~~~~~~~~~


Description:
............

The pre-order as a PNG image.



XMCDA related:
..............

- **Tag:** other

----------------------------


.. _plotAlternativesValuesPreorder-ITTB-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
