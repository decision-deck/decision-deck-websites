:orphan:



.. _DEASMAAvalueADDPreferenceRelations-PUT:

DEASMAAvalueADDPreferenceRelations
==================================

:Provider: PUT
:Version: 1.0

Description
-----------

Determines dominance relations for the given DMUs (alternatives) using SMAA-D method and additive Data Envelopment Analysis Model. For given number of samples  returns a matrix with alternatives in each row and column. Single cell indicates how many samples of alternative in a row dominates alternative in a column.

- **Contact:** Malgorzata Napieraj (napieraj.malgorzata@gmail.com)

- **Reference:** Gouveia M. C., Dias L. C., Antunes C. H., Additive DEA based on MCDA with imprecise information (2008).
			Lahdelma R., Salminen P., Stochastic multicriteria acceptability analysis using the data envelopment model (2004).



Inputs
------
(For outputs, see :ref:`below <DEASMAAvalueADDPreferenceRelations-PUT_outputs>`)


- :ref:`inputsOutputs <DEASMAAvalueADDPreferenceRelations-PUT-inputsOutputs>`
- :ref:`units <DEASMAAvalueADDPreferenceRelations-PUT-units>`
- :ref:`performanceTable <DEASMAAvalueADDPreferenceRelations-PUT-performanceTable>`
- :ref:`weightsLinearConstraints <DEASMAAvalueADDPreferenceRelations-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <DEASMAAvalueADDPreferenceRelations-PUT-methodParameters>`

.. _DEASMAAvalueADDPreferenceRelations-PUT-inputsOutputs:

inputsOutputs
~~~~~~~~~~~~~


Description:
............

A list of criteria with specified preference direction. List has to contains at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output).



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    <criteria>
                        <criterion>
							<scale>
								<quantitative>
									<preferenceDirection>
										[...]
									</preferenceDirection>
								</quantitative>
							</scale>
                            [...]
                        </criterion>
                        [...]
                    </criteria>

----------------------------


.. _DEASMAAvalueADDPreferenceRelations-PUT-units:

units
~~~~~


Description:
............

A list of alternatives (DMUs).



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>

----------------------------


.. _DEASMAAvalueADDPreferenceRelations-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~


Description:
............

A list of alternatives (DMUs) performances.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    <performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>

----------------------------


.. _DEASMAAvalueADDPreferenceRelations-PUT-weightsLinearConstraints:

weightsLinearConstraints
~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

A list of criteria linear constraints.



XMCDA related:
..............

- **Tag:** criteriaLinearConstraints

- **Code:**

  ::

    <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID>[...]</criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>

----------------------------


.. _DEASMAAvalueADDPreferenceRelations-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~


Description:
............

"boundariesProvided" indicates if criteria values boundaries are given in criteria input or they have to be computed based on alternatives performances. "samplesNo" represents the number of samples to generate. "transformToUtilites" indicates if given alternatives values are utilities or if they have to be firstly transformed to utilities.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** boundariesProvided

  

  - **Default value:** false
- **Name:** samplesNo

  

  - **Constraint description:** The value should be a positive integer.

  - **Type:** integer
  - **Default value:** 100
- **Name:** transformToUtilities

  

  - **Default value:** true

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    <methodParameters>
							<parameter name="boundariesProvided">
								<value><boolean>%1</boolean></value>
							</parameter>
							<parameter name="samplesNo">
								<value><integer>%2</integer></value>
							</parameter>
							<parameter name="transformToUtilities">
								<value><boolean>%3</boolean></value>
							</parameter>
					</methodParameters>

----------------------------



.. _DEASMAAvalueADDPreferenceRelations-PUT_outputs:

Outputs
-------


- :ref:`pairwiseOutrankingIndices <DEASMAAvalueADDPreferenceRelations-PUT-pairwiseOutrankingIndices>`
- :ref:`messages <DEASMAAvalueADDPreferenceRelations-PUT-messages>`

.. _DEASMAAvalueADDPreferenceRelations-PUT-pairwiseOutrankingIndices:

pairwiseOutrankingIndices
~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

A performance table for given alternatives. Single performance consists of attribute criterionID representing dominated alternative, and a value representing ratio of samples dominating this alternative.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    <performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>geq [...]</criterionID>
									<value>
									[...]
									</value>
							</performance>
							[...]
						</alternativePerformances>
					</performanceTable>

----------------------------


.. _DEASMAAvalueADDPreferenceRelations-PUT-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
