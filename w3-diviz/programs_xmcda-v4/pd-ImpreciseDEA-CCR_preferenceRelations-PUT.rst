:orphan:



.. _ImpreciseDEA-CCR_preferenceRelations-PUT:

ImpreciseDEA-CCR_preferenceRelations
====================================

:Provider: PUT
:Version: 1.0

Description
-----------

Computes extrene efficiency ranks for the given DMUs (alternatives) using CCR Data Envelopment Analysis Model.

- **Contact:** Anna Labijak <support@decision-deck.org>



Inputs
------
(For outputs, see :ref:`below <ImpreciseDEA-CCR_preferenceRelations-PUT_outputs>`)


- :ref:`units <ImpreciseDEA-CCR_preferenceRelations-PUT-units>`
- :ref:`inputsOutputs <ImpreciseDEA-CCR_preferenceRelations-PUT-inputsOutputs>`
- :ref:`inputsOutputsScales <ImpreciseDEA-CCR_preferenceRelations-PUT-inputsOutputsScales>`
- :ref:`performanceTable <ImpreciseDEA-CCR_preferenceRelations-PUT-performanceTable>`
- :ref:`maxPerformanceTable <ImpreciseDEA-CCR_preferenceRelations-PUT-maxPerformanceTable>` *(optional)*
- :ref:`weightsLinearConstraints <ImpreciseDEA-CCR_preferenceRelations-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <ImpreciseDEA-CCR_preferenceRelations-PUT-methodParameters>`

.. _ImpreciseDEA-CCR_preferenceRelations-PUT-units:

units
~~~~~


Description:
............

A list of alternatives (DMUs).



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>

----------------------------


.. _ImpreciseDEA-CCR_preferenceRelations-PUT-inputsOutputs:

inputs/outputs
~~~~~~~~~~~~~~


Description:
............

A list of criteria.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    <criteria>
                        <criterion>[...]</criterion>
                        [...]
                    </criteria>

----------------------------


.. _ImpreciseDEA-CCR_preferenceRelations-PUT-inputsOutputsScales:

inputs/outputs scales
~~~~~~~~~~~~~~~~~~~~~


Description:
............

Informations about inputs and outputs scales and optionally about boundaries



XMCDA related:
..............

- **Tag:** criteriaScales

- **Code:**

  ::

    
<criteriaScales>
    <criterionScale>
      <criterionID>[...]</criterionID>
      <scales>
        <scale>
          [...]
        </scale>
      </scales>
    </criterionScale>
    [...]
</criteriaScales>

----------------------------


.. _ImpreciseDEA-CCR_preferenceRelations-PUT-performanceTable:

performance
~~~~~~~~~~~


Description:
............

A list of alternatives (DMUs) minimal performances (or exact performances if intervals should be created by tolerance).



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<values><value>[...]</value></values>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>

----------------------------


.. _ImpreciseDEA-CCR_preferenceRelations-PUT-maxPerformanceTable:

max performance
~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A list of alternatives (DMUs) maximal performances.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<values><value>[...]</value></values>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>

----------------------------


.. _ImpreciseDEA-CCR_preferenceRelations-PUT-weightsLinearConstraints:

weights constraints
~~~~~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

A list of criteria linear constraints.



XMCDA related:
..............

- **Tag:** criteriaLinearConstraints

- **Code:**

  ::

    
            <criteriaLinearConstraints>
                <constraints>
                    <constraint>
                        <elements>
                            <element>
                                <criterionID> [...] </criterionID>
                                <coefficient>
                                    [...]
                                </coefficient>
                            </element>
                            [...]
                        </elements>
                        <operator>[...]</operator>
                        <rhs>
                            [...]
                        </rhs>
                    </constraint>
                    [...]
                </constraints>
            </criteriaLinearConstraints>

----------------------------


.. _ImpreciseDEA-CCR_preferenceRelations-PUT-methodParameters:

parameters
~~~~~~~~~~


Description:
............

Represents method parameters (tolerance).



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** tolerance

  

  - **Constraint description:** The value should be a non-negative number.

  - **Type:** float
  - **Default value:** 0.00

XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
    <programParameters>
        <parameter id="tolerance">
            <values>
                <value><real>%1</real></value>
            </values>
        </parameter>
    </programParameters>

----------------------------



.. _ImpreciseDEA-CCR_preferenceRelations-PUT_outputs:

Outputs
-------


- :ref:`necessaryDominance <ImpreciseDEA-CCR_preferenceRelations-PUT-necessaryDominance>`
- :ref:`possibleDominance <ImpreciseDEA-CCR_preferenceRelations-PUT-possibleDominance>`
- :ref:`messages <ImpreciseDEA-CCR_preferenceRelations-PUT-messages>`

.. _ImpreciseDEA-CCR_preferenceRelations-PUT-necessaryDominance:

necessary dominance
~~~~~~~~~~~~~~~~~~~


Description:
............

A list of pairs of pairs of DMU related with necessary preference relation.



XMCDA related:
..............

- **Tag:** alternativesMatrix

- **Code:**

  ::

    
                    <alternativesMatrix>
						<row>
                            <alternativeID>[...]</alternativeID>
                            <column>
                                <alternativeID>[...]</alternativeID>
                                <values>
                                    <value><integer>1</integer></value>
                                </values>
                            </column>
                            [...]
                        </row>
                        [...]
					</alternativesMatrix>
                    

----------------------------


.. _ImpreciseDEA-CCR_preferenceRelations-PUT-possibleDominance:

possible dominance
~~~~~~~~~~~~~~~~~~


Description:
............

A list of pairs of pairs of DMU related with possible preference relation.



XMCDA related:
..............

- **Tag:** alternativesMatrix

- **Code:**

  ::

    
                    <alternativesMatrix>
						<row>
                            <alternativeID>[...]</alternativeID>
                            <column>
                                <alternativeID>[...]</alternativeID>
                                <values>
                                    <value><integer>1</integer></value>
                                </values>
                            </column>
                            [...]
                        </row>
                        [...]
					</alternativesMatrix>
                    

----------------------------


.. _ImpreciseDEA-CCR_preferenceRelations-PUT-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
