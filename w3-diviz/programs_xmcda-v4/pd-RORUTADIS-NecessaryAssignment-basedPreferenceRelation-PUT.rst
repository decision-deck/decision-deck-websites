:orphan:



.. _RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT:

RORUTADIS-NecessaryAssignment-basedPreferenceRelation
=====================================================

:Provider: PUT
:Version: 0.1

Description
-----------

Compares necessary assignments of alternatives using Robust Ordinal Regression for value-based sorting. It is possible to provide an additional optional preference information: example alternatives assignments, assignment pairwise comparisons and desired class cardinalities. Service developed by Krzysztof Ciomek (Poznan University of Technology, under supervision of Milosz Kadzinski).

- **Contact:** 
			Krzysztof Ciomek (k.ciomek@gmail.com),
			Milosz Kadzinski (milosz.kadzinski@cs.put.poznan.pl)
		

- **Web page:** https://github.com/kciomek/rorutadis

- **Reference:** None



Inputs
------
(For outputs, see :ref:`below <RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT_outputs>`)


- :ref:`criteria <RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-criteria>`
- :ref:`alternatives <RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-alternatives>`
- :ref:`categories <RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-categories>`
- :ref:`performanceTable <RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-performanceTable>`
- :ref:`assignmentExamples <RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-assignmentExamples>` *(optional)*
- :ref:`assignmentComparisons <RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-assignmentComparisons>` *(optional)*
- :ref:`categoriesCardinalities <RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-categoriesCardinalities>` *(optional)*
- :ref:`strictlyMonotonicValueFunctions <RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-strictlyMonotonicValueFunctions>`

.. _RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-criteria:

criteria
~~~~~~~~


Description:
............

A list of criteria (<criteria> tag) with information about preference direction (<criteriaValues mcdaConcept="preferenceDirection">, 0 - gain, 1 - cost) and number of characteristic points (<criteriaValues mcdaConcept="numberOfCharacteristicPoints">, 0 for the most general marginal utility function or integer grater or equal to 2) of each criterion.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
<criteria>
	<criterion id="[...]" />
	[...]
</criteria>

<criteriaValues mcdaConcept="preferenceDirection">
	<criterionValue>
		<criterionID>[...]</criterionID>
		<values><value><integer>[...]</integer></value></values>
	</criterionValue>
	[...]
</criteriaValues>

<criteriaValues mcdaConcept="numberOfCharacteristicPoints">
	<criterionValue>
		<criterionID>[...]</criterionID>
		<values><value><integer>[0|integer greater or equal to 2]</integer></value></values>
	</criterionValue>
	[...]
</criteriaValues>


----------------------------


.. _RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-alternatives:

alternatives
~~~~~~~~~~~~


Description:
............

A list of alternatives.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
	<alternatives>
		<alternative id="[...]">
			<active>[...]</active>
		</alternative>
	[...]
	</alternatives>


----------------------------


.. _RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-categories:

categories
~~~~~~~~~~


Description:
............

A list of categories (classes). List must be sorted from the worst category to the best.



XMCDA related:
..............

- **Tag:** categories

- **Code:**

  ::

    
<categories>
	<category id="[...]" />
	[...]
</categories>


----------------------------


.. _RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~


Description:
............

The performances of the alternatives.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-assignmentExamples:

assignmentExamples
~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A list of assignment examples of alternatives to intervals of categories (classes) or to a specific category (class).



XMCDA related:
..............

- **Tag:** alternativesAssignments

- **Code:**

  ::

    
<alternativesAssignments>
	<alternativeAssignment>
		<alternativeID>[...]</alternativeID>
		<categoryID>[...]</categoryID>
	</alternativeAssignment>
	[...]
	<alternativeAssignment>
		<alternativeID>[...]</alternativeID>
		<categoriesInterval>
			<lowerBound>
				<categoryID>[...]</categoryID>
			</lowerBound>
			<upperBound>
				<categoryID>[...]</categoryID>
			</upperBound>
		</categoriesInterval>
	</alternativeAssignment>
	[...]
	<alternativeAssignment>
		<alternativeID>[...]</alternativeID>
		<categoriesSet>
			<categoryID>[...]</categoryID>
			[...]
		</categoriesSet>
	</alternativeAssignment>
	[...]
</alternativesAssignments>


----------------------------


.. _RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-assignmentComparisons:

assignmentComparisons
~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Two lists of assignment pairwise comparisons. A comparison from list with attribute mcdaConcept="atLeastAsGoodAs" indicates that some alternative should be assigned to class at least as good as class of some other alternative (k = 0) or at least better by k classes (k > 0). A comparison from list with attribute mcdaConcept="atMostAsGoodAs" indicates that some alternative should be assigned to class at most better by k classes (k > 0) then some other alternative.



XMCDA related:
..............

- **Tag:** alternativesMatrix

- **Code:**

  ::

    
<alternativesMatrixs mcdaConcept="atLeastAsGoodAs">
	<row>
		<alternativeID>[...]</alternativeID>
		<column>
			<alternativeID>[...]</alternativeID>
			<values><value><integer>k</integer></value></values>
		</column>
		[...]
	</row>
	[...]
</alternativesMatrix>

<alternativesMatrixs mcdaConcept="atMostAsGoodAs">
	[...]
</alternativesMatrix>


----------------------------


.. _RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-categoriesCardinalities:

categoriesCardinalities
~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A list of category (class) cardinality constraints. It allows to define minimal and/or maximal desired category (class) cardinalities.



XMCDA related:
..............

- **Tag:** categoriesValues

- **Code:**

  ::

    
<categoriesValues>
	<categoryValue>
		<categoryID>[...]</categoryID>
		<values>
			<value>
				<interval>
					<lowerBound><integer>[...]</integer></lowerBound>
					<upperBound><integer>[...]</integer></upperBound>
				</interval>
			</value>
		</values>
	</categoryValue>
	[...]
</categoriesValues>


----------------------------


.. _RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-strictlyMonotonicValueFunctions:

strictlyMonotonicValueFunctions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

Whether marginal value functions strictly monotonic (true) or weakly monotonic (false).



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** strictlyMonotonicValueFunctions

  *Whether marginal value functions strictly monotonic or not.*

  - **Default value:** false

XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
<programParameters>
	<parameter name="strictlyMonotonicValueFunctions">
		<values>
			<value>
				<boolean>%1</boolean>
			</value>
		</values>
	</parameter>
</programParameters>


----------------------------



.. _RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT_outputs:

Outputs
-------


- :ref:`assignmentComparisons <RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-assignmentComparisons-out>`
- :ref:`messages <RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-messages>`

.. _RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-assignmentComparisons-out:

assignmentComparisons
~~~~~~~~~~~~~~~~~~~~~


Description:
............

Necessary assignment comparisons. This output is compatible with assignmentComparisons input for various RORUTADIS-... services. Possible to visualize with HasseDiagram service.



XMCDA related:
..............

- **Tag:** alternativesMatrix

- **Code:**

  ::

    
<alternativesMatrix mcdaConcept="atLeastAsGoodAs">
	<row>
		<alternativeID>[...]</alternativeID>
		<column>
			<alternativeID>[...]</alternativeID>
			<values><value><integer>k</integer></value></values>
		</column>
		[...]
	</row>
<alternativesMatrix/>


----------------------------


.. _RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-messages:

messages
~~~~~~~~


Description:
............

Messages generated by the program.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
