:orphan:



.. _plotCriteriaFunctions-discrete-ITTB:

plotCriteriaFunctions-discrete
==============================

:Provider: ITTB
:Version: 2.0

Description
-----------

This web service allows to plot discrete criteria functions. Colors can be used. You can specify how to display the functions: by line, by column or by square. A linear interpolation can be processed in order to connect the different points of the discrete functions.

- **Contact:** Sébastien Bigaret (sebastien.bigaret@telecom-bretagne.eu)



Inputs
------
(For outputs, see :ref:`below <plotCriteriaFunctions-discrete-ITTB_outputs>`)


- :ref:`criteria <plotCriteriaFunctions-discrete-ITTB-criteria>` *(optional)*
- :ref:`criteriaFunctions <plotCriteriaFunctions-discrete-ITTB-criteriaFunctions>`
- :ref:`parameters <plotCriteriaFunctions-discrete-ITTB-parameters>`

.. _plotCriteriaFunctions-discrete-ITTB-criteria:

criteria
~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
	<criteria>
		<criterion>
			<active>[...]</active>
			[...]
		</criterion>
		[...]
	</criteria>


----------------------------


.. _plotCriteriaFunctions-discrete-ITTB-criteriaFunctions:

criteria functions
~~~~~~~~~~~~~~~~~~


Description:
............

The criteria functions to plot



XMCDA related:
..............

- **Tag:** criteriaFunctions

- **Code:**

  ::

    
	<criteriaFunctions>
		<criterionFunction>
			<criterionID>[...]</criterionID>
			<functions>
				<function>
					<discrete>
						<point>
							<abscissa><real>[...]</real></abscissa>
							<ordinate><real>[...]</real></ordinate>
						</point>
						[...]
					</discrete>
				</function>
			</functions>
		</criterionFunction>
	</criteriaFunctions>


----------------------------


.. _plotCriteriaFunctions-discrete-ITTB-parameters:

parameters
~~~~~~~~~~


Description:
............

None



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** Unique or multiple plot(s)?

  *In a unique plot, only one image is generated containing all the functions. Multiple plots can be obtained. The default value is true.*

  - **Type:** drop-down list
  - **Possible values:**
      - Unique (XMCDA label : true) (default)

      - Multiple (XMCDA label : false) 

- **Name:** Plots arrangement

  *In the case of a unique plot, you can specify how to display the functions: by line, by column or in a grid. The default value is by column.*

  - **Type:** drop-down list
  - **Possible values:**
      - Column (XMCDA label : column) (default)

      - Line (XMCDA label : line) 

      - Grid (XMCDA label : grid) 

- **Name:** Linear interpolation ?

  *Linear interpolation means that lines connect the points in each function. The default value is true.*

  - **Type:** drop-down list
  - **Possible values:**
      - Yes (XMCDA label : true) (default)

      - No (XMCDA label : false) 

- **Name:** Add vertical bars ?

  *Without linear interpolation, you can add vertical lines so that points are more visible. The default value is false.*

  - **Type:** drop-down list
  - **Possible values:**
      - Yes (XMCDA label : true) 

      - No (XMCDA label : false) (default)

- **Name:** Use Colors?

  *The use of colors: true for colored functions.*

  - **Type:** drop-down list
  - **Possible values:**
      - Yes (XMCDA label : true) 

      - No (XMCDA label : false) (default)

- **Name:** Choose color:

  *Choose between "Black", "Red", "Blue", "Green", "Yellow", "Magenta" and "Cyan".*

  - **Type:** drop-down list
  - **Possible values:**
      - Black (XMCDA label : Black) (default)

      - Red (XMCDA label : Red) 

      - Blue (XMCDA label : Blue) 

      - Green (XMCDA label : Green) 

      - Yellow (XMCDA label : Yellow) 

      - Magenta (XMCDA label : Magenta) 

      - Cyan (XMCDA label : Cyan) 


XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
	<programParameters>
		<parameter id="unique_plot" name="Unique plot">
			<values>
				<value>
					<label>%1</label>
				</value>
			</values>
		</parameter>
		<parameter id="plots_display" name="Plots' display">
			<values>
				<value>
					<label>%2</label>
				</value>
			</values>
		</parameter>
		<parameter id="linear_interpolation" name="Linear interpolation">
			<values>
				<value>
					<label>%3</label>
				</value>
			</values>
		</parameter>
		<parameter id="vertical_lines" name="Vertical lines">
			<values>
				<value>
					<label>%4</label>
				</value>
			</values>
		</parameter>
		<parameter id="use_color" name="Colors in the plots">
			<values>
				<value>
					<label>%5</label>
				</value>
			</values>
		</parameter>
		<parameter id="selected_color" name="Selected color">
			<values>
				<value>
					<label>%6</label>
				</value>
			</values>
			</parameter>
	</programParameters>


----------------------------



.. _plotCriteriaFunctions-discrete-ITTB_outputs:

Outputs
-------


- :ref:`criteriaFunctions.png <plotCriteriaFunctions-discrete-ITTB-png>`
- :ref:`messages <plotCriteriaFunctions-discrete-ITTB-messages>`

.. _plotCriteriaFunctions-discrete-ITTB-png:

criteria functions (png)
~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The graphical representation of the discrete functions, as a PNG image.



XMCDA related:
..............

- **Tag:** other

----------------------------


.. _plotCriteriaFunctions-discrete-ITTB-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
