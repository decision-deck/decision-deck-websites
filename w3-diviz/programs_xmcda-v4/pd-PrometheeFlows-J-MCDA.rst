:orphan:



.. _PrometheeFlows-J-MCDA:

PrometheeFlows
==============

:Provider: J-MCDA
:Version: 0.5.5

Description
-----------

Computes Promethee flows (net flows, positive flows, or negative flows).

- **Contact:** Olivier Cailloux <olivier.cailloux@ecp.fr>

- **Web page:** http://sourceforge.net/projects/j-mcda/

- **Reference:** Cailloux, Olivier. Electre and Promethee MCDA methods as reusable software components. In Proceedings of the 25th Mini-EURO Conference on Uncertainty and Robustness in Planning and Decision Making (URPDM 2010). Coimbra, Portugal, 2010.



Inputs
------
(For outputs, see :ref:`below <PrometheeFlows-J-MCDA_outputs>`)


- :ref:`alternatives <PrometheeFlows-J-MCDA-input0>` *(optional)*
- :ref:`preference <PrometheeFlows-J-MCDA-input1>`
- :ref:`flow_type <PrometheeFlows-J-MCDA-input2>`

.. _PrometheeFlows-J-MCDA-input0:

alternatives
~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The alternatives to consider. Set some alternatives as inactive (or remove them) to ignore them.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _PrometheeFlows-J-MCDA-input1:

preference
~~~~~~~~~~


Description:
............

The preference relation from which to compute flows.



XMCDA related:
..............

- **Tag:** alternativesMatrix

----------------------------


.. _PrometheeFlows-J-MCDA-input2:

flow_type
~~~~~~~~~


Description:
............

The type of flow to compute: positive, negative or net.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** flow_type

  

  - **Type:** drop-down list
  - **Possible values:**
      - The positive flow (XMCDA label : POSITIVE) (default)

      - The negative flow (XMCDA label : NEGATIVE) 

      - The net flow (XMCDA label : NET) 


XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    

<programParameters>
	<parameter>
		<values>
			<value>
				<label>%1</label>
			</value>
		</values>
	</parameter>
</programParameters>

			

----------------------------



.. _PrometheeFlows-J-MCDA_outputs:

Outputs
-------


- :ref:`flows <PrometheeFlows-J-MCDA-output0>`
- :ref:`messages <PrometheeFlows-J-MCDA-output1>`

.. _PrometheeFlows-J-MCDA-output0:

flows
~~~~~


Description:
............

The flows computed from the given data.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _PrometheeFlows-J-MCDA-output1:

messages
~~~~~~~~


Description:
............

A status message.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
