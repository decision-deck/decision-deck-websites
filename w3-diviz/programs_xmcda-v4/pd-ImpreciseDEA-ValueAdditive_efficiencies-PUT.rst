:orphan:



.. _ImpreciseDEA-ValueAdditive_efficiencies-PUT:

ImpreciseDEA-CCR_efficiencies
=============================

:Provider: PUT
:Version: 1.0

Description
-----------

Computes efficiency scores for the given DMUs (alternatives)  using Additive Data Envelopment Analysis Model with imprecise information.

- **Contact:** Anna Labijak <support@decision-deck.org>



Inputs
------
(For outputs, see :ref:`below <ImpreciseDEA-ValueAdditive_efficiencies-PUT_outputs>`)


- :ref:`units <ImpreciseDEA-ValueAdditive_efficiencies-PUT-units>`
- :ref:`inputsOutputs <ImpreciseDEA-ValueAdditive_efficiencies-PUT-inputsOutputs>`
- :ref:`inputsOutputsScales <ImpreciseDEA-ValueAdditive_efficiencies-PUT-inputsOutputsScales>`
- :ref:`inputsOutputsFunctions <ImpreciseDEA-ValueAdditive_efficiencies-PUT-inputsOutputsFunctions>` *(optional)*
- :ref:`performanceTable <ImpreciseDEA-ValueAdditive_efficiencies-PUT-performanceTable>`
- :ref:`maxPerformanceTable <ImpreciseDEA-ValueAdditive_efficiencies-PUT-maxPerformanceTable>` *(optional)*
- :ref:`weightsLinearConstraints <ImpreciseDEA-ValueAdditive_efficiencies-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <ImpreciseDEA-ValueAdditive_efficiencies-PUT-methodParameters>`

.. _ImpreciseDEA-ValueAdditive_efficiencies-PUT-units:

units
~~~~~


Description:
............

A list of alternatives (DMUs).



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>

----------------------------


.. _ImpreciseDEA-ValueAdditive_efficiencies-PUT-inputsOutputs:

inputs/outputs
~~~~~~~~~~~~~~


Description:
............

A list of criteria with specified preference direction. List has to contains at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output).



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    <criteria>
                        <criterion>[...]</criterion>
                        [...]
                    </criteria>

----------------------------


.. _ImpreciseDEA-ValueAdditive_efficiencies-PUT-inputsOutputsScales:

inputs/outputs scales
~~~~~~~~~~~~~~~~~~~~~


Description:
............

Informations about inputs and outputs scales and optionally about boundaries



XMCDA related:
..............

- **Tag:** criteriaScales

- **Code:**

  ::

    
<criteriaScales>
    <criterionScale>
      <criterionID>[...]</criterionID>
      <scales>
        <scale>
          [...]
        </scale>
      </scales>
    </criterionScale>
    [...]
</criteriaScales>

----------------------------


.. _ImpreciseDEA-ValueAdditive_efficiencies-PUT-inputsOutputsFunctions:

inputs/outputs function shapes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

Shapes of functions assigned to inputs and outputs (if other than linear)



XMCDA related:
..............

- **Tag:** criteriaFunctions

- **Code:**

  ::

    
<criteriaFunctions>
    <criterionFunction>
      <criterionID>[...]</criterionID>
      <functions>
        <function>
          <piecewiseLinear>
            <segment>
              <head>
                [...]
              </head>
              <tail>
                [...]
              </tail>
            </segment>
          </piecewiseLinear>
        </function>
        [...]
      </functions>
    </criterionFunction>
    [...]
</criteriaFunctions>

----------------------------


.. _ImpreciseDEA-ValueAdditive_efficiencies-PUT-performanceTable:

performance
~~~~~~~~~~~


Description:
............

A list of alternatives (DMUs) minimal performances (or exact performances if intervals should be created by tolerance).



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<values><value>[...]</value></values>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>

----------------------------


.. _ImpreciseDEA-ValueAdditive_efficiencies-PUT-maxPerformanceTable:

max performance
~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A list of alternatives (DMUs) maximal performances.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<values><value>[...]</value></values>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>

----------------------------


.. _ImpreciseDEA-ValueAdditive_efficiencies-PUT-weightsLinearConstraints:

weights constraints
~~~~~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

A list of criteria linear constraints.



XMCDA related:
..............

- **Tag:** criteriaLinearConstraints

- **Code:**

  ::

    
            <criteriaLinearConstraints>
                <constraints>
                    <constraint>
                        <elements>
                            <element>
                                <criterionID> [...] </criterionID>
                                <coefficient>
                                    [...]
                                </coefficient>
                            </element>
                            [...]
                        </elements>
                        <operator>[...]</operator>
                        <rhs>
                            [...]
                        </rhs>
                    </constraint>
                    [...]
                </constraints>
            </criteriaLinearConstraints>

----------------------------


.. _ImpreciseDEA-ValueAdditive_efficiencies-PUT-methodParameters:

parameters
~~~~~~~~~~


Description:
............

Represents method parameters.
            "tolerance" represents the fraction for creating interval data (created interval: [data*(1-tolerance), data*(1+tolerance)]),
            "transformToUtilities" means if data should be tranformed into values from range [0-1],
            "boundariesProvided" means if inputsOutputs file contains information about min and max data for each factor,
            "functionShapeProvided" means if inputsOutputs file contains information about the shapes of value function for given factor.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** tolerance

  

  - **Constraint description:** The value should be non-negative.

  - **Type:** float
  - **Default value:** 0.00
- **Name:** transform to utilities

  

  - **Default value:** true
- **Name:** boundaries provided

  

  - **Default value:** false
- **Name:** function shapes provided

  

  - **Default value:** false

XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
    <programParameters>
        <parameter id="tolerance">
            <values>
                <value><real>%1</real></value>
            </values>
        </parameter>
        <parameter id="transformToUtilities">
            <values>
                <value><boolean>%2</boolean></value>
            </values>
		</parameter>
		<parameter id="boundariesProvided">
            <values>
                <value><boolean>%3</boolean></value>
            </values>
		</parameter>
		<parameter id="functionShapeProvided">
            <values>
                <value><boolean>%4</boolean></value>
            </values>
		</parameter>
    </programParameters>

----------------------------



.. _ImpreciseDEA-ValueAdditive_efficiencies-PUT_outputs:

Outputs
-------


- :ref:`minEfficiency <ImpreciseDEA-ValueAdditive_efficiencies-PUT-minEfficiency>`
- :ref:`maxEfficiency <ImpreciseDEA-ValueAdditive_efficiencies-PUT-maxEfficiency>`
- :ref:`minDistance <ImpreciseDEA-ValueAdditive_efficiencies-PUT-minDistance>`
- :ref:`maxDistance <ImpreciseDEA-ValueAdditive_efficiencies-PUT-maxDistance>`
- :ref:`messages <ImpreciseDEA-ValueAdditive_efficiencies-PUT-messages>`

.. _ImpreciseDEA-ValueAdditive_efficiencies-PUT-minEfficiency:

min efficiency
~~~~~~~~~~~~~~


Description:
............

A list of alternatives with computed minimum efficiency scores.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
						  <values>
                <value>[...]</value>
              </values>
						</alternativeValue>
						[...]
					</alternativesValues>

----------------------------


.. _ImpreciseDEA-ValueAdditive_efficiencies-PUT-maxEfficiency:

max efficiency
~~~~~~~~~~~~~~


Description:
............

A list of alternatives with computed maximum efficiency scores.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
						  <values>
                <value>[...]</value>
              </values>
						</alternativeValue>
						[...]
					</alternativesValues>

----------------------------


.. _ImpreciseDEA-ValueAdditive_efficiencies-PUT-minDistance:

min distance
~~~~~~~~~~~~


Description:
............

A list of alternatives with computed minimal distance to efficienct frontier.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
         <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
						  <values>
                <value>[...]</value>
              </values>
						</alternativeValue>
						[...]
					</alternativesValues>
       

----------------------------


.. _ImpreciseDEA-ValueAdditive_efficiencies-PUT-maxDistance:

max distance
~~~~~~~~~~~~


Description:
............

A list of alternatives with computed maximal distance to efficienct frontier.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
         <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
						  <values>
                <value>[...]</value>
              </values>
						</alternativeValue>
						[...]
					</alternativesValues>
       

----------------------------


.. _ImpreciseDEA-ValueAdditive_efficiencies-PUT-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
