:orphan:



.. _MRSort_inference_exact-R-MCDA:

Exact inference of MRSort
=========================

:Provider: R-MCDA
:Version: 1.0

Description
-----------

The MRSort method, a simplification of the Electre TRI method, uses the pessimistic assignment rule, without indifference or preference thresholds attached to criteria. Only a binary discordance condition is considered, i.e. a veto forbids an outranking in any possible concordance situation, or not. The identification of the profiles, weights and majority threshold are done by taking into account assignment examples.

- **Contact:** Alexandru Olteanu (alexandru.olteanu@univ-ubs.fr)



Inputs
------
(For outputs, see :ref:`below <MRSort_inference_exact-R-MCDA_outputs>`)


- :ref:`alternatives <MRSort_inference_exact-R-MCDA-inalt>`
- :ref:`performanceTable <MRSort_inference_exact-R-MCDA-inperf>`
- :ref:`criteria <MRSort_inference_exact-R-MCDA-incrit>`
- :ref:`alternativesAssignments <MRSort_inference_exact-R-MCDA-assignments>`
- :ref:`categoriesRanks <MRSort_inference_exact-R-MCDA-incategval>`
- :ref:`parameters <MRSort_inference_exact-R-MCDA-parameters>` *(optional)*

.. _MRSort_inference_exact-R-MCDA-inalt:

alternatives
~~~~~~~~~~~~


Description:
............

A complete list of alternatives to be considered when inferring the MR-Sort model.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _MRSort_inference_exact-R-MCDA-inperf:

performance table
~~~~~~~~~~~~~~~~~


Description:
............

The evaluations of the alternatives on the set of criteria.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _MRSort_inference_exact-R-MCDA-incrit:

criteria scales
~~~~~~~~~~~~~~~


Description:
............

A list of criteria on which the alternatives are evaluated. For each criterion, the preference direction should be provided.



XMCDA related:
..............

- **Tag:** criteriaScales

----------------------------


.. _MRSort_inference_exact-R-MCDA-assignments:

alternatives assignments
~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The alternatives assignments to categories.



XMCDA related:
..............

- **Tag:** alternativesAssignments

----------------------------


.. _MRSort_inference_exact-R-MCDA-incategval:

categories ranks
~~~~~~~~~~~~~~~~


Description:
............

A list of categories ranks, 1 stands for the most preferred category and the higher the number the lower the preference for that category.



XMCDA related:
..............

- **Tag:** categoriesValues

----------------------------


.. _MRSort_inference_exact-R-MCDA-parameters:

parameters
~~~~~~~~~~


Description:
............

The program parameters.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** Include vetoes

  *An indicator for whether vetoes should be included in the model or not.*

  - **Default value:** false
- **Name:** Readable weights

  *An indicator for whether the weights should made easier to read.*

  - **Default value:** false
- **Name:** Readable profiles

  *An indicator for whether the profiles should made easier to read.*

  - **Default value:** false

XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
			   
    <programParameters>
        <parameter id="veto">
            <values>
                <value>
                    <boolean>%1</boolean>
                </value>
            </values>
        </parameter>
        <parameter id="readableWeights">
            <values>
                <value>
                    <boolean>%2</boolean>
                </value>
            </values>
        </parameter>
        <parameter id="readableProfiles">
            <values>
                <value>
                    <boolean>%3</boolean>
                </value>
            </values>
        </parameter>
    </programParameters>

			

----------------------------



.. _MRSort_inference_exact-R-MCDA_outputs:

Outputs
-------


- :ref:`categoriesProfilesPerformanceTable <MRSort_inference_exact-R-MCDA-outcatprofpt>`
- :ref:`vetoProfilesPerformanceTable <MRSort_inference_exact-R-MCDA-outcatvetopt>`
- :ref:`criteriaWeights <MRSort_inference_exact-R-MCDA-weights>`
- :ref:`categoriesProfiles <MRSort_inference_exact-R-MCDA-outcatprof>`
- :ref:`vetoProfiles <MRSort_inference_exact-R-MCDA-outcatveto>`
- :ref:`majorityThreshold <MRSort_inference_exact-R-MCDA-majority>`
- :ref:`messages <MRSort_inference_exact-R-MCDA-msg>`

.. _MRSort_inference_exact-R-MCDA-outcatprofpt:

categories profiles performance table
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The evaluations of the category profiles.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _MRSort_inference_exact-R-MCDA-outcatvetopt:

veto profiles performance table
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The evaluations of the veto profiles.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _MRSort_inference_exact-R-MCDA-weights:

criteria weights
~~~~~~~~~~~~~~~~


Description:
............

The criteria weights.



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _MRSort_inference_exact-R-MCDA-outcatprof:

categories profiles
~~~~~~~~~~~~~~~~~~~


Description:
............

The categories delimiting profiles.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _MRSort_inference_exact-R-MCDA-outcatveto:

veto profiles
~~~~~~~~~~~~~


Description:
............

The categories veto profiles.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _MRSort_inference_exact-R-MCDA-majority:

majority threshold
~~~~~~~~~~~~~~~~~~


Description:
............

The majority threshold.



XMCDA related:
..............

- **Tag:** programParameters

----------------------------


.. _MRSort_inference_exact-R-MCDA-msg:

messages
~~~~~~~~


Description:
............

Messages from the execution of the webservice. Possible errors in the input data will be given here.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
