
:orphan:

.. _plotAlternativesAssignments-PyXMCDA:

plotAlternativesAssignments
===========================

:Provider: PyXMCDA
:Version: 1.1

Description
-----------

Generate plots representing provided alternativesAssignments as well as the dot files generating these plots.
Alternatives can be assigned to either a category, a categories set or a categories interval.
Graphviz dot program and format are used by this service.

- **Contact:** Nicolas Duminy (nicolas.duminy@telecom-bretagne.eu)

- **Web page:** https://gitlab.com/nduminy/ws-pyxmcda



Inputs
------
(For outputs, see :ref:`below <plotAlternativesAssignments-PyXMCDA_outputs>`)


- :ref:`alternatives <plotAlternativesAssignments-PyXMCDA-alternatives>` *(optional)*
- :ref:`categories <plotAlternativesAssignments-PyXMCDA-categories>` *(optional)*
- :ref:`categoriesSets <plotAlternativesAssignments-PyXMCDA-categoriesSets>` *(optional)*
- :ref:`alternativesAssignments <plotAlternativesAssignments-PyXMCDA-alternativesAssignments>`
- :ref:`categoriesValues <plotAlternativesAssignments-PyXMCDA-categoriesValues>` *(optional)*
- :ref:`parameters <plotAlternativesAssignments-PyXMCDA-parameters>` *(optional)*

.. _plotAlternativesAssignments-PyXMCDA-alternatives:

alternatives
~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The alternatives to be plotted. All are plotted if not provided.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _plotAlternativesAssignments-PyXMCDA-categories:

categories
~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

The categories to be plotted. All are plotted if not provided.



XMCDA related:
..............

- **Tag:** categories

----------------------------


.. _plotAlternativesAssignments-PyXMCDA-categoriesSets:

categories sets
~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

The categories sets assignable. Mandatory if alternatives are assigned to categoriesSets.



XMCDA related:
..............

- **Tag:** categoriesSets

----------------------------


.. _plotAlternativesAssignments-PyXMCDA-alternativesAssignments:

alternatives assignments
~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The alternatives ssignments to be plotted.



XMCDA related:
..............

- **Tag:** alternativesAssignments

----------------------------


.. _plotAlternativesAssignments-PyXMCDA-categoriesValues:

categories values
~~~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

The categories values ranking provided categories. Mandatory if alternatives are assigned to categoriesIntervals.
                Used to order categories in plot. Only read if categories are supplied.



XMCDA related:
..............

- **Tag:** categoriesValues

----------------------------


.. _plotAlternativesAssignments-PyXMCDA-parameters:

parameters
~~~~~~~~~~


Description:
............

Parameters of the method



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** Color

  *Color used in the plots.*

  - **Type:** drop-down list
  - **Possible values:**
      - Black (XMCDA label : black) (default)

      - White (XMCDA label : white) 

      - Red (XMCDA label : red) 

      - Green (XMCDA label : green) 

      - Blue (XMCDA label : blue) 

      - Magenta (XMCDA label : magenta) 

      - Yellow (XMCDA label : yellow) 

      - Cyan (XMCDA label : cyan) 

      - Orange (XMCDA label : orange) 

      - Pink (XMCDA label : pink) 

      - Brown (XMCDA label : brown) 

      - Gray (XMCDA label : gray) 

- **Name:** Shape

  *Shape use to represent categories.*

  - **Type:** drop-down list
  - **Possible values:**
      - Rectangle (XMCDA label : box) 

      - Circle (XMCDA label : circle) 

      - Diamond (XMCDA label : diamond) 

      - Ellipse (XMCDA label : ellipse) 

      - Oval (XMCDA label : oval) (default)

      - Polygon (XMCDA label : polygon) 

      - Triangle (XMCDA label : triangle) 

- **Name:** Image file extension

  *File extension of generated image figure.*

  - **Type:** drop-down list
  - **Possible values:**
      - .bmp (Windows Bitmap Format) (XMCDA label : bmp) 

      - .dia (DIA Format) (XMCDA label : dia) 

      - .fig (FIG graphics format) (XMCDA label : fig) 

      - .gif (Graphics Interchange Format) (XMCDA label : gif) 

      - .hpgl (Hewlett Packard Graphic Language 2) (XMCDA label : hpgl) 

      - .ico (Icon Image File Format) (XMCDA label : ico) 

      - .jpg (Joint Photographic Experts Group) (XMCDA label : jpg) 

      - .jpe (Joint Photographic Experts Group) (XMCDA label : jpe) 

      - .pdf (Portable Document Format) (XMCDA label : pdf) 

      - .png (Portable Network Graphics) (XMCDA label : png) (default)

      - .ps (PostScript) (XMCDA label : ps) 

      - .ps2 (PostScript for PDF) (XMCDA label : ps2) 

      - .svg (Scalable Vector Graphics) (XMCDA label : svg) 

      - .svgz (Compressed Scalable Vector Graphics) (XMCDA label : svgz) 

      - .tif (Tagged Image File Format) (XMCDA label : tif) 

- **Name:** Layout

  *Defines the plot layout.*

  - **Type:** drop-down list
  - **Possible values:**
      - Line (XMCDA label : line) (default)

      - Column (XMCDA label : column) 

      - Grid (XMCDA label : grid) 

- **Name:** Show values

  *Defines if values are shown (between parentheses) on the plots or not.*

  - **Default value:** false
- **Name:** Naming conventions

  *How categories and alternatives are labelled on the graph.*

  - **Type:** drop-down list
  - **Possible values:**
      - Only ids are shown (XMCDA label : id) 

      - Only names are shown (disambiguated by appending the ids, if needed) (XMCDA label : name) (default)

      - Names and ids are shown in that order (XMCDA label : name (id)) 

      - Ids and names are shown in that order (XMCDA label : id (name)) 


XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
    <programParameters>
        <programParameter id="color" name="Color">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="shape" name="Shape">
            <values>
                <value>
                    <label>%2</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="image_file_extension" name="Image file extension">
            <values>
                <value>
                    <label>%3</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="layout" name="Layout">
            <values>
                <value>
                    <label>%4</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="show_values" name="Show values">
            <values>
                <value>
                    <boolean>%5</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="naming_conventions" name="Naming conventions">
            <values>
                <value>
                    <label>%6</label>
                </value>
            </values>
        </programParameter>
    </programParameters>


----------------------------



.. _plotAlternativesAssignments-PyXMCDA_outputs:

Outputs
-------


- :ref:`glob:alternativesAssignments.{bmp,dia,fig,gif,hpgl,ico,jpg,jpe,pdf,png,ps,ps2,svg,svgz,tif} <plotAlternativesAssignments-PyXMCDA-alternativesAssignmentsPlot>`
- :ref:`alternativesAssignments.dot <plotAlternativesAssignments-PyXMCDA-alternativesAssignmentsPlotScript>`
- :ref:`messages <plotAlternativesAssignments-PyXMCDA-messages>`

.. _plotAlternativesAssignments-PyXMCDA-alternativesAssignmentsPlot:

alternatives assignments plot
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

Image containing all selected alternatives assignments plots. Format corresponds to the one given in parameters (default is .png).



XMCDA related:
..............

- **Tag:** None

----------------------------


.. _plotAlternativesAssignments-PyXMCDA-alternativesAssignmentsPlotScript:

alternatives assignments plot script
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

Generated graphviz dot script that made the image. Given to enable users to later customize the appearance of the plots.



XMCDA related:
..............

- **Tag:** None

----------------------------


.. _plotAlternativesAssignments-PyXMCDA-messages:

messages
~~~~~~~~


Description:
............

Status messages.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
