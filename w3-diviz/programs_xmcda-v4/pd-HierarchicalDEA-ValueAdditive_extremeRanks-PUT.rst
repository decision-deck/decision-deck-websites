:orphan:



.. _HierarchicalDEA-ValueAdditive_extremeRanks-PUT:

HierarchicalDEA-ValueAdditive_extremeRanks
==========================================

:Provider: PUT
:Version: 1.0

Description
-----------

Computes extreme efficiency ranks for the given DMUs (alternatives) using Additive Data Envelopment Analysis Model with hierarchical structure of inputs and outputs.

- **Contact:** 
            Anna Labijak <anna.labijak@cs.put.poznan.pl>
        



Inputs
------
(For outputs, see :ref:`below <HierarchicalDEA-ValueAdditive_extremeRanks-PUT_outputs>`)


- :ref:`units <HierarchicalDEA-ValueAdditive_extremeRanks-PUT-units>`
- :ref:`inputsOutputsScales <HierarchicalDEA-ValueAdditive_extremeRanks-PUT-inputsOutputsScales>`
- :ref:`performanceTable <HierarchicalDEA-ValueAdditive_extremeRanks-PUT-performanceTable>`
- :ref:`hierarchy <HierarchicalDEA-ValueAdditive_extremeRanks-PUT-hierarchy>`
- :ref:`weightsLinearConstraints <HierarchicalDEA-ValueAdditive_extremeRanks-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <HierarchicalDEA-ValueAdditive_extremeRanks-PUT-methodParameters>`

.. _HierarchicalDEA-ValueAdditive_extremeRanks-PUT-units:

units
~~~~~


Description:
............

A list of alternatives (DMUs).



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
                <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
            

----------------------------


.. _HierarchicalDEA-ValueAdditive_extremeRanks-PUT-inputsOutputsScales:

inputs/outputs scales
~~~~~~~~~~~~~~~~~~~~~


Description:
............

Information about inputs and outpus (leaf criteria) scales (preference directions) and optionally about boundaries



XMCDA related:
..............

- **Tag:** criteriaScales

- **Code:**

  ::

    
                
<criteriaScales>
    <criterionScale>
      <criterionID>[...]</criterionID>
      <scales>
        <scale>
          [...]
        </scale>
      </scales>
    </criterionScale>
    [...]
</criteriaScales>
            

----------------------------


.. _HierarchicalDEA-ValueAdditive_extremeRanks-PUT-performanceTable:

performance
~~~~~~~~~~~


Description:
............

A list of alternatives (DMUs) performances (on leaf hierarchy criteria).



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    
                <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<values><value>[...]</value></values>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>
            

----------------------------


.. _HierarchicalDEA-ValueAdditive_extremeRanks-PUT-hierarchy:

hierarchy
~~~~~~~~~


Description:
............

The hierarchical structure of criteria.



XMCDA related:
..............

- **Tag:** criteriaHierarchy

- **Code:**

  ::

    
                <criteriaHierarchy>
						<nodes>
                            <node>
                                <criterionID>[...]</criterionID>
                                <nodes>
                                    <node>
                                        <criterionID>[...]</criterionID>
                                        [...]
                                    </node>
                                    [...]
                                </nodes>
                            </node>
                        <nodes>
					</criteriaHierarchy>
            

----------------------------


.. _HierarchicalDEA-ValueAdditive_extremeRanks-PUT-weightsLinearConstraints:

weights constraints
~~~~~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

A list of hierarchy criteria linear constraints.



XMCDA related:
..............

- **Tag:** criteriaLinearConstraints

- **Code:**

  ::

    
                
            <criteriaLinearConstraints>
                <constraints>
                    <constraint>
                        <elements>
                            <element>
                                <criterionID> [...] </criterionID>
                                <coefficient>
                                    [...]
                                </coefficient>
                            </element>
                            [...]
                        </elements>
                        <operator>[...]</operator>
                        <rhs>
                            [...]
                        </rhs>
                    </constraint>
                    [...]
                </constraints>
            </criteriaLinearConstraints>
            

----------------------------


.. _HierarchicalDEA-ValueAdditive_extremeRanks-PUT-methodParameters:

parameters
~~~~~~~~~~


Description:
............

Represents parameters.
                "hierarchy node" is the ID of the hierarchy criterion for which the analysis should be performed;
                "transformToUtilities" means if data should be tranformed into values from range [0-1];
                "boundariesProvided" means if inputsOutputs file contains information about min and max data for each factor.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** hierarchy node

  

  - **Type:** string
  - **Default value:** "root"
- **Name:** transform to utilities

  

  - **Default value:** true
- **Name:** boundaries provided

  

  - **Default value:** false

XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
                
    <programParameters>
        <parameter id="hierarchyNode">
            <values>
                <value><label>%1</label></value>
            </values>
        </parameter>
        <parameter id="transformToUtilities">
      <values>
        <value><boolean>%2</boolean></value>
      </values>
		</parameter>
		<parameter id="boundariesProvided">
      <values>
        <value><boolean>%3</boolean></value>
      </values>
		</parameter>
    </programParameters>
            

----------------------------



.. _HierarchicalDEA-ValueAdditive_extremeRanks-PUT_outputs:

Outputs
-------


- :ref:`bestRank <HierarchicalDEA-ValueAdditive_extremeRanks-PUT-bestRank>`
- :ref:`worstRank <HierarchicalDEA-ValueAdditive_extremeRanks-PUT-worstRank>`
- :ref:`messages <HierarchicalDEA-ValueAdditive_extremeRanks-PUT-messages>`

.. _HierarchicalDEA-ValueAdditive_extremeRanks-PUT-bestRank:

best rank
~~~~~~~~~


Description:
............

A list of alternatives with computed best rank for each of them.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
                <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
						  <values>
							 <value>[...]</value>
						  </values>
						</alternativeValue>
						[...]
					</alternativesValues>
            

----------------------------


.. _HierarchicalDEA-ValueAdditive_extremeRanks-PUT-worstRank:

worst rank
~~~~~~~~~~


Description:
............

A list of alternatives with computed worst rank for each of them.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
                <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
						  <values>
							 <value>[...]</value>
						  </values>
						</alternativeValue>
						[...]
					</alternativesValues>
            

----------------------------


.. _HierarchicalDEA-ValueAdditive_extremeRanks-PUT-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
