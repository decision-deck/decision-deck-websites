:orphan:



.. _plotAlternativesValuesPreorder-PyXMCDA:

plotAlternativesValuesPreorder
==============================

:Provider: PyXMCDA
:Version: 1.1

Description
-----------

Generate directed graph from provided alternativesValues as well as the dot script generating the graph.

- **Contact:** Nicolas Duminy (nicolas.duminy@telecom-bretagne.eu)

- **Web page:** https://gitlab.com/nduminy/ws-pyxmcda



Inputs
------
(For outputs, see :ref:`below <plotAlternativesValuesPreorder-PyXMCDA_outputs>`)


- :ref:`alternatives <plotAlternativesValuesPreorder-PyXMCDA-alternatives>` *(optional)*
- :ref:`alternativesValues <plotAlternativesValuesPreorder-PyXMCDA-alternativesValues>`
- :ref:`parameters <plotAlternativesValuesPreorder-PyXMCDA-parameters>` *(optional)*

.. _plotAlternativesValuesPreorder-PyXMCDA-alternatives:

alternatives
~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The alternatives to be plotted. All are plotted if not provided.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _plotAlternativesValuesPreorder-PyXMCDA-alternativesValues:

alternatives values
~~~~~~~~~~~~~~~~~~~


Description:
............

The alternatives values.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _plotAlternativesValuesPreorder-PyXMCDA-parameters:

parameters
~~~~~~~~~~


Description:
............

Parameters of the method



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** Chart title

  *Title of the chart to be plotted.*

  - **Type:** string
  - **Default value:** "Alternatives Values preorder"
- **Name:** Color

  *Color used in the plots.*

  - **Type:** drop-down list
  - **Possible values:**
      - Black (XMCDA label : black) (default)

      - White (XMCDA label : white) 

      - Red (XMCDA label : red) 

      - Green (XMCDA label : green) 

      - Blue (XMCDA label : blue) 

      - Magenta (XMCDA label : magenta) 

      - Yellow (XMCDA label : yellow) 

      - Cyan (XMCDA label : cyan) 

      - Orange (XMCDA label : orange) 

      - Pink (XMCDA label : pink) 

      - Brown (XMCDA label : brown) 

      - Gray (XMCDA label : gray) 

- **Name:** Shape

  *Shape use to represent categories.*

  - **Type:** drop-down list
  - **Possible values:**
      - Rectangle (XMCDA label : box) 

      - Circle (XMCDA label : circle) 

      - Diamond (XMCDA label : diamond) 

      - Ellipse (XMCDA label : ellipse) 

      - Oval (XMCDA label : oval) (default)

      - Polygon (XMCDA label : polygon) 

      - Triangle (XMCDA label : triangle) 

- **Name:** Image file extension

  *File extension of generated image figure.*

  - **Type:** drop-down list
  - **Possible values:**
      - .bmp (Windows Bitmap Format) (XMCDA label : bmp) 

      - .dia (DIA Format) (XMCDA label : dia) 

      - .fig (FIG graphics format) (XMCDA label : fig) 

      - .gif (Graphics Interchange Format) (XMCDA label : gif) 

      - .hpgl (Hewlett Packard Graphic Language 2) (XMCDA label : hpgl) 

      - .ico (Icon Image File Format) (XMCDA label : ico) 

      - .jpg (Joint Photographic Experts Group) (XMCDA label : jpg) 

      - .jpeg (Joint Photographic Experts Group) (XMCDA label : jpeg) 

      - .jpe (Joint Photographic Experts Group) (XMCDA label : jpe) 

      - .pdf (Portable Document Format) (XMCDA label : pdf) 

      - .png (Portable Network Graphics) (XMCDA label : png) (default)

      - .ps (PostScript) (XMCDA label : ps) 

      - .ps2 (PostScript for PDF) (XMCDA label : ps2) 

      - .svg (Scalable Vector Graphics) (XMCDA label : svg) 

      - .svgz (Compressed Scalable Vector Graphics) (XMCDA label : svgz) 

      - .tif (Tagged Image File Format) (XMCDA label : tif) 

      - .tiff (Tagged Image File Format) (XMCDA label : tiff) 

- **Name:** Layout

  *Defines the plot layout.*

  - **Type:** drop-down list
  - **Possible values:**
      - Line (XMCDA label : line) 

      - Column (XMCDA label : column) (default)

- **Name:** Reverse order

  *Defines if order direction should be reversed (descending order) or not.*

  - **Default value:** false
- **Name:** Naming conventions

  *How categories and alternatives are labelled on the graph.*

  - **Type:** drop-down list
  - **Possible values:**
      - Only ids are shown (XMCDA label : id) 

      - Only names are shown (disambiguated by appending the ids, if needed) (XMCDA label : name) (default)

      - Names and ids are shown in that order (XMCDA label : name (id)) 

      - Ids and names are shown in that order (XMCDA label : id (name)) 


XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
    <programParameters>
        <programParameter id="chart_title" name="Chart title">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="color" name="Color">
            <values>
                <value>
                    <label>%2</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="shape" name="Shape">
            <values>
                <value>
                    <label>%3</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="image_file_extension" name="Image file extension">
            <values>
                <value>
                    <label>%4</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="layout" name="Layout">
            <values>
                <value>
                    <label>%5</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="reverse_order" name="Reverse order">
            <values>
                <value>
                    <boolean>%6</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="naming_conventions" name="Naming conventions">
            <values>
                <value>
                    <label>%7</label>
                </value>
            </values>
        </programParameter>
    </programParameters>


----------------------------



.. _plotAlternativesValuesPreorder-PyXMCDA_outputs:

Outputs
-------


- :ref:`glob:alternativesValuesPreorder.{bmp,dia,fig,gif,hpgl,ico,jpg,jpeg,jpe,pdf,png,ps,ps2,svg,svgz,tif,tiff} <plotAlternativesValuesPreorder-PyXMCDA-alternativesValuesPreorderPlot>`
- :ref:`alternativesValuesPreorder.dot <plotAlternativesValuesPreorder-PyXMCDA-alternativesValuesPreorderPlotScript>`
- :ref:`messages <plotAlternativesValuesPreorder-PyXMCDA-messages>`

.. _plotAlternativesValuesPreorder-PyXMCDA-alternativesValuesPreorderPlot:

alternatives values preorder plot
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

Image containing all selected alternatives values. Format corresponds to the one given in parameters (default is .png).



XMCDA related:
..............

- **Tag:** None

----------------------------


.. _plotAlternativesValuesPreorder-PyXMCDA-alternativesValuesPreorderPlotScript:

alternatives values preorder plot script
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

Generated graphviz dot script that made the graph. Given to enable users to later customize the appearance of the plots.



XMCDA related:
..............

- **Tag:** None

----------------------------


.. _plotAlternativesValuesPreorder-PyXMCDA-messages:

messages
~~~~~~~~


Description:
............

Status messages.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
