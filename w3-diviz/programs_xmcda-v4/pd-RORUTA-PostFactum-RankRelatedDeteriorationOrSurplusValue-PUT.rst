:orphan:



.. _RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue-PUT:

RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue
========================================================

:Provider: PUT
:Version: 1.0

Description
-----------

For a given ranking position n and the alternative a that already attains at least n-th position in ranking, depending on the need, the module find a one of the two possible types of values. These are the maximal deterioration of the performance of the alternative a or the maximal surplus value of the comprehensive score of this alternative. In the first case, the procedure finds a minimal value from the range of 0 to 1, that multiplied by the performances on the indicated criteria allows to maintain at least the desired n-th position. In the other case, it finds the maximal value that may be subtracted from the comprehensive score of this alternative in order to maintain at least n-th position. In both cases it is possible to consider the target for both all or at least one compatible value funtion.

- **Contact:** Pawel Rychly (pawelrychly@gmail.com).



Inputs
------
(For outputs, see :ref:`below <RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue-PUT_outputs>`)


- :ref:`criteria <RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue-PUT-criteria>`
- :ref:`selected-criteria <RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue-PUT-selected-criteria>` *(optional)*
- :ref:`alternatives <RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue-PUT-alternatives>`
- :ref:`performances <RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue-PUT-performances>`
- :ref:`characteristic-points <RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue-PUT-characteristic-points>` *(optional)*
- :ref:`preferences <RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue-PUT-preferences>` *(optional)*
- :ref:`intensities-of-preferences <RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue-PUT-intensities-of-preferences>` *(optional)*
- :ref:`target <RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue-PUT-target>`
- :ref:`rank-related-requirements <RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue-PUT-rank-related-requirements>` *(optional)*
- :ref:`parameters <RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue-PUT-parameters>`

.. _RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue-PUT-criteria:

criteria
~~~~~~~~


Description:
............

A list of all considered criteria. The input value should be a valid XMCDA document whose main tag is criteria.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
                
                    <criteria>
                        <criterion id="%1" name="%1"></criterion>
                        [...]
                    </criteria>
                    
            

----------------------------


.. _RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue-PUT-selected-criteria:

selected criteria
~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A set of ids of the criteria that should be taken into account for modification of the performances. This parameter is used only if the modification of performances is searched.



XMCDA related:
..............

- **Tag:** criteriaSets

- **Code:**

  ::

    
	<criteriaSets>
		<criteriaSet>
			<element>
				<criterionID>...</criterionID>
			</element>
			<element>
				<criterionID>...</criterionID>
			</element>
			[...]
		</criteriaSet>
	</criteriaSets>


----------------------------


.. _RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue-PUT-alternatives:

alternatives
~~~~~~~~~~~~


Description:
............

The list of all considered alternatives. The input value should be a valid XMCDA document whose main tag is alternatives. Each alternative may be described using two attributes: id and name. While the first one denotes a machine readable name, the second represents a human readable name.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
<alternatives>
	<alternative id="..." name="..." />
	[...]
</alternatives>


----------------------------


.. _RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue-PUT-performances:

performances
~~~~~~~~~~~~


Description:
............

Description of evaluation of alternatives on different criteria. It is required to provide the IDs of both criteria and alternatives described previously. The input value should be provided as a valid XMCDA document whose main tag is performanceTable



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    
<performanceTable>
	<alternativePerformances>
		<alternativeID>a01</alternativeID>
		<performance>
			<criterionID>c01</criterionID>
			<values>
				<value>
					<real>1.0</real>
				</value>
			</values>
		</performance>
		[...]
	</alternativePerformances>
	[...]
<performanceTable>


----------------------------


.. _RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue-PUT-characteristic-points:

characteristic points
~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A set of values associated with the criteria. This input allows to determine what type of value function should be used for the particular criterion. For each criterion that has an associated greater than one value, a piecewise linear value function is used. In this case, the mentioned value denotes a number of characteristic points of this value function. For the criteria that are not listed in this file, or for these for which the provided values are lower than two uses a general value function. The input value should be provided as a valid XMCDA document whose main tag is criteriaValues. Each element should contain both an id of the criterion, and value tag.



XMCDA related:
..............

- **Tag:** criteriaValues

- **Code:**

  ::

    
<criteriaValues mcdaConcept="characteristicPoints">
	<criterionValue>
		<criterionID>c01</criterionID>
		<values>
			<value>
				<integer>2</integer>
			</value>
		</values>
	</criterionValue>
	[...]
</criteriaValues>


----------------------------


.. _RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue-PUT-preferences:

preferences
~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Set of pairwise comparisons of reference alternatives. For a pair of alternatives three types of comparisons are supported. These are the strict preference, weak preference, and indifference. The input value should be provided as a valid XMCDA document whose main tag is alternativesComparisons. For each type of comparison, a separate alternativesComparisons tag should be used. Within these groups a mentioned types are denoted using a comparisonType tag by respectively strict, weak, and indif label. Comparisons should be provided as pairs of alternatives ids.



XMCDA related:
..............

- **Tag:** alternativesMatrix

- **Code:**

  ::

    
                
                    <alternativesComparisons>
                        <comparisonType>
                            %1<!-- type of preference: strong, weak, or indif -->
                        </comparisonType>
                        <pairs>
                            <pair>
                                <initial>
                                    <alternativeID>%2</alternativeID>
                                </initial>
                                <terminal>
                                    <alternativeID>%3</alternativeID>
                                </terminal>
                            </pair>
                            [...]
                        </pairs>
                    </alternativesComparisons>
                    [...]
                    
            

----------------------------


.. _RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue-PUT-intensities-of-preferences:

intensities-of-preferences
~~~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Set of comparisons of intensities of preference. For a pair of preference relations three types of comparisons are supported. These are the strict preference, weak, preference, and indifference. The input value should be provided as a valid XMCDA document whose main tag is alternativesComparisons. For each type of comparison, a separate alternativesComparisons tag should be used. Within these groups  aforementioned types are denoted using a comparisonType tag by respectively strict, weak, and indif label. Comparisons should be provided as pairs of two elementary sets of alternatives ids. The following form is expected:



XMCDA related:
..............

- **Tag:** alternativesMatrix

- **Code:**

  ::

    
                
                    <alternativesComparisons>
                        <comparisonType>%1</comparisonType>
                        <pairs>
                            <pair>
                                <initial>
                                    <alternativesSet>
                                        <element>
                                            <alternativeID>%2</alternativeID>
                                        </element>
                                        <element>
                                            <alternativeID>%3</alternativeID>
                                        </element>
                                    </alternativesSet>
                                </initial>
                                <terminal>
                                    <alternativesSet>
                                        <element>
                                            <alternativeID>%4</alternativeID>
                                        </element>
                                        <element>
                                            <alternativeID>%5</alternativeID>
                                        </element>
                                    </alternativesSet>
                                </terminal>
                            </pair>
                            [...]
                        </pairs>
                    </alternativesComparisons>
                    [...]
                    
            

----------------------------


.. _RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue-PUT-target:

target
~~~~~~


Description:
............

Description of the target to achieve which has a form of the worst possible position that may be attained by the particular alternative. The input value should be provided as a valid XMCDA document whose main tag is alternativesValues. It should contain one alternative Id and one integer value which denotes the desired ranking position.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
<alternativesValues>
	<alternativeValue>
		<alternativeID>...</alternativeID>
		<values>
			<value>
				<integer>...</integer>
			</value>
		</values>
	</alternativeValue>
</alternativesValues>


----------------------------


.. _RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue-PUT-rank-related-requirements:

rank-related-requirements
~~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Set of rank-related requirements. In other words it is a set of  ranges of possible positions in the final ranking for a chosen alternatives. The input value should be provided as a valid XMCDA document whose main tag is alternativesValues. Each requirement should contain both an id of the reffered alternative and a pair of values that denote the desired range. These information should be provided within a separate alternativesValues tag.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
<alternativesValues>
	<alternativeValue>
		<alternativeID>...</alternativeID>
		<value>
			<interval>
				<lowerBound><integer>...</integer></lowerBound>
				<upperBound><integer>...</integer></upperBound>
			</interval>
		</value>
	</alternativeValue>
	[...]
 </alternativesValues>


----------------------------


.. _RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue-PUT-parameters:

parameters
~~~~~~~~~~


Description:
............

Method parameters
                strict %1 - Single boolean value. Determines whether to use sctrictly increasing (true) or monotonously increasing (false) value functions.
                type_of_result %2 - Determines whether analyse the modification of performances or comprehensive score.
                precision %3 - A float value between 0 and 1 (bounds excluded). It describes the precision of expected result.
                possible_or_necessary %4 - One of the two label values (possible or necessary). This parameter determines does target should be satisfied by one (possible) or all(necessary) compatible value functions.



GUI information:
................


- **Name:** Use strictly increasing value functions?

  *Single boolean value. Determines whether to use sctrictly increasing (true) or monotonously increasing (false) value functions.*

  - **Default value:** false
- **Name:** type of result

  *Type of searched value*

  - **Type:** drop-down list
  - **Possible values:**
      - deterioration (XMCDA label : deterioration) (default)

      - surplus-value (XMCDA label : surplus-value) 

- **Name:** precision of the result

  

  - **Constraint description:** A float value between 0 and 1 (bounds excluded). It denotes the expected precision of the result.

  - **Type:** float
  - **Default value:** 0.005
- **Name:** Whether the target should be achieved possibly or necessarily?

  *One of the two label values (possible or necessary). This parameter determines does target should be satisfied by one (possible) or all(necessary) value functions*

  - **Type:** drop-down list
  - **Possible values:**
      - possible (XMCDA label : possible) 

      - necessary (XMCDA label : necessary) (default)


XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
<programParameters>
	<parameter name="strict">
		<values>
			<value>
				<boolean>%1</boolean>
			</value>
		</values>
	</parameter>
	<parameter name="type_of_result">
		<values>
			<value>
				<label>%2</label>
			</value>
		</values>
	</parameter>
	<parameter name="precision">
		<values>
			<value>
				<real>%3</real>
			</value>
		</values>
	</parameter>
	<parameter name="possible_or_necessary">
		<values>
			<value>
				<label>%4</label>
			</value>
		</values>
	</parameter>
</programParameters>


----------------------------



.. _RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue-PUT_outputs:

Outputs
-------


- :ref:`result <RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue-PUT-result>`
- :ref:`messages <RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue-PUT-messages>`

.. _RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue-PUT-result:

result
~~~~~~


Description:
............

A real value associated with the id of the considered alternative. Depending on chosen option, it may denote the deterioration of performances of this alternative or the surplus value of its comprehensive score. The output value is provided as a valid XMCDA document whose main tag is alternativesValues.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
<alternativesValues>
	<alternativeValue>
		<alternativeID>...</alternativeID>
		<values>
			<value>
				<real>...</real>
			</value>
		</values>
	</alternativeValue>
</alternativesValues>


----------------------------


.. _RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue-PUT-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
