:orphan:



.. _plotAlternativesValues-ITTB:

plotAlternativesValues
======================

:Provider: ITTB
:Version: 2.0

Description
-----------

This web service generates a barplot or a pie plot representing a numeric quantity for each alternative, like, e.g., an importance value. Compared to the web service plotAlternativesValues, some parameters are added. Colors can be used and the title of the plot can be typed. In the case of a bar chart, the axis-labels can also be typed. The alternatives' evaluations are supposed to be real or integer numeric values.

- **Contact:** Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)



Inputs
------
(For outputs, see :ref:`below <plotAlternativesValues-ITTB_outputs>`)


- :ref:`alternatives <plotAlternativesValues-ITTB-alternatives>`
- :ref:`alternativesValues <plotAlternativesValues-ITTB-alternativesValues>`
- :ref:`parameters <plotAlternativesValues-ITTB-parameters>`

.. _plotAlternativesValues-ITTB-alternatives:

alternatives
~~~~~~~~~~~~


Description:
............

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
	<alternatives>
		<alternative>
			<active>[...]</active>
			[...]
		</alternative>
		[...]
	</alternatives>


----------------------------


.. _plotAlternativesValues-ITTB-alternativesValues:

alternativesValues
~~~~~~~~~~~~~~~~~~


Description:
............

A list of <alternativesValue> representing a certain numeric quantity for each alternative, like, e.g., an overall value.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _plotAlternativesValues-ITTB-parameters:

parameters
~~~~~~~~~~


Description:
............

Plot type method: choose between "Bar chart" and "Pie chart". The default plot is a bar chart.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** Chart type:

  *Type of the plot: choose between "Bar chart" and "Pie chart". The default plot is a bar chart.*

  - **Type:** drop-down list
  - **Possible values:**
      - bar chart (XMCDA label : barChart) (default)

      - pie chart (XMCDA label : pieChart) 

- **Name:** Order by:

  *Choose between "name", "id" or "values".*

  - **Type:** drop-down list
  - **Possible values:**
      - name (XMCDA label : name) 

      - id (XMCDA label : id) 

      - values (XMCDA label : values) (default)

- **Name:** Order:

  *The parameter which says if the highest or lowest value is to be placed first.*

  - **Type:** drop-down list
  - **Possible values:**
      - increasing (XMCDA label : increasing) (default)

      - decreasing (XMCDA label : decreasing) 

- **Name:** Colors:

  *The use of colors: true for a colored plot.*

  - **Type:** drop-down list
  - **Possible values:**
      - gradient (XMCDA label : true) 

      - black and white (XMCDA label : false) (default)

- **Name:** Initial color:

  *String that indicates the initial color in the generated barplot or pieplot.Choose between "Black", "Red", "Blue", "Green", "Yellow", "Magenta" and "Cyan".*

  - **Type:** drop-down list
  - **Possible values:**
      - black (XMCDA label : black) (default)

      - red (XMCDA label : red) 

      - blue (XMCDA label : blue) 

      - green (XMCDA label : green) 

      - yellow (XMCDA label : yellow) 

      - magenta (XMCDA label : magenta) 

      - cyan (XMCDA label : cyan) 

      - white (XMCDA label : white) 

- **Name:** Final color:

  *String that indicates the final color in the generated barplot or pieplot. choose between "Black","White", "Red", "Blue", "Green", "Yellow", "Magenta" and "Cyan".*

  - **Type:** drop-down list
  - **Possible values:**
      - black (XMCDA label : black) (default)

      - red (XMCDA label : red) 

      - blue (XMCDA label : blue) 

      - green (XMCDA label : green) 

      - yellow (XMCDA label : yellow) 

      - magenta (XMCDA label : magenta) 

      - cyan (XMCDA label : cyan) 

      - white (XMCDA label : white) 

- **Name:** Chart title:

  *String for the title of the plot. The default value is an empty field.*

  - **Type:** string
  - **Default value:** ""
- **Name:** X axis label:

  *String for the horizontal axis-label.The default value is an empty field.*

  - **Type:** string
  - **Default value:** ""
- **Name:** Y axis label:

  *String for the vertical axis-label.The default value is an empty field.*

  - **Type:** string
  - **Default value:** ""

XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
	<programParameters>
		<parameter id="chart_type" name="Chart type">
			<values>
				<value>
					<label>%1</label>
				 </value>
			 </values>
		</parameter>
		<parameter id="order_by" name="Order by">
			<values>
				<value>
					<label>%2</label>
				 </value>
			 </values>
		</parameter>
		<parameter id="order" name="Order">
			<values>
				<value>
					<label>%3</label>
				 </value>
			 </values>
		</parameter>
		<parameter id="use_color" name="Colors in the chart">
			<values>
				<value>
					<label>%4</label>
				 </value>
			 </values>
		</parameter>
		<parameter id="initial_color" name="Initial color">
			<values>
				<value>
					<label>%5</label>
				 </value>
			 </values>
		</parameter>
		<parameter id="final_color" name="Final color">
			<values>
				<value>
					<label>%6</label>
				 </value>
			 </values>
		</parameter>
		<parameter id="chart_title" name="Chart title">
			<values>
				<value>
					<label>%7</label>
				 </value>
			 </values>
		</parameter>
		<parameter id="domain_axis" name="Domain axis label">
			<values>
				<value>
					<label>%8</label>
				 </value>
			 </values>
		</parameter >
		<parameter id="range_axis" name="Range axis label">
			<values>
				<value>
					<label>%9</label>
				 </value>
			 </values>
		</parameter>
	</programParameters>


----------------------------



.. _plotAlternativesValues-ITTB_outputs:

Outputs
-------


- :ref:`alternativesValues.png <plotAlternativesValues-ITTB-png>`
- :ref:`messages <plotAlternativesValues-ITTB-messages>`

.. _plotAlternativesValues-ITTB-png:

graph (png)
~~~~~~~~~~~


Description:
............

The generated barplot or pieplot as a PNG image.



XMCDA related:
..............

- **Tag:** other

----------------------------


.. _plotAlternativesValues-ITTB-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
