
:orphan:

.. _convertPerformanceTableToNumerics-PyXMCDA:

convertPerformanceTableToNumerics
=================================

:Provider: PyXMCDA
:Version: 1.1

Description
-----------

Numerize performance table using provided criteriaScales.

N.B: Nominal scales are not supported.

- **Contact:** Nicolas Duminy (nicolas.duminy@telecom-bretagne.eu)

- **Web page:** https://gitlab.com/nduminy/ws-pyxmcda



Inputs
------
(For outputs, see :ref:`below <convertPerformanceTableToNumerics-PyXMCDA_outputs>`)


- :ref:`performanceTable <convertPerformanceTableToNumerics-PyXMCDA-performanceTable>`
- :ref:`criteriaScales <convertPerformanceTableToNumerics-PyXMCDA-criteriaScales>`
- :ref:`parameters <convertPerformanceTableToNumerics-PyXMCDA-parameters>` *(optional)*

.. _convertPerformanceTableToNumerics-PyXMCDA-performanceTable:

performance table
~~~~~~~~~~~~~~~~~


Description:
............

The performance table.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _convertPerformanceTableToNumerics-PyXMCDA-criteriaScales:

criteria scales
~~~~~~~~~~~~~~~


Description:
............

The criteria scales defined.



XMCDA related:
..............

- **Tag:** criteriaScales

----------------------------


.. _convertPerformanceTableToNumerics-PyXMCDA-parameters:

parameters
~~~~~~~~~~


Description:
............

Parameters of the method



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** Normalize

  *Defines if performances should be normalized or not.*

  - **Default value:** false

XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
    <programParameters>
        <programParameter id="normalize" name="Normalize">
            <values>
                <value>
                    <boolean>%1</boolean>
                </value>
            </values>
        </programParameter>
    </programParameters>


----------------------------



.. _convertPerformanceTableToNumerics-PyXMCDA_outputs:

Outputs
-------


- :ref:`performanceTable <convertPerformanceTableToNumerics-PyXMCDA-numericPerformanceTable>`
- :ref:`messages <convertPerformanceTableToNumerics-PyXMCDA-messages>`

.. _convertPerformanceTableToNumerics-PyXMCDA-numericPerformanceTable:

performance table
~~~~~~~~~~~~~~~~~


Description:
............

The numerized performance table.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _convertPerformanceTableToNumerics-PyXMCDA-messages:

messages
~~~~~~~~


Description:
............

Status messages.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
