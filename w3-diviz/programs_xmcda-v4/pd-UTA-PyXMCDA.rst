
:orphan:

.. _UTA-PyXMCDA:

UTA
===

:Provider: PyXMCDA
:Version: 1.0

Description
-----------

Perform UTA computation on provided inputs.
Decision Maker's Preferences can be provided using either a global ranking of reference alternatives, or the pair-wise alternatives preferences and indifferences.
Outputs optimal valueFunctions along their valuesErrors.
Can also apply post-optimality analysis for a more balanced output.

The service generate discrete functions for criteria with qualitative scales.
Number of segments will be still be used to compute a continuous marginal utility function.
It is therefore advised to use (n - 1) as the number of segments (with n the number of labels of the qualitative scale), while using incremented integer values in such scale definition (e.g. \{ 1, 2, 3,..., n\}).
Labels of the scale are used as the abscissa.

This service raises an error if the solution found does not give the same ranking of reference alternatives than in inputs (even with application of the values errors, called sigma in (Munda, 2005).

The implementation and indexing conventions are based on:

    Munda, G. (2005). UTA Methods.
    In Multiple criteria decision analysis: State of the art surveys
    (pp 297-343). Springer, New York, NY.

N.B: This service uses the python module Pulp for representing and solving UTA problems.

- **Contact:** Nicolas Duminy (nicolas.duminy@telecom-bretagne.eu)

- **Web page:** https://gitlab.com/nduminy/ws-pyxmcda



Inputs
------
(For outputs, see :ref:`below <UTA-PyXMCDA_outputs>`)


- :ref:`alternatives <UTA-PyXMCDA-alternatives>` *(optional)*
- :ref:`criteria <UTA-PyXMCDA-criteria>` *(optional)*
- :ref:`performanceTable <UTA-PyXMCDA-performanceTable>`
- :ref:`alternativesPreferences <UTA-PyXMCDA-alternativesPreferences>` *(optional)*
- :ref:`alternativesIndifferences <UTA-PyXMCDA-alternativesIndifferences>` *(optional)*
- :ref:`alternativesRanks <UTA-PyXMCDA-alternativesRanks>` *(optional)*
- :ref:`criteriaScales <UTA-PyXMCDA-criteriaScales>` *(optional)*
- :ref:`criteriaSegments <UTA-PyXMCDA-criteriaSegments>`
- :ref:`parameters <UTA-PyXMCDA-parameters>`

.. _UTA-PyXMCDA-alternatives:

alternatives
~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The alternatives. Only used to exclude inactive alternatives from computations (all are considered if not provided).



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _UTA-PyXMCDA-criteria:

criteria
~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The criteria. Only used to exclude inactive criteria from computations (all are considered if not provided).



XMCDA related:
..............

- **Tag:** criteria

----------------------------


.. _UTA-PyXMCDA-performanceTable:

performance table
~~~~~~~~~~~~~~~~~


Description:
............

The performance table.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _UTA-PyXMCDA-alternativesPreferences:

alternatives preferences
~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The alternatives preferences used to infer utility functions.
                If alternatives is provided, all reference alternatives set as inactive are not considered.



XMCDA related:
..............

- **Tag:** alternativesMatrix

----------------------------


.. _UTA-PyXMCDA-alternativesIndifferences:

alternatives indifferences
~~~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The alternatives indifferences used to infer utility functions.
                If alternatives is provided, all reference alternatives set as inactive are not considered.



XMCDA related:
..............

- **Tag:** alternativesMatrix

----------------------------


.. _UTA-PyXMCDA-alternativesRanks:

alternatives ranks
~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Ranking of reference alternatives used to infer utility functions.
                If alternatives is provided, all reference alternatives set as inactive are not considered.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _UTA-PyXMCDA-criteriaScales:

criteria scales
~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The criteria scales. Mandatory if qualitative values are in performance table.
                If not provided, minimum/maximum values will be extracted from performance table,
                and preference direction will be set to max.
                Nominal and fuzzy qualitative scales are forbidden.



XMCDA related:
..............

- **Tag:** criteriaScales

----------------------------


.. _UTA-PyXMCDA-criteriaSegments:

criteria segments
~~~~~~~~~~~~~~~~~


Description:
............

Number of segments per marginal utility function.



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _UTA-PyXMCDA-parameters:

parameters
~~~~~~~~~~


Description:
............

Parameters of the method



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** Inputs alternatives

  *How to provide alternatives hierarchy*

  - **Type:** drop-down list
  - **Possible values:**
      - Use ranking of alternatives (XMCDA label : ranking) (default)

      - Use pairwise preferences and indifferences (XMCDA label : relations) 

- **Name:** Discrimination threshold

  *Discrimination threshold value.*

  - **Constraint description:** Bigger than 0.0.

  - **Type:** float
  - **Default value:** 0.001
- **Name:** Post optimality

  *Post optimality applied or not.*

  - **Type:** drop-down list
  - **Possible values:**
      - Applied (XMCDA label : true) 

      - Not applied (XMCDA label : false) (default)

- **Name:** Post-optimality threshold

  *Threshold for acceptable solutions used in post-optimality analysis.*

  - **Constraint description:** Bigger or equal to 0.0.

  - **Type:** float
  - **Default value:** 0.1
- **Name:** Significative figures

  *Number of significative figures in outputs values.*

  - **Constraint description:** Bigger than 0.

  - **Type:** integer
  - **Default value:** 3
- **Name:** Absolute tolerance

  *Float absolute tolerance used when computing Kendall tau.*

  - **Constraint description:** Bigger than 0.0.

  - **Type:** float
  - **Default value:** 1e-6
- **Name:** Solver

  *Which solver is used.*

  - **Type:** drop-down list
  - **Possible values:**
      - Coin-Or CBC (version included in Pulp) (XMCDA label : cbc) (default)

      - Choco (version included in Pulp) (XMCDA label : choco) 

      - GLPK (XMCDA label : glpk) 


XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
    <programParameters>
        <!-- %N -->
        <programParameter id="discrimination_threshold" name="Discrimination threshold">
	        <values>
                <value>
		            <real>%2</real>
                </value>
	        </values>
        </programParameter>
        <programParameter id="post_optimality" name="Post optimality">
	        <values>
                <value>
		            <boolean>%1</boolean>
                </value>
	        </values>
        </programParameter>%5<programParameter id="significative_figures" name="Significative figures">
	        <values>
                <value>
		            <integer>%4</integer>
                </value>
	        </values>
        </programParameter>
        <programParameter id="atol" name="Absolute tolerance">
	        <values>
                <value>
		            <real>%6</real>
                </value>
	        </values>
        </programParameter>
        <programParameter id="solver" name="Solver">
	        <values>
                <value>
		            <label>%3</label>
                </value>
	        </values>
        </programParameter>
    </programParameters>


----------------------------



.. _UTA-PyXMCDA_outputs:

Outputs
-------


- :ref:`valueFunctions <UTA-PyXMCDA-valueFunctions>`
- :ref:`valuesErrors <UTA-PyXMCDA-valuesErrors>`
- :ref:`messages <UTA-PyXMCDA-messages>`

.. _UTA-PyXMCDA-valueFunctions:

value functions
~~~~~~~~~~~~~~~


Description:
............

Optimal value functions found by the service.



XMCDA related:
..............

- **Tag:** criteriaFunctions

----------------------------


.. _UTA-PyXMCDA-valuesErrors:

values errors
~~~~~~~~~~~~~


Description:
............

Optimal values errors found by the service for the reference alternatives.
                Noted sigma in (Munda, 2005).



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _UTA-PyXMCDA-messages:

messages
~~~~~~~~


Description:
............

Status messages.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
