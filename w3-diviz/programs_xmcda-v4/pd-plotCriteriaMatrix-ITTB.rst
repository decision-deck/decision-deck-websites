:orphan:



.. _plotCriteriaMatrix-ITTB:

plotCriteriaMatrix
==================

:Provider: ITTB
:Version: 2.0

Description
-----------

This web service generates a graph representing a partial preorder on the criteria. The generated graph can be valued. It can also be transitive. Several shapes for the nodes are proposed and colors can be used.

- **Contact:** Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)



Inputs
------
(For outputs, see :ref:`below <plotCriteriaMatrix-ITTB_outputs>`)


- :ref:`criteria <plotCriteriaMatrix-ITTB-criteria>`
- :ref:`criteriaMatrix <plotCriteriaMatrix-ITTB-criteriaMatrix>`
- :ref:`parameters <plotCriteriaMatrix-ITTB-parameters>`

.. _plotCriteriaMatrix-ITTB-criteria:

criteria
~~~~~~~~


Description:
............

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
	<criteria>
		<criterion>
			<active>[...]</active>
			[...]
		</criterion>
		[...]
	</criteria>


----------------------------


.. _plotCriteriaMatrix-ITTB-criteriaMatrix:

criteriaMatrix
~~~~~~~~~~~~~~


Description:
............

A valued relation relative to comparisons of the criteria. A numeric <value> indicates a the valuation for each <pair> of the relation. If there is no such valuation, the value should be <NA/>



XMCDA related:
..............

- **Tag:** criteriaMatrix

- **Code:**

  ::

    
	<criteriaMatrix>
		<row>
			< criterionID>a01</criterionID>
			<column>
				<criterionID>[...]</criterionID>
				<values>
					<value>
						<real>[...]</real>
					</value>
				</values>
			</column>
			[...]
		</row>
		[...]
	</criterionMatrix>


----------------------------


.. _plotCriteriaMatrix-ITTB-parameters:

parameters
~~~~~~~~~~


Description:
............

Generates a graph taking into account the proposed options.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** Graph type:

  *Choose between true (values appear with their transitions) or false.*

  - **Type:** drop-down list
  - **Possible values:**
      - Arcs and values (XMCDA label : true) 

      - Arcs only (XMCDA label : false) (default)

- **Name:** With transitive reduction?

  

  - **Type:** drop-down list
  - **Possible values:**
      - Yes (XMCDA label : true) 

      - No (XMCDA label : false) (default)

- **Name:** Shape of the nodes?

  *Choose between rectangle, square, ellipse, circle or diamond.*

  - **Type:** drop-down list
  - **Possible values:**
      - Rectangle (XMCDA label : Rectangle) (default)

      - Square (XMCDA label : Square) 

      - Ellipse (XMCDA label : Ellipse) 

      - Circle (XMCDA label : Circle) 

      - Diamond (XMCDA label : Diamond) 

- **Name:** Use colors?

  *The use of colors: true for a colored graph.*

  - **Type:** drop-down list
  - **Possible values:**
      - Yes (XMCDA label : true) 

      - No (XMCDA label : false) (default)

- **Name:** Choose color:

  *String that indicates the color in the generated graph.Choose between "Black", "Red", "Blue", "Green", "Yellow", "Magenta", and "Cyan".*

  - **Type:** drop-down list
  - **Possible values:**
      - Black (XMCDA label : Black) (default)

      - Red (XMCDA label : Red) 

      - Blue (XMCDA label : Blue) 

      - Green (XMCDA label : Green) 

      - Yellow (XMCDA label : Yellow) 

      - Magenta (XMCDA label : Magenta) 

      - Cyan (XMCDA label : Cyan) 


XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
	<programParameters>
		<parameter id="valued_graph" name="Valued graph">
			<values>
				<value>
					<label>%1</label>
				</value>
			</values>
		</parameter>
		<parameter id="transitive_reduction" name="Transitive reduction">
			<values>
				<value>
					<label>%2</label>
				</value>
			</values>
		</parameter>
		<parameter id="node_shape" name="Node shape">
			<values>
				<value>
					<label>%3</label>
				</value>
			</values>
		</parameter>
		<parameter id="use_color" name="Colors in the graph">
			<values>
				<value>
					<label>%4</label>
				</value>
			</values>
		</parameter>
		<parameter id="selected_color" name="Selected color">
			<values>
				<value>
					<label>%5</label>
				</value>
			</values>
		</parameter>
	</programParameters>


----------------------------



.. _plotCriteriaMatrix-ITTB_outputs:

Outputs
-------


- :ref:`criteriaMatrix.dot <plotCriteriaMatrix-ITTB-dot>`
- :ref:`criteriaMatrix.png <plotCriteriaMatrix-ITTB-png>`
- :ref:`messages <plotCriteriaMatrix-ITTB-messages>`

.. _plotCriteriaMatrix-ITTB-dot:

criteria matrix (dot)
~~~~~~~~~~~~~~~~~~~~~


Description:
............

The dot file used to generate the PNG image.



XMCDA related:
..............

- **Tag:** other

----------------------------


.. _plotCriteriaMatrix-ITTB-png:

criteria matrix (png)
~~~~~~~~~~~~~~~~~~~~~


Description:
............

The png image reprensenting the graph.



XMCDA related:
..............

- **Tag:** other

----------------------------


.. _plotCriteriaMatrix-ITTB-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
