:orphan:



.. _MRSortInferenceExact-R-MCDA:

Exact inference of MRSort
=========================

:Provider: R-MCDA
:Version: 1.0

Description
-----------

The MRSort method, a simplification of the Electre TRI method, uses the pessimistic assignment rule, without indifference or preference thresholds attached to criteria. Only a binary discordance condition is considered, i.e. a veto forbids an outranking in any possible concordance situation, or not. The identification of the profiles, weights and majority threshold are done by taking into account assignment examples.

- **Contact:** Alexandru Olteanu (al.olteanu@telecom-bretagne.eu)



Inputs
------
(For outputs, see :ref:`below <MRSortInferenceExact-R-MCDA_outputs>`)


- :ref:`alternatives <MRSortInferenceExact-R-MCDA-inalt>`
- :ref:`criteria <MRSortInferenceExact-R-MCDA-incrit>`
- :ref:`categoriesRanks <MRSortInferenceExact-R-MCDA-incategval>`
- :ref:`performanceTable <MRSortInferenceExact-R-MCDA-inperf>`
- :ref:`alternativesAssignments <MRSortInferenceExact-R-MCDA-assignments>`
- :ref:`parameters <MRSortInferenceExact-R-MCDA-parameters>` *(optional)*

.. _MRSortInferenceExact-R-MCDA-inalt:

alternatives
~~~~~~~~~~~~


Description:
............

A complete list of alternatives to be considered when inferring the MR-Sort model.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
			

----------------------------


.. _MRSortInferenceExact-R-MCDA-incrit:

criteria scales
~~~~~~~~~~~~~~~


Description:
............

A list of criteria on which the alternatives are evaluated. For each criterion, the preference direction should be provided.



XMCDA related:
..............

- **Tag:** criteriaScales

----------------------------


.. _MRSortInferenceExact-R-MCDA-incategval:

categories ranks
~~~~~~~~~~~~~~~~


Description:
............

A list of categories ranks, 1 stands for the most preferred category and the higher the number the lower the preference for that category.



XMCDA related:
..............

- **Tag:** categoriesValues

----------------------------


.. _MRSortInferenceExact-R-MCDA-inperf:

performance table
~~~~~~~~~~~~~~~~~


Description:
............

The evaluations of the alternatives on the set of criteria.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _MRSortInferenceExact-R-MCDA-assignments:

alternatives assignments
~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The alternatives assignments to categories.



XMCDA related:
..............

- **Tag:** alternativesAssignments

----------------------------


.. _MRSortInferenceExact-R-MCDA-parameters:

parameters
~~~~~~~~~~


Description:
............

An indicator for whether vetoes should be included in the model or not.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** Include vetoes

  

  - **Default value:** false
- **Name:** Readable weights

  *An indicator for whether the weights should made easier to read.*

  - **Default value:** false
- **Name:** Readable profiles

  *An indicator for whether the profiles should made easier to read.*

  - **Default value:** false

XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
			   
    <programParameters>
        <parameter id="veto">
            <values>
                <value>
                    <boolean>%1</boolean>
                </value>
            </values>
        </parameter>
        <parameter id="readableWeights">
            <values>
                <value>
                    <boolean>%2</boolean>
                </value>
            </values>
        </parameter>
        <parameter id="readableProfiles">
            <values>
                <value>
                    <boolean>%3</boolean>
                </value>
            </values>
        </parameter>
    </programParameters>

			

----------------------------



.. _MRSortInferenceExact-R-MCDA_outputs:

Outputs
-------


- :ref:`majorityThreshold <MRSortInferenceExact-R-MCDA-majority>`
- :ref:`criteriaWeights <MRSortInferenceExact-R-MCDA-weights>`
- :ref:`categoriesProfiles <MRSortInferenceExact-R-MCDA-outcatprof>`
- :ref:`categoriesProfilesPerformanceTable <MRSortInferenceExact-R-MCDA-outcatprofpt>`
- :ref:`vetoProfiles <MRSortInferenceExact-R-MCDA-outcatveto>`
- :ref:`vetoProfilesPerformanceTable <MRSortInferenceExact-R-MCDA-outcatvetopt>`
- :ref:`messages <MRSortInferenceExact-R-MCDA-msg>`

.. _MRSortInferenceExact-R-MCDA-majority:

majority threshold
~~~~~~~~~~~~~~~~~~


Description:
............

The majority threshold.



XMCDA related:
..............

- **Tag:** programParameters

----------------------------


.. _MRSortInferenceExact-R-MCDA-weights:

criteria weights
~~~~~~~~~~~~~~~~


Description:
............

The criteria weights.



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _MRSortInferenceExact-R-MCDA-outcatprof:

categories profiles
~~~~~~~~~~~~~~~~~~~


Description:
............

The categories delimiting profiles.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _MRSortInferenceExact-R-MCDA-outcatprofpt:

categories profiles performance table
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The evaluations of the category profiles.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _MRSortInferenceExact-R-MCDA-outcatveto:

veto profiles
~~~~~~~~~~~~~


Description:
............

The categories veto profiles.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _MRSortInferenceExact-R-MCDA-outcatvetopt:

veto profiles performance table
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The evaluations of the veto profiles.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _MRSortInferenceExact-R-MCDA-msg:

messages
~~~~~~~~


Description:
............

Messages from the execution of the webservice. Possible errors in the input data will be given here.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
