:orphan:



.. _ElectreIsDiscordanceBinary-PUT:

ElectreIsDiscordanceBinary
==========================

:Provider: PUT
:Version: 0.1.0

Description
-----------

Computes discordance matrix as in Electre Is method. Resulting discordance indices are from range {0, 1}, hence "binary" in module's name.

The key feature of this module is its flexibility in terms of the types of elements allowed to compare, i.e. alternatives vs alternatives, alternatives vs boundary profiles and alternatives vs central (characteristic) profiles.

- **Web page:** http://github.com/xor-xor/electre_diviz



Inputs
------
(For outputs, see :ref:`below <ElectreIsDiscordanceBinary-PUT_outputs>`)


- :ref:`criteria <ElectreIsDiscordanceBinary-PUT-input3>`
- :ref:`alternatives <ElectreIsDiscordanceBinary-PUT-input1>`
- :ref:`criteriaScales <ElectreIsDiscordanceBinary-PUT-criteriaScales>`
- :ref:`criteriaThresholds <ElectreIsDiscordanceBinary-PUT-criteriaThresholds>`
- :ref:`performance_table <ElectreIsDiscordanceBinary-PUT-input4>`
- :ref:`profiles_performance_table <ElectreIsDiscordanceBinary-PUT-input5>` *(optional)*
- :ref:`classes_profiles <ElectreIsDiscordanceBinary-PUT-input2>` *(optional)*
- :ref:`method_parameters <ElectreIsDiscordanceBinary-PUT-input6>`

.. _ElectreIsDiscordanceBinary-PUT-input3:

criteria
~~~~~~~~


Description:
............

Criteria to consider, possibly with the veto thresholds. Each criterion must have a preference direction specified (min or max). It is worth mentioning that this module allows to define veto threshold as a constant as well as a linear function.



XMCDA related:
..............

- **Tag:** criteria

----------------------------


.. _ElectreIsDiscordanceBinary-PUT-input1:

alternatives
~~~~~~~~~~~~


Description:
............

Alternatives to consider.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _ElectreIsDiscordanceBinary-PUT-criteriaScales:

criteria scales
~~~~~~~~~~~~~~~


Description:
............

The scales of the Criteria to consider.



XMCDA related:
..............

- **Tag:** criteriaScales

----------------------------


.. _ElectreIsDiscordanceBinary-PUT-criteriaThresholds:

criteria thresholds
~~~~~~~~~~~~~~~~~~~


Description:
............

The thresholds for the criteria to consider



XMCDA related:
..............

- **Tag:** criteriaThresholds

----------------------------


.. _ElectreIsDiscordanceBinary-PUT-input4:

performance_table
~~~~~~~~~~~~~~~~~


Description:
............

The performance of alternatives.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _ElectreIsDiscordanceBinary-PUT-input5:

profiles_performance_table
~~~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The performance of profiles (boundary or central).



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _ElectreIsDiscordanceBinary-PUT-input2:

classes_profiles
~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Definitions of profiles (boundary or central) which should be used for classes (categories) representation.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _ElectreIsDiscordanceBinary-PUT-input6:

method_parameters
~~~~~~~~~~~~~~~~~


Description:
............

This parameter specifies the type of elements provided for comparison.

Choosing 'boundary_profiles' or 'central_profiles' requires providing inputs 'classes_profiles' and 'profiles_performance_table' as well (which are optional by default).



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** comparison_with

  

  - **Type:** drop-down list
  - **Possible values:**
      - alternatives vs alternatives (XMCDA label : alternatives) (default)

      - alternatives vs boundary profiles (XMCDA label : boundary_profiles) 

      - alternatives vs central (characteristic) profiles (XMCDA label : central_profiles) 


XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
<programParameters>
	<parameter name="comparison_with">
		<values>
			<value>
				<label>%1</label>
			</value>
		</values>
	</parameter>
</programParameters>


----------------------------



.. _ElectreIsDiscordanceBinary-PUT_outputs:

Outputs
-------


- :ref:`discordance_aggregated <ElectreIsDiscordanceBinary-PUT-output1>`
- :ref:`discordance_partials <ElectreIsDiscordanceBinary-PUT-output2>`
- :ref:`messages <ElectreIsDiscordanceBinary-PUT-output3>`

.. _ElectreIsDiscordanceBinary-PUT-output1:

discordance_aggregated
~~~~~~~~~~~~~~~~~~~~~~


Description:
............

Aggregated discordance indices computed from the given data.



XMCDA related:
..............

- **Tag:** alternativesMatrix

----------------------------


.. _ElectreIsDiscordanceBinary-PUT-output2:

discordance_partials
~~~~~~~~~~~~~~~~~~~~


Description:
............

Non-aggregated (i.e. per-criterion) discordance indices computed from the given data.



XMCDA related:
..............

- **Tag:** alternativesMatrix

----------------------------


.. _ElectreIsDiscordanceBinary-PUT-output3:

messages
~~~~~~~~


Description:
............

Messages or errors generated by this module.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
