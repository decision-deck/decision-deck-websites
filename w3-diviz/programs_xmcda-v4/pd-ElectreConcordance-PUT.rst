:orphan:



.. _ElectreConcordance-PUT:

ElectreConcordance
==================

:Provider: PUT
:Version: 0.2.0

Description
-----------

Computes concordance matrix using procedure which is common to the most methods from the Electre family.

The key feature of this module is its flexibility in terms of the types of elements allowed to compare, i.e. alternatives vs alternatives, alternatives vs boundary profiles and alternatives vs central (characteristic) profiles.

- **Web page:** http://github.com/xor-xor/electre_diviz



Inputs
------
(For outputs, see :ref:`below <ElectreConcordance-PUT_outputs>`)


- :ref:`alternatives <ElectreConcordance-PUT-input1>`
- :ref:`criteria <ElectreConcordance-PUT-input3>`
- :ref:`criteriaScales <ElectreConcordance-PUT-criteriaScales>`
- :ref:`criteriaThresholds <ElectreConcordance-PUT-criteriaThresholds>` *(optional)*
- :ref:`performance_table <ElectreConcordance-PUT-input4>`
- :ref:`profiles_performance_table <ElectreConcordance-PUT-input5>` *(optional)*
- :ref:`weights <ElectreConcordance-PUT-input6>`
- :ref:`classes_profiles <ElectreConcordance-PUT-input2>` *(optional)*
- :ref:`method_parameters <ElectreConcordance-PUT-input7>`

.. _ElectreConcordance-PUT-input1:

alternatives
~~~~~~~~~~~~


Description:
............

Alternatives to consider.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _ElectreConcordance-PUT-input3:

criteria
~~~~~~~~


Description:
............

Criteria to consider, possibly with preference and indifference thresholds. Each criterion must have a preference direction specified (min or max). It is worth mentioning that this module allows to define thresholds as constants as well as linear functions.



XMCDA related:
..............

- **Tag:** criteria

----------------------------


.. _ElectreConcordance-PUT-criteriaScales:

criteria scales
~~~~~~~~~~~~~~~


Description:
............

The scales of the criteria to consider. Each criterion must have a preference direction specified (min or max).



XMCDA related:
..............

- **Tag:** criteriaScales

----------------------------


.. _ElectreConcordance-PUT-criteriaThresholds:

criteria thresholds
~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The criteria' preference and indifference thresholds. It is worth mentioning that this module allows to define thresholds as constants as well as linear functions.



XMCDA related:
..............

- **Tag:** criteriaThresholds

----------------------------


.. _ElectreConcordance-PUT-input4:

performance_table
~~~~~~~~~~~~~~~~~


Description:
............

The performance of alternatives.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _ElectreConcordance-PUT-input5:

profiles_performance_table
~~~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The performance of profiles (boundary or central).



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _ElectreConcordance-PUT-input6:

weights
~~~~~~~


Description:
............

Weights of criteria to consider.



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _ElectreConcordance-PUT-input2:

classes_profiles
~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Definitions of profiles (boundary or central) which should be used for classes (categories) representation.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _ElectreConcordance-PUT-input7:

method_parameters
~~~~~~~~~~~~~~~~~


Description:
............

This parameter specifies the type of elements provided for comparison.

Choosing 'boundary_profiles' or 'central_profiles' requires providing inputs 'classes_profiles' and 'profiles_performance_table' as well (which are optional by default).



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** comparison_with

  

  - **Type:** drop-down list
  - **Possible values:**
      - alternatives vs alternatives (XMCDA label : alternatives) (default)

      - alternatives vs boundary profiles (XMCDA label : boundary_profiles) 

      - alternatives vs central (characteristic) profiles (XMCDA label : central_profiles) 


XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
<programParameters>
	<parameter name="comparison_with">
		<values>
			<value>
				<label>%1</label>
			</value>
		</values>
	</parameter>
</programParameters>


----------------------------



.. _ElectreConcordance-PUT_outputs:

Outputs
-------


- :ref:`concordance <ElectreConcordance-PUT-output1>`
- :ref:`messages <ElectreConcordance-PUT-output2>`

.. _ElectreConcordance-PUT-output1:

concordance
~~~~~~~~~~~


Description:
............

Concordance matrix computed from the given data. This matrix aggregates partial concordances from all criteria into single concordance index per pair of alternatives or alternatives/profiles.



XMCDA related:
..............

- **Tag:** alternativesMatrix

----------------------------


.. _ElectreConcordance-PUT-output2:

messages
~~~~~~~~


Description:
............

Messages or errors generated by this module.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
