:orphan:



.. _ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT:

ImpreciseDEA-ValueAdditive-SMAA_efficiencies
============================================

:Provider: PUT
:Version: 1.0

Description
-----------

Computes efficiency scores for the given DMUs (alternatives) using SMAA-D method and Additive Data Envelopment Analysis Model with imprecise data. For given number of buckets and samples, returns a matrix with alternatives in each row and buckets representing efficiency intervals in each column. Single cell indicates how many samples gave efficiency scores of respective alternative in respective bucket.

- **Contact:** Anna Labijak <support@decision-deck.org>



Inputs
------
(For outputs, see :ref:`below <ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT_outputs>`)


- :ref:`units <ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-units>`
- :ref:`inputsOutputs <ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-inputsOutputs>`
- :ref:`inputsOutputsScales <ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-inputsOutputsScales>`
- :ref:`inputsOutputsFunctions <ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-inputsOutputsFunctions>` *(optional)*
- :ref:`performanceTable <ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-performanceTable>`
- :ref:`maxPerformanceTable <ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-maxPerformanceTable>` *(optional)*
- :ref:`weightsLinearConstraints <ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-methodParameters>`

.. _ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-units:

units
~~~~~


Description:
............

A list of alternatives (DMUs).



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
        <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
      

----------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-inputsOutputs:

inputs/outputs
~~~~~~~~~~~~~~


Description:
............

A list of criteria with specified preference direction. List has to contains at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output).



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
        <criteria>
                        <criterion>[...]</criterion>
                        [...]
                    </criteria>
      

----------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-inputsOutputsScales:

inputs/outputs scales
~~~~~~~~~~~~~~~~~~~~~


Description:
............

Informations about inputs and outputs scales and optionally about boundaries



XMCDA related:
..............

- **Tag:** criteriaScales

- **Code:**

  ::

    
        
<criteriaScales>
    <criterionScale>
      <criterionID>[...]</criterionID>
      <scales>
        <scale>
          [...]
        </scale>
      </scales>
    </criterionScale>
    [...]
</criteriaScales>
      

----------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-inputsOutputsFunctions:

inputs/outputs function shapes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

Shapes of functions assigned to inputs and outputs (if other than linear)



XMCDA related:
..............

- **Tag:** criteriaFunctions

- **Code:**

  ::

    
        
<criteriaFunctions>
    <criterionFunction>
      <criterionID>[...]</criterionID>
      <functions>
        <function>
          <piecewiseLinear>
            <segment>
              <head>
                [...]
              </head>
              <tail>
                [...]
              </tail>
            </segment>
          </piecewiseLinear>
        </function>
        [...]
      </functions>
    </criterionFunction>
    [...]
</criteriaFunctions>
      

----------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-performanceTable:

performance
~~~~~~~~~~~


Description:
............

A list of alternatives (DMUs) minimal performances (or exact performances if intervals should be created by tolerance).



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    
        <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<values><value>[...]</value></values>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>
      

----------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-maxPerformanceTable:

max performance
~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A list of alternatives (DMUs) maximal performances.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    
        <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<values><value>[...]</value></values>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>
      

----------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-weightsLinearConstraints:

weights constraints
~~~~~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

A list of criteria linear constraints.



XMCDA related:
..............

- **Tag:** criteriaLinearConstraints

- **Code:**

  ::

    
        
            <criteriaLinearConstraints>
                <constraints>
                    <constraint>
                        <elements>
                            <element>
                                <criterionID> [...] </criterionID>
                                <coefficient>
                                    [...]
                                </coefficient>
                            </element>
                            [...]
                        </elements>
                        <operator>[...]</operator>
                        <rhs>
                            [...]
                        </rhs>
                    </constraint>
                    [...]
                </constraints>
            </criteriaLinearConstraints>
      

----------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-methodParameters:

parameters
~~~~~~~~~~


Description:
............

Represents method parameters.
          "tolerance" represents the fraction for creating interval data (created interval: [data*(1-tolerance), data*(1+tolerance)]),
          "transformToUtilities" means if data should be tranformed into values from range [0-1],
          "boundariesProvided" means if inputsOutputs file contains information about min and max data for each factor,
          "functionShapeProvided" means if inputsOutputs file contains information about the shapes of value function for given factor,
          "samplesNb" determines number of samples used to calculate results,
          "intervalsNb" determines number of buckets, for which the efficiency scores are divided.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** number of samples

  

  - **Constraint description:** The value should be a positive integer.

  - **Type:** integer
  - **Default value:** 100
- **Name:** number of intervals

  

  - **Constraint description:** The value should be a positive integer.

  - **Type:** integer
  - **Default value:** 10
- **Name:** tolerance

  

  - **Constraint description:** The value should be a non-negative number.

  - **Type:** float
  - **Default value:** 0.0
- **Name:** transform to utilities

  

  - **Default value:** true
- **Name:** boundaries provided

  

  - **Default value:** false
- **Name:** function shapes provided

  

  - **Default value:** false

XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
        
	<programParameters>
		<parameter id="samplesNb">
			<values>
         <value><integer>%1</integer></value>
      </values>
		</parameter>
		<parameter id="intervalsNb">
			<values>
        <value><integer>%2</integer></value>
      </values>
		</parameter>
		<parameter id="tolerance">
      <values>
        <value><real>%3</real></value>
      </values>
  </parameter>
  <parameter id="transformToUtilities">
      <values>
        <value><boolean>%4</boolean></value>
      </values>
		</parameter>
		<parameter id="boundariesProvided">
      <values>
        <value><boolean>%5</boolean></value>
      </values>
		</parameter>
		<parameter id="functionShapeProvided">
      <values>
        <value><boolean>%6</boolean></value>
      </values>
		</parameter>
	</programParameters>

      

----------------------------



.. _ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT_outputs:

Outputs
-------


- :ref:`efficiencyDistribution <ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-efficiencyDistribution>`
- :ref:`maxEfficiency <ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-maxEfficiency>`
- :ref:`minEfficiency <ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-minEfficiency>`
- :ref:`avgEfficiency <ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-avgEfficiency>`
- :ref:`messages <ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-messages>`

.. _ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-efficiencyDistribution:

efficiency distribution
~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

A performance table for given alternatives. Single performance consists of attribute criterionID representing certain bucket, and a value representing the ratio of efficiency scores in this bucket.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    
        <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>[...]</criterionID>
								<values>
                    <value>[...]</value>
                </values>
							</performance>
							[...]
						</alternativePerformances>
                        [...]
					</performanceTable>
      

----------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-maxEfficiency:

max efficiency
~~~~~~~~~~~~~~


Description:
............

A list of alternatives with maximum efficiency scores (obtained for given sample).



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
        <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
              <values>
                    <value>
                        [...]
                    </value>
              </values>
						</alternativeValue>
						[...]
					</alternativesValues>
      

----------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-minEfficiency:

min efficiency
~~~~~~~~~~~~~~


Description:
............

A list of alternatives with computed minimum efficiency scores (obtained for given sample).



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
        <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
              <values>
                    <value>
                        [...]
                    </value>
              </values>
						</alternativeValue>
						[...]
					</alternativesValues>
      

----------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-avgEfficiency:

average efficiency
~~~~~~~~~~~~~~~~~~


Description:
............

A list of alternatives with average efficiency scores (obtained for given sample).



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
        <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
              <values>
                    <value>
                        [...]
                    </value>
              </values>
						</alternativeValue>
						[...]
					</alternativesValues>
      

----------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
