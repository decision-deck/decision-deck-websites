:orphan:



.. _OWA_aggregation-URV:

OWA aggregation
===============

:Provider: URV
:Version: 4.0

Description
-----------

This module implements the OWA aggregation operator: The Ordered Weighted Averaging operators, commonly called OWA operators, provide a parameterized class of mean type aggregation operators.
OWA was defined by R.R. Yager 1988. Here we provide an implementation of OWA operator. The main characteristic of this aggregation operator is the fact that the weights are associated to values instead of criteria. In this way, we can define different aggregation policies according to the importance that we associate to high and low performance scores.

- **Contact:** Aida Valls <aida.valls@urv.cat>

- **Reference:** A. Valls, The Ordered Weighted Averaging Operator, In: Proc. IEEE International Conference on Fuzzy Systems, FUZZ-IEEE 2010, IEEE Computer Society, Barcelona, Catalonia, 2010, pp. 3063-3070.



Inputs
------
(For outputs, see :ref:`below <OWA_aggregation-URV_outputs>`)


- :ref:`criteria <OWA_aggregation-URV-input1>`
- :ref:`alternatives <OWA_aggregation-URV-input0>`
- :ref:`performanceTable <OWA_aggregation-URV-input3>`
- :ref:`weightsOWA <OWA_aggregation-URV-input2>` *(optional)*
- :ref:`programParameters <OWA_aggregation-URV-input4>` *(optional)*

.. _OWA_aggregation-URV-input1:

criteria
~~~~~~~~


Description:
............

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false).
By default (no <active> tag), criteria are considered as active.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
                            
                    <criteria id="...">
                        <criterion>
                            <active>[...]</active>
                        </criterion>
                        [...]
                    </criteria>
                    
                     

----------------------------


.. _OWA_aggregation-URV-input0:

alternatives
~~~~~~~~~~~~


Description:
............

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). 
By default (no <active> tag), alternatives are considered as active.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
				
           	     	<alternatives id="...">
                        	<alternative>
                            		<active>[...]</active>
                        	</alternative>
			    [...]
	       		</alternatives>
				
			

----------------------------


.. _OWA_aggregation-URV-input3:

performanceTable
~~~~~~~~~~~~~~~~


Description:
............

The performance table will contain the all criteria values, por each alternative.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    

		  
			<performanceTable>
				<alternativePerformances>
					<alternativeID>[...]</alternativeID>
					<performance>
						<criterionID>[...]</criterionID>
						<value>
							<float>[...]</float>
						</value>
					</performance>
					[...]
				</alternativePerformances>
				[...]
			</performanceTable>
		  
		  

----------------------------


.. _OWA_aggregation-URV-input2:

weights
~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The weights are associated to the values of the alternatives rather than to the criteria. In this way they can define different aggregation policies. Assuming that the values on the alternative will be sorted from the best to the worst, the list of weights must be ordered according to the importance that is given to the values, from the highest to the lowest. 
For example a list of weights as (0.5, 0.5, 0, 0, 0) is ignoring the 3 lowest values, and making an average of the two highest ones. A list like (0, 0, 1, 0 ,0 ) is calculating the median, while (0, 0, 0, 0, 1) is taking the minimum.
Notice that the sum of weights is required to be 1. In version 4.0 the weights have only one structure, where the user can specify her/his weights without using any identifier for each weight.



XMCDA related:
..............

- **Tag:** criteriaSetsValues

- **Code:**

  ::

    
			
			<criteriaSetsValues>
				<criteriaSetValues id="owa-weights" mcdaConcept="OWA weights">
					<criteriaSetID>...</criteriaSetID>
					<values>
						<value>
							<real>...</real>
						</value>
						<value>
							[...]
						</value>
						[....]
					</values>
				</criteriaSetValues>
			</criteriaSetsValues>
		  	
			

----------------------------


.. _OWA_aggregation-URV-input4:

orness
~~~~~~

**Optional:** yes, enabled by default

Description:
............

The weights are associated to the values of the alternatives rather than to the criteria. In this way they can define different aggregation policies. In this option, the system computes the OWA weights vector from the degree of orness indicated as input. The orness values must be in the continuous range from 0 to 1. Notice that 0.0 means andness, 0.5 neutrality, 1.0 corresponds to maximum orness.



XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
		  
        		<programParameters>
            			<parameter !-- REQUIRED --> id="active">
                			<values>
						<value>
							<boolean>...</boolean>
						</value>
					</values>
            			</parameter>
            			<parameter !-- REQUIRED --> id="orness">
                			<values>
						<value>
							<real>...</real>
						</value>
					</values>
            			</parameter>
        		</programParameters>
		  
		  

----------------------------



.. _OWA_aggregation-URV_outputs:

Outputs
-------


- :ref:`alternativesValues <OWA_aggregation-URV-output0>`
- :ref:`messages <OWA_aggregation-URV-output1>`

.. _OWA_aggregation-URV-output0:

owaResults
~~~~~~~~~~


Description:
............

Result obtained from the OWA aggregation on each alternative. It will be a numeric value.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _OWA_aggregation-URV-output1:

messages
~~~~~~~~


Description:
............

A status message.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
