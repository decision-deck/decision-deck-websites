:orphan:



.. _DEAvalueADDEfficiency-PUT:

DEAvalueADDEfficiency
=====================

:Provider: PUT
:Version: 1.0

Description
-----------

Computes efficiency scores for the given DMUs (alternatives) using additive Data Envelopment Analysis Model.

- **Contact:** Malgorzata Napieraj (napieraj.malgorzata@gmail.com)

- **Reference:** Gouveia M. C., Dias L. C., Antunes C. H., Additive DEA based on MCDA with imprecise information (2008).



Inputs
------
(For outputs, see :ref:`below <DEAvalueADDEfficiency-PUT_outputs>`)


- :ref:`inputsOutputs <DEAvalueADDEfficiency-PUT-inputsOutputs>`
- :ref:`units <DEAvalueADDEfficiency-PUT-units>`
- :ref:`performanceTable <DEAvalueADDEfficiency-PUT-performanceTable>`
- :ref:`weightsLinearConstraints <DEAvalueADDEfficiency-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <DEAvalueADDEfficiency-PUT-methodParameters>`

.. _DEAvalueADDEfficiency-PUT-inputsOutputs:

inputsOutputs
~~~~~~~~~~~~~


Description:
............

A list of criteria with specified preference direction. List has to contains at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output). In addition, minimum and maximum possible value for each critetion can be defined. If not defined, they will be computed from alternatives performances on given criteria.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    <criteria>
                        <criterion>
							<scale>
								<quantitative>
									<preferenceDirection>
										[...]
									</preferenceDirection>
								</quantitative>
							</scale>
							<minimum>[...]</minimum>
							<maximum>[...]</maximum>
                            [...]
                        </criterion>
                        [...]
                    </criteria>

----------------------------


.. _DEAvalueADDEfficiency-PUT-units:

units
~~~~~


Description:
............

A list of alternatives (DMUs).



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>

----------------------------


.. _DEAvalueADDEfficiency-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~


Description:
............

A list of alternatives (DMUs) performances.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    <performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>

----------------------------


.. _DEAvalueADDEfficiency-PUT-weightsLinearConstraints:

weightsLinearConstraints
~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

A list of criteria linear constraints.



XMCDA related:
..............

- **Tag:** criteriaLinearConstraints

- **Code:**

  ::

    <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID>[...]</criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>

----------------------------


.. _DEAvalueADDEfficiency-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~


Description:
............

"boundariesProvided" indicates if criteria values boundaries are given in criteria input or they have to be computed based on alternatives performances. "transformToUtilites" indicates if given alternatives values are utilities or if they have to be firstly transformed to utilities.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** boundariesProvided

  

  - **Default value:** false
- **Name:** transformToUtilities

  

  - **Default value:** true

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    <methodParameters>
							<parameter name="boundariesProvided">
								<value><boolean>%1</boolean></value>
							</parameter>
							<parameter name="transformToUtilities">
								<value><boolean>%2</boolean></value>
							</parameter>
					</methodParameters>

----------------------------



.. _DEAvalueADDEfficiency-PUT_outputs:

Outputs
-------


- :ref:`efficiency <DEAvalueADDEfficiency-PUT-efficiency>`
- :ref:`distance <DEAvalueADDEfficiency-PUT-distance>`
- :ref:`messages <DEAvalueADDEfficiency-PUT-messages>`

.. _DEAvalueADDEfficiency-PUT-efficiency:

efficiency
~~~~~~~~~~


Description:
............

A list of alternatives with computed efficiency scores.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    <alternativesValues mcdaConcept="efficiency">
						<alternativeValue>
						  <alternativeID>[...]</alternativeID>
						  <value>
							[...]
						  </value>
						</alternativeValue>
						[...]
					</alternativesValues>

----------------------------


.. _DEAvalueADDEfficiency-PUT-distance:

distance
~~~~~~~~


Description:
............

A list of alternatives with computed distances from efficient DMUs.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    <alternativesValues mcdaConcept="distance">
						<alternativeValue>
						  <alternativeID>[...]</alternativeID>
						  <value>
							[...]
						  </value>
						</alternativeValue>
						[...]
					</alternativesValues>

----------------------------


.. _DEAvalueADDEfficiency-PUT-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
