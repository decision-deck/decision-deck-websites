
:orphan:

.. _plotFuzzyCriteriaScales-PyXMCDA:

plotFuzzyCriteriaScales
=======================

:Provider: PyXMCDA
:Version: 1.3

Description
-----------

Generate plots from provided fuzzy scales as well as the scripts generating these plots.

Colormap can be defined by the user, by giving a list of colors in the parameters.xml file.
The number of colors is not restrained, and the colormap will linearly distribute the color in their provided order and interpolate between them.
If only one is provided, it will be used for all data plot.
Each color is either one of the color names predefined in matplotlib (See https://matplotlib.org/stable/gallery/color/named_colors.html#sphx-glr-gallery-color-named-colors-py) or a RGB color defined in hexadecimal '#RRGGBB'.

- **Contact:** Nicolas Duminy (nicolas.duminy@telecom-bretagne.eu)

- **Web page:** https://gitlab.com/nduminy/ws-pyxmcda



Inputs
------
(For outputs, see :ref:`below <plotFuzzyCriteriaScales-PyXMCDA_outputs>`)


- :ref:`criteria <plotFuzzyCriteriaScales-PyXMCDA-criteria>` *(optional)*
- :ref:`criteriaScales <plotFuzzyCriteriaScales-PyXMCDA-criteriaScales>`
- :ref:`parameters <plotFuzzyCriteriaScales-PyXMCDA-parameters>`

.. _plotFuzzyCriteriaScales-PyXMCDA-criteria:

criteria
~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The criteria to be plotted. All are plotted if not provided.



XMCDA related:
..............

- **Tag:** criteria

----------------------------


.. _plotFuzzyCriteriaScales-PyXMCDA-criteriaScales:

criteria scales
~~~~~~~~~~~~~~~


Description:
............

The criteria and their criteria scales defined. Only the fuzzy ones can be considered.
                Unidentified criteria scales are accepted as well (they won't be labelled though).



XMCDA related:
..............

- **Tag:** criteriaScales

----------------------------


.. _plotFuzzyCriteriaScales-PyXMCDA-parameters:

parameters
~~~~~~~~~~


Description:
............

Parameters of the method



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** X-axis label

  *X-axis name to display below each plot.*

  - **Type:** string
  - **Default value:** "series"
- **Name:** Y-axis label

  *Y-axis name to display on the left of each plot.*

  - **Type:** string
  - **Default value:** "fuzzy numbers"
- **Name:** Colormap

  *Type of the colormap.*

  - **Type:** drop-down list
  - **Possible values:**
      - Monochrome (XMCDA label : monochrome) (default)

      - Bicolor (XMCDA label : bicolor) 

      - Multi-color (advanced) (XMCDA label : multicolor (advanced)) 

- **Name:** Nb of colors

  *The number of colors supplied to define the colormap.*

  - **Constraint description:** The value should be between 2 and 10.

  - **Type:** integer
  - **Default value:** 2
- **Name:** Color

  *Color to use.*

  - **Type:** drop-down list
  - **Possible values:**
      - black (XMCDA label : 
                <value>
                    <label>black</label>
                </value>) (default)

      - red (XMCDA label : 
                <value>
                    <label>red</label>
                </value>) 

      - green (XMCDA label : 
                <value>
                    <label>green</label>
                </value>) 

      - blue (XMCDA label : 
                <value>
                    <label>blue</label>
                </value>) 

      - orange (XMCDA label : 
                <value>
                    <label>orange</label>
                </value>) 

      - green (XMCDA label : 
                <value>
                    <label>green</label>
                </value>) 

      - purple (XMCDA label : 
                <value>
                    <label>purple</label>
                </value>) 

      - cyan (XMCDA label : 
                <value>
                    <label>cyan</label>
                </value>) 

      - magenta (XMCDA label : 
                <value>
                    <label>magenta</label>
                </value>) 

      - yellow (XMCDA label : 
                <value>
                    <label>yellow</label>
                </value>) 

      - salmon (XMCDA label : 
                <value>
                    <label>salmon</label>
                </value>) 

      - orangered (XMCDA label : 
                <value>
                    <label>orangered</label>
                </value>) 

      - chocolate (XMCDA label : 
                <value>
                    <label>chocolate</label>
                </value>) 

      - greenyellow (XMCDA label : 
                <value>
                    <label>greenyellow</label>
                </value>) 

      - aquamarine (XMCDA label : 
                <value>
                    <label>aquamarine</label>
                </value>) 

      - pink (XMCDA label : 
                <value>
                    <label>pink</label>
                </value>) 

      - grey (XMCDA label : 
                <value>
                    <label>grey</label>
                </value>) 

      - whitesmoke (XMCDA label : 
                <value>
                    <label>whitesmoke</label>
                </value>) 

- **Name:** 1st color

  *First color to use for the colormap.*

  - **Type:** drop-down list
  - **Possible values:**
      - black (XMCDA label : 
                <value>
                    <label>black</label>
                </value>) (default)

      - red (XMCDA label : 
                <value>
                    <label>red</label>
                </value>) 

      - green (XMCDA label : 
                <value>
                    <label>green</label>
                </value>) 

      - blue (XMCDA label : 
                <value>
                    <label>blue</label>
                </value>) 

      - orange (XMCDA label : 
                <value>
                    <label>orange</label>
                </value>) 

      - green (XMCDA label : 
                <value>
                    <label>green</label>
                </value>) 

      - purple (XMCDA label : 
                <value>
                    <label>purple</label>
                </value>) 

      - cyan (XMCDA label : 
                <value>
                    <label>cyan</label>
                </value>) 

      - magenta (XMCDA label : 
                <value>
                    <label>magenta</label>
                </value>) 

      - yellow (XMCDA label : 
                <value>
                    <label>yellow</label>
                </value>) 

      - salmon (XMCDA label : 
                <value>
                    <label>salmon</label>
                </value>) 

      - orange-red (XMCDA label : 
                <value>
                    <label>orangered</label>
                </value>) 

      - chocolate (XMCDA label : 
                <value>
                    <label>chocolate</label>
                </value>) 

      - green-yellow (XMCDA label : 
                <value>
                    <label>greenyellow</label>
                </value>) 

      - aquamarine (XMCDA label : 
                <value>
                    <label>aquamarine</label>
                </value>) 

      - pink (XMCDA label : 
                <value>
                    <label>pink</label>
                </value>) 

      - grey (XMCDA label : 
                <value>
                    <label>grey</label>
                </value>) 

      - whitesmoke (XMCDA label : 
                <value>
                    <label>whitesmoke</label>
                </value>) 

      - white (XMCDA label : 
                <value>
                    <label>white</label>
                </value>) 

- **Name:** 2nd color

  *Second color to use for the colormap.*

  - **Type:** drop-down list
  - **Possible values:**
      - black (XMCDA label : 
                <value>
                    <label>black</label>
                </value>) (default)

      - red (XMCDA label : 
                <value>
                    <label>red</label>
                </value>) 

      - green (XMCDA label : 
                <value>
                    <label>green</label>
                </value>) 

      - blue (XMCDA label : 
                <value>
                    <label>blue</label>
                </value>) 

      - orange (XMCDA label : 
                <value>
                    <label>orange</label>
                </value>) 

      - green (XMCDA label : 
                <value>
                    <label>green</label>
                </value>) 

      - purple (XMCDA label : 
                <value>
                    <label>purple</label>
                </value>) 

      - cyan (XMCDA label : 
                <value>
                    <label>cyan</label>
                </value>) 

      - magenta (XMCDA label : 
                <value>
                    <label>magenta</label>
                </value>) 

      - yellow (XMCDA label : 
                <value>
                    <label>yellow</label>
                </value>) 

      - salmon (XMCDA label : 
                <value>
                    <label>salmon</label>
                </value>) 

      - orangered (XMCDA label : 
                <value>
                    <label>orangered</label>
                </value>) 

      - chocolate (XMCDA label : 
                <value>
                    <label>chocolate</label>
                </value>) 

      - greenyellow (XMCDA label : 
                <value>
                    <label>greenyellow</label>
                </value>) 

      - aquamarine (XMCDA label : 
                <value>
                    <label>aquamarine</label>
                </value>) 

      - pink (XMCDA label : 
                <value>
                    <label>pink</label>
                </value>) 

      - grey (XMCDA label : 
                <value>
                    <label>grey</label>
                </value>) 

      - whitesmoke (XMCDA label : 
                <value>
                    <label>whitesmoke</label>
                </value>) 

- **Name:** Color 1 (ex.: cyan or #00FFFF)

  *1st color (ex.: cyan or #00FFFF)*

  - **Type:** string
  - **Default value:** "black"
- **Name:** Color 2

  *2nd color*

  - **Type:** string
  - **Default value:** "black"
- **Name:** Color 3

  *3rd color*

  - **Type:** string
  - **Default value:** "black"
- **Name:** Color 4

  *4th color*

  - **Type:** string
  - **Default value:** "black"
- **Name:** Color 5

  *5th color*

  - **Type:** string
  - **Default value:** "black"
- **Name:** Color 6

  *6th color*

  - **Type:** string
  - **Default value:** "black"
- **Name:** Color 7

  *7th color*

  - **Type:** string
  - **Default value:** "black"
- **Name:** Color 8

  *8th color*

  - **Type:** string
  - **Default value:** "black"
- **Name:** Color 9

  *9th color*

  - **Type:** string
  - **Default value:** "black"
- **Name:** Color 10

  *10th color*

  - **Type:** string
  - **Default value:** "black"
- **Name:** Use markers

  *Set if markers are used for the fuzzy numbers plots.
                    If true, markers will be used, different for each fuzzy numbers.
                    Markers are all taken from list of matplotlib supported markers, see https://matplotlib.org/stable/api/markers_api.html.*

  - **Default value:** false
- **Name:** Linestyle

  *Linestyle used to plot lines.*

  - **Type:** drop-down list
  - **Possible values:**
      - solid line (XMCDA label : solid) (default)

      - dashed line (XMCDA label : dashed) 

      - dashed and dotted line (XMCDA label : dashdot) 

      - dotted line (XMCDA label : dotted) 

      - no line (XMCDA label : None) 

- **Name:** Show grid

  *Set if grid is visible on each plot.*

  - **Default value:** false
- **Name:** Image file extension

  *File extension of generated image figure.*

  - **Type:** drop-down list
  - **Possible values:**
      - .eps (Encapsulated PostScript) (XMCDA label : eps) 

      - .jpg (Joint Photographic Experts Group) (XMCDA label : jpg) 

      - .pdf (Portable Document Format) (XMCDA label : pdf) 

      - .pgf (Progressive Graphics File) (XMCDA label : pgf) 

      - .png (Portable Network Graphics) (XMCDA label : png) (default)

      - .ps (PostScript) (XMCDA label : ps) 

      - .raw (Raw RGBA bitmap) (XMCDA label : raw) 

      - .rgba (Silicon Graphics RGB) (XMCDA label : rgba) 

      - .svg (Scalable Vector Graphics) (XMCDA label : svg) 

      - .svgz (Compressed Scalable Vector Graphics) (XMCDA label : svgz) 

      - .tif (Tagged Image File Format) (XMCDA label : tif) 

- **Name:** Image file extension

  *File extension of generated image figure.*

  - **Type:** drop-down list
  - **Possible values:**
      - .eps (Encapsulated PostScript) (XMCDA label : eps) 

      - .jpg (Joint Photographic Experts Group) (XMCDA label : jpg) 

      - .pdf (Portable Document Format) (XMCDA label : pdf) 

      - .png (Portable Network Graphics) (XMCDA label : png) (default)

      - .svg (Scalable Vector Graphics) (XMCDA label : svg) 

- **Name:** Plotter

  *Plotter used to generate image.*

  - **Type:** drop-down list
  - **Possible values:**
      - Matplotlib (XMCDA label : matplotlib) (default)

      - Gnuplot (XMCDA label : gnuplot) 

- **Name:** Naming conventions

  *How categories and alternatives are labelled on the graph.*

  - **Type:** drop-down list
  - **Possible values:**
      - Only ids are shown (XMCDA label : id) 

      - Only names are shown (disambiguated by appending the ids, if needed) (XMCDA label : name) (default)

      - Names and ids are shown in that order (XMCDA label : name (id)) 

      - Ids and names are shown in that order (XMCDA label : id (name)) 


XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
    <programParameters>
        <programParameter id="x_axis" name="X-axis label">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="y_axis" name="Y-axis label">
            <values>
                <value>
                    <label>%2</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="colors" name="Colors">
            <!-- colormap: %colormap / nb colors: %nb_colors -->
            <values>%color-mono%color-A%color-B%color-a%color-b%color-c%color-d%color-e%color-f%color-g%color-h%color-i%color-j
            </values>
        </programParameter>
        <programParameter id="use_markers">
            <values>
                <value>
                    <boolean>%6</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="linestyle" name="Linestyle">
            <values>
                <value>
                    <label>%7</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="show_grid">
            <values>
                <value>
                    <boolean>%8</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="image_file_extension" name="Image file extension">
            <values>
                <value>
                    <label>%9a%9b</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="plotter" name="Plotter">
            <values>
                <value>
                    <label>%10</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="naming_conventions" name="Naming conventions">
            <values>
                <value>
                    <label>%11</label>
                </value>
            </values>
        </programParameter>
    </programParameters>


----------------------------



.. _plotFuzzyCriteriaScales-PyXMCDA_outputs:

Outputs
-------


- :ref:`glob:fuzzyCriteriaScales.{bmp,dia,fig,gif,hpgl,ico,jpg,jpe,pdf,png,ps,ps2,svg,svgz,tif} <plotFuzzyCriteriaScales-PyXMCDA-fuzzyCriteriaScalesPlot>`
- :ref:`plotFuzzyCriteriaScales.py <plotFuzzyCriteriaScales-PyXMCDA-fuzzyCriteriaScalesPlotScript>`
- :ref:`messages <plotFuzzyCriteriaScales-PyXMCDA-messages>`

.. _plotFuzzyCriteriaScales-PyXMCDA-fuzzyCriteriaScalesPlot:

fuzzy criteria scales plot
~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

Image containing all selected fuzzy criteriaScales plots. Format corresponds to the one given in parameters (default is .png).



XMCDA related:
..............

- **Tag:** None

----------------------------


.. _plotFuzzyCriteriaScales-PyXMCDA-fuzzyCriteriaScalesPlotScript:

fuzzy criteria scales plot script
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

Generated Python script that made the image. Given to enable users to later customize the appearance of the plots.



XMCDA related:
..............

- **Tag:** None

----------------------------


.. _plotFuzzyCriteriaScales-PyXMCDA-messages:

messages
~~~~~~~~


Description:
............

Status messages.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
