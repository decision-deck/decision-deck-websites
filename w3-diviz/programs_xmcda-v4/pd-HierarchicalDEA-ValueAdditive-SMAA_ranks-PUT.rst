:orphan:



.. _HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT:

HierarchicalDEA-ValueAdditive-SMAA_ranks
========================================

:Provider: PUT
:Version: 1.0

Description
-----------

Computes ranks for the given DMUs (alternatives) using SMAA-D method and Additive Data Envelopment Analysis Model 
        with hierarchical structure of inputs and outputs. For given number of samples  returns a matrix with alternatives in each row and rankings in each column. Single cell indicates how many samples of respective alternative gave respective position in ranking.

- **Contact:** 
            Anna Labijak <anna.labijak@cs.put.poznan.pl>
        



Inputs
------
(For outputs, see :ref:`below <HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT_outputs>`)


- :ref:`units <HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-units>`
- :ref:`inputsOutputsScales <HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-inputsOutputsScales>`
- :ref:`performanceTable <HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-performanceTable>`
- :ref:`hierarchy <HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-hierarchy>`
- :ref:`weightsLinearConstraints <HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-methodParameters>`

.. _HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-units:

units
~~~~~


Description:
............

A list of alternatives (DMUs).



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
                <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
            

----------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-inputsOutputsScales:

inputs/outputs scales
~~~~~~~~~~~~~~~~~~~~~


Description:
............

Information about inputs and outpus (leaf criteria) scales (preference directions) and optionally about boundaries



XMCDA related:
..............

- **Tag:** criteriaScales

- **Code:**

  ::

    
                
<criteriaScales>
    <criterionScale>
      <criterionID>[...]</criterionID>
      <scales>
        <scale>
          [...]
        </scale>
      </scales>
    </criterionScale>
    [...]
</criteriaScales>
            

----------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-performanceTable:

performance
~~~~~~~~~~~


Description:
............

A list of alternatives (DMUs) performances (on leaf hierarchy criteria).



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    
                <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<values><value>[...]</value></values>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>
            

----------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-hierarchy:

hierarchy
~~~~~~~~~


Description:
............

The hierarchical structure of criteria.



XMCDA related:
..............

- **Tag:** criteriaHierarchy

- **Code:**

  ::

    
                <criteriaHierarchy>
						<nodes>
                            <node>
                                <criterionID>[...]</criterionID>
                                <nodes>
                                    <node>
                                        <criterionID>[...]</criterionID>
                                        [...]
                                    </node>
                                    [...]
                                </nodes>
                            </node>
                        <nodes>
					</criteriaHierarchy>
            

----------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-weightsLinearConstraints:

weights constraints
~~~~~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

A list of hierarchy criteria linear constraints.



XMCDA related:
..............

- **Tag:** criteriaLinearConstraints

- **Code:**

  ::

    
                
            <criteriaLinearConstraints>
                <constraints>
                    <constraint>
                        <elements>
                            <element>
                                <criterionID> [...] </criterionID>
                                <coefficient>
                                    [...]
                                </coefficient>
                            </element>
                            [...]
                        </elements>
                        <operator>[...]</operator>
                        <rhs>
                            [...]
                        </rhs>
                    </constraint>
                    [...]
                </constraints>
            </criteriaLinearConstraints>
            

----------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-methodParameters:

parameters
~~~~~~~~~~


Description:
............

Represents parameters.
                "number of samples" represents the number of samples to generate;
                "hierarchy node" is the ID of the hierarchy criterion for which the analysis should be performed;
                "transformToUtilities" means if data should be tranformed into values from range [0-1];
                "boundariesProvided" means if inputsOutputs file contains information about min and max data for each factor.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** number of samples

  

  - **Constraint description:** The value should be a positive integer.

  - **Type:** integer
  - **Default value:** 100
- **Name:** hierarchy node

  

  - **Type:** string
  - **Default value:** "root"
- **Name:** transform to utilities

  

  - **Default value:** true
- **Name:** boundaries provided

  

  - **Default value:** false
- **Name:** random seed (-1 for default time-based seed)

  

  - **Constraint description:** The value should be a non-negative integer or -1 if no constant seed required.

  - **Type:** integer
  - **Default value:** -1

XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
                
    <programParameters>
        <parameter id="samplesNb">
            <values>
                <value><integer>%1</integer></value>
            </values>
        </parameter>
        <parameter id="hierarchyNode">
            <values>
                <value><label>%2</label></value>
            </values>
        </parameter>
        <parameter id="transformToUtilities">
      <values>
        <value><boolean>%3</boolean></value>
      </values>
		</parameter>
		<parameter id="boundariesProvided">
      <values>
        <value><boolean>%4</boolean></value>
      </values>
		</parameter>
        <parameter id="randomSeed">
            <values>
                <value><integer>%5</integer></value>
            </values>
        </parameter>
    </programParameters>
            

----------------------------



.. _HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT_outputs:

Outputs
-------


- :ref:`rankAcceptabilityIndices <HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-rankAcceptabilityIndices>`
- :ref:`avgRank <HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-avgRank>`
- :ref:`messages <HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-messages>`

.. _HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-rankAcceptabilityIndices:

rank acceptability indices
~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

A performance table for given alternatives. Single performance consists of attribute criterionID representing certain ranking, and a value representing ratio of samples attaining this ranking.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    
                <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID> Rank [...]</criterionID>
									<values>
									  <value>[...]</value>
									</values>
							</performance>
							[...]
						</alternativePerformances>
            [...]
					</performanceTable>
            

----------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-avgRank:

average rank
~~~~~~~~~~~~


Description:
............

A list of alternatives with average rank (obtained for given sample).



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
                <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
						  <values>
							  <value>[...]</value>
						  </values>
						</alternativeValue>
						[...]
					</alternativesValues>
            

----------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
