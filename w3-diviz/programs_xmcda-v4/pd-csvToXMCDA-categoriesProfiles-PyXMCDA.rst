:orphan:



.. _csvToXMCDA-categoriesProfiles-PyXMCDA:

csvToXMCDA-categoriesProfiles
=============================

:Provider: PyXMCDA
:Version: 2.0

Description
-----------

Transforms a file containing categories profiles from a comma-separated values (CSV) file to XMCDA compliant files.

- **Contact:** Sébastien Bigaret (sebastien.bigaret@telecom-bretagne.eu)

- **Web page:** https://gitlab.com/sbigaret/ws-pyxmcda



Inputs
------
(For outputs, see :ref:`below <csvToXMCDA-categoriesProfiles-PyXMCDA_outputs>`)


- :ref:`categoriesProfiles.csv <csvToXMCDA-categoriesProfiles-PyXMCDA-categoriesProfiles_csv>`
- :ref:`parameters <csvToXMCDA-categoriesProfiles-PyXMCDA-parameters>`

.. _csvToXMCDA-categoriesProfiles-PyXMCDA-categoriesProfiles_csv:

categories profiles (csv)
~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The categories profiles as a CSV file.

Example::

  cat1 (Bad),,a1
  cat22 (Medium),a1,a2
  cat3 (Good),a2,

The first column consists in categories ids (and names), in the form `id (name)` --the name between parenthesis is optional, and there is a parameter to disable the lookup of names altogether.

Each lines consists in 3 elements: the category, then the ids of the alternatives being respectively the lower bound and the upper bound of the category's profile.  Either one may be empty (but not both).



XMCDA related:
..............

- **Tag:** other

----------------------------


.. _csvToXMCDA-categoriesProfiles-PyXMCDA-parameters:

parameters
~~~~~~~~~~


Description:
............

Parameters of the program



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** CSV delimiter

  *Indicates the delimiter to use. Leave blank for auto-detection.  It is especially handful when the auto-detection fails to determine the csv delimiter.*

  - **Constraint description:** One character maximum

  - **Type:** string
  - **Default value:** ""
- **Name:** First column

  *Content of the first column*

  - **Type:** drop-down list
  - **Possible values:**
      - id (XMCDA label : false) 

      - id (name) (XMCDA label : true) (default)


XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
    <programParameters>
        <parameter id="csv_delimiter">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </parameter>
        <parameter id="name_in_id">
            <values>
                <value>
                    <boolean>%2</boolean>
                </value>
            </values>
        </parameter>
    </programParameters>


----------------------------



.. _csvToXMCDA-categoriesProfiles-PyXMCDA_outputs:

Outputs
-------


- :ref:`categoriesProfiles <csvToXMCDA-categoriesProfiles-PyXMCDA-categoriesProfiles>`
- :ref:`messages <csvToXMCDA-categoriesProfiles-PyXMCDA-messages>`

.. _csvToXMCDA-categoriesProfiles-PyXMCDA-categoriesProfiles:

categories profiles
~~~~~~~~~~~~~~~~~~~


Description:
............

The equivalent categories profiles.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _csvToXMCDA-categoriesProfiles-PyXMCDA-messages:

messages
~~~~~~~~


Description:
............

Status messages.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
