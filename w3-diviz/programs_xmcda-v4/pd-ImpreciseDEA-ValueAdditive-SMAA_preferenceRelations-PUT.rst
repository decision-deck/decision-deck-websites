:orphan:



.. _ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT:

ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations
===================================================

:Provider: PUT
:Version: 1.0

Description
-----------

Determines dominance relations for the given DMUs (alternatives) using SMAA-D method and Additive Data Envelopment Analysis Model with imprecise data. For given number of samples  returns a matrix with alternatives in each row and column. Single cell indicates how many samples of alternative in a row dominates alternative in a column.

- **Contact:** Anna Labijak <support@decision-deck.org>



Inputs
------
(For outputs, see :ref:`below <ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT_outputs>`)


- :ref:`units <ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-units>`
- :ref:`inputsOutputs <ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-inputsOutputs>`
- :ref:`inputsOutputsScales <ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-inputsOutputsScales>`
- :ref:`inputsOutputsFunctions <ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-inputsOutputsFunctions>` *(optional)*
- :ref:`performanceTable <ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-performanceTable>`
- :ref:`maxPerformanceTable <ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-maxPerformanceTable>` *(optional)*
- :ref:`weightsLinearConstraints <ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-methodParameters>`

.. _ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-units:

units
~~~~~


Description:
............

A list of alternatives (DMUs).



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
         <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
       

----------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-inputsOutputs:

inputs/outputs
~~~~~~~~~~~~~~


Description:
............

A list of criteria with specified preference direction. List has to contains at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output).



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
         <criteria>
                        <criterion>[...]</criterion>
                        [...]
                    </criteria>
       

----------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-inputsOutputsScales:

inputs/outputs scales
~~~~~~~~~~~~~~~~~~~~~


Description:
............

Informations about inputs and outputs scales and optionally about boundaries



XMCDA related:
..............

- **Tag:** criteriaScales

- **Code:**

  ::

    
         
<criteriaScales>
    <criterionScale>
      <criterionID>[...]</criterionID>
      <scales>
        <scale>
          [...]
        </scale>
      </scales>
    </criterionScale>
    [...]
</criteriaScales>
       

----------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-inputsOutputsFunctions:

inputs/outputs function shapes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

Shapes of functions assigned to inputs and outputs (if other than linear)



XMCDA related:
..............

- **Tag:** criteriaFunctions

- **Code:**

  ::

    
         
<criteriaFunctions>
    <criterionFunction>
      <criterionID>[...]</criterionID>
      <functions>
        <function>
          <piecewiseLinear>
            <segment>
              <head>
                [...]
              </head>
              <tail>
                [...]
              </tail>
            </segment>
          </piecewiseLinear>
        </function>
        [...]
      </functions>
    </criterionFunction>
    [...]
</criteriaFunctions>
       

----------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-performanceTable:

performance
~~~~~~~~~~~


Description:
............

A list of alternatives (DMUs) minimal performances (or exact performances if intervals should be created by tolerance).



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    
         <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<values><value>[...]</value></values>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>
       

----------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-maxPerformanceTable:

max performance
~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A list of alternatives (DMUs) maximal performances.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    
         <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<values><value>[...]</value></values>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>
       

----------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-weightsLinearConstraints:

weights constraints
~~~~~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

A list of criteria linear constraints.



XMCDA related:
..............

- **Tag:** criteriaLinearConstraints

- **Code:**

  ::

    
         
            <criteriaLinearConstraints>
                <constraints>
                    <constraint>
                        <elements>
                            <element>
                                <criterionID> [...] </criterionID>
                                <coefficient>
                                    [...]
                                </coefficient>
                            </element>
                            [...]
                        </elements>
                        <operator>[...]</operator>
                        <rhs>
                            [...]
                        </rhs>
                    </constraint>
                    [...]
                </constraints>
            </criteriaLinearConstraints>
       

----------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-methodParameters:

parameters
~~~~~~~~~~


Description:
............

Represents method parameters.
            "tolerance" represents the fraction for creating interval data (created interval: [data*(1-tolerance), data*(1+tolerance)]),
            "transformToUtilities" means if data should be tranformed into values from range [0-1],
            "boundariesProvided" means if inputsOutputs file contains information about min and max data for each factor,
            "functionShapeProvided" means if inputsOutputs file contains information about the shapes of value function for given factor,
            "samplesNb" determines number of samples used to calculate results.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** number of samples

  

  - **Constraint description:** The value should be a positive integer.

  - **Type:** integer
  - **Default value:** 100
- **Name:** tolerance

  

  - **Constraint description:** The value should be a non-negative.

  - **Type:** float
  - **Default value:** 0.0
- **Name:** transform to utilities

  

  - **Default value:** true
- **Name:** boundaries provided

  

  - **Default value:** false
- **Name:** function shapes provided

  

  - **Default value:** false

XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
      
	<programParameters>
		<parameter id="samplesNb">
			<values>
				<value><integer>%1</integer></value>
			</values>
		</parameter>
		<parameter id="tolerance">
			<values>
				<value><real>%2</real></value>
			</values>
		</parameter>
		<parameter id="transformToUtilities">
			<values>
				<value><boolean>%3</boolean></value>
			</values>
		</parameter>
		<parameter id="boundariesProvided">
			<values>
				<value><boolean>%4</boolean></value>
			</values>
		</parameter>
		<parameter id="functionShapeProvided">
			<values>
				<value><boolean>%5</boolean></value>
			</values>
		</parameter>
	</programParameters>


----------------------------



.. _ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT_outputs:

Outputs
-------


- :ref:`pairwiseOutrankingIndices <ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-pairwiseOutrankingIndices>`
- :ref:`messages <ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-messages>`

.. _ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-pairwiseOutrankingIndices:

pairwise outranking indices
~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

A performance table for given alternatives. Single performance consists of attribute criterionID representing dominated alternative, and a value representing ratio of samples dominating this alternative.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    
         <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID> geq [...]</criterionID>
								<values>
                    <value>[...]</value>
                </values>
							</performance>
							[...]
						</alternativePerformances>
            [...]
					</performanceTable>
       

----------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
