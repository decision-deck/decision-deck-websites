:orphan:



.. _applyCriteriaFunctionsToPerformanceTable-PyXMCDA:

applyCriteriaFunctionsToPerformanceTable
========================================

:Provider: PyXMCDA
:Version: 1.0

Description
-----------

Apply criteria functions to input performance table.

- **Contact:** Nicolas Duminy (nicolas.duminy@telecom-bretagne.eu)

- **Web page:** https://gitlab.com/nduminy/ws-pyxmcda



Inputs
------
(For outputs, see :ref:`below <applyCriteriaFunctionsToPerformanceTable-PyXMCDA_outputs>`)


- :ref:`alternatives <applyCriteriaFunctionsToPerformanceTable-PyXMCDA-alternatives>` *(optional)*
- :ref:`criteria <applyCriteriaFunctionsToPerformanceTable-PyXMCDA-criteria>` *(optional)*
- :ref:`performanceTable <applyCriteriaFunctionsToPerformanceTable-PyXMCDA-performanceTable>`
- :ref:`criteriaFunctions <applyCriteriaFunctionsToPerformanceTable-PyXMCDA-criteriaFunctions>`

.. _applyCriteriaFunctionsToPerformanceTable-PyXMCDA-alternatives:

alternatives
~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The alternatives. Only used to exclude inactive alternatives from computations (all are considered if not provided).



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _applyCriteriaFunctionsToPerformanceTable-PyXMCDA-criteria:

criteria
~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

The criteria. Only used to exclude inactive criteria from computations (all are considered if not provided).



XMCDA related:
..............

- **Tag:** criteria

----------------------------


.. _applyCriteriaFunctionsToPerformanceTable-PyXMCDA-performanceTable:

performance table
~~~~~~~~~~~~~~~~~


Description:
............

The performance table.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _applyCriteriaFunctionsToPerformanceTable-PyXMCDA-criteriaFunctions:

criteria functions
~~~~~~~~~~~~~~~~~~


Description:
............

Criteria functions to apply.



XMCDA related:
..............

- **Tag:** criteriaFunctions

----------------------------



.. _applyCriteriaFunctionsToPerformanceTable-PyXMCDA_outputs:

Outputs
-------


- :ref:`performanceTable <applyCriteriaFunctionsToPerformanceTable-PyXMCDA-performanceTable_out>`
- :ref:`messages <applyCriteriaFunctionsToPerformanceTable-PyXMCDA-messages>`

.. _applyCriteriaFunctionsToPerformanceTable-PyXMCDA-performanceTable_out:

performance table
~~~~~~~~~~~~~~~~~


Description:
............

The output performance table.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _applyCriteriaFunctionsToPerformanceTable-PyXMCDA-messages:

messages
~~~~~~~~


Description:
............

Status messages.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
