:orphan:



.. _RORUTA-RepresentativeValueFunction-PUT:

RORUTA-RepresentativeValueFunction
==================================

:Provider: PUT
:Version: 1.0

Description
-----------

The procedure finds a representative value function for a given set of necessary relations.

- **Contact:** Pawel Rychly (pawelrychly@gmail.com).



Inputs
------
(For outputs, see :ref:`below <RORUTA-RepresentativeValueFunction-PUT_outputs>`)


- :ref:`criteria <RORUTA-RepresentativeValueFunction-PUT-criteria>`
- :ref:`alternatives <RORUTA-RepresentativeValueFunction-PUT-alternatives>`
- :ref:`performances <RORUTA-RepresentativeValueFunction-PUT-performances>`
- :ref:`characteristic-points <RORUTA-RepresentativeValueFunction-PUT-characteristic-points>` *(optional)*
- :ref:`criteria-preference-directions <RORUTA-RepresentativeValueFunction-PUT-criteria-preference-directions>` *(optional)*
- :ref:`necessary-relations <RORUTA-RepresentativeValueFunction-PUT-necessary-relations>`
- :ref:`preferences <RORUTA-RepresentativeValueFunction-PUT-preferences>` *(optional)*
- :ref:`intensities-of-preferences <RORUTA-RepresentativeValueFunction-PUT-intensities-of-preferences>` *(optional)*
- :ref:`rank-related-requirements <RORUTA-RepresentativeValueFunction-PUT-rank-related-requirements>` *(optional)*
- :ref:`parameters <RORUTA-RepresentativeValueFunction-PUT-parameters>`

.. _RORUTA-RepresentativeValueFunction-PUT-criteria:

criteria
~~~~~~~~


Description:
............

A list of all considered criteria. The input value should be a valid XMCDA document whose main tag is criteria.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
                
                    <criteria>
                        <criterion id="%1" name="%1"></criterion>
                        [...]
                    </criteria>
                    
            

----------------------------


.. _RORUTA-RepresentativeValueFunction-PUT-alternatives:

alternatives
~~~~~~~~~~~~


Description:
............

The list of all considered alternatives. The input value should be a valid XMCDA document whose main tag is alternatives. Each alternative may be described using two attributes: id and name. While the first one denotes a machine readable name, the second represents a human readable name.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
<alternatives>
	<alternative id="..." name="..." />
	[...]
</alternatives>


----------------------------


.. _RORUTA-RepresentativeValueFunction-PUT-performances:

performances
~~~~~~~~~~~~


Description:
............

Description of evaluation of alternatives on different criteria. It is required to provide the IDs of both criteria and alternatives described previously. The input value should be provided as a valid XMCDA document whose main tag is performanceTable



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    
<performanceTable>
	<alternativePerformances>
		<alternativeID>...</alternativeID>
		<performance>
			<criterionID>...</criterionID>
			<values>
				<value>
					<real>...</real>
				</value>
			</values>
		</performance>
		[...]
	</alternativePerformances>
	[...]
<performanceTable>


----------------------------


.. _RORUTA-RepresentativeValueFunction-PUT-characteristic-points:

characteristic points
~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A set of values associated with the criteria. This input allows to determine what type of value function should be used for the particular criterion. For each criterion that has an associated greater than one value, a piecewise linear value function is used. In this case, the mentioned value denotes a number of characteristic points of this value function. For the criteria that are not listed in this file, or for these for which the provided values are lower than two uses a general value function. The input value should be provided as a valid XMCDA document whose main tag is criteriaValues. Each element should contain both an id of the criterion, and value tag.



XMCDA related:
..............

- **Tag:** criteriaValues

- **Code:**

  ::

    
<criteriaValues mcdaConcept="characteristicPoints">
	<criterionValue>
		<criterionID>...</criterionID>
		<values>
			<value>
				<integer>...</integer>
			</value>
		</values>
	</criterionValue>
	[...]
</criteriaValues>


----------------------------


.. _RORUTA-RepresentativeValueFunction-PUT-criteria-preference-directions:

criteria preference directions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A set of values associated with criteria that determine their preference direction (0 - gain, 1 - cost).



XMCDA related:
..............

- **Tag:** criteriaValues

- **Code:**

  ::

    
<criteriaValues mcdaConcept="preferenceDirection">
	<criterionValue>
		<criterionID>...</criterionID>
		<values>
			<value>
				<integer>...</integer>
			</value>
		</values>
	</criterionValue>
	[...]
</criteriaValues>


----------------------------


.. _RORUTA-RepresentativeValueFunction-PUT-necessary-relations:

necessary-relations
~~~~~~~~~~~~~~~~~~~


Description:
............

A list of all necessary weak preference relations on the set of alternatives. The input value should be a valid XMCDA document whose main tag is alternativesMatrix. Each relation is denoted as a pair of alternativesID.



XMCDA related:
..............

- **Tag:** alternativesMatrix

- **Code:**

  ::

    
<alternativesMatrix>
	<row>
		<alternativeID>...</alternativeID>
		<column>
			<alternativeID>...</alternativeID>
			<values><value><NA/></value></values>
		</column>
		[...]
	</row>
</alternativesMatrix>


----------------------------


.. _RORUTA-RepresentativeValueFunction-PUT-preferences:

preferences
~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Set of pairwise comparisons of reference alternatives. For a pair of alternatives three types of comparisons are supported. These are the strict preference, weak preference, and indifference. The input value should be provided as a valid XMCDA document whose main tag is alternativesComparisons. For each type of comparison, a separate alternativesComparisons tag should be used. Within these groups a mentioned types are denoted using a comparisonType tag by respectively strict, weak, and indif label. Comparisons should be provided as pairs of alternatives ids.



XMCDA related:
..............

- **Tag:** alternativesMatrix

- **Code:**

  ::

    
                
                    <alternativesComparisons>
                        <comparisonType>
                            %1<!-- type of preference: strong, weak, or indif -->
                        </comparisonType>
                        <pairs>
                            <pair>
                                <initial>
                                    <alternativeID>%2</alternativeID>
                                </initial>
                                <terminal>
                                    <alternativeID>%3</alternativeID>
                                </terminal>
                            </pair>
                            [...]
                        </pairs>
                    </alternativesComparisons>
                    [...]
                    
            

----------------------------


.. _RORUTA-RepresentativeValueFunction-PUT-intensities-of-preferences:

intensities-of-preferences
~~~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Set of comparisons of intensities of preference. For a pair of preference relations three types of comparisons are supported. These are the strict preference, weak, preference, and indifference. The input value should be provided as a valid XMCDA document whose main tag is alternativesComparisons. For each type of comparison, a separate alternativesComparisons tag should be used. Within these groups  aforementioned types are denoted using a comparisonType tag by respectively strict, weak, and indif label. Comparisons should be provided as pairs of two elementary sets of alternatives ids. The following form is expected:



XMCDA related:
..............

- **Tag:** alternativesMatrix

- **Code:**

  ::

    
                
                    <alternativesComparisons>
                        <comparisonType>%1</comparisonType>
                        <pairs>
                            <pair>
                                <initial>
                                    <alternativesSet>
                                        <element>
                                            <alternativeID>%2</alternativeID>
                                        </element>
                                        <element>
                                            <alternativeID>%3</alternativeID>
                                        </element>
                                    </alternativesSet>
                                </initial>
                                <terminal>
                                    <alternativesSet>
                                        <element>
                                            <alternativeID>%4</alternativeID>
                                        </element>
                                        <element>
                                            <alternativeID>%5</alternativeID>
                                        </element>
                                    </alternativesSet>
                                </terminal>
                            </pair>
                            [...]
                        </pairs>
                    </alternativesComparisons>
                    [...]
                    
            

----------------------------


.. _RORUTA-RepresentativeValueFunction-PUT-rank-related-requirements:

rank-related-requirements
~~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Set of rank-related requirements. In other words it is a set of  ranges of possible positions in the final ranking for a chosen alternatives. The input value should be provided as a valid XMCDA document whose main tag is alternativesValues. Each requirement should contain both an id of the reffered alternative and a pair of values that denote the desired range. These information should be provided within a separate alternativesValue tag.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    
                
                    <alternativesValues>
                        <alternativeValue>
                            <alternativeID>%1</alternativeID>
                            <value>
                                <interval>
                                    <lowerBound><integer>%2</integer></lowerBound>
                                    <upperBound><integer>%3</integer></upperBound>
                                </interval>
                            </value>
                        </alternativeValue>
                        [...]
                    </alternativesValues>
                    
            

----------------------------


.. _RORUTA-RepresentativeValueFunction-PUT-parameters:

parameters
~~~~~~~~~~


Description:
............

Method parameters
                    strict %1 - Single boolean value. Determines whether to use sctrictly increasing (true) or monotonously increasing (false) value functions
                    compromise %2 -A boolean parameter that determines does the compromise method should be used.



GUI information:
................


- **Name:** Use strictly increasing value functions?

  *Single boolean value. Determines whether to use sctrictly increasing (true) or monotonously increasing (false) value functions.*

  - **Default value:** false
- **Name:** Use a compromise method?

  *This parameter defines does the compromise method is used.*

  - **Default value:** false

XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
<programParameters>
	<parameter name="strict">
		<values>
			<value>
				<boolean>%1</boolean>
			</value>
		</values>
	</parameter>
	<parameter name="compromise">
		<values>
			<value>
				<boolean>%2</boolean>
			</value>
		</values>
	</parameter>
</programParameters>


----------------------------



.. _RORUTA-RepresentativeValueFunction-PUT_outputs:

Outputs
-------


- :ref:`representative-value-function <RORUTA-RepresentativeValueFunction-PUT-representative-value-function>`
- :ref:`messages <RORUTA-RepresentativeValueFunction-PUT-messages>`

.. _RORUTA-RepresentativeValueFunction-PUT-representative-value-function:

representative value function
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The lists of characteristic points of the representative value function that are provided for each criterion. The returned value is an XMCDA document whose main tag is criteriaFunctions. It contains a description of characteristic points on all considered criteria. The id attribute of each criterion tag denotes the id of described criterion. Each function is described using a criterionFunction tag.



XMCDA related:
..............

- **Tag:** criteriaFunctions

- **Code:**

  ::

    
<criteriaFunctions>
	<criterionFunction>
		<criterionID>...</criterionID>
		<functions>
			<function>
				<discrete>
					<point>
						<abscissa>
							<real>...</real>
						</abscissa>
						<ordinate>
							<real>...</real>
						</ordinate>
					</point>
					[...]
				</discrete>
			</function>
			[...]
		</functions>
	</criterionFunction>


----------------------------


.. _RORUTA-RepresentativeValueFunction-PUT-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
