:orphan:



.. _csvToXMCDA-criteriaThresholds-PyXMCDA:

csvToXMCDA-criteriaThresholds
=============================

:Provider: PyXMCDA
:Version: 2.0

Description
-----------

Transforms a file containing criteria discrimination thresholds and preference directions from a comma-separated values (CSV) file to three XMCDA compliant files, containing resp. the criteria ids, their preference direction and the related discrimination thresholds.

- **Contact:** Sébastien Bigaret (sebastien.bigaret@telecom-bretagne.eu)

- **Web page:** https://gitlab.com/sbigaret/ws-pyxmcda



Inputs
------
(For outputs, see :ref:`below <csvToXMCDA-criteriaThresholds-PyXMCDA_outputs>`)


- :ref:`thresholds.csv <csvToXMCDA-criteriaThresholds-PyXMCDA-csv>`
- :ref:`parameters <csvToXMCDA-criteriaThresholds-PyXMCDA-parameters>`

.. _csvToXMCDA-criteriaThresholds-PyXMCDA-csv:

criteria thresholds (csv)
~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

A csv with criteria' scales and thresholds.

Example:
  ,cost,risks,employment,connection
  ind,1,2,3,4
  pref,2,3,4,5
  veto,3,4.6,5,6
  preferenceDirection,min,max,min,max

The labels of the separation thresholds ("ind", "pref", "veto") can be chosen freely in order to be in accordance with the selected outranking method. Further separation thresholds can also be added. The last line always represents the preferenceDirection; its label must be "preferenceDirection".

The separator used in csv will be determined by examing the file (this means that it can be different than a comma: a semicolon, a tab or space character, etc.).

Thresholds values should be float; both decimal separator '.' and ',' are supported.  If a threshold value is left empty, the corresponding combination (criterion id, separation thresholds) is not present in the XMCDA output.  Same for "preferenceDirection": the corresponding tag 'scale' is present in the XMCDA output only if the preferenceDirection is supplied.



XMCDA related:
..............

- **Tag:** other

----------------------------


.. _csvToXMCDA-criteriaThresholds-PyXMCDA-parameters:

parameters
~~~~~~~~~~


Description:
............

Parameters of the method



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** CSV delimiter

  *Indicates the delimiter to use. Leave blank for auto-detection.  It is especially handful when the auto-detection fails to determine the csv delimiter.*

  - **Constraint description:** One character maximum

  - **Type:** string
  - **Default value:** ""
- **Name:** Default content

  *.*

  - **Type:** drop-down list
  - **Possible values:**
      - float (XMCDA label : float) (default)

      - integer (XMCDA label : integer) 

- **Name:** First line

  *Content of the first line*

  - **Type:** drop-down list
  - **Possible values:**
      - id (XMCDA label : false) 

      - id (name) (XMCDA label : true) (default)


XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
    <programParameters>
        <parameter id="csv_delimiter">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </parameter>
        <parameter id="default_prefix">
            <values>
                <value>
                    <label>%2</label>
                </value>
            </values>
        </parameter>
        <parameter id="name_in_id">
            <values>
                <value>
                    <boolean>%3</boolean>
                </value>
            </values>
        </parameter>
    </programParameters>


----------------------------



.. _csvToXMCDA-criteriaThresholds-PyXMCDA_outputs:

Outputs
-------


- :ref:`criteria <csvToXMCDA-criteriaThresholds-PyXMCDA-criteria>`
- :ref:`criteriaScales <csvToXMCDA-criteriaThresholds-PyXMCDA-criteria_scales>`
- :ref:`criteriaThresholds <csvToXMCDA-criteriaThresholds-PyXMCDA-criteria_thresholds>`
- :ref:`messages <csvToXMCDA-criteriaThresholds-PyXMCDA-messages>`

.. _csvToXMCDA-criteriaThresholds-PyXMCDA-criteria:

criteria
~~~~~~~~


Description:
............

The equivalent XMCDA file containing the criteria declared in the csv.



XMCDA related:
..............

- **Tag:** criteria

----------------------------


.. _csvToXMCDA-criteriaThresholds-PyXMCDA-criteria_scales:

criteria scales
~~~~~~~~~~~~~~~


Description:
............

The equivalent XMCDA file containing the preference directions of the criteria declared in the csv.



XMCDA related:
..............

- **Tag:** criteriaScales

----------------------------


.. _csvToXMCDA-criteriaThresholds-PyXMCDA-criteria_thresholds:

criteria thresholds
~~~~~~~~~~~~~~~~~~~


Description:
............

The equivalent XMCDA file containing the discrimination thresholds.



XMCDA related:
..............

- **Tag:** criteriaThresholds

----------------------------


.. _csvToXMCDA-criteriaThresholds-PyXMCDA-messages:

messages
~~~~~~~~


Description:
............

Status messages.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
