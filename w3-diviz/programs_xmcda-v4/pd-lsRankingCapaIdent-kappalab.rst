:orphan:



.. _lsRankingCapaIdent-kappalab:

lsRankingCapaIdent
==================

:Provider: kappalab
:Version: 1.0

Description
-----------

Identifies a Mobius capacity by means of an approach called TOMASO in the particular ranking framework. More precisely, given a preorder on the alternatives, and possibly additional linear constraints expressing preferences, importance of criteria, etc., this algorithm determines, if it exists, a capacity minimising the  "gap" between the given ranking and the one derived from the Choquet integral, and compatible with the additional linear constraints. The problem is solved by quadratic programming.

- **Contact:** Patrick Meyer (patrick.meyer@telecom-bretagne.eu)

- **Web page:** None

- **Reference:** P. Meyer, M. Roubens (2005), Choice, Ranking and Sorting in Fuzzy Multiple Criteria Decision Aid, in: J. Figueira, S. Greco, and M. Ehrgott, Eds, Multiple Criteria Decision Analysis: State of the Art Surveys, volume 78 of International Series in Operations Research and Management Science, chapter 12, pages 471-506. Springer Science + Business Media, Inc., New York. 



Inputs
------
(For outputs, see :ref:`below <lsRankingCapaIdent-kappalab_outputs>`)


- :ref:`criteria <lsRankingCapaIdent-kappalab-criteria>`
- :ref:`alternatives <lsRankingCapaIdent-kappalab-alternatives>`
- :ref:`performanceTable <lsRankingCapaIdent-kappalab-performanceTable>`
- :ref:`shapleyPreorder <lsRankingCapaIdent-kappalab-shapleyPreorder>` *(optional)*
- :ref:`interactionPreorder <lsRankingCapaIdent-kappalab-interactionPreorder>` *(optional)*
- :ref:`shapleyInterval <lsRankingCapaIdent-kappalab-shapleyInterval>` *(optional)*
- :ref:`interactionInterval <lsRankingCapaIdent-kappalab-interactionInterval>` *(optional)*
- :ref:`alternativesPreorder <lsRankingCapaIdent-kappalab-alternativesPreorder>`
- :ref:`kAdditivity <lsRankingCapaIdent-kappalab-kAdditivity>`
- :ref:`separationThreshold <lsRankingCapaIdent-kappalab-separationThreshold>`

.. _lsRankingCapaIdent-kappalab-criteria:

criteria
~~~~~~~~


Description:
............

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
	<criteria>
		<criterion>
			<active>[...]</active>
			[...]
		</criterion>
	    [...]
	</criteria>


----------------------------


.. _lsRankingCapaIdent-kappalab-alternatives:

alternatives
~~~~~~~~~~~~


Description:
............

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
	<alternatives>
		<alternative id="..." [...]>
			<active>[...]</active>
		</alternative>
		[...]
	</alternatives>


----------------------------


.. _lsRankingCapaIdent-kappalab-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~


Description:
............

A performance table. The evaluations should be numeric values, i.e. <real>, <integer> or <rational>.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _lsRankingCapaIdent-kappalab-shapleyPreorder:

shapleyPreorder
~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A valued relation on criteria expressing importance constraints on the critera. A numeric <value> indicates a minimal preference threshold for each <pair>. One <pair> represents an affirmation of the type "the Shapley importance index of criterion g1 is greater than the Shapley importance index of criterion g2 with preference threshold delta".



XMCDA related:
..............

- **Tag:** criteriaMatrix

----------------------------


.. _lsRankingCapaIdent-kappalab-interactionPreorder:

interactionPreorder
~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A valued relation on pairs of criteria expressing constraints on value of the the Shapley interaction index. A numeric <value> indicates a minimal preference threshold for each <pair> of the relation. One <pair> represents a constraint of the type "the Shapley interaction index of the pair (g1,g2) of criteria is greater than the Shapley interaction index of the pair (g3,g4) of criteria with preference threshold delta".



XMCDA related:
..............

- **Tag:** criteriaSets,criteriaSetsMatrix

----------------------------


.. _lsRankingCapaIdent-kappalab-shapleyInterval:

shapleyInterval
~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A list of <criterionValue> containing the constraints relative to the quantitative importance of the criteria. Each <criterionValue> contains an an <interval>. Each <criteriaValue> represents an affirmation of the type "the Shapley importance index of criterion g1 lies in the interval [a,b]".



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _lsRankingCapaIdent-kappalab-interactionInterval:

interactionInterval
~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A list of <criterionValue> containing the constraints relative to the type and the magnitude of the Shapley interaction index for pairs of criteria. Each <criterionValue> contains an an <interval>. Each <criteriaValue> represents an affirmation of the type "the Shapley interaction index of the pair (g1,g2) of criteria lies in the interval [a,b]".



XMCDA related:
..............

- **Tag:** criteriaSets,criteriaSetsValues

----------------------------


.. _lsRankingCapaIdent-kappalab-alternativesPreorder:

alternativesPreorder
~~~~~~~~~~~~~~~~~~~~


Description:
............

A valued relation relative to the preorder of the alternatives. A numeric <value> indicates a minimal preference threshold for each <pair> of the relation. One <pair> represents a constraint of the type "alternative a is preferred to alternative b with preference threshold delta".



XMCDA related:
..............

- **Tag:** alternativesMatrix

----------------------------


.. _lsRankingCapaIdent-kappalab-kAdditivity:

kAdditivity
~~~~~~~~~~~


Description:
............

Indicates the level of k-additivity of the Mobius capacity (the Mobius transform of subsets whose cardinal is superior to k vanishes).



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via the XMCDA file.

- **Name:** kAdditivity

  *Indicates the level of k-additivity of the Mobius capacity (the Mobius transform of subsets whose cardinal is superior to k vanishes).*

  - **Constraint description:** The value should be a positive integer, less than or equal to the number of criteria.

  - **Type:** integer
  - **Default value:** 1

XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
	<programParameters>
		<parameter name="kAdditivity"> <!-- name: REQUIRED -->
			<values>
				<value>
					<integer>%1</integer>
				</value>
			</values>
		</parameter>
	</programParameters>


----------------------------


.. _lsRankingCapaIdent-kappalab-separationThreshold:

separationThreshold
~~~~~~~~~~~~~~~~~~~


Description:
............

Threshold value indicating the minimal difference in terms of the Choquet integral between two neighbor alternatives in the given ranking.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** separationThreshold

  *Threshold value indicating the minimal difference in terms of the Choquet integral between two neighbor alternatives in the given ranking.*

  - **Constraint description:** The value should be a strictly positive float, less than the highest possible overall value.

  - **Type:** float

XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
	<programParameters>
		<parameter name="separationThreshold"> <!-- name: REQUIRED -->
			<values>
				<value>
					<real>%1</real>
				</value>
			</values>
		</parameter>
	</programParameters>


----------------------------



.. _lsRankingCapaIdent-kappalab_outputs:

Outputs
-------


- :ref:`mobiusCapacity <lsRankingCapaIdent-kappalab-mobiusCapacity>`
- :ref:`messages <lsRankingCapaIdent-kappalab-messages>`

.. _lsRankingCapaIdent-kappalab-mobiusCapacity:

mobiusCapacity
~~~~~~~~~~~~~~


Description:
............

The Mobius transform of a capacity.



XMCDA related:
..............

- **Tag:** criteriaSets,criteriaSetsValues

----------------------------


.. _lsRankingCapaIdent-kappalab-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
