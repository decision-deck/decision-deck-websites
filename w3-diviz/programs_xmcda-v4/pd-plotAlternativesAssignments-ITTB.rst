:orphan:



.. _plotAlternativesAssignments-ITTB:

plotAlternativesAssignments
===========================

:Provider: ITTB
:Version: 2.0

Description
-----------

This web service generates a plot representing the alternatives assignments. Colors can be used. You can specify how to display the different categories: by line, by column or in a grid. The plots can also be ordered: alphabetical order, its inverse or by categories.

- **Contact:** Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)



Inputs
------
(For outputs, see :ref:`below <plotAlternativesAssignments-ITTB_outputs>`)


- :ref:`alternatives <plotAlternativesAssignments-ITTB-alternatives>`
- :ref:`categories <plotAlternativesAssignments-ITTB-categories>` *(optional)*
- :ref:`alternativesAssignments <plotAlternativesAssignments-ITTB-alternativesAssignments>`
- :ref:`parameters <plotAlternativesAssignments-ITTB-parameters>`

.. _plotAlternativesAssignments-ITTB-alternatives:

alternatives
~~~~~~~~~~~~


Description:
............

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _plotAlternativesAssignments-ITTB-categories:

categories
~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The list of categories and their rank (required!).



XMCDA related:
..............

- **Tag:** categories

- **Code:**

  ::

    
	<categories>
		<category id=[...]>
			<active>[...]</active>
			<rank>
				<integer>[...]</integer>
			</rank>
		</category>
		[...]
	</categories>


----------------------------


.. _plotAlternativesAssignments-ITTB-alternativesAssignments:

alternatives assignments
~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

Assignment of reference alternatives



XMCDA related:
..............

- **Tag:** alternativesAssignments

----------------------------


.. _plotAlternativesAssignments-ITTB-parameters:

parameters
~~~~~~~~~~


Description:
............

None



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** Plot title:

  *String for the title of the plot. The default value is an empty field.*

  - **Type:** string
  - **Default value:** ""
- **Name:** Unique plot?

  *In a unique plot, only one image is generated containing all the alternatives affectations. Multiple plots can be obtained. The default value is true.*

  - **Type:** drop-down list
  - **Possible values:**
      - Yes (XMCDA label : true) (default)

      - No (XMCDA label : false) 

- **Name:** Display assignments by:

  *In the case of a unique plot, you can specify how to display the categories: by line, by column or in a grid. The default value is by column.*

  - **Type:** drop-down list
  - **Possible values:**
      - Column (XMCDA label : Column) (default)

      - Line (XMCDA label : Line) 

      - Grid (XMCDA label : Grid) 

- **Name:** Order:

  *The parameter which says if the categories are sorted out. Choose between "increasing" (alphabetical order), "decreasing" or "categories".*

  - **Type:** drop-down list
  - **Possible values:**
      - increasing (XMCDA label : increasing) (default)

      - decreasing (XMCDA label : decreasing) 

      - categories (XMCDA label : categories) 

- **Name:** Choose color:

  *Choose between "Black", "Red", "Blue", "Green", "Yellow", "Magenta" and "Cyan".*

  - **Type:** drop-down list
  - **Possible values:**
      - Black (XMCDA label : Black) (default)

      - Red (XMCDA label : Red) 

      - Blue (XMCDA label : Blue) 

      - Green (XMCDA label : Green) 

      - Yellow (XMCDA label : Yellow) 

      - Magenta (XMCDA label : Magenta) 

      - Cyan (XMCDA label : Cyan) 

- **Name:** Plot alternatives in:

  *The categories can be plotted in a rectangle, an oval or a diamond.*

  - **Type:** drop-down list
  - **Possible values:**
      - rectangle (XMCDA label : rectangle) 

      - oval (XMCDA label : oval) (default)

      - diamond (XMCDA label : diamond) 


XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
	<programParameters>
		<parameter id="plot_title" name="Plot title">
			<values>
				<value>
					<label>%2</label>
				</value>
			</values>
		</parameter>
		<parameter id="unique_plot" name="Unique plot">
			<values>
				<value>
					<label>%3</label>
				</value>
			</values>
		</parameter>
		<parameter id="plots_display" name="Plots' display">
			<values>
				<value>
					<label>%4</label>
				</value>
			</values>
		</parameter>
		<parameter id="order" name="Order">
			<values>
				<value>
					<label>%5</label>
				</value>
			</values>
		</parameter>
		<parameter id="selected_color" name="Selected color">
			<values>
				<value>
					<label>%7</label>
				</value>
			</values>
		</parameter>
		<parameter id="categories_shape" name="Categories shape">
			<values>
				<value>
					<label>%8</label>
				</value>
			</values>
		</parameter>
	</programParameters>
            

----------------------------



.. _plotAlternativesAssignments-ITTB_outputs:

Outputs
-------


- :ref:`alternativesAssignments.png <plotAlternativesAssignments-ITTB-png>`
- :ref:`messages <plotAlternativesAssignments-ITTB-messages>`

.. _plotAlternativesAssignments-ITTB-png:

alternativesAssignments (png)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

A string containing the base64 representation of the png image of the generated plot.



XMCDA related:
..............

- **Tag:** other

----------------------------


.. _plotAlternativesAssignments-ITTB-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
