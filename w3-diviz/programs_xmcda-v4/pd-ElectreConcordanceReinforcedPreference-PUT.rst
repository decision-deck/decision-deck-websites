:orphan:



.. _ElectreConcordanceReinforcedPreference-PUT:

ElectreConcordanceReinforcedPreference
======================================

:Provider: PUT
:Version: 0.1.0

Description
-----------

Computes concordance matrix using procedure which is common to the most methods from the Electre family.

This module is an extended version of 'ElectreConcordance' - it brings the concept of 'reinforced_preference', which boils down to the new threshold of the same name and a new input file where the 'reinforcement factors' are defined (one for each criterion where 'reinforced_preference' threshold is present).

- **Web page:** http://github.com/xor-xor/electre_diviz

- **Reference:** Bernard Roy and Roman Słowiński; Handling effects of reinforced preference and counter-veto in credibility of outranking; European Journal of Operational Research 188(1):185–190; 2008; doi:10.1016/j.ejor.2007.04.005



Inputs
------
(For outputs, see :ref:`below <ElectreConcordanceReinforcedPreference-PUT_outputs>`)


- :ref:`alternatives <ElectreConcordanceReinforcedPreference-PUT-input1>`
- :ref:`criteria <ElectreConcordanceReinforcedPreference-PUT-input3>`
- :ref:`criteria_scales <ElectreConcordanceReinforcedPreference-PUT-input3b>`
- :ref:`criteria_thresholds <ElectreConcordanceReinforcedPreference-PUT-input3c>` *(optional)*
- :ref:`performance_table <ElectreConcordanceReinforcedPreference-PUT-input4>`
- :ref:`profiles_performance_table <ElectreConcordanceReinforcedPreference-PUT-input5>` *(optional)*
- :ref:`weights <ElectreConcordanceReinforcedPreference-PUT-input6>`
- :ref:`reinforcement_factors <ElectreConcordanceReinforcedPreference-PUT-input7>` *(optional)*
- :ref:`classes_profiles <ElectreConcordanceReinforcedPreference-PUT-input2>` *(optional)*
- :ref:`method_parameters <ElectreConcordanceReinforcedPreference-PUT-input8>`

.. _ElectreConcordanceReinforcedPreference-PUT-input1:

alternatives
~~~~~~~~~~~~


Description:
............

Alternatives to consider.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _ElectreConcordanceReinforcedPreference-PUT-input3:

criteria
~~~~~~~~


Description:
............

Criteria to consider, possibly with 'preference', 'indifference' and 'reinforced preference' thresholds (see also 'reinforcement_factors' input below). Each criterion must have a preference direction specified (min or max). It is worth mentioning that this module allows to define thresholds as constants as well as linear functions.



XMCDA related:
..............

- **Tag:** criteria

----------------------------


.. _ElectreConcordanceReinforcedPreference-PUT-input3b:

criteria scales
~~~~~~~~~~~~~~~


Description:
............

The scales of the criteria to consider. Each criterion must have a preference direction specified (min or max).



XMCDA related:
..............

- **Tag:** criteriaScales

----------------------------


.. _ElectreConcordanceReinforcedPreference-PUT-input3c:

criteria thresholds
~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The criteria' 'preference', 'indifference' and 'reinforced preference' thresholds (see also 'reinforcement_factors' input below). It is worth mentioning that this module allows to define thresholds as constants as well as linear functions.



XMCDA related:
..............

- **Tag:** criteriaThresholds

----------------------------


.. _ElectreConcordanceReinforcedPreference-PUT-input4:

performance_table
~~~~~~~~~~~~~~~~~


Description:
............

The performance of alternatives.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _ElectreConcordanceReinforcedPreference-PUT-input5:

profiles_performance_table
~~~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The performance of profiles (boundary or central).



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _ElectreConcordanceReinforcedPreference-PUT-input6:

weights
~~~~~~~


Description:
............

Weights of criteria to consider.



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _ElectreConcordanceReinforcedPreference-PUT-input7:

reinforcement_factors
~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Definitions of so-called 'reinforcement factors', one per each criterion for which 'reinforcement threshold' has been defined. For more regarding these concepts see the paper from 'Reference' section.

Technically, this input is optional, but it doesn't make much sense to use 'ElectreConcordanceReinforcedPreference' without it, since in such scenario it will fall back to 'ElectreConcordance'.



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _ElectreConcordanceReinforcedPreference-PUT-input2:

classes_profiles
~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Definitions of profiles (boundary or central) which should be used for classes (categories) representation.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _ElectreConcordanceReinforcedPreference-PUT-input8:

method_parameters
~~~~~~~~~~~~~~~~~


Description:
............

This parameter specifies the type of elements provided for comparison.

Choosing 'boundary_profiles' or 'central_profiles' requires providing inputs 'classes_profiles' and 'profiles_performance_table' as well (which are optional by default).



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** comparison_with

  

  - **Type:** drop-down list
  - **Possible values:**
      - alternatives vs alternatives (XMCDA label : alternatives) (default)

      - alternatives vs boundary profiles (XMCDA label : boundary_profiles) 

      - alternatives vs central (characteristic) profiles (XMCDA label : central_profiles) 


XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
<programParameters>
	<parameter name="comparison_with">
		<values>
			<value>
				<label>%1</label>
			</value>
		</values>
	</parameter>
</programParameters>


----------------------------



.. _ElectreConcordanceReinforcedPreference-PUT_outputs:

Outputs
-------


- :ref:`concordance <ElectreConcordanceReinforcedPreference-PUT-output1>`
- :ref:`messages <ElectreConcordanceReinforcedPreference-PUT-output2>`

.. _ElectreConcordanceReinforcedPreference-PUT-output1:

concordance
~~~~~~~~~~~


Description:
............

Concordance matrix computed from the given data. This matrix aggregates partial concordances from all criteria into single concordance index per pair of alternatives or alternatives/profiles.



XMCDA related:
..............

- **Tag:** alternativesMatrix

----------------------------


.. _ElectreConcordanceReinforcedPreference-PUT-output2:

messages
~~~~~~~~


Description:
............

Messages or errors generated by this module.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
