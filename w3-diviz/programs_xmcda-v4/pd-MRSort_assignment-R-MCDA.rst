:orphan:



.. _MRSort_assignment-R-MCDA:

MRSort_assignment
=================

:Provider: R-MCDA
:Version: 1.0

Description
-----------

This simplification of the Electre TRI method uses the pessimistic assignment rule, without indifference or preference thresholds attached to criteria.
Only a binary discordance condition is considered, i.e. a veto forbids an outranking in any possible concordance situation, or not.

- **Contact:** Alexandru Olteanu (alexandru.olteanu@univ-ubs.fr)

- **Reference:** Bouyssou, D. and Marchant, T. An axiomatic approach to noncompensatory sorting methods in MCDM, II: more than two categories. European Journal of Operational Research, 178(1): 246--276, 2007.



Inputs
------
(For outputs, see :ref:`below <MRSort_assignment-R-MCDA_outputs>`)


- :ref:`alternatives <MRSort_assignment-R-MCDA-inalt>`
- :ref:`categories <MRSort_assignment-R-MCDA-incateg>`
- :ref:`performanceTable <MRSort_assignment-R-MCDA-inperf>`
- :ref:`categoriesProfilesPerformanceTable <MRSort_assignment-R-MCDA-incatprofpt>`
- :ref:`vetoProfilesPerformanceTable <MRSort_assignment-R-MCDA-invetoprofpt>` *(optional)*
- :ref:`criteria <MRSort_assignment-R-MCDA-incrit>`
- :ref:`criteriaWeights <MRSort_assignment-R-MCDA-weights>`
- :ref:`categoriesProfiles <MRSort_assignment-R-MCDA-incatprof>`
- :ref:`vetoProfiles <MRSort_assignment-R-MCDA-invetoprof>` *(optional)*
- :ref:`categoriesRanks <MRSort_assignment-R-MCDA-incategval>`
- :ref:`majorityThreshold <MRSort_assignment-R-MCDA-majority>`

.. _MRSort_assignment-R-MCDA-inalt:

alternatives
~~~~~~~~~~~~


Description:
............

A complete list of alternatives to be considered by the MR-Sort method.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _MRSort_assignment-R-MCDA-incateg:

categories
~~~~~~~~~~


Description:
............

A list of categories to which the alternatives will be assigned.



XMCDA related:
..............

- **Tag:** categories

----------------------------


.. _MRSort_assignment-R-MCDA-inperf:

performance table
~~~~~~~~~~~~~~~~~


Description:
............

The evaluations of the alternatives on the set of criteria.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _MRSort_assignment-R-MCDA-incatprofpt:

categories profiles performance table
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The evaluations of the category profiles.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _MRSort_assignment-R-MCDA-invetoprofpt:

veto profiles performance table
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The evaluations of the veto profiles.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _MRSort_assignment-R-MCDA-incrit:

criteria scales
~~~~~~~~~~~~~~~


Description:
............

A list of criteria on which the alternatives are evaluated. For each criterion, the preference direction should be provided.



XMCDA related:
..............

- **Tag:** criteriaScales

----------------------------


.. _MRSort_assignment-R-MCDA-weights:

criteria weights
~~~~~~~~~~~~~~~~


Description:
............

The criteria weights.



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _MRSort_assignment-R-MCDA-incatprof:

categories profiles
~~~~~~~~~~~~~~~~~~~


Description:
............

The categories delimiting profiles.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _MRSort_assignment-R-MCDA-invetoprof:

veto profiles
~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The categories veto profiles.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _MRSort_assignment-R-MCDA-incategval:

categories ranks
~~~~~~~~~~~~~~~~


Description:
............

A list of categories ranks, 1 stands for the most preferred category and the higher the number the lower the preference for that category.



XMCDA related:
..............

- **Tag:** categoriesValues

----------------------------


.. _MRSort_assignment-R-MCDA-majority:

majority threshold
~~~~~~~~~~~~~~~~~~


Description:
............

The majority threshold.



XMCDA related:
..............

- **Tag:** programParameters

----------------------------



.. _MRSort_assignment-R-MCDA_outputs:

Outputs
-------


- :ref:`alternativesAssignments <MRSort_assignment-R-MCDA-outaffect>`
- :ref:`messages <MRSort_assignment-R-MCDA-msg>`

.. _MRSort_assignment-R-MCDA-outaffect:

alternatives assignments
~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The alternatives assignments to categories.



XMCDA related:
..............

- **Tag:** alternativesAssignments

----------------------------


.. _MRSort_assignment-R-MCDA-msg:

messages
~~~~~~~~


Description:
............

Messages from the execution of the webservice. Possible errors in the input data will be given here.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
