:orphan:



.. _plotCriteriaFunctions-PyXMCDA:

plotCriteriaFunctions
=====================

:Provider: PyXMCDA
:Version: 1.3

Description
-----------

Generate plots from provided criteriaFunctions as well as the scripts generating these plots.

- **Contact:** Nicolas Duminy (nicolas.duminy@telecom-bretagne.eu)

- **Web page:** https://gitlab.com/nduminy/ws-pyxmcda



Inputs
------
(For outputs, see :ref:`below <plotCriteriaFunctions-PyXMCDA_outputs>`)


- :ref:`criteria <plotCriteriaFunctions-PyXMCDA-criteria>` *(optional)*
- :ref:`criteriaFunctions <plotCriteriaFunctions-PyXMCDA-criteriaFunctions>`
- :ref:`parameters <plotCriteriaFunctions-PyXMCDA-parameters>` *(optional)*

.. _plotCriteriaFunctions-PyXMCDA-criteria:

criteria
~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The criteria to be plotted. All are plotted if not provided.



XMCDA related:
..............

- **Tag:** criteria

----------------------------


.. _plotCriteriaFunctions-PyXMCDA-criteriaFunctions:

criteria functions
~~~~~~~~~~~~~~~~~~


Description:
............

The criteria functions.



XMCDA related:
..............

- **Tag:** criteriaFunctions

----------------------------


.. _plotCriteriaFunctions-PyXMCDA-parameters:

parameters
~~~~~~~~~~


Description:
............

Parameters of the method



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** X-axis label

  *X-axis name to display below each plot.*

  - **Type:** string
  - **Default value:** "x"
- **Name:** Y-axis label

  *Y-axis name to display on the left of each plot.*

  - **Type:** string
  - **Default value:** "y"
- **Name:** Color

  *Color used to draw the plots.*

  - **Type:** drop-down list
  - **Possible values:**
      - red (XMCDA label : r) 

      - blue (XMCDA label : b) 

      - green (XMCDA label : g) 

      - cyan (XMCDA label : c) 

      - magenta (XMCDA label : m) 

      - yellow (XMCDA label : y) 

      - black (XMCDA label : k) (default)

      - white (XMCDA label : w) 

- **Name:** Marker

  *Marker used to plot points of discrete and piecewise linear functions..*

  - **Type:** drop-down list
  - **Possible values:**
      - point (XMCDA label : .) 

      - none (XMCDA label : None) 

      - circle (XMCDA label : o) (default)

      - triangle down (XMCDA label : v) 

      - triangle up (XMCDA label : ^) 

      - triangle left (XMCDA label : &lt;) 

      - triangle right (XMCDA label : &gt;) 

      - square (XMCDA label : s) 

      - pentagon (XMCDA label : p) 

      - star (XMCDA label : *) 

      - hexagon 1 (XMCDA label : h) 

      - hexagon 2 (XMCDA label : H) 

      - plus (XMCDA label : +) 

      - x (XMCDA label : x) 

      - thin diamond (XMCDA label : d) 

      - diamond (XMCDA label : D) 

      - vline (XMCDA label : |) 

      - hline (XMCDA label : _) 

      - plus filled (XMCDA label : P) 

      - x (filled) (XMCDA label : X) 

- **Name:** Linestyle

  *Linestyle used to plot lines.*

  - **Type:** drop-down list
  - **Possible values:**
      - solid line (XMCDA label : solid) 

      - dashed line (XMCDA label : dashed) (default)

      - dashed and dotted line (XMCDA label : dashdot) 

      - dotted line (XMCDA label : dotted) 

      - no line (XMCDA label : None) 

- **Name:** Show grid

  *Set if grid is visible on each plot.*

  - **Default value:** false
- **Name:** Image file extension

  *File extension of generated image figure.*

  - **Type:** drop-down list
  - **Possible values:**
      - .eps (Encapsulated PostScript) (XMCDA label : eps) 

      - .jpg (Joint Photographic Experts Group) (XMCDA label : jpg) 

      - .jpeg (Joint Photographic Experts Group) (XMCDA label : jpeg) 

      - .pdf (Portable Document Format) (XMCDA label : pdf) 

      - .pgf (Progressive Graphics File) (XMCDA label : pgf) 

      - .png (Portable Network Graphics) (XMCDA label : png) (default)

      - .ps (PostScript) (XMCDA label : ps) 

      - .raw (Raw RGBA bitmap) (XMCDA label : raw) 

      - .rgba (Silicon Graphics RGB) (XMCDA label : rgba) 

      - .svg (Scalable Vector Graphics) (XMCDA label : svg) 

      - .svgz (Compressed Scalable Vector Graphics) (XMCDA label : svgz) 

      - .tif (Tagged Image File Format) (XMCDA label : tif) 

      - .tiff (Tagged Image File Format) (XMCDA label : tiff) 

- **Name:** Image file extension

  *File extension of generated image figure.*

  - **Type:** drop-down list
  - **Possible values:**
      - .eps (Encapsulated PostScript) (XMCDA label : eps) 

      - .jpg (Joint Photographic Experts Group) (XMCDA label : jpg) 

      - .jpeg (Joint Photographic Experts Group) (XMCDA label : jpeg) 

      - .pdf (Portable Document Format) (XMCDA label : pdf) 

      - .png (Portable Network Graphics) (XMCDA label : png) (default)

      - .svg (Scalable Vector Graphics) (XMCDA label : svg) 

- **Name:** Plotter

  *Plotter used to generate image.*

  - **Type:** drop-down list
  - **Possible values:**
      - Matplotlib (XMCDA label : matplotlib) (default)

      - Gnuplot (XMCDA label : gnuplot) 

- **Name:** Order by

  *Defines how criteria will be sorted on the figure.*

  - **Type:** drop-down list
  - **Possible values:**
      - ids (XMCDA label : id) (default)

      - names (XMCDA label : name) 

- **Name:** Reverse order

  *Defines if order direction should be reversed (descending order) or not.*

  - **Default value:** false
- **Name:** Linear interpolation

  *Set if discrete criterionFunctions with numeric x-axis are to be interpolated or not.*

  - **Default value:** false
- **Name:** Naming conventions

  *How categories and alternatives are labelled on the graph.*

  - **Type:** drop-down list
  - **Possible values:**
      - Only ids are shown (XMCDA label : id) 

      - Only names are shown (disambiguated by appending the ids, if needed) (XMCDA label : name) (default)

      - Names and ids are shown in that order (XMCDA label : name (id)) 

      - Ids and names are shown in that order (XMCDA label : id (name)) 


XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
    <programParameters>
        <programParameter id="x_axis" name="X-axis label">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="y_axis" name="Y-axis label">
            <values>
                <value>
                    <label>%2</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="color" name="Color">
            <values>
                <value>
                    <label>%3</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="marker" name="Marker">
            <values>
                <value>
                    <label>%4</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="linestyle" name="Linestyle">
            <values>
                <value>
                    <label>%5</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="show_grid">
            <values>
                <value>
                    <boolean>%6</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="image_file_extension" name="Image file extension">
            <values>
                <value>
                    <label>%7a%7b</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="plotter" name="Plotter">
            <values>
                <value>
                    <label>%8</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="order_by" name="Order by">
            <values>
                <value>
                    <label>%9</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="reverse_order" name="Reverse order">
            <values>
                <value>
                    <boolean>%10</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="linear_interpolation">
            <values>
                <value>
                    <boolean>%11</boolean>
                </value>
            </values>
        </programParameter>
	<programParameter id="naming_conventions" name="Naming conventions">
	        <values>
                <value>
		            <label>%12</label>
                </value>
	        </values>
        </programParameter>
    </programParameters>


----------------------------



.. _plotCriteriaFunctions-PyXMCDA_outputs:

Outputs
-------


- :ref:`glob:criteriaFunctions.{bmp,dia,fig,gif,hpgl,ico,jpg,jpeg,jpe,pdf,png,ps,ps2,svg,svgz,tif,tiff} <plotCriteriaFunctions-PyXMCDA-criteriaFunctionsPlot>`
- :ref:`glob:plot_criteriaFunctions.{py,plt} <plotCriteriaFunctions-PyXMCDA-criteriaFunctionsPlotScript>`
- :ref:`messages <plotCriteriaFunctions-PyXMCDA-messages>`

.. _plotCriteriaFunctions-PyXMCDA-criteriaFunctionsPlot:

criteria functions plot
~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

Image containing all selected criteriaFunctions plots. Format corresponds to the one given in parameters (default is .png).



XMCDA related:
..............

- **Tag:** None

----------------------------


.. _plotCriteriaFunctions-PyXMCDA-criteriaFunctionsPlotScript:

criteria functions plot script
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

Generated Python script that made the image. Given to enable users to later customize the appearance of the plots. Extension is .py if matplotlib is used, .plt if gnuplot.



XMCDA related:
..............

- **Tag:** None

----------------------------


.. _plotCriteriaFunctions-PyXMCDA-messages:

messages
~~~~~~~~


Description:
............

Status messages.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
