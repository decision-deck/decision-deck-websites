
:orphan:

.. _plotAlternativesMatrix-PyXMCDA:

plotAlternativesMatrix
======================

:Provider: PyXMCDA
:Version: 1.1

Description
-----------

Generate graph representing provided alternativesMatrix as well as the dot files generating this graph.
Values associated to transitions can be shown without any preprocessing.
Graphviz dot program and format are used by this service.

- **Contact:** Nicolas Duminy (nicolas.duminy@telecom-bretagne.eu)

- **Web page:** https://gitlab.com/nduminy/ws-pyxmcda



Inputs
------
(For outputs, see :ref:`below <plotAlternativesMatrix-PyXMCDA_outputs>`)


- :ref:`alternatives <plotAlternativesMatrix-PyXMCDA-alternatives>` *(optional)*
- :ref:`alternativesMatrix <plotAlternativesMatrix-PyXMCDA-alternativesMatrix>`
- :ref:`parameters <plotAlternativesMatrix-PyXMCDA-parameters>` *(optional)*

.. _plotAlternativesMatrix-PyXMCDA-alternatives:

alternatives
~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The alternatives to be plotted. All are plotted if not provided.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _plotAlternativesMatrix-PyXMCDA-alternativesMatrix:

alternatives matrix
~~~~~~~~~~~~~~~~~~~


Description:
............

The alternatives matrix to be plotted.



XMCDA related:
..............

- **Tag:** alternativesMatrix

----------------------------


.. _plotAlternativesMatrix-PyXMCDA-parameters:

parameters
~~~~~~~~~~


Description:
............

Parameters of the method



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** Color

  *Color used in the plots.*

  - **Type:** drop-down list
  - **Possible values:**
      - Black (XMCDA label : black) (default)

      - White (XMCDA label : white) 

      - Red (XMCDA label : red) 

      - Green (XMCDA label : green) 

      - Blue (XMCDA label : blue) 

      - Magenta (XMCDA label : magenta) 

      - Yellow (XMCDA label : yellow) 

      - Cyan (XMCDA label : cyan) 

      - Orange (XMCDA label : orange) 

      - Pink (XMCDA label : pink) 

      - Brown (XMCDA label : brown) 

      - Gray (XMCDA label : gray) 

- **Name:** Shape

  *Shape use to represent categories.*

  - **Type:** drop-down list
  - **Possible values:**
      - Rectangle (XMCDA label : box) 

      - Circle (XMCDA label : circle) 

      - Diamond (XMCDA label : diamond) 

      - Ellipse (XMCDA label : ellipse) 

      - Oval (XMCDA label : oval) (default)

      - Polygon (XMCDA label : polygon) 

      - Triangle (XMCDA label : triangle) 

- **Name:** Image file extension

  *File extension of generated image figure.*

  - **Type:** drop-down list
  - **Possible values:**
      - .bmp (Windows Bitmap Format) (XMCDA label : bmp) 

      - .dia (DIA Format) (XMCDA label : dia) 

      - .fig (FIG graphics format) (XMCDA label : fig) 

      - .gif (Graphics Interchange Format) (XMCDA label : gif) 

      - .hpgl (Hewlett Packard Graphic Language 2) (XMCDA label : hpgl) 

      - .ico (Icon Image File Format) (XMCDA label : ico) 

      - .jpg (Joint Photographic Experts Group) (XMCDA label : jpg) 

      - .jpe (Joint Photographic Experts Group) (XMCDA label : jpe) 

      - .pdf (Portable Document Format) (XMCDA label : pdf) 

      - .png (Portable Network Graphics) (XMCDA label : png) (default)

      - .ps (PostScript) (XMCDA label : ps) 

      - .ps2 (PostScript for PDF) (XMCDA label : ps2) 

      - .svg (Scalable Vector Graphics) (XMCDA label : svg) 

      - .svgz (Compressed Scalable Vector Graphics) (XMCDA label : svgz) 

      - .tif (Tagged Image File Format) (XMCDA label : tif) 

- **Name:** Show values

  *Defines if values are shown (between parentheses) on the plots or not.*

  - **Default value:** false
- **Name:** Directed graph

  *Defines if graph representing alternatives matrix is directed or not.*

  - **Type:** drop-down list
  - **Possible values:**
      - Directed (XMCDA label : true) (default)

      - Undirected (XMCDA label : false) 

- **Name:** Transitive reduction

  *Defines if transitive reduction is applied or not.*

  - **Default value:** false
- **Name:** Transitive closure

  *Defines if transitive closure is applied or not.*

  - **Default value:** false
- **Name:** Naming conventions

  *How categories and alternatives are labelled on the graph.*

  - **Type:** drop-down list
  - **Possible values:**
      - Only ids are shown (XMCDA label : id) 

      - Only names are shown (disambiguated by appending the ids, if needed) (XMCDA label : name) (default)

      - Names and ids are shown in that order (XMCDA label : name (id)) 

      - Ids and names are shown in that order (XMCDA label : id (name)) 


XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
    <programParameters>
        <programParameter id="color" name="Color">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="shape" name="Shape">
            <values>
                <value>
                    <label>%2</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="image_file_extension" name="Image file extension">
            <values>
                <value>
                    <label>%3</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="show_values" name="Show values">
            <values>
                <value>
                    <boolean>%4</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="directed_graph" name="Directed graph">
            <values>
                <value>
                    <boolean>%7</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="transitive_reduction" name="Transitive reduction">
            <values>
                <value>
                    <boolean>%5</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="transitive_closure" name="Transitive closure">
            <values>
                <value>
                    <boolean>%6</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="naming_conventions" name="Naming conventions">
            <values>
                <value>
                    <label>%8</label>
                </value>
            </values>
        </programParameter>
    </programParameters>


----------------------------



.. _plotAlternativesMatrix-PyXMCDA_outputs:

Outputs
-------


- :ref:`glob:alternativesMatrix.{bmp,dia,fig,gif,hpgl,ico,jpg,jpe,pdf,png,ps,ps2,svg,svgz,tif} <plotAlternativesMatrix-PyXMCDA-alternativesMatrixPlot>`
- :ref:`alternativesMatrix.dot <plotAlternativesMatrix-PyXMCDA-alternativesMatrixPlotScript>`
- :ref:`messages <plotAlternativesMatrix-PyXMCDA-messages>`

.. _plotAlternativesMatrix-PyXMCDA-alternativesMatrixPlot:

alternatives matrix plot
~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

Image containing alternatives matrix graph. Format corresponds to the one given in parameters (default is .png).



XMCDA related:
..............

- **Tag:** None

----------------------------


.. _plotAlternativesMatrix-PyXMCDA-alternativesMatrixPlotScript:

alternatives matrix plot script
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

Generated graphviz dot script that made the image. Given to enable users to later customize the appearance of the graph.



XMCDA related:
..............

- **Tag:** None

----------------------------


.. _plotAlternativesMatrix-PyXMCDA-messages:

messages
~~~~~~~~


Description:
............

Status messages.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
