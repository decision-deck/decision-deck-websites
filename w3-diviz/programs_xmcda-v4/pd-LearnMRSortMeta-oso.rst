:orphan:



.. _LearnMRSortMeta-oso:

LearnMRSortMeta
===============

:Provider: oso
:Version: 1.0

Description
-----------

The purpose of this webservice is to infer parameters of an ELECTRE TRI Bouyssou-Marchant model, also called MR-Sort model. It takes in input the categories and criteria of the model and examples of assignments. The user should also specify several parameters of the metaheuristic: the size of the population of models (nmodels), the number of iterations to do with the heuristic adjusting the profiles, and the number of iterations (niter_heur) of the main loop (niter_meta). The metaheuristic will try to find a MRSort model which is compatible with the highest number of examples given in input.

- **Contact:** Olivier Sobrie (olivier.sobrie@gmail.com)

- **Reference:** Olivier Sobrie, Vincent Mousseau, Marc Pirlot: Learning a Majority Rule Model from Large Sets of Assignment Examples



Inputs
------
(For outputs, see :ref:`below <LearnMRSortMeta-oso_outputs>`)


- :ref:`criteria <LearnMRSortMeta-oso-criteria>`
- :ref:`alternatives <LearnMRSortMeta-oso-alternatives>`
- :ref:`categories <LearnMRSortMeta-oso-categories>`
- :ref:`perfs_table <LearnMRSortMeta-oso-perfs_table>`
- :ref:`assign <LearnMRSortMeta-oso-assign>`
- :ref:`params <LearnMRSortMeta-oso-params>`
- :ref:`solver <LearnMRSortMeta-oso-solver>` *(optional)*

.. _LearnMRSortMeta-oso-criteria:

criteria
~~~~~~~~


Description:
............

The list of criteria of the model.



XMCDA related:
..............

- **Tag:** criteria

----------------------------


.. _LearnMRSortMeta-oso-alternatives:

alternatives
~~~~~~~~~~~~


Description:
............

The list of learning alternatives to use to learn the MR-Sort model.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _LearnMRSortMeta-oso-categories:

categories
~~~~~~~~~~


Description:
............

The list of categories of the model and their rank



XMCDA related:
..............

- **Tag:** categories

----------------------------


.. _LearnMRSortMeta-oso-perfs_table:

performanceTable
~~~~~~~~~~~~~~~~


Description:
............

The performance table of the learning alternatives.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _LearnMRSortMeta-oso-assign:

alternativesAssignments
~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The assignments of the alternatives in the different categories.



XMCDA related:
..............

- **Tag:** alternativesAffectations

----------------------------


.. _LearnMRSortMeta-oso-params:

params
~~~~~~


Description:
............

The parameters of the metaheuristic. 'nmodels': the size of the population; 'niter_heur': the number of iterations of the heuristic; 'niter_meta': the number of iterations of the main loop.



XMCDA related:
..............

- **Tag:** methodParameters

----------------------------


.. _LearnMRSortMeta-oso-solver:

solver
~~~~~~

**Optional:** yes, enabled by default

Description:
............

The solver to use. Currently CPLEX and GLPK are supported. In none specified, GLPK is used.



XMCDA related:
..............

- **Tag:** methodParameters

----------------------------



.. _LearnMRSortMeta-oso_outputs:

Outputs
-------


- :ref:`compatible_alts <LearnMRSortMeta-oso-compatible_alts_out>`
- :ref:`profiles_perfs <LearnMRSortMeta-oso-profiles_perfs_out>`
- :ref:`crit_weights <LearnMRSortMeta-oso-crit_weights_out>`
- :ref:`cat_profiles <LearnMRSortMeta-oso-cat_profiles_out>`
- :ref:`lambda <LearnMRSortMeta-oso-lambda_out>`
- :ref:`messages <LearnMRSortMeta-oso-message>`

.. _LearnMRSortMeta-oso-compatible_alts_out:

compatibleAlternatives
~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The reference alternatives that are compatible with the profiles, weights and majority threshold computed by the webservice.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _LearnMRSortMeta-oso-profiles_perfs_out:

profilesPerformances
~~~~~~~~~~~~~~~~~~~~


Description:
............

The profiles performance table of the profiles computed by the webservice.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _LearnMRSortMeta-oso-crit_weights_out:

criteriaWeights
~~~~~~~~~~~~~~~


Description:
............

The set of criteria weights found by the webservice.



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _LearnMRSortMeta-oso-cat_profiles_out:

categoriesProfiles
~~~~~~~~~~~~~~~~~~


Description:
............

The category profiles computed by the webservice.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _LearnMRSortMeta-oso-lambda_out:

majorityThreshold
~~~~~~~~~~~~~~~~~


Description:
............

The majority threshold computed by the webservice.



XMCDA related:
..............

- **Tag:** methodParameters

----------------------------


.. _LearnMRSortMeta-oso-message:

messages
~~~~~~~~


Description:
............

A list of messages generated by the webservice. In this output the result of the inference will be given. It gives informations on what might be wrong in the inputs.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
