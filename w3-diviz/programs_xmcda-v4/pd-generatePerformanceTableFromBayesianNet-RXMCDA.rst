:orphan:



.. _generatePerformanceTableFromBayesianNet-RXMCDA:

generatePerformanceTableFromBayesianNet
=======================================

:Provider: RXMCDA
:Version: 1.0

Description
-----------

Generates a random performance table, given a weighted bayesian net representing the interactions (and their correlation indexes) between the criteria, together with desired mean and standard deviation values for each criterion. The number of alternatives to generate can be specified, as well as the prefix to be used for the ids of these alternatives. An optional seed can also be given.

- **Contact:** Patrick Meyer (patrick.meyer@telecom-bretagne.eu) and Marc Pirlot (marc.pirlot@umons.be).



Inputs
------
(For outputs, see :ref:`below <generatePerformanceTableFromBayesianNet-RXMCDA_outputs>`)


- :ref:`criteria <generatePerformanceTableFromBayesianNet-RXMCDA-in4>`
- :ref:`graph <generatePerformanceTableFromBayesianNet-RXMCDA-in5>`
- :ref:`means <generatePerformanceTableFromBayesianNet-RXMCDA-in6>`
- :ref:`standardDeviations <generatePerformanceTableFromBayesianNet-RXMCDA-in7>`
- :ref:`parameters <generatePerformanceTableFromBayesianNet-RXMCDA-parameters>`

.. _generatePerformanceTableFromBayesianNet-RXMCDA-in4:

criteria
~~~~~~~~


Description:
............

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
                
                    <criteria>
                        <criterion id="...">
                            <active>[...]</active>
                            [...]
                        </criterion>
                        [...]
                    </criteria>
                    
            

----------------------------


.. _generatePerformanceTableFromBayesianNet-RXMCDA-in5:

bayesian correlations graph
~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The bayesian net, weighted by the desired correlations between the criteria.



XMCDA related:
..............

- **Tag:** criteriaComparisons

- **Code:**

  ::

    
                
                    <criteriaComparisons>
                        <pairs>
                            <pair>
                                <initial>
                                    <criterionID>[...]</criterionID>
                                </initial>
                                <terminal>
                                    <criterionID>[...]</criterionID>
                                </terminal>
                                <value>
                                    <real>[...]</real>
                                </value>
                          </pair>
                          [...]
                      </pairs>
                   </criteriaComparisons>
                    
            

----------------------------


.. _generatePerformanceTableFromBayesianNet-RXMCDA-in6:

means
~~~~~


Description:
............

The desired means of the criteria of the generated performance table.



XMCDA related:
..............

- **Tag:** criteriaValues

- **Code:**

  ::

    
                
                    <criteriaValues>
                       <criterionValue>
                           <criterionID>[...]</criterionID>
                               <value>
                                   <real>[...]</real>
                                </value>
                        </criterionValue>
                        [...]
                    </criteriaValues>
                    
            

----------------------------


.. _generatePerformanceTableFromBayesianNet-RXMCDA-in7:

standardDeviations
~~~~~~~~~~~~~~~~~~


Description:
............

The desired standard deviations of the criteria of the generated performance table.



XMCDA related:
..............

- **Tag:** criteriaValues

- **Code:**

  ::

    
                
                    <criteriaValues>
                       <criterionValue>
                           <criterionID>[...]</criterionID>
                               <value>
                                   <real>[...]</real>
                                </value>
                        </criterionValue>
                        [...]
                    </criteriaValues>
                    
            

----------------------------


.. _generatePerformanceTableFromBayesianNet-RXMCDA-parameters:

seed
~~~~


Description:
............

The seed for the random numbers generator (integer).



GUI information:
................


- **Name:** seed

  *The seed for the random numbers generator (integer).*

  - **Constraint description:** An integer value.

  - **Type:** integer
  - **Default value:** 1
- **Name:** number of alternatives to generate

  *The number of alternatives to generate in the output performance table.*

  - **Constraint description:** An integer value.

  - **Type:** integer
  - **Default value:** 2
- **Name:** prefix for the alternatives ids

  *The prefix for the ids of the generated alternatives.*

  - **Type:** string
  - **Default value:** "x"

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                
                    <methodParameters>
                        <parameter
                             name="seed"> <!-- REQUIRED  -->
                            <value>
                                <integer>%1</integer>
                            </value>
                        </parameter>
                         <parameter
                             name="numAlt"> <!-- REQUIRED  -->
                            <value>
                                <integer>%2</integer>
                            </value>
                        </parameter>
                        <parameter
                             name="prefix"> <!-- REQUIRED  -->
                            <value>
                                <label>%3</label>
                            </value>
                        </parameter>
                   </methodParameters>
                    
            

----------------------------



.. _generatePerformanceTableFromBayesianNet-RXMCDA_outputs:

Outputs
-------


- :ref:`alternatives <generatePerformanceTableFromBayesianNet-RXMCDA-out3>`
- :ref:`performanceTable <generatePerformanceTableFromBayesianNet-RXMCDA-out2>`
- :ref:`messages <generatePerformanceTableFromBayesianNet-RXMCDA-out1>`

.. _generatePerformanceTableFromBayesianNet-RXMCDA-out3:

alternatives
~~~~~~~~~~~~


Description:
............

The ids of the generated alternatives.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
                
                    <alternatives>
                        <alternative id="..."/>
                        [...]
                    </alternatives>
                    
            

----------------------------


.. _generatePerformanceTableFromBayesianNet-RXMCDA-out2:

performanceTable
~~~~~~~~~~~~~~~~


Description:
............

The generated performance table.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _generatePerformanceTableFromBayesianNet-RXMCDA-out1:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
