:orphan:



.. _HierarchicalDEA-CCR-SMAA_preferenceRelations-PUT:

HierarchicalDEA-CCR-SMAA_preferenceRelations
============================================

:Provider: PUT
:Version: 1.0

Description
-----------

Determines dominance relations for the given DMUs (alternatives) using SMAA-D method and CCR Data Envelopment Analysis Model with hierarchical structure of outputs. For given number of samples  returns a matrix with alternatives in each row and column. Single cell indicates how many samples of alternative in a row dominates alternative in a column.

- **Contact:** 
            Anna Labijak <anna.labijak@cs.put.poznan.pl>
        



Inputs
------
(For outputs, see :ref:`below <HierarchicalDEA-CCR-SMAA_preferenceRelations-PUT_outputs>`)


- :ref:`units <HierarchicalDEA-CCR-SMAA_preferenceRelations-PUT-units>`
- :ref:`performanceTable <HierarchicalDEA-CCR-SMAA_preferenceRelations-PUT-performanceTable>`
- :ref:`hierarchy <HierarchicalDEA-CCR-SMAA_preferenceRelations-PUT-hierarchy>`
- :ref:`weightsLinearConstraints <HierarchicalDEA-CCR-SMAA_preferenceRelations-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <HierarchicalDEA-CCR-SMAA_preferenceRelations-PUT-methodParameters>`

.. _HierarchicalDEA-CCR-SMAA_preferenceRelations-PUT-units:

units
~~~~~


Description:
............

A list of alternatives (DMUs).



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
                <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
            

----------------------------


.. _HierarchicalDEA-CCR-SMAA_preferenceRelations-PUT-performanceTable:

performance
~~~~~~~~~~~


Description:
............

A list of alternatives (DMUs) performances (on leaf hierarchy criteria).



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    
                <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<values><value>[...]</value></values>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>
            

----------------------------


.. _HierarchicalDEA-CCR-SMAA_preferenceRelations-PUT-hierarchy:

hierarchy
~~~~~~~~~


Description:
............

The hierarchical structure of criteria.



XMCDA related:
..............

- **Tag:** criteriaHierarchy

- **Code:**

  ::

    
                <criteriaHierarchy>
						<nodes>
                            <node>
                                <criterionID>[...]</criterionID>
                                <nodes>
                                    <node>
                                        <criterionID>[...]</criterionID>
                                        [...]
                                    </node>
                                    [...]
                                </nodes>
                            </node>
                        <nodes>
					</criteriaHierarchy>
            

----------------------------


.. _HierarchicalDEA-CCR-SMAA_preferenceRelations-PUT-weightsLinearConstraints:

weights constraints
~~~~~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

A list of hierarchy criteria linear constraints.



XMCDA related:
..............

- **Tag:** criteriaLinearConstraints

- **Code:**

  ::

    
                
            <criteriaLinearConstraints>
                <constraints>
                    <constraint>
                        <elements>
                            <element>
                                <criterionID> [...] </criterionID>
                                <coefficient>
                                    [...]
                                </coefficient>
                            </element>
                            [...]
                        </elements>
                        <operator>[...]</operator>
                        <rhs>
                            [...]
                        </rhs>
                    </constraint>
                    [...]
                </constraints>
            </criteriaLinearConstraints>
            

----------------------------


.. _HierarchicalDEA-CCR-SMAA_preferenceRelations-PUT-methodParameters:

parameters
~~~~~~~~~~


Description:
............

"number of samples" represents the number of samples to generate; "hierarchy node" is the ID of the hierarchy criterion for which the analysis should be performed.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** number of samples

  

  - **Constraint description:** The value should be a positive integer.

  - **Type:** integer
  - **Default value:** 100
- **Name:** hierarchy node

  

  - **Type:** string
  - **Default value:** "root"
- **Name:** random seed (-1 for default time-based seed)

  

  - **Constraint description:** The value should be a non-negative integer or -1 if no constant seed required.

  - **Type:** integer
  - **Default value:** -1

XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
                
    <programParameters>
        <parameter id="samplesNb">
            <values>
                <value><integer>%1</integer></value>
            </values>
        </parameter>
        <parameter id="hierarchyNode">
            <values>
                <value><label>%3</label></value>
            </values>
        </parameter>
        <parameter id="randomSeed">
            <values>
                <value><integer>%4</integer></value>
            </values>
        </parameter>
    </programParameters>
            

----------------------------



.. _HierarchicalDEA-CCR-SMAA_preferenceRelations-PUT_outputs:

Outputs
-------


- :ref:`pairwiseOutrankingIndices <HierarchicalDEA-CCR-SMAA_preferenceRelations-PUT-pairwiseOutrankingIndices>`
- :ref:`messages <HierarchicalDEA-CCR-SMAA_preferenceRelations-PUT-messages>`

.. _HierarchicalDEA-CCR-SMAA_preferenceRelations-PUT-pairwiseOutrankingIndices:

pairwise outranking indices
~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

A performance table for given alternatives. Single performance consists of attribute criterionID representing dominated alternative, and a value representing ratio of samples dominating this alternative.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    
                <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID> geq [...]</criterionID>
									<values>
                                        <value>[...]</value>
                                    </values>
							</performance>
							[...]
						</alternativePerformances>
                        [...]
					</performanceTable>
            

----------------------------


.. _HierarchicalDEA-CCR-SMAA_preferenceRelations-PUT-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
