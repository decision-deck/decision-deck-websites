:orphan:



.. _ElectreTri-CClassAssignments-PUT:

ElectreTri-CClassAssignments
============================

:Provider: PUT
:Version: 0.2.0

Description
-----------

Computes class assignments according to the Electre TRI-C method.

This method uses central reference actions (profiles) instead of boundary actions known from Electre TRI.

- **Web page:** http://github.com/xor-xor/electre_diviz



Inputs
------
(For outputs, see :ref:`below <ElectreTri-CClassAssignments-PUT_outputs>`)


- :ref:`alternatives <ElectreTri-CClassAssignments-PUT-input1>`
- :ref:`classes <ElectreTri-CClassAssignments-PUT-input2>`
- :ref:`credibility <ElectreTri-CClassAssignments-PUT-input4>`
- :ref:`outranking <ElectreTri-CClassAssignments-PUT-input5>`
- :ref:`classes_profiles <ElectreTri-CClassAssignments-PUT-input3>`

.. _ElectreTri-CClassAssignments-PUT-input1:

alternatives
~~~~~~~~~~~~


Description:
............

Alternatives to consider.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _ElectreTri-CClassAssignments-PUT-input2:

classes
~~~~~~~


Description:
............

Definitions of the classes (categories) to be considered for the assignments. Each class must have its rank provided as an integer, where the lowest number defines the most preferred class.



XMCDA related:
..............

- **Tag:** categories

----------------------------


.. _ElectreTri-CClassAssignments-PUT-input4:

credibility
~~~~~~~~~~~


Description:
............

Credibility matrix (alternatives vs central profiles) which should be used in exploitation procedure.



XMCDA related:
..............

- **Tag:** alternativesMatrix

----------------------------


.. _ElectreTri-CClassAssignments-PUT-input5:

outranking
~~~~~~~~~~


Description:
............

Outranking relations for pairs of alternatives and central profiles, which should be used in exploitation procedure. This input should be provided in a 'crisp' form, i.e. it should contain only pairs where outranking occurs, but without assigning any explicit value (e.g. '1.0') to them. For example:

	<alternativesMatrix mcdaConcept="alternativesProfilesComparisons">
		<row>
			<alternativeID>a01</alternativeID>
			<column>
				<alternativeID>a05</alternativeID>
				<values><value><NA/></value></values>
			</column>
			<column>
				<alternativeID>a06</alternativeID>
				<values><value><NA/></value></values>
			</column>
			[...]
		</row>
		[...]
	</alternativesMatrix>



XMCDA related:
..............

- **Tag:** alternativesMatrix

----------------------------


.. _ElectreTri-CClassAssignments-PUT-input3:

classes_profiles
~~~~~~~~~~~~~~~~


Description:
............

Definitions of central profiles which should be used for classes (categories) representation.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------



.. _ElectreTri-CClassAssignments-PUT_outputs:

Outputs
-------


- :ref:`assignments <ElectreTri-CClassAssignments-PUT-output1>`
- :ref:`messages <ElectreTri-CClassAssignments-PUT-output2>`

.. _ElectreTri-CClassAssignments-PUT-output1:

assignments
~~~~~~~~~~~


Description:
............

Assignments of the alternatives computed from the given data. The assignments are provided as intervals, i.e. combining the results of 'descending' and 'ascending' assignment procedures.



XMCDA related:
..............

- **Tag:** alternativesAssignments

----------------------------


.. _ElectreTri-CClassAssignments-PUT-output2:

messages
~~~~~~~~


Description:
............

Messages or errors generated by this module.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
