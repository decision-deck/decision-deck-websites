:orphan:



.. _SRMP_ranking-R-MCDA:

SRMP_ranking
============

:Provider: R-MCDA
:Version: 1.0

Description
-----------

SRMP is a ranking method that uses dominating reference profiles, in a given lexicographic ordering, in order to output a total preorder of a set of alternatives.

- **Contact:** Alexandru Olteanu (alexandru.olteanu@univ-ubs.fr)

- **Reference:** A. Rolland. Procédures d’agrégation ordinale de préférences avec points de référence pour l’aide a la décision. PhD thesis, Université Paris VI, 2008.



Inputs
------
(For outputs, see :ref:`below <SRMP_ranking-R-MCDA_outputs>`)


- :ref:`alternatives <SRMP_ranking-R-MCDA-inalt>`
- :ref:`referenceProfiles <SRMP_ranking-R-MCDA-inprof>`
- :ref:`performanceTable <SRMP_ranking-R-MCDA-inperf>`
- :ref:`referenceProfilesPerformanceTable <SRMP_ranking-R-MCDA-inprofpt>`
- :ref:`criteria <SRMP_ranking-R-MCDA-incrit>`
- :ref:`criteriaWeights <SRMP_ranking-R-MCDA-weights>`
- :ref:`lexicographicOrder <SRMP_ranking-R-MCDA-inprofval>`

.. _SRMP_ranking-R-MCDA-inalt:

alternatives
~~~~~~~~~~~~


Description:
............

A complete list of alternatives to be considered by the method.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
			

----------------------------


.. _SRMP_ranking-R-MCDA-inprof:

reference profiles
~~~~~~~~~~~~~~~~~~


Description:
............

A list of the reference profiles.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _SRMP_ranking-R-MCDA-inperf:

performance table
~~~~~~~~~~~~~~~~~


Description:
............

The evaluations of the alternatives on the set of criteria.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _SRMP_ranking-R-MCDA-inprofpt:

reference profiles performance table
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The evaluations of the category profiles.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _SRMP_ranking-R-MCDA-incrit:

criteria scales
~~~~~~~~~~~~~~~


Description:
............

A list of criteria on which the alternatives are evaluated. For each criterion, the preference direction should be provided.



XMCDA related:
..............

- **Tag:** criteriaScales

----------------------------


.. _SRMP_ranking-R-MCDA-weights:

criteria weights
~~~~~~~~~~~~~~~~


Description:
............

The criteria weights.



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _SRMP_ranking-R-MCDA-inprofval:

lexicographic order
~~~~~~~~~~~~~~~~~~~


Description:
............

A list of the order of the reference profiles, 1 being for the first profile that will be used when comparing two alternatives together.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------



.. _SRMP_ranking-R-MCDA_outputs:

Outputs
-------


- :ref:`alternativesValues <SRMP_ranking-R-MCDA-outvalues>`
- :ref:`messages <SRMP_ranking-R-MCDA-msg>`

.. _SRMP_ranking-R-MCDA-outvalues:

alternatives ranks
~~~~~~~~~~~~~~~~~~


Description:
............

The order of the alternatives given in ranks, or values, 1 being the top alternative.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _SRMP_ranking-R-MCDA-msg:

messages
~~~~~~~~


Description:
............

Messages from the execution of the webservice. Possible errors in the input data will be given here.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
