:orphan:



.. _RORUTADIS-PreferentialReducts-PUT:

RORUTADIS-PreferentialReducts
=============================

:Provider: PUT
:Version: 0.1

Description
-----------

Robust Ordinal Regression for value-based sorting: RORUTADIS-PreferentialReducts service allows to obtain explanation of assignments. It finds all preferential reducts and calculates preferential core.

- **Contact:** 
			Krzysztof Ciomek (k.ciomek@gmail.com),
			Milosz Kadzinski (milosz.kadzinski@cs.put.poznan.pl)
		

- **Web page:** https://github.com/kciomek/rorutadis

- **Reference:** None



Inputs
------
(For outputs, see :ref:`below <RORUTADIS-PreferentialReducts-PUT_outputs>`)


- :ref:`criteria <RORUTADIS-PreferentialReducts-PUT-criteria>`
- :ref:`alternatives <RORUTADIS-PreferentialReducts-PUT-alternatives>`
- :ref:`categories <RORUTADIS-PreferentialReducts-PUT-categories>`
- :ref:`performanceTable <RORUTADIS-PreferentialReducts-PUT-performanceTable>`
- :ref:`possibleAssignments <RORUTADIS-PreferentialReducts-PUT-possibleAssignments>`
- :ref:`assignmentExamples <RORUTADIS-PreferentialReducts-PUT-assignmentExamples>` *(optional)*
- :ref:`assignmentComparisons <RORUTADIS-PreferentialReducts-PUT-assignmentComparisons>` *(optional)*
- :ref:`categoriesCardinalities <RORUTADIS-PreferentialReducts-PUT-categoriesCardinalities>` *(optional)*
- :ref:`methodParameters <RORUTADIS-PreferentialReducts-PUT-methodParameters>`

.. _RORUTADIS-PreferentialReducts-PUT-criteria:

criteria
~~~~~~~~


Description:
............

A list of criteria (<criteria> tag) with information about preference direction (<criteriaValues mcdaConcept="preferenceDirection">, 0 - gain, 1 - cost) and number of characteristic points (<criteriaValues mcdaConcept="numberOfCharacteristicPoints">, 0 for the most general marginal utility function or integer grater or equal to 2) of each criterion.



XMCDA related:
..............

- **Tag:** criteria,criteriaValues

- **Code:**

  ::

    
<criteria>
	<criterion id="[...]" />
	[...]
</criteria>

<criteriaValues mcdaConcept="preferenceDirection">
	<criterionValue>
		<criterionID>[...]</criterionID>
		<value><integer>[...]</integer></value>
	</criterionValue>
	[...]
</criteriaValues>

<criteriaValues mcdaConcept="numberOfCharacteristicPoints">
	<criterionValue>
		<criterionID>[...]</criterionID>
		<value><integer>[0|integer greater or equal to 2]</integer></value>
	</criterionValue>
	[...]
</criteriaValues>


----------------------------


.. _RORUTADIS-PreferentialReducts-PUT-alternatives:

alternatives
~~~~~~~~~~~~


Description:
............

A list of alternatives.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
<alternatives>
	<alternative id="...">
		<active>[...]</active>
	</alternative>
</alternatives>


----------------------------


.. _RORUTADIS-PreferentialReducts-PUT-categories:

categories
~~~~~~~~~~


Description:
............

A list of categories (classes). List must be sorted from the worst category to the best.



XMCDA related:
..............

- **Tag:** categories

- **Code:**

  ::

    
<categories>
	<category id="[...]" />
	[...]
</categories>


----------------------------


.. _RORUTADIS-PreferentialReducts-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~


Description:
............

The performances of the alternatives.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _RORUTADIS-PreferentialReducts-PUT-possibleAssignments:

possibleAssignments
~~~~~~~~~~~~~~~~~~~


Description:
............

Possible assignments (calculated with RORUTADIS-PossibleAndNecessaryAssignments service).



XMCDA related:
..............

- **Tag:** alternativesAssignments

----------------------------


.. _RORUTADIS-PreferentialReducts-PUT-assignmentExamples:

assignmentExamples
~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A list of assignment examples of alternatives to intervals of categories (classes) or to a specific category (class).



XMCDA related:
..............

- **Tag:** alternativesAssignments

- **Code:**

  ::

    
<alternativesAssignments>
	<alternativeAssignment>
		<alternativeID>[...]</alternativeID>
		<categoryID>[...]</categoryID>
	</alternativeAssignment>
	[...]
	<alternativeAssignment>
		<alternativeID>[...]</alternativeID>
		<categoriesInterval>
			<lowerBound>
				<categoryID>[...]</categoryID>
			</lowerBound>
			<upperBound>
				<categoryID>[...]</categoryID>
			</upperBound>
		</categoriesInterval>
	</alternativeAssignment>
	[...]
	<alternativeAssignment>
		<alternativeID>[...]</alternativeID>
		<categoriesSet>
			<categoryID>[...]</categoryID>
			[...]
		</categoriesSet>
	</alternativeAssignment>
	[...]
</alternativesAssignments>


----------------------------


.. _RORUTADIS-PreferentialReducts-PUT-assignmentComparisons:

assignmentComparisons
~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Two lists of assignment pairwise comparisons. A comparison from list with attribute mcdaConcept="atLeastAsGoodAs" indicates that some alternative should be assigned to class at least as good as class of some other alternative (k = 0) or at least better by k classes (k > 0). A comparison from list with attribute mcdaConcept="atMostAsGoodAs" indicates that some alternative should be assigned to class at most better by k classes (k > 0) then some other alternative.



XMCDA related:
..............

- **Tag:** alternativesMatrix

- **Code:**

  ::

    
<alternativesMatrix mcdaConcept="atLeastAsGoodAs">
	<row>
		<alternativeID>[...]</alternativeID>
		<column>
			<alternativeID>[...]</alternativeID>
			<values>
				<value>
					<integer>k</integer>
				</value>
			</values>
		</column>
	</row>
	[...]
</alternativesMatrix>

<alternativesMatrix mcdaConcept="atMostAsGoodAs">
	<row>[...]</row>
	[...]
</alternativesMatrix>


----------------------------


.. _RORUTADIS-PreferentialReducts-PUT-categoriesCardinalities:

categoriesCardinalities
~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

A list of category (class) cardinality constraints. It allows to define minimal and/or maximal desired category (class) cardinalities.



XMCDA related:
..............

- **Tag:** categoriesValues

- **Code:**

  ::

    
<categoriesValues>
	<categoryValue>
		<categoryID>[...]</categoryID>
		<values>
			<value>
				<interval>
					<lowerBound><integer>[...]</integer></lowerBound>
					<upperBound><integer>[...]</integer></upperBound>
				</interval>
			</value>
		</values>
	</categoryValue>
	[...]
</categoriesValues>


----------------------------


.. _RORUTADIS-PreferentialReducts-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~


Description:
............

Whether marginal value functions strictly monotonic (true) or weakly monotonic (false).



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** strictlyMonotonicValueFunctions

  *Whether marginal value functions strictly monotonic or not.*

  - **Default value:** false
- **Name:** alternative

  *An identifier of alternative for assignment to explain.*

  - **Type:** string

XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
<programParameters>
	<parameter name="strictlyMonotonicValueFunctions">
		<value>
			<boolean>%1</boolean>
		</value>
	</parameter>
	<parameter name="alternative">
		<value>
			<label>%2</label>
		</value>
	</parameter>
</programParameters>


----------------------------



.. _RORUTADIS-PreferentialReducts-PUT_outputs:

Outputs
-------


- :ref:`preferentialReducts <RORUTADIS-PreferentialReducts-PUT-preferentialReducts>`
- :ref:`preferentialCore <RORUTADIS-PreferentialReducts-PUT-preferentialCore>`
- :ref:`messages <RORUTADIS-PreferentialReducts-PUT-messages>`

.. _RORUTADIS-PreferentialReducts-PUT-preferentialReducts:

preferentialReducts
~~~~~~~~~~~~~~~~~~~


Description:
............

All preferential reducts. Each reduct consists of three types of information (alternative assignments, pairwise alternative comparisons and desired class cardinalities) and it is identified by a string stored in attribute 'id' of root level tags.



XMCDA related:
..............

- **Tag:** alternativesAssignments,alternativesMatrix,categoriesValues

----------------------------


.. _RORUTADIS-PreferentialReducts-PUT-preferentialCore:

preferentialCore
~~~~~~~~~~~~~~~~


Description:
............

preferential core.



XMCDA related:
..............

- **Tag:** alternativesAffectations,alternativesMatrix,categoriesValues

----------------------------


.. _RORUTADIS-PreferentialReducts-PUT-messages:

messages
~~~~~~~~


Description:
............

Messages generated by the program.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
