
:orphan:

.. _plotNumericPerformanceTable-PyXMCDA:

plotNumericPerformanceTable
===========================

:Provider: PyXMCDA
:Version: 1.4

Description
-----------

Generate plots from provided performance tables as well as the scripts generating these plots.

Colormap can be defined by the user, by giving a list of colors in the parameters.xml file.
The number of colors is not restrained, and the colormap will linearly distribute the color in their provided order and interpolate between them.
If only one is provided, it will be used for all data plot.
Each color is either one of the color names predefined in matplotlib (See https://matplotlib.org/stable/gallery/color/named_colors.html#sphx-glr-gallery-color-named-colors-py) or a RGB color defined in hexadecimal '#RRGGBB'.

N.B.: This service can handle missing data by not representing them except for starCharts which centre values represent missing data.
Also, when plotting pie charts, negative and null values are not represented!

- **Contact:** Nicolas Duminy (nicolas.duminy@telecom-bretagne.eu)

- **Web page:** https://gitlab.com/nduminy/ws-pyxmcda



Inputs
------
(For outputs, see :ref:`below <plotNumericPerformanceTable-PyXMCDA_outputs>`)


- :ref:`alternatives <plotNumericPerformanceTable-PyXMCDA-alternatives>` *(optional)*
- :ref:`criteria <plotNumericPerformanceTable-PyXMCDA-criteria>` *(optional)*
- :ref:`performanceTable <plotNumericPerformanceTable-PyXMCDA-performanceTable>`
- :ref:`parameters <plotNumericPerformanceTable-PyXMCDA-parameters>` *(optional)*

.. _plotNumericPerformanceTable-PyXMCDA-alternatives:

alternatives
~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The alternatives to be plotted. All are plotted if not provided.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _plotNumericPerformanceTable-PyXMCDA-criteria:

criteria
~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The criteria to be plotted. All are plotted if not provided.



XMCDA related:
..............

- **Tag:** criteria

----------------------------


.. _plotNumericPerformanceTable-PyXMCDA-performanceTable:

performance table
~~~~~~~~~~~~~~~~~


Description:
............

The performance table.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _plotNumericPerformanceTable-PyXMCDA-parameters:

parameters
~~~~~~~~~~


Description:
............

Parameters of the method



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** X-axis label

  *X-axis name to display below each plot.*

  - **Type:** string
  - **Default value:** ""
- **Name:** Y-axis label

  *Y-axis name to display on the left of each plot.*

  - **Type:** string
  - **Default value:** ""
- **Name:** Chart type

  *Type of chart to be plotted.*

  - **Type:** drop-down list
  - **Possible values:**
      - bar chart (XMCDA label : barChart) (default)

      - pie chart (XMCDA label : pieChart) 

      - star chart (XMCDA label : starChart) 

- **Name:** Colormap

  *Type of the colormap.*

  - **Type:** drop-down list
  - **Possible values:**
      - Monochrome (XMCDA label : monochrome) (default)

      - Bicolor (XMCDA label : bicolor) 

      - Multi-color (advanced) (XMCDA label : multicolor (advanced)) 

- **Name:** Nb of colors

  *The number of colors supplied to define the colormap.*

  - **Constraint description:** The value should be between 1 and 10.

  - **Type:** integer
  - **Default value:** 2
- **Name:** Color

  *Color to use.*

  - **Type:** drop-down list
  - **Possible values:**
      - black (XMCDA label : 
                <value>
                    <label>black</label>
                </value>) (default)

      - red (XMCDA label : 
                <value>
                    <label>red</label>
                </value>) 

      - green (XMCDA label : 
                <value>
                    <label>green</label>
                </value>) 

      - blue (XMCDA label : 
                <value>
                    <label>blue</label>
                </value>) 

      - orange (XMCDA label : 
                <value>
                    <label>orange</label>
                </value>) 

      - green (XMCDA label : 
                <value>
                    <label>green</label>
                </value>) 

      - purple (XMCDA label : 
                <value>
                    <label>purple</label>
                </value>) 

      - cyan (XMCDA label : 
                <value>
                    <label>cyan</label>
                </value>) 

      - magenta (XMCDA label : 
                <value>
                    <label>magenta</label>
                </value>) 

      - yellow (XMCDA label : 
                <value>
                    <label>yellow</label>
                </value>) 

      - salmon (XMCDA label : 
                <value>
                    <label>salmon</label>
                </value>) 

      - orangered (XMCDA label : 
                <value>
                    <label>orangered</label>
                </value>) 

      - chocolate (XMCDA label : 
                <value>
                    <label>chocolate</label>
                </value>) 

      - greenyellow (XMCDA label : 
                <value>
                    <label>greenyellow</label>
                </value>) 

      - aquamarine (XMCDA label : 
                <value>
                    <label>aquamarine</label>
                </value>) 

      - pink (XMCDA label : 
                <value>
                    <label>pink</label>
                </value>) 

      - grey (XMCDA label : 
                <value>
                    <label>grey</label>
                </value>) 

      - whitesmoke (XMCDA label : 
                <value>
                    <label>whitesmoke</label>
                </value>) 

- **Name:** 1st color

  *First color to use for the colormap.*

  - **Type:** drop-down list
  - **Possible values:**
      - black (XMCDA label : 
                <value>
                    <label>black</label>
                </value>) (default)

      - red (XMCDA label : 
                <value>
                    <label>red</label>
                </value>) 

      - green (XMCDA label : 
                <value>
                    <label>green</label>
                </value>) 

      - blue (XMCDA label : 
                <value>
                    <label>blue</label>
                </value>) 

      - orange (XMCDA label : 
                <value>
                    <label>orange</label>
                </value>) 

      - green (XMCDA label : 
                <value>
                    <label>green</label>
                </value>) 

      - purple (XMCDA label : 
                <value>
                    <label>purple</label>
                </value>) 

      - cyan (XMCDA label : 
                <value>
                    <label>cyan</label>
                </value>) 

      - magenta (XMCDA label : 
                <value>
                    <label>magenta</label>
                </value>) 

      - yellow (XMCDA label : 
                <value>
                    <label>yellow</label>
                </value>) 

      - salmon (XMCDA label : 
                <value>
                    <label>salmon</label>
                </value>) 

      - orange-red (XMCDA label : 
                <value>
                    <label>orangered</label>
                </value>) 

      - chocolate (XMCDA label : 
                <value>
                    <label>chocolate</label>
                </value>) 

      - green-yellow (XMCDA label : 
                <value>
                    <label>greenyellow</label>
                </value>) 

      - aquamarine (XMCDA label : 
                <value>
                    <label>aquamarine</label>
                </value>) 

      - pink (XMCDA label : 
                <value>
                    <label>pink</label>
                </value>) 

      - grey (XMCDA label : 
                <value>
                    <label>grey</label>
                </value>) 

      - whitesmoke (XMCDA label : 
                <value>
                    <label>whitesmoke</label>
                </value>) 

      - white (XMCDA label : 
                <value>
                    <label>white</label>
                </value>) 

- **Name:** 2nd color

  *Second color to use for the colormap.*

  - **Type:** drop-down list
  - **Possible values:**
      - black (XMCDA label : 
                <value>
                    <label>black</label>
                </value>) (default)

      - red (XMCDA label : 
                <value>
                    <label>red</label>
                </value>) 

      - green (XMCDA label : 
                <value>
                    <label>green</label>
                </value>) 

      - blue (XMCDA label : 
                <value>
                    <label>blue</label>
                </value>) 

      - orange (XMCDA label : 
                <value>
                    <label>orange</label>
                </value>) 

      - green (XMCDA label : 
                <value>
                    <label>green</label>
                </value>) 

      - purple (XMCDA label : 
                <value>
                    <label>purple</label>
                </value>) 

      - cyan (XMCDA label : 
                <value>
                    <label>cyan</label>
                </value>) 

      - magenta (XMCDA label : 
                <value>
                    <label>magenta</label>
                </value>) 

      - yellow (XMCDA label : 
                <value>
                    <label>yellow</label>
                </value>) 

      - salmon (XMCDA label : 
                <value>
                    <label>salmon</label>
                </value>) 

      - orangered (XMCDA label : 
                <value>
                    <label>orangered</label>
                </value>) 

      - chocolate (XMCDA label : 
                <value>
                    <label>chocolate</label>
                </value>) 

      - greenyellow (XMCDA label : 
                <value>
                    <label>greenyellow</label>
                </value>) 

      - aquamarine (XMCDA label : 
                <value>
                    <label>aquamarine</label>
                </value>) 

      - pink (XMCDA label : 
                <value>
                    <label>pink</label>
                </value>) 

      - grey (XMCDA label : 
                <value>
                    <label>grey</label>
                </value>) 

      - whitesmoke (XMCDA label : 
                <value>
                    <label>whitesmoke</label>
                </value>) 

- **Name:** Color 1 (ex.: cyan or #00FFFF)

  *1st color (ex.: cyan or #00FFFF)*

  - **Type:** string
  - **Default value:** "black"
- **Name:** Color 2

  *2nd color*

  - **Type:** string
  - **Default value:** "black"
- **Name:** Color 3

  *3rd color*

  - **Type:** string
  - **Default value:** "black"
- **Name:** Color 4

  *4th color*

  - **Type:** string
  - **Default value:** "black"
- **Name:** Color 5

  *5th color*

  - **Type:** string
  - **Default value:** "black"
- **Name:** Color 6

  *6th color*

  - **Type:** string
  - **Default value:** "black"
- **Name:** Color 7

  *7th color*

  - **Type:** string
  - **Default value:** "black"
- **Name:** Color 8

  *8th color*

  - **Type:** string
  - **Default value:** "black"
- **Name:** Color 9

  *9th color*

  - **Type:** string
  - **Default value:** "black"
- **Name:** Color 10

  *10th color*

  - **Type:** string
  - **Default value:** "black"
- **Name:** Image file extension

  *File extension of generated image figure.*

  - **Type:** drop-down list
  - **Possible values:**
      - .eps (Encapsulated PostScript) (XMCDA label : eps) 

      - .jpg (Joint Photographic Experts Group) (XMCDA label : jpg) 

      - .pdf (Portable Document Format) (XMCDA label : pdf) 

      - .pgf (Progressive Graphics File) (XMCDA label : pgf) 

      - .png (Portable Network Graphics) (XMCDA label : png) (default)

      - .ps (PostScript) (XMCDA label : ps) 

      - .raw (Raw RGBA bitmap) (XMCDA label : raw) 

      - .rgba (Silicon Graphics RGB) (XMCDA label : rgba) 

      - .svg (Scalable Vector Graphics) (XMCDA label : svg) 

      - .svgz (Compressed Scalable Vector Graphics) (XMCDA label : svgz) 

      - .tif (Tagged Image File Format) (XMCDA label : tif) 

- **Name:** Image file extension

  *File extension of generated image figure.*

  - **Type:** drop-down list
  - **Possible values:**
      - .eps (Encapsulated PostScript) (XMCDA label : eps) 

      - .jpg (Joint Photographic Experts Group) (XMCDA label : jpg) 

      - .pdf (Portable Document Format) (XMCDA label : pdf) 

      - .png (Portable Network Graphics) (XMCDA label : png) (default)

      - .svg (Scalable Vector Graphics) (XMCDA label : svg) 

- **Name:** Plotter

  *Plotter used to generate image.*

  - **Type:** drop-down list
  - **Possible values:**
      - Matplotlib (XMCDA label : matplotlib) (default)

      - Gnuplot (XMCDA label : gnuplot) 

- **Name:** Order alternatives by

  *Defines how alternatives will be sorted on the plots.*

  - **Type:** drop-down list
  - **Possible values:**
      - ids (XMCDA label : id) (default)

      - names (XMCDA label : name) 

      - values (XMCDA label : value) 

- **Name:** Order criteria by

  *Defines how criteria will be sorted on the figure.*

  - **Type:** drop-down list
  - **Possible values:**
      - ids (XMCDA label : id) (default)

      - names (XMCDA label : name) 

- **Name:** Reverse order

  *Defines if order direction should be reversed (descending order) or not.*

  - **Default value:** false
- **Name:** Mix colors

  *Defines if colors should be mixed for a better contrast (more color-blind friendly) or not.*

  - **Default value:** false
- **Name:** Superposed plot title

  *Title of the plot superposing all alternatives performances.
                    This plot is not rendered if this parameter is not provided.
                    Only usable for starChart plots.*

  - **Type:** string
- **Name:** Naming conventions

  *How categories and alternatives are labelled on the graph.*

  - **Type:** drop-down list
  - **Possible values:**
      - Only ids are shown (XMCDA label : id) 

      - Only names are shown (disambiguated by appending the ids, if needed) (XMCDA label : name) (default)

      - Names and ids are shown in that order (XMCDA label : name (id)) 

      - Ids and names are shown in that order (XMCDA label : id (name)) 


XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
    <programParameters>
        <programParameter id="x_axis" name="X-axis label">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="y_axis" name="Y-axis label">
            <values>
                <value>
                    <label>%2</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="chart_type" name="Chart type">
            <values>
                <value>
                    <label>%3</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="colors" name="Colors">
            <!-- colormap: %colormap / nb colors: %nb_colors -->
            <values>%color-mono%color-A%color-B%color-a%color-b%color-c%color-d%color-e%color-f%color-g%color-h%color-i%color-j
            </values>
        </programParameter>
        <programParameter id="image_file_extension" name="Image file extension">
            <values>
                <value>
                    <label>%7a%7b</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="plotter" name="Plotter">
            <values>
                <value>
                    <label>%8</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="order_alternatives_by" name="Order alternatives by">
            <values>
                <value>
                    <label>%9</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="order_criteria_by" name="Order criteria by">
            <values>
                <value>
                    <label>%10</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="reverse_order" name="Reverse order">
            <values>
                <value>
                    <boolean>%11</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="mix_colors" name="Mix colors">
            <values>
                <value>
                    <boolean>%12</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="superposed_plot_title" name="Superposed plot title">
            <values>
                <value>
                    <label>%13</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="naming_conventions" name="Naming conventions">
            <values>
                <value>
                    <label>%14</label>
                </value>
            </values>
        </programParameter>
    </programParameters>


----------------------------



.. _plotNumericPerformanceTable-PyXMCDA_outputs:

Outputs
-------


- :ref:`glob:numericPerformanceTable.{bmp,dia,fig,gif,hpgl,ico,jpg,jpe,pdf,png,ps,ps2,svg,svgz,tif} <plotNumericPerformanceTable-PyXMCDA-numericPerformanceTablePlot>`
- :ref:`glob:plot_numericPerformanceTable.{py,plt} <plotNumericPerformanceTable-PyXMCDA-numericPerformanceTablePlotScript>`
- :ref:`messages <plotNumericPerformanceTable-PyXMCDA-messages>`

.. _plotNumericPerformanceTable-PyXMCDA-numericPerformanceTablePlot:

performance table plot
~~~~~~~~~~~~~~~~~~~~~~


Description:
............

Image containing all selected values for performance table plots. Format corresponds to the one given in parameters (default is .png).



XMCDA related:
..............

- **Tag:** None

----------------------------


.. _plotNumericPerformanceTable-PyXMCDA-numericPerformanceTablePlotScript:

performance table plot script
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

Generated Python or Gnuplot script that made the image.
                Given to enable users to later customize the appearance of the plots.
                Extension is .py if matplotlib is used, .plt for gnuplot.



XMCDA related:
..............

- **Tag:** None

----------------------------


.. _plotNumericPerformanceTable-PyXMCDA-messages:

messages
~~~~~~~~


Description:
............

Status messages.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
