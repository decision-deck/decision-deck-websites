:orphan:



.. _csvToXMCDA-criteriaFunctions-PyXMCDA:

csvToXMCDA-criteriaFunctions
============================

:Provider: PyXMCDA
:Version: 2.0

Description
-----------

Transforms a file containing criteria functions from a comma-separated values (CSV) file to two XMCDA compliant files, containing the criteria ids and their names, and their criterion functions (points).

- **Contact:** Sébastien Bigaret (sebastien.bigaret@telecom-bretagne.eu)

- **Web page:** https://gitlab.com/sbigaret/ws-pyxmcda



Inputs
------
(For outputs, see :ref:`below <csvToXMCDA-criteriaFunctions-PyXMCDA_outputs>`)


- :ref:`criteriaFunctions.csv <csvToXMCDA-criteriaFunctions-PyXMCDA-criteriaFunctions_csv>`
- :ref:`parameters <csvToXMCDA-criteriaFunctions-PyXMCDA-parameters>`

.. _csvToXMCDA-criteriaFunctions-PyXMCDA-criteriaFunctions_csv:

criteria functions (csv)
~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The discrete criteria functions as a CSV file.  The discrete functions are defined by a series of point.

Example:

  g1,cost,0,0
  ,,21334,1
  g2,Acceleration,0,0
  ,,30.8,1
  g3,PickUp,0,0
  ,,41.6,1
  g4,Brakes,0,0
  ,,2.66,1
  g5,RoadHold,0,0
  ,,3.25,1

defines for criterion `g1` a discrete function with two points, (0,0) and (21334,1).



XMCDA related:
..............

- **Tag:** other

----------------------------


.. _csvToXMCDA-criteriaFunctions-PyXMCDA-parameters:

parameters
~~~~~~~~~~


Description:
............

Parameters of the method



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** CSV delimiter

  *Indicates the delimiter to use. Leave blank for auto-detection.  It is especially handful when the auto-detection fails to determine the csv delimiter.*

  - **Constraint description:** One character maximum

  - **Type:** string
  - **Default value:** ""
- **Name:** Default content

  *.*

  - **Type:** drop-down list
  - **Possible values:**
      - float (XMCDA label : float) (default)

      - string (XMCDA label : label) 

      - integer (XMCDA label : integer) 

      - boolean (XMCDA label : boolean) 

- **Name:** First column

  *Content of the first column*

  - **Type:** drop-down list
  - **Possible values:**
      - id (XMCDA label : false) 

      - id (name) (XMCDA label : true) (default)


XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
    <programParameters>
        <parameter id="csv_delimiter">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </parameter>
        <parameter id="default_prefix">
            <values>
                <value>
                    <label>%2</label>
                </value>
            </values>
        </parameter>
        <parameter id="name_in_id">
            <values>
                <value>
                    <boolean>%3</boolean>
                </value>
            </values>
        </parameter>
    </programParameters>


----------------------------



.. _csvToXMCDA-criteriaFunctions-PyXMCDA_outputs:

Outputs
-------


- :ref:`criteria <csvToXMCDA-criteriaFunctions-PyXMCDA-criteria>`
- :ref:`criteriaFunctions <csvToXMCDA-criteriaFunctions-PyXMCDA-criteriaFunctions>`
- :ref:`messages <csvToXMCDA-criteriaFunctions-PyXMCDA-messages>`

.. _csvToXMCDA-criteriaFunctions-PyXMCDA-criteria:

criteria
~~~~~~~~


Description:
............

The criteria.



XMCDA related:
..............

- **Tag:** criteria

----------------------------


.. _csvToXMCDA-criteriaFunctions-PyXMCDA-criteriaFunctions:

value functions
~~~~~~~~~~~~~~~


Description:
............

The value functions.



XMCDA related:
..............

- **Tag:** criteriaFunctions

----------------------------


.. _csvToXMCDA-criteriaFunctions-PyXMCDA-messages:

messages
~~~~~~~~


Description:
............

Status messages.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
