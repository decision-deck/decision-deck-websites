:orphan:



.. _MRSortInferenceApprox-R-MCDA:

Approximative inference of MRSort
=================================

:Provider: R-MCDA
:Version: 1.0

Description
-----------

MRSort is a simplification of the Electre TRI method that uses the pessimistic assignment rule, without indifference or preference thresholds attached to criteria. Only a binary discordance condition is considered, i.e. a veto forbids an outranking in any possible concordance situation, or not. The identification of the profiles, weights, majority threshold and veto thresholds are done by taking into account assignment examples.

- **Contact:** Alexandru Olteanu (al.olteanu@telecom-bretagne.eu)



Inputs
------
(For outputs, see :ref:`below <MRSortInferenceApprox-R-MCDA_outputs>`)


- :ref:`alternatives <MRSortInferenceApprox-R-MCDA-inalt>`
- :ref:`criteria <MRSortInferenceApprox-R-MCDA-incrit>`
- :ref:`categoriesRanks <MRSortInferenceApprox-R-MCDA-incategval>`
- :ref:`performanceTable <MRSortInferenceApprox-R-MCDA-inperf>`
- :ref:`alternativesAssignments <MRSortInferenceApprox-R-MCDA-assignments>`
- :ref:`veto <MRSortInferenceApprox-R-MCDA-veto>` *(optional)*
- :ref:`time <MRSortInferenceApprox-R-MCDA-time>` *(optional)*
- :ref:`population <MRSortInferenceApprox-R-MCDA-population>` *(optional)*
- :ref:`mutation <MRSortInferenceApprox-R-MCDA-mutation>` *(optional)*

.. _MRSortInferenceApprox-R-MCDA-inalt:

alternatives
~~~~~~~~~~~~


Description:
............

A complete list of alternatives to be considered when inferring the MR-Sort model.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
			

----------------------------


.. _MRSortInferenceApprox-R-MCDA-incrit:

criteria
~~~~~~~~


Description:
............

A list of criteria on which the alternatives are evaluated. For each criterion, the preference direction should be provided.



XMCDA related:
..............

- **Tag:** criteriaScales

----------------------------


.. _MRSortInferenceApprox-R-MCDA-incategval:

categoriesRanks
~~~~~~~~~~~~~~~


Description:
............

A list of categories ranks, 1 stands for the most preferred category and the higher the number the lower the preference for that category.



XMCDA related:
..............

- **Tag:** categoriesValues

----------------------------


.. _MRSortInferenceApprox-R-MCDA-inperf:

performanceTable
~~~~~~~~~~~~~~~~


Description:
............

The evaluations of the alternatives on the set of criteria.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _MRSortInferenceApprox-R-MCDA-assignments:

alternativesAssignments
~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The alternatives assignments to categories.



XMCDA related:
..............

- **Tag:** alternativesAssignments

----------------------------


.. _MRSortInferenceApprox-R-MCDA-veto:

veto
~~~~


Description:
............

An indicator for whether vetoes should be included in the model or not.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** Include vetoes

  


XMCDA related:
..............

- **Tag:** programParameters

----------------------------


.. _MRSortInferenceApprox-R-MCDA-time:

time
~~~~


Description:
............

The execution time limit in seconds.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** Time limit

  

  - **Constraint description:** An integer value (minimum 1)

  - **Type:** integer

XMCDA related:
..............

- **Tag:** programParameters

----------------------------


.. _MRSortInferenceApprox-R-MCDA-population:

population
~~~~~~~~~~


Description:
............

The algorithm population size.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** Population size

  

  - **Constraint description:** An integer value (minimum 10)

  - **Type:** integer

XMCDA related:
..............

- **Tag:** programParameters

----------------------------


.. _MRSortInferenceApprox-R-MCDA-mutation:

mutation
~~~~~~~~


Description:
............

The algorithm mutation probability.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** Mutation probability

  

  - **Constraint description:** A value between 0 and 1

  - **Type:** float

XMCDA related:
..............

- **Tag:** programParameters

----------------------------



.. _MRSortInferenceApprox-R-MCDA_outputs:

Outputs
-------


- :ref:`fitness <MRSortInferenceApprox-R-MCDA-fitness>`
- :ref:`majorityThreshold <MRSortInferenceApprox-R-MCDA-majority>`
- :ref:`criteriaWeights <MRSortInferenceApprox-R-MCDA-weights>`
- :ref:`categoriesProfiles <MRSortInferenceApprox-R-MCDA-outcatprof>`
- :ref:`categoriesProfilesPerformanceTable <MRSortInferenceApprox-R-MCDA-outcatprofpt>`
- :ref:`categoriesVetoes <MRSortInferenceApprox-R-MCDA-outcatveto>`
- :ref:`categoriesVetoesPerformanceTable <MRSortInferenceApprox-R-MCDA-outcatvetopt>`
- :ref:`messages <MRSortInferenceApprox-R-MCDA-msg>`

.. _MRSortInferenceApprox-R-MCDA-fitness:

fitness
~~~~~~~


Description:
............

The model fitness.



XMCDA related:
..............

- **Tag:** programParameters

----------------------------


.. _MRSortInferenceApprox-R-MCDA-majority:

majorityThreshold
~~~~~~~~~~~~~~~~~


Description:
............

The majority threshold.



XMCDA related:
..............

- **Tag:** programParameters

----------------------------


.. _MRSortInferenceApprox-R-MCDA-weights:

criteriaWeights
~~~~~~~~~~~~~~~


Description:
............

The criteria weights.



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _MRSortInferenceApprox-R-MCDA-outcatprof:

categoriesProfiles
~~~~~~~~~~~~~~~~~~


Description:
............

The categories delimiting profiles.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _MRSortInferenceApprox-R-MCDA-outcatprofpt:

categoriesProfilesPerformanceTable
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The evaluations of the category profiles.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _MRSortInferenceApprox-R-MCDA-outcatveto:

categoriesVetoes
~~~~~~~~~~~~~~~~


Description:
............

The categories veto profiles.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _MRSortInferenceApprox-R-MCDA-outcatvetopt:

categoriesVetoesPerformanceTable
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The evaluations of the veto profiles.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _MRSortInferenceApprox-R-MCDA-msg:

messages
~~~~~~~~


Description:
............

Messages from the execution of the webservice. Possible errors in the input data will be given here.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
