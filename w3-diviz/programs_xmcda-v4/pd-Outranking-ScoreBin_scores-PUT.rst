:orphan:



.. _Outranking-ScoreBin_scores-PUT:

Outranking-ScoreBin_scores
==========================

:Provider: PUT
:Version: 1.0.0

Description
-----------

Module for calculation ScoreBin scores.

- **Contact:** Krzysztof Martyn <krzysztof.martyn@wp.pl>

- **Web page:** https://bitbucket.org/Krzysztof_Martyn/prefrank



Inputs
------
(For outputs, see :ref:`below <Outranking-ScoreBin_scores-PUT_outputs>`)


- :ref:`alternatives <Outranking-ScoreBin_scores-PUT-input1>`
- :ref:`preferences <Outranking-ScoreBin_scores-PUT-input2>`
- :ref:`parameters <Outranking-ScoreBin_scores-PUT-input3>`

.. _Outranking-ScoreBin_scores-PUT-input1:

alternatives
~~~~~~~~~~~~


Description:
............

Alternatives to consider.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _Outranking-ScoreBin_scores-PUT-input2:

preferences
~~~~~~~~~~~


Description:
............

Aggregated preferences binary matrix or pairs for which the outrank relationship occurs.



XMCDA related:
..............

- **Tag:** alternativesMatrix

----------------------------


.. _Outranking-ScoreBin_scores-PUT-input3:

parameters
~~~~~~~~~~


Description:
............

First parameter specifies if preference are given by matrix or pairs. Second parameter specifies the algorithm to calculate ranking. There are three algorithms to choose from: PageRank, HITS and Salsa.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** input type

  

  - **Type:** drop-down list
  - **Possible values:**
      - Preferences given by whole matrix 1-0 valued. (XMCDA label : matrix) (default)

      - Preferences given by pairs for whom the outranking relationship occurs (crisp). (XMCDA label : pair) 

- **Name:** algorithm type

  

  - **Type:** drop-down list
  - **Possible values:**
      - ScoreBin I (XMCDA label : scorebin_1) (default)

      - ScoreBin II (XMCDA label : scorebin_2) 

      - ScoreBin III (XMCDA label : scorebin_3) 

- **Name:** number of iteration

  *Number of iteration*

  - **Constraint description:** The value should be greater than 1

  - **Type:** integer
  - **Default value:** 1000
- **Name:** Run averaging if not converge?

  *Turning on the version with averaging the score value, if after "number of iteration" iteration the score does not converge, that is: the maximum score difference between the last two iterations is greater than 0.001.*

  - **Default value:** false
- **Name:** Earlier stop if converge?

  *Earlier stop if the maximum difference in score between two consecutive iterations is less than 0.00001*

  - **Default value:** true

XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
        
		<programParameters>
			<parameter id="input_type" name="input_type">
				<values>
					<value>
						<label>%1</label>
					</value>
				</values>
			</parameter>	
			<parameter id="algorithm_type" name="algorithm_type">
				<values>
					<value>
						<label>%2</label>
					</value>
				</values>
			</parameter>
			<parameter id="number_of_iteration" name="number_of_iteration">
				<values>
					<value>
						<integer>%3</integer>
					</value>
				</values>
			</parameter>
			<parameter id="check_convergence" name="check_convergence">
				<values>
					<value>
						<boolean>%4</boolean>
					</value>
				</values>
			</parameter>
			<parameter id="early_stopping" name="early_stopping">
				<values>
					<value>
						<boolean>%5</boolean>
					</value>
				</values>
			</parameter>
		</programParameters>
        
      

----------------------------



.. _Outranking-ScoreBin_scores-PUT_outputs:

Outputs
-------


- :ref:`positive_flows <Outranking-ScoreBin_scores-PUT-output1>`
- :ref:`negative_flows <Outranking-ScoreBin_scores-PUT-output2>`
- :ref:`total_flows <Outranking-ScoreBin_scores-PUT-output3>`
- :ref:`ranking <Outranking-ScoreBin_scores-PUT-output4>`
- :ref:`messages <Outranking-ScoreBin_scores-PUT-output5>`

.. _Outranking-ScoreBin_scores-PUT-output1:

positive flows
~~~~~~~~~~~~~~


Description:
............

Positive outranking flows.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _Outranking-ScoreBin_scores-PUT-output2:

negative flows
~~~~~~~~~~~~~~


Description:
............

Negative outranking flows.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _Outranking-ScoreBin_scores-PUT-output3:

total flows
~~~~~~~~~~~


Description:
............

Final flows computed from the given data.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _Outranking-ScoreBin_scores-PUT-output4:

ranking
~~~~~~~


Description:
............

ScoreBin scores computed from the given data.



XMCDA related:
..............

- **Tag:** alternativesMatrix

----------------------------


.. _Outranking-ScoreBin_scores-PUT-output5:

messages
~~~~~~~~


Description:
............

Messages or errors generated by this module.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
