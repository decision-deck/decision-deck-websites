:orphan:



.. _csvToXMCDA-alternativesValues-PyXMCDA:

csvToXMCDA-alternativesValues
=============================

:Provider: PyXMCDA
:Version: 2.0

Description
-----------

Transforms a file containing alternatives values from a comma-separated values (CSV) file to two XMCDA compliant files, containing the corresponding alternatives ids and their alternativesValues.

- **Contact:** Sébastien Bigaret (sebastien.bigaret@telecom-bretagne.eu)

- **Web page:** https://gitlab.com/sbigaret/ws-pyxmcda



Inputs
------
(For outputs, see :ref:`below <csvToXMCDA-alternativesValues-PyXMCDA_outputs>`)


- :ref:`alternativesValues.csv <csvToXMCDA-alternativesValues-PyXMCDA-csv>`
- :ref:`parameters <csvToXMCDA-alternativesValues-PyXMCDA-parameters>`

.. _csvToXMCDA-alternativesValues-PyXMCDA-csv:

alternativesValues (csv)
~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The alternatives and their alternatives values as a CSV file.  The first line is made of two cells, the first one being empty, and the second one will be the content of the attribute "mcdaConcept" in the tag "<alternativesValues>", if supplied.  The following lines are made of at least two cells, with the first cell being an alternative' id (and name, see below), and the remaining cells their associated values.

Example::

    ,ranks
    a1 (eggs),1,2,3,4,5
    a2 (spam),3.14159
    a3 (parrot),int:0xDEAD

The first column contains the alternatives' ids. Additionally, the alternatives' names are also extracted when the cells are formatted like `id (name)`.  Set the parameter "First column" to "id" to deactivate the extraction of alternatives' names.

By default the values are considered as float numbers.  This can be changed using the parameter "Default content".  It is possible to specify the type of a value by prepending it with a prefix:

- `float:` for floats (ex.: `1`, `1.2`, `1.2e3`)
- `integer:` for integers (decimal representation: `127`, hexadecimal: `0x7f`, octal: `0o177`, binary: `0b1111111`)
- `string:` for strings (note that a string with a colon should always be prefixed by `string:`, no matter what the default prefix is).
- `boolean:` for booleans: 1 or 'true' (case insensitive) are True values, everything else is false.
- `na:` for N/A (everything after the colon is ignored)

Example::

    ,values
    a1,float:1.0
    a2,integer:2
    a3,3.03
    a4,string:a label
    a4b,string:another label
    a5,na:content ignored for N/A
    a6,boolean:1



XMCDA related:
..............

- **Tag:** other

----------------------------


.. _csvToXMCDA-alternativesValues-PyXMCDA-parameters:

parameters
~~~~~~~~~~


Description:
............

Parameters of the method



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** CSV delimiter

  *Indicates the delimiter to use. Leave blank for auto-detection.  It is especially handful when the auto-detection fails to determine the csv delimiter.*

  - **Constraint description:** One character maximum

  - **Type:** string
  - **Default value:** ""
- **Name:** Default content

  *.*

  - **Type:** drop-down list
  - **Possible values:**
      - float (XMCDA label : float) (default)

      - string (XMCDA label : label) 

      - integer (XMCDA label : integer) 

      - boolean (XMCDA label : boolean) 

- **Name:** First column

  *Content of the first column.*

  - **Type:** drop-down list
  - **Possible values:**
      - id (XMCDA label : false) 

      - id (name) (XMCDA label : true) (default)


XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
    <programParameters>
        <parameter id="csv_delimiter">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </parameter>
        <parameter id="default_prefix">
            <values>
                <value>
                    <label>%2</label>
                </value>
            </values>
        </parameter>
        <parameter id="name_in_id">
            <values>
                <value>
                    <boolean>%3</boolean>
                </value>
            </values>
        </parameter>
    </programParameters>


----------------------------



.. _csvToXMCDA-alternativesValues-PyXMCDA_outputs:

Outputs
-------


- :ref:`alternatives <csvToXMCDA-alternativesValues-PyXMCDA-alternatives>`
- :ref:`alternativesValues <csvToXMCDA-alternativesValues-PyXMCDA-alternativesValues>`
- :ref:`messages <csvToXMCDA-alternativesValues-PyXMCDA-messages>`

.. _csvToXMCDA-alternativesValues-PyXMCDA-alternatives:

alternatives
~~~~~~~~~~~~


Description:
............

The equivalent alternatives ids.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _csvToXMCDA-alternativesValues-PyXMCDA-alternativesValues:

alternatives values
~~~~~~~~~~~~~~~~~~~


Description:
............

The equivalent alternatives values.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _csvToXMCDA-alternativesValues-PyXMCDA-messages:

messages
~~~~~~~~


Description:
............

Status messages.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
