:orphan:



.. _randomPerformanceTable-PyXMCDA:

randomPerformanceTable
======================

:Provider: PyXMCDA
:Version: 1.0

Description
-----------

This service generate a random performance table from inputs.
Any scale type can be used and the service have different minimum requirements:
    - Quantitative scales need at least the 'minimum' and 'maximum' values to be set
    - Qualitative scales need at least the valued labels set
    - Nominal scales need at least the labels set

Distributions can be defined per criterion for the quantitative scales in the criteriaDistributions input.
Default is a uniform distribution between scale minimum and maximum bounds.
To set another distribution, user needs to add a criterionValues with following identified values:
    - Uniform distribution: value 'uniform' with id 'distribution', numerical values with id 'min' and 'max' to override scale boundaries (optional)
    - Normal distribution: value 'normal' with id 'distribution', numerical values with id 'mu' and 'std' for average and standard value, numerical values with id 'min' and 'max' to override scale boundaries (optional)
N.B: 'min' and 'max' values will be ignored if they are outside of the scale limits.

Other types of scales use a uniform random sampler to choose labels (distributions inputs for these scales are ignored).

Example of normal distribution (with optional min/max values)::

    <criterionValues>
        <criterionID>g1</criterionID>
        <values>
            <value id="distribution">
                <label>normal</label>
            </value>
            <value id="mu">
                <real>25000</real>
            </value>
            <value id="std">
                <real>5000</real>
            </value>
            <value id="min">
                <real>0</real>
            </value>
            <value id="max">
                <real>40000</real>
            </value>
        </values>
    </criterionValues>

Example of uniform distribution (with optional min/max values)::

    <criterionValues>
    <criterionID>g2</criterionID>
        <values>
            <value id="distribution">
                <label>uniform</label>
            </value>
            <value id="min">
                <real>5</real>
            </value>
            <value id="max">
                <real>15</real>
            </value>
        </values>
    </criterionValues>

- **Contact:** Nicolas Duminy (nicolas.duminy@telecom-bretagne.eu)

- **Web page:** https://gitlab.com/nduminy/ws-pyxmcda



Inputs
------
(For outputs, see :ref:`below <randomPerformanceTable-PyXMCDA_outputs>`)


- :ref:`alternatives <randomPerformanceTable-PyXMCDA-alternatives>`
- :ref:`criteriaScales <randomPerformanceTable-PyXMCDA-criteriaScales>`
- :ref:`criteriaDistributions <randomPerformanceTable-PyXMCDA-criteriaDistributions>` *(optional)*
- :ref:`parameters <randomPerformanceTable-PyXMCDA-parameters>` *(optional)*

.. _randomPerformanceTable-PyXMCDA-alternatives:

alternatives
~~~~~~~~~~~~


Description:
............

The alternatives (active field disregarded for this service).



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _randomPerformanceTable-PyXMCDA-criteriaScales:

criteria scales
~~~~~~~~~~~~~~~


Description:
............

The criteria scales.



XMCDA related:
..............

- **Tag:** criteriaScales

----------------------------


.. _randomPerformanceTable-PyXMCDA-criteriaDistributions:

criteria distributions
~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The distributions to use for each criterion.



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _randomPerformanceTable-PyXMCDA-parameters:

parameters
~~~~~~~~~~


Description:
............

Parameters of the method



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** Seed

  *Seed to be used for random numbers.*

  - **Type:** integer
  - **Default value:** 0

XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
    <programParameters>
        <programParameter id="seed" name="Seed">
            <values>
                <value>
                    <integer>%1</integer>
                </value>
            </values>
        </programParameter>
    </programParameters>


----------------------------



.. _randomPerformanceTable-PyXMCDA_outputs:

Outputs
-------


- :ref:`performanceTable <randomPerformanceTable-PyXMCDA-performanceTable>`
- :ref:`messages <randomPerformanceTable-PyXMCDA-messages>`

.. _randomPerformanceTable-PyXMCDA-performanceTable:

performance table
~~~~~~~~~~~~~~~~~


Description:
............

The performance table.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _randomPerformanceTable-PyXMCDA-messages:

messages
~~~~~~~~


Description:
............

Status messages.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
