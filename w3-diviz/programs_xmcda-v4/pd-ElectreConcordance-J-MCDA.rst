:orphan:



.. _ElectreConcordance-J-MCDA:

ElectreConcordance
==================

:Provider: J-MCDA
:Version: 0.5.5

Description
-----------

Computes a concordance relation.

- **Contact:** Olivier Cailloux <olivier.cailloux@ecp.fr>

- **Web page:** http://sourceforge.net/projects/j-mcda/

- **Reference:** Cailloux, Olivier. Electre and Promethee MCDA methods as reusable software components. In Proceedings of the 25th Mini-EURO Conference on Uncertainty and Robustness in Planning and Decision Making (URPDM 2010). Coimbra, Portugal, 2010.



Inputs
------
(For outputs, see :ref:`below <ElectreConcordance-J-MCDA_outputs>`)


- :ref:`criteria <ElectreConcordance-J-MCDA-input1>`
- :ref:`alternatives <ElectreConcordance-J-MCDA-input0>` *(optional)*
- :ref:`criteriaScales <ElectreConcordance-J-MCDA-criteriaScales>`
- :ref:`criteriaThresholds <ElectreConcordance-J-MCDA-criteriaThresholds>` *(optional)*
- :ref:`performances <ElectreConcordance-J-MCDA-input3>`
- :ref:`weights <ElectreConcordance-J-MCDA-input2>`

.. _ElectreConcordance-J-MCDA-input1:

criteria
~~~~~~~~


Description:
............

The criteria to consider, possibly with preference and veto thresholds (provided separated via the input 'criteriaThresholds'). Each one must have a preference direction (see input 'criteriaScales'). Set some criteria as inactive (or remove them) to ignore them.



XMCDA related:
..............

- **Tag:** criteria

----------------------------


.. _ElectreConcordance-J-MCDA-input0:

alternatives
~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The alternatives to consider. Set some alternatives as inactive (or remove them) to ignore them.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _ElectreConcordance-J-MCDA-criteriaScales:

criteria scales
~~~~~~~~~~~~~~~


Description:
............

The scales of the criteria to consider.



XMCDA related:
..............

- **Tag:** criteriaScales

----------------------------


.. _ElectreConcordance-J-MCDA-criteriaThresholds:

criteria thresholds
~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The criteria' preference and indifference thresholds.



XMCDA related:
..............

- **Tag:** criteriaThresholds

----------------------------


.. _ElectreConcordance-J-MCDA-input3:

performances
~~~~~~~~~~~~


Description:
............

The performances of the alternatives on the criteria to consider.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _ElectreConcordance-J-MCDA-input2:

weights
~~~~~~~


Description:
............

The weights of the criteria to consider.



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------



.. _ElectreConcordance-J-MCDA_outputs:

Outputs
-------


- :ref:`concordance <ElectreConcordance-J-MCDA-output0>`
- :ref:`messages <ElectreConcordance-J-MCDA-output1>`

.. _ElectreConcordance-J-MCDA-output0:

concordance
~~~~~~~~~~~


Description:
............

The concordance relation computed from the given input data.



XMCDA related:
..............

- **Tag:** alternativesMatrix

----------------------------


.. _ElectreConcordance-J-MCDA-output1:

messages
~~~~~~~~


Description:
............

A status message.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
