:orphan:



.. _Fuzzy-Labels_descriptors-URV:

Fuzzy-Labels_descriptors
========================

:Provider: URV
:Version: 4.0

Description
-----------

Two types of uncertainty in fuzzy sets are recognized: (1) specificity, related to the measurement of imprecision, which is based on the cardinality of the set, and (2) fuzziness, or entropy, which measures the vagueness of the set as a result of having imprecise boundaries. Specificity and fuzziness refer to two different characteristics of fuzzy sets. Specificity (or its counterpart, non-specificity) measures the degree of truth of the sentence: Containing just one element. Fuzziness measures the difference from a crisp set.

- **Contact:** Aida Valls <aida.valls@urv.cat>

- **Reference:** A. Valls, The Unbalanced Linguistic Ordered Weighted Averaging Operator, In: Proc. IEEE International Conference on Fuzzy Systems, FUZZ-IEEE 2010, IEEE Computer Society, Barcelona, Catalonia, 2010, pp. 3063-3070.



Inputs
------
(For outputs, see :ref:`below <Fuzzy-Labels_descriptors-URV_outputs>`)


- :ref:`fuzzyNumbers <Fuzzy-Labels_descriptors-URV-input0>`

.. _Fuzzy-Labels_descriptors-URV-input0:

fuzzyNumbers
~~~~~~~~~~~~


Description:
............

Definition of the fuzzy sets associated to the linguistic variable used for all the criteria. The semantics of the linguistic labels are given by a trapezoidal membership function, each membership  function is represented as a group of four consecutive segments, each segment is descrived with two 2D points (head, tail). Into this version exists the possibility to define a segment using the tail of the previous segment (only in the same label) as the head point of the new one, avoiding the possibility of continuity errors of the segments. If the second segment is equal to third segment, the fuzzy is triangular. The values must be ordered increasingly. For each label in the linguistic domain (categoriesValues list), a fuzzy set must be defined. The labels must be ordered from the worst to the best performance (fi. Low, Medium, High, Perfect).



XMCDA related:
..............

- **Tag:** criteriaScales

- **Code:**

  ::

    
		  				
			<criteriaScales id="fuzzy-numbers">
				<criterionScales>
					<criterionID>...<\criterionID>
					<scales>
						<scale id="fuzzy-scales">
							<qualitative>
								<preferenceDirection>min</preferenceDirection>
								<valuedLabels>
									<valuedLabel>
										<label>...</label>
										<value>
											<fuzzyNumber>
												<piecewiseLinear>
													<segment>
														<head>
															<abscissa>
																<real>...<\real>
															<\abscissa>
															<ordinate>
																<real>...<\real>
															<\ordinate>
														<\head>
														<tail>
															<abscissa>
																<real>...<\real>
															<\abscissa>
															<ordinate>
																<real>...<\real>
															<\ordinate>
														<\tail>
													<\segment>
													<segment>
														<\head>
														<tail>
															<abscissa>
																<real>...<\real>
															<\abscissa>
															<ordinate>
																<real>...<\real>
															<\ordinate>
														<\tail>
													<\segment>
													<segment>
													[...]
													<\segment>
													<segment>
													[...]
													<\segment>
												<\piecewiseLinear>
											<\fuzzyNumber>
										<\value>
									<\valuedLabel>
									<valuedLabel>
										[...]
									</valuedLabel>
									[...]
								</valuedLabels>
							<\qualitative>
						<\scale>
					<\scales>
				<\criterionScales>
			<\criteriaScales>
		  	
			

----------------------------



.. _Fuzzy-Labels_descriptors-URV_outputs:

Outputs
-------


- :ref:`specificity <Fuzzy-Labels_descriptors-URV-output0>`
- :ref:`fuzziness <Fuzzy-Labels_descriptors-URV-output1>`
- :ref:`messages <Fuzzy-Labels_descriptors-URV-output2>`

.. _Fuzzy-Labels_descriptors-URV-output0:

specificity
~~~~~~~~~~~


Description:
............

Result obtained from apply Specificity calculation for each fuzzy Number from the list.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _Fuzzy-Labels_descriptors-URV-output1:

fuzziness
~~~~~~~~~


Description:
............

Result obtained from apply Fuzziness calculation for each fuzzy Number from the list.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _Fuzzy-Labels_descriptors-URV-output2:

messages
~~~~~~~~


Description:
............

A status message.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
