:orphan:



.. _plotNumericPerformanceTable-ITTB:

plotNumericPerformanceTable
===========================

:Provider: ITTB
:Version: 2.0

Description
-----------

This web service generates a plot representing the performance table (abscissa: alternatives). The generated plot can be a bar plot or pie plot. Colors can be used and the title of the plot can be typed. In the case of a bar chart, the axis-labels can also be typed. It is possible to have multiple images or only one image per plot: by column, by line or a grid. In the generated plot, you can also order the alternatives by name, by id or by values in an ascending or descending order.

- **Contact:** Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)



Inputs
------
(For outputs, see :ref:`below <plotNumericPerformanceTable-ITTB_outputs>`)


- :ref:`alternatives <plotNumericPerformanceTable-ITTB-alternatives>`
- :ref:`criteria <plotNumericPerformanceTable-ITTB-criteria>`
- :ref:`parameters <plotNumericPerformanceTable-ITTB-parameters>`
- :ref:`performanceTable <plotNumericPerformanceTable-ITTB-performanceTable>`

.. _plotNumericPerformanceTable-ITTB-alternatives:

alternatives
~~~~~~~~~~~~


Description:
............

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
	<alternatives>
		<alternative>
			<active>[...]</active>
			[...]
		</alternative>
		[...]
	</alternatives>


----------------------------


.. _plotNumericPerformanceTable-ITTB-criteria:

criteria
~~~~~~~~


Description:
............

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
	<criteria>
		<criterion>
			<active>[...]</active>
			[...]
		</criterion>
		[...]
	</criteria>


----------------------------


.. _plotNumericPerformanceTable-ITTB-parameters:

parameters
~~~~~~~~~~


Description:
............

Several options to take into account.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** Chart type:

  *Type of the plot: choose between "Bar chart" and "Pie chart". The default plot is a bar chart.*

  - **Type:** drop-down list
  - **Possible values:**
      - Bar chart (XMCDA label : barChart) (default)

      - Pie chart (XMCDA label : pieChart) 

- **Name:** Number of images

  *One image per plot or multiple plots can be obtained.*

  - **Type:** drop-down list
  - **Possible values:**
      - Unique (XMCDA label : true) (default)

      - Multiple (XMCDA label : false) 

- **Name:** Plots arrangement

  *In the case of a unique plot, you can specify how to display the images: by line, by column or by square. The default value is by column.*

  - **Type:** drop-down list
  - **Possible values:**
      - Column (XMCDA label : column) (default)

      - Line (XMCDA label : line) 

      - Grid (XMCDA label : grid) 

- **Name:** Order abscissa by:

  *Choose between "name", "id" or "values".*

  - **Type:** drop-down list
  - **Possible values:**
      - name (XMCDA label : name) 

      - id (XMCDA label : id) 

      - values (XMCDA label : values) (default)

- **Name:** Order:

  *The parameter which says if the highest or lowest value is to be placed first.*

  - **Type:** drop-down list
  - **Possible values:**
      - increasing (XMCDA label : increasing) (default)

      - decreasing (XMCDA label : decreasing) 

- **Name:** Colors:

  *The use of colors: true for a colored plot.*

  - **Type:** drop-down list
  - **Possible values:**
      - Yes (XMCDA label : true) 

      - No (XMCDA label : false) (default)

- **Name:** Initial color:

  *String that indicates the initial color in the generated barplot or pieplot.Choose between "Black", "Red", "Blue", "Green", "Yellow", "Magenta" and "Cyan".*

  - **Type:** drop-down list
  - **Possible values:**
      - Black (XMCDA label : black) (default)

      - Red (XMCDA label : red) 

      - Blue (XMCDA label : blue) 

      - Green (XMCDA label : green) 

      - Yellow (XMCDA label : yellow) 

      - Magenta (XMCDA label : magenta) 

      - Cyan (XMCDA label : cyan) 

- **Name:** Final color:

  *String that indicates the final color in the generated barplot or pieplot. choose between "Black","White", "Red", "Blue", "Green", "Yellow", "Magenta" and "Cyan".*

  - **Type:** drop-down list
  - **Possible values:**
      - Black (XMCDA label : black) (default)

      - Red (XMCDA label : red) 

      - Blue (XMCDA label : blue) 

      - Green (XMCDA label : green) 

      - Yellow (XMCDA label : yellow) 

      - Magenta (XMCDA label : magenta) 

      - Cyan (XMCDA label : cyan) 

- **Name:** Chart title:

  *String for the title of the plot. The default value is an empty field.*

  - **Type:** string
  - **Default value:** ""
- **Name:** X axis label:

  *String for the horizontal axis-label.The default value is an empty field.*

  - **Type:** string
  - **Default value:** ""
- **Name:** Y axis label:

  *String for the vertical axis-label.The default value is an empty field.*

  - **Type:** string
  - **Default value:** ""

XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
	<programParameters>
		<parameter id="chart_type" name="Chart type">
			<values>
				<value>
					<label>%1</label>
				</value>
			</values>
		</parameter>
		 <parameter id="unique_plot" name="Unique plot">
			<values>
				<value>
					<label>%2</label>
				</value>
			</values>
		</parameter>
		 <parameter id="plots_display" name="Plots' display">
			<values>
				<value>
					<label>%3</label>
				</value>
			</values>
		</parameter>
		<parameter id="order_by" name="Order by">
			<values>
				<value>
					<label>%4</label>
				</value>
			</values>
		</parameter>
		<parameter id="order" name="order">
			<values>
				<value>
					<label>%5</label>
				</value>
			</values>
		</parameter>
		<parameter id="use_color" name="Colors in the chart">
			<values>
				<value>
					<label>%6</label>
				</value>
			</values>
		</parameter>
		<parameter id="initial_color" name="Initial color">
			<values>
				<value>
					<label>%7</label>
				</value>
			</values>
		</parameter>
		<parameter id="final_color" name="Final color">
			<values>
				<value>
					<label>%8</label>
				</value>
			</values>
		</parameter>
		<parameter id= "chart_title" name="Chart title">
			<values>
				<value>
					<label>%9</label>
				</value>
			</values>
		</parameter>
		<parameter id="domain_axis" name="Domain axis label">
			<values>
				<value>
					<label>%10</label>
				</value>
			</values>
		</parameter >
		<parameter id="range_axis" name="Range axis label">
			<values>
				<value>
					<label>%11</label>
				</value>
			</values>
		</parameter>
	</programParameters>


----------------------------


.. _plotNumericPerformanceTable-ITTB-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~


Description:
............

A performance table. The evaluations should be only real or integer numeric values, i.e. <real> or <integer>.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------



.. _plotNumericPerformanceTable-ITTB_outputs:

Outputs
-------


- :ref:`performanceTable.png <plotNumericPerformanceTable-ITTB-png>`
- :ref:`messages <plotNumericPerformanceTable-ITTB-messages>`

.. _plotNumericPerformanceTable-ITTB-png:

performanceTable (png)
~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The representation of the performance table as a PNG image.



XMCDA related:
..............

- **Tag:** criterionValue

----------------------------


.. _plotNumericPerformanceTable-ITTB-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
