:orphan:



.. _randomCriteria-PyXMCDA:

randomCriteria
==============

:Provider: PyXMCDA
:Version: 1.1

Description
-----------

This web service allows to create a simple list of criteria by providing the desired number of criteria. Now, it is not taking into account creation of thresholds.

- **Contact:** Thomas Veneziano (thomas.veneziano@uni.lu)



Inputs
------
(For outputs, see :ref:`below <randomCriteria-PyXMCDA_outputs>`)


- :ref:`nbCriteria <randomCriteria-PyXMCDA-nbCriteria>`
- :ref:`criteriaPrefix <randomCriteria-PyXMCDA-criteriaPrefix>` *(optional)*
- :ref:`criteriaNames <randomCriteria-PyXMCDA-criteriaNames>` *(optional)*
- :ref:`criteriaType <randomCriteria-PyXMCDA-criteriaType>` *(optional)*
- :ref:`seed <randomCriteria-PyXMCDA-seed>` *(optional)*
- :ref:`lowerBounds <randomCriteria-PyXMCDA-lowerBounds>` *(optional)*
- :ref:`upperBounds <randomCriteria-PyXMCDA-upperBounds>` *(optional)*
- :ref:`numberOfLevels <randomCriteria-PyXMCDA-numberOfLevels>` *(optional)*
- :ref:`preferenceDirection <randomCriteria-PyXMCDA-preferenceDirection>` *(optional)*
- :ref:`thresholdsNames <randomCriteria-PyXMCDA-thresholdsNames>` *(optional)*

.. _randomCriteria-PyXMCDA-nbCriteria:

nbCriteria
~~~~~~~~~~


Description:
............

Indicates the desired number of criteria. It must be a strict positive integer.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** nbCriteria

  *Indicates the desired number of criteria.*

  - **Constraint description:** The value should be a strict positive integer.

  - **Type:** integer
  - **Default value:** 2

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                            
                    <methodParameters>
                        <parameter
                             name="nbCriteria"> <!-- REQUIRED  -->
                            <value>
                                <integer>%1</integer>
                            </value>
                        </parameter>
                    </methodParameters>
                    
                     

----------------------------


.. _randomCriteria-PyXMCDA-criteriaPrefix:

criteriaPrefix
~~~~~~~~~~~~~~


Description:
............

Indicates the desired prefix for the name of the criteria. If not provided, criteria will be called g1, g2, ... If provided, criteria will be called prefix1, prefix2, ... Note that it will only be used if you provide a number of alternatives.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** criteriaPrefix

  *Indicates the desired prefix for the criteria.*

  - **Type:** string
  - **Default value:** "g"

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                            
                    <methodParameters>
                        <parameter
                             name="criteriaPrefix"> <!-- REQUIRED  -->
                            <value>
                                <label>%1</label>
                            </value>
                        </parameter>
                    </methodParameters>
                    
                     

----------------------------


.. _randomCriteria-PyXMCDA-criteriaNames:

criteriaNames
~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

Indicates a list of criteria names. Note that if criteriaNames and nbCriteria are provided at the same time, then only criteriaNames are considered.



XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                            
                    <methodParameters>
                        <parameters
                             name="criteriaNames"> <!-- REQUIRED  -->
                            <parameter><value>
                                <label>[...]</label>
                            </value></parameter>
                            [..]
                        </parameter>
                    </methodParameters>
                    
                     

----------------------------


.. _randomCriteria-PyXMCDA-criteriaType:

criteriaType
~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

Allows to indicate the type of some criteria (qualitative or quantitative). The default value for each criterion is quantitative.



XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                            
                    <methodParameters>
                        <parameters
                             name="criteriaType"> <!-- REQUIRED  -->
                            <parameter name="<!-- The name of the criterion -->>
                                 <value><label><!-- qualitative or quantitative --></label></value>
                            </parameter>
                            [..]
                        </parameter>
                    </methodParameters>
                    
                     

----------------------------


.. _randomCriteria-PyXMCDA-seed:

seed
~~~~


Description:
............

Allows to initialize the random generator with a precise seed.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via the XMCDA file.

- **Name:** seed

  *Allows to initialize the random generator with a precise seed*

  - **Constraint description:** The value should be a positive integer.

  - **Type:** integer

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                            
                                   <methodParameters>
                                          <parameter
                                                 name="seed"> <!-- REQUIRED  -->
                                                 <value>
                                                        <integer>%1</integer>
                                                 </value>
                                          </parameter>
                                   </methodParameters>
                            
                     

----------------------------


.. _randomCriteria-PyXMCDA-lowerBounds:

lowerBounds
~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

Allows to indicate a lower bound for some quantitative criteria. The default value is 0. Lower bounds for qualitative criteria are ignored.



XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                            
                    <methodParameters>
                        <parameters
                             name="lowerBound"> <!-- REQUIRED  -->
                            <parameter name="<!-- The name of a quantitative criterion -->>
                                 <value>[...]</value>
                            </parameter>
                            [..]
                        </parameter>
                    </methodParameters>
                    
                     

----------------------------


.. _randomCriteria-PyXMCDA-upperBounds:

upperBounds
~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

Allows to indicate an upper bound for some quantitative criteria. The default value is 100. Upper bounds for qualitative criteria are ignored.



XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                            
                    <methodParameters>
                        <parameters
                             name="upperBound"> <!-- REQUIRED  -->
                            <parameter name="<!-- The name of a quantitative criterion -->>
                                 <value>[...]</value>
                            </parameter>
                            [..]
                        </parameter>
                    </methodParameters>
                    
                     

----------------------------


.. _randomCriteria-PyXMCDA-numberOfLevels:

numberOfLevels
~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

Allows to indicate a number of levels for some qualitative criteria. The default value is 10. if provided for quantitative criteria, it will be ignored.



XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                            
                    <methodParameters>
                        <parameters
                             name="numberOfLevels"> <!-- REQUIRED  -->
                            <parameter name="<!-- The name of a qualitative criterion -->>
                                 <value><integer>[...]</integer></value>
                            </parameter>
                            [..]
                        </parameter>
                    </methodParameters>
                    
                     

----------------------------


.. _randomCriteria-PyXMCDA-preferenceDirection:

preferenceDirection
~~~~~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

Allows to indicate a preference direction (min or max) for some criteria. The default value is max.



XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                            
                    <methodParameters>
                        <parameters
                             name="preferenceDirection"> <!-- REQUIRED  -->
                            <parameter name="<!-- The name of a criterion -->>
                                 <value><label><!-- min or max --></label></value>
                            </parameter>
                            [..]
                        </parameter>
                    </methodParameters>
                    
                     

----------------------------


.. _randomCriteria-PyXMCDA-thresholdsNames:

thresholdsNames
~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

Indicates a list of thresholds names. The names must be provided in an increasing order of importance, as the randomly generated values will be sorted and associated to the thresholds in the given order. The thresholds are added for every criteria (with different random values for each one).



XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    
                            
                    <methodParameters>
                        <parameters
                             name="thresholdsNames"> <!-- REQUIRED  -->
                            <parameter>
                                 <value><label><!-- the name of a threshold --></label></value>
                            </parameter>
                            [..]
                        </parameter>
                    </methodParameters>
                    
                     

----------------------------



.. _randomCriteria-PyXMCDA_outputs:

Outputs
-------


- :ref:`criteria <randomCriteria-PyXMCDA-criteria>`
- :ref:`messages <randomCriteria-PyXMCDA-messages>`

.. _randomCriteria-PyXMCDA-criteria:

criteria
~~~~~~~~


Description:
............

A list of criteria with active tags.



XMCDA related:
..............

- **Tag:** criteria

----------------------------


.. _randomCriteria-PyXMCDA-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
