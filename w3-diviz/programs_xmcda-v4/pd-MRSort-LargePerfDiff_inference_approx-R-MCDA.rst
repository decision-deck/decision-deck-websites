:orphan:



.. _MRSort-LargePerfDiff_inference_approx-R-MCDA:

Approximative inference of MRSort with large performance differences
====================================================================

:Provider: R-MCDA
:Version: 1.0

Description
-----------

MRSort is a simplified ELECTRE TRI sorting method, where alternatives are assigned to an ordered set of categories. In this case, we also take into account large performance differences, both negative (vetoes) and positive (dictators). The identification of the profiles, weights and majority threshold are done by taking into account assignment examples.

- **Contact:** Alexandru Olteanu (alexandru.olteanu@univ-ubs.fr)



Inputs
------
(For outputs, see :ref:`below <MRSort-LargePerfDiff_inference_approx-R-MCDA_outputs>`)


- :ref:`alternatives <MRSort-LargePerfDiff_inference_approx-R-MCDA-inalt>`
- :ref:`performanceTable <MRSort-LargePerfDiff_inference_approx-R-MCDA-inperf>`
- :ref:`criteria <MRSort-LargePerfDiff_inference_approx-R-MCDA-incrit>`
- :ref:`alternativesAssignments <MRSort-LargePerfDiff_inference_approx-R-MCDA-assignments>`
- :ref:`categoriesRanks <MRSort-LargePerfDiff_inference_approx-R-MCDA-incategval>`
- :ref:`parameters <MRSort-LargePerfDiff_inference_approx-R-MCDA-parameters>` *(optional)*

.. _MRSort-LargePerfDiff_inference_approx-R-MCDA-inalt:

alternatives
~~~~~~~~~~~~


Description:
............

A complete list of alternatives to be considered when inferring the MR-Sort model.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
			

----------------------------


.. _MRSort-LargePerfDiff_inference_approx-R-MCDA-inperf:

performance table
~~~~~~~~~~~~~~~~~


Description:
............

The evaluations of the alternatives on the set of criteria.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _MRSort-LargePerfDiff_inference_approx-R-MCDA-incrit:

criteria scales
~~~~~~~~~~~~~~~


Description:
............

A list of criteria on which the alternatives are evaluated. For each criterion, the preference direction should be provided.



XMCDA related:
..............

- **Tag:** criteriaScales

----------------------------


.. _MRSort-LargePerfDiff_inference_approx-R-MCDA-assignments:

alternativesAssignments
~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The alternatives assignments to categories.



XMCDA related:
..............

- **Tag:** alternativesAssignments

----------------------------


.. _MRSort-LargePerfDiff_inference_approx-R-MCDA-incategval:

categories ranks
~~~~~~~~~~~~~~~~


Description:
............

A list of categories ranks, 1 stands for the most preferred category and the higher the number the lower the preference for that category.



XMCDA related:
..............

- **Tag:** categoriesValues

----------------------------


.. _MRSort-LargePerfDiff_inference_approx-R-MCDA-parameters:

parameters
~~~~~~~~~~


Description:
............

The method parameters.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** Assignment rule

  *The type of assignment rule. Can be anything from the list M (majority rule), V (veto), D (dictator), v (veto weakened by dictator), d (dictator weakened by veto), dV (dominating veto and weakened dictator), Dv (dominating dictator and weakened veto) and dv (conflicting veto and dictator).*

  - **Type:** drop-down list
  - **Possible values:**
      - Majority rule (XMCDA label : M) (default)

      - Veto (XMCDA label : V) 

      - Dictator (XMCDA label : D) 

      - Veto weakened by dictator (XMCDA label : v) 

      - Dictator weakened by veto (XMCDA label : d) 

      - Dominating Veto and weakened Dictator (XMCDA label : dV) 

      - Dominating Dictator and weakened veto (XMCDA label : Dv) 

      - Conflicting Veto and Dictator (XMCDA label : dv) 

- **Name:** Time limit

  *The execution time limit in seconds..*

  - **Constraint description:** An integer value (minimum 1)

  - **Type:** integer
  - **Default value:** 60
- **Name:** Population size

  *The algorithm population size.*

  - **Constraint description:** An integer value (minimum 10)

  - **Type:** integer
  - **Default value:** 20
- **Name:** Mutation probability

  *The algorithm mutation probability..*

  - **Constraint description:** A value between 0 and 1

  - **Type:** float
  - **Default value:** 0.1
- **Name:** Test

  *Fixes the random generator seed. Only used for testing.*

  - **Default value:** false

XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    

    <programParameters>
        <parameter id="assignmentRule">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </parameter>
        <parameter id="time">
            <values>
                <value>
                    <integer>%2</integer>
                </value>
            </values>
        </parameter>
        <parameter id="population">
            <values>
                <value>
                    <integer>%3</integer>
                </value>
            </values>
        </parameter>
        <parameter id="mutation">
            <values>
                <value>
                    <real>%4</real>
                </value>
            </values>
        </parameter>
        <parameter id="test">
            <values>
                <value>
                    <boolean>%5</boolean>
                </value>
            </values>
        </parameter>
    </programParameters>

			

----------------------------



.. _MRSort-LargePerfDiff_inference_approx-R-MCDA_outputs:

Outputs
-------


- :ref:`categoriesProfilesPerformanceTable <MRSort-LargePerfDiff_inference_approx-R-MCDA-outcatprofpt>`
- :ref:`vetoProfilesPerformanceTable <MRSort-LargePerfDiff_inference_approx-R-MCDA-outcatvetopt>`
- :ref:`dictatorProfilesPerformanceTable <MRSort-LargePerfDiff_inference_approx-R-MCDA-outcatdictatorpt>`
- :ref:`criteriaWeights <MRSort-LargePerfDiff_inference_approx-R-MCDA-weights>`
- :ref:`categoriesProfiles <MRSort-LargePerfDiff_inference_approx-R-MCDA-outcatprof>`
- :ref:`vetoProfiles <MRSort-LargePerfDiff_inference_approx-R-MCDA-outcatveto>`
- :ref:`dictatorProfiles <MRSort-LargePerfDiff_inference_approx-R-MCDA-outcatdictator>`
- :ref:`fitness <MRSort-LargePerfDiff_inference_approx-R-MCDA-fitness>`
- :ref:`majorityThreshold <MRSort-LargePerfDiff_inference_approx-R-MCDA-majority>`
- :ref:`messages <MRSort-LargePerfDiff_inference_approx-R-MCDA-msg>`

.. _MRSort-LargePerfDiff_inference_approx-R-MCDA-outcatprofpt:

categories profiles performance table
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The evaluations of the category profiles.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _MRSort-LargePerfDiff_inference_approx-R-MCDA-outcatvetopt:

veto profiles performance table
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The evaluations of the veto profiles.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _MRSort-LargePerfDiff_inference_approx-R-MCDA-outcatdictatorpt:

dictator profiles performance table
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The evaluations of the dictator profiles.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _MRSort-LargePerfDiff_inference_approx-R-MCDA-weights:

criteria weights
~~~~~~~~~~~~~~~~


Description:
............

The criteria weights.



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _MRSort-LargePerfDiff_inference_approx-R-MCDA-outcatprof:

categories profiles
~~~~~~~~~~~~~~~~~~~


Description:
............

The categories delimiting profiles.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _MRSort-LargePerfDiff_inference_approx-R-MCDA-outcatveto:

veto profiles
~~~~~~~~~~~~~


Description:
............

The categories veto profiles.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _MRSort-LargePerfDiff_inference_approx-R-MCDA-outcatdictator:

dictator profiles
~~~~~~~~~~~~~~~~~


Description:
............

The categories dictator profiles.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _MRSort-LargePerfDiff_inference_approx-R-MCDA-fitness:

fitness
~~~~~~~


Description:
............

The model fitness.



XMCDA related:
..............

- **Tag:** programParameters

----------------------------


.. _MRSort-LargePerfDiff_inference_approx-R-MCDA-majority:

majority threshold
~~~~~~~~~~~~~~~~~~


Description:
............

The majority threshold.



XMCDA related:
..............

- **Tag:** programParameters

----------------------------


.. _MRSort-LargePerfDiff_inference_approx-R-MCDA-msg:

messages
~~~~~~~~


Description:
............

Messages from the execution of the webservice. Possible errors in the input data will be given here.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
