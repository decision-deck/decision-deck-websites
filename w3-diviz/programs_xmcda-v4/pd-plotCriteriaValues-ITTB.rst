:orphan:



.. _plotCriteriaValues-ITTB:

plotCriteriaValues
==================

:Provider: ITTB
:Version: 2.0

Description
-----------

This web service generates a barplot or a pie plot representing a numeric quantity for each criterion, like, e.g., an importance value. Compared to the web service plotCriteriaValues, some parameters are added. Colors can be used and the title of the plot can be typed. In the case of a bar chart, the axis-labels can also be typed. The criteria's evaluations are supposed to be real or integer numeric values.

- **Contact:** Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)



Inputs
------
(For outputs, see :ref:`below <plotCriteriaValues-ITTB_outputs>`)


- :ref:`criteria <plotCriteriaValues-ITTB-criteria>`
- :ref:`criteriaValues <plotCriteriaValues-ITTB-criteriaValues>`
- :ref:`parameters <plotCriteriaValues-ITTB-parameters>`

.. _plotCriteriaValues-ITTB-criteria:

criteria
~~~~~~~~


Description:
............

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    
	<criteria>
		<criterion>
			<active>[...]</active>
			[...]
		</criterion>
		[...]
	</criteria>


----------------------------


.. _plotCriteriaValues-ITTB-criteriaValues:

criteriaValues
~~~~~~~~~~~~~~


Description:
............

A list of <criterionValue> representing a certain numeric quantity for each criterion, like, e.g., an importance value.



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _plotCriteriaValues-ITTB-parameters:

parameters
~~~~~~~~~~


Description:
............

Plot type method: choose between "Bar chart" and "Pie chart". The default plot is a bar chart.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** Chart type:

  *Type of the plot: choose between "Bar chart" and "Pie chart". The default plot is a bar chart.*

  - **Type:** drop-down list
  - **Possible values:**
      - bar chart (XMCDA label : barChart) (default)

      - pie chart (XMCDA label : pieChart) 

- **Name:** Order by:

  *Choose between "name", "id" or "values".*

  - **Type:** drop-down list
  - **Possible values:**
      - name (XMCDA label : name) 

      - id (XMCDA label : id) 

      - values (XMCDA label : values) (default)

- **Name:** Order:

  *The parameter which says if the highest or lowest value is to be placed first.*

  - **Type:** drop-down list
  - **Possible values:**
      - increasing (XMCDA label : increasing) (default)

      - decreasing (XMCDA label : decreasing) 

- **Name:** Colors:

  *The use of colors: true for a colored plot.*

  - **Type:** drop-down list
  - **Possible values:**
      - gradient (XMCDA label : true) 

      - black and white (XMCDA label : false) (default)

- **Name:** Initial color:

  *String that indicates the initial color in the generated barplot or pieplot.Choose between "Black", "Red", "Blue", "Green", "Yellow", "Magenta" and "Cyan".*

  - **Type:** drop-down list
  - **Possible values:**
      - black (XMCDA label : black) (default)

      - red (XMCDA label : red) 

      - blue (XMCDA label : blue) 

      - green (XMCDA label : green) 

      - yellow (XMCDA label : yellow) 

      - magenta (XMCDA label : magenta) 

      - cyan (XMCDA label : cyan) 

- **Name:** Final color:

  *String that indicates the final color in the generated barplot or pieplot. choose between "Black", "White", "Red", "Blue", "Green", "Yellow", "Magenta", "Cyan" and "White".*

  - **Type:** drop-down list
  - **Possible values:**
      - black (XMCDA label : black) (default)

      - red (XMCDA label : red) 

      - blue (XMCDA label : blue) 

      - green (XMCDA label : green) 

      - yellow (XMCDA label : yellow) 

      - magenta (XMCDA label : magenta) 

      - cyan (XMCDA label : cyan) 

      - white (XMCDA label : white) 

- **Name:** Chart title:

  *String for the title of the plot. The default value is an empty field.*

  - **Type:** string
  - **Default value:** ""
- **Name:** X axis label:

  *String for the horizontal axis-label.The default value is an empty field.*

  - **Type:** string
  - **Default value:** ""
- **Name:** Y axis label:

  *String for the vertical axis-label.The default value is an empty field.*

  - **Type:** string
  - **Default value:** ""

XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
	<programParameters>
		<parameter id="chart_type" name="Chart type">
			<values>
				<value>
					<label>%1</label>
				</value>
			</values>
		</parameter>
		<parameter id="order_by" name="Order by">
			<values>
				<value>
					<label>%2</label>
				</value>
			</values>
		</parameter>
		<parameter id="order" name="Order">
			<values>
				<value>
					<label>%3</label>
				</value>
			</values>
		</parameter>
		<parameter id="use_color" name="Colors in the chart">
			<values>
				<value>
					<label>%4</label>
				</value>
			</values>
		</parameter>
		<parameter id="initial_color" name="Initial color">
			<values>
				<value>
					<label>%5</label>
				</value>
			</values>
		</parameter>
		<parameter id="final_color" name="Final color">
			<values>
				<value>
					<label>%6</label>
				</value>
			</values>
		</parameter>
		<parameter id= "chart_title" name="Chart title">
			<values>
				<value>
					<label>%7</label>
				</value>
			</values>
		</parameter>
		<parameter id="domain_axis" name="Domain axis label">
			<values>
				<value>
					<label>%8</label>
				</value>
			</values>
		</parameter >
		<parameter id="range_axis" name="Range axis label">
			<values>
				<value>
					<label>%9</label>
				</value>
			</values>
		</parameter>
	</programParameters>


----------------------------



.. _plotCriteriaValues-ITTB_outputs:

Outputs
-------


- :ref:`criteriaValues.png <plotCriteriaValues-ITTB-png>`
- :ref:`messages <plotCriteriaValues-ITTB-messages>`

.. _plotCriteriaValues-ITTB-png:

criteria values (png)
~~~~~~~~~~~~~~~~~~~~~


Description:
............

The requested barplot or pieplot as a PNG image.



XMCDA related:
..............

- **Tag:** criterionValue

----------------------------


.. _plotCriteriaValues-ITTB-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
