:orphan:



.. _PROMETHEE-Cluster-PUT:

PROMETHEE-Cluster
=================

:Provider: PUT
:Version: 1.0

Description
-----------

Clustering using Promethee Tri and k-means algorithm

- **Contact:** Mateusz Sarbinowski <mateusz.sarbinowski@student.put.edu.pl>

- **Web page:** https://github.com/dohko93/mcdm/tree/master/PROMETHEE-Cluster



Inputs
------
(For outputs, see :ref:`below <PROMETHEE-Cluster-PUT_outputs>`)


- :ref:`criteria <PROMETHEE-Cluster-PUT-input2>`
- :ref:`alternatives <PROMETHEE-Cluster-PUT-input1>`
- :ref:`performanceTable <PROMETHEE-Cluster-PUT-input4>`
- :ref:`criteriaValues <PROMETHEE-Cluster-PUT-input3>`
- :ref:`criteriaScales <PROMETHEE-Cluster-PUT-input6>`
- :ref:`criteriaThresholds <PROMETHEE-Cluster-PUT-input7>`
- :ref:`programParameters <PROMETHEE-Cluster-PUT-input5>`

.. _PROMETHEE-Cluster-PUT-input2:

criteria
~~~~~~~~


Description:
............

Information about criterias required to compute Promethee Tri: criteria id, criteria thresholds (indifference and preference) and criteria scales.



XMCDA related:
..............

- **Tag:** criteria

----------------------------


.. _PROMETHEE-Cluster-PUT-input1:

alternatives
~~~~~~~~~~~~


Description:
............

List of alternatives.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _PROMETHEE-Cluster-PUT-input4:

performance table
~~~~~~~~~~~~~~~~~


Description:
............

The performance of alternatives.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _PROMETHEE-Cluster-PUT-input3:

weights
~~~~~~~


Description:
............

Weights of criteria to consider.



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _PROMETHEE-Cluster-PUT-input6:

criteria scales
~~~~~~~~~~~~~~~


Description:
............

None



XMCDA related:
..............

- **Tag:** criteriaScales

----------------------------


.. _PROMETHEE-Cluster-PUT-input7:

criteria thresholds
~~~~~~~~~~~~~~~~~~~


Description:
............

None



XMCDA related:
..............

- **Tag:** criteriaThresholds

----------------------------


.. _PROMETHEE-Cluster-PUT-input5:

parameters
~~~~~~~~~~


Description:
............

First parameter specifies number of output clusters K.
                    Where K is integer greater than 1 and less than number of alternatives.
                    Second parameter specifies parameter P of deviation used in Promethee TRI.
                    In this case deviation is weighted generalised mean of difference between flows on criteria, where P is used exponent.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** number of clusters

  

  - **Constraint description:** A integer value greater than 1 and lower than the number of alternatives

  - **Type:** integer
- **Name:** distance

  

  - **Type:** integer
  - **Default value:** 1
- **Name:** Use random seed

  

  - **Default value:** false
- **Name:** random seed

  

  - **Type:** integer

XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
    <programParameters>
        %1
        %2
<!-- %3 -->
        %4
    </programParameters>


            

----------------------------



.. _PROMETHEE-Cluster-PUT_outputs:

Outputs
-------


- :ref:`result <PROMETHEE-Cluster-PUT-output1>`
- :ref:`messages <PROMETHEE-Cluster-PUT-output2>`

.. _PROMETHEE-Cluster-PUT-output1:

alternatives sets
~~~~~~~~~~~~~~~~~


Description:
............

List of alternatives sets where each set representing one cluster.



XMCDA related:
..............

- **Tag:** alternativesSets

----------------------------


.. _PROMETHEE-Cluster-PUT-output2:

messages
~~~~~~~~


Description:
............

Messages or errors generated by this module.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
