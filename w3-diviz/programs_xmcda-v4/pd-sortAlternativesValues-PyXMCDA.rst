:orphan:



.. _sortAlternativesValues-PyXMCDA:

sortAlternativesValues
======================

:Provider: PyXMCDA
:Version: 1.0

Description
-----------

Sort alternatives into categories per values.
The categories are delimited by one interval of numeric values each.

- **Contact:** Nicolas Duminy (nicolas.duminy@telecom-bretagne.eu)

- **Web page:** https://gitlab.com/nduminy/ws-pyxmcda



Inputs
------
(For outputs, see :ref:`below <sortAlternativesValues-PyXMCDA_outputs>`)


- :ref:`alternatives <sortAlternativesValues-PyXMCDA-alternatives>` *(optional)*
- :ref:`categories <sortAlternativesValues-PyXMCDA-categories>` *(optional)*
- :ref:`alternativesValues <sortAlternativesValues-PyXMCDA-alternativesValues>`
- :ref:`categoriesLimits <sortAlternativesValues-PyXMCDA-categorieslimits>`

.. _sortAlternativesValues-PyXMCDA-alternatives:

alternatives
~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The alternatives to assign (only the active ones). All are considered if not provided.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _sortAlternativesValues-PyXMCDA-categories:

categories
~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The categories to assign to (only the active ones). All are considered if not provided.



XMCDA related:
..............

- **Tag:** categories

----------------------------


.. _sortAlternativesValues-PyXMCDA-alternativesValues:

alternatives values
~~~~~~~~~~~~~~~~~~~


Description:
............

The alternatives values.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _sortAlternativesValues-PyXMCDA-categorieslimits:

categories limits
~~~~~~~~~~~~~~~~~


Description:
............

Categories limits represented by intervals of numeric values.



XMCDA related:
..............

- **Tag:** categoriesValues

----------------------------



.. _sortAlternativesValues-PyXMCDA_outputs:

Outputs
-------


- :ref:`alternativesAssignments <sortAlternativesValues-PyXMCDA-alternativesAssignments>`
- :ref:`messages <sortAlternativesValues-PyXMCDA-messages>`

.. _sortAlternativesValues-PyXMCDA-alternativesAssignments:

alternatives assignments
~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The computed alternatives assignments.



XMCDA related:
..............

- **Tag:** alternativesAssignments

----------------------------


.. _sortAlternativesValues-PyXMCDA-messages:

messages
~~~~~~~~


Description:
............

Status messages.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
