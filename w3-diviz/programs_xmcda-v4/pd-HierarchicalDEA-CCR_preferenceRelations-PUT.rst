:orphan:



.. _HierarchicalDEA-CCR_preferenceRelations-PUT:

HierarchicalDEA-CCR_preferenceRelations
=======================================

:Provider: PUT
:Version: 1.0

Description
-----------

Computes necessary and possible preference relations for the pairs of DMUs (alternatives) using CCR Data Envelopment Analysis Model with hierarchical structure of outputs.

- **Contact:** Anna Labijak <anna.labijak@cs.put.poznan.pl>



Inputs
------
(For outputs, see :ref:`below <HierarchicalDEA-CCR_preferenceRelations-PUT_outputs>`)


- :ref:`units <HierarchicalDEA-CCR_preferenceRelations-PUT-units>`
- :ref:`performanceTable <HierarchicalDEA-CCR_preferenceRelations-PUT-performanceTable>`
- :ref:`hierarchy <HierarchicalDEA-CCR_preferenceRelations-PUT-hierarchy>`
- :ref:`weightsLinearConstraints <HierarchicalDEA-CCR_preferenceRelations-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <HierarchicalDEA-CCR_preferenceRelations-PUT-methodParameters>`

.. _HierarchicalDEA-CCR_preferenceRelations-PUT-units:

units
~~~~~


Description:
............

A list of alternatives (DMUs).



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>

----------------------------


.. _HierarchicalDEA-CCR_preferenceRelations-PUT-performanceTable:

performance
~~~~~~~~~~~


Description:
............

A list of alternatives (DMUs) performances (on leaf hierarchy criteria).



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<values><value>[...]</value></values>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>

----------------------------


.. _HierarchicalDEA-CCR_preferenceRelations-PUT-hierarchy:

hierarchy
~~~~~~~~~


Description:
............

The hierarchical structure of criteria.



XMCDA related:
..............

- **Tag:** criteriaHierarchy

- **Code:**

  ::

    
                <criteriaHierarchy>
						<nodes>
                            <node>
                                <criterionID>[...]</criterionID>
                                <nodes>
                                    <node>
                                        <criterionID>[...]</criterionID>
                                        [...]
                                    </node>
                                    [...]
                                </nodes>
                            </node>
                        <nodes>
					</criteriaHierarchy>
            

----------------------------


.. _HierarchicalDEA-CCR_preferenceRelations-PUT-weightsLinearConstraints:

weights constraints
~~~~~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

A list of hierarchy criteria linear constraints.



XMCDA related:
..............

- **Tag:** criteriaLinearConstraints

- **Code:**

  ::

    
            <criteriaLinearConstraints>
                <constraints>
                    <constraint>
                        <elements>
                            <element>
                                <criterionID> [...] </criterionID>
                                <coefficient>
                                    [...]
                                </coefficient>
                            </element>
                            [...]
                        </elements>
                        <operator>[...]</operator>
                        <rhs>
                            [...]
                        </rhs>
                    </constraint>
                    [...]
                </constraints>
            </criteriaLinearConstraints>

----------------------------


.. _HierarchicalDEA-CCR_preferenceRelations-PUT-methodParameters:

parameters
~~~~~~~~~~


Description:
............

Represents parameters (hierarchyNode).



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** hierarchy node

  *ID of the hierarchy criterion for which the analysis should be performed.*

  - **Type:** string
  - **Default value:** "root"

XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
    <programParameters>
        <parameter id="hierarchyNode">
            <values>
                <value><label>%1</label></value>
            </values>
        </parameter>
    </programParameters>

----------------------------



.. _HierarchicalDEA-CCR_preferenceRelations-PUT_outputs:

Outputs
-------


- :ref:`necessaryDominance <HierarchicalDEA-CCR_preferenceRelations-PUT-necessaryDominance>`
- :ref:`possibleDominance <HierarchicalDEA-CCR_preferenceRelations-PUT-possibleDominance>`
- :ref:`messages <HierarchicalDEA-CCR_preferenceRelations-PUT-messages>`

.. _HierarchicalDEA-CCR_preferenceRelations-PUT-necessaryDominance:

necessary dominance
~~~~~~~~~~~~~~~~~~~


Description:
............

A matrix containing necessary preference relation between pairs of DMUs.



XMCDA related:
..............

- **Tag:** alternativesMatrix

- **Code:**

  ::

    
                    <alternativesMatrix>
						<row>
                            <alternativeID>[...]</alternativeID>
                            <column>
                                <alternativeID>[...]</alternativeID>
                                <values>
                                    <value><integer>1</integer></value>
                                </values>
                            </column>
                            [...]
                        </row>
                        [...]
					</alternativesMatrix>
                    

----------------------------


.. _HierarchicalDEA-CCR_preferenceRelations-PUT-possibleDominance:

possible dominance
~~~~~~~~~~~~~~~~~~


Description:
............

A matrix containing possible preference relation between pairs of DMUs.



XMCDA related:
..............

- **Tag:** alternativesMatrix

- **Code:**

  ::

    
                    <alternativesMatrix>
						<row>
                            <alternativeID>[...]</alternativeID>
                            <column>
                                <alternativeID>[...]</alternativeID>
                                <values>
                                    <value><integer>1</integer></value>
                                </values>
                            </column>
                            [...]
                        </row>
                        [...]
					</alternativesMatrix>
                    

----------------------------


.. _HierarchicalDEA-CCR_preferenceRelations-PUT-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
