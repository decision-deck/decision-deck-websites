
:orphan:

.. _plotXYAlternativesValues-PyXMCDA:

plotXYAlternativesValues
========================

:Provider: PyXMCDA
:Version: 1.3

Description
-----------

Generate plot from provided alternativesValues as well as the scripts generating these plots.
The plot compares both alternativesValues by plotting them as 2D points with first set of values as abscissa and second one as ordinates.

- **Contact:** Nicolas Duminy (nicolas.duminy@telecom-bretagne.eu)

- **Web page:** https://gitlab.com/nduminy/ws-pyxmcda



Inputs
------
(For outputs, see :ref:`below <plotXYAlternativesValues-PyXMCDA_outputs>`)


- :ref:`alternatives <plotXYAlternativesValues-PyXMCDA-alternatives>` *(optional)*
- :ref:`alternativesValues1 <plotXYAlternativesValues-PyXMCDA-alternativesValues1>`
- :ref:`alternativesValues2 <plotXYAlternativesValues-PyXMCDA-alternativesValues2>`
- :ref:`parameters <plotXYAlternativesValues-PyXMCDA-parameters>` *(optional)*

.. _plotXYAlternativesValues-PyXMCDA-alternatives:

alternatives
~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The alternatives to be plotted. All are plotted if not provided.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _plotXYAlternativesValues-PyXMCDA-alternativesValues1:

alternatives values 1
~~~~~~~~~~~~~~~~~~~~~


Description:
............

The reference alternatives values to compare to.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _plotXYAlternativesValues-PyXMCDA-alternativesValues2:

alternatives values 2
~~~~~~~~~~~~~~~~~~~~~


Description:
............

The alternatives values subject to comparison.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _plotXYAlternativesValues-PyXMCDA-parameters:

parameters
~~~~~~~~~~


Description:
............

Parameters of the method



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** X-axis label

  *X-axis name to display below each plot.*

  - **Type:** string
  - **Default value:** "alternativesValues 1"
- **Name:** Y-axis label

  *Y-axis name to display on the left of each plot.*

  - **Type:** string
  - **Default value:** "alternativesValues 2"
- **Name:** Chart title

  *Title of the chart to be plotted.*

  - **Type:** string
  - **Default value:** "Alternatives Values XY"
- **Name:** Color

  *Color used to draw the plot.*

  - **Type:** drop-down list
  - **Possible values:**
      - red (XMCDA label : r) 

      - blue (XMCDA label : b) 

      - green (XMCDA label : g) 

      - cyan (XMCDA label : c) 

      - magenta (XMCDA label : m) 

      - yellow (XMCDA label : y) 

      - black (XMCDA label : k) (default)

      - white (XMCDA label : w) 

- **Name:** Marker

  *Marker used to plot points.*

  - **Type:** drop-down list
  - **Possible values:**
      - point (XMCDA label : .) 

      - none (XMCDA label : None) 

      - circle (XMCDA label : o) (default)

      - triangle down (XMCDA label : v) 

      - triangle up (XMCDA label : ^) 

      - triangle left (XMCDA label : &lt;) 

      - triangle right (XMCDA label : &gt;) 

      - square (XMCDA label : s) 

      - pentagon (XMCDA label : p) 

      - star (XMCDA label : *) 

      - hexagon 1 (XMCDA label : h) 

      - hexagon 2 (XMCDA label : H) 

      - plus (XMCDA label : +) 

      - x (XMCDA label : x) 

      - thin diamond (XMCDA label : d) 

      - diamond (XMCDA label : D) 

      - vline (XMCDA label : |) 

      - hline (XMCDA label : _) 

      - plus filled (XMCDA label : P) 

      - x (filled) (XMCDA label : X) 

- **Name:** Linestyle

  *Linestyle used to plot lines.*

  - **Type:** drop-down list
  - **Possible values:**
      - solid line (XMCDA label : solid) 

      - dashed line (XMCDA label : dashed) (default)

      - dashed and dotted line (XMCDA label : dashdot) 

      - dotted line (XMCDA label : dotted) 

      - no line (XMCDA label : None) 

- **Name:** Image file extension

  *File extension of generated image figure.*

  - **Type:** drop-down list
  - **Possible values:**
      - .eps (Encapsulated PostScript) (XMCDA label : eps) 

      - .jpg (Joint Photographic Experts Group) (XMCDA label : jpg) 

      - .pdf (Portable Document Format) (XMCDA label : pdf) 

      - .pgf (Progressive Graphics File) (XMCDA label : pgf) 

      - .png (Portable Network Graphics) (XMCDA label : png) (default)

      - .ps (PostScript) (XMCDA label : ps) 

      - .raw (Raw RGBA bitmap) (XMCDA label : raw) 

      - .rgba (Silicon Graphics RGB) (XMCDA label : rgba) 

      - .svg (Scalable Vector Graphics) (XMCDA label : svg) 

      - .svgz (Compressed Scalable Vector Graphics) (XMCDA label : svgz) 

      - .tif (Tagged Image File Format) (XMCDA label : tif) 

- **Name:** Image file extension

  *File extension of generated image figure.*

  - **Type:** drop-down list
  - **Possible values:**
      - .eps (Encapsulated PostScript) (XMCDA label : eps) 

      - .jpg (Joint Photographic Experts Group) (XMCDA label : jpg) 

      - .pdf (Portable Document Format) (XMCDA label : pdf) 

      - .png (Portable Network Graphics) (XMCDA label : png) (default)

      - .svg (Scalable Vector Graphics) (XMCDA label : svg) 

- **Name:** Plotter

  *Plotter used to generate image.*

  - **Type:** drop-down list
  - **Possible values:**
      - Matplotlib (XMCDA label : matplotlib) (default)

      - Gnuplot (XMCDA label : gnuplot) 


XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
    <programParameters>
        <programParameter id="x_axis" name="X-axis label">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="y_axis" name="Y-axis label">
            <values>
                <value>
                    <label>%2</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="chart_title" name="Chart title">
            <values>
                <value>
                    <label>%3</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="color" name="Color">
            <values>
                <value>
                    <label>%4</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="marker" name="Marker">
            <values>
                <value>
                    <label>%5</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="linestyle" name="Linestyle">
            <values>
                <value>
                    <label>%6</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="image_file_extension" name="Image file extension">
            <values>
                <value>
                    <label>%7a%7b</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="plotter" name="Plotter">
            <values>
                <value>
                    <label>%8</label>
                </value>
            </values>
        </programParameter>
    </programParameters>


----------------------------



.. _plotXYAlternativesValues-PyXMCDA_outputs:

Outputs
-------


- :ref:`glob:XYalternativesValues.{bmp,dia,fig,gif,hpgl,ico,jpg,jpe,pdf,png,ps,ps2,svg,svgz,tif} <plotXYAlternativesValues-PyXMCDA-XYAlternativesValues>`
- :ref:`glob:plot_XYalternativesValues.{py,plt} <plotXYAlternativesValues-PyXMCDA-XYAlternativesValuesPlotScript>`
- :ref:`messages <plotXYAlternativesValues-PyXMCDA-messages>`

.. _plotXYAlternativesValues-PyXMCDA-XYAlternativesValues:

alternativesValues XY plot
~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

Image containing all selected values for XY alternativesValues plots. Format corresponds to the one given in parameters (default is .png).



XMCDA related:
..............

- **Tag:** None

----------------------------


.. _plotXYAlternativesValues-PyXMCDA-XYAlternativesValuesPlotScript:

alternativesValues XY plot script
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

Generated Python or Gnuplot script that made the image.
                Given to enable users to later customize the appearance of the plots.
                Extension is .py if matplotlib is used, .plt for gnuplot.



XMCDA related:
..............

- **Tag:** None

----------------------------


.. _plotXYAlternativesValues-PyXMCDA-messages:

messages
~~~~~~~~


Description:
............

Status messages.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
