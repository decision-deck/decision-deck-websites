:orphan:



.. _DEASMAACCRRanks-PUT:

DEASMAACCRRanks
===============

:Provider: PUT
:Version: 1.0

Description
-----------

Computes rankings for the given DMUs (alternatives) using SMAA-D method and CCR Data Envelopment Analysis Model. For given number of samples  returns a matrix with alternatives in each row and rankings in each column. Single cell indicates how many samples of respective alternative gave respective position in ranking.

- **Contact:** Malgorzata Napieraj (napieraj.malgorzata@gmail.com)

- **Reference:** Cooper W., Seiford L., Tone K., Data Envelopment Analysis: A Comprehensive Text with Models, Applications, References and DEA-Solver (2007).

- **Reference:** Lahdelma R., Salminen P., Stochastic multicriteria acceptability analysis using the data envelopment model (2004).



Inputs
------
(For outputs, see :ref:`below <DEASMAACCRRanks-PUT_outputs>`)


- :ref:`inputsOutputs <DEASMAACCRRanks-PUT-inputsOutputs>`
- :ref:`units <DEASMAACCRRanks-PUT-units>`
- :ref:`performanceTable <DEASMAACCRRanks-PUT-performanceTable>`
- :ref:`weightsLinearConstraints <DEASMAACCRRanks-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <DEASMAACCRRanks-PUT-methodParameters>`

.. _DEASMAACCRRanks-PUT-inputsOutputs:

inputsOutputs
~~~~~~~~~~~~~


Description:
............

A list of criteria with specified preference direction. List has to contains at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output).



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    <criteria>
                        <criterion>
							<scale>
								<quantitative>
									<preferenceDirection>
										[...]
									</preferenceDirection>
								</quantitative>
							</scale>
                            [...]
                        </criterion>
                        [...]
                    </criteria>

----------------------------


.. _DEASMAACCRRanks-PUT-units:

units
~~~~~


Description:
............

A list of alternatives (DMUs).



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>

----------------------------


.. _DEASMAACCRRanks-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~


Description:
............

A list of alternatives (DMUs) performances.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    <performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>

----------------------------


.. _DEASMAACCRRanks-PUT-weightsLinearConstraints:

weightsLinearConstraints
~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

A list of criteria linear constraints.



XMCDA related:
..............

- **Tag:** criteriaLinearConstraints

- **Code:**

  ::

    <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID>[...]</criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>

----------------------------


.. _DEASMAACCRRanks-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~


Description:
............

"samplesNo" represents the number of samples to generate.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** samplesNo

  

  - **Constraint description:** The value should be a positive integer.

  - **Type:** integer
  - **Default value:** 100

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    <methodParameters>
							<parameter name="samplesNo">
								<value><integer>%1</integer></value>
							</parameter>
					</methodParameters>

----------------------------



.. _DEASMAACCRRanks-PUT_outputs:

Outputs
-------


- :ref:`rankAcceptabilityIndices <DEASMAACCRRanks-PUT-rankAcceptabilityIndices>`
- :ref:`avgRank <DEASMAACCRRanks-PUT-avgRank>`
- :ref:`messages <DEASMAACCRRanks-PUT-messages>`

.. _DEASMAACCRRanks-PUT-rankAcceptabilityIndices:

rankAcceptabilityIndices
~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

A performance table for given alternatives. Single performance consists of attribute criterionID representing certain ranking, and a value representing ratio of samples attaining this ranking.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    <performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>Rank [...]</criterionID>
									<value>
									[...]
									</value>
							</performance>
							[...]
						</alternativePerformances>
					</performanceTable>

----------------------------


.. _DEASMAACCRRanks-PUT-avgRank:

avgRank
~~~~~~~


Description:
............

A list of alternatives with average rank (obtained for given sample).



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    <alternativesValues mcdaConcept="efficiency">
						<alternativeValue>
						  <alternativeID>[...]</alternativeID>
						  <value>
							[...]
						  </value>
						</alternativeValue>
						[...]
					</alternativesValues>

----------------------------


.. _DEASMAACCRRanks-PUT-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
