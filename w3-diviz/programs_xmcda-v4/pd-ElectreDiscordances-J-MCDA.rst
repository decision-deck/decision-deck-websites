:orphan:



.. _ElectreDiscordances-J-MCDA:

ElectreDiscordances
===================

:Provider: J-MCDA
:Version: 0.5.5

Description
-----------

Computes a discordance relation per criteria.

- **Contact:** Olivier Cailloux <olivier.cailloux@ecp.fr>

- **Web page:** http://sourceforge.net/projects/j-mcda/

- **Reference:** Cailloux, Olivier. Electre and Promethee MCDA methods as reusable software components. In Proceedings of the 25th Mini-EURO Conference on Uncertainty and Robustness in Planning and Decision Making (URPDM 2010). Coimbra, Portugal, 2010.



Inputs
------
(For outputs, see :ref:`below <ElectreDiscordances-J-MCDA_outputs>`)


- :ref:`criteria <ElectreDiscordances-J-MCDA-input1>`
- :ref:`alternatives <ElectreDiscordances-J-MCDA-input0>` *(optional)*
- :ref:`criteriaScales <ElectreDiscordances-J-MCDA-criteriaScales>`
- :ref:`criteriaThresholds <ElectreDiscordances-J-MCDA-criteriaThresholds>` *(optional)*
- :ref:`performances <ElectreDiscordances-J-MCDA-input2>`

.. _ElectreDiscordances-J-MCDA-input1:

criteria
~~~~~~~~


Description:
............

The criteria to consider, possibly with preference and veto thresholds (provided separated via the input 'criteriaThresholds'). Each one must have a preference direction (see input 'criteriaScales'). Set some criteria as inactive (or remove them) to ignore them.



XMCDA related:
..............

- **Tag:** criteria

----------------------------


.. _ElectreDiscordances-J-MCDA-input0:

alternatives
~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The alternatives to consider. Set some alternatives as inactive (or remove them) to ignore them.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _ElectreDiscordances-J-MCDA-criteriaScales:

criteria scales
~~~~~~~~~~~~~~~


Description:
............

The scales of the criteria to consider.



XMCDA related:
..............

- **Tag:** criteriaScales

----------------------------


.. _ElectreDiscordances-J-MCDA-criteriaThresholds:

criteria thresholds
~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The criteria' preference and indifference thresholds.



XMCDA related:
..............

- **Tag:** criteriaThresholds

----------------------------


.. _ElectreDiscordances-J-MCDA-input2:

performances
~~~~~~~~~~~~


Description:
............

The performances of the alternatives on the criteria to consider.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------



.. _ElectreDiscordances-J-MCDA_outputs:

Outputs
-------


- :ref:`discordances <ElectreDiscordances-J-MCDA-output0>`
- :ref:`messages <ElectreDiscordances-J-MCDA-output1>`

.. _ElectreDiscordances-J-MCDA-output0:

discordances
~~~~~~~~~~~~


Description:
............

The discordances relations (one relation per criterion) computed from the given input data.



XMCDA related:
..............

- **Tag:** alternativesMatrix

----------------------------


.. _ElectreDiscordances-J-MCDA-output1:

messages
~~~~~~~~


Description:
............

A status message.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
