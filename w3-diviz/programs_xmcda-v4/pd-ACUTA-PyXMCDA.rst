
:orphan:

.. _ACUTA-PyXMCDA:

ACUTA
=====

:Provider: PyXMCDA
:Version: 1.0

Description
-----------

Perform ACUTA computation on provided inputs.
Decision Maker's Preferences can be provided using either a global ranking of reference alternatives, or the pair-wise alternatives preferences and indifferences.
Outputs optimal valueFunctions along their valuesErrors.

The service generate discrete functions for criteria with qualitative scales.
Number of segments will be still be used to compute a continuous marginal utility function.
It is therefore advised to use (n - 1) as the number of segments (with n the number of labels of the qualitative scale), while using incremented integer values in such scale definition (e.g. \{ 1, 2, 3,..., n\}).
Labels of the scale are used as the abscissa.

This service raises an error if the solution found does not give the same ranking of reference alternatives than in inputs (even with application of the values errors, called sigma in (Munda, 2005).

Also it outputs the UTA* optimum solution if analytic center computation fails.

The implementation and indexing conventions are based on:

    Munda, G. (2005). UTA Methods.
    In Multiple criteria decision analysis: State of the art surveys
    (pp 297-343). Springer, New York, NY.

N.B: This service uses the python module Pulp for representing and solving UTA problems.

- **Contact:** Nicolas Duminy (nicolas.duminy@telecom-bretagne.eu)

- **Web page:** https://gitlab.com/nduminy/ws-pyxmcda



Inputs
------
(For outputs, see :ref:`below <ACUTA-PyXMCDA_outputs>`)


- :ref:`alternatives <ACUTA-PyXMCDA-alternatives>` *(optional)*
- :ref:`criteria <ACUTA-PyXMCDA-criteria>` *(optional)*
- :ref:`performanceTable <ACUTA-PyXMCDA-performanceTable>`
- :ref:`alternativesPreferences <ACUTA-PyXMCDA-alternativesPreferences>` *(optional)*
- :ref:`alternativesIndifferences <ACUTA-PyXMCDA-alternativesIndifferences>` *(optional)*
- :ref:`alternativesRanks <ACUTA-PyXMCDA-alternativesRanks>` *(optional)*
- :ref:`criteriaScales <ACUTA-PyXMCDA-criteriaScales>` *(optional)*
- :ref:`criteriaSegments <ACUTA-PyXMCDA-criteriaSegments>`
- :ref:`parameters <ACUTA-PyXMCDA-parameters>`

.. _ACUTA-PyXMCDA-alternatives:

alternatives
~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The alternatives. Only used to exclude inactive alternatives from computations (all are considered if not provided).



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _ACUTA-PyXMCDA-criteria:

criteria
~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The criteria. Only used to exclude inactive criteria from computations (all are considered if not provided).



XMCDA related:
..............

- **Tag:** criteria

----------------------------


.. _ACUTA-PyXMCDA-performanceTable:

performance table
~~~~~~~~~~~~~~~~~


Description:
............

The performance table.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _ACUTA-PyXMCDA-alternativesPreferences:

alternatives preferences
~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The alternatives preferences used to infer utility functions.
                If alternatives is provided, all reference alternatives set as inactive are not considered.



XMCDA related:
..............

- **Tag:** alternativesMatrix

----------------------------


.. _ACUTA-PyXMCDA-alternativesIndifferences:

alternatives indifferences
~~~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The alternatives indifferences used to infer utility functions.
                If alternatives is provided, all reference alternatives set as inactive are not considered.



XMCDA related:
..............

- **Tag:** alternativesMatrix

----------------------------


.. _ACUTA-PyXMCDA-alternativesRanks:

alternatives ranks
~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

Ranking of reference alternatives used to infer utility functions.
                If alternatives is provided, all reference alternatives set as inactive are not considered.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _ACUTA-PyXMCDA-criteriaScales:

criteria scales
~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The criteria scales. Mandatory if qualitative values are in performance table.
                If not provided, minimum/maximum values will be extracted from performance table,
                and preference direction will be set to max.
                Nominal and fuzzy qualitative scales are forbidden.



XMCDA related:
..............

- **Tag:** criteriaScales

----------------------------


.. _ACUTA-PyXMCDA-criteriaSegments:

criteria segments
~~~~~~~~~~~~~~~~~


Description:
............

Number of segments per marginal utility function.



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _ACUTA-PyXMCDA-parameters:

parameters
~~~~~~~~~~


Description:
............

Parameters of the method



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** Inputs alternatives

  *How to provide alternatives hierarchy*

  - **Type:** drop-down list
  - **Possible values:**
      - Use ranking of alternatives (XMCDA label : ranking) (default)

      - Use pairwise preferences and indifferences (XMCDA label : relations) 

- **Name:** Discrimination threshold

  *Discrimination threshold value.*

  - **Constraint description:** Bigger than 0.0.

  - **Type:** float
  - **Default value:** 0.001
- **Name:** Monotonicity threshold

  *Monotonicity threshold value.*

  - **Constraint description:** Bigger than or equal to 0.0.

  - **Type:** float
  - **Default value:** 0.001
- **Name:** Analytic center epsilon

  *Absolute tolerance for analytic center computation (closeness to optimal solution).*

  - **Constraint description:** Bigger than 0.0.

  - **Type:** float
  - **Default value:** 1e-6
- **Name:** Newton step coefficient

  *Coefficient applied to compute newton steps in analytic center computation.*

  - **Constraint description:** Bigger than 0.0.

  - **Type:** float
  - **Default value:** 0.9
- **Name:** Significative figures

  *Number of significative figures in outputs values.*

  - **Constraint description:** Bigger than 0.

  - **Type:** integer
  - **Default value:** 3
- **Name:** Absolute tolerance

  *Float absolute tolerance used when computing Kendall tau.*

  - **Constraint description:** Bigger than 0.0.

  - **Type:** float
  - **Default value:** 1e-6
- **Name:** Solver

  *Which solver is used.*

  - **Type:** drop-down list
  - **Possible values:**
      - Coin-Or CBC (version included in Pulp) (XMCDA label : cbc) (default)

      - Choco (version included in Pulp) (XMCDA label : choco) 

      - GLPK (XMCDA label : glpk) 


XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
    <programParameters>
        <!-- %N -->
        <programParameter id="discrimination_threshold" name="Discrimination threshold">
	        <values>
                <value>
		            <real>%2</real>
                </value>
	        </values>
        </programParameter>
        <programParameter id="monotonicity_threshold" name="Monotonicity threshold">
	        <values>
                <value>
		            <real>%3</real>
                </value>
	        </values>
        </programParameter>
        <programParameter id="ac_epsilon" name="Analytic center epsilon">
	        <values>
                <value>
		            <real>%7</real>
                </value>
	        </values>
        </programParameter>
        <programParameter id="newton_step_coeff" name="Newton step coefficient">
	        <values>
                <value>
		            <real>%8</real>
                </value>
	        </values>
        </programParameter>
        <programParameter id="significative_figures" name="Significative figures">
	        <values>
                <value>
		            <integer>%5</integer>
                </value>
	        </values>
        </programParameter>
        <programParameter id="atol" name="Absolute tolerance">
	        <values>
                <value>
		            <real>%6</real>
                </value>
	        </values>
        </programParameter>
        <programParameter id="solver" name="Solver">
	        <values>
                <value>
		            <label>%4</label>
                </value>
	        </values>
        </programParameter>
    </programParameters>


----------------------------



.. _ACUTA-PyXMCDA_outputs:

Outputs
-------


- :ref:`valueFunctions <ACUTA-PyXMCDA-valueFunctions>`
- :ref:`valuesErrors <ACUTA-PyXMCDA-valuesErrors>`
- :ref:`messages <ACUTA-PyXMCDA-messages>`

.. _ACUTA-PyXMCDA-valueFunctions:

value functions
~~~~~~~~~~~~~~~


Description:
............

Optimal value functions found by the service.



XMCDA related:
..............

- **Tag:** criteriaFunctions

----------------------------


.. _ACUTA-PyXMCDA-valuesErrors:

values errors
~~~~~~~~~~~~~


Description:
............

Optimal values errors found by the service for the reference alternatives.
                The 2 values for each alternatives are respectively sigma+ and sigma-.
                As noted in (Munda, 2005).



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _ACUTA-PyXMCDA-messages:

messages
~~~~~~~~


Description:
............

Status messages.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
