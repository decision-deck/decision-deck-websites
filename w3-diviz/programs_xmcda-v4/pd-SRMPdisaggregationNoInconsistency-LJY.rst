:orphan:



.. _SRMPdisaggregationNoInconsistency-LJY:

S-RMP Disaggregation without inconsistency
==========================================

:Provider: LJY
:Version: 1.2

Description
-----------

Elicit a S-RMP model from a set of pairwise comparisons without inconsistency. The algorithm will find out a S-RMP model which is able to exactly represent all the input pairwise comparisons (if it exists). The elicitation may be infeasible due to the unknown input inconsistency.

- **Contact:** Jinyan Liu (jinyan.liu@ecp.fr)

- **Web page:** http://www.lgi.ecp.fr/pmwiki.php/PagesPerso/JinyanLiu

- **Reference:** Zheng, J., Rolland, A. & Mousseau, V., 2012. Inferring a reference based multicriteria ranking model from pairwise comparisons. European Journal of Operational Research.

- **Reference:** Jun Zheng. Preference elicitation for aggregation models based on reference points: algorithms and procedures. These PhD. May, 2012.



Inputs
------
(For outputs, see :ref:`below <SRMPdisaggregationNoInconsistency-LJY_outputs>`)


- :ref:`criteria <SRMPdisaggregationNoInconsistency-LJY-inc>`
- :ref:`alternatives <SRMPdisaggregationNoInconsistency-LJY-ina>`
- :ref:`performanceTable <SRMPdisaggregationNoInconsistency-LJY-inperf>`
- :ref:`alternativesComparisons <SRMPdisaggregationNoInconsistency-LJY-inac>`

.. _SRMPdisaggregationNoInconsistency-LJY-inc:

criteria
~~~~~~~~


Description:
............

A list of criteria involved in the ranking problem. For each criterion, the preference direction should be provided. Besides, the maximum and minimum should also be included.



XMCDA related:
..............

- **Tag:** criteria

----------------------------


.. _SRMPdisaggregationNoInconsistency-LJY-ina:

alternatives
~~~~~~~~~~~~


Description:
............

A complete list of alternatives to be considered in the ranking problem. At least, all the alternatives appear in the provided pairwise comparisons should be included.



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    
			

----------------------------


.. _SRMPdisaggregationNoInconsistency-LJY-inperf:

performance table
~~~~~~~~~~~~~~~~~


Description:
............

Values of criteria for each alternatives defined in the alternatives list.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _SRMPdisaggregationNoInconsistency-LJY-inac:

pairwise comparisons between alternatives
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The pairwise comparisons of alternatives provided by the decision maker.



XMCDA related:
..............

- **Tag:** alternativesComparisons

----------------------------



.. _SRMPdisaggregationNoInconsistency-LJY_outputs:

Outputs
-------


- :ref:`profileConfigs <SRMPdisaggregationNoInconsistency-LJY-outrp>`
- :ref:`weights <SRMPdisaggregationNoInconsistency-LJY-outw>`
- :ref:`lexicography <SRMPdisaggregationNoInconsistency-LJY-outlexi>`
- :ref:`messages <SRMPdisaggregationNoInconsistency-LJY-msg>`

.. _SRMPdisaggregationNoInconsistency-LJY-outrp:

reference points
~~~~~~~~~~~~~~~~


Description:
............

A set of reference points (special alternatives) with their performance values on each criteria. For each reference point, the id number indicates its lexicographic order.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _SRMPdisaggregationNoInconsistency-LJY-outw:

criteria weights
~~~~~~~~~~~~~~~~


Description:
............

The weights of criteria. For each criteria, the value of weight is strictly positive and less than 0.49. The sum of weights is normalized to 1.



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _SRMPdisaggregationNoInconsistency-LJY-outlexi:

lexicographic order
~~~~~~~~~~~~~~~~~~~


Description:
............

the lexicographic order of the reference points we use during aggregation.



XMCDA related:
..............

- **Tag:** alternativesValues

----------------------------


.. _SRMPdisaggregationNoInconsistency-LJY-msg:

messages
~~~~~~~~


Description:
............

Some status messages.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
