
:orphan:

.. _plotCriteriaMatrix-PyXMCDA:

plotCriteriaMatrix
==================

:Provider: PyXMCDA
:Version: 1.1

Description
-----------

Generate graph representing provided criteriaMatrix as well as the dot files generating this graph.
Values associated to transitions can be shown without any preprocessing.
Graphviz dot program and format are used by this service.

- **Contact:** Nicolas Duminy (nicolas.duminy@telecom-bretagne.eu)

- **Web page:** https://gitlab.com/nduminy/ws-pyxmcda



Inputs
------
(For outputs, see :ref:`below <plotCriteriaMatrix-PyXMCDA_outputs>`)


- :ref:`criteria <plotCriteriaMatrix-PyXMCDA-criteria>` *(optional)*
- :ref:`criteriaMatrix <plotCriteriaMatrix-PyXMCDA-criteriaMatrix>`
- :ref:`parameters <plotCriteriaMatrix-PyXMCDA-parameters>` *(optional)*

.. _plotCriteriaMatrix-PyXMCDA-criteria:

criteria
~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The criteria to be plotted. All are plotted if not provided.



XMCDA related:
..............

- **Tag:** criteria

----------------------------


.. _plotCriteriaMatrix-PyXMCDA-criteriaMatrix:

criteria matrix
~~~~~~~~~~~~~~~


Description:
............

The criteria matrix to be plotted.



XMCDA related:
..............

- **Tag:** criteriaMatrix

----------------------------


.. _plotCriteriaMatrix-PyXMCDA-parameters:

parameters
~~~~~~~~~~


Description:
............

Parameters of the method



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** Color

  *Color used in the plots.*

  - **Type:** drop-down list
  - **Possible values:**
      - Black (XMCDA label : black) (default)

      - White (XMCDA label : white) 

      - Red (XMCDA label : red) 

      - Green (XMCDA label : green) 

      - Blue (XMCDA label : blue) 

      - Magenta (XMCDA label : magenta) 

      - Yellow (XMCDA label : yellow) 

      - Cyan (XMCDA label : cyan) 

      - Orange (XMCDA label : orange) 

      - Pink (XMCDA label : pink) 

      - Brown (XMCDA label : brown) 

      - Gray (XMCDA label : gray) 

- **Name:** Shape

  *Shape use to represent categories.*

  - **Type:** drop-down list
  - **Possible values:**
      - Rectangle (XMCDA label : box) 

      - Circle (XMCDA label : circle) 

      - Diamond (XMCDA label : diamond) 

      - Ellipse (XMCDA label : ellipse) 

      - Oval (XMCDA label : oval) (default)

      - Polygon (XMCDA label : polygon) 

      - Triangle (XMCDA label : triangle) 

- **Name:** Image file extension

  *File extension of generated image figure.*

  - **Type:** drop-down list
  - **Possible values:**
      - .bmp (Windows Bitmap Format) (XMCDA label : bmp) 

      - .dia (DIA Format) (XMCDA label : dia) 

      - .fig (FIG graphics format) (XMCDA label : fig) 

      - .gif (Graphics Interchange Format) (XMCDA label : gif) 

      - .hpgl (Hewlett Packard Graphic Language 2) (XMCDA label : hpgl) 

      - .ico (Icon Image File Format) (XMCDA label : ico) 

      - .jpg (Joint Photographic Experts Group) (XMCDA label : jpg) 

      - .jpe (Joint Photographic Experts Group) (XMCDA label : jpe) 

      - .pdf (Portable Document Format) (XMCDA label : pdf) 

      - .png (Portable Network Graphics) (XMCDA label : png) (default)

      - .ps (PostScript) (XMCDA label : ps) 

      - .ps2 (PostScript for PDF) (XMCDA label : ps2) 

      - .svg (Scalable Vector Graphics) (XMCDA label : svg) 

      - .svgz (Compressed Scalable Vector Graphics) (XMCDA label : svgz) 

      - .tif (Tagged Image File Format) (XMCDA label : tif) 

- **Name:** Show values

  *Defines if values are shown (between parentheses) on the plots or not.*

  - **Default value:** false
- **Name:** Directed graph

  *Defines if graph representing alternatives matrix is directed or not.*

  - **Type:** drop-down list
  - **Possible values:**
      - Directed (XMCDA label : true) (default)

      - Undirected (XMCDA label : false) 

- **Name:** Transitive reduction

  *Defines if transitive reduction is applied or not.*

  - **Default value:** false
- **Name:** Transitive closure

  *Defines if transitive closure is applied or not.*

  - **Default value:** false
- **Name:** Naming conventions

  *How categories and alternatives are labelled on the graph.*

  - **Type:** drop-down list
  - **Possible values:**
      - Only ids are shown (XMCDA label : id) 

      - Only names are shown (disambiguated by appending the ids, if needed) (XMCDA label : name) (default)

      - Names and ids are shown in that order (XMCDA label : name (id)) 

      - Ids and names are shown in that order (XMCDA label : id (name)) 


XMCDA related:
..............

- **Tag:** programParameters

- **Code:**

  ::

    
    <programParameters>
        <programParameter id="color" name="Color">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="shape" name="Shape">
            <values>
                <value>
                    <label>%2</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="image_file_extension" name="Image file extension">
            <values>
                <value>
                    <label>%3</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="show_values" name="Show values">
            <values>
                <value>
                    <boolean>%4</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="directed_graph" name="Directed graph">
            <values>
                <value>
                    <boolean>%7</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="transitive_reduction" name="Transitive reduction">
            <values>
                <value>
                    <boolean>%5</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="transitive_closure" name="Transitive closure">
            <values>
                <value>
                    <boolean>%6</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="naming_conventions" name="Naming conventions">
            <values>
                <value>
                    <label>%8</label>
                </value>
            </values>
        </programParameter>
    </programParameters>


----------------------------



.. _plotCriteriaMatrix-PyXMCDA_outputs:

Outputs
-------


- :ref:`glob:criteriaMatrix.{bmp,dia,fig,gif,hpgl,ico,jpg,jpe,pdf,png,ps,ps2,svg,svgz,tif} <plotCriteriaMatrix-PyXMCDA-criteriaMatrixPlot>`
- :ref:`criteriaMatrix.dot <plotCriteriaMatrix-PyXMCDA-criteriaMatrixPlotScript>`
- :ref:`messages <plotCriteriaMatrix-PyXMCDA-messages>`

.. _plotCriteriaMatrix-PyXMCDA-criteriaMatrixPlot:

criteria matrix plot
~~~~~~~~~~~~~~~~~~~~


Description:
............

Image containing criteria matrix graph. Format corresponds to the one given in parameters (default is .png).



XMCDA related:
..............

- **Tag:** None

----------------------------


.. _plotCriteriaMatrix-PyXMCDA-criteriaMatrixPlotScript:

criteria matrix plot script
~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

Generated graphviz dot script that made the image. Given to enable users to later customize the appearance of the graph.



XMCDA related:
..............

- **Tag:** None

----------------------------


.. _plotCriteriaMatrix-PyXMCDA-messages:

messages
~~~~~~~~


Description:
............

Status messages.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
