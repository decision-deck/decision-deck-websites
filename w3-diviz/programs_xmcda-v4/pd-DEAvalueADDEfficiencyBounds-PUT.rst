:orphan:



.. _DEAvalueADDEfficiencyBounds-PUT:

DEAvalueADDEfficiencyBounds
===========================

:Provider: PUT
:Version: 1.0

Description
-----------

Computes efficiency bounds for the given DMUs (alternatives) using additive Data Envelopment Analysis Model.

- **Contact:** Malgorzata Napieraj (napieraj.malgorzata@gmail.com)

- **Reference:** Gouveia M. C., Dias L. C., Antunes C. H., Super-efficiency and stability intervals in additive DEA (2012).



Inputs
------
(For outputs, see :ref:`below <DEAvalueADDEfficiencyBounds-PUT_outputs>`)


- :ref:`inputsOutputs <DEAvalueADDEfficiencyBounds-PUT-inputsOutputs>`
- :ref:`units <DEAvalueADDEfficiencyBounds-PUT-units>`
- :ref:`performanceTable <DEAvalueADDEfficiencyBounds-PUT-performanceTable>`
- :ref:`weightsLinearConstraints <DEAvalueADDEfficiencyBounds-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <DEAvalueADDEfficiencyBounds-PUT-methodParameters>`

.. _DEAvalueADDEfficiencyBounds-PUT-inputsOutputs:

inputsOutputs
~~~~~~~~~~~~~


Description:
............

A list of criteria with specified preference direction. List has to contains at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output). In addition, minimum and maximum possible value for each critetion can be defined. If not defined, they will be computed from alternatives performances on given criteria.



XMCDA related:
..............

- **Tag:** criteria

- **Code:**

  ::

    <criteria>
                        <criterion>
							<scale>
								<quantitative>
									<preferenceDirection>
										[...]
									</preferenceDirection>
								</quantitative>
							</scale>
							<minimum>[...]</minimum>
							<maximum>[...]</maximum>
                            [...]
                        </criterion>
                        [...]
                    </criteria>

----------------------------


.. _DEAvalueADDEfficiencyBounds-PUT-units:

units
~~~~~


Description:
............

A list of alternatives (DMUs).



XMCDA related:
..............

- **Tag:** alternatives

- **Code:**

  ::

    <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>

----------------------------


.. _DEAvalueADDEfficiencyBounds-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~


Description:
............

A list of alternatives (DMUs) performances.



XMCDA related:
..............

- **Tag:** performanceTable

- **Code:**

  ::

    <performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>

----------------------------


.. _DEAvalueADDEfficiencyBounds-PUT-weightsLinearConstraints:

weightsLinearConstraints
~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, disabled by default

Description:
............

A list of criteria linear constraints.



XMCDA related:
..............

- **Tag:** criteriaLinearConstraints

- **Code:**

  ::

    <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID>[...]</criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>

----------------------------


.. _DEAvalueADDEfficiencyBounds-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~


Description:
............

"boundariesProvided" indicates if criteria values boundaries are given in criteria input or they have to be computed based on alternatives performances. "transformToUtilites" indicates if given alternatives values are utilities or if they have to be firstly transformed to utilities.



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** boundariesProvided

  

  - **Default value:** false
- **Name:** transformToUtilities

  

  - **Default value:** true

XMCDA related:
..............

- **Tag:** methodParameters

- **Code:**

  ::

    <methodParameters>
							<parameter name="boundariesProvided">
								<value><boolean>%1</boolean></value>
							</parameter>
							<parameter name="transformToUtilities">
								<value><boolean>%2</boolean></value>
							</parameter>
					</methodParameters>

----------------------------



.. _DEAvalueADDEfficiencyBounds-PUT_outputs:

Outputs
-------


- :ref:`minEfficiency <DEAvalueADDEfficiencyBounds-PUT-minEfficiency>`
- :ref:`minDistance <DEAvalueADDEfficiencyBounds-PUT-minDistance>`
- :ref:`maxEfficiency <DEAvalueADDEfficiencyBounds-PUT-maxEfficiency>`
- :ref:`maxDistance <DEAvalueADDEfficiencyBounds-PUT-maxDistance>`
- :ref:`messages <DEAvalueADDEfficiencyBounds-PUT-messages>`

.. _DEAvalueADDEfficiencyBounds-PUT-minEfficiency:

minEfficiency
~~~~~~~~~~~~~


Description:
............

A list of alternatives with computed minimum efficiency scores.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    <alternativesValues mcdaConcept="minEfficiency">
						<alternativeValue>
						  <alternativeID>[...]</alternativeID>
						  <value>
							[...]
						  </value>
						</alternativeValue>
						[...]
					</alternativesValues>

----------------------------


.. _DEAvalueADDEfficiencyBounds-PUT-minDistance:

minDistance
~~~~~~~~~~~


Description:
............

A list of alternatives with computed minimum distances from efficient DMUs.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    <alternativesValues mcdaConcept="minDistance">
						<alternativeValue>
						  <alternativeID>[...]</alternativeID>
						  <value>
							[...]
						  </value>
						</alternativeValue>
						[...]
					</alternativesValues>

----------------------------


.. _DEAvalueADDEfficiencyBounds-PUT-maxEfficiency:

maxEfficiency
~~~~~~~~~~~~~


Description:
............

A list of alternatives with computed maximum efficiency scores.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    <alternativesValues mcdaConcept="maxEfficiency">
						<alternativeValue>
						  <alternativeID>[...]</alternativeID>
						  <value>
							[...]
						  </value>
						</alternativeValue>
						[...]
					</alternativesValues>

----------------------------


.. _DEAvalueADDEfficiencyBounds-PUT-maxDistance:

maxDistance
~~~~~~~~~~~


Description:
............

A list of alternatives with computed maximum distances from efficient DMUs.



XMCDA related:
..............

- **Tag:** alternativesValues

- **Code:**

  ::

    <alternativesValues mcdaConcept="maxDistance">
						<alternativeValue>
						  <alternativeID>[...]</alternativeID>
						  <value>
							[...]
						  </value>
						</alternativeValue>
						[...]
					</alternativesValues>

----------------------------


.. _DEAvalueADDEfficiencyBounds-PUT-messages:

messages
~~~~~~~~


Description:
............

A list of messages generated by the algorithm.



XMCDA related:
..............

- **Tag:** methodMessages

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
