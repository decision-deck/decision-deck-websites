:orphan:



.. _MRSort-LargePerfDiff_assignment-R-MCDA:

MRSort with large performance differences
=========================================

:Provider: R-MCDA
:Version: 1.0

Description
-----------

MRSort is a simplified ELECTRE TRI sorting method, where alternatives are assigned to an ordered set of categories. In this case, we also take into account large performance differences, both negative (vetoes) and positive (dictators).

- **Contact:** Alexandru Olteanu (alexandru.olteanu@univ-ubs.fr)

- **Reference:** P. MEYER, A-L. OLTEANU, Integrating large positive and negative performance differences into multicriteria majority-rule sorting models, Computers and Operations Research, 81, pp. 216 - 230, 2017.



Inputs
------
(For outputs, see :ref:`below <MRSort-LargePerfDiff_assignment-R-MCDA_outputs>`)


- :ref:`alternatives <MRSort-LargePerfDiff_assignment-R-MCDA-inalt>`
- :ref:`categories <MRSort-LargePerfDiff_assignment-R-MCDA-incateg>`
- :ref:`performanceTable <MRSort-LargePerfDiff_assignment-R-MCDA-inperf>`
- :ref:`categoriesProfilesPerformanceTable <MRSort-LargePerfDiff_assignment-R-MCDA-incatprofpt>`
- :ref:`vetoProfilesPerformanceTable <MRSort-LargePerfDiff_assignment-R-MCDA-invetoprofpt>` *(optional)*
- :ref:`dictatorProfilesPerformanceTable <MRSort-LargePerfDiff_assignment-R-MCDA-indictprofpt>` *(optional)*
- :ref:`criteria <MRSort-LargePerfDiff_assignment-R-MCDA-incrit>`
- :ref:`criteriaWeights <MRSort-LargePerfDiff_assignment-R-MCDA-weights>`
- :ref:`categoriesProfiles <MRSort-LargePerfDiff_assignment-R-MCDA-incatprof>`
- :ref:`vetoProfiles <MRSort-LargePerfDiff_assignment-R-MCDA-invetoprof>` *(optional)*
- :ref:`dictatorProfiles <MRSort-LargePerfDiff_assignment-R-MCDA-indictprof>` *(optional)*
- :ref:`categoriesRanks <MRSort-LargePerfDiff_assignment-R-MCDA-incategval>`
- :ref:`majorityThreshold <MRSort-LargePerfDiff_assignment-R-MCDA-majorityThreshold>`
- :ref:`assignmentRule <MRSort-LargePerfDiff_assignment-R-MCDA-assignmentRule>`

.. _MRSort-LargePerfDiff_assignment-R-MCDA-inalt:

alternatives
~~~~~~~~~~~~


Description:
............

A complete list of alternatives to be considered by the MR-Sort method.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _MRSort-LargePerfDiff_assignment-R-MCDA-incateg:

categories
~~~~~~~~~~


Description:
............

A list of categories to which the alternatives will be assigned.



XMCDA related:
..............

- **Tag:** categories

----------------------------


.. _MRSort-LargePerfDiff_assignment-R-MCDA-inperf:

performance table
~~~~~~~~~~~~~~~~~


Description:
............

The evaluations of the alternatives on the set of criteria.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _MRSort-LargePerfDiff_assignment-R-MCDA-incatprofpt:

categories profiles performance table
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The evaluations of the category profiles.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _MRSort-LargePerfDiff_assignment-R-MCDA-invetoprofpt:

veto profiles performance table
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The evaluations of the veto profiles.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _MRSort-LargePerfDiff_assignment-R-MCDA-indictprofpt:

dictator profiles performance table
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The evaluations of the dictator profiles.



XMCDA related:
..............

- **Tag:** performanceTable

----------------------------


.. _MRSort-LargePerfDiff_assignment-R-MCDA-incrit:

criteria scales
~~~~~~~~~~~~~~~


Description:
............

A list of criteria on which the alternatives are evaluated. For each criterion, the preference direction should be provided.



XMCDA related:
..............

- **Tag:** criteriaScales

----------------------------


.. _MRSort-LargePerfDiff_assignment-R-MCDA-weights:

criteria weights
~~~~~~~~~~~~~~~~


Description:
............

The criteria weights.



XMCDA related:
..............

- **Tag:** criteriaValues

----------------------------


.. _MRSort-LargePerfDiff_assignment-R-MCDA-incatprof:

categories profiles
~~~~~~~~~~~~~~~~~~~


Description:
............

The categories delimiting profiles.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _MRSort-LargePerfDiff_assignment-R-MCDA-invetoprof:

veto profiles
~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The categories veto profiles.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _MRSort-LargePerfDiff_assignment-R-MCDA-indictprof:

dictator profiles
~~~~~~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The categories dictator profiles.



XMCDA related:
..............

- **Tag:** categoriesProfiles

----------------------------


.. _MRSort-LargePerfDiff_assignment-R-MCDA-incategval:

categories ranks
~~~~~~~~~~~~~~~~


Description:
............

A list of categories ranks, 1 stands for the most preferred category and the higher the number the lower the preference for that category.



XMCDA related:
..............

- **Tag:** categoriesValues

----------------------------


.. _MRSort-LargePerfDiff_assignment-R-MCDA-majorityThreshold:

majority threshold
~~~~~~~~~~~~~~~~~~


Description:
............

The majority threshold.



XMCDA related:
..............

- **Tag:** programParameters

----------------------------


.. _MRSort-LargePerfDiff_assignment-R-MCDA-assignmentRule:

assignment rule
~~~~~~~~~~~~~~~


Description:
............

The type of assignment rule. Can be anything from the list M (majority rule), V (veto), D (dictator), v (veto weakened by dictator), d (dictator weakened by veto), dV (dominating veto and weakened dictator), Dv (dominating dictator and weakened veto) and dv (conflicting veto and dictator).



GUI information:
................

- Parameter values can be defined via the GUI or the XMCDA file, by default via GUI.

- **Name:** Assignment rule

  *Indicates the type of assignment rule to be used.*

  - **Type:** drop-down list
  - **Possible values:**
      - Majority rule (XMCDA label : M) (default)

      - Veto (XMCDA label : V) 

      - Dictator (XMCDA label : D) 

      - Veto weakened by dictator (XMCDA label : v) 

      - Dictator weakened by veto (XMCDA label : d) 

      - Dominating Veto and weakened Dictator (XMCDA label : dV) 

      - Dominating Dictator and weakened veto (XMCDA label : Dv) 

      - Conflicting Veto and Dictator (XMCDA label : dv) 


XMCDA related:
..............

- **Tag:** programParameters

----------------------------



.. _MRSort-LargePerfDiff_assignment-R-MCDA_outputs:

Outputs
-------


- :ref:`alternativesAssignments <MRSort-LargePerfDiff_assignment-R-MCDA-outaffect>`
- :ref:`messages <MRSort-LargePerfDiff_assignment-R-MCDA-msg>`

.. _MRSort-LargePerfDiff_assignment-R-MCDA-outaffect:

alternatives assignments
~~~~~~~~~~~~~~~~~~~~~~~~


Description:
............

The alternatives assignments to categories.



XMCDA related:
..............

- **Tag:** alternativesAssignments

----------------------------


.. _MRSort-LargePerfDiff_assignment-R-MCDA-msg:

messages
~~~~~~~~


Description:
............

Messages from the execution of the webservice. Possible errors in the input data will be given here.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
