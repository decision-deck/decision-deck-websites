:orphan:



.. _ElectreOutranking-J-MCDA:

ElectreOutranking
=================

:Provider: J-MCDA
:Version: 0.5.5

Description
-----------

Computes an outranking relation.

- **Contact:** Olivier Cailloux <olivier.cailloux@ecp.fr>

- **Web page:** http://sourceforge.net/projects/j-mcda/

- **Reference:** Cailloux, Olivier. Electre and Promethee MCDA methods as reusable software components. In Proceedings of the 25th Mini-EURO Conference on Uncertainty and Robustness in Planning and Decision Making (URPDM 2010). Coimbra, Portugal, 2010.



Inputs
------
(For outputs, see :ref:`below <ElectreOutranking-J-MCDA_outputs>`)


- :ref:`criteria <ElectreOutranking-J-MCDA-input1>` *(optional)*
- :ref:`alternatives <ElectreOutranking-J-MCDA-input0>` *(optional)*
- :ref:`discordances <ElectreOutranking-J-MCDA-input2>`
- :ref:`concordance <ElectreOutranking-J-MCDA-input3>`

.. _ElectreOutranking-J-MCDA-input1:

criteria
~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The criteria to consider, only used as filter. Set some criteria as inactive (or remove them) to ignore them. Please note that these criteria will only (possibly) filter out corresponding discordances. The concordances must have been computed with the same set of criteria, otherwise the results will be meaningless.



XMCDA related:
..............

- **Tag:** criteria

----------------------------


.. _ElectreOutranking-J-MCDA-input0:

alternatives
~~~~~~~~~~~~

**Optional:** yes, enabled by default

Description:
............

The alternatives to consider. Set some alternatives as inactive (or remove them) to ignore them.



XMCDA related:
..............

- **Tag:** alternatives

----------------------------


.. _ElectreOutranking-J-MCDA-input2:

discordances
~~~~~~~~~~~~


Description:
............

The discordance relations to use (one per criterion).



XMCDA related:
..............

- **Tag:** alternativesMatrix

----------------------------


.. _ElectreOutranking-J-MCDA-input3:

concordance
~~~~~~~~~~~


Description:
............

The concordance relation to use.



XMCDA related:
..............

- **Tag:** alternativesMatrix

----------------------------



.. _ElectreOutranking-J-MCDA_outputs:

Outputs
-------


- :ref:`outranking <ElectreOutranking-J-MCDA-output0>`
- :ref:`messages <ElectreOutranking-J-MCDA-output1>`

.. _ElectreOutranking-J-MCDA-output0:

outranking
~~~~~~~~~~


Description:
............

The outranking relation computed from the given data.



XMCDA related:
..............

- **Tag:** alternativesMatrix

----------------------------


.. _ElectreOutranking-J-MCDA-output1:

messages
~~~~~~~~


Description:
............

A status message.



XMCDA related:
..............

- **Tag:** programExecutionResult

----------------------------



For further technical details on the web service underlying this program, have a look at its documentation `here <//www.decision-deck.org/ws>`_.
