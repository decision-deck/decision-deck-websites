.. index:: download

.. raw:: html

    <hr>
    <div style="font-size: x-large; text-align: center;">
      <b style="color: firebrick;">New!</b> Test now the <a href="/diviz-XMCDAv4.html">next version of diviz</a>!
    </div>
    <hr>


.. _download:

Download diviz v20
==================

.. ..note:: The diviz server will be shut down for maintenance from Friday 22 October 2010 13:30 until Friday 22 October 15:30.

..   During that period you will not be able to download diviz, because it requires a connection to that server.

..   Both the server and the diviz client will be upgraded to a new version. The download link will be made available again after the maintenance task.

The latest version of diviz is 20 «Goofy's Railway Express» (see the `changes <https://gitlab.com/decision-deck/diviz/blob/master/CHANGES.en>`_), pick up yours:

============================== ===========
 Operating System               Installer
============================== ===========
Windows 32 bits                `diviz_windows-x32_20.exe <https://downloads.diviz.org/diviz/diviz_windows-x32_20.exe>`_
Windows 64 bits                `diviz_windows-x64_20.exe <https://downloads.diviz.org/diviz/diviz_windows-x64_20.exe>`_
Linux                          `diviz_unix_20.sh <https://downloads.diviz.org/diviz/diviz_unix_20.sh>`_ (install with: ``sh diviz_unix_20.sh``)
Mac OS X                       | **Warning:** Mac OS X «High Sierra» 10.13: see the note, below
                               | `diviz_macos_20.dmg <https://downloads.diviz.org/diviz/diviz_macos_20.dmg>`_
Others / Platform Independent  `diviz-client_v20.jar <https://downloads.diviz.org/diviz/diviz-client_v20.jar>`_ (this is not an installer, see below)
============================== ===========

.. note:: macOS «High Sierra» 10.13 and higher: some users report that diviz cannot execute workflow with the latest macOS, when using the "dmg" installer.  If this happens to you, please use the Platform Independent *jar* file (above).  We do not have any Mac at hand, hence we are not able to replicate, diagnose and solve this problem.

Each of the installers (except the last entry, see below the note :ref:`jar-client`) installs a version of diviz with an auto-update functionality: diviz checks if a new version is available each time it is launched (except for the last entry, see below).  When a new version is released, diviz offers to download and install the new version for you --alternatively, you can come back to this page and download the new installer.

If you wish to be informed of future releases, we suggest that you subscribe to the (low traffic) announcement mailing list of diviz (for further details, please have a look at the :doc:`contact page<contact>`).

Should you have *any* problem with these installers, please `report the problem </contact.html>`_, so that we can help you installing the software and at the same time fix the problem.  Thank you!


Regarding Java being required
-----------------------------

diviz requires Java 8 (update 111 or later), but depending on the situation, you do not necessarily install it yourself.

- Windows and macOS installers: these installers will automatically download and install Java for diviz when no suitable Java can be found on your computer.  In other words, the installers take care of everything and you do not have to worry about whether Java is installed.  Note that when diviz installs its own Java, it will *not* affect neither your system nor other programs, it will be used by diviz and diviz only.

- The Linux/Unix installer is not shipped with Java 8: please refer to the documentation to determine how Java 8 can be installed.  This is because there is too much differences between the different Linux distributions.

- The jar client: if you use the "jar" client, you must install Java 8 yourself. It can downloaded from the latest Java version |java_download_link|.

.. |java_download_link| raw:: html

   <a href="https://www.java.com" target="_blank">here</a>


Last, Windows users can download a version of diviz with the JRE already bundled in the installer:

============================== ===========
 Operating System               Installer
============================== ===========
Windows 32 bits (with Java)    `diviz_windows_20_jre8.exe <https://downloads.diviz.org/diviz/diviz_windows-x32_20_jre8.exe>`_
Windows 64 bits (with Java)    `diviz_windows-x64_20_jre8.exe <https://downloads.diviz.org/diviz/diviz_windows-x64_20_jre8.exe>`_
============================== ===========

The only difference with the installers featured at the top of this page is that they do not require an extra download of Java when the installer detects that Java is not installed, or that the installed version is not suitable for diviz.  In some restricted environments, it makes the installation easier.


.. _jar-client:

Using ``diviz-client_v20.jar``
------------------------------

When you download the raw .jar file, there is no mechanism for updating it, you will have to come back to this page when the software informs you that a new client is ready to be downloaded.

To run the jar file:

- either double click on the downloaded jar file;

- or run the following command (in a terminal window (Linux) or a command prompt (Windows)):

  ::

    java -jar diviz-client_v20.jar

(Windows users open the command prompt by clicking on Start > All Programs > Accessories > Command Prompt)


Command-line execution
~~~~~~~~~~~~~~~~~~~~~~

Since version 1.4, a command-line is also available, for launching the execution of workflows without having to use the graphical user interface:

::

  DVZ_CMD="java -Djava.util.logging.config.file= -Done-jar.main.class=eu.telecom_bretagne.praxis.client.SimpleCommandLine -jar diviz-client_v20.jar"

Executing a workflow:

::

  $DVZ_CMD /path/to/diviz/workspace/weightedSum/current/weightedSum.dvz

For details on the available options: ``$DVZ_CMD -h``

Note:

  - The command-line can substitute the workflow's input files with others before execution

  - Option '-v' triggers verbose logging.


Note on the installers
----------------------

All installers (except the last "jar" entry) are made by the `multi-platform installer builder "install4j" <https://www.ej-technologies.com/products/install4j/overview.html>`_ (for which we benefit from an open-source project license).
The development of diviz is also helped by the use of the `Java profiler "JProfiler" <https://www.ej-technologies.com/products/jprofiler/overview.html>`_  (for which we also have an open-source project license).
