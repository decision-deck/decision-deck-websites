.. index:: workflow, x4, diviz-x4, kappalab

The kappalab R library: ``linProgCapaIdent``
============================================

.. toctree::
   :maxdepth: 2

Short description
-----------------

A sample workflow built from elementary MCDA components of the kappalab R library [GrabischEtAl2008]_, using the ``linProgCapaIdent`` capacity identification method.

Detailed description
--------------------

This workflow uses, among other components, the :ref:`linProgCapaIdent (kappalab)<linProgCapaIdent-kappalab>` component to identify a capacity underlying a preorder on some reference alternatives and some further preferential information given by the decision maker.

The output capacity is then used to calculate the Choquet integral of the alternatives. The ranks of the alternatives is then calculated and the output preorder is plotted as a graph. Various indexes are calculated on the capacity, and the shapley Indexes are represented as a barchart. The input partial preorder on the reference alternatives is also plotted as a graph.

Screenshot
----------

.. figure:: workflows/x4/kappalab.linProgCapaIdent.png
   :width: 100%

Download link
-------------

:download:`kappalab.linProgCapaIdent.dvz</workflows/x4/kappalab.linProgCapaIdent.dvz>`

Click :ref:`here<tutorial_import>` for a detailed description on how to import the workflow into diviz.

Bibliography
------------

.. [GrabischEtAl2008] Grabisch M, Kojadinovic I, Meyer P, A review of capacity identification methods for Choquet integral based multi-attribute utility theory, Applications of the Kappalab R package, European Journal of Operational Research, 186 (2), 766-785, 2008.
