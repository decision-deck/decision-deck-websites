.. index:: workflow, ELECTRE, ELECTRE 3

ELECTRE 3
=========

.. toctree::
   :maxdepth: 2

Short description
-----------------

This workflow demonstrates the construction of an outranking relation according to the ELECTRE [Roy1996]_ methods approaches and the exploitation via a similar algorithm than ELECTRE 3.

Detailed description
--------------------

This workflow uses, among other components, the :ref:`ELECTRE Concordance<ElectreConcordance-J-MCDA>`, :ref:`ELECTRE Discordances<ElectreDiscordances-J-MCDA>` and :ref:`ELECTRE Outranking<ElectreOutranking-J-MCDA>` components to calculate the outranking relation according to the ELECTRE methods approaches. It then uses the :ref:`alternativesRankingViaQualificationDistillation<alternativesRankingViaQualificationDistillation-ITTB>` component to determine a ranking of the alternatives, based on an algorithm similar to the ELECTRE 3 exploitation procedure. From a 0-1 valued outranking graph (obtained after a cut), the strength (resp. the weakness) of each alternative is calculated by counting the number of alternatives that they beat (resp. by which they are beaten). The qualification is the difference between the strength and the weakness. For the downwards (resp. upwards) distillation, the best (resp. worst) alternatives are then removed, i.e. those with the highest (resp. lowest) qualification, and put in the first (resp. last) position of the final ranking. The procedure is repeated with the remaining alternatives, until no alternative is left.

Screenshot
----------

.. figure:: workflows/method.ELECTRE-3.png
   :width: 100%

Download link
-------------

:download:`method.ELECTRE-3.dvz</workflows/method.ELECTRE-3.dvz>`

Click :ref:`here<tutorial_import>` for a detailed description on how to import the workflow into diviz.

Bibliography
------------

.. [Roy1996] Roy B., Multicriteria Methodology for Decision Analysis, Kluwer Academic Publishers, 1996
