.. index:: features, workflow design, workflow deployment, visualisation, open source

Features
========

What diviz is
-------------

The goals of diviz are:

- to help **researchers** to construct algorithmic MCDA workflows ( = methods) from elementary MCDA components;
- to help **teachers** to present MCDA methods and let the students experiment their own creations;
- to help to easily **compare** results of different methods;
- to allow to easily **add** new elementary MCDA components;
- to avoid heavy calculations on your local computer by executing the methods on distant servers;
- to ease the **dissemination** of new MCDA algorithms, methods and experiments.

The main properties of diviz are:

- all available MCDA components in diviz are (currently) (opensource) **web services** (`XMCDA web services <//www.decision-deck.org/ws>`_);
- the **history** of all the past executions is available (ideal for parameter tuning);
- use of `XMCDA <//www.decision-deck.org/xmcda>`_  to make elementary components interoperable;
- use of `XMCDA <//www.decision-deck.org/xmcda>`_ and XSL (eXtensible Stylesheet Language) and CSS (Cascading Style Sheets) for a standardised visualisation of input and output data.

It is open-source, released under the `CeCILL license <https://cecill.info/index.en.html>`_; the source code is hosted on its `gitlab project's page <https://gitlab.com/decision-deck/diviz>`_

Below you can find the main characteristics of diviz with a description of each of the features.

A tool for MCDA components workflow design, execution, and deployment
*********************************************************************

Based on basic algorithmic components, diviz allows to combine these elements in view of creating complex MCDA workflows or MCDA methods.

Once the workflow is designed, it can be executed on various data sets written according to the `XMCDA <//www.decision-deck.org/xmcda>`_ standard.
This execution is performed on distant servers via web services (each of the components is in fact proposed as a web service). See the list of `XMCDA web services <//www.decision-deck.org/ws>`_ for further details.

Once the execution is completed, the outputs of the different elementary components are available and can be visualised in diviz.
The user can then modify the input files in order to rerun the workflow, or change the existing workflow to adapt it to his needs.
The history of past executions is available at any moment, which helps to compare the input and output data and tune the parameters or the workflow.

Finally, the constructed workflow can be deployed as a new web service which is then automatically made available to all the users which are employing diviz (in development).

A simple and standardised MCDA data visualisation tool
******************************************************

The data exchanged between the elementary algorithmic components is conform to the `XMCDA <//www.decision-deck.org/xmcda>`_ standard.
`XMCDA <//www.decision-deck.org/xmcda>`_ allows to express MCDA concepts through a few general XML structures.

By using the power of XSL (eXtensible Stylesheet Language) combined with a standard CSS (Cascading Style Sheets), we can easily visualise any data elements which are sent to components or produced by them.
This visualisation is done in a particular pane of the diviz window and is easily accessible for all past executions.

Platform independent
********************

The diviz client software is written in Java and is therefore independent of the operating system you are using.

Open source
***********

diviz is distributed as an open source software. The various elementary MCDA components (which are the `XMCDA web services <//www.decision-deck.org/ws>`_) are also available as open source software bricks.

What diviz is not
-----------------

A decision aid process designer and manager, and a role manager
***************************************************************

As mentionned above, diviz is a workflow manager for algorithmic components.

As such it should not be mixed up with a decision aid process workflow manager.
Such a tool would model the different important steps, as well as the various roles might be involved in such decision aid process.
The development of such a decision aid process manager is one of the objectives of the `Decision Deck <//www.decision-deck.org>`_ project.
