.. _diviz_XMCDAv4:
.. _diviz.x4:

===============================
 diviz for XMCDAv4 - Open Beta
===============================

.. contents:: Table of contents
   :local:
   :depth: 2
   :backlinks: top

Introduction
============

The next version of diviz, referred to as diviz-x4 in this document, will be based on XMCDA v4. It will replace the current version of diviz, and this replacement is currently scheduled for August 2021.

This version is now in open beta, allowing everyone to get started and test the software.

On this page you will find important information to guide your testing and how to give us feedback.  You will learn how to convert existing diviz x2 workflows into the new version, and you will also read about the elements of the new version that are not yet fully finalised and on which we need your feedback.

The download links are in the next section.

.. _download:
Download
========
You will find here the latest version of it:  `diviz-x4.v1-beta-10.jar <https://downloads.diviz.org/diviz-x4/beta/diviz-x4.v1-beta-10.jar>`_

============================== ===========
 Operating System               Installer
============================== ===========
Windows 32 bits                `diviz-x4_windows-x32_v1-beta-10.exe <https://downloads.diviz.org/diviz-x4/beta/diviz-x4_windows-x32_v1-beta-10.exe>`_
Windows 64 bits                `diviz-x4_windows-x64_v1-beta-10.exe <https://downloads.diviz.org/diviz-x4/beta/diviz-x4_windows-x64_v1-beta-10.exe>`_
Linux                          `diviz-x4_unix_v1-beta-10.sh <https://downloads.diviz.org/diviz-x4/beta/diviz-x4_unix_v1-beta-10.sh>`_ (install with: ``sh diviz_unix_v1-beta-10.sh``)
Mac OS X                       `diviz-x4_macos_v1-beta-10.dmg <https://downloads.diviz.org/diviz-x4/beta/diviz-x4_macos_v1-beta-10.dmg>`_
Others / Platform Independent  `diviz-x4.v1-beta-10.jar <https://downloads.diviz.org/diviz-x4/beta/diviz-x4.v1-beta-10.jar>`_ (this is not an installer)
============================== ===========


.. _workflows-for-diviz-x4:
Example workflows for diviz x4
==============================

Example workflows are available at: :doc:`workflows_x4`.


.. _testing-reporting:
Testing & reporting
===================

This version will replace the current version of diviz.  The replacement is currently scheduled for August 2021.

It uses version 4 of XMCDA (the current version uses version 2): in the following, we refer to the current version as "diviz x2" and to this beta version as "diviz x4".

..
  XXX link to v4 description + TODO this page

We need you to test it and give us feedback on your tests, in particular:

- Are there any programs that don't work as expected?

- Are there any programs missing that you need?


Things to know before you test:

- All programs in the current version of diviz are in the program tree.  Some of them are not (yet) available: they are greyed out and marked with a cross, as in the example below:
    
  .. image:: images/unavailable-program-example.png
      :align: center
      :alt: an example of an unavailable program


  If you need some of these unavailable programs before upgrading to the new version, please let us know!

- Some XMCDA v4 elements are not yet correctly viewed in diviz-x4: at any time, you can examine the content of a result file by clicking on the "XML View" button (see below).  Please report files that are not correctly represented in the default HTML view!

  .. image:: images/results_xml_view.png
      :align: center
      :alt: Click on "XML view" to view the content of the XMCDA file

- diviz-x2 and diviz-x4 can be opened simultaneously, to allow easier comparison of the same same workflow in both versions.

  

.. _converting-exiting-x2-workflows:
Converting existing "x2" workflows
==================================

diviz-x4 offers the possibility to convert diviz-x2 workflows.

However, this feature is not perfect, it is there to speed up the transition from one version to another, but it is still necessary to carefully examine the translated workflows.

To convert one or more workflows:

- In diviz-x4, go to the File menu, then click on the menu item "Convert from diviz - XMCDA v2..."

- A window opens with the existing diviz-x2 workflows

- Select the workflows you wish to convert (to select several at once, hold down the 'Ctrl' key)

Once converted, the workflows are automatically opened in the diviz-x4 workspace.
There are several things to check:

- Check that all programs are present: some may not be available,

- Check the settings of the programs, it is possible that some parameters have not been correctly translated, especially the default values.

- In general, check the "wiring" between files and programs, and between programs: some links may have disappeared (NB: in general, if the entries of a program are not all linked, it is not possible to execute the workflow).


.. _howto-submit-feedback:
How to submit feedback: bug reports, questions, suggestions?
============================================================

We look forward to your feedback!  You can do so:

- by mail, to support-x4@diviz.org
- by `opening a new ticket <https://gitlab.com/decision-deck/diviz/issues/new?issue>`_ (requires a (free) account on `GitLab <https://www.gitlab.com>`_).

In either case, this opens a support ticket on the diviz ticket tracking system hosted by `gitlab <https://www.gitlab.com/decision-deck/diviz/>`_.  If you choose email, you will receive our replies by email and you can reply by email as well: you don't need to have an account on GitLab in this case.
  
In any case, please describe the problem as precisely as possible, attaching if necessary the workflows (including the original diviz-x2 workflows when it is relevant), screenshots, etc.
