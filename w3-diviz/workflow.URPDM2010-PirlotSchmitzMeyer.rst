.. index:: workflow, ACUTA, weighted sum, method comparison

..
  [Pirlot, Schmitz, Meyer, URPDM2010]

Choquet integral, additive value functions and weighted sum: comparing expressiveness
=====================================================================================

.. toctree::
   :maxdepth: 2

Short description
-----------------

This workflow compares the expressiveness of a Choquet integral, a weighted sum and additive value functions, created for the URPDM2010 conference in Coimbra, Portugal.

Detailed description
--------------------

This workflow generates a number of alternatives by drawing at random a vector of evaluations for each of them via the :ref:`randomNormalizedPerformanceTable<randomNormalizedPerformanceTable-RXMCDA>` component and a random order on these alternatives is drawn via the :ref:`randomAlternativesRanks<randomAlternativesRanks-RXMCDA>` component. Then we examine whether this order is representable by a simple weighted sum (:ref:`additiveValueFunctionsIdentification<additiveValueFunctionsIdentification-RXMCDA>` with 1 segment) a Choquet integral with respect to a 2 additive capacity (:ref:`linProgCapaIdent<linProgCapaIdent-kappalab>` with k=2), a piecewise-linear additive value function with 2 pieces (:ref:`additiveValueFunctionsIdentification<additiveValueFunctionsIdentification-RXMCDA>` with 2 segments) or a general additive value function.

The outputs are then pairwisely compared via Kendall's Tau coefficient.

This example is inspired from [PirlotSchmitzMeyer2010]_.

Screenshot
----------

.. figure:: workflows/article-URPDM2010-PirlotSchmitzMeyer.png
   :width: 100%

Download link
-------------

:download:`article-URPDM2010-PirlotSchmitzMeyer.dvz</workflows/article-URPDM2010-PirlotSchmitzMeyer.dvz>`

Click :ref:`here<tutorial_import>` for a detailed description on how to import the workflow into diviz.

Bibliography
------------

.. [PirlotSchmitzMeyer2010] Pirlot M, Schmitz H, Meyer P, An empirical comparison of the expressiveness of the additive value function and the Choquet integral models for representing rankings. Proc. of 25th Mini-EURO Conference "Uncertainty and Robustness in Planning and Decision Making" (URPDM 2010), Coimbra, Portugal, 15-17 April 2010 (preliminary :download:`pdf</files/articles/PirlotSchmitzMeyer-URPDM2010.pdf>`).
