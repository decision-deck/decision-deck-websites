.. index:: workflow, ACUTA, weighted sum, method comparison

..
  [Bigaret, Meyer, URPDM2010]

Ranking: ACUTA vs. weighted sum
===============================

.. toctree::
   :maxdepth: 2

Short description
-----------------

This workflow compares the ranking obtained by the ACUTA algorithm with the one obtained with a weighted sum, on the classical "Thierry's sports car choice" example [BouyssouEtAl2000]_, created for the URPDM2010 conference in Coimbra, Portugal.

Detailed description
--------------------

This workflow contains 3 parts:

- the upper area represents the use of the disaggregation technique ACUTA [BousEtAl2010]_ to compute partial value functions on basis of a ranking given as an input;
- the lower area represents a classical weighted sum;
- the right part compares the output of the two techniques.

This example is discussed in further details in [BigaretMeyer2010]_.

Screenshot
----------

.. figure:: workflows/article-URPDM2010-BigaretMeyer.png
   :width: 100%

Download link
-------------

:download:`article-URPDM2010-BigaretMeyer.dvz</workflows/article-URPDM2010-BigaretMeyer.dvz>`

Click :ref:`here<tutorial_import>` for a detailed description on how to import the workflow into diviz.

Bibliography
------------

.. [BigaretMeyer2010] Bigaret S, Meyer P, diviz: an MCDA workflow design, execution and sharing tool. Proc. of 25th Mini-EURO Conference "Uncertainty and Robustness in Planning and Decision Making" (URPDM 2010), Coimbra, Portugal, 15-17 April 2010 (preliminary :download:`pdf</files/articles/divizURPDM2010.pdf>`).

.. [BousEtAl2010] Bous G, Fortemps P, Glineur F, Pirlot M, ACUTA: A novel method for eliciting additive value functions on the basis of holistic preference statements, European Journal of Operational Research, submitted, 2010.

.. [BouyssouEtAl2000] Bouyssou D, Marchant T, Pirlot M, Perny P, Tsoukiàs A, Vincke P. Evaluation and decision models: A critical perspective, Kluwer, 2000.
