.. index:: workflow, x4, diviz-x4, ACUTA

ACUTA
=====

.. toctree::
   :maxdepth: 2

Short description
-----------------

This workflow demonstrates the construction of the ACUTA method [BousEtAl2010]_ and the exploitation of some of its results.

Detailed description
--------------------

This workflow uses the :ref:`ACUTA<ACUTA-UTAR>` component to determine value functions based on a ranking of the alternatives given by the decision maker.

These value functions are then applied on the original performance table and the overall values of the alternatives are computed. The whole data is then analysed via some graphical representations.

Screenshot
----------

.. figure:: workflows/x4/method.ACUTA.png
   :width: 100%

Download link
-------------

:download:`method.ACUTA.dvz</workflows/x4/method.ACUTA.dvz>`

Click :ref:`here<tutorial_import>` for a detailed description on how to import the workflow into diviz.

Bibliography
------------

.. [BousEtAl2010] Bous G, Fortemps P, Glineur F, Pirlot M, ACUTA: A novel method for eliciting additive value functions on the basis of holistic preference statements, European Journal of Operational Research, submitted, 2010.
