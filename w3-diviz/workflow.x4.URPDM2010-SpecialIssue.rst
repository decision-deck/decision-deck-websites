.. index:: workflow, x4, diviz-x4, ACUTA, PROMETHEE, weighted sum, method comparison

..
   [Bigaret, Meyer, URPDM2010SpecialIssue]

ACUTA and PROMETHEE: comparing rankings
=======================================

.. toctree::
   :maxdepth: 2

Short description
-----------------

This workflow compares the ranking obtained by the ACUTA algorithm with the one obtained with the PROMETHEE method, on the classical "Thierry's sports car choice" example [BouyssouEtAl2000]_, based on the example for the URPDM2010 conference in Coimbra, Portugal. It extends this example for the special issue of a journal linked to the conference.

Detailed description
--------------------

This workflow contains 3 parts:

- the upper area represents the use of the PROMETHEE method to obtain a ranking of the alternatives;
- The lower area represents the disaggregation technique ACUTA [BousEtAl2010]_ to compute partial value functions on basis of a ranking given as an input;
- the right part compares the output of the two techniques.

This example is discussed in further details in [BigaretMeyer2011]_.

Screenshot
----------

.. figure:: workflows/x4/article-URPDM2010-SpecialIssue-BigaretMeyer.png
   :width: 100%

Download link
-------------

:download:`article-URPDM2010-SpecialIssue-BigaretMeyer.dvz</workflows/x4/article-URPDM2010-SpecialIssue-BigaretMeyer.dvz>`

Click :ref:`here<tutorial_import>` for a detailed description on how to import the workflow into diviz.

Bibliography
------------

.. [BigaretMeyer2011] Meyer P, Bigaret S, diviz: a tool for modeling, processing and sharing algorithmic workflows in MCDA, (submitted) (preliminary :download:`pdf</files/articles/divizURPDM2010specialIssue.pdf>`).

.. [BousEtAl2010] Bous G, Fortemps P, Glineur F, Pirlot M, ACUTA: A novel method for eliciting additive value functions on the basis of holistic preference statements, European Journal of Operational Research, submitted, 2010.

.. [BouyssouEtAl2000] Bouyssou D, Marchant T, Pirlot M, Perny P, Tsoukiàs A, Vincke P. Evaluation and decision models: A critical perspective, Kluwer, 2000.
