.. index:: upcoming events, conferences

Scientific dissemination
========================


.. 

    Upcoming events involving diviz
    -------------------------------

Presentation of diviz at conferences
------------------------------------

- Patrick Meyer, Sébastien Bigaret, XMCDA web services - latest available algorithms and their use via diviz. 7th Decision Deck Workshop, 06-06 october 2010, Paris, France, 2010.

- Patrick Meyer, Sébastien Bigaret, Designing, executing and sharing MCDA workflows via the diviz software and the XMCDA web services, 72nd meeting of the European Working Group "Multiple Criteria Decision Aiding", 07-09 october 2010, Paris, France, 2010.

- Sébastien Bigaret, Patrick Meyer, diviz: an MCDA workflow design, execution and sharing tool. 25th Mini-EURO Conference Uncertainty and Robustness in Planning and Decision Making (URPDM 2010), Coimbra : 15-17 april 2010, Coimbra, Portugal, 2010, ISBN 978-989-95055-3-7.

- Sébastien Bigaret, Patrick Meyer, Latest developments on diviz. 5th Decision Deck Workshop, 17-18 september 2009, Brest, France, 2009.

- Sébastien Bigaret, Patrick Meyer, Tutorial : how to create and submit a web service proposal and integration into diviz. 5th Decision Deck Workshop, 17-18 september 2009, Brest, France, 2009.

- Thomas Veneziano, Sébastien Bigaret, Patrick Meyer, diviz : an MCDA components workflow execution engine. EURO XXIII : 23rd European Conference on Operational Research, 05-08 july 2009, Bonn, Germany, 2009

- Sébastien Bigaret, Patrick Meyer, Vincent Mousseau, The Decision Project: Towards Open Source Software Tools Implementing Multiple Criteria Decision Aid, Journées de l'Optimisation 2009, Montréal, Canada, 4-6 May 2009.

    The Decision Deck project aims at collaboratively developing open source software tools implementing Multiple Criteria Decision Aid (MCDA). Its purpose is to provide effective tools for three types of users: 

    - practitioners who use MCDA tools to support actual decision makers involved in real world decision problems; 

    - teachers who present MCDA methods in courses, for didactic purposes; 

    - researchers who want to test and compare methods or to develop new ones. 

    In this talk, we present the Decision Deck project and detail the different research initiatives which have emerged inside it. In particular, we focus on the diviz software platform which is a tool for designing, executing and deploying MCDA methods.

- Patrick Meyer, Raymond Bisdorff, Vincent Mousseau, M. Pirlot : Latest news on the Decision Deck Project and focus on diviz, MCDA69, 69th Meeting of the Euro Working Group "Multiple Criteria Decision Aiding", Brussels, Belgium, April 2-3, 2009.

- Sébastien Bigaret and Patrick Meyer, diviz, an MCDA components workflow execution engine , 4th Decision Deck Workshop, Mons, Belgium, 30-31 March 2009.

    diviz is a software platform, derived from the open source BioSide software, which aims at helping researchers to build workflows based on elementary algorithmic components available as web services. We focus on the use of components originating from kappalab, an R library, which can be used for Choquet integral based MCDA (slides). 

Past events involving diviz
---------------------------

- 23 September 2013: 11th Decision Deck workshop at École Centrale Paris, France

- 24 September 2013: 3rd Decision Deck Developer's Day at École Centrale Paris, France

- 11-12 April 2013: MCDA'77: 77th meeting of the European working group on multicriteria decision aiding, Rouen, France, http://www.iae-rouen.fr/mcda77/programme-scientifique (Decision Deck session, 12 April)

- 12-14 April 2012, MCDA'75: 75th meeting of the European Working Group in Multiple Criteria Decision Aid, Tarragone, Spain, http://deim.urv.cat/~itaka/CMS4/images/MCDAdocuments/Program_MCDA75.pdf

- 11th April 2012: 10th Decision Deck workshop, Tarragone, Spain, http://deim.urv.cat/~itaka/CMS4/index.php?option=com_content&view=article&id=10&Itemid=14

- 13 April 2011: 8th Decision Deck workshop in Corte, Corsica, France, http://mcda.univ-corse.fr/Welcome-Bienvenue-_a8.html

- 6 October 2010: 7th Decision Deck workshop at Université Paris Dauphine, http://www.lgi.ecp.fr/~mousseau/D2Workshop/pmwiki-2.2.7/pmwiki.php/Main/HomePage?setlang=en

- 14-15 April 2010: 6th Decision Deck workshop at INESC Coimbra, Portugal, http://www.inescc.pt/ddws6/

- 27 June - 8 July 8 2010: 10th MCDA Summer School at Ecole Centrale Paris, France, http://www.gi.ecp.fr/mcda-ss

- 17-18 September 2009: 5th Decision Deck workshop at Telecom Bretagne, France, http://conferences.telecom-bretagne.eu/ddws5/

- 30-31 March 2009: 4th Decision Deck workshop at the University of Mons, Belgium.
