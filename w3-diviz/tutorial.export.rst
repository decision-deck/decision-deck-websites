.. index:: tutorial, workflow export

Export of a workflow in diviz
=============================

.. toctree::
   :maxdepth: 2

Short description of the tutorial
---------------------------------

This tutorial presents you how to export workflows from diviz to share them with other users.

Detailed description of the tutorial
------------------------------------

This tutorial focusses on the following aspects:

  - Exporting a workflow without the input files;
  - Exporting a workflow with the input files.

Tutorial
--------

Run diviz. 

After the splash screen you can see an empty workspace. 
The left panel is used to display the open workflows and their past executions. 
The right pane contains the list of available elementary components. 
The center pane is used to construct and display the workflows.

Open a workflow that you have created or have a look at the workflow creation :ref:`tutorial<tutorial1>` to create one.

To export a workflow without the input files, open the workflow menu and click on "Export workflow without input files". A dialog then lets you select the filename in which you wish to save the workflow. 

If you wish to export the workflow with the input files, open the workflow menu and click on "Export workflow with input files". A dialog then lets you select the filename of the zip archive in which you wish to save the workflow with the input files. 


