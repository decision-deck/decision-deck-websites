.. index:: workflow,

[Kadzinski, Labijak, Napieraj, 2016, Omega]
===========================================

.. toctree::
   :maxdepth: 2

Short description
-----------------

The workflows implement the robustness and stochastic analysis of efficiency of Polish airports described in the paper published Omega - The International Journal of Management Science.

Detailed description
--------------------

The workflows use the set of all feasible input and output weights to answer various robustness concerns by deriving: (1) extreme efficiency scores and (2) extreme efficiency ranks for each airport, (3) possible and necessary efficiency preference relations for pairs of airports, (4) efficiency distribution, (5) efficiency rank acceptability indices, and (6) pairwise efficiency outranking indices. We consider four inputs related to the capacities of a terminal, runways, and an apron, and to the airport's catchment area, and two outputs concerning passenger traffic and number of aircraft movements. We present how the results are affected by integrating the weight constraints and eliminating outlier airport. The case study is discussed in detail in [KadzinskiLabijakNapieraj2016]_. Note that the stochastic results obtained by executing the workflows in diviz might differ slightly from the ones presented in the article. This is due to the fact that some parts of the resolution are not fully deterministic.

Screenshot
----------

.. figure:: workflows/DEAPolishAirports.png
   :width: 100%


Download links
--------------

- :download:`DEAPolishAirports.dvz</workflows/DEAPolishAirports.dvz>` for results without considering weight constraints;

- :download:`DEAPolishAirportsWithConstraints.dvz</workflows/DEAPolishAirportsWithConstraints.dvz>` for results when considering weight constraints;

- :download:`DEAPolishAirportsWithoutOutlier.dvz</workflows/DEAPolishAirportsWithoutOutlier.dvz>` for results when considering the set of airports without outlier (WAW).

Click :ref:`here<tutorial_import>` for a detailed description on how to import the workflow into diviz.

Bibliography
------------

.. [KadzinskiLabijakNapieraj2016] Milosz Kadzinski, Anna Labijak, Malgorzata Napieraj, Integrated Framework for Robustness Analysis Using Ratio-Based Efficiency Model with Application to Evaluation of Polish Airports, Omega, 2016, https://www.sciencedirect.com/science/article/pii/S0305048316000499
