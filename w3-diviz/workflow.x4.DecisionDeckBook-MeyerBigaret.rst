.. index:: workflow, x4, diviz-x4, UTASTAR, method test

UTASTAR
=======
.. toctree::
   :maxdepth: 2

Short description
-----------------

This workflow demonstrates the UTASTAR method on Thierry's car choice problem [BouyssouMarchantPirlotPernyTsoukiasVincke2000]_.

Detailed description
--------------------

This workflow shows how to implement the UTASTAR method, by using the components:

- :ref:`UTASTAR<UTASTAR-UTAR>`,
- :ref:`computeNormalisedPerformanceTable<computeNormalisedPerformanceTable-UTAR>`,
- :ref:`generalWeightedSum<generalWeightedSum-UTAR>`,
- :ref:`plotAlternativesValuesPreorder<plotAlternativesValuesPreorder-ITTB>`,
- :ref:`plotValueFunctions<plotValueFunctions-ITTB>`.

Screenshot
----------

.. figure:: workflows/x4/article-DecisionDeckBook-MeyerBigaret.png
   :width: 100%

Download link
-------------

:download:`article-DecisionDeckBook-MeyerBigaret.dvz</workflows/x4/article-DecisionDeckBook-MeyerBigaret.dvz>`

Click :ref:`here<tutorial_import>` for a detailed description on how to import the workflow into diviz.

Bibliography
------------

.. [BouyssouMarchantPirlotPernyTsoukiasVincke2000] Bouyssou, D., Marchant, T., Pirlot, M., Perny, P., Tsoukias, A., and Vincke, P. (2000). Evaluation and decision models, A critical Perspective. Kluwer’s International Series. Kluwer, Massachusetts.
