.. index:: workflow, x4, diviz-x4, AHP, TOPSIS

AHP-TOPSIS
==========

AHP-criteria with TOPSIS, by Witold Kupś.


Screenshot
----------

.. figure:: workflows/x4/AHP-TOPSIS.png
   :width: 100%

Download link
-------------

:download:`AHP-TOPSIS.dvz</workflows/x4/AHP-TOPSIS.dvz>`

Click :ref:`here<tutorial_import>` for a detailed description on how to import the workflow into diviz.
