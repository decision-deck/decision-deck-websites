.. index:: workflow, PROMETHEE

PROMETHEE
=========

.. toctree::
   :maxdepth: 2

Short description
-----------------

This workflow demonstrates the construction of the PROMETHEE [BransMareschal2002]_ method.

Detailed description
--------------------

This workflow uses, among other components, the :ref:`PROMETHEE Preference<PrometheePreference-J-MCDA>` and :ref:`PROMETHEE Flows<PrometheeFlows-J-MCDA>` components to calculate the net flows from the performance table and a set of criteria weights and discrimination thresholds. It also uses the :ref:`plotGaiaPlane<plotGaiaPlane-RXMCDA>` component to conveniently represent the problem in a plane.

Screenshot
----------

.. figure:: workflows/method.PROMETHEE.png
   :width: 100%

Download link
-------------

:download:`method.PROMETHEE.dvz</workflows/method.PROMETHEE.dvz>`

Click :ref:`here<tutorial_import>` for a detailed description on how to import the workflow into diviz.

Bibliography
------------

.. [BransMareschal2002] Brans J.-P., Mareschal B., Prométhée-Gaia: Une méthodologie d'Aide à la Décision en présence de critères multiples, Statistique et Mathématiques Appliquées, Editions de l'Université de Bruxelles, Editions Ellipses, 2002.
