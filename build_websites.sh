#!/bin/bash

# Ce script va construire les fichiers de chaque sous-site et les placer
# dans un répertoire donné

#L'emplacement physique sur le disque dur du site
#SITE_ROOT_DIR=/var/www/decision-deck
SITE_ROOT_DIR=www-decision-deck

#SUDO=sudo
SUDO=

CP_R="cp -R"

#CHOWN="chown www-data:www-data"
# ignore chown directives with:
CHOWN=/bin/true

create_directories () {
	for site in project d2 d3 diviz gis news r ws xmcda; do
		mkdir -p "${SITE_ROOT_DIR}/${site}"
		r=$?
		if [ $r -ne 0 ]; then
			echo "Unable to create ${SITE_ROOT_DIR}/${site}" >&2
			return $r
		fi
	done
}

insert_piwik_code()
{
    piwik='\
<!-- Matomo -->\
<script type="text/javascript">\
  var _paq = _paq || [];\
  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */\
  _paq.push(["setDocumentTitle", document.domain + "/" + document.title]);\
  _paq.push(["setCookieDomain", "*.decision-deck.org"]);\
  _paq.push(["setDomains", ["*.decision-deck.org","*.diviz.org"]]);\
  _paq.push(["enableCrossDomainLinking"]);\
  _paq.push(["trackPageView"]);\
  _paq.push(["enableLinkTracking"]);\
  (function() {\
    var u="//analytics.decision-deck.org/";\
    _paq.push(["setTrackerUrl", u+"piwik.php"]);\
    _paq.push(["setSiteId", "1"]);\
    var d=document, g=d.createElement("script"), s=d.getElementsByTagName("script")[0];\
    g.type="text/javascript"; g.async=true; g.defer=true; g.src=u+"piwik.js"; s.parentNode.insertBefore(g,s);\
  })();\
</script>\
<noscript><p><img src="//analytics.decision-deck.org/piwik.php?idsite=1&rec=1" style="border:0;" alt="" /></p></noscript>\
<!-- End Matomo Code -->\
'
    find $1/_build/html -type f -name '*.html' \
        -exec sed -i -e "s@</head>@${piwik}@g" {} \;
}

make_site()
{
    $SUDO rm -f "${SITE_ROOT_DIR}/$1/*"
    cd "$2"; make clean html; cd ..
    insert_piwik_code "$2"
    $SUDO $CP_R "$2"/_build/html/* "${SITE_ROOT_DIR}/$1/"
}

make_all()
{
	make_site project w3-dd
	make_site d2 w3-d2
	make_site d3 w3-d3
	make_site diviz w3-diviz
	make_site gis w3-gis
	make_site news w3-news
	make_site r w3-r
	make_site ws w3-ws
	make_site xmcda w3-xmcda
}

fix_perms()
{
	$SUDO $CHOWN "${SITE_ROOT_DIR}"
	echo "Setting permissions..." >&2
	find "${SITE_ROOT_DIR}" -type f -exec chmod 644 '{}' \;
	find "${SITE_ROOT_DIR}" -type d -exec chmod 755 '{}' \;
}

update_website ()
{
	cp .htaccess ${SITE_ROOT_DIR}/
	choice=$1
	case $choice in
		"dd" | "decision-deck" | "project")
			make_site project w3-dd
			;;
		"di" | "diviz")
			make_site diviz w3-diviz
			;;
		"ws" | "web-services")
			make_site ws w3-ws
			;;
		"x" | "xmcda")
			make_site xmcda w3-xmcda
			;;
		"gi")
			make_site gis w3-gis
			;;
		"ne")
			make_site news w3-news
			;;
		"r")
			make_site r w3-r
			;;
		"d2")
			make_site d2 w3-d2
			;;
		"d3")
			make_site d3 w3-d3
			;;
		*)
			make_all
			;;
	esac

	fix_perms
}

interactive_update ()
{
	# On compile les sous-sites
	echo "Which sub-site do you want to rebuild?"
	echo "   For deploying everything: leave blank and press ENTER"
	echo "   For Decision Deck      : dd"
	echo "   For d2                 : d2"
	echo "   For d3                 : d3"
	echo "   For diviz              : di"
	echo "   For gis                : gi"
	echo "   For news               : ne"
	echo "   For r                  : r"
	echo "   For web services       : ws"
	echo "   For XMCDA              : x"
	echo -n "-> "
	read choice

    update_website $choice
}

usage ()
{
	cat >&2 <<EOF
Usage: $1 [--non-interactive [site]]

- Without any parameter: interactive mode
- Non-interactive mode: the second optional parameter 'site' can be:
    - 'dd' or 'decision-deck': builds the part relative to the DD project
    - 'diviz': /diviz/ corresponding to www.diviz.org
    - 'web-services': www.decision-deck.org/ws/
    - 'xmcda': www.decision-deck.org/xmcda/
    - 'd2', 'd3: builds resp. www.decision-deck.org/d2/ and .../d3/
    - otherwise, or if 'site' is not specified, rebuilds everything
EOF
}
############################

if [ "$1" = "--help" -o "$1" = "-h" ];
then
  usage $0
  exit 0
fi

if [ "$1" = "--non-interactive" ];
then
	set -e
	create_directories
	update_website "$2"
else
	create_directories
	status=$?
	if [ $status -ne 0 ]; then
		echo "Exiting" >&2
		exit $status
	fi
	interactive_update
fi
