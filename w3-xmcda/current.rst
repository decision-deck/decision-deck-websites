.. _current:

Current version
===============

The current *operational* version of XMCDA is 2.2.3  --refer to :ref:`XMCDA v3 current state<xmcda_v3_current_state>` for the full details.

.. Important:: Developers of new projects, please refer to :ref:`the developer's corner<xmcda-developers-corner>`.

Below you will find various information and files related to this current version of the XMCDA standard.

For older versions, have a look at the :ref:`archives<archives>`.

Documentation
-------------

.. toctree::
   :maxdepth: 1

   about
   spec
   doc
   beginner

Declared Namespace
------------------

   - xmlns:xmcda="http://www.decision-deck.org/2019/XMCDA-2.2.3"

Release notes
-------------

   - Release notes for XMCDA :ref:`2.2.3 <release_2_2_3>`

XML schema
----------

   - XML :download:`schema <files/XMCDA/XMCDA-2.2.3.xsd>` to validate your XMCDA file;

Visualisation
-------------

 - Generic `XSL <https://github.com/paterijk/XMCDA/raw/master/stylesheets/XMCDA.xsl>`_ stylesheet; and,
 - Generic `CSS <https://github.com/paterijk/XMCDA/raw/master/stylesheets/XMCDA.css>`_.

that you can adapt to your needs. Both files allow you to easily visualise your XMCDA file in any web browser.

  - Advanced `Rubis <https://github.com/paterijk/XMCDA/raw/master/stylesheets/Rubis/xmcda2Rubis.xsl>`_ stylesheet tuned for the RUBIS method (see also example hereafter);
  - Another advanced `Rubis Choice <https://github.com/paterijk/XMCDA/blob/master/stylesheets/Rubis/xmcda2RubisChoice.xsl>`_ stylesheet tuned for the RUBIS method.


Sample instances
----------------

  - XMCDA :download:`file <files/XMCDASampleFiles/kappalabSample.xml>` related to the kappalab R library;
  - XMCDA :download:`file <files/XMCDASampleFiles/rubisSample.xml>` related to the RUBIS method;
  - XMCDA :download:`file <files/XMCDASampleFiles/rubisChoiceRecommendation.xml>` related to the output of the RUBIS method;
  - XMCDA :download:`file <files/XMCDASampleFiles/testXMCDA2Digraph.xml>` containing a digraph;
  - XMCDA :download:`file <files/XMCDASampleFiles/testXMCDA2PerfTab.xml>` containing a detailed performance table.

.. index:: current version, XSL, CSS
