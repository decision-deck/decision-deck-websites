.. _library_Java:

J-XMCDA Java library
====================

.. Important:: New projects developped for inclusion in the Decision Deck software infrastructure **should** now the :ref:`XMCDA reference library <xmcda-java-reference-library>`, **not** this library.


Description
***********

A library part of the J-MCDA project, allowing to persist MCDA objects into (and read from) XMCDA v2.x files, as well as easily create XMCDA web services.

Please see the `J-MCDA project <https://sourceforge.net/apps/mediawiki/j-mcda/>`_ page.
