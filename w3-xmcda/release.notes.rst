.. _release_3_1_1:

XMCDA 3.1.1 release notes
=========================

- Performance tables can now be empty (i.e. no ``<alternativePerformances>``)


.. _release_3_1_0:

XMCDA 3.1.0 release notes
=========================

- Introducing two new tags: ``criteriaHierarchy`` and ``criteriaSetsHierarchy``.


.. _release_3_0_1:

XMCDA 3.0.1 release notes
=========================

- ``<categoriesInterval>``, ``<categoryProfiles>``, ``<interval>``:
  bug fix: as expected, all these three tags should contain at least a
  ``<lowerBound>`` or an ``<upperBound>``, or both.

- ``<real>`` type is changed from ``float`` to ``double`` (ref. https://www.w3.org/TR/xmlschema-2/#double).


.. _release_3_0_0:

XMCDA 3.0.0 release notes
=========================

XMCDA, rebooted.

(To be written)


.. _release_2_2_3:

XMCDA 2.2.3 release notes
=========================

- Performance tables can now be empty (i.e. no ``<alternativePerformances>``)


.. _release_2_2_2:

XMCDA 2.2.2 release notes
=========================

- Root-tag <categoriesValues> and tag <categoryValue> now have an id,
  a name and a mcdaConcept, just as their siblings.

- Tag <categoryProfile> should only contain one alternativeID only,
  one bounding/central only.  The preceding definition allowing
  multiple <alternativeID> e.g. was definitely a bug.

- Root tag <categoriesProfiles> now has a description, like its
  siblings

- Element <alternativeID> in <categoryProfile> is now mandatory.

Backports of bug-fixes in v3.0.1 onto v2.2.2:

- <real> type is changed from 'float' to 'double'

- <categoriesInterval>, <categoryProfiles>, <interval>: bug fix: as
  expected, all these three tags should contain at least a
  <lowerBound> or an <upperBound>, or both.


.. _release_2_2_1:

XMCDA 2.2.1 release notes
=========================

(To be written)


.. _release_2_2_0:

XMCDA 2.2.0 release notes
=========================

- Added two tags related to fuzzy numbers : <fuzzyNumber/> and <fuzzyLabel/>.

- The qualitative scale has been extended to take into account these fuzzy labels.

- A minor bug has been corrected in the parameter tag, where multiple <value> can now be stored.


.. _release_2_1_0:

XMCDA 2.1.0 release notes
=========================

- Added a matrix of alternatives x criteria under the tag <alternativesCriteriaValues/>.

- Added a <variable/> tag in <criteriaLinearConstraints/>, <attributesLinearConstraints/> and <categoriesLinearConstraints/>, and corrected missing <value/> and <values/> for all the constraints.

- In the standard XSL, taking into account of the <values/> tag in the <pair> tag.


.. _release_2_0_0:

XMCDA 2.0.0 release notes
=========================

XMCDA is a data standard which allows to represent MultiCriteria Decision Analysis (MCDA) data elements in XML according to a clearly defined grammar.

XMCDA is an instance of UMCDA-ML, which is the Universal MultiCriteria Decision Analysis Modelling Language and which is one of the scientific initiatives inside the Decision Deck project. UMCDA-ML is intended to be a universal modelling language to express MCDA concepts and generic decision aid processes.

XMCDA focusses more particularly on MCDA concepts and data structures and is defined by an XML schema.

XMCDA is maintained by the `Decision Deck <//www.decision-deck.org>`_ project and supported by the COST Action IC0602 Algorithmic Decision Theory.

A very natural field of application of XMCDA is given by the web services developed inside the Decision Deck project.

XMCDA 2.0.0 has been approved by the General Assembly 2009 of the Decision Deck Consortium on 31 March 2009.

Structure
---------

The abstract description of the XMCDA structure is performend via a detailed XML schema. In order to motivate as many programers as possible to adopt the XMCDA representation, we have decided to express as many MCDA concepts as possible through a few general data structures coded in XML.

To avoid misunderstandings, note the following conventions which are used in this document:

  - The term MCDA concept describes an real or abstract construction related to the field of MCDA which needs to be stored in XMCDA (like, for example, the importance of the criteria);
  - The term XMCDA type stands for an XML structure that we created for the purpose of XMCDA (like, for example, criteriaValues to store general values related to a set of criteria).

On the tag names
----------------

By convention, the name of a tag starts by a lower-case letter. The rest of the name is in mixed case with the first letter of each internal word capitalised. This allows to easily read and understand the meaning of a tag. We use whole words and avoid as much as possible acronyms and abbreviations. Consider for example the tagnames methodOptions, performanceTable and criterionValue. Note that objects of the same XMCDA type can in general be gathered in a compound tag, represented by a single XML tag named after the plural form of its components (e.g., alternatives).

On the attributes of the tags
-----------------------------

The three following attributes can be found in any of the main data tags: id, name and mcdaConcept. They are in general optional, except for the id attribute in the description of an alternative, a criterion or a category. Each of these three attributes has a particular purpose in XMCDA:

  - The id attribute allows to identify an object with a machine readable code or identifier;
  - The name attribute allows to give a human-readable name to a particular object;
  - The mcdaConcept attribute allows to identify the MCDA concept linked to a particular instance of an XMCDA type.

Structure outline
-----------------

An XMCDA file may contain several tags under the root element. These tags allow to describe various MCDA related data from the a few general categories:

  - Project or file description;
  - Output messages from methods (log or error messages) and input information for methods (parameters);
  - Description of major MCDA concepts as attributes, criteria, alternatives, categories;
  - The performance table;
  - Further preferential information related to criteria, alternatives, attributes or categories.

Further information
-------------------

For further information on the current release, we recommend reading the detailed :doc:`documentation <doc>` of the XML schema or the :doc:`Quick Dive <beginner>` document.


Important note
--------------

An error has been detected in the first officially published version of XMCDA 2.0.0 (online from 06/04/2009 - 23/04/2009).

It contained a <options> and a <option> tag under the <methodOptions> root element. This error has been corrected and these information can now be found under <methodParameters> in the tags <parameters> and <parameter>. If you have used the XMCDA version which was published during that period, please check your code to be sure you have corrected that bug.

.. index:: release notes
