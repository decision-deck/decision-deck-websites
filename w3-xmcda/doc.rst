.. _doc:

Detailed documentation
======================

The detailed descriptions of XMCDA is given by the documentation of the corresponding XML schema.

  - `XMCDA v4.0.0 <_static/html-doc/4.0.0/XMCDA-4.0.0.html>`_

  - `XMCDA v2.2.3 <_static/html-doc/2.2.3/XMCDA-2.2.3.html>`_

.. index:: schema, documentation

.. http://www.decision-deck.org/xmcda/_static/html-doc/4.0.0/XMCDA-4.0.0.html

