#/usr/bin/bash

echo "Starting to copy local version of the website to distant server."

scp -r _build/html/* pat@charles-sanders-peirce.uni.lu:/var/www/html/d2cms/xmcda

echo "Copy finished."
