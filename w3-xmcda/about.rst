.. _about:

About XMCDA
===========

Maintainers
-----------

XMCDA has been developped and is currently maintained by a technical committee within the Decision Deck project called the :ref:`Decision Deck Specifications Committee<spec>`.

Release notes
-------------

XMCDA release notes are specific to each version of the standard.
They summarise changes made in all update releases compared to the previous one.

Select your version from the list below to see the release notes for it.


.. toctree::
   :maxdepth: 1
   :glob:

   release.*

Version naming policy
---------------------

The releases of XMCDA are named according to the following standard:

   XMCDA a.b.c

a, b and c are integers which are increased in case of a new release, according to the following rules:

   - Change from XMCDA a.b.c to XMCDA a.b.(c+1) for minor modifications on the standard, like, e.g., the adding of a new subtag in an XMCDA type;
   - Change from XMCDA a.b.c to XMCDA a.(b+1).0 for more substantial modifications on the standard, like, e.g., the adding of a new tag (or XMCDA type) under the root element;
   - Change from XMCDA a.b.c to XMCDA (a+1).0.0 for fundamental modifications on the standard which do not allow full compatibility to earlier versions, like, e.g., the renaming of a fundamental XMCDA type.

XMCDA modifications policy
--------------------------

Any modification request on the standard must be submitted to the :ref:`specifications<spec>` committee (see :ref:`contact<contact>` details).

Any modification can only be performend on the standard after the approval of the :ref:`specifications<spec>` committee (which generates a new version according to the previously described naming policy).

Copyright notice
----------------

Copyright (c) DECISION DECK Consortium 2009-2016. All Rights Reserved.

*This document and translations of it may be copied and furnished to others, and derivative works that comment on or otherwise explain it or assist in its implementation may be prepared, copied, published, and distributed, in whole or in part, without restriction of any kind, provided that the above copyright notice and this section are included on all such copies and derivative works. However, this document itself may not be modified in any way, including by removing the copyright notice or references to DECISION DECK Consortium, except as needed for the purpose of developing any document or deliverable produced by the Decision Deck Specifications Committee or as required to translate it into languages other than English.*

*The limited permissions granted above are perpetual and will not be revoked by DECISION DECK Consortium or its successors or assigns.*

*This document and the information contained herein is provided on an "AS IS" basis and the Decision Deck Consortium DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY WARRANTY THAT THE USE OF THE INFORMATION HEREIN WILL NOT INFRINGE ANY OWNERSHIP RIGHTS OR ANY IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.*

*DECISION DECK Consortium requests that any Party that believes it has patent claims that would necessarily be infringed by implementations of this DECISION DECK Committee Specification, to notify DECISION DECK Consortium authorities and provide an indication of its willingness to grant patent licenses to such patent claims in a manner consistent with the above mentioned IPR protecting policy.*

*DECISION DECK Consortium invites any party to contact the DECISION DECK Consortium authorities if it is aware of a claim of ownership of any patent claims that would necessarily be infringed by implementations of this specification by a patent holder that is not willing to provide a license to such patent claims in a manner consistent with the above mentioned IPR protecting policy.*

*DECISION DECK Consortium takes no position regarding the validity or scope of any intellectual property or other rights that might be claimed to pertain to the implementation or use of the technology described in this document or the extent to which any license under such rights might or might not be available; neither does it represent that it has made any effort to identify any such rights.*

.. index:: release notes, version policy
