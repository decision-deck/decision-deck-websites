Projects using XMCDA
====================

Below you can find various projects using the XMCDA standard to access MCDA data.

 - The `diviz <//www.diviz.org/index.html>`_ software;
 - The `XMCDA web-services <//www.decision-deck.org/ws/index.html>`_;
 - The kappalab R library through the :ref:`XMCDA <library_R>` R library;
 - The Python `digraphs <http://ernst-schroeder.uni.lu/Digraph/>`_ module for Rubis;
 - The `J-MCDA project <https://sourceforge.net/apps/mediawiki/j-mcda/>`_ (Helping develop MCDA methods in Java).

Have also a look at the currently existing XMCDA :ref:`libraries<xmcda-developers-corner>`.

.. index:: diviz, kappalab, digraphs
