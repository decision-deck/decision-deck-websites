.. _library_R:

RXMCDA R library
================

.. Important:: New projects developped for inclusion in the Decision Deck software infrastructure **should** now the :ref:`XMCDA reference library <xmcda-java-reference-library>`, **not** this library.

Description
***********

The RXMCDA library for the R statistical software allows you to read many XMCDA v2.x tags and transform them into R variables which are then usable in your algorithms written in R.
The library also allows to write certain R variables into XML files according to the XMCDA v2.x standard.

Download link
*************

The R package of the RXMCDA is available on the `CRAN's RXMCDA package page <https://cran.r-project.org/web/packages/RXMCDA/index.html>`_. You can install it in R via the command

  ::

     install.packages("RXMCDA", dependencies=TRUE)

The latest version of RXMCDA is also available from `github <https://github.com/paterijk/RXMCDA>`_.

Detailed documentation
**********************
:download:`Documentation <files/XMCDALibraries/R/RXMCDA-manual.pdf>` for the RXMCDA library for R

.. index:: XMCDA, library, R, RXMCDA
