Features
========

Below you can find the main characteristics of XMCDA with a description of each of the features.

XMCDA is structured XML
-----------------------
XML is quite convenient because it allows us to define easily some mark-up elements.
XML's purpose is to aid information systems in sharing structured data, to encode documents, and to serialise data.

These reasons have led us to code XMCDA in XML and to define the structure of XMCDA via an XML schema.

XMCDA is described by an XML schema
-----------------------------------
An XML schema is a structured file used to describe an XML language, like XMCDA.
It uses a rich data typing system and is used to generate constraints on an XML document's logical structure.

The XML schema of XMCDA can be used to validate an instance of XMCDA and in many XML editing tools, it can be used to help writing an XMCDA instance.

A detailed documentation of the XMCDA XML schema can be found here :ref:`here<doc>`.

XMCDA can evolve according to your needs
----------------------------------------
If you consider that some of your data is not describable via the XMCDA structures currently available, do not hesitate to submit your request to the Decision Deck specification committee.
Any evolution proposal will be considered seriously. In case the committee considers that your data is representable via XMCDA, we will suggest you an appropriate solution.

XMCDA can conveniently be visualised in any web browser
-------------------------------------------------------
A further advantage of using a markup language like XML to represent the MCDA structures is the ability to visualise easily data structures in common programs like web browsers.
Such a visualisation done according to an XSLT file which performs a transformation of the XML data file into a format which is suitable for a web browser to display (as HTML).

Next to the XML schema, we also provide a basic XSLT file which you can adapt for you specific needs.

XMCDA allows you to test a given problem instance on various algorithms (supporting XMCDA)
------------------------------------------------------------------------------------------
If you describe your problem instance according to the XMCDA standard, you will be able to apply any MCDA algorithm supporting XMCDA to solve your problem (with possibly some minor modifications).
This can already be tested on the `diviz <//www.diviz.org>`_ software platform.

XMCDA promotes interoperability between MCDA algorithms
-------------------------------------------------------
XMCDA allows that the input of an MCDA algorithm can directly be reinjected into another algorithm. This allows to chain MCDA algorithms in complex workflow, as it is done in the `diviz <//www.diviz.org>`_ software platform.

XMCDA is not so difficult to use
--------------------------------
We suggest any interested person to first have a look at the :doc:`Quick dive <beginner>` document that we have prepared to ease the understanding of the XMCDA standard.
A detailed :ref:`documentation<doc>` of the XML schema associated with XMCDA is intended to complete the elements given in the beginner's guide.

Besides do not hesitate to contact us (see :ref:`contact details<contact>`) for any questions on XMCDA.
