.. _xmcda_v4_current_state:

XMCDA v4 current state
======================

The current version of XMCDA is 4.0.0.
It is available as usual as a XML schema, at: :download:`XMCDA-4.0.0.xsd <files/XMCDA/XMCDA-4.0.0.xsd>`.

However, it is not the *operational* version of XMCDA yet, and XMCDA
v2.2.3 is still the one on which web-services and diviz operate.

The reason is that switching from v2.x to v4.x is a **huge** step for all of us.


.. _why_xmcda_v4:
Why XMCDA v4?
-------------

The objective of the redesign of XMCDA v2 into XMCDA v4 was to remove ambiguities in the v2 format, and to make it more consistent.

Some of the ambiguities historically came from a desire to simplify, where possible, the writing of an XMCDA file.  For example, in many places such as in the performance table, it was possible to give a value or a list of values in two ways, either directly:

.. code-block:: xml

   <value>1<value>

or by including them in a ``<values>`` tag:

.. code-block:: xml

   <values>
       <value>1<value>
   </values>

Similarly, it was possible to declare ``<criterionValue>`` directly in the root ``XMCDA`` tag or inside a ``<criteriaValues>`` tag, etc.

In practice, this has caused real interoperability problems, when some programs that had adopted only one of the two ways were unable to read the other.


The other type of ambiguity found in XMCDA v2 is structural and occurs in the representation of MCDA concepts, some of which could be represented in two different ways.  For example, weights on criteria could be described either directly in the ``criteria'' tag or as ``criteriaValues''.  The problem of interoperability of programs using the standard obviously arose in this case, too.  Within the Decision Deck Consortium, we solved this problem by checking the submissions of program authors before they were integrated into the web services offering, and by guiding the authors when necessary to choose the representation that was used by the programs already present.  In this way we ensured the interoperability of all programs hosted by the Consortium (and used in the `diviz <https://www.diviz.org/>`_ but this was of course not satisfactory, most importantly from the perspective of the XMCDA standard.

Finally, there were some inconsistencies in XMCDA v2: criteria' scale or functions could only be declared inside ``criterion``, while criteria values could be declared bot inside a ``criterion`` tag or as ``criteriaValues``.  On the other hand, categories themselves could only declare ranks (in addition to ``categoriesValues``).  hence, the decision was made to unify everything: alternatives, criteria and categories (and their "xxxSets" equivalent) only describe these objects, and everythong else get it own, dedicated tag (``alternativesValues``, ``criteriaFunctions``, etc.).


That were the main reasons why we decided to re-examine the XMCDA v2 format, resulting in the current version 4 (version 3 was the next version until we realized we forgot to rename some tags!).


Roadmap
-------

The following roadmap has been adopted:

1. Propose a reference implementation of XMCDA v4.x

  a. This implementation should be available to (at the minimum) the following languages: Java, R and Python.

  b. This implementation should be able to translate from one version to another, using the principle of the maximum efforts (bot versions are **not** compatible so certain things cannot be translated).

2. Develop, experiment and validate a program wrapper so the current 200 or so web-services are available to accept and produce XMCDA v4.x files (problems that may arise due to the incompatibility of the two XMCDA versions will be addressed with the web-services authors directly).

3. When this is done, diviz will entirely switch to XMCDA 4.x

4. At this point, XMCDA v2.x will officially be abandoned on the *operational level* and XMCDA v4.x will be fully in charge of conveying out MCDA data.  Note that at the time of the switch, the web-services and diviz operating with XMCDA v2.x will not be immediately shut down, but they we will be freezed and left for use for at least 6 months, so that everyone has enough time to handle the transition.


Current state
-------------

Work is in progress with point 3., an `open-beta for diviz w/ XMCDA v4 <http://www.diviz.org/diviz-XMCDAv4.html>`_ has started.


Full details about the Java reference library is available on :ref:`the developer's corner<xmcda-developers-corner>`.

It is also usable with:

- Python

- Java: `xmcda-java <https://gitlab.com/sbigaret/xmcda-java/-/tree/v4>`_
  
- and R, using the `XMCDA-R package <https://gitlab.com/XMCDA-library/XMCDA-R>`_

..
   Examples in the three languages are available here:
  
   - the `weightedSum-java example <https://gitlab.com/XMCDA-library/weightedSum-java>`_,
  
   - the `weightedSum-jython example <https://gitlab.com/XMCDA-library/weightedSum-jython>`_,
  
   - the `weightedSum-R example <https://gitlab.com/XMCDA-library/weightedSum-R>`_.

