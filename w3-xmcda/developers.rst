.. _xmcda-developers-corner:

Developer's corner
===================

.. _xmcda-reference-libraries:
.. _xmcda-java-reference-library:

XMCDA v4.x (and v2.2.3) Java reference library
-----------------------------------------------

The Java reference library is available on `its gitlab repository
<https://gitlab.com/XMCDA-library/XMCDA-java>`_.

The latest stable version (for XMCDA v2) is on the branch ``master``: `XMCDA-java (stable) <https://gitlab.com/XMCDA-library/XMCDA-java/builds/artifacts/master/download?job=build_jar>`_.

It is also available as an `external Maven dependency <https://search.maven.org/search?q=a:xmcda>`_.

The branch "v4" is unsurpringly dedicated to XMCDA v4: `xmcda-java <https://gitlab.com/sbigaret/xmcda-java/-/tree/v4>`_


It is also usable with:

  - Python `python-xmcda <https://gitlab.com/sbigaret/xmcda-python/-/tree/XMCDAv4>`_

  - and R, using the `XMCDA-R package <https://gitlab.com/XMCDA-library/XMCDA-R>`_

..
  Examples of use of the library with Java, Jython and R are avaible on the `XMCDA-library group of projects <https://gitlab.com/groups/XMCDA-library>`_.

XMCDA v2.x libraries
--------------------

The following library were made for XMCDA v2.x.  They are referenced here because they are used by existing projects.

.. Important:: New projects developped for inclusion in the Decision Deck software infrastructure **should** now the XMCDA reference library (above), **not** one of the following libraries.

.. toctree::
   :maxdepth: 1
   :glob:

   library.*
