Scientific dissemination
========================

Upcoming events involving XMCDA
-------------------------------

- 17-18 September 2009: 5th Decision Deck workshop at Télécom Bretagne, France, http://conferences.telecom-bretagne.eu/ddws5/

- 27 June - 8 July 8 2010: 10th MCDA Summer School at Ecole Centrale Paris, France, http://www.gi.ecp.fr/mcda-ss

Presentation of XMCDA at conferences
------------------------------------

- Patrick Meyer, XMCDA 2.0, an XML schema dedicated to MCDA objects and data, 4th Decision Deck Workshop, Mons, Belgium, 30-31 March 2009.

     XMCDA is a structured XML proposal to represent objects and data issued from the field of MCDA. Its main objectives are to allow different MCDA algorithms to interact and to be easily callable from a common platform like diviz. We present this standardised XML structure and detail the underlying data types via examples speaking for themselves.


.. index:: upcoming events, conferences
