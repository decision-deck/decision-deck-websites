.. _spec:

Specifications Committee
========================

Purpose
-------

The Decision Deck Specifications committee's main task is to work at a technical level to propose the Universal Multicriteria Decision Analysis - Modelling Language (UMCDA-ML) standards.

Concerning the XMCDA instance, its task is to maintain the current version and propose evolutions, according to needs expressed by users of XMCDA.

Composition
-----------

    - :ref:`Patrick Meyer<contact>` (chair)
    - Raymond Bisdorff
    - Olivier Cailloux
    - Vincent Mousseau
    - Marc Pirlot
    - Thomas Veneziano
    - Michel Zam

Past and future meetings
------------------------

    - Specifications Meeting 8, September 23 , 2013, École Centrale, Paris: adoption of XMCDA v3.0.0
    - Specifications Meeting 7, January 14, 2009, École Centrale, Paris
    - Specifications Meeting 6, May 28, 2008, UL, Luxembourg
    - Specifications Meeting 5, January 25, 2008, ULB, Brussels
    - Specifications Meeting 4, November 15th, 2007, Lamsade, Paris
    - Specifications Meeting 3, October 12th, 2007, Lamsade, Paris
    - Specifications Meeting 2, 4 September, 2007, Lamsade, Paris
    - Specifications Meeting 1, 15 May, 2007, Lamsade, Paris

.. index:: specifications committee, XMCDA, UMCDA-ML
