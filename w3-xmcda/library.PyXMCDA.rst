.. _library_PyXMCDA:

PyXMCDA Python library
======================

.. Important:: New projects developped for inclusion in the Decision Deck software infrastructure **should** now the :ref:`XMCDA reference library <xmcda-java-reference-library>`, **not** this library.

Description
***********

The PyXMCDA library for Python allows you to read many XMCDA v2.x tags and transform them into Python variables which are then usable in your algorithms written in Python.
The library also allows to write certain Python variables into XML files according to the XMCDA v2.x standard.

Download link
*************

The source for the python package can be found on the `decision-deck/pyxmcda repository <https://gitlab.com/decision-deck/lib-pyXMCDA>`_.

.. index:: XMCDA, library, Python, PyXMCDA
