.. _index:

.. raw:: html

    <div id="title" class="text-center">
      <img class="img-responsive" src="_static/xmcdalogo.png" style="max-height:70px;display:block;margin: 20px auto;">
      <h2>The XMCDA standard</h2>
      <p>XMCDA is a data standard which allows to represent MultiCriteria Decision Analysis (MCDA) data elements in XML according to a clearly defined grammar</p>
    </div>


XMCDA is an instance of UMCDA-ML, which is the Universal MultiCriteria Decision Analysis Modelling Language and which is one of the scientific initiatives inside the Decision Deck project. UMCDA-ML is intended to be a universal modelling language to express MCDA concepts and generic decision aid processes.

XMCDA focusses more particularly on MCDA concepts and data structures and is defined by an XML schema. 

The goals of XMCDA are to ease:

   - the interaction of different MCDA algorithms;
   - the execution of various algorithms on the same problem instance;
   - the visual representation of MCDA concepts and data structures via standard tools like web browsers.

XMCDA is maintained by the `Decision Deck <//www.decision-deck.org/>`_ project and supported by the COST Action IC0602 Algorithmic Decision Theory.

A very natural field of application of XMCDA is given by the web services developed inside the Decision Deck project.  

General information
-------------------

.. toctree::
   :maxdepth: 1

   features
   about
   current
   xmcda_v4_current_state
   dissemination
   projects
   contact
   
Guides
------

.. toctree::
   :maxdepth: 1
   
   beginner
   developers
   doc
   samples
   
.. index:: MCDA, Decision Deck
