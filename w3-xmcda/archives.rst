.. _archives:

Archives
========

3.1.1 (current v3)
------------------

   - The :download:`XML schema <files/XMCDA/XMCDA-3.1.1.xsd>`;

   - Release notes for XMCDA :ref:`3.1.1 <release_3_1_1>`

3.1.0
-----

   - The :download:`XML schema <files/XMCDA/XMCDA-3.1.0.xsd>`;

   - Release notes for XMCDA :ref:`3.1.0 <release_3_1_0>`

3.0.2
-----

   - The :download:`XML schema <files/XMCDA/XMCDA-3.0.2.xsd>`;

   - Release notes for XMCDA :ref:`3.0.2 <release_3_0_2>`

3.0.1
-----

   - The :download:`XML schema <files/XMCDA/XMCDA-3.0.1.xsd>`;

   - Release notes for XMCDA :ref:`3.0.1 <release_3_0_1>`

3.0.0
-----

   - The :download:`XML schema <files/XMCDA/XMCDA-3.0.0.xsd>`;

   - Release notes for XMCDA :ref:`3.0.0 <release_3_0_0>`

2.2.3 (current v2.x)
--------------------

   - The :download:`XML schema <files/XMCDA/XMCDA-2.2.3.xsd>`;

   - Release notes for XMCDA :ref:`2.2.3 <release_2_2_3>`

2.2.2
-----

   - The :download:`XML schema <files/XMCDA/XMCDA-2.2.2.xsd>`;

   - Release notes for XMCDA :ref:`2.2.2 <release_2_2_2>`

2.2.1
-----

   - The :download:`XML schema <files/XMCDA/XMCDA-2.2.1.xsd>`;

   - Release notes for XMCDA :ref:`2.2.1 <release_2_2_1>`

2.2.0
-----

   - The :download:`XML schema <files/XMCDA/XMCDA-2.2.0.xsd>`;

   - Release notes for XMCDA :ref:`2.2.0 <release_2_2_0>`

2.1.0
-----

   - The :download:`XML schema <files/XMCDA/XMCDA-2.1.0.xsd>`;

   - Release notes for XMCDA :ref:`2.1.0 <release_2_1_0>`


2.0.0
-----

   - The :download:`XML schema <files/XMCDA/XMCDA-2.0.0.xsd>`;

   - Release notes for XMCDA :ref:`2.0.0 <release_2_0_0>`

.. index:: archives
