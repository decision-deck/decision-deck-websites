.. _contact:

Contact details
===============

Patrick Meyer

| Institut Télécom; Télécom Bretagne
| LUSSI Department
| Technopôle Brest-Iroise CS 83818
| F-29238 Brest Cedex 3
| France

patrick.meyer@telecom-bretagne.eu

.. index:: contact
