Beginner's guides
=================

XMCDA Quick Dive
----------------

In this guide you can quickly learn the main caracteristics of the XMCDA standard.

You can download the pdf version of this guide :download:`here<files/quickDive/quickDiveXMCDA.pdf>`.
