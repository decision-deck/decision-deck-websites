.. _samples:

Sample files
============

You will find here files describing an MCDA problem or part of a problem in XMCDA format. These files can be used
e.g. to test your software's XMCDA parsing ability, can be used as a basis when writing a tutorial about your software, ...

* `Six Real Cars <https://github.com/oliviercailloux/Six-real-cars/>`_
