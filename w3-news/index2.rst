Latest news
-----------

`See the newsletter </newsletter1.html>`_

.. raw:: html

    <div class="row">
      <div class="col-md-1 col-sm-2 col-xs-3" style = "display: inline-block;vertical-align: middle;float: none;">
        <img src="_static/ddlogo.png" style="width:50px">
      </div><!--
    --><div class="col-md-11 col-sm-10 col-xs-9" style = "display: inline-block;vertical-align: middle;float: none;">
        <a href="post-20170704.html">
        <h2>86th Meeting of the European Working Group on MCDA</h2>
        </a>
        <p><span class="glyphicon glyphicon-time"></span> Posted on 4 July 2017</p>
        <p>The `86th Meeting of the European Working Group on Multicriteria Decision Aiding <http://www.lamsade.dauphine.fr/ewg2017/>`_ will take place on September 21-23, 2017 in Paris.</p>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-1 col-sm-2 col-xs-3" style = "display: inline-block;vertical-align: middle;float: none;">
        <img src="_static/xmcdalogo.png" style="width:50px">
      </div><!--
    --><div class="col-md-11 col-sm-10 col-xs-9" style = "display: inline-block;vertical-align: middle;float: none;">
        <a href="post-20170324.html">
        <h2>XMCDA 2.2.2</h2>
        </a>
        <p><span class="glyphicon glyphicon-time"></span> Posted on 24 March 2017</p>
        <p>XMCDA version 2.2.2 has been released on March, 24th 2017.</p>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-1 col-sm-2 col-xs-3" style = "display: inline-block;vertical-align: middle;float: none;">
        <img src="_static/ewgmcda.png" style="width:50px">
      </div><!--
    --><div class="col-md-11 col-sm-10 col-xs-9" style = "display: inline-block;vertical-align: middle;float: none;">
        <a href="post-20170222.html">
        <h2>EWG-MCDA support</h2>
        </a>
        <p><span class="glyphicon glyphicon-time"></span> Posted on 22 February 2017</p>
        <p>Decision Deck is proud to rely on the renewed support of the `EURO Working Group on MultiCriteria Decision Aiding <http://www.cs.put.poznan.pl/ewgmcda/>`_ (EWG-MCDA).</p>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-1 col-sm-2 col-xs-3" style = "display: inline-block;vertical-align: middle;float: none;">
        <img src="_static/xmcdalogo.png" style="width:50px">
      </div><!--
    --><div class="col-md-11 col-sm-10 col-xs-9" style = "display: inline-block;vertical-align: middle;float: none;">
        <a href="post-20161125.html">
        <h2>XMCDA 3.0.1</h2>
        </a>
        <p><span class="glyphicon glyphicon-time"></span> Posted on 25 November 2016</p>
        <p>XMCDA version 3.0.1 has been released on November, 25th 2016.</p>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-1 col-sm-2 col-xs-3" style = "display: inline-block;vertical-align: middle;float: none;">
        <img src="_static/ddlogo.png" style="width:50px">
      </div><!--
    --><div class="col-md-11 col-sm-10 col-xs-9" style = "display: inline-block;vertical-align: middle;float: none;">
        <a href="post-2016101401.html">
        <h2>13th Decision Deck Workshop</h2>
        </a>
        <p><span class="glyphicon glyphicon-time"></span> Posted on 14 October 2016</p>
        <p>The 13th Decision Deck Workshop is taking place at École Centrale Paris on the 14 October 2016. Workshop page: `link <http://www.lgi.ecp.fr/~mousseau/D2Workshop/pmwiki-2.2.7/pmwiki.php/Main/HomeP ...</p>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-1 col-sm-2 col-xs-3" style = "display: inline-block;vertical-align: middle;float: none;">
        <img src="_static/xmcdalogo.png" style="width:50px">
      </div><!--
    --><div class="col-md-11 col-sm-10 col-xs-9" style = "display: inline-block;vertical-align: middle;float: none;">
        <a href="post-20160908.html">
        <h2>XMCDA v2/v3 Java reference library</h2>
        </a>
        <p><span class="glyphicon glyphicon-time"></span> Posted on 8 September 2016</p>
        <p>First release of the 'XMCDA v2/v3 Java reference library <https://www.decision-deck.org/xmcda/developers.html#xmcda-java-reference-library>'_</p>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-1 col-sm-2 col-xs-3" style = "display: inline-block;vertical-align: middle;float: none;">
        <img src="_static/divizlogo2.png" style="width:50px">
      </div><!--
    --><div class="col-md-11 col-sm-10 col-xs-9" style = "display: inline-block;vertical-align: middle;float: none;">
        <a href="post-2015120201.html">
        <h2>diviz 1.15.1 "Ghosts & Goblins"</h2>
        </a>
        <p><span class="glyphicon glyphicon-time"></span> Posted on 2 December 2015</p>
        <p>A new version of diviz (1.15.1 "Ghosts & Goblins") has been released! `Download it! <https://www.decision-deck.org/downloads>`_</p>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-1 col-sm-2 col-xs-3" style = "display: inline-block;vertical-align: middle;float: none;">
        <img src="_static/divizlogo2.png" style="width:50px">
      </div><!--
    --><div class="col-md-11 col-sm-10 col-xs-9" style = "display: inline-block;vertical-align: middle;float: none;">
        <a href="post-20151126.html">
        <h2>diviz 1.15 "Ghosts & Goblins"</h2>
        </a>
        <p><span class="glyphicon glyphicon-time"></span> Posted on 26 Novermber 2015</p>
        <p>A new version of diviz (1.15 "Ghosts & Goblins") has been released! `Download it! <https://www.decision-deck.org/downloads>`_</p>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-1 col-sm-2 col-xs-3" style = "display: inline-block;vertical-align: middle;float: none;">
        <img src="_static/divizlogo2.png" style="width:50px">
      </div><!--
    --><div class="col-md-11 col-sm-10 col-xs-9" style = "display: inline-block;vertical-align: middle;float: none;">
        <a href="post-20150507.html">
        <h2>diviz 1.14 "Hells Bells"</h2>
        </a>
        <p><span class="glyphicon glyphicon-time"></span> Posted on 7 May 2015</p>
        <p>A new version of diviz (1.14 "Hells Bells") has been released! `Download it! <https://www.decision-deck.org/downloads>`_</p>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-1 col-sm-2 col-xs-3" style = "display: inline-block;vertical-align: middle;float: none;">
        <img src="_static/ddlogo.png" style="width:50px">
      </div><!--
    --><div class="col-md-11 col-sm-10 col-xs-9" style = "display: inline-block;vertical-align: middle;float: none;">
        <a href="post-20141130.html">
        <h2>12th Decision Deck Workshop</h2>
        </a>
        <p><span class="glyphicon glyphicon-time"></span> Posted on 30 November 2014</p>
        <p>The 12th Decision Deck Workshop is taking place at Télécom Bretagne, Brest on the 30 and 31 October 2016. Workshop page: ... Program: ~~~~~~~~ ...</p>
      </div>
    </div>


  <ul class="pagination">
    <li><a href="index1.html">&laquo;</a></li>
    <li><a href="index.html">1</a></li>
    <li class="active"><a href="#">2</a></li>
    <li><a href="index3.html">3</a></li>
    <li><a href="index3.html">&raquo;</a></li>
  </ul>