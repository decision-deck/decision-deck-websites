.. role:: html(raw)
   :format: html

:html:`<div class="text-center">`

Newsletter
==========

:html:`<ul class="pagination" style=""><li class="disabled"><a href="#">&laquo;</a></li><li><a href="newsletter1.html">1</a></li><li class="disabled"><a href="#">&raquo;</a></li></ul>`

:html:`<div class="text-left">`

----------

:html:`<a data-toggle="collapse" href="#element1"><img src="_static/ddbanner.png" align="right" style="width:22%"/></a>`

What is Decision Deck?
----------------------

Decision Deck collaboratively develops Open Source software tools to support the Multi-Criteria Decision Aiding (MCDA) process. :html:`<a data-toggle="collapse" href="#element1">Open <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></a>`

:html:`<div id="element1" class="collapse">`

Its purpose is to provide **effective tools** for consultants, teachers and researchers who use MCDA tools for real world problems, didactic purposes and to test, compare and develop MCDA methods.

It's current initiatives are:

* **XMCDA**: A standardized XML recommendation to represent objects and data from the MCDA field which allows MCDA software to be interoperable;
* **XMCDA webservices**: Distributed computational MCDA resources, using the XMCDA standard;
* **diviz**: A software for designing, executing and sharing MCDA methods, algorithms and experiments;
* **R**: An initiative linking the statistical software and programming environment R and MCDA;
* **GIS**: An initiative linking Geographical Information Systems and MCDA;

:html:`<div style="text-align:center"><a data-toggle="collapse" href="#element1">Close <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span></a></div></div>`

----------

The Decision Deck Workshop
--------------------------

:html:`<a data-toggle="collapse" href="#element2"><img src="_static/lisbon.jpg" style="width:100%"></a>`

On the **26th of September 2018** our annual `workshop <http://ddws2018.idsswh.sysresearch.org>`_ took place in sunny Lisbon, hosted by José Luis Borbinha and his team in the INESC-ID laboratory, next to the Alameda campus of the `Instituto Superior Técnico <https://tecnico.ulisboa.pt/en/>`_. :html:`<a data-toggle="collapse" href="#element2">Open <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></a>`

:html:`<div id="element2" class="collapse">`

The program was full of interesting presentations which may be found below.

- `Latest news and developments <_static/slides15ddw1.pdf>`_, *Patrick Meyer*
- `Robust efficiency analysis methods on diviz <_static/slides15ddw2.pdf>`_, *Milosz Kadzinski, Anna Labijak*
- `How modelling assistants can be integrated in the SW tools to improve the effectiveness of decision aiding <_static/slides15ddw3.ppt>`_, *Maria Franca Norese, Andrea Profili, Antonino Scarelli*
- `Using PROMETHEE to rank the EU countries on Environmental Health and Ecosystem Vitality (EPI's indicators) from 2006 to 2016 <_static/slides15ddw4.pptx>`_, *Panagiota Digkoglou, Jason Papathanasiou*
- `Learning MCDA with the help of diviz <_static/slides15ddw5.pptx>`_, *Maria De Vicente y Oliva, Vincent Cliville, Jaime Manera Bassa*
- `Criteria weights assessment through prioritizations (WAP) software tool <_static/slides15ddw6.pdf>`_, *Athanasios Spyridakos, Nikos Tsotsolas, Isaak Vryzidis*
- `DECSPACE: A user-friendly web-based platform to explore MCDA methods <_static/slides15ddw7.pdf>`_, *Ana Sara Costa, Joao Amador, Ruben Rodrigues, Jose Rui Figueira, Jose Borbinha*
- `Integrating time into majority-rule sorting models: application to the cyber-defense context <_static/slides15ddw8.pdf>`_, *Arthur Valko, Alexandru-Liviu Olteanu, David Brosset, Patrick Meyer*

The next workshop will take place in **fall 2019** at `IMT Atlantique <www.imt-atlantique.fr>`_ in Brest, France.

.. image:: _static/brest.jpg
   :width: 100%

:html:`<div style="text-align:center"><a data-toggle="collapse" href="#element2">Close <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span></a></div></div>`

----------

DecSpace - a web-based MCDA platform crossing paths with diviz
--------------------------------------------------------------

:html:`<a data-toggle="collapse" href="#element6"><img src="_static/decspacebanner.png" align="right" style="width:40%"/></a>`

DecSpace is a platform that has been developed by students at the IST (Universidade de Lisboa). Its original motivation was to support the use of MCDA methods developed by local researchers, but soon it evolved into a platform that was getting very similar to diviz, but with a web interface. :html:`<a data-toggle="collapse" href="#element6">Open <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></a>`

:html:`<div id="element6" class="collapse">`

:html:`<a data-toggle="collapse" href="#element6"><img src="_static/decspace.png" style="width:100%"></a>`

José Figueira, researcher at the CEG-IST, and José Borbinha, researcher at the INESC-ID laboratory, set out to solve together the challenge of developing a web based platform for testing and using new MCDA methods. It was soon realized that DecSpace was becoming similar to the diviz initiative from Decision Deck and so the two teams sat together in order to explore potential collaborations. As a result, the development on DecSpace will now be done in collaboration with the Decision Deck team, and in close integration with diviz.

:html:`<div style="text-align:center"><a data-toggle="collapse" href="#element6">Close <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span></a></div></div>`

----------

:html:`<a data-toggle="collapse" href="#element3"><img src="_static/uig.png" align="right" style="width:30%"/></a>`

User-friendly interactions with diviz workflows
-----------------------------------------------

A new initiative is born in Decision Deck, whose goal is to generate *user-friendly interfaces* as *stand-alone programs* from diviz workflows for decision makers. :html:`<a data-toggle="collapse" href="#element3">Open <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></a>`

:html:`<div id="element3" class="collapse">`

The **diviz** workbench allows to design, execute and share *workflows* of MCDA algorithms. In those workflows, each of the algorithms can be separately configured, and data can easily be exchanged between them. However, for non-experts in MCDA, as, e.g. decision makers, modifying the input data of a workflow, or even parameterizing its algorithms, might be a complicated task. We are therefore working on the **diviz-UIG** initiative, which allows to easily generate *user-friendly interfaces* from diviz workflows. These programs allow the decision makers to interact easily with a diviz workflow, without requiring any knowledge on the underlying MCDA technique. 

.. image:: _static/diviz-UIG.png
   :width: 100%

In the above picture we can see a program which allows to interact with an *MR-Sort disaggregation / aggregation workflow*, and to visualize various data from a district heating optimization problem.


:html:`<div style="text-align:center"><a data-toggle="collapse" href="#element3">Close <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span></a></div></div>`

----------

:html:`<a data-toggle="collapse" href="#element4"><img src="_static/funmooc.png" align="right" style="width:22%"/></a>`

Learning MCDA with the help of diviz
------------------------------------

An MCDA MOOC (Massive Open Online Course) was developed by Université Grenoble Alpes, Université Savoie-Mont Blanc (both in France) and the Rey Juan Carlos University (Madrid, Spain), in which diviz is used to implement some of the methods contained in the course. :html:`<a data-toggle="collapse" href="#element4">Open <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></a>`

:html:`<div id="element4" class="collapse">`

We present here an MCDA course for which diviz is used for working with multicriteria decision aid cases/examples in the framework of a MOOC. The MOOC is oriented towards beginners and is available on the `FUN <https://www.fun-mooc.fr>`_ (France Univeristé Numérique) platform.

This course is aimed at a public, student or professional, who wants to know more about the existing tools to make "reasonable" decisions. The idea is to situate the context of decision-making when the optimization of a single criterion is not sufficient. The essential of the field is presented (typology of the problems of decision, concept of scale, outranking and aggregation) before detailing some of the most widely-used methods (AHP, ELECTRE III, MACBETH and UTA).

diviz is presented in a user-friendly approach to study decision aiding cases or examples implemented with ELECTRE III and UTA. According to the future development of the diviz webservices, diviz could also be used for MACBETH and AHP.

From more than 4000 initial inscriptions, about 400 students have followed the first part of the Mooc (principles), more than 100 have discovered the diviz software and its use for ELECTRE III and UTA. About 50 students have applied it at a case study (2/3 for ELECTRE III, and 1/3 for UTA). Surprisingly, students need just a little support by the authors of the Mooc through the dedicated forum, their autonomy being very good. We also note that the revision of the case study by others students reinforces the method learning.

The authors want to thank the diviz team for their availability and their guidance.

.. image:: _static/mooc.png
   :width: 100%

Picture extracted from  the FUN platform of the planned work for the week dedicated to diviz

:html:`<div style="text-align:center"><a data-toggle="collapse" href="#element4">Close <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span></a></div></div>`

----------

:html:`<a data-toggle="collapse" href="#element5"><img src="_static/rising.png" align="right" style="width:15%"/></a>`

An update on our initiatives
----------------------------

The number of available XMCDA webservices continues to grow, thanks in large part to the contributors from Poznan University of Technology (PUT), while diviz usage is also going up. The MCDA package for R has also acquired a steady and global following. :html:`<a data-toggle="collapse" href="#element5">Open <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></a>`

:html:`<div id="element5" class="collapse">`

At the time of this writing, diviz is set to surpass the number of users and connections from 2017 by the end of this year.

.. image:: _static/divizusage2018.png
   :width: 100%

Furthermore, it appears that the more and more complex workflows are being tested, since the number of webservice executions is increasing at an exponential rate. For instance, in 2016 we had around 15,000 executions, in 2017 we had 38,000, while so far this year we've had 72,000.

We wish to give special thanks to *Giota Digkoglou* for providing us with a *Greek translation* of diviz and *Maria de Vicente y Oliva* and *Jaime Manera Bassa* for a *Spanish* one.

**We are always looking for diviz translations in other languages. The process is very simple, so if you are motivated, please contact us !**

The R MCDA package also appears to be a popular choice, with the available statistics being in the form of numbers of downloads from unique IPs (illustrated below for October 2017 to October 2018).

.. image:: _static/rmcdamaplastY2018.png
   :width: 100%

.. image:: _static/rmcdamapEUlastY2018.png
   :width: 100%

:html:`<div style="text-align:center"><a data-toggle="collapse" href="#element5">Close <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span></a></div></div>`

:html:`</div></div>`

:html:`<ul class="pagination"><li class="disabled"><a href="#">&laquo;</a></li><li><a href="newsletter1.html">1</a></li><li class="disabled"><a href="#">&raquo;</a></li></ul>`



