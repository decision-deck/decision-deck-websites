import os
posts_per_page = 10
# get all txt files and reverse order them
txtfiles = []
for fname in os.listdir("./posts/"):
    if fname.endswith(".txt"):
        txtfiles.append(fname)
txtfiles.sort(reverse = True)
# for each txt file build its post file and the page file
total_posts = len(txtfiles)
total_index_pages = total_posts / posts_per_page
if total_posts % posts_per_page != 0:
    total_index_pages += 1
post_ct = 0
post_page_ct = 0
index_ct = 0
output_index = 'Latest news\n-----------\n\n`See the newsletter </news/newsletter1.html>`_\n\n.. raw:: html\n\n'
for fname in txtfiles:
    post_ct += 1
    post_page_ct += 1
    # read input file
    fi = open("./posts/" + fname, "r")
    lines = fi.read().splitlines()
    fi.close()
    # get data
    if len(lines) > 0:
        post_title = lines[0]
    else:
        post_title = 'Unknown'
    if len(lines) > 1:
        post_date = lines[1]
    else:
        post_date = 'Unknown'
    if len(lines) > 2:
        post_img_small = lines[2]
    else:
        post_img_small = ''
    if len(lines) > 3:
        post_img_big = lines[3]
    else:
        post_img_big = ''
    if len(lines) > 4:
        post_text = ''
        for line in lines[4:]:
           post_text += line + '\n'
        post_text_nowhitespace = ' '.join(post_text.split())
    else:
        post_text = ''
        post_text_nowhitespace = ''
    # create post page content
    output_post = post_title + '\n'
    for c in post_title:
        output_post += '-'
    output_post += '\n\n'
    output_post += '.. raw:: html\n\n  <hr>\n'
    output_post += '  <p><span class="glyphicon glyphicon-time"></span> Posted on ' + post_date + '</p>\n  <hr>\n'
    output_post += '  <img class="img-responsive" src="' + post_img_big + '" alt="" style="width:auto;max-height:300px">\n  <hr>\n\n'
    output_post += post_text
    # write post page file
    nfname = 'post-' + fname.split('.')[0] + '.rst'
    fo = open('./' + nfname, "w")
    fo.write(output_post)
    fo.close()
    # add content to index page
    output_index += '    <div class="row">\n'
    output_index += '      <div class="col-md-1 col-sm-2 col-xs-3" style = "display: inline-block;vertical-align: middle;float: none;">\n'
    output_index += '        <img src="' + post_img_small + '" style="width:50px">\n'
    output_index += '      </div><!--\n'
    output_index += '    --><div class="col-md-11 col-sm-10 col-xs-9" style = "display: inline-block;vertical-align: middle;float: none;">\n'
    output_index += '        <a href="post-' + fname.split('.')[0] + '.html">\n'
    output_index += '        <h2>' + post_title + '</h2>\n'
    output_index += '        </a>\n'
    output_index += '        <p><span class="glyphicon glyphicon-time"></span> Posted on ' + post_date + '</p>\n'
    if len(post_text_nowhitespace) > 200:
        post_text_nowhitespace = post_text_nowhitespace[:196] + ' ...'
    output_index += '        <p>' + post_text_nowhitespace + '</p>\n'
    output_index += '      </div>\n'
    output_index += '    </div>\n'
    if post_page_ct < posts_per_page and post_ct < total_posts:
        output_index += '    <hr>\n'
    else:
        post_page_ct = 0
        # add pagination
        output_index += '\n\n'
        output_index += '  <ul class="pagination">\n'
        prev_index = index_ct
        if prev_index == 0:
            output_index += '    <li class="disabled"><a href="#">&laquo;</a></li>\n'
        else:
            output_index += '    <li><a href="index%d.html">&laquo;</a></li>\n'%prev_index
        for i in range(total_index_pages):
            if i == index_ct:
                output_index += '    <li class="active"><a href="#">%d</a></li>\n'%(i+1)
            else:
                if i == 0:
                    output_index += '    <li><a href="index.html">%d</a></li>\n'%(i+1)
                else:
                    output_index += '    <li><a href="index%d.html">%d</a></li>\n'%(i+1,i+1)
        next_index = index_ct + 2
        if next_index == total_index_pages + 1:
            output_index += '    <li class="disabled"><a href="#">&raquo;</a></li>\n'
        else:
            output_index += '    <li><a href="index%d.html">&raquo;</a></li>\n'%next_index
        output_index += '  </ul>'
        # write index file
        ifname = 'index.rst'
        if index_ct > 0:
            ifname = 'index%d.rst'%(index_ct + 1)
        fo = open('./' + ifname, "w")
        fo.write(output_index)
        fo.close()
        output_index = 'Latest news\n-----------\n\n`See the newsletter </newsletter1.html>`_\n\n.. raw:: html\n\n'
        index_ct += 1

