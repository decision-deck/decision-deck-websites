diviz v17.1 «Civilization»
26 July 2018
_static/divizlogo2.png
_static/diviz_big.png
A new version of diviz (v17.2 a.k.a. «Civilization») has been released! `Download it! </diviz/download_v17.2.html>`_  It fixes an important bug in v17 where diviz could not start on system where the default language was not supported by diviz.
