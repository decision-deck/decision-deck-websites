XMCDA 3.1.0
31st January 2019
_static/xmcdalogo.png
_static/xmcdalogo.png
XMCDA version 3.1.0 has been released.
It introduces two new tags: ``criteriaHierarchy`` and ``criteriaSetsHierarchy``.
