.. index:: accessing
.. _accessing:

.. |br| raw:: html

   <br />

Accessing the XMCDA web services
================================

Currently there exist 2 ways to access the XMCDA web services:

 - standard :ref:`soap-requests`, detailed below;
 - the `diviz <//www.diviz.org>`_ software;

The standard `SOAP <https://en.wikipedia.org/wiki/SOAP>`_ requests allow you (among other things) to integrate the call of the web services into your own application.

The `diviz <//www.diviz.org>`_ software allows you to build execution workflows of XMCDA web services via a Graphical User Interface.


In the following, we detail how to directly call the web-services with the SOAP protocol, using a dedicated python3 script we provide.  For diviz, please refer to `<//www.diviz.org>`_.

.. _soap-requests:

SOAP requests
-------------

All web-services are available through the standard `SOAP protocol <https://en.wikipedia.org/wiki/SOAP>`_.

The URLs under which they are published take the following form::

   https://webservices.decision-deck.org/soap/<service_name>.py

The ``<service_name>`` of each web-service can be found in its documentation, along with the description of its input and output parameters; this documentation can be found `here <wsindex-1.html>`_.


Example: using a python script
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
A python (python 3) script accessing those web-services is available `here: ddws_soap_client.py <https://gitlab.com/decision-deck/decision-deck-webservices-infrastructure/raw/master/python/ddws_soap_client.py>`_. |br|
You also need to install `the simplesoap library <https://pypi.python.org/pypi/PySimpleSOAP/>`_ to make it work.

In this example, we want to call the service named 'rankAlternativesValues-RXMCDA'.
Its :doc:`documentation <wsd-rankAlternativesValues-RXMCDA>` states that two inputs are mandatory:

- alternatives: the set of alternatives
- overallValues: the overall numeric value for each alternative.

We have examples of such inputs, you'll get them here: `rankAlternativesValues-RXMCDA/tests/in1 <https://gitlab.com/decision-deck/decision-deck-webservices-infrastructure/tree/master/XMCDAWebServices/rankAlternativesValues-RXMCDA/tests/in1>`_

Say we go for 'tests/in1/', there are two files:

- `alternatives.xml <https://gitlab.com/decision-deck/decision-deck-webservices-infrastructure/raw/master/XMCDAWebServices/rankAlternativesValues-RXMCDA/tests/in1/alternatives.xml>`_

- `overallValues.xml  <https://gitlab.com/decision-deck/decision-deck-webservices-infrastructure/raw/master/XMCDAWebServices/rankAlternativesValues-RXMCDA/tests/in1/overallValues.xml>`_

Now, the command-line to submit a new job to the service ``rankAlternativesValues-RXMCDA`` with those two files is the following::

    python3 ./ddws_soap_client.py -n rankAlternativesValues-RXMCDA \
            --submit-problem alternatives:./alternatives.xml   \
                             overallValues:./overallValues.xml

The job is submitted, and after having submitted it successfully, a ticket number is returned which we use to retrieve the results. |br|
Suppose we received the ticket 'orPiIXdej7SVgOLf', let's ask for our results:

::

    python3 ./ddws_soap_client.py -n rankAlternativesValues-RXMCDA \
            --request-solution orPiIXdej7SVgOLf

If everything goes fine, the results are dumped into the current directory and the script prints out their names on the standard output:  we have just received two files, 'alternativesRank.xml' and 'messages.xml'.
Again, the :ref:`documentation <rankAlternativesValues-RXMCDA_outputs>` tells us that this is what we expect: the latter contains informational messages, while the former is the result itself: the ranking of the alternatives supplied as input.

Of course, this example does not take long to execute: the dataset is small and the program has a simple task to perform, so by the time we typed or copy-pasted the above command lines, the job is finished and you get the results.  However, if you combine real-life data with complex algorithm this can result in jobs requests which require minutes, hours, or even days to complete.  This is why every XMCDA web-service was designed this way: decoupling the task submission from the request for results makes it possible to execute long-running jobs without having to keep a program running, or fearing that the HTTP connection drops as some point.

What happens if you request a solution but the service has not finished computing it yet?  Then the script will appear to hang a little and then returns, without printing anything (except if we ask it to be more verbose by adding the option ``-v``).  The default is 60 seconds, it can be changed with option ``-t``.  When returning without a solution, the script's exit status is 2; as expected, the return status is 0 when results are available and have been successfully retrieved.

Last, the submission of a job and the request for the results can be done with one single command::

    python ./ddws_soap_client.py -n rankAlternativesValues-RXMCDA  \
            --submit-and-wait-solution alternatives:./alternatives.xml \
                                       overallValues:./overallValues.xml

If the script fails to retrieve the result within the defined period, again it exits with an exit status equal to 2.  The results can be retrieved later of course, using the ticket number that was given.

*Integration into a shell script:*
  This script should be simple to integrate into a shell script.

  - When submitting a new task, only one thing is printed on the standard input: the ticket number.  In bash, you get it simply like in: ``ticketNumber=$(python ./ddws_soap_client.py -s ...)``.

  - When requesting results, only the names of the returned files are printed on the standard output, one per line.

  - the exit status is 0 if everything goes fine, 2 if the timeout was reached when waiting for solutions.

  Options ``-h`` or ``--help`` gives all the details.
