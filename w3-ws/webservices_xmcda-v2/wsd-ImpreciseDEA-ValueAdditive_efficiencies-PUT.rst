.. _ImpreciseDEA-ValueAdditive_efficiencies-PUT:

ImpreciseDEA-ValueAdditive_efficiencies
=======================================

:Version: 1.0
:Provider: PUT
:SOAP service's name: ``ImpreciseDEA-ValueAdditive_efficiencies-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Computes efficiency scores for the given DMUs (alternatives)  using Additive Data Envelopment Analysis Model with imprecise information.

**Contact:** Anna Labijak <support@decision-deck.org>


Inputs
------
(For outputs, see :ref:`below <ImpreciseDEA-ValueAdditive_efficiencies-PUT_outputs>`)


- :ref:`units <ImpreciseDEA-ValueAdditive_efficiencies-PUT-units>`
- :ref:`inputsOutputs <ImpreciseDEA-ValueAdditive_efficiencies-PUT-inputsOutputs>`
- :ref:`performanceTable <ImpreciseDEA-ValueAdditive_efficiencies-PUT-performanceTable>`
- :ref:`maxPerformanceTable <ImpreciseDEA-ValueAdditive_efficiencies-PUT-maxPerformanceTable>` *(optional)*
- :ref:`weightsLinearConstraints <ImpreciseDEA-ValueAdditive_efficiencies-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <ImpreciseDEA-ValueAdditive_efficiencies-PUT-methodParameters>`


.. _ImpreciseDEA-ValueAdditive_efficiencies-PUT-units:

units
~~~~~

A list of alternatives (DMUs).

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>


------------------------


.. _ImpreciseDEA-ValueAdditive_efficiencies-PUT-inputsOutputs:

inputsOutputs
~~~~~~~~~~~~~

A list of criteria with specified preference direction. List has to contains at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output).

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   <criteria>
                        <criterion>
							<scale>
                                [...]
							</scale>
                            [...]
                        </criterion>
                        [...]
                    </criteria>


------------------------


.. _ImpreciseDEA-ValueAdditive_efficiencies-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A list of alternatives (DMUs) minimal performances (or exact performances if intervals should be created by tolerance).

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>


------------------------


.. _ImpreciseDEA-ValueAdditive_efficiencies-PUT-maxPerformanceTable:

maxPerformanceTable *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of alternatives (DMUs) maximal performances.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>


------------------------


.. _ImpreciseDEA-ValueAdditive_efficiencies-PUT-weightsLinearConstraints:

weightsLinearConstraints *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of criteria linear constraints.

The input value should be a valid XMCDA document whose main tag is ``<criteriaLinearConstraints>``.
It must have the following form::

   <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID> [...] </criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>


------------------------


.. _ImpreciseDEA-ValueAdditive_efficiencies-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~

Represents method parameters.
            "tolerance" represents the fraction for creating interval data (created interval: [data*(1-tolerance), data*(1+tolerance)]),
            "transformToUtilities" means if data should be tranformed into values from range [0-1],
            "boundariesProvided" means if inputsOutputs file contains information about min and max data for each factor,
            "functionShapeProvided" means if inputsOutputs file contains information about the shapes of value function for given factor.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
	<methodParameters>
		<parameter id="tolerance">
			<value><real>%1</real></value>
		</parameter>
		<parameter id="transformToUtilities">
			<value><boolean>%2</boolean></value>
		</parameter>
		<parameter id="boundariesProvided">
			<value><boolean>%3</boolean></value>
		</parameter>
		<parameter id="functionShapeProvided">
			<value><boolean>%4</boolean></value>
		</parameter>
	</methodParameters>


where:

- **%1** is a parameter named "tolerance". This is a float, and the value should conform to the following constraint: The value should be non-negative..  More formally, the constraint is::

     %1 >= 0
  The default value is 0.00.

- **%2** is a parameter named "transform to utilities". This is a boolean.
  The default value is true.

- **%3** is a parameter named "boundaries provided". This is a boolean.
  The default value is false.

- **%4** is a parameter named "function shapes provided". This is a boolean.
  The default value is false.


------------------------



.. _ImpreciseDEA-ValueAdditive_efficiencies-PUT_outputs:

Outputs
-------


- :ref:`minEfficiency <ImpreciseDEA-ValueAdditive_efficiencies-PUT-minEfficiency>`
- :ref:`maxEfficiency <ImpreciseDEA-ValueAdditive_efficiencies-PUT-maxEfficiency>`
- :ref:`minDistance <ImpreciseDEA-ValueAdditive_efficiencies-PUT-minDistance>`
- :ref:`maxDistance <ImpreciseDEA-ValueAdditive_efficiencies-PUT-maxDistance>`
- :ref:`messages <ImpreciseDEA-ValueAdditive_efficiencies-PUT-messages>`


.. _ImpreciseDEA-ValueAdditive_efficiencies-PUT-minEfficiency:

minEfficiency
~~~~~~~~~~~~~

A list of alternatives with computed minimum efficiency scores.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   
                <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
						  <values>
                <value>
							  [...]
						    </value>
              </values>
						</alternativeValue>
						[...]
					</alternativesValues>


------------------------


.. _ImpreciseDEA-ValueAdditive_efficiencies-PUT-maxEfficiency:

maxEfficiency
~~~~~~~~~~~~~

A list of alternatives with computed maximum efficiency scores.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   
                <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
						  <values>
                <value>
							  [...]
						    </value>
              </values>
						</alternativeValue>
						[...]
					</alternativesValues>


------------------------


.. _ImpreciseDEA-ValueAdditive_efficiencies-PUT-minDistance:

minDistance
~~~~~~~~~~~

A list of alternatives with computed minimal distance to efficienct frontier.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   
         <alternativesValues">
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
						  <values>
                <value>
							  [...]
						    </value>
              </values>
						</alternativeValue>
						[...]
					</alternativesValues>
       


------------------------


.. _ImpreciseDEA-ValueAdditive_efficiencies-PUT-maxDistance:

maxDistance
~~~~~~~~~~~

A list of alternatives with computed maximal distrance fo efficient frontier.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   
         <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
						  <values>
                <value>
							  [...]
						    </value>
              </values>
						</alternativeValue>
						[...]
					</alternativesValues>
       


------------------------


.. _ImpreciseDEA-ValueAdditive_efficiencies-PUT-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/ImpreciseDEA-ValueAdditive_efficiencies-PUT/description-wsDD.xml>`
