.. _plotFuzzyCategoriesValues-ITTB:

plotFuzzyCategoriesValues
=========================

:Version: 1.0
:Provider: ITTB
:SOAP service's name: ``plotFuzzyCategoriesValues-ITTB`` (see :ref:`soap-requests` for details)

Description
-----------

This web service allows to plot fuzzy sets. There are some options to take into account: title, X-axis label, Y-axis label and the use of colors.

**Contact:** Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)


Inputs
------
(For outputs, see :ref:`below <plotFuzzyCategoriesValues-ITTB_outputs>`)


- :ref:`fuzzyCategoriesValues <plotFuzzyCategoriesValues-ITTB-fuzzyCategoriesValues>`
- :ref:`options <plotFuzzyCategoriesValues-ITTB-options>`


.. _plotFuzzyCategoriesValues-ITTB-fuzzyCategoriesValues:

fuzzyCategoriesValues
~~~~~~~~~~~~~~~~~~~~~

A set of fuzzy numbers: trapezoidal or triangular. The abscissa and ordinate evaluations should be only real or integer numeric values, i.e. <real> or <integer>..

The input value should be a valid XMCDA document whose main tag is ``<categoriesValues>``.
It must have the following form::

   
                            
                   <categoriesValues>
		              <categoryValue>
			             <values>
			                 <value id=[...] name=[...]>
                	           <fuzzyNumber>
                	 	             <trapezoidal>
                	 		                <point1>
	                	 		               <abscissa><real>[...]</real></abscissa>
	                	 		               <ordinate><integer>[...]</integer></ordinate>
	                	 	                </point1>
	                	 	                <point2>
	                	 		               <abscissa><real>[...]</real></abscissa>
	                	 		               <ordinate><real>[...]</real></ordinate>
	                	 	                </point2>
	                	 	                <point3>
	                	 		               <abscissa><integer>[...]</integer></abscissa>
	                	 		               <ordinate><integer>[...]</integer></ordinate>
	                	 	                </point3>
	                	 	                <point4>
		                	 	               <abscissa><integer>[...]</integer></abscissa>
		                	 	               <ordinate><real>[...]</real></ordinate>
	                	 	                </point4>
                	 	          </trapezoidal>
                	           </fuzzyNumber>
                	         </value>
			                 <value id=[...] name=[...]>
                	           <fuzzyNumber>
                	 	             <triangular>
                	 		                <point1>
	                	 		               <abscissa><real>[...]</real></abscissa>
	                	 		               <ordinate><real>[...]</real></ordinate>
	                	 	                </point1>
	                	 	                <point2>
	                	 		               <abscissa><integer>[...]</integer></abscissa>
	                	 		               <ordinate><real>[...]</real></ordinate>
	                	 	                </point2>
	                	 	                <point3>
	                	 		               <abscissa><integer>[...]</integer></abscissa>
	                	 		               <ordinate><integer>[...]</integer></ordinate>
	                	 	                </point3>
                	 	          </triangular>
                	           </fuzzyNumber>
                            </value>
                            [...]
			             </values>
                      </categoryValue>
	              </categoriesValues>
                    
                     


------------------------


.. _plotFuzzyCategoriesValues-ITTB-options:

options
~~~~~~~

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                   
					<methodParameters>	
						  <parameter id= "chart_title" name="Chart title">
							<value>
            					<label>%1</label>
       						 </value>
						</parameter>
						<parameter id="x_axis" name="X-axis label">
							<value>
            					<label>%2</label>
       						 </value>
						</parameter >
						<parameter id="y_axis" name="Y-axis label">
							<value>
            					<label>%3</label>
       						 </value>
						</parameter>
						 <parameter id="use_color" name="Use colors">
							<value>
            					<label>%4</label>
       						 </value>
						</parameter>
					</methodParameters>
				
               

where:

- **%1** is a parameter named "Chart title:". This is a string.
- **%2** is a parameter named "X-axis label:". This is a string.
- **%3** is a parameter named "Y axis label:". This is a string.
- **%4** is a parameter named "Use Colors?". It can have the following values:

  - ``true``: Yes

  - ``false``: No

  The default value is false.


------------------------



.. _plotFuzzyCategoriesValues-ITTB_outputs:

Outputs
-------


- :ref:`fuzzyCategoriesValuesPlot <plotFuzzyCategoriesValues-ITTB-fuzzyCategoriesValuesPlot>`
- :ref:`messages <plotFuzzyCategoriesValues-ITTB-messages>`


.. _plotFuzzyCategoriesValues-ITTB-fuzzyCategoriesValuesPlot:

fuzzyCategoriesValuesPlot
~~~~~~~~~~~~~~~~~~~~~~~~~

A string containing the base64 representation of the png image of the generated plot.

The returned value is a XMCDA document whose main tag is ``<categoriesValues>``.


------------------------


.. _plotFuzzyCategoriesValues-ITTB-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/plotFuzzyCategoriesValues-ITTB/description-wsDD.xml>`
