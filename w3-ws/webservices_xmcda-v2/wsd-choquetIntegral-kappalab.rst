.. _choquetIntegral-kappalab:

choquetIntegral
===============

:Version: 1.0
:Provider: kappalab
:SOAP service's name: ``choquetIntegral-kappalab`` (see :ref:`soap-requests` for details)

Description
-----------

Computes the Choquet integral from a normalised Mobius transform of a capacity.

**Contact:** Patrick Meyer (patrick.meyer@telecom-bretagne.eu)


Inputs
------
(For outputs, see :ref:`below <choquetIntegral-kappalab_outputs>`)


- :ref:`criteria <choquetIntegral-kappalab-criteria>`
- :ref:`alternatives <choquetIntegral-kappalab-alternatives>`
- :ref:`performanceTable <choquetIntegral-kappalab-performanceTable>`
- :ref:`mobiusCapacity <choquetIntegral-kappalab-mobiusCapacity>`
- :ref:`kAdditivity <choquetIntegral-kappalab-kAdditivity>`


.. _choquetIntegral-kappalab-criteria:

criteria
~~~~~~~~

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
                
                    <criteria>
                        <criterion>
                            <active>[...]</active>
                            [...]
                        </criterion>
                        [...]
                    </criteria>
                    
            


------------------------


.. _choquetIntegral-kappalab-alternatives:

alternatives
~~~~~~~~~~~~

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
                
                    <alternatives>
                        <alternative>
                            <active>[...]</active>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
                    
            


------------------------


.. _choquetIntegral-kappalab-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A performance table. The evaluations should be numeric values, i.e. <real>, <integer> or <rational>.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _choquetIntegral-kappalab-mobiusCapacity:

mobiusCapacity
~~~~~~~~~~~~~~

The Mobius transform of a capacity.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.
It must have the following form::

   
                
                    <criteriaValues>
                        <criterionValue>
                            <criteriaSet>
                                <element>
                                    <criterionID>[...]</criterionID>
                                </element>
                                [...]
                            </criteriaSet>
                            <value>
                                <real>[...]</real>
                            </value>
                        </criterionValue>
                        [...]
                    </criteriaValues>
                    
            


------------------------


.. _choquetIntegral-kappalab-kAdditivity:

kAdditivity
~~~~~~~~~~~

Indicates the level of k-additivity of the Mobius capacity (the Mobius transform of subsets whose cardinal is superior to k vanishes).

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                
                    <methodParameters>
                        <parameter
                             name="kAdditivity"> <!-- REQUIRED  -->
                            <value>
                                <integer>%1</integer>
                            </value>
                        </parameter>
                    </methodParameters>
                    
            

where:

- **%1** is a parameter named "kAdditivity". This is a int, and the value should conform to the following constraint: The value should be a positive integer, less than or equal to the number of criteria..  More formally, the constraint is::

      %1 > 0 
  The default value is 1.


------------------------



.. _choquetIntegral-kappalab_outputs:

Outputs
-------


- :ref:`overallValues <choquetIntegral-kappalab-overallValues>`
- :ref:`messages <choquetIntegral-kappalab-messages>`


.. _choquetIntegral-kappalab-overallValues:

overallValues
~~~~~~~~~~~~~

The overall values of the alternatives.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   
                
                    <alternativesValues mcdaConcept="overallValues">
                        <alternativeValue>
                            [...]
                        </alternativeValue>
                        [...]
                    </alternativesValues>
                    
            


------------------------


.. _choquetIntegral-kappalab-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/choquetIntegral-kappalab/description-wsDD.xml>`
