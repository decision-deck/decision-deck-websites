.. _PrometheeFlows-J-MCDA:

PrometheeFlows
==============

:Version: 0.5.5
:Provider: J-MCDA
:SOAP service's name: ``PrometheeFlows-J-MCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Computes Promethee flows (net flows, positive flows, or negative flows).

**Contact:** Olivier Cailloux <olivier.cailloux@ecp.fr>

**Web page:** http://sourceforge.net/projects/j-mcda/

**Reference:** Cailloux, Olivier. Electre and Promethee MCDA methods as reusable software components. In Proceedings of the 25th Mini-EURO Conference on Uncertainty and Robustness in Planning and Decision Making (URPDM 2010). Coimbra, Portugal, 2010.


Inputs
------
(For outputs, see :ref:`below <PrometheeFlows-J-MCDA_outputs>`)


- :ref:`alternatives <PrometheeFlows-J-MCDA-input0>` *(optional)*
- :ref:`preference <PrometheeFlows-J-MCDA-input1>`
- :ref:`flow_type <PrometheeFlows-J-MCDA-input2>`


.. _PrometheeFlows-J-MCDA-input0:

alternatives *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~

The alternatives to consider. Set some alternatives as inactive (or remove them) to ignore them.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _PrometheeFlows-J-MCDA-input1:

preference
~~~~~~~~~~

The preference relation from which to compute flows.

The input value should be a valid XMCDA document whose main tag is ``<alternativesComparisons>``.


------------------------


.. _PrometheeFlows-J-MCDA-input2:

flow_type
~~~~~~~~~

The type of flow to compute: positive, negative or net.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   

      <methodParameters>
         <parameter>
               <value>
                  <label>%1</label>
               </value>
         </parameter>
      </methodParameters>

			

where:

- **%1** is a parameter named "flow_type". It can have the following values:

  - ``POSITIVE``: The positive flow.

  - ``NEGATIVE``: The negative flow.

  - ``NET``: The net flow.

  The default value is item0.


------------------------



.. _PrometheeFlows-J-MCDA_outputs:

Outputs
-------


- :ref:`flows <PrometheeFlows-J-MCDA-output0>`
- :ref:`messages <PrometheeFlows-J-MCDA-output1>`


.. _PrometheeFlows-J-MCDA-output0:

flows
~~~~~

The flows computed from the given data.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _PrometheeFlows-J-MCDA-output1:

messages
~~~~~~~~

A status message.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/PrometheeFlows-J-MCDA/description-wsDD.xml>`
