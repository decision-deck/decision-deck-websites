.. _RORUTADIS-GroupAssignments-PUT:

RORUTADIS-GroupAssignments
==========================

:Version: 0.1
:Provider: PUT
:SOAP service's name: ``RORUTADIS-GroupAssignments-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

This service allows for merging different assignments, e.g. from various decision makers (group result, group assignment). Service developed by Krzysztof Ciomek (Poznan University of Technology, under supervision of Milosz Kadzinski).

**Contact:** 
			Krzysztof Ciomek (k.ciomek@gmail.com),
			Milosz Kadzinski (milosz.kadzinski@cs.put.poznan.pl)
		

**Web page:** https://github.com/kciomek/rorutadis

**Reference:** None


Inputs
------
(For outputs, see :ref:`below <RORUTADIS-GroupAssignments-PUT_outputs>`)


- :ref:`alternatives <RORUTADIS-GroupAssignments-PUT-alternatives>`
- :ref:`categories <RORUTADIS-GroupAssignments-PUT-categories>`
- :ref:`possibleAssignmentsDM1 <RORUTADIS-GroupAssignments-PUT-possibleAssignmentsDM1>`
- :ref:`necessaryAssignmentsDM1 <RORUTADIS-GroupAssignments-PUT-necessaryAssignmentsDM1>`
- :ref:`possibleAssignmentsDM2 <RORUTADIS-GroupAssignments-PUT-possibleAssignmentsDM2>` *(optional)*
- :ref:`necessaryAssignmentsDM2 <RORUTADIS-GroupAssignments-PUT-necessaryAssignmentsDM2>` *(optional)*
- :ref:`possibleAssignmentsDM3 <RORUTADIS-GroupAssignments-PUT-possibleAssignmentsDM3>` *(optional)*
- :ref:`necessaryAssignmentsDM3 <RORUTADIS-GroupAssignments-PUT-necessaryAssignmentsDM3>` *(optional)*
- :ref:`possibleAssignmentsDM4 <RORUTADIS-GroupAssignments-PUT-possibleAssignmentsDM4>` *(optional)*
- :ref:`necessaryAssignmentsDM4 <RORUTADIS-GroupAssignments-PUT-necessaryAssignmentsDM4>` *(optional)*
- :ref:`possibleAssignmentsDM5 <RORUTADIS-GroupAssignments-PUT-possibleAssignmentsDM5>` *(optional)*
- :ref:`necessaryAssignmentsDM5 <RORUTADIS-GroupAssignments-PUT-necessaryAssignmentsDM5>` *(optional)*
- :ref:`possibleAssignmentsDM6 <RORUTADIS-GroupAssignments-PUT-possibleAssignmentsDM6>` *(optional)*
- :ref:`necessaryAssignmentsDM6 <RORUTADIS-GroupAssignments-PUT-necessaryAssignmentsDM6>` *(optional)*
- :ref:`possibleAssignmentsDM7 <RORUTADIS-GroupAssignments-PUT-possibleAssignmentsDM7>` *(optional)*
- :ref:`necessaryAssignmentsDM7 <RORUTADIS-GroupAssignments-PUT-necessaryAssignmentsDM7>` *(optional)*
- :ref:`possibleAssignmentsDM8 <RORUTADIS-GroupAssignments-PUT-possibleAssignmentsDM8>` *(optional)*
- :ref:`necessaryAssignmentsDM8 <RORUTADIS-GroupAssignments-PUT-necessaryAssignmentsDM8>` *(optional)*


.. _RORUTADIS-GroupAssignments-PUT-alternatives:

alternatives
~~~~~~~~~~~~

A list of alternatives.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
				
					<alternatives>
                        <alternative id="[...]">
                            <active>[...]</active>
                        </alternative>
                        [...]
                    </alternatives>
				
			


------------------------


.. _RORUTADIS-GroupAssignments-PUT-categories:

categories
~~~~~~~~~~

A list of categories (classes). List must be sorted from the worst category to the best.

The input value should be a valid XMCDA document whose main tag is ``<categories>``.
It must have the following form::

   
				
					<categories>
                        <category id="[...]" />
                        [...]
                    </categories>
				
			


------------------------


.. _RORUTADIS-GroupAssignments-PUT-possibleAssignmentsDM1:

possibleAssignmentsDM1
~~~~~~~~~~~~~~~~~~~~~~

Possible assignments computed for Decision Maker 1.

The input value should be a valid XMCDA document whose main tag is ``<alternativesAffectations>``.
It must have the following form::

   
				
					<alternativesAffectations>
						<alternativeAffectation>
							<alternativeID>[...]</alternativeID>
							<categoryID>[...]</categoryID>
						</alternativeAffectation>
						[...]
						<alternativeAffectation>
							<alternativeID>[...]</alternativeID>
							<categoriesInterval>
								<lowerBound>
									<categoryID>[...]</categoryID>
								</lowerBound>
								<upperBound>
									<categoryID>[...]</categoryID>
								</upperBound>
							</categoriesInterval>
						</alternativeAffectation>
						[...]
						<alternativeAffectation>
							<alternativeID>[...]</alternativeID>
							<categoriesSet>
								<categoryID>[...]</categoryID>
								[...]
							</categoriesSet>
						</alternativeAffectation>
						[...]
					</alternativesAffectations>
				
			


------------------------


.. _RORUTADIS-GroupAssignments-PUT-necessaryAssignmentsDM1:

necessaryAssignmentsDM1
~~~~~~~~~~~~~~~~~~~~~~~

Necessary assignments computed for Decision Maker 1.

The input value should be a valid XMCDA document whose main tag is ``<alternativesAffectations>``.


------------------------


.. _RORUTADIS-GroupAssignments-PUT-possibleAssignmentsDM2:

possibleAssignmentsDM2 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Possible assignments computed for Decision Maker 2.

The input value should be a valid XMCDA document whose main tag is ``<alternativesAffectations>``.


------------------------


.. _RORUTADIS-GroupAssignments-PUT-necessaryAssignmentsDM2:

necessaryAssignmentsDM2 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Necessary assignments computed for Decision Maker 2.

The input value should be a valid XMCDA document whose main tag is ``<alternativesAffectations>``.


------------------------


.. _RORUTADIS-GroupAssignments-PUT-possibleAssignmentsDM3:

possibleAssignmentsDM3 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Possible assignments computed for Decision Maker 3.

The input value should be a valid XMCDA document whose main tag is ``<alternativesAffectations>``.


------------------------


.. _RORUTADIS-GroupAssignments-PUT-necessaryAssignmentsDM3:

necessaryAssignmentsDM3 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Necessary assignments computed for Decision Maker 3.

The input value should be a valid XMCDA document whose main tag is ``<alternativesAffectations>``.


------------------------


.. _RORUTADIS-GroupAssignments-PUT-possibleAssignmentsDM4:

possibleAssignmentsDM4 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Possible assignments computed for Decision Maker 4.

The input value should be a valid XMCDA document whose main tag is ``<alternativesAffectations>``.


------------------------


.. _RORUTADIS-GroupAssignments-PUT-necessaryAssignmentsDM4:

necessaryAssignmentsDM4 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Necessary assignments computed for Decision Maker 4.

The input value should be a valid XMCDA document whose main tag is ``<alternativesAffectations>``.


------------------------


.. _RORUTADIS-GroupAssignments-PUT-possibleAssignmentsDM5:

possibleAssignmentsDM5 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Possible assignments computed for Decision Maker 5.

The input value should be a valid XMCDA document whose main tag is ``<alternativesAffectations>``.


------------------------


.. _RORUTADIS-GroupAssignments-PUT-necessaryAssignmentsDM5:

necessaryAssignmentsDM5 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Necessary assignments computed for Decision Maker 5.

The input value should be a valid XMCDA document whose main tag is ``<alternativesAffectations>``.


------------------------


.. _RORUTADIS-GroupAssignments-PUT-possibleAssignmentsDM6:

possibleAssignmentsDM6 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Possible assignments computed for Decision Maker 6.

The input value should be a valid XMCDA document whose main tag is ``<alternativesAffectations>``.


------------------------


.. _RORUTADIS-GroupAssignments-PUT-necessaryAssignmentsDM6:

necessaryAssignmentsDM6 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Necessary assignments computed for Decision Maker 6.

The input value should be a valid XMCDA document whose main tag is ``<alternativesAffectations>``.


------------------------


.. _RORUTADIS-GroupAssignments-PUT-possibleAssignmentsDM7:

possibleAssignmentsDM7 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Possible assignments computed for Decision Maker 7.

The input value should be a valid XMCDA document whose main tag is ``<alternativesAffectations>``.


------------------------


.. _RORUTADIS-GroupAssignments-PUT-necessaryAssignmentsDM7:

necessaryAssignmentsDM7 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Necessary assignments computed for Decision Maker 7.

The input value should be a valid XMCDA document whose main tag is ``<alternativesAffectations>``.


------------------------


.. _RORUTADIS-GroupAssignments-PUT-possibleAssignmentsDM8:

possibleAssignmentsDM8 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Possible assignments computed for Decision Maker 8.

The input value should be a valid XMCDA document whose main tag is ``<alternativesAffectations>``.


------------------------


.. _RORUTADIS-GroupAssignments-PUT-necessaryAssignmentsDM8:

necessaryAssignmentsDM8 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Necessary assignments computed for Decision Maker 8.

The input value should be a valid XMCDA document whose main tag is ``<alternativesAffectations>``.


------------------------



.. _RORUTADIS-GroupAssignments-PUT_outputs:

Outputs
-------


- :ref:`necessaryNecessaryAssignments <RORUTADIS-GroupAssignments-PUT-necessaryNecessaryAssignments>`
- :ref:`necessaryPossibleAssignments <RORUTADIS-GroupAssignments-PUT-necessaryPossibleAssignments>`
- :ref:`possibleNecessaryAssignments <RORUTADIS-GroupAssignments-PUT-possibleNecessaryAssignments>`
- :ref:`possiblePossibleAssignments <RORUTADIS-GroupAssignments-PUT-possiblePossibleAssignments>`
- :ref:`messages <RORUTADIS-GroupAssignments-PUT-messages>`


.. _RORUTADIS-GroupAssignments-PUT-necessaryNecessaryAssignments:

necessaryNecessaryAssignments
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Necessary-necessary assignments. An alternative is assigned to some class iff it is necessarily assigned to this class for all Decision Makers.

The returned value is a XMCDA document whose main tag is ``<alternativesAffectations>``.
It has the following form::

   
				
					<alternativesAffectations>
						<alternativeAffectation>
							<alternativeID>[...]</alternativeID>
							<categoryID>[...]</categoryID>
						</alternativeAffectation>
						[...]
						<alternativeAffectation>
							<alternativeID>[...]</alternativeID>
							<categoriesInterval>
								<lowerBound>
									<categoryID>[...]</categoryID>
								</lowerBound>
								<upperBound>
									<categoryID>[...]</categoryID>
								</upperBound>
							</categoriesInterval>
						</alternativeAffectation>
						[...]
						<alternativeAffectation>
							<alternativeID>[...]</alternativeID>
							<categoriesSet>
								<categoryID>[...]</categoryID>
								[...]
							</categoriesSet>
						</alternativeAffectation>
						[...]
					</alternativesAffectations>
				
			


------------------------


.. _RORUTADIS-GroupAssignments-PUT-necessaryPossibleAssignments:

necessaryPossibleAssignments
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Necessary-possible assignments. An alternative is assigned to some class iff it is necessarily assigned to this class for at least one Decision Maker.

The returned value is a XMCDA document whose main tag is ``<alternativesAffectations>``.


------------------------


.. _RORUTADIS-GroupAssignments-PUT-possibleNecessaryAssignments:

possibleNecessaryAssignments
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Possible-necessary assignments. An alternative is assigned to some class iff it is possibly assigned to this class for all Decision Makers.

The returned value is a XMCDA document whose main tag is ``<alternativesAffectations>``.


------------------------


.. _RORUTADIS-GroupAssignments-PUT-possiblePossibleAssignments:

possiblePossibleAssignments
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Possible-possible assignments. An alternative is assigned to some class iff it is possibly assigned to this class for at least one Decision Maker.

The returned value is a XMCDA document whose main tag is ``<alternativesAffectations>``.


------------------------


.. _RORUTADIS-GroupAssignments-PUT-messages:

messages
~~~~~~~~

Messages generated by the program.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/RORUTADIS-GroupAssignments-PUT/description-wsDD.xml>`
