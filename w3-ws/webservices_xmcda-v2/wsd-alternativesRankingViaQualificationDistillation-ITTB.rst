.. _alternativesRankingViaQualificationDistillation-ITTB:

alternativesRankingViaQualificationDistillation
===============================================

:Version: 1.1
:Provider: ITTB
:SOAP service's name: ``alternativesRankingViaQualificationDistillation-ITTB`` (see :ref:`soap-requests` for details)

Description
-----------

This web service computes rankings on the alternatives by distillation of alternatives' qualification. Compared to the web service alternativesRankingViaQualificationDistillation, this web service requires a non-valued or a 0-1 valued outranking relation as input. The partial ranking (intersectionDistillation) is obtained by taking the intersection of upwards and downwards distillation preorders (which are also provided).

**Contact:** Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)


Inputs
------
(For outputs, see :ref:`below <alternativesRankingViaQualificationDistillation-ITTB_outputs>`)


- :ref:`alternatives <alternativesRankingViaQualificationDistillation-ITTB-alternatives>`
- :ref:`outrankingRelation <alternativesRankingViaQualificationDistillation-ITTB-outrankingRelation>`


.. _alternativesRankingViaQualificationDistillation-ITTB-alternatives:

alternatives
~~~~~~~~~~~~

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
				
                    <alternatives>
                        <alternative>
                            <active>[...]</active>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
                    
			


------------------------


.. _alternativesRankingViaQualificationDistillation-ITTB-outrankingRelation:

outrankingRelation
~~~~~~~~~~~~~~~~~~

A non-valued relation or a 0-1 valued outranking relation. A numeric <value> indicates a the valuation for each <pair> of the relation.

The input value should be a valid XMCDA document whose main tag is ``<alternativesComparisons>``.
It must have the following form::

   
				
                    <alternativesComparisons>
                        <pairs>
                            <pair>
                                <initial>
                                    <alternativeID>[...]</alternativeID>
                                </initial>
                                <terminal>
                                    <alternativeID>[...]</alternativeID>
                                </terminal>
                                <value>
                                    <real>[...]</real>
                                </value>
                            </pair>

                            [...]
                        </pairs>
                    </alternativesComparisons>
                    
			


------------------------



.. _alternativesRankingViaQualificationDistillation-ITTB_outputs:

Outputs
-------


- :ref:`intersectionDistillation <alternativesRankingViaQualificationDistillation-ITTB-intersectionDistillation>`
- :ref:`downwardsDistillation <alternativesRankingViaQualificationDistillation-ITTB-downwardsDistillation>`
- :ref:`upwardsDistillation <alternativesRankingViaQualificationDistillation-ITTB-upwardsDistillation>`
- :ref:`messages <alternativesRankingViaQualificationDistillation-ITTB-messages>`


.. _alternativesRankingViaQualificationDistillation-ITTB-intersectionDistillation:

intersectionDistillation
~~~~~~~~~~~~~~~~~~~~~~~~

An <alternativesComparisons> containing the partial preorder obtained by taking the intersection of the downwards and upwards distillation preorders.

The returned value is a XMCDA document whose main tag is ``<alternativesComparisons>``.
It has the following form::

   
				
                    <alternativesComparisons mcdaConcept="Intersection distillation">
                        <pairs>
                            <pair>
                                <initial>
                                    <alternativeID>[...]</alternativeID>
                                </initial>
                                <terminal>
                                    <alternativeID>[...]</alternativeID>
                                </terminal>
                            </pair>
                            [...]
                        </pairs>
                    </alternativesComparisons>
                    
			


------------------------


.. _alternativesRankingViaQualificationDistillation-ITTB-downwardsDistillation:

downwardsDistillation
~~~~~~~~~~~~~~~~~~~~~

Alternatives' ranks in the downwards distillation preorder.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   
				
					<alternativesValues mcdaConcept="Downwards distillation">
				    	<alternativeValue>
	                       	<alternativeID>[...]</alternativeID>
                        	<value>
	                          	<real>[...]</real>
                        	</value>
                    	</alternativeValue>
                    </alternativesValues>
                
			


------------------------


.. _alternativesRankingViaQualificationDistillation-ITTB-upwardsDistillation:

upwardsDistillation
~~~~~~~~~~~~~~~~~~~

Alternatives' ranks in the upwards distillation preorder.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   
				
					<alternativesValues mcdaConcept="Upwards distillation">
				    	<alternativeValue>
	                       	<alternativeID>[...]</alternativeID>
                        	<value>
	                          	<real>[...]</real>
                        	</value>
                    	</alternativeValue>
                    </alternativesValues>
                
			


------------------------


.. _alternativesRankingViaQualificationDistillation-ITTB-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/alternativesRankingViaQualificationDistillation-ITTB/description-wsDD.xml>`
