.. _HierarchicalDEA-CCR_preferenceRelations-PUT:

HierarchicalDEA-CCR_preferenceRelations
=======================================

:Version: 1.0
:Provider: PUT
:SOAP service's name: ``HierarchicalDEA-CCR_preferenceRelations-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Computes necessary and possible preference relations for the pairs of DMUs (alternatives) using CCR Data Envelopment Analysis Model with hierarchical structure of outputs.

**Contact:** 
            Anna Labijak <anna.labijak@cs.put.poznan.pl>
        


Inputs
------
(For outputs, see :ref:`below <HierarchicalDEA-CCR_preferenceRelations-PUT_outputs>`)


- :ref:`units <HierarchicalDEA-CCR_preferenceRelations-PUT-units>`
- :ref:`performanceTable <HierarchicalDEA-CCR_preferenceRelations-PUT-performanceTable>`
- :ref:`hierarchy <HierarchicalDEA-CCR_preferenceRelations-PUT-hierarchy>`
- :ref:`weightsLinearConstraints <HierarchicalDEA-CCR_preferenceRelations-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <HierarchicalDEA-CCR_preferenceRelations-PUT-methodParameters>`


.. _HierarchicalDEA-CCR_preferenceRelations-PUT-units:

units
~~~~~

A list of alternatives (DMUs).

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
                <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
            


------------------------


.. _HierarchicalDEA-CCR_preferenceRelations-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A list of alternatives (DMUs) performances (on leaf hierarchy criteria).

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   
                <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>
            


------------------------


.. _HierarchicalDEA-CCR_preferenceRelations-PUT-hierarchy:

hierarchy
~~~~~~~~~

The hierarchical structure of criteria.

The input value should be a valid XMCDA document whose main tag is ``<hierarchy>``.
It must have the following form::

   
                
                <hierarchy>
                    <node>
                        <criterionID>[...]</criterionID>
                        <node>
                            <criterionID>[...]</criterionID>
                            <node>
                                [...]
                            </node>
                            [...]
                        </node>
                        [...]
                    </node>
                </hierarchy>
            


------------------------


.. _HierarchicalDEA-CCR_preferenceRelations-PUT-weightsLinearConstraints:

weightsLinearConstraints *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of hierarchy criteria linear constraints.

The input value should be a valid XMCDA document whose main tag is ``<criteriaLinearConstraints>``.
It must have the following form::

   
                <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID> [...] </criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>
            


------------------------


.. _HierarchicalDEA-CCR_preferenceRelations-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~

Represents parameters (hierarchyNode).

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                
    <methodParameters>
        <parameter id="hierarchyNode">
            <value><label>%1</label></value>
        </parameter>
    </methodParameters>
            

where:

- **%1** is a parameter named "hierarchy node". This is a string.
  The default value is root.


------------------------



.. _HierarchicalDEA-CCR_preferenceRelations-PUT_outputs:

Outputs
-------


- :ref:`necessaryDominance <HierarchicalDEA-CCR_preferenceRelations-PUT-necessaryDominance>`
- :ref:`possibleDominance <HierarchicalDEA-CCR_preferenceRelations-PUT-possibleDominance>`
- :ref:`messages <HierarchicalDEA-CCR_preferenceRelations-PUT-messages>`


.. _HierarchicalDEA-CCR_preferenceRelations-PUT-necessaryDominance:

necessaryDominance
~~~~~~~~~~~~~~~~~~

A list of pairs of DMUs related with necessary preference relation.

The returned value is a XMCDA document whose main tag is ``<alternativesComparisons>``.
It has the following form::

   
                
         <alternativesComparisons>
						<pairs>
              <pair>
                  <initial>
                    <alternativeID>[...]</alternativeID>
                  </initial>
                  <terminal>
                    <alternativeID>[...]</alternativeID>
                  </terminal>
                  <values>
                    <value>
                      <real>1</real>
                    </value>
                  </values>
              </pair>
						  [...]
            </pairs>
            [...]
					</alternativesComparisons>
            


------------------------


.. _HierarchicalDEA-CCR_preferenceRelations-PUT-possibleDominance:

possibleDominance
~~~~~~~~~~~~~~~~~

A list of pairs of DMUs related with possible preference relation.

The returned value is a XMCDA document whose main tag is ``<alternativesComparisons>``.
It has the following form::

   
                
         <alternativesComparisons>
						<pairs>
              <pair>
                  <initial>
                    <alternativeID>[...]</alternativeID>
                  </initial>
                  <terminal>
                    <alternativeID>[...]</alternativeID>
                  </terminal>
                  <values>
                    <value>
                      <real>1</real>
                    </value>
                  </values>
              </pair>
						  [...]
            </pairs>
            [...]
					</alternativesComparisons>
            


------------------------


.. _HierarchicalDEA-CCR_preferenceRelations-PUT-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/HierarchicalDEA-CCR_preferenceRelations-PUT/description-wsDD.xml>`
