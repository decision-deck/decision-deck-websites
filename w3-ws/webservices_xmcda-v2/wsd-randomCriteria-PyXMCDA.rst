.. _randomCriteria-PyXMCDA:

randomCriteria
==============

:Version: 1.1
:Provider: PyXMCDA
:SOAP service's name: ``randomCriteria-PyXMCDA`` (see :ref:`soap-requests` for details)

Description
-----------

This web service allows to create a simple list of criteria by providing the desired number of criteria. Now, it is not taking into account creation of thresholds.

**Contact:** Thomas Veneziano (thomas.veneziano@uni.lu)


Inputs
------
(For outputs, see :ref:`below <randomCriteria-PyXMCDA_outputs>`)


- :ref:`nbCriteria <randomCriteria-PyXMCDA-nbCriteria>`
- :ref:`criteriaPrefix <randomCriteria-PyXMCDA-criteriaPrefix>` *(optional)*
- :ref:`criteriaNames <randomCriteria-PyXMCDA-criteriaNames>` *(optional)*
- :ref:`criteriaType <randomCriteria-PyXMCDA-criteriaType>` *(optional)*
- :ref:`seed <randomCriteria-PyXMCDA-seed>` *(optional)*
- :ref:`lowerBounds <randomCriteria-PyXMCDA-lowerBounds>` *(optional)*
- :ref:`upperBounds <randomCriteria-PyXMCDA-upperBounds>` *(optional)*
- :ref:`numberOfLevels <randomCriteria-PyXMCDA-numberOfLevels>` *(optional)*
- :ref:`preferenceDirection <randomCriteria-PyXMCDA-preferenceDirection>` *(optional)*
- :ref:`thresholdsNames <randomCriteria-PyXMCDA-thresholdsNames>` *(optional)*


.. _randomCriteria-PyXMCDA-nbCriteria:

nbCriteria
~~~~~~~~~~

Indicates the desired number of criteria. It must be a strict positive integer.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                            
                    <methodParameters>
                        <parameter
                             name="nbCriteria"> <!-- REQUIRED  -->
                            <value>
                                <integer>%1</integer>
                            </value>
                        </parameter>
                    </methodParameters>
                    
                     

where:

- **%1** is a parameter named "nbCriteria". This is a int, and the value should conform to the following constraint: The value should be a strict positive integer..  More formally, the constraint is::

      %1 > 0 
  The default value is 2.


------------------------


.. _randomCriteria-PyXMCDA-criteriaPrefix:

criteriaPrefix *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Indicates the desired prefix for the name of the criteria. If not provided, criteria will be called g1, g2, ... If provided, criteria will be called prefix1, prefix2, ... Note that it will only be used if you provide a number of alternatives.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                            
                    <methodParameters>
                        <parameter
                             name="criteriaPrefix"> <!-- REQUIRED  -->
                            <value>
                                <label>%1</label>
                            </value>
                        </parameter>
                    </methodParameters>
                    
                     

where:

- **%1** is a parameter named "criteriaPrefix". This is a string.
  The default value is g.


------------------------


.. _randomCriteria-PyXMCDA-criteriaNames:

criteriaNames *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~

Indicates a list of criteria names. Note that if criteriaNames and nbCriteria are provided at the same time, then only criteriaNames are considered.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                            
                    <methodParameters>
                        <parameters
                             name="criteriaNames"> <!-- REQUIRED  -->
                            <parameter><value>
                                <label>[...]</label>
                            </value></parameter>
                            [..]
                        </parameter>
                    </methodParameters>
                    
                     


------------------------


.. _randomCriteria-PyXMCDA-criteriaType:

criteriaType *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~

Allows to indicate the type of some criteria (qualitative or quantitative). The default value for each criterion is quantitative.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                            
                    <methodParameters>
                        <parameters
                             name="criteriaType"> <!-- REQUIRED  -->
                            <parameter name="<!-- The name of the criterion -->>
                                 <value><label><!-- qualitative or quantitative --></label></value>
                            </parameter>
                            [..]
                        </parameter>
                    </methodParameters>
                    
                     


------------------------


.. _randomCriteria-PyXMCDA-seed:

seed *(optional)*
~~~~~~~~~~~~~~~~~

Allows to initialize the random generator with a precise seed.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                            
                                   <methodParameters>
                                          <parameter
                                                 name="seed"> <!-- REQUIRED  -->
                                                 <value>
                                                        <integer>%1</integer>
                                                 </value>
                                          </parameter>
                                   </methodParameters>
                            
                     

where:

- **%1** is a parameter named "seed". This is a int, and the value should conform to the following constraint: The value should be a positive integer..  More formally, the constraint is::

      %1 > 0 

------------------------


.. _randomCriteria-PyXMCDA-lowerBounds:

lowerBounds *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~

Allows to indicate a lower bound for some quantitative criteria. The default value is 0. Lower bounds for qualitative criteria are ignored.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                            
                    <methodParameters>
                        <parameters
                             name="lowerBound"> <!-- REQUIRED  -->
                            <parameter name="<!-- The name of a quantitative criterion -->>
                                 <value>[...]</value>
                            </parameter>
                            [..]
                        </parameter>
                    </methodParameters>
                    
                     


------------------------


.. _randomCriteria-PyXMCDA-upperBounds:

upperBounds *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~

Allows to indicate an upper bound for some quantitative criteria. The default value is 100. Upper bounds for qualitative criteria are ignored.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                            
                    <methodParameters>
                        <parameters
                             name="upperBound"> <!-- REQUIRED  -->
                            <parameter name="<!-- The name of a quantitative criterion -->>
                                 <value>[...]</value>
                            </parameter>
                            [..]
                        </parameter>
                    </methodParameters>
                    
                     


------------------------


.. _randomCriteria-PyXMCDA-numberOfLevels:

numberOfLevels *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Allows to indicate a number of levels for some qualitative criteria. The default value is 10. if provided for quantitative criteria, it will be ignored.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                            
                    <methodParameters>
                        <parameters
                             name="numberOfLevels"> <!-- REQUIRED  -->
                            <parameter name="<!-- The name of a qualitative criterion -->>
                                 <value><integer>[...]</integer></value>
                            </parameter>
                            [..]
                        </parameter>
                    </methodParameters>
                    
                     


------------------------


.. _randomCriteria-PyXMCDA-preferenceDirection:

preferenceDirection *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Allows to indicate a preference direction (min or max) for some criteria. The default value is max.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                            
                    <methodParameters>
                        <parameters
                             name="preferenceDirection"> <!-- REQUIRED  -->
                            <parameter name="<!-- The name of a criterion -->>
                                 <value><label><!-- min or max --></label></value>
                            </parameter>
                            [..]
                        </parameter>
                    </methodParameters>
                    
                     


------------------------


.. _randomCriteria-PyXMCDA-thresholdsNames:

thresholdsNames *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Indicates a list of thresholds names. The names must be provided in an increasing order of importance, as the randomly generated values will be sorted and associated to the thresholds in the given order. The thresholds are added for every criteria (with different random values for each one).

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                            
                    <methodParameters>
                        <parameters
                             name="thresholdsNames"> <!-- REQUIRED  -->
                            <parameter>
                                 <value><label><!-- the name of a threshold --></label></value>
                            </parameter>
                            [..]
                        </parameter>
                    </methodParameters>
                    
                     


------------------------



.. _randomCriteria-PyXMCDA_outputs:

Outputs
-------


- :ref:`criteria <randomCriteria-PyXMCDA-criteria>`
- :ref:`messages <randomCriteria-PyXMCDA-messages>`


.. _randomCriteria-PyXMCDA-criteria:

criteria
~~~~~~~~

A list of criteria with active tags.

The returned value is a XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _randomCriteria-PyXMCDA-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/randomCriteria-PyXMCDA/description-wsDD.xml>`
