.. _ImpreciseDEA-CCR_preferenceRelations-PUT:

ImpreciseDEA-CCR_preferenceRelations
====================================

:Version: 1.0
:Provider: PUT
:SOAP service's name: ``ImpreciseDEA-CCR_preferenceRelations-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Computes extrene efficiency ranks for the given DMUs (alternatives) using CCR Data Envelopment Analysis Model.

**Contact:** Anna Labijak <support@decision-deck.org>


Inputs
------
(For outputs, see :ref:`below <ImpreciseDEA-CCR_preferenceRelations-PUT_outputs>`)


- :ref:`units <ImpreciseDEA-CCR_preferenceRelations-PUT-units>`
- :ref:`inputsOutputs <ImpreciseDEA-CCR_preferenceRelations-PUT-inputsOutputs>`
- :ref:`performanceTable <ImpreciseDEA-CCR_preferenceRelations-PUT-performanceTable>`
- :ref:`maxPerformanceTable <ImpreciseDEA-CCR_preferenceRelations-PUT-maxPerformanceTable>` *(optional)*
- :ref:`weightsLinearConstraints <ImpreciseDEA-CCR_preferenceRelations-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <ImpreciseDEA-CCR_preferenceRelations-PUT-methodParameters>`


.. _ImpreciseDEA-CCR_preferenceRelations-PUT-units:

units
~~~~~

A list of alternatives (DMUs).

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>


------------------------


.. _ImpreciseDEA-CCR_preferenceRelations-PUT-inputsOutputs:

inputsOutputs
~~~~~~~~~~~~~

A list of criteria with specified preference direction. List has to contains at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output).

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   <criteria>
                        <criterion>
							<scale>								
                                [...]
							</scale>
                            [...]
                        </criterion>
                        [...]
                    </criteria>


------------------------


.. _ImpreciseDEA-CCR_preferenceRelations-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A list of alternatives (DMUs) minimal performances (or exact performances if intervals should be created by tolerance).

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>


------------------------


.. _ImpreciseDEA-CCR_preferenceRelations-PUT-maxPerformanceTable:

maxPerformanceTable *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of alternatives (DMUs) maximal performances.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>


------------------------


.. _ImpreciseDEA-CCR_preferenceRelations-PUT-weightsLinearConstraints:

weightsLinearConstraints *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of criteria linear constraints.

The input value should be a valid XMCDA document whose main tag is ``<criteriaLinearConstraints>``.
It must have the following form::

   <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID> [...] </criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>


------------------------


.. _ImpreciseDEA-CCR_preferenceRelations-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~

Represents method parameters (tolerance).

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
    <methodParameters>
        <parameter id="tolerance">
            <value><real>%1</real></value>
        </parameter>
    </methodParameters>

where:

- **%1** is a parameter named "tolerance". This is a float, and the value should conform to the following constraint: The value should be a non-negative number..  More formally, the constraint is::

     %1 >= 0
  The default value is 0.00.


------------------------



.. _ImpreciseDEA-CCR_preferenceRelations-PUT_outputs:

Outputs
-------


- :ref:`necessaryDominance <ImpreciseDEA-CCR_preferenceRelations-PUT-necessaryDominance>`
- :ref:`possibleDominance <ImpreciseDEA-CCR_preferenceRelations-PUT-possibleDominance>`
- :ref:`messages <ImpreciseDEA-CCR_preferenceRelations-PUT-messages>`


.. _ImpreciseDEA-CCR_preferenceRelations-PUT-necessaryDominance:

necessaryDominance
~~~~~~~~~~~~~~~~~~

A list of pairs of pairs of DMU related with necessary preference relation.

The returned value is a XMCDA document whose main tag is ``<alternativesComparisons>``.
It has the following form::

   
         <alternativesComparisons>
						<pairs>
              <pair>
                  <initial>
                    <alternativeID>[...]</alternativeID>
                  </initial>
                  <terminal>
                    <alternativeID>[...]</alternativeID>
                  </terminal>
                  <values>
                    <value>
                      <real>1</real>
                    </value>
                  </values>
              </pair>
						  [...]
            </pairs>
            [...]
					</alternativesComparisons>


------------------------


.. _ImpreciseDEA-CCR_preferenceRelations-PUT-possibleDominance:

possibleDominance
~~~~~~~~~~~~~~~~~

A list of pairs of pairs of DMU related with possible preference relation.

The returned value is a XMCDA document whose main tag is ``<alternativesComparisons>``.
It has the following form::

   
         <alternativesComparisons>
						<pairs>
              <pair>
                  <initial>
                    <alternativeID>[...]</alternativeID>
                  </initial>
                  <terminal>
                    <alternativeID>[...]</alternativeID>
                  </terminal>
                  <values>
                    <value>
                      <real>1</real>
                    </value>
                  </values>
              </pair>
						  [...]
            </pairs>
            [...]
					</alternativesComparisons>


------------------------


.. _ImpreciseDEA-CCR_preferenceRelations-PUT-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/ImpreciseDEA-CCR_preferenceRelations-PUT/description-wsDD.xml>`
