.. _plotCriteriaValues-ITTB:

plotCriteriaValues
==================

:Version: 1.1
:Provider: ITTB
:SOAP service's name: ``plotCriteriaValues-ITTB`` (see :ref:`soap-requests` for details)

Description
-----------

This web service generates a barplot or a pie plot representing a numeric quantity for each criterion, like, e.g., an importance value. Compared to the web service plotCriteriaValues, some parameters are added. Colors can be used and the title of the plot can be typed. In the case of a bar chart, the axis-labels can also be typed. The criteria's evaluations are supposed to be real or integer numeric values.

**Contact:** Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)


Inputs
------
(For outputs, see :ref:`below <plotCriteriaValues-ITTB_outputs>`)


- :ref:`criteria <plotCriteriaValues-ITTB-criteria>`
- :ref:`criteriaValues <plotCriteriaValues-ITTB-criteriaValues>`
- :ref:`methodPlotOptions <plotCriteriaValues-ITTB-methodPlotOptions>`


.. _plotCriteriaValues-ITTB-criteria:

criteria
~~~~~~~~

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
                            
                    <criteria>
                        <criterion>
                            <active>[...]</active>
                            [...]
                        </criterion>
                        [...]
                    </criteria>
                    
                     


------------------------


.. _plotCriteriaValues-ITTB-criteriaValues:

criteriaValues
~~~~~~~~~~~~~~

A list of <criterionValue> representing a certain numeric quantity for each criterion, like, e.g., an importance value.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.
It must have the following form::

   

                     


------------------------


.. _plotCriteriaValues-ITTB-methodPlotOptions:

methodPlotOptions
~~~~~~~~~~~~~~~~~

Plot type method: choose between "Bar chart" and "Pie chart". The default plot is a bar chart.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                   
					<methodParameters>
						<parameter id="chart_type" name="Chart type">
							<value>
            					<label>%1</label>
       						 </value>
						</parameter>
						<parameter id="order_by" name="Order by">
							<value>
            					<label>%2</label>
       						 </value>
						</parameter>
						<parameter id="order" name="Order">
							<value>
            					<label>%3</label>
       						 </value>
						</parameter>
						<parameter id="use_color" name="Colors in the chart">
							<value>
            					<label>%4</label>
       						 </value>
						</parameter>
						<parameter id="initial_color" name="Initial color">
							<value>
            					<label>%5</label>
       						 </value>
						</parameter>
						<parameter id="final_color" name="Final color">
							<value>
            					<label>%6</label>
       						 </value>
						</parameter>
						<parameter id= "chart_title" name="Chart title">
							<value>
            					<label>%7</label>
       						 </value>
						</parameter>
						<parameter id="domain_axis" name="Domain axis label">
							<value>
            					<label>%8</label>
       						 </value>
						</parameter >
						<parameter id="range_axis" name="Range axis label">
							<value>
            					<label>%9</label>
       						 </value>
						</parameter>
					</methodParameters>
				
               

where:

- **%1** is a parameter named "Chart type:". It can have the following values:

  - ``barChart``: bar chart

  - ``pieChart``: pie chart

  The default value is bar_chart.

- **%2** is a parameter named "Order by:". It can have the following values:

  - ``name``: name

  - ``id``: id

  - ``values``: values

  The default value is order_by_values.

- **%3** is a parameter named "Order:". It can have the following values:

  - ``increasing``: increasing

  - ``decreasing``: decreasing

  The default value is increasing.

- **%4** is a parameter named "Colors:". It can have the following values:

  - ``true``: gradient

  - ``false``: black and white

  The default value is false.

- **%5** is a parameter named "Initial color:". It can have the following values:

  - ``black``: black

  - ``red``: red

  - ``blue``: blue

  - ``green``: green

  - ``yellow``: yellow

  - ``magenta``: magenta

  - ``cyan``: cyan

  The default value is black.

- **%6** is a parameter named "Final color:". It can have the following values:

  - ``black``: black

  - ``red``: red

  - ``blue``: blue

  - ``green``: green

  - ``yellow``: yellow

  - ``magenta``: magenta

  - ``cyan``: cyan

  The default value is black.

- **%7** is a parameter named "Chart title:". This is a string.
- **%8** is a parameter named "X axis label:". This is a string.
- **%9** is a parameter named "Y axis label:". This is a string.

------------------------



.. _plotCriteriaValues-ITTB_outputs:

Outputs
-------


- :ref:`criteriaValuesPlot <plotCriteriaValues-ITTB-criteriaValuesPlot>`
- :ref:`messages <plotCriteriaValues-ITTB-messages>`


.. _plotCriteriaValues-ITTB-criteriaValuesPlot:

criteriaValuesPlot
~~~~~~~~~~~~~~~~~~

A string containing the base64 representation of the png image of the generated barplot or pieplot .

The returned value is a XMCDA document whose main tag is ``<criterionValue>``.


------------------------


.. _plotCriteriaValues-ITTB-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/plotCriteriaValues-ITTB/description-wsDD.xml>`
