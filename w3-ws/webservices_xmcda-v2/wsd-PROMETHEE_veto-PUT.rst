.. _PROMETHEE_veto-PUT:

PrometheeVeto
=============

:Version: 1.0.0
:Provider: PUT
:Name: PROMETHEE_veto
:SOAP service's name: ``PROMETHEE_veto-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Computes veto indices which
The key feature of this module is its flexibility in terms of the types of
elements allowed to compare, i.e. alternatives vs alternatives, alternatives vs
boundary profiles and alternatives vs central (characteristic) profiles.

**Web page:** https://github.com/Yamadads/PrometheeDiviz


Inputs
------
(For outputs, see :ref:`below <PROMETHEE_veto-PUT_outputs>`)


- :ref:`criteria <PROMETHEE_veto-PUT-input3>`
- :ref:`alternatives <PROMETHEE_veto-PUT-input1>`
- :ref:`performance_table <PROMETHEE_veto-PUT-input4>`
- :ref:`profiles_performance_table <PROMETHEE_veto-PUT-input5>` *(optional)*
- :ref:`weights <PROMETHEE_veto-PUT-input6>` *(optional)*
- :ref:`categories_profiles <PROMETHEE_veto-PUT-input2>` *(optional)*
- :ref:`method_parameters <PROMETHEE_veto-PUT-input7>`


.. _PROMETHEE_veto-PUT-input3:

criteria
~~~~~~~~

Criteria to consider, possibly with preference and indifference thresholds. For Gaussian function it needs inflection point (sigma). Each criterion must have a preference direction specified (min or max). It is worth mentioning that this module allows to define thresholds as constants as well as linear functions.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _PROMETHEE_veto-PUT-input1:

alternatives
~~~~~~~~~~~~

Alternatives to consider.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _PROMETHEE_veto-PUT-input4:

performance_table
~~~~~~~~~~~~~~~~~

The performance of alternatives.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _PROMETHEE_veto-PUT-input5:

profiles_performance_table *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The performance of profiles (boundary or central).

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _PROMETHEE_veto-PUT-input6:

weights *(optional)*
~~~~~~~~~~~~~~~~~~~~

Weights of criteria to consider.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _PROMETHEE_veto-PUT-input2:

categories_profiles *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Definitions of central or boundary profiles connected with classes (categories)

The input value should be a valid XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _PROMETHEE_veto-PUT-input7:

method_parameters
~~~~~~~~~~~~~~~~~

First parameter specifies the type of elements provided for comparison.

Choosing 'boundary_profiles' or 'central_profiles' requires providing inputs 'classes_profiles' and 'profiles_performance_table' as well (which are optional by default).

Second parameter specifies the type of function used for comparison of each criterion.
Choosing 'specified' requires providing inputs "generalised_criterion" which is optional by default.
Choosing some of numbers sets same function for all criteria.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
        
        <methodParameters>
          <parameter name="comparison_with">
            <value>
              <label>%1</label>
            </value>
          </parameter>
          <parameter name="weights_specified">
            <value>
              <label>%2</label>
            </value>
          </parameter>
        </methodParameters>
        
      

where:

- **%1** is a parameter named "comparison with". It can have the following values:

  - ``alternatives``: alternatives vs alternatives

  - ``boundary_profiles``: alternatives vs boundary profiles

  - ``central_profiles``: alternatives vs central (characteristic) profiles

  The default value is item0.

- **%2** is a parameter named "weights specified". It can have the following values:

  - ``specified``: Total veto coeficient is weighted average of veto on each criterion

  - ``not_specified``: Weights not specified, if veto on one criterion then veto equals 1.0

  The default value is item0.


------------------------



.. _PROMETHEE_veto-PUT_outputs:

Outputs
-------


- :ref:`veto <PROMETHEE_veto-PUT-output1>`
- :ref:`partial_veto <PROMETHEE_veto-PUT-output2>`
- :ref:`messages <PROMETHEE_veto-PUT-output3>`


.. _PROMETHEE_veto-PUT-output1:

veto
~~~~

Aggregated veto matrix computed from the given data. This matrix aggregates partial veto indices from all criteria into single veto index per pair of alternatives or alternatives/profiles.

The returned value is a XMCDA document whose main tag is ``<alternativesComparisons>``.


------------------------


.. _PROMETHEE_veto-PUT-output2:

partial_veto
~~~~~~~~~~~~

Veto matrix computed from the given data. This matrix shows partial veto indices for all criteria, for all pairs of alternatives or alternatives/profiles.

The returned value is a XMCDA document whose main tag is ``<alternativesComparisons>``.


------------------------


.. _PROMETHEE_veto-PUT-output3:

messages
~~~~~~~~

Messages or errors generated by this module.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/PROMETHEE_veto-PUT/description-wsDD.xml>`
