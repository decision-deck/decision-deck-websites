.. _plotCriteriaValuesPreorder-ITTB:

plotCriteriaValuesPreorder
==========================

:Version: 1.1
:Provider: ITTB
:SOAP service's name: ``plotCriteriaValuesPreorder-ITTB`` (see :ref:`soap-requests` for details)

Description
-----------

This web service generates a graph representing a preorder on the criteria, according to numerical values taken by the criteria (the "best" criteria has the highest value). Compared to the web service plotCriteriaValuesPreorder, some parameters are added. Colors can be used and the title of the graph can be typed. You can choose between an increasing or a decreasing order for the graph. It is also possible to show the name of the criteria instead of the id, etc. The criteria evaluations are supposed to be real or integer numeric values.

**Contact:** Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)


Inputs
------
(For outputs, see :ref:`below <plotCriteriaValuesPreorder-ITTB_outputs>`)


- :ref:`criteria <plotCriteriaValuesPreorder-ITTB-criteria>`
- :ref:`criteriaValues <plotCriteriaValuesPreorder-ITTB-criteriaValues>`
- :ref:`options <plotCriteriaValuesPreorder-ITTB-options>`


.. _plotCriteriaValuesPreorder-ITTB-criteria:

criteria
~~~~~~~~

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
                   
                    <criteria>
                        <criterion>
                            <active>[...]</active>
                            [...]
                        </criterion>
                        [...]
                    </criteria>
                    
               


------------------------


.. _plotCriteriaValuesPreorder-ITTB-criteriaValues:

criteriaValues
~~~~~~~~~~~~~~

A list of <criterionValue> representing a certain numeric quantity for each criterion, like, e.g., an importance value.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.
It must have the following form::

   

               


------------------------


.. _plotCriteriaValuesPreorder-ITTB-options:

options
~~~~~~~

Generates a graph taking into account the proposed options.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                   
					<methodParameters>
						  <parameter id="plot_title" name="Plot Title">
							<value>
            					<label>%1</label>
       						 </value>
						</parameter>
						<parameter id="plot_order" name="Plot order">
							<value>
            					<label>%2</label>
       						 </value>
						</parameter>
						<parameter id="node_shape" name="Node shape">
							<value>
            					<label>%3</label>
       						 </value>
						</parameter>
						 <parameter id="show_names" name="Show criteria names">
							<value>
            					<label>%4</label>
       						 </value>
						</parameter>
						 <parameter id="use_color" name="Colors in the graph">
							<value>
            					<label>%5</label>
       						 </value>
						</parameter>
						 <parameter id="selected_color" name="Selected color">
							<value>
            					<label>%6</label>
       						 </value>
						</parameter >
					</methodParameters>
				
               

where:

- **%1** is a parameter named "Chart title:". This is a string.
- **%2** is a parameter named "Order:". It can have the following values:

  - ``increasing``: Ascending

  - ``decreasing``: Descending

  The default value is decreasing.

- **%3** is a parameter named "Shape of the nodes?". It can have the following values:

  - ``Rectangle``: Rectangle

  - ``Square``: Square

  - ``Ellipse``: Ellipse

  - ``Circle``: Circle

  - ``Diamond``: Diamond

  The default value is rectangle.

- **%4** is a parameter named "Criterion name or id?". It can have the following values:

  - ``true``: name

  - ``false``: id

  The default value is false.

- **%5** is a parameter named "Use colors?". It can have the following values:

  - ``true``: Yes

  - ``false``: No

  The default value is false.

- **%6** is a parameter named "Choose color:". It can have the following values:

  - ``Black``: Black

  - ``Red``: Red

  - ``Blue``: Blue

  - ``Green``: Green

  - ``Yellow``: Yellow

  - ``Magenta``: Magenta

  - ``Cyan``: Cyan

  The default value is black.


------------------------



.. _plotCriteriaValuesPreorder-ITTB_outputs:

Outputs
-------


- :ref:`criteriaValuesPlot <plotCriteriaValuesPreorder-ITTB-criteriaValuesPlot>`
- :ref:`messages <plotCriteriaValuesPreorder-ITTB-messages>`


.. _plotCriteriaValuesPreorder-ITTB-criteriaValuesPlot:

criteriaValuesPlot
~~~~~~~~~~~~~~~~~~

A string containing the base64 representation of the png image of the generated graph.

The returned value is a XMCDA document whose main tag is ``<criterionValue>``.


------------------------


.. _plotCriteriaValuesPreorder-ITTB-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/plotCriteriaValuesPreorder-ITTB/description-wsDD.xml>`
