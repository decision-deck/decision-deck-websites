.. _defuzzificationCOG-URV:

defuzzificationCOG
==================

:Version: 1.0
:Provider: URV
:SOAP service's name: ``defuzzificationCOG-URV`` (see :ref:`soap-requests` for details)

Description
-----------

Implementation of a defuzzification of set of fuzzy labels Java implementation of a defuzzification of set of fuzzy labels according to the COG method: Center of Gravity.
This method consists in calculating the center of gravity of each trapezoidal fuzzy set.
The trapezoidal set is defined in the FuzzyNumbers file.
Given a set of alternatives that are associated to a linguistic fuzzy term (alternativeValues file) a translation to the corresponding numerical COG is made.

**Contact:** Aida Valls <aida.valls@urv.cat>


Inputs
------
(For outputs, see :ref:`below <defuzzificationCOG-URV_outputs>`)


- :ref:`linguisticScores <defuzzificationCOG-URV-input0>`
- :ref:`fuzzyNumbers <defuzzificationCOG-URV-input1>`


.. _defuzzificationCOG-URV-input0:

linguisticScores
~~~~~~~~~~~~~~~~

Linguistic label describing the performance of each alternative

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.
It must have the following form::

   
		  
			<alternativesValues>
			  <alternativeValue>
                                <alternativeID>
                                        identifier
                                </alernativeID>
				<value>
					<label> ... </label>
				</value>
			  </alternativeValue>
			
                          [... list of other alternatives with id and label ]

			</alternativesValues>
		  
		  


------------------------


.. _defuzzificationCOG-URV-input1:

fuzzyNumbers
~~~~~~~~~~~~

Definition of a fuzzy variable as a list of labels (trapezoidal)

The input value should be a valid XMCDA document whose main tag is ``<categoriesValues>``.
It must have the following form::

   
                    
			<categoriesValue>
			<categoryValue>
			   <values>
				<value id="xxx" name="xxxxxxxx">
						<fuzzyNumber>
							<trapezoidal>
								<point1>
									<abscissa>[...]</abscissa>
									<ordinate>[...]</ordinate>
								</point1>
								[...]
								<point4>
									[...]
								</point4>
							</trapezoidal>
						</fuzzyNumber>
				</value>
			    	[...]
        	            </values>
			</categoryValue>
			</categoriesValue>
                    
                     


------------------------



.. _defuzzificationCOG-URV_outputs:

Outputs
-------


- :ref:`alternativesValues <defuzzificationCOG-URV-output0>`
- :ref:`messages <defuzzificationCOG-URV-output1>`


.. _defuzzificationCOG-URV-output0:

alternativesValues
~~~~~~~~~~~~~~~~~~

Numerical score for each alternative according to the input fuzzy label.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _defuzzificationCOG-URV-output1:

messages
~~~~~~~~

A status message.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/defuzzificationCOG-URV/description-wsDD.xml>`
