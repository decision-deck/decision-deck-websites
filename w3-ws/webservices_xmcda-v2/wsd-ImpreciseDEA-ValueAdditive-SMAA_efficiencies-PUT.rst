.. _ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT:

ImpreciseDEA-ValueAdditive-SMAA_efficiencies
============================================

:Version: 1.0
:Provider: PUT
:SOAP service's name: ``ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Computes efficiency scores for the given DMUs (alternatives) using SMAA-D method and Additive Data Envelopment Analysis Model with imprecise data. For given number of buckets and samples, returns a matrix with alternatives in each row and buckets representing efficiency intervals in each column. Single cell indicates how many samples gave efficiency scores of respective alternative in respective bucket.

**Contact:** Anna Labijak <support@decision-deck.org>


Inputs
------
(For outputs, see :ref:`below <ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT_outputs>`)


- :ref:`units <ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-units>`
- :ref:`inputsOutputs <ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-inputsOutputs>`
- :ref:`performanceTable <ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-performanceTable>`
- :ref:`maxPerformanceTable <ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-maxPerformanceTable>` *(optional)*
- :ref:`weightsLinearConstraints <ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-methodParameters>`


.. _ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-units:

units
~~~~~

A list of alternatives (DMUs).

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>


------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-inputsOutputs:

inputsOutputs
~~~~~~~~~~~~~

A list of criteria with specified preference direction. List has to contains at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output).

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   <criteria>
                        <criterion>
							<scale>
                                [...]
							</scale>
                            [...]
                        </criterion>
                        [...]
                    </criteria>


------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A list of alternatives (DMUs) minimal performances (or exact performances if intervals should be created by tolerance).

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>


------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-maxPerformanceTable:

maxPerformanceTable *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of alternatives (DMUs) maximal performances.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>


------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-weightsLinearConstraints:

weightsLinearConstraints *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of criteria linear constraints.

The input value should be a valid XMCDA document whose main tag is ``<criteriaLinearConstraints>``.
It must have the following form::

   <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID> [...] </criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>


------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~

Represents method parameters.
            "tolerance" represents the fraction for creating interval data (created interval: [data*(1-tolerance), data*(1+tolerance)]),
            "transformToUtilities" means if data should be tranformed into values from range [0-1],
            "boundariesProvided" means if inputsOutputs file contains information about min and max data for each factor,
            "functionShapeProvided" means if inputsOutputs file contains information about the shapes of value function for given factor,
            "samplesNb" determines number of samples used to calculate results,
            "intervalsNb" determines number of buckets, for which the efficiency scores are divided.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
	<methodParameters>
		<parameter id="samplesNb">
			<value><integer>%1</integer></value>
		</parameter>
		<parameter id="intervalsNb">
			<value><integer>%2</integer></value>
		</parameter>
		<parameter id="tolerance">
			<value><real>%3</real></value>
		</parameter>
		<parameter id="transformToUtilities">
			<value><boolean>%4</boolean></value>
		</parameter>
		<parameter id="boundariesProvided">
			<value><boolean>%5</boolean></value>
		</parameter>
		<parameter id="functionShapeProvided">
			<value><boolean>%6</boolean></value>
		</parameter>
	</methodParameters>


where:

- **%1** is a parameter named "number of samples". This is a int, and the value should conform to the following constraint: The value should be a positive integer..  More formally, the constraint is::

     %1 > 0
  The default value is 100.

- **%2** is a parameter named "number of intervals". This is a int, and the value should conform to the following constraint: The value should be a positive integer..  More formally, the constraint is::

     %2 > 0
  The default value is 10.

- **%3** is a parameter named "tolerance". This is a float, and the value should conform to the following constraint: The value should be a non-negative number..  More formally, the constraint is::

     %3 >= 0
  The default value is 0.0.

- **%4** is a parameter named "transform to utilities". This is a boolean.
  The default value is true.

- **%5** is a parameter named "boundaries provided". This is a boolean.
  The default value is false.

- **%6** is a parameter named "function shapes provided". This is a boolean.
  The default value is false.


------------------------



.. _ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT_outputs:

Outputs
-------


- :ref:`efficiencyDistribution <ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-efficiencyDistribution>`
- :ref:`maxEfficiency <ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-maxEfficiency>`
- :ref:`minEfficiency <ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-minEfficiency>`
- :ref:`avgEfficiency <ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-avgEfficiency>`
- :ref:`messages <ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-messages>`


.. _ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-efficiencyDistribution:

efficiencyDistribution
~~~~~~~~~~~~~~~~~~~~~~

A performance table for given alternatives. Single performance consists of attribute criterionID representing certain bucket, and a value representing the ratio of efficiency scores in this bucket.

The returned value is a XMCDA document whose main tag is ``<performanceTable>``.
It has the following form::

   <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID> Bucket [...]</criterionID>
									<value>
									[...]
									</value>
							</performance>
							[...]
						</alternativePerformances>
					</performanceTable>


------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-maxEfficiency:

maxEfficiency
~~~~~~~~~~~~~

A list of alternatives with maximum efficiency scores (obtained for given sample).

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   
           <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
						  <values>
                <value>
							  [...]
						    </value>
              </values>
						</alternativeValue>
						[...]
					</alternativesValues>


------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-minEfficiency:

minEfficiency
~~~~~~~~~~~~~

A list of alternatives with computed minimum efficiency scores (obtained for given sample).

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
						  <values>
                <value>
							  [...]
						    </value>
              </values>
						</alternativeValue>
						[...]
					</alternativesValues>


------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-avgEfficiency:

avgEfficiency
~~~~~~~~~~~~~

A list of alternatives with average efficiency scores (obtained for given sample).

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
              <values>
						    <value>
							  [...]
						    </value>
              </values>
						</alternativeValue>
						[...]
					</alternativesValues>


------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT/description-wsDD.xml>`
