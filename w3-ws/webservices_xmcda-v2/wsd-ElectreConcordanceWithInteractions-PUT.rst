.. _ElectreConcordanceWithInteractions-PUT:

ElectreConcordanceWithInteractions
==================================

:Version: 0.2.0
:Provider: PUT
:SOAP service's name: ``ElectreConcordanceWithInteractions-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Computes concordance matrix taking into account interactions between criteria. Possible interactions are: 'strengthening', 'weakening' and 'antagonistic'.

This module allows to compute concordance matrix for both for alternatives vs alternatives and alternatives vs profiles comparison - where profiles may be boundary or central (characteristic).

**Web page:** http://github.com/xor-xor/electre_diviz


Inputs
------
(For outputs, see :ref:`below <ElectreConcordanceWithInteractions-PUT_outputs>`)


- :ref:`criteria <ElectreConcordanceWithInteractions-PUT-input3>`
- :ref:`alternatives <ElectreConcordanceWithInteractions-PUT-input1>`
- :ref:`performance_table <ElectreConcordanceWithInteractions-PUT-input5>`
- :ref:`profiles_performance_table <ElectreConcordanceWithInteractions-PUT-input6>` *(optional)*
- :ref:`interactions <ElectreConcordanceWithInteractions-PUT-input4>`
- :ref:`weights <ElectreConcordanceWithInteractions-PUT-input7>`
- :ref:`classes_profiles <ElectreConcordanceWithInteractions-PUT-input2>` *(optional)*
- :ref:`method_parameters <ElectreConcordanceWithInteractions-PUT-input8>`


.. _ElectreConcordanceWithInteractions-PUT-input3:

criteria
~~~~~~~~

Criteria to consider, possibly with preference and indifference thresholds. Each criterion must have a preference direction specified (min or max). It is worth mentioning that this module allows to define thresholds as constants as well as linear functions.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _ElectreConcordanceWithInteractions-PUT-input1:

alternatives
~~~~~~~~~~~~

Alternatives to consider.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _ElectreConcordanceWithInteractions-PUT-input5:

performance_table
~~~~~~~~~~~~~~~~~

The performance of alternatives.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _ElectreConcordanceWithInteractions-PUT-input6:

profiles_performance_table *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The performance of profiles (boundary or central).

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _ElectreConcordanceWithInteractions-PUT-input4:

interactions
~~~~~~~~~~~~

Declarations of criteria interactions which should be taken into account. Possible interactions are: "strengthening", "weakening" and "antagonistic".

Every such declaration contains a pair of criteria and a numerical value for the interaction coefficient. This value should be negative for "weakening" and positive for "strenghtening" and "antagonistic". This coefficient should be interpreted as follows: the larger its absolute value, the more important the interaction effect.  All interaction coefficients should fulfill positive net balance condition.

The order in which each criterion appears in its interaction definition is significant, i.e. it designates the direction in which certain criterion affects the other, which is especially true when it comes to "antagonistic" interaction (i.e. the second criterion is the affected one).

There are no limits on the number of interactions that can be declared, but keep in mind that it should be rather small to make sense.

It is possible to declare more than one interactions of the same type, although please note that considering the same pair of criteria involved, the "strengthening" and "weakening" interactions are mutually exclusive.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.
It must have the following form::

   
        
          <criteriaValues mcdaConcept="criteriaInteractions">
            <criterionValue mcdaConcept="strengthening">
              <criteriaSet>
                <element>
                  <criterionID>[...]</criterionID>
                </element>
                <element>
                  <criterionID>[...]</criterionID>
                </element>
              </criteriaSet>
              <value>
                <real>[...]</real>
              </value>
            </criterionValue>
            <criterionValue mcdaConcept="weakening">
              [...]
            </criterionValue>
            <criterionValue mcdaConcept="antagonistic">
              [...]
            </criterionValue>
          </criteraValues>
        
      


------------------------


.. _ElectreConcordanceWithInteractions-PUT-input7:

weights
~~~~~~~

Weights of criteria to consider.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _ElectreConcordanceWithInteractions-PUT-input2:

classes_profiles *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Definitions of profiles (boundary or central) which should be used for classes (categories) representation.

The input value should be a valid XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _ElectreConcordanceWithInteractions-PUT-input8:

method_parameters
~~~~~~~~~~~~~~~~~

A set of parameters provided to tune up the module's operation.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
        
        <methodParameters>
          <parameter name="comparison_with">
            <value>
              <label>%1</label>
            </value>
          </parameter>
          <parameter name="z_function">
            <value>
              <label>%2</label>
            </value>
          </parameter>
         </methodParameters>
        
      

where:

- **%1** is a parameter named "comparison_with". It can have the following values:

  - ``alternatives``: alternatives vs alternatives

  - ``boundary_profiles``: alternatives vs boundary profiles

  - ``central_profiles``: alternatives vs central (characteristic) profiles

  The default value is item0.

- **%2** is a parameter named "z_function". It can have the following values:

  - ``multiplication``: Z(x, y) = xy

  - ``minimum``: Z(x, y) = min{x, y}

  The default value is item0.


------------------------



.. _ElectreConcordanceWithInteractions-PUT_outputs:

Outputs
-------


- :ref:`concordance <ElectreConcordanceWithInteractions-PUT-output1>`
- :ref:`messages <ElectreConcordanceWithInteractions-PUT-output2>`


.. _ElectreConcordanceWithInteractions-PUT-output1:

concordance
~~~~~~~~~~~

Matrix of aggregated concordance indices computed from the given data.

The returned value is a XMCDA document whose main tag is ``<alternativesComparisons>``.


------------------------


.. _ElectreConcordanceWithInteractions-PUT-output2:

messages
~~~~~~~~

Messages or errors generated by this module.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/ElectreConcordanceWithInteractions-PUT/description-wsDD.xml>`
