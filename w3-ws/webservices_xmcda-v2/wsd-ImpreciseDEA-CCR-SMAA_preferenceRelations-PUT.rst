.. _ImpreciseDEA-CCR-SMAA_preferenceRelations-PUT:

ImpreciseDEA-CCR-SMAA_preferenceRelations
=========================================

:Version: 1.0
:Provider: PUT
:SOAP service's name: ``ImpreciseDEA-CCR-SMAA_preferenceRelations-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Determines dominance relations for the given DMUs (alternatives) using SMAA-D method and CCR Data Envelopment Analysis Model with imprecise data. For given number of samples  returns a matrix with alternatives in each row and column. Single cell indicates how many samples of alternative in a row dominates alternative in a column.

**Contact:** Anna Labijak <support@decision-deck.org>


Inputs
------
(For outputs, see :ref:`below <ImpreciseDEA-CCR-SMAA_preferenceRelations-PUT_outputs>`)


- :ref:`units <ImpreciseDEA-CCR-SMAA_preferenceRelations-PUT-units>`
- :ref:`inputsOutputs <ImpreciseDEA-CCR-SMAA_preferenceRelations-PUT-inputsOutputs>`
- :ref:`performanceTable <ImpreciseDEA-CCR-SMAA_preferenceRelations-PUT-performanceTable>`
- :ref:`maxPerformanceTable <ImpreciseDEA-CCR-SMAA_preferenceRelations-PUT-maxPerformanceTable>` *(optional)*
- :ref:`weightsLinearConstraints <ImpreciseDEA-CCR-SMAA_preferenceRelations-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <ImpreciseDEA-CCR-SMAA_preferenceRelations-PUT-methodParameters>`


.. _ImpreciseDEA-CCR-SMAA_preferenceRelations-PUT-units:

units
~~~~~

A list of alternatives (DMUs).

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>


------------------------


.. _ImpreciseDEA-CCR-SMAA_preferenceRelations-PUT-inputsOutputs:

inputsOutputs
~~~~~~~~~~~~~

A list of criteria with specified preference direction. List has to contains at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output).

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   <criteria>
                        <criterion>
							<scale>
								<quantitative>
									<preferenceDirection>
										[...]
									</preferenceDirection>
								</quantitative>
							</scale>
                            [...]
                        </criterion>
                        [...]
                    </criteria>


------------------------


.. _ImpreciseDEA-CCR-SMAA_preferenceRelations-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A list of alternatives (DMUs) performances.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>


------------------------


.. _ImpreciseDEA-CCR-SMAA_preferenceRelations-PUT-maxPerformanceTable:

maxPerformanceTable *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of alternatives (DMUs) maximal performances.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>


------------------------


.. _ImpreciseDEA-CCR-SMAA_preferenceRelations-PUT-weightsLinearConstraints:

weightsLinearConstraints *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of criteria linear constraints.

The input value should be a valid XMCDA document whose main tag is ``<criteriaLinearConstraints>``.
It must have the following form::

   <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID> [...] </criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>


------------------------


.. _ImpreciseDEA-CCR-SMAA_preferenceRelations-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~

"samplesNb" represents the number of samples to generate.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
         <methodParameters>
            <parameter id="samplesNb">
                <value><integer>%1</integer></value>
            </parameter>
            <parameter id="tolerance">
                <value><real>%2</real></value>
            </parameter>
        </methodParameters>

where:

- **%1** is a parameter named "number of samples". This is a int, and the value should conform to the following constraint: The value should be a positive integer..  More formally, the constraint is::

     %1 > 0
  The default value is 100.

- **%2** is a parameter named "tolerance". This is a float, and the value should conform to the following constraint: The value should be a non-negative number..  More formally, the constraint is::

     %2 >= 0
  The default value is 0.0.


------------------------



.. _ImpreciseDEA-CCR-SMAA_preferenceRelations-PUT_outputs:

Outputs
-------


- :ref:`pairwiseOutrankingIndices <ImpreciseDEA-CCR-SMAA_preferenceRelations-PUT-pairwiseOutrankingIndices>`
- :ref:`messages <ImpreciseDEA-CCR-SMAA_preferenceRelations-PUT-messages>`


.. _ImpreciseDEA-CCR-SMAA_preferenceRelations-PUT-pairwiseOutrankingIndices:

pairwiseOutrankingIndices
~~~~~~~~~~~~~~~~~~~~~~~~~

A performance table for given alternatives. Single performance consists of attribute criterionID representing dominated alternative, and a value representing ratio of samples dominating this alternative.

The returned value is a XMCDA document whose main tag is ``<performanceTable>``.
It has the following form::

   <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID> geq [...]</criterionID>
									<value>
									[...]
									</value>
							</performance>
							[...]
						</alternativePerformances>
					</performanceTable>


------------------------


.. _ImpreciseDEA-CCR-SMAA_preferenceRelations-PUT-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/ImpreciseDEA-CCR-SMAA_preferenceRelations-PUT/description-wsDD.xml>`
