.. _plotAlternativesHasseDiagram-PUT:

plotAlternativesHasseDiagram
============================

:Version: 0.2
:Provider: PUT
:SOAP service's name: ``plotAlternativesHasseDiagram-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Draws Hasse diagram - visualization of transitive reduction of a finite partially ordered set. Service developed by Krzysztof Ciomek (Poznan University of Technology, under supervision of Milosz Kadzinski).

**Contact:** 
			Krzysztof Ciomek (k.ciomek@gmail.com),
			Milosz Kadzinski (milosz.kadzinski@cs.put.poznan.pl)
		

**Web page:** None

**Reference:** None


Inputs
------
(For outputs, see :ref:`below <plotAlternativesHasseDiagram-PUT_outputs>`)


- :ref:`alternatives <plotAlternativesHasseDiagram-PUT-alternatives>`
- :ref:`preferenceRelation <plotAlternativesHasseDiagram-PUT-preferenceRelation>`
- :ref:`parameters <plotAlternativesHasseDiagram-PUT-parameters>`


.. _plotAlternativesHasseDiagram-PUT-alternatives:

alternatives
~~~~~~~~~~~~

A list of alternatives.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
				
					<alternatives>
                        <alternative id="[...]">
                            <active>[...]</active>
                        </alternative>
                        [...]
                    </alternatives>
				
			


------------------------


.. _plotAlternativesHasseDiagram-PUT-preferenceRelation:

preferenceRelation
~~~~~~~~~~~~~~~~~~

Necessary assignment based preference relation between alternatives.

The input value should be a valid XMCDA document whose main tag is ``<alternativesComparisons>``.
It must have the following form::

   
				
					<alternativesComparisons>
						<pairs>
							<pair>
								<initial><alternativeID>[...]</alternativeID></initial>
								<terminal><alternativeID>[...]</alternativeID></terminal>
							</pair>
							[...]
						</pairs>
					</alternativesComparisons>
				
			


------------------------


.. _plotAlternativesHasseDiagram-PUT-parameters:

parameters
~~~~~~~~~~

Diagram drawing parameters.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                
                    <methodParameters>
                        <parameter name="cluster">
                            <value>
                                <boolean>%1</boolean>
                            </value>
                        </parameter>
                        <parameter name="transitiveReduction">
                            <value>
                                <boolean>%2</boolean>
                            </value>
                        </parameter>
                        <parameter name="shape">
                            <value>
                                <label>%3</label>
                            </value>
                        </parameter>
                        <parameter name="arrows">
                            <value>
                                <label>%4</label>
                            </value>
                        </parameter>
                    </methodParameters>
                
            

where:

- **%1** is a parameter named "cluster". This is a boolean.
  The default value is true.

- **%2** is a parameter named "transitiveReduction". This is a boolean.
  The default value is true.

- **%3** is a parameter named "shape". It can have the following values:

  - ``roundrect``: Rounded rectangle.

  - ``rect``: Rectangle.

  The default value is roundrect.

- **%4** is a parameter named "arrows". It can have the following values:

  - ``forward``: Forward.

  - ``backward``: Backward.

  - ``both``: Both directions.

  The default value is forward.


------------------------



.. _plotAlternativesHasseDiagram-PUT_outputs:

Outputs
-------


- :ref:`hasseDiagram <plotAlternativesHasseDiagram-PUT-hasseDiagram>`
- :ref:`messages <plotAlternativesHasseDiagram-PUT-messages>`


.. _plotAlternativesHasseDiagram-PUT-hasseDiagram:

hasseDiagram
~~~~~~~~~~~~

Hasse diagram.

The returned value is a XMCDA document whose main tag is ``<alternativeValue>``.
It has the following form::

   
				
					<alternativeValue mcdaConcept="hasseDiagram">
						<alternativesSet>
							<element>
								<alternativeID>[...]</alternativeID>
							</element>
							[...]
						</alternativesSet>
						<value>
							<image>[PNG as BASE64]</image>
						</value>
					</alternativeValue>
				
			


------------------------


.. _plotAlternativesHasseDiagram-PUT-messages:

messages
~~~~~~~~

Messages generated by the program.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/plotAlternativesHasseDiagram-PUT/description-wsDD.xml>`
