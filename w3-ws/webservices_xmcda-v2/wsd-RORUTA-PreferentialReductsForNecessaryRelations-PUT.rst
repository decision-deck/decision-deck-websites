.. _RORUTA-PreferentialReductsForNecessaryRelations-PUT:

RORUTA-PreferentialReductsForNecessaryRelations
===============================================

:Version: 1.0
:Provider: PUT
:SOAP service's name: ``RORUTA-PreferentialReductsForNecessaryRelations-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Finds all preferential reducts for the necessary relations. In other words, for each necessary weak preference relation, it find all minimal sets of pairwise comparisons that imply this relation.

**Contact:** Pawel Rychly (pawelrychly@gmail.com).


Inputs
------
(For outputs, see :ref:`below <RORUTA-PreferentialReductsForNecessaryRelations-PUT_outputs>`)


- :ref:`criteria <RORUTA-PreferentialReductsForNecessaryRelations-PUT-criteria>`
- :ref:`alternatives <RORUTA-PreferentialReductsForNecessaryRelations-PUT-alternatives>`
- :ref:`performances <RORUTA-PreferentialReductsForNecessaryRelations-PUT-performances>`
- :ref:`characteristic-points <RORUTA-PreferentialReductsForNecessaryRelations-PUT-characteristic-points>` *(optional)*
- :ref:`criteria-preference-directions <RORUTA-PreferentialReductsForNecessaryRelations-PUT-criteria-preference-directions>` *(optional)*
- :ref:`necessary-relations <RORUTA-PreferentialReductsForNecessaryRelations-PUT-necessary-relations>`
- :ref:`preferences <RORUTA-PreferentialReductsForNecessaryRelations-PUT-preferences>` *(optional)*
- :ref:`parameters <RORUTA-PreferentialReductsForNecessaryRelations-PUT-parameters>`


.. _RORUTA-PreferentialReductsForNecessaryRelations-PUT-criteria:

criteria
~~~~~~~~

A list of all considered criteria. The input value should be a valid XMCDA document whose main tag is criteria.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
                
                    <criteria>
                        <criterion id="%1" name="%1"></criterion>
                        [...]
                    </criteria>
                    
            


------------------------


.. _RORUTA-PreferentialReductsForNecessaryRelations-PUT-alternatives:

alternatives
~~~~~~~~~~~~

The list of all considered alternatives. The input value should be a valid XMCDA document whose main tag is alternatives. Each alternative may be described using two attributes: id and name. While the first one denotes a machine readable name, the second represents a human readable name.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
                
                    <alternatives>
                        <alternative id="%1" name="%2" />
                        [...]
                    </alternatives>
                    
            


------------------------


.. _RORUTA-PreferentialReductsForNecessaryRelations-PUT-performances:

performances
~~~~~~~~~~~~

Description of evaluation of alternatives on different criteria. It is required to provide the IDs of both criteria and alternatives described previously. The input value should be provided as a valid XMCDA document whose main tag is performanceTable

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   
                
                    <performanceTable>
                        <alternativePerformances>
                            <alternativeID>%1</alternativeID>
                            <performance>
                                <criterionID>%2</criterionID>
                                <value>
                                    <real>%3</real>
                                </value>
                            </performance>
                            [...]
                        </alternativePerformances>
                        [...]
                    </performanceTable>
                    
            


------------------------


.. _RORUTA-PreferentialReductsForNecessaryRelations-PUT-characteristic-points:

characteristic-points *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A set of values associated with the criteria. This input allows to determine what type of value function should be used for the particular criterion. For each criterion that has an associated greater than one value, a piecewise linear value function is used. In this case, the mentioned value denotes a number of characteristic points of this value function. For the criteria that are not listed in this file, or for these for which the provided values are lower than two uses a general value function. The input value should be provided as a valid XMCDA document whose main tag is criteriaValues. Each element should contain both an id of the criterion, and value tag.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.
It must have the following form::

   
                
                     <criteriaValues>
                        <criterionValue>
                            <criterionID>%1</criterionID>
                            <value>
                                <integer>%2</integer>
                            </value>
                        </criterionValue>
                        [...]
                    </criteriaValues>
                    
            


------------------------


.. _RORUTA-PreferentialReductsForNecessaryRelations-PUT-criteria-preference-directions:

criteria-preference-directions *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A set of values associated with criteria that determine their preference direction (0 - gain, 1 - cost).

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.
It must have the following form::

   
                
                     <criteriaValues mcdaConcept="preferenceDirection">
                        <criterionValue>
                            <criterionID>%1</criterionID>
                            <value>
                                <integer>%2</integer>
                            </value>
                        </criterionValue>
                        [...]
                    </criteriaValues>
                    
            


------------------------


.. _RORUTA-PreferentialReductsForNecessaryRelations-PUT-necessary-relations:

necessary-relations
~~~~~~~~~~~~~~~~~~~

A list of all necessary weak preference relations on the set of alternatives. The expected value should be a valid XMCDA document whose main tag is alternativesComparisons. Each relation is denoted as a pair of alternativesID

The input value should be a valid XMCDA document whose main tag is ``<alternativesComparisons>``.
It must have the following form::

   
                
                <alternativesComparisons>
                    <pairs>
                      <pair>
                        <initial>
                          <alternativeID>%1</alternativeID>
                        </initial>
                        <terminal>
                          <alternativeID>%2</alternativeID>
                        </terminal>
                      </pair>
                      [...]
                    </pairs>
                </alternativesComparisons>
                
            


------------------------


.. _RORUTA-PreferentialReductsForNecessaryRelations-PUT-preferences:

preferences *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~

Set of pairwise comparisons of reference alternatives. For a pair of alternatives three types of comparisons are supported. These are the strict preference, weak preference, and indifference. The input value should be provided as a valid XMCDA document whose main tag is alternativesComparisons. For each type of comparison, a separate alternativesComparisons tag should be used. Within these groups a mentioned types are denoted using a comparisonType tag by respectively strict, weak, and indif label. Comparisons should be provided as pairs of alternatives ids.

The input value should be a valid XMCDA document whose main tag is ``<alternativesComparisons>``.
It must have the following form::

   
                
                    <alternativesComparisons>
                        <comparisonType>
                            %1<!-- type of preference: strong, weak, or indif -->
                        </comparisonType>
                        <pairs>
                            <pair>
                                <initial>
                                    <alternativeID>%2</alternativeID>
                                </initial>
                                <terminal>
                                    <alternativeID>%3</alternativeID>
                                </terminal>
                            </pair>
                            [...]
                        </pairs>
                    </alternativesComparisons>
                    [...]
                    
            


------------------------


.. _RORUTA-PreferentialReductsForNecessaryRelations-PUT-parameters:

parameters
~~~~~~~~~~

Single boolean value. Determines whether to use sctrictly increasing (true) or monotonously increasing (false) value functions

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                
                     <methodParameters>
                        <parameter name="strict">
                            <value>
                                <boolean>%1</boolean>
                            </value>
                        </parameter>
                    </methodParameters>
                    
            

where:

- **%1** is a parameter named "Use strictly increasing value functions?". This is a boolean.
  The default value is false.


------------------------



.. _RORUTA-PreferentialReductsForNecessaryRelations-PUT_outputs:

Outputs
-------


- :ref:`reducts-by-necessary-relations <RORUTA-PreferentialReductsForNecessaryRelations-PUT-reducts-by-necessary-relations>`
- :ref:`messages <RORUTA-PreferentialReductsForNecessaryRelations-PUT-messages>`


.. _RORUTA-PreferentialReductsForNecessaryRelations-PUT-reducts-by-necessary-relations:

reducts-by-necessary-relations
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of all necessary relations and underlying preferential reducts. The returned value is a valid XMCDA document whose main tag is the alternativesComparisons. Each element of this data, contains a pair of alternatives ids related by the necessary relation. Each necessary relation has an associated set of string values that describe different possible minimal sets of pairwise comparisons which imply it. Each of these values is a comma separated list of relations. Each comparison is described as a pair of ids separated by one of the three possible labels: strong, weak, and indif. These abbreviations denote respectively a strong preference, weak preference or indifference between mentioned alternatives.

The returned value is a XMCDA document whose main tag is ``<alternativesComparisons>``.
It has the following form::

   
                
                    <alternativesComparisons>
                        <pairs>
                          <pair>
                            <initial>
                              <alternativeID>%1</alternativeID>
                            </initial>
                            <terminal>
                              <alternativeID>%2</alternativeID>
                            </terminal>
                            <values>
                                <value><label>%3</label></value>
                                [...]
                            </values>
                          </pair>
                          [...]
                      </pairs>
                    </alternativesComparisons>
                    
            


------------------------


.. _RORUTA-PreferentialReductsForNecessaryRelations-PUT-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/RORUTA-PreferentialReductsForNecessaryRelations-PUT/description-wsDD.xml>`
