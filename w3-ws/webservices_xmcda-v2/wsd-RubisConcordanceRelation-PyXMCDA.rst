.. _RubisConcordanceRelation-PyXMCDA:

RubisConcordanceRelation
========================

:Version: 1.0
:Provider: PyXMCDA
:SOAP service's name: ``RubisConcordanceRelation-PyXMCDA`` (see :ref:`soap-requests` for details)

Description
-----------

This web service allows to compute a concordance relation as defined in the Rubis methodology.

**Contact:** Thomas Veneziano (thomas.veneziano@uni.lu)

**Reference:** R. Bisdorff, P. Meyer, M. Roubens, Rubis: a bipolar-valued outranking method for the best choice decision problem, 4OR, 6 (2), June 2008, Springer (doi:10.1007/s10288-007-0045-5).


Inputs
------
(For outputs, see :ref:`below <RubisConcordanceRelation-PyXMCDA_outputs>`)


- :ref:`criteria <RubisConcordanceRelation-PyXMCDA-criteria>`
- :ref:`alternatives <RubisConcordanceRelation-PyXMCDA-alternatives>`
- :ref:`performanceTable <RubisConcordanceRelation-PyXMCDA-performanceTable>`
- :ref:`criteriaWeights <RubisConcordanceRelation-PyXMCDA-criteriaWeights>`
- :ref:`valuationDomain <RubisConcordanceRelation-PyXMCDA-valuationDomain>` *(optional)*


.. _RubisConcordanceRelation-PyXMCDA-criteria:

criteria
~~~~~~~~

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.
                             Using thresholds is optional, only the constant ones with mcdaConcept equals to "indifference", "preference" or "veto" will be considered.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
                            
                    <criteria>
                        <criterion>
                            <active>[...]</active>
                            <thresholds>
                            	<threshold
                            		mcdaConcept="indifference"><!-- REQUIRED, must be indifference, preference or veto  -->
                            		<constant><real>[...]</real></constant>
                            	</threshold>
                             </thresholds>
                             [...]
                        </criterion>
                        [...]
                    </criteria>
                    
                     


------------------------


.. _RubisConcordanceRelation-PyXMCDA-alternatives:

alternatives
~~~~~~~~~~~~

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
                            
                    <alternatives>
                        <alternative>
                            <active>[...]</active>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
                    
                     


------------------------


.. _RubisConcordanceRelation-PyXMCDA-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A performance table. The evaluations should be numeric values, i.e. <real>, <integer> or <rational>.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _RubisConcordanceRelation-PyXMCDA-criteriaWeights:

criteriaWeights
~~~~~~~~~~~~~~~

The set of criteria weights.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _RubisConcordanceRelation-PyXMCDA-valuationDomain:

valuationDomain *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Indicates the minimal and the maximal values of the valuation domain for computing the concordance relation. By default the valuation domain is {0,0.5,1}. The median indetermination value is computed as the average of the minimal and the maximal values.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                            
                    <methodParameters
                             name="valuationDomain"> <!-- REQUIRED  -->
                        <parameter
                             name="min"> <!-- REQUIRED  -->
                            <value>
                                <integer>%1</integer>
                            </value>
                        </parameter>
                        <parameter
                             name="max"> <!-- REQUIRED  -->
                            <value>
                                <integer>%2</integer>
                            </value>
                        </parameter>
                    </methodParameters>
                    
                     

where:

- **%1** is a parameter named "min". This is a float.
- **%2** is a parameter named "max". This is a float.

------------------------



.. _RubisConcordanceRelation-PyXMCDA_outputs:

Outputs
-------


- :ref:`alternativesComparisons <RubisConcordanceRelation-PyXMCDA-alternativesComparisons>`
- :ref:`messages <RubisConcordanceRelation-PyXMCDA-messages>`


.. _RubisConcordanceRelation-PyXMCDA-alternativesComparisons:

alternativesComparisons
~~~~~~~~~~~~~~~~~~~~~~~

The concordance relation.

The returned value is a XMCDA document whose main tag is ``<alternativesComparisons>``.


------------------------


.. _RubisConcordanceRelation-PyXMCDA-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/RubisConcordanceRelation-PyXMCDA/description-wsDD.xml>`
