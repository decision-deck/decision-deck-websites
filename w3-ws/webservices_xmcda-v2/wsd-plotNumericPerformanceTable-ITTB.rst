.. _plotNumericPerformanceTable-ITTB:

plotNumericPerformanceTable
===========================

:Version: 1.1
:Provider: ITTB
:SOAP service's name: ``plotNumericPerformanceTable-ITTB`` (see :ref:`soap-requests` for details)

Description
-----------

This web service generates a plot representing the performance table (abscissa: alternatives). Compared to the web service plotNumericPerformanceTable, several options are added. The generated plot can be a bar plot or pie plot. Colors can be used and the title of the plot can be typed. In the case of a bar chart, the axis-labels can also be typed. It is possible to have multiple images or only one image per plot: by column, by line or a grid. In the generated plot, you can also order the alternatives by name, by id or by values in an ascending or descending order.

**Contact:** Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)


Inputs
------
(For outputs, see :ref:`below <plotNumericPerformanceTable-ITTB_outputs>`)


- :ref:`criteria <plotNumericPerformanceTable-ITTB-criteria>`
- :ref:`alternatives <plotNumericPerformanceTable-ITTB-alternatives>`
- :ref:`performanceTable <plotNumericPerformanceTable-ITTB-performanceTable>`
- :ref:`plotOptions <plotNumericPerformanceTable-ITTB-plotOptions>`


.. _plotNumericPerformanceTable-ITTB-criteria:

criteria
~~~~~~~~

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
                            
                    <criteria>
                        <criterion>
                            <active>[...]</active>
                            [...]
                        </criterion>
                        [...]
                    </criteria>
                    
                     


------------------------


.. _plotNumericPerformanceTable-ITTB-alternatives:

alternatives
~~~~~~~~~~~~

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
                   
                    <alternatives>
                        <alternative>
                            <active>[...]</active>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
                    
               


------------------------


.. _plotNumericPerformanceTable-ITTB-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A performance table. The evaluations should be only real or integer numeric values, i.e. <real> or <integer>.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _plotNumericPerformanceTable-ITTB-plotOptions:

plotOptions
~~~~~~~~~~~

Several options to take into account.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                   
					<methodParameters>
						<parameter id="chart_type" name="Chart type">
							<value>
            					<label>%1</label>
       						 </value>
						</parameter>
						 <parameter id="unique_plot" name="Unique plot">
							<value>
            					<label>%2</label>
       						 </value>
						</parameter>
						 <parameter id="plots_display" name="Plots' display">
							<value>
            					<label>%3</label>
       						 </value>
						</parameter>
						
						<parameter id="order_by" name="Order by">
							<value>
            					<label>%4</label>
       						 </value>
						</parameter>
						<parameter id="order" name="order">
							<value>
            					<label>%5</label>
       						 </value>
						</parameter>
						<parameter id="use_color" name="Colors in the chart">
							<value>
            					<label>%6</label>
       						 </value>
						</parameter>
						<parameter id="initial_color" name="Initial color">
							<value>
            					<label>%7</label>
       						 </value>
						</parameter>
						<parameter id="final_color" name="Final color">
							<value>
            					<label>%8</label>
       						 </value>
						</parameter>
						<parameter id= "chart_title" name="Chart title">
							<value>
            					<label>%9</label>
       						 </value>
						</parameter>
						<parameter id="domain_axis" name="Domain axis label">
							<value>
            					<label>%10</label>
       						 </value>
						</parameter >
						<parameter id="range_axis" name="Range axis label">
							<value>
            					<label>%11</label>
       						 </value>
						</parameter>
					</methodParameters>
				
               

where:

- **%1** is a parameter named "Chart type:". It can have the following values:

  - ``barChart``: Bar chart

  - ``pieChart``: Pie chart

  The default value is bar_chart.

- **%2** is a parameter named "Number of images". It can have the following values:

  - ``true``: Unique

  - ``false``: Multiple

  The default value is true.

- **%3** is a parameter named "Plots arrangement". It can have the following values:

  - ``column``: Column

  - ``line``: Line

  - ``grid``: Grid

  The default value is arrange_imgs_by_column.

- **%4** is a parameter named "Order abscissa by:". It can have the following values:

  - ``name``: name

  - ``id``: id

  - ``values``: values

  The default value is order_abs_by_values.

- **%5** is a parameter named "Order:". It can have the following values:

  - ``increasing``: increasing

  - ``decreasing``: decreasing

  The default value is ascending.

- **%6** is a parameter named "Colors:". It can have the following values:

  - ``true``: Yes

  - ``false``: No

  The default value is false.

- **%7** is a parameter named "Initial color:". It can have the following values:

  - ``black``: Black

  - ``red``: Red

  - ``blue``: Blue

  - ``green``: Green

  - ``yellow``: Yellow

  - ``magenta``: Magenta

  - ``cyan``: Cyan

  The default value is black.

- **%8** is a parameter named "Final color:". It can have the following values:

  - ``black``: Black

  - ``red``: Red

  - ``blue``: Blue

  - ``green``: Green

  - ``yellow``: Yellow

  - ``magenta``: Magenta

  - ``cyan``: Cyan

  The default value is black.

- **%9** is a parameter named "Chart title:". This is a string.
- **%10** is a parameter named "X axis label:". This is a string.
- **%11** is a parameter named "Y axis label:". This is a string.

------------------------



.. _plotNumericPerformanceTable-ITTB_outputs:

Outputs
-------


- :ref:`performanceTablePlot <plotNumericPerformanceTable-ITTB-performanceTablePlot>`
- :ref:`messages <plotNumericPerformanceTable-ITTB-messages>`


.. _plotNumericPerformanceTable-ITTB-performanceTablePlot:

performanceTablePlot
~~~~~~~~~~~~~~~~~~~~

A string containing the base64 representation of the png image of the generated plot.

The returned value is a XMCDA document whose main tag is ``<criterionValue>``.


------------------------


.. _plotNumericPerformanceTable-ITTB-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/plotNumericPerformanceTable-ITTB/description-wsDD.xml>`
