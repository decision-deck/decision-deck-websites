.. _csvToXMCDA-categoriesProfiles-PyXMCDA:

csvToXMCDA-categoriesProfiles
=============================

:Version: 1.0
:Provider: PyXMCDA
:SOAP service's name: ``csvToXMCDA-categoriesProfiles-PyXMCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Transforms a file containing categories profiles from a comma-separated values (CSV) file to XMCDA compliant file, containing the alternatives ids with their limits (lowerCategory and upperCategory).

**Contact:** Sébastien Bigaret (sebastien.bigaret@telecom-bretagne.eu)

**Web page:** http://github.com/sbigaret/ws-PyXMCDA


Inputs
------
(For outputs, see :ref:`below <csvToXMCDA-categoriesProfiles-PyXMCDA_outputs>`)


- :ref:`categoriesProfiles.csv <csvToXMCDA-categoriesProfiles-PyXMCDA-categoriesProfiles_csv>`


.. _csvToXMCDA-categoriesProfiles-PyXMCDA-categoriesProfiles_csv:

categoriesProfiles.csv
~~~~~~~~~~~~~~~~~~~~~~

The categories profiles as a CSV file.

Example::

  pMG,Medium,Good
  pMB,Bad,Medium

The input value should be a valid XMCDA document whose main tag is ``<other>``.


------------------------



.. _csvToXMCDA-categoriesProfiles-PyXMCDA_outputs:

Outputs
-------


- :ref:`categoriesProfiles <csvToXMCDA-categoriesProfiles-PyXMCDA-categoriesProfiles>`
- :ref:`messages <csvToXMCDA-categoriesProfiles-PyXMCDA-messages>`


.. _csvToXMCDA-categoriesProfiles-PyXMCDA-categoriesProfiles:

categoriesProfiles
~~~~~~~~~~~~~~~~~~

The equivalent categories profiles.

The returned value is a XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _csvToXMCDA-categoriesProfiles-PyXMCDA-messages:

messages
~~~~~~~~

Status messages.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/csvToXMCDA-categoriesProfiles-PyXMCDA/description-wsDD.xml>`
