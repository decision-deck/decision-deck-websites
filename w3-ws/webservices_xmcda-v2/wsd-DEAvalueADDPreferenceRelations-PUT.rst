.. _DEAvalueADDPreferenceRelations-PUT:

DEAvalueADDPreferenceRelations
==============================

:Version: 1.0
:Provider: PUT
:SOAP service's name: ``DEAvalueADDPreferenceRelations-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Determines necessary and possible dominance relations for the given DMUs (alternatives) using additive Data Envelopment Analysis Model.

**Contact:** Malgorzata Napieraj (napieraj.malgorzata@gmail.com)

**Reference:** Gouveia M. C., Dias L. C., Antunes C. H., Additive DEA based on MCDA with imprecise information (2008).
			Greco S., Mousseau V., Slowinski R., The Possible and the Necessary for Multiple Criteria Group Decision (2009).


Inputs
------
(For outputs, see :ref:`below <DEAvalueADDPreferenceRelations-PUT_outputs>`)


- :ref:`inputsOutputs <DEAvalueADDPreferenceRelations-PUT-inputsOutputs>`
- :ref:`units <DEAvalueADDPreferenceRelations-PUT-units>`
- :ref:`performanceTable <DEAvalueADDPreferenceRelations-PUT-performanceTable>`
- :ref:`weightsLinearConstraints <DEAvalueADDPreferenceRelations-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <DEAvalueADDPreferenceRelations-PUT-methodParameters>`


.. _DEAvalueADDPreferenceRelations-PUT-inputsOutputs:

inputsOutputs
~~~~~~~~~~~~~

A list of criteria with specified preference direction. List has to contains at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output). In addition, minimum and maximum possible value for each critetion can be defined. If not defined, they will be computed from alternatives performances on given criteria.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   <criteria>
                        <criterion>
							<scale>
								<quantitative>
									<preferenceDirection>
										[...]
									</preferenceDirection>
								</quantitative>
							</scale>
							<minimum>[...]</minimum>
							<maximum>[...]</maximum>
                            [...]
                        </criterion>
                        [...]
                    </criteria>


------------------------


.. _DEAvalueADDPreferenceRelations-PUT-units:

units
~~~~~

A list of alternatives (DMUs).

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>


------------------------


.. _DEAvalueADDPreferenceRelations-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A list of alternatives (DMUs) performances.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   <performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>


------------------------


.. _DEAvalueADDPreferenceRelations-PUT-weightsLinearConstraints:

weightsLinearConstraints *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of criteria linear constraints.

The input value should be a valid XMCDA document whose main tag is ``<criteriaLinearConstraints>``.
It must have the following form::

   <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID>[...]</criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>


------------------------


.. _DEAvalueADDPreferenceRelations-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~

"boundariesProvided" indicates if criteria values boundaries are given in criteria input or they have to be computed based on alternatives performances. "transformToUtilites" indicates if given alternatives values are utilities or if they have to be firstly transformed to utilities.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   <methodParameters>
							<parameter name="boundariesProvided">
								<value><boolean>%1</boolean></value>
							</parameter>
							<parameter name="transformToUtilities">
								<value><boolean>%2</boolean></value>
							</parameter>
					</methodParameters>

where:

- **%1** is a parameter named "boundariesProvided". This is a boolean.
  The default value is false.

- **%2** is a parameter named "transformToUtilities". This is a boolean.
  The default value is true.


------------------------



.. _DEAvalueADDPreferenceRelations-PUT_outputs:

Outputs
-------


- :ref:`possibleDominance <DEAvalueADDPreferenceRelations-PUT-possibleDominance>`
- :ref:`necessaryDominance <DEAvalueADDPreferenceRelations-PUT-necessaryDominance>`
- :ref:`messages <DEAvalueADDPreferenceRelations-PUT-messages>`


.. _DEAvalueADDPreferenceRelations-PUT-possibleDominance:

possibleDominance
~~~~~~~~~~~~~~~~~

A list of alternatives pairs being in a possible dominance relation. If list contains pair (A,B), it means that alternative A dominates alternative B.

The returned value is a XMCDA document whose main tag is ``<alternativesComparisons>``.
It has the following form::

   <alternativesComparisons mcdaConcept="possibleDominance">
						<pairs>
						  <pair>
							<initial>
							  <alternativeID>[...]</alternativeID>
							</initial>
							<terminal>
							  <alternativeID>[...]</alternativeID>
							</terminal>
							<value>
							  [...]
							</value>
						  </pair>	
						  [...]
						</pairs>
					</alternativesComparisons>


------------------------


.. _DEAvalueADDPreferenceRelations-PUT-necessaryDominance:

necessaryDominance
~~~~~~~~~~~~~~~~~~

A list of alternatives pairs being in a necessary dominance relation. If list contains pair (A,B), it means that alternative A dominates alternative B.

The returned value is a XMCDA document whose main tag is ``<alternativesComparisons>``.
It has the following form::

   <alternativesComparisons mcdaConcept="necessaryDominance">
						<pairs>
						  <pair>
							<initial>
							  <alternativeID>[...]</alternativeID>
							</initial>
							<terminal>
							  <alternativeID>[...]</alternativeID>
							</terminal>
							<value>
							  [...]
							</value>
						  </pair>	
						  [...]
						</pairs>
					</alternativesComparisons>


------------------------


.. _DEAvalueADDPreferenceRelations-PUT-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/DEAvalueADDPreferenceRelations-PUT/description-wsDD.xml>`
