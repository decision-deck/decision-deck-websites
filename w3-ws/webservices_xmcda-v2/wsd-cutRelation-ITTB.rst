.. _cutRelation-ITTB:

cutRelation
===========

:Version: 1.1
:Provider: ITTB
:SOAP service's name: ``cutRelation-ITTB`` (see :ref:`soap-requests` for details)

Description
-----------

This web service cuts a fuzzy relation (on alternatives) at a given threshold and produces a relation. Compared to the web service cutRelation, the produced relation is not necessary binary. In fact, some parameters are added. You can choose between classical cut and bipolar cut. The cut level is specified and an output relation is generated. In the case of classical cut, the output relation can be crisp output, binary output or other binary output. When a bipolar cut is taken into account, the output relation can be a bipolar output or other bipolar output.

**Contact:** Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)


Inputs
------
(For outputs, see :ref:`below <cutRelation-ITTB_outputs>`)

- :ref:`options <cutRelation-ITTB-options>`- :ref:`alternatives <cutRelation-ITTB-alternatives>`- :ref:`relation <cutRelation-ITTB-relation>`


.. _cutRelation-ITTB-options:

options
~~~~~~~

Generates a graph taking into account the proposed options.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
				
					<methodParameters>
						 <parameter id= "cut_type" name="Cut type">
							<value>
            					<label>%1</label>
       						 </value>
						</parameter>
						<parameter id= "cut_threshold" name="Cut threshold">
							<value>
            					<real>%2</real>
       						 </value>
						</parameter>
						<parameter id= "classical_output" name="Classical output">
							<value>
            					<label>%3</label>
       						 </value>
						</parameter>					
						<parameter id= "bipolar_output" name="Bipolar output">
							<value>
            					<label>%4</label>
       						 </value>
						</parameter>						
						 <parameter id= "true" name="True">
							<value>
            					<real>%5</real>
       						 </value>
						</parameter>
						 <parameter id= "false" name="False">
							<value>
            					<real>%6</real>
       						 </value>
						</parameter>
						 <parameter id= "indeterminate" name="Indeterminate">
							<value>
            					<real>%7</real>
       						 </value>
						</parameter>						
					</methodParameters>
				
			

where:

- **%1** is a parameter named "Cut type:". It can have the following values:

  - ``classical``: classical

  - ``bipolar``: bipolar

  The default value is classical.

- **%2** is a parameter named "Cut threshold:". This is a float.
  The default value is 0.

- **%3** is a parameter named "Classical output:". It can have the following values:

  - ``crisp``: crisp

  - ``classical_binary``: 0-1 valued

  - ``other_binary``: other valued

  The default value is binary.

- **%4** is a parameter named "Bipolar output:". It can have the following values:

  - ``classical_bipolar``: -1-0-1 valued

  - ``other_bipolar``: other valued

  The default value is bipolar.

- **%5** is a parameter named "True:". This is a float.
  The default value is 1.

- **%6** is a parameter named "False:". This is a float.
  The default value is 0.

- **%7** is a parameter named "Indeterminate:". This is a float.
  The default value is 0.5.


------------------------


.. _cutRelation-ITTB-alternatives:

alternatives
~~~~~~~~~~~~

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
				
                    <alternatives>
                        <alternative>
                            <active>[...]</active>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
                    
			


------------------------


.. _cutRelation-ITTB-relation:

relation
~~~~~~~~

A valued relation relative to comparisons of the alternatives. A numeric <value> indicates a the valuation for each <pair> of the relation.

The input value should be a valid XMCDA document whose main tag is ``<alternativesComparisons>``.
It must have the following form::

   
				
                    <alternativesComparisons>
                        <pairs>
                            <pair>
                                <initial>
                                    <alternativeID>[...]</alternativeID>
                                </initial>
                                <terminal>
                                    <alternativeID>[...]</alternativeID>
                                </terminal>
                                <value>
                                    <real>[...]</real>
                                </value>
                            </pair>
                           
                            [...]
                        </pairs> 
                    </alternativesComparisons>
                    
			


------------------------



.. _cutRelation-ITTB_outputs:

Outputs
-------

- :ref:`output_relation <cutRelation-ITTB-outputRelation>`- :ref:`messages <cutRelation-ITTB-messages>`


.. _cutRelation-ITTB-outputRelation:

output_relation
~~~~~~~~~~~~~~~

The relation resulting from the cut.

The returned value is a XMCDA document whose main tag is ``<alternativesComparisons>``.


------------------------


.. _cutRelation-ITTB-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/cutRelation-ITTB/description-wsDD.original.DO_NOT_USE.xml>`
