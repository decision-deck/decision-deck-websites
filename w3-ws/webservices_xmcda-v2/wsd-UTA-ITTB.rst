.. _UTA-ITTB:

UTA
===

:Version: 1.0
:Provider: ITTB
:SOAP service's name: ``UTA-ITTB`` (see :ref:`soap-requests` for details)

Description
-----------

Computes UTA method and if necessary uses post-optimality analysis.

**Contact:** Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)


Inputs
------
(For outputs, see :ref:`below <UTA-ITTB_outputs>`)


- :ref:`criteria <UTA-ITTB-criteria>`
- :ref:`alternatives <UTA-ITTB-alternatives>`
- :ref:`performanceTable <UTA-ITTB-performanceTable>`
- :ref:`criteriaSegments <UTA-ITTB-criteriaSegments>`
- :ref:`alternativesRanks <UTA-ITTB-alternativesRanks>`
- :ref:`methodParameters <UTA-ITTB-methodParameters>` *(optional)*


.. _UTA-ITTB-criteria:

criteria
~~~~~~~~

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active. Optimization direction for the selected criteria is provided (min or max).

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
				
					<criteria>
						<criterion>
							<criterionID>[...]</criterionID>
							<scale>
								<quantitative>
									<preferenceDirection>[...]</preferenceDirection>
								</quantitative>
							</scale>
						</criterion>
					</criteria>
				
			


------------------------


.. _UTA-ITTB-alternatives:

alternatives
~~~~~~~~~~~~

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
				
					<alternatives>
						<alternative>
							<alternativeID>[...]</alternativeID>
						</alternative>
					</alternatives>
				
			


------------------------


.. _UTA-ITTB-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

Values of criteria for different alternatives. It must contains IDs of both criteria and alternatives described previously.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   
				
					<performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>[...]</criterionID>
								<value>
            							<real>[...]</real>
       						 	</value>
							</performance>
						</alternativePerformances>
					</performanceTable>
				
			


------------------------


.. _UTA-ITTB-criteriaSegments:

criteriaSegments
~~~~~~~~~~~~~~~~

Number of segments in each value function to be constructed by UTA.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.
It must have the following form::

   
				
					<criteriaValues>
						<criterionValue>
							<criterionID>[...]</criterionID>
							<value>
            							<integer>[...]</integer>
       						 	</value>
						</criterionValue>
					</criteriaValues>
				
			


------------------------


.. _UTA-ITTB-alternativesRanks:

alternativesRanks
~~~~~~~~~~~~~~~~~

Ranking (preorder) of alternatives, corresponding to pariwize preference and indifference statements

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.
It must have the following form::

   
				
					<alternativesValues>
						<alternativeValue>
							<alternativeID>[...]</alternativeID>
							<value>
            							<integer>[...]</integer>
       						 	</value>
						</alternativeValue>
					</alternativesValues>
				
			


------------------------


.. _UTA-ITTB-methodParameters:

methodParameters *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Post-optimality method : choose between "Yes" or "No".

-**Reference:** E. Jacquet-Lagreze; J. Siskos. Assessing a set of additive utility functions for multicriteria decision-making: the UTA method.European Journal of Operational Research (June 1982), 10 (2), pg. 151-164.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
				
					<methodParameters>
					<parameter id="post_optimality" name="Post optimality analysis">
							<value>
            					<boolean>%1</boolean>
       						 </value>
						</parameter>
					</methodParameters>
				
			

where:

- **%1** is a parameter named "Post-optimality analysis:". It can have the following values:

  - ``true``: Yes

  - ``false``: No

  The default value is false.


------------------------



.. _UTA-ITTB_outputs:

Outputs
-------


- :ref:`valueFunctions <UTA-ITTB-valueFunctions>`
- :ref:`messages <UTA-ITTB-messages>`


.. _UTA-ITTB-valueFunctions:

valueFunctions
~~~~~~~~~~~~~~

Constructed value functions for the selected criteria.

The returned value is a XMCDA document whose main tag is ``<criteria>``.
It has the following form::

   
				
					<criteria mcdaConcept="criteria">
						<criterion>
							<criterionID>[...]</criterionID>
							<criterionFunction>
								<points>
									<point>
										<abscissa><real>[...]</real></abscissa>
										<ordinate><real>[...]</real></ordinate>
									</point>
								</points>
							</criterionFunction>
						</criterion>
					</criteria>
				
			


------------------------


.. _UTA-ITTB-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/UTA-ITTB/description-wsDD.xml>`
