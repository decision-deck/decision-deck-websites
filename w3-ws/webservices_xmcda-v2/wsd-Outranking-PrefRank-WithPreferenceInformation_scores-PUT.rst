.. _Outranking-PrefRank-WithPreferenceInformation_scores-PUT:

Outranking-PrefRank-WithPreferenceInformation_scores
====================================================

:Version: 1.0.0
:Provider: PUT
:SOAP service's name: ``Outranking-PrefRank-WithPreferenceInformation_scores-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Module for calculation PrefRank scores using preference information given by decision maker.

**Contact:** Krzysztof Martyn <krzysztof.martyn@wp.pl>

**Web page:** https://bitbucket.org/Krzysztof_Martyn/prefrank


Inputs
------
(For outputs, see :ref:`below <Outranking-PrefRank-WithPreferenceInformation_scores-PUT_outputs>`)


- :ref:`alternatives <Outranking-PrefRank-WithPreferenceInformation_scores-PUT-input1>`
- :ref:`preferences <Outranking-PrefRank-WithPreferenceInformation_scores-PUT-input2>`
- :ref:`preference_information <Outranking-PrefRank-WithPreferenceInformation_scores-PUT-input3>`
- :ref:`parameters <Outranking-PrefRank-WithPreferenceInformation_scores-PUT-input4>`


.. _Outranking-PrefRank-WithPreferenceInformation_scores-PUT-input1:

alternatives
~~~~~~~~~~~~

Alternatives to consider.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _Outranking-PrefRank-WithPreferenceInformation_scores-PUT-input2:

preferences
~~~~~~~~~~~

Aggregated preferences matrix.

The input value should be a valid XMCDA document whose main tag is ``<alternativesComparisons>``.


------------------------


.. _Outranking-PrefRank-WithPreferenceInformation_scores-PUT-input3:

preference_information
~~~~~~~~~~~~~~~~~~~~~~

Preference information about alternatives. Strength for the alternative is given to the value of true, weakness for false, when no value is given, the alternative does not gain strength or weakness.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _Outranking-PrefRank-WithPreferenceInformation_scores-PUT-input4:

parameters
~~~~~~~~~~

Parameter specifies the algorithm to calculate ranking. There are three algorithms to choose from: PageRank, HITS and Salsa.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
        
		<methodParameters>
			<parameter id="algorithm_type" name="algorithm_type">
				<value>
					<label>%1</label>
				</value>
			</parameter>
			<parameter id="q" name="q">
				<value>
					<real>%2</real>
				</value>
			</parameter>
			<parameter id="number_of_iteration" name="number_of_iteration">
				<value>
					<integer>%3</integer>
				</value>
			</parameter>
			<parameter id="check_convergence" name="check_convergence">
				<value>
					<boolean>%4</boolean>
				</value>
			</parameter>
			<parameter id="early_stopping" name="early_stopping">
				<value>
					<boolean>%5</boolean>
				</value>
			</parameter>
		</methodParameters>
			
      

where:

- **%1** is a parameter named "algorithm type". It can have the following values:

  - ``prefrank_with_preference_information_1``: PrefRank with preference information I

  - ``prefrank_with_preference_information_2``: PrefRank with preference information II

  - ``prefrank_with_preference_information_3``: PrefRank with preference information III

  The default value is item0.

- **%2** is a parameter named "damping factor". This is a float, and the value should conform to the following constraint: The real value must be between 0 and 1.  More formally, the constraint is::

      %2 >= 0 && %2 <= 1 
  The default value is 0.15.

- **%3** is a parameter named "number of iteration". This is a int, and the value should conform to the following constraint: The value should be greater than 1.  More formally, the constraint is::

      %3 > 1
  The default value is 100.

- **%4** is a parameter named "Run averaging if not converge?". This is a boolean.
  The default value is false.

- **%5** is a parameter named "Earlier stop if converge?". This is a boolean.
  The default value is true.


------------------------



.. _Outranking-PrefRank-WithPreferenceInformation_scores-PUT_outputs:

Outputs
-------


- :ref:`positive_flows <Outranking-PrefRank-WithPreferenceInformation_scores-PUT-output1>`
- :ref:`negative_flows <Outranking-PrefRank-WithPreferenceInformation_scores-PUT-output2>`
- :ref:`total_flows <Outranking-PrefRank-WithPreferenceInformation_scores-PUT-output3>`
- :ref:`ranking <Outranking-PrefRank-WithPreferenceInformation_scores-PUT-output4>`
- :ref:`messages <Outranking-PrefRank-WithPreferenceInformation_scores-PUT-output5>`


.. _Outranking-PrefRank-WithPreferenceInformation_scores-PUT-output1:

positive_flows
~~~~~~~~~~~~~~

Positive outranking flows.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _Outranking-PrefRank-WithPreferenceInformation_scores-PUT-output2:

negative_flows
~~~~~~~~~~~~~~

Negative outranking flows.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _Outranking-PrefRank-WithPreferenceInformation_scores-PUT-output3:

total_flows
~~~~~~~~~~~

Final flows computed from the given data.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _Outranking-PrefRank-WithPreferenceInformation_scores-PUT-output4:

ranking
~~~~~~~

PrefRank scores computed from the given data.

The returned value is a XMCDA document whose main tag is ``<alternativesComparisons>``.


------------------------


.. _Outranking-PrefRank-WithPreferenceInformation_scores-PUT-output5:

messages
~~~~~~~~

Messages or errors generated by this module.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/Outranking-PrefRank-WithPreferenceInformation_scores-PUT/description-wsDD.xml>`
