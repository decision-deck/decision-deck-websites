.. _lsRankingCapaIdent-kappalab:

lsRankingCapaIdent
==================

:Version: 1.0
:Provider: kappalab
:SOAP service's name: ``lsRankingCapaIdent-kappalab`` (see :ref:`soap-requests` for details)

Description
-----------

Identifies a Mobius capacity by means of an approach called TOMASO in the particular ranking framework. More precisely, given a preorder on the alternatives, and possibly additional linear constraints expressing preferences, importance of criteria, etc., this algorithm determines, if it exists, a capacity minimising the  "gap" between the given ranking and the one derived from the Choquet integral, and compatible with the additional linear constraints. The problem is solved by quadratic programming.

**Contact:** Patrick Meyer (patrick.meyer@telecom-bretagne.eu)

**Web page:** None

**Reference:** P. Meyer, M. Roubens (2005), Choice, Ranking and Sorting in Fuzzy Multiple Criteria Decision Aid, in: J. Figueira, S. Greco, and M. Ehrgott, Eds, Multiple Criteria Decision Analysis: State of the Art Surveys, volume 78 of International Series in Operations Research and Management Science, chapter 12, pages 471-506. Springer Science + Business Media, Inc., New York. 


Inputs
------
(For outputs, see :ref:`below <lsRankingCapaIdent-kappalab_outputs>`)


- :ref:`criteria <lsRankingCapaIdent-kappalab-criteria>`
- :ref:`alternatives <lsRankingCapaIdent-kappalab-alternatives>`
- :ref:`performanceTable <lsRankingCapaIdent-kappalab-performanceTable>`
- :ref:`shapleyPreorder <lsRankingCapaIdent-kappalab-shapleyPreorder>` *(optional)*
- :ref:`interactionPreorder <lsRankingCapaIdent-kappalab-interactionPreorder>` *(optional)*
- :ref:`shapleyInterval <lsRankingCapaIdent-kappalab-shapleyInterval>` *(optional)*
- :ref:`interactionInterval <lsRankingCapaIdent-kappalab-interactionInterval>` *(optional)*
- :ref:`alternativesPreorder <lsRankingCapaIdent-kappalab-alternativesPreorder>`
- :ref:`kAdditivity <lsRankingCapaIdent-kappalab-kAdditivity>`
- :ref:`separationThreshold <lsRankingCapaIdent-kappalab-separationThreshold>`


.. _lsRankingCapaIdent-kappalab-criteria:

criteria
~~~~~~~~

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
                
                    <criteria>
                        <criterion>
                            <active>[...]</active>
                            [...]
                        </criterion>
                        [...]
                    </criteria>
                    
            


------------------------


.. _lsRankingCapaIdent-kappalab-alternatives:

alternatives
~~~~~~~~~~~~

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
                
                    <alternatives>
                        <alternative>
                            <active>[...]</active>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
                    
            


------------------------


.. _lsRankingCapaIdent-kappalab-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A performance table. The evaluations should be numeric values, i.e. <real>, <integer> or <rational>.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _lsRankingCapaIdent-kappalab-shapleyPreorder:

shapleyPreorder *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A valued relation on criteria expressing importance constraints on the critera. A numeric <value> indicates a minimal preference threshold for each <pair>. One <pair> represents an affirmation of the type "the Shapley importance index of criterion g1 is greater than the Shapley importance index of criterion g2 with preference threshold delta".

The input value should be a valid XMCDA document whose main tag is ``<criteriaComparisons>``.


------------------------


.. _lsRankingCapaIdent-kappalab-interactionPreorder:

interactionPreorder *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A valued relation on pairs of criteria expressing constraints on value of the the Shapley interaction index. A numeric <value> indicates a minimal preference threshold for each <pair> of the relation. One <pair> represents a constraint of the type "the Shapley interaction index of the pair (g1,g2) of criteria is greater than the Shapley interaction index of the pair (g3,g4) of criteria with preference threshold delta".

The input value should be a valid XMCDA document whose main tag is ``<criteriaComparisons>``.


------------------------


.. _lsRankingCapaIdent-kappalab-shapleyInterval:

shapleyInterval *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of <criterionValue> containing the constraints relative to the quantitative importance of the criteria. Each <criterionValue> contains an an <interval>. Each <criteriaValue> represents an affirmation of the type "the Shapley importance index of criterion g1 lies in the interval [a,b]".

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _lsRankingCapaIdent-kappalab-interactionInterval:

interactionInterval *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of <criterionValue> containing the constraints relative to the type and the magnitude of the Shapley interaction index for pairs of criteria. Each <criterionValue> contains an an <interval>. Each <criteriaValue> represents an affirmation of the type "the Shapley interaction index of the pair (g1,g2) of criteria lies in the interval [a,b]".

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _lsRankingCapaIdent-kappalab-alternativesPreorder:

alternativesPreorder
~~~~~~~~~~~~~~~~~~~~

A valued relation relative to the preorder of the alternatives. A numeric <value> indicates a minimal preference threshold for each <pair> of the relation. One <pair> represents a constraint of the type "alternative a is preferred to alternative b with preference threshold delta".

The input value should be a valid XMCDA document whose main tag is ``<alternativesComparisons>``.


------------------------


.. _lsRankingCapaIdent-kappalab-kAdditivity:

kAdditivity
~~~~~~~~~~~

Indicates the level of k-additivity of the Mobius capacity (the Mobius transform of subsets whose cardinal is superior to k vanishes).

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                
                    <methodParameters>
                        <parameter
                             name="kAdditivity"> <!-- REQUIRED  -->
                            <value>
                                <integer>%1</integer>
                            </value>
                        </parameter>
                    </methodParameters>
                    
            

where:

- **%1** is a parameter named "kAdditivity". This is a int, and the value should conform to the following constraint: The value should be a positive integer, less than or equal to the number of criteria..  More formally, the constraint is::

      %1 > 0 
  The default value is 1.


------------------------


.. _lsRankingCapaIdent-kappalab-separationThreshold:

separationThreshold
~~~~~~~~~~~~~~~~~~~

Threshold value indicating the minimal difference in terms of the Choquet integral between two neighbor alternatives in the given ranking.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                
                    <methodParameters>
                        <parameter
                             name="separationThreshold"> <!-- REQUIRED  -->
                            <value>
                                <real>%1</real>
                            </value>
                        </parameter>
                    </methodParameters>
                    
            

where:

- **%1** is a parameter named "separationThreshold". This is a float, and the value should conform to the following constraint: The value should be a strictly positive float, less than the highest possible overall value..  More formally, the constraint is::

      %1 > 0 

------------------------



.. _lsRankingCapaIdent-kappalab_outputs:

Outputs
-------


- :ref:`mobiusCapacity <lsRankingCapaIdent-kappalab-mobiusCapacity>`
- :ref:`messages <lsRankingCapaIdent-kappalab-messages>`


.. _lsRankingCapaIdent-kappalab-mobiusCapacity:

mobiusCapacity
~~~~~~~~~~~~~~

The Mobius transform of a capacity.

The returned value is a XMCDA document whose main tag is ``<criteriaValues>``.
It has the following form::

   
                
                    <criteriaValues mcdaConcept="mobiusCapacity">
                        <criterionValue>
                            <criteriaSet>
                                <element>
                                    <criterionID>[...]</criterionID>
                                </element>
                                [...]
                            </criteriaSet>
                            <value>
                                <real>[...]</real>
                            </value>
                        </criterionValue>
                        [...]
                    </criteriaValues>
                    
            


------------------------


.. _lsRankingCapaIdent-kappalab-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/lsRankingCapaIdent-kappalab/description-wsDD.xml>`
