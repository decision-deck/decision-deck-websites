.. _ACUTA-UTAR:

ACUTA
=====

:Version: 2.0
:Provider: UTAR
:SOAP service's name: ``ACUTA-UTAR`` (see :ref:`soap-requests` for details)

Description
-----------

Computes ACUTA method - analytic center for UTA - which provides a set of additive value functions that have a central position in the polyhedron of admissible value functions, given some preference information (preference relation).

**Contact:** Boris Leistedt (boris.leistedt@gmail.com)


Inputs
------
(For outputs, see :ref:`below <ACUTA-UTAR_outputs>`)


- :ref:`criteria <ACUTA-UTAR-crit>`
- :ref:`alternatives <ACUTA-UTAR-alt>`
- :ref:`performanceTable <ACUTA-UTAR-perfTable>`
- :ref:`preferencesDirections <ACUTA-UTAR-prefDir>`
- :ref:`segments <ACUTA-UTAR-critSeg>`
- :ref:`alternativesRanking <ACUTA-UTAR-altRank>` *(optional)*
- :ref:`alternativesPreferences <ACUTA-UTAR-altComp1>` *(optional)*
- :ref:`alternativesIndifferences <ACUTA-UTAR-altComp2>` *(optional)*
- :ref:`delta <ACUTA-UTAR-delta>` *(optional)*


.. _ACUTA-UTAR-crit:

criteria
~~~~~~~~

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
				
					<criteria>
						<criterion>
							<criterionID>[...]</criterionID>
						</criterion>
					</criteria>
				
			


------------------------


.. _ACUTA-UTAR-alt:

alternatives
~~~~~~~~~~~~

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
				
					<alternatives>
						<alternative>
							<alternativeID>[...]</alternativeID>
						</alternative>
					</alternatives>
				
			


------------------------


.. _ACUTA-UTAR-perfTable:

performanceTable
~~~~~~~~~~~~~~~~

Values of criteria for different alternatives. It must contains IDs of both criteria and alternatives previously described.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   
				
					<performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>[...]</criterionID>
								<value>
            							<real>[...]</real>
       						 	</value>
							</performance>
						</alternativePerformances>
					</performanceTable>
				
			


------------------------


.. _ACUTA-UTAR-prefDir:

preferencesDirections
~~~~~~~~~~~~~~~~~~~~~

Optimization direction for the selected criteria (min or max).

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.
It must have the following form::

   
				
					<criteriaValues>
						<criterionValue>
							<criterionID>[...]</criterionID>
							<value>
            							<label>[...]</label>
       						 	</value>
						</criterionValue>
					</criteriaValues>
				
			


------------------------


.. _ACUTA-UTAR-critSeg:

segments
~~~~~~~~

Number of segments in each value function to be constructed by UTA.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.
It must have the following form::

   
				
					<criteriaValues>
						<criterionValue>
							<criterionID>[...]</criterionID>
							<value>
            							<integer>[...]</integer>
       						 	</value>
						</criterionValue>
					</criteriaValues>
				
			


------------------------


.. _ACUTA-UTAR-altRank:

alternativesRanking *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ranking (preorder) of alternatives, corresponding to pariwize preference and indifference statements.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.
It must have the following form::

   
				
					<alternativesValues>
						<alternativeValue>
							<alternativeID>[...]</alternativeID>
							<value>
            							<integer>[...]</integer>
       						 	</value>
						</alternativeValue>
					</alternativesValues>
				
			


------------------------


.. _ACUTA-UTAR-altComp1:

alternativesPreferences *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Asymmetric part of the preference relation, representing strict preference statements, under the form of paiwise comparisons of alternatives.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.
It must have the following form::

   
				
				<alternativesComparisons>
					<pairs>
						<pair>
							<initial>
								<alternativeID>[...]</alternativeID>
							</initial>
							<terminal>
								<alternativeID>[...]</alternativeID>
							</terminal>
						</pair>
						[...]
					</pairs>
				</alternativesComparisons>
				                			
			


------------------------


.. _ACUTA-UTAR-altComp2:

alternativesIndifferences *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Symmetric part of the preference relation, representing indifference statements, under the form of paiwise comparisons of alternatives.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.
It must have the following form::

   
				
				<alternativesComparisons>
					<pairs>
						<pair>
							<initial>
								<alternativeID>[...]</alternativeID>
							</initial>
							<terminal>
								<alternativeID>[...]</alternativeID>
							</terminal>
						</pair>
						[...]
					</pairs>
				</alternativesComparisons>
				                			
			


------------------------


.. _ACUTA-UTAR-delta:

delta *(optional)*
~~~~~~~~~~~~~~~~~~

Optional delta value for UTA - delta is the utility gap between two successive alternatives in the preference ranking.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
				
					<methodParameters>
						<parameter>
							<value>
								<real>%1</real>
							</value>
						</parameter>
					</methodParameters>
				
			

where:

- **%1** is a parameter named "delta". This is a float, and the value should conform to the following constraint: Delta should be positive and small .  More formally, the constraint is::

      %1 > 0  &&  %1 < 1 

------------------------



.. _ACUTA-UTAR_outputs:

Outputs
-------


- :ref:`valueFunctions <ACUTA-UTAR-valueFunctions>`
- :ref:`message <ACUTA-UTAR-logMessage>`


.. _ACUTA-UTAR-valueFunctions:

valueFunctions
~~~~~~~~~~~~~~

Constructed value functions for the selected criteria and the provided rankings, using ACUTA method.

The returned value is a XMCDA document whose main tag is ``<criteria>``.
It has the following form::

   
				
					<criteria mcdaConcept="criteria">
						<criterion>
							<criterionID>[...]</criterionID>
							<criterionFunction>
								<points>
									<point>
										<abscissa><real>[...]</real></abscissa>
										<ordinate><real>[...]</real></ordinate>
									</point>
								</points>
							</criterionFunction>
						</criterion>
					</criteria>
				
			


------------------------


.. _ACUTA-UTAR-logMessage:

message
~~~~~~~

Log message.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.
It has the following form::

   
				
					<methodMessages mcdaConcept="methodMessage">
						<logMessage>
							<text>[...]</text>
						</logMessage>
						<errorMessage>
							<text>[...]</text>
						</errorMessage>
					</methodMessages>
				
			


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/ACUTA-UTAR/description-wsDD.xml>`
