.. _plotValueFunctions-ITTB:

plotValueFunctions
==================

:Version: 1.1
:Provider: ITTB
:SOAP service's name: ``plotValueFunctions-ITTB`` (see :ref:`soap-requests` for details)

Description
-----------

This web service allows to plot utility functions. Compared to the web service plotValueFunctions, some parameters are added. Colors can be used. You can specify how to display the utility functions: by line, by column or by square. A linear interpolation can be processed in order to connect the different points in the generated utility function.

**Contact:** Sébastien Bigaret (sebastien.bigaret@telecom-bretagne.eu)


Inputs
------
(For outputs, see :ref:`below <plotValueFunctions-ITTB_outputs>`)


- :ref:`criteria <plotValueFunctions-ITTB-criteria>` *(optional)*
- :ref:`valueFunctions <plotValueFunctions-ITTB-valueFunctions>`
- :ref:`methodPlotOptions <plotValueFunctions-ITTB-methodPlotOptions>`


.. _plotValueFunctions-ITTB-criteria:

criteria *(optional)*
~~~~~~~~~~~~~~~~~~~~~

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
                            
                    <criteria>
                        <criterion>
                            <active>[...]</active>
                            [...]
                        </criterion>
                        [...]
                    </criteria>
                    
                     


------------------------


.. _plotValueFunctions-ITTB-valueFunctions:

valueFunctions
~~~~~~~~~~~~~~

Values of utilities of chosen criteria abscissa - Utility functions

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
                   
					<criteria>
						<criterion>
							<criterionID>[...]</criterionID>
							<criterionFunction>
								<points>
									<point>
										<abscissa><real>[...]</real></abscissa>
										<ordinate><real>[...]</real></ordinate>
									</point>
								</points>
							</criterionFunction>
						</criterion>
					</criteria>
				
               


------------------------


.. _plotValueFunctions-ITTB-methodPlotOptions:

methodPlotOptions
~~~~~~~~~~~~~~~~~

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                   
					<methodParameters>	
						  <parameter id="unique_plot" name="Unique plot">
							<value>
            					<label>%1</label>
       						 </value>
						</parameter>
						 <parameter id="plots_display" name="Plots' display">
							<value>
            					<label>%2</label>
       						 </value>
						</parameter>
						<parameter id="linear_interpolation" name="Linear interpolation">
							<value>
            					<label>%3</label>
       						 </value>
						</parameter>
						<parameter id="vertical_lines" name="Vertical lines">
							<value>
            					<label>%4</label>
       						 </value>
						</parameter>	
						 <parameter id="use_color" name="Colors in the plots">
							<value>
            					<label>%5</label>
       						 </value>
						</parameter>
						 <parameter id="selected_color" name="Selected color">
							<value>
            					<label>%6</label>
       						 </value>
       						 </parameter>
					</methodParameters>
				
               

where:

- **%1** is a parameter named "Unique or multiple plot(s)?". It can have the following values:

  - ``true``: Unique

  - ``false``: Multiple

  The default value is true.

- **%2** is a parameter named "Plots arrangement". It can have the following values:

  - ``column``: Column

  - ``line``: Line

  - ``grid``: Grid

  The default value is by_column.

- **%3** is a parameter named "Linear interpolation ?". It can have the following values:

  - ``true``: Yes

  - ``false``: No

  The default value is true.

- **%4** is a parameter named "Add vertical bars ?". It can have the following values:

  - ``true``: Yes

  - ``false``: No

  The default value is false.

- **%5** is a parameter named "Use Colors?". It can have the following values:

  - ``true``: Yes

  - ``false``: No

  The default value is false.

- **%6** is a parameter named "Choose color:". It can have the following values:

  - ``Black``: Black

  - ``Red``: Red

  - ``Blue``: Blue

  - ``Green``: Green

  - ``Yellow``: Yellow

  - ``Magenta``: Magenta

  - ``Cyan``: Cyan

  The default value is black.


------------------------



.. _plotValueFunctions-ITTB_outputs:

Outputs
-------


- :ref:`valueFunctionsPlot <plotValueFunctions-ITTB-valueFunctionsPlot>`
- :ref:`messages <plotValueFunctions-ITTB-messages>`


.. _plotValueFunctions-ITTB-valueFunctionsPlot:

valueFunctionsPlot
~~~~~~~~~~~~~~~~~~

A string containing the base64 representation of the png image of the generated multi-subplot.

The returned value is a XMCDA document whose main tag is ``<criterionValue>``.


------------------------


.. _plotValueFunctions-ITTB-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/plotValueFunctions-ITTB/description-wsDD.xml>`
