.. _PROMETHEE_preference-interactions-PUT:

PrometheePreferenceWithInteractions
===================================

:Version: 1.0.0
:Provider: PUT
:Name: PROMETHEE_preference-interactions
:SOAP service's name: ``PROMETHEE_preference-interactions-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Computes aggregated preference indices with interactions between criteria
The key feature of this module is its flexibility in terms of the types of
elements allowed to compare, i.e. alternatives vs alternatives, alternatives vs
boundary profiles and alternatives vs central (characteristic) profiles.
Each criterion can have its own preference function (one of six predefined functions).

**Web page:** https://github.com/Yamadads/PrometheeDiviz


Inputs
------
(For outputs, see :ref:`below <PROMETHEE_preference-interactions-PUT_outputs>`)


- :ref:`criteria <PROMETHEE_preference-interactions-PUT-input3>`
- :ref:`alternatives <PROMETHEE_preference-interactions-PUT-input1>`
- :ref:`performance_table <PROMETHEE_preference-interactions-PUT-input4>`
- :ref:`profiles_performance_table <PROMETHEE_preference-interactions-PUT-input5>` *(optional)*
- :ref:`weights <PROMETHEE_preference-interactions-PUT-input6>`
- :ref:`generalised_criteria <PROMETHEE_preference-interactions-PUT-input7>` *(optional)*
- :ref:`interactions <PROMETHEE_preference-interactions-PUT-input8>`
- :ref:`categories_profiles <PROMETHEE_preference-interactions-PUT-input2>` *(optional)*
- :ref:`method_parameters <PROMETHEE_preference-interactions-PUT-input9>`


.. _PROMETHEE_preference-interactions-PUT-input3:

criteria
~~~~~~~~

Criteria to consider, possibly with preference and indifference thresholds. For Gaussian function it needs inflection point (sigma). Each criterion must have a preference direction specified (min or max). It is worth mentioning that this module allows to define thresholds as constants as well as linear functions.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _PROMETHEE_preference-interactions-PUT-input1:

alternatives
~~~~~~~~~~~~

Alternatives to consider.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _PROMETHEE_preference-interactions-PUT-input4:

performance_table
~~~~~~~~~~~~~~~~~

The performance of alternatives.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _PROMETHEE_preference-interactions-PUT-input5:

profiles_performance_table *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The performance of profiles (boundary or central).

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _PROMETHEE_preference-interactions-PUT-input6:

weights
~~~~~~~

Weights of criteria to consider.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _PROMETHEE_preference-interactions-PUT-input7:

generalised_criteria *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ID number of predefined preference function specified for each criterion.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _PROMETHEE_preference-interactions-PUT-input8:

interactions
~~~~~~~~~~~~

Declarations of criteria interactions which should be taken into account. Possible interactions are: "strengthening", "weakening" and "antagonistic".

Every such declaration contains a pair of criteria and a numerical value for the interaction coefficient. This value should be negative for "weakening" and positive for "strenghtening" and "antagonistic".
This coefficient should be interpreted as follows: the larger its absolute value, the more important the interaction effect.  All interaction coefficients should fulfill positive net balance condition.

The order in which each criterion appears in its interaction definition is significant for antagonistic interactions (it designates the direction in which certain criterion affects the other).
For strengthening and weakening order is not important. (Coefficients k_ij = k_ji and only one of this is needed).

There are no limits on the number of interactions that can be declared, but keep in mind that it should be rather small to make sense.

It is possible to declare more than one interactions of the same type, although please note that considering the same pair of criteria involved, the "strengthening" and "weakening" interactions are mutually exclusive.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.
It must have the following form::

   
 <criteriaValues mcdaConcept="criteriaInteractions">
    <criterionValue mcdaConcept="strengthening">
      <criteriaSet>
        <element>
          <criterionID>c01</criterionID>
        </element>
        <element>
          <criterionID>c02</criterionID>
        </element>
      </criteriaSet>
      <value>
        <real>9.5</real>
      </value>
    </criterionValue>
    <criterionValue mcdaConcept="antagonistic">
      <criteriaSet>
        <element>
          <criterionID>c02</criterionID>
        </element>
        <element>
          <criterionID>c04</criterionID>
        </element>
      </criteriaSet>
      <value>
        <real>0.01</real>
      </value>
    </criterionValue>
  </criteriaValues>
      


------------------------


.. _PROMETHEE_preference-interactions-PUT-input2:

categories_profiles *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Definitions of central or boundary profiles connected with classes (categories)

The input value should be a valid XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _PROMETHEE_preference-interactions-PUT-input9:

method_parameters
~~~~~~~~~~~~~~~~~

First parameter specifies the type of elements provided for comparison.

Choosing 'boundary_profiles' or 'central_profiles' requires providing inputs 'classes_profiles' and 'profiles_performance_table' as well (which are optional by default).

Second parameter specifies the type of function used for comparison of each criterion.
Choosing 'specified' requires providing inputs "generalised_criterion" which is optional by default.
Choosing some of numbers sets same function for all criteria.
Third parameter specifies type of Z function in computing criteria interaction effect.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
        
        <methodParameters>
          <parameter name="comparison_with">
            <value>
              <label>%1</label>
            </value>
          </parameter>
          <parameter name="generalised_criterion">
            <value>
              <label>%2</label>
            </value>
          </parameter>
          <parameter name="z_function">
            <value>
              <label>%3</label>
            </value>
          </parameter>
        </methodParameters>
        
      

where:

- **%1** is a parameter named "comparison with". It can have the following values:

  - ``alternatives``: alternatives vs alternatives

  - ``boundary_profiles``: alternatives vs boundary profiles

  - ``central_profiles``: alternatives vs central (characteristic) profiles

  The default value is item0.

- **%2** is a parameter named "generalised criterion". It can have the following values:

  - ``specified``: Each criterion needs its own function

  - ``usual``: Usual Criterion

  - ``u-shape``: U-Shape Criterion, needs indifference threshold specified in criterion.

  - ``v-shape``: V-Shape Criterion, needs threshold of strict preference specified in criterion.

  - ``level``: Level Criterion, needs both indifference and strict preference thresholds specified in criterion.

  - ``v-shape-ind``: V-Shape with Indifference Criterion, needs both indifference and strict preference thresholds specified in criterion.

  - ``gaussian``: Gaussian Criterion, needs the inflection point of the preference function specified in criterion.

  The default value is item0.

- **%3** is a parameter named "z function". It can have the following values:

  - ``multiplication``: Z(x, y) = xy

  - ``minimum``: Z(x, y) = min{x, y}

  The default value is item0.


------------------------



.. _PROMETHEE_preference-interactions-PUT_outputs:

Outputs
-------


- :ref:`preferences <PROMETHEE_preference-interactions-PUT-output1>`
- :ref:`partial_preferences <PROMETHEE_preference-interactions-PUT-output2>`
- :ref:`messages <PROMETHEE_preference-interactions-PUT-output3>`


.. _PROMETHEE_preference-interactions-PUT-output1:

preferences
~~~~~~~~~~~

Aggregated preference matrix computed from the given data. This matrix aggregates partial preference indices from all criteria into single preference index per pair of alternatives or alternatives/profiles.

The returned value is a XMCDA document whose main tag is ``<alternativesComparisons>``.


------------------------


.. _PROMETHEE_preference-interactions-PUT-output2:

partial_preferences
~~~~~~~~~~~~~~~~~~~

Preference matrix computed from the given data. This matrix contains partial preference indices for all criteria and all pairs of alternatives or alternatives/profiles.

The returned value is a XMCDA document whose main tag is ``<alternativesComparisons>``.


------------------------


.. _PROMETHEE_preference-interactions-PUT-output3:

messages
~~~~~~~~

Messages or errors generated by this module.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/PROMETHEE_preference-interactions-PUT/description-wsDD.xml>`
