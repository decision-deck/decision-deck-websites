.. _plotStarGraphPerformanceTable-ITTB:

plotStarGraphPerformanceTable
=============================

:Version: 1.0
:Provider: ITTB
:SOAP service's name: ``plotStarGraphPerformanceTable-ITTB`` (see :ref:`soap-requests` for details)

Description
-----------

This web service generates, for each alternative, a plot representing the performance table as a  star graph. Colors can be used. You can specify how to display the star graphs: by line, by column or in a grid. The star graphs can also be ordered by name or by id (of the alternatives).

**Contact:** Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)


Inputs
------
(For outputs, see :ref:`below <plotStarGraphPerformanceTable-ITTB_outputs>`)


- :ref:`criteria <plotStarGraphPerformanceTable-ITTB-criteria>`
- :ref:`alternatives <plotStarGraphPerformanceTable-ITTB-alternatives>`
- :ref:`performanceTable <plotStarGraphPerformanceTable-ITTB-performanceTable>`
- :ref:`options <plotStarGraphPerformanceTable-ITTB-options>`


.. _plotStarGraphPerformanceTable-ITTB-criteria:

criteria
~~~~~~~~

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active. Preference direction for the selected criteria can be provided (min or max). In this web service, the default value is set to max.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
                   
                    <criteria>
                        <criterion>
                            <active>[...]</active>
                            <scale>
								<quantitative>
									<preferenceDirection>[...]</preferenceDirection>
								</quantitative>
							</scale>
                        </criterion>
                        [...]
                    </criteria>
                    
               


------------------------


.. _plotStarGraphPerformanceTable-ITTB-alternatives:

alternatives
~~~~~~~~~~~~

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
                   
                    <alternatives>
                        <alternative>
                            <active>[...]</active>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
                    
               


------------------------


.. _plotStarGraphPerformanceTable-ITTB-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A performance table. The evaluations should be only real or integer numeric values, i.e. <real> or <integer>.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _plotStarGraphPerformanceTable-ITTB-options:

options
~~~~~~~

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                   
					<methodParameters>	
						
						  <parameter id="preference_direction" name="Use preference direction">
							<value>
            					<label>%1</label>
       						 </value>
						</parameter>
						  <parameter id="unique_plot" name="Unique plot">
							<value>
            					<label>%2</label>
       						 </value>
						</parameter>
						 <parameter id="plots_display" name="Plots' display">
							<value>
            					<label>%3</label>
       						 </value>
						</parameter>
                        <parameter id="order_by" name="Order by">
							<value>
            					<label>%4</label>
       						 </value>
						</parameter>
                        <parameter id="order" name="order">
							<value>
            					<label>%5</label>
       						 </value>
						</parameter>						
						 <parameter id="use_color" name="Colors in the plots">
							<value>
            					<label>%6</label>
       						 </value>
						</parameter>
						 <parameter id="selected_color" name="Selected color">
							<value>
            					<label>%7</label>
       						 </value>
       						 </parameter>
					</methodParameters>
				
               

where:

- **%1** is a parameter named "Use preference directions?". It can have the following values:

  - ``true``: Yes

  - ``false``: No

  The default value is false.

- **%2** is a parameter named "Unique or multiple plot(s)?". It can have the following values:

  - ``true``: Unique

  - ``false``: Multiple

  The default value is true.

- **%3** is a parameter named "Plots arrangement". It can have the following values:

  - ``column``: Column

  - ``line``: Line

  - ``grid``: Grid

  The default value is column.

- **%4** is a parameter named "Order abscissa by:". It can have the following values:

  - ``name``: name

  - ``id``: id

  The default value is order_by_id.

- **%5** is a parameter named "order". It can have the following values:

  - ``increasing``: Ascending order

  - ``decreasing``: Descending order

  The default value is increasing.

- **%6** is a parameter named "Use Colors?". It can have the following values:

  - ``true``: Yes

  - ``false``: No

  The default value is false.

- **%7** is a parameter named "Choose color:". It can have the following values:

  - ``black``: Black

  - ``red``: Red

  - ``blue``: Blue

  - ``green``: Green

  - ``yellow``: Yellow

  - ``magenta``: Magenta

  - ``cyan``: Cyan

  The default value is black.


------------------------



.. _plotStarGraphPerformanceTable-ITTB_outputs:

Outputs
-------


- :ref:`starGraph <plotStarGraphPerformanceTable-ITTB-starGraph>`
- :ref:`messages <plotStarGraphPerformanceTable-ITTB-messages>`


.. _plotStarGraphPerformanceTable-ITTB-starGraph:

starGraph
~~~~~~~~~

A string containing the base64 representation of the png image of the generated plot.

The returned value is a XMCDA document whose main tag is ``<alternativeValue>``.


------------------------


.. _plotStarGraphPerformanceTable-ITTB-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/plotStarGraphPerformanceTable-ITTB/description-wsDD.xml>`
