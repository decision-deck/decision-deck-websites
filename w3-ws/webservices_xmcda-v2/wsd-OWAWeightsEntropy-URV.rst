.. _OWAWeightsEntropy-URV:

OWAWeightsEntropy
=================

:Version: 1.0
:Provider: URV
:SOAP service's name: ``OWAWeightsEntropy-URV`` (see :ref:`soap-requests` for details)

Description
-----------

Entropy of the weights given to the Ordered Weighted Average operator (OWA)

 The Ordered Weighted Averaging operators, commonly called OWA operators, provide a parameterized class of mean type aggregation operators.
 OWA was defined by R.R. Yager in 1988.
 The main characteristic of this aggregation operator is the fact that the weights are associated to the values instead to criteria.
 In this way, we can define different aggregation policies according to the importance that we associate to high and low performance scores.
 There exist different measures to characterise a set of weights associated to an OWA operator.
 In this module we implement the ENTROPY measure.

**Contact:** Aida Valls <aida.valls@urv.cat>

**Reference:** 
		R.R. Yager, On Ordered Weighted Averaging Aggregation, IEEE Transactions on Systems, Man and Cybernetics, 18, pp. 119-145 (1988)
		


Inputs
------
(For outputs, see :ref:`below <OWAWeightsEntropy-URV_outputs>`)


- :ref:`criteria <OWAWeightsEntropy-URV-input1>`
- :ref:`weights <OWAWeightsEntropy-URV-input2>`


.. _OWAWeightsEntropy-URV-input1:

criteria
~~~~~~~~

A list of criteria. Criteria are always active.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
                            
                    <criteria>
                        <criterion>
                            <active>[...]</active>
                            [...]
                        </criterion>
                        [...]
                    </criteria>
                    
                     


------------------------


.. _OWAWeightsEntropy-URV-input2:

weights
~~~~~~~

The weights are associated to the values of the alteranatives. Assuming that the values
on the alternative will be sorted from the best to the worst (e.g. 7, 5, 5, 3, 1), the list of weights must be ordered
according to the importance that is given to the values, from the highest to the lowest.
For example a list of weights as (0.5, 0.5, 0, 0, 0) is ignoring the 3 lowest values, and making an average of the
two highest ones. A list like (0, 0, 1, 0 ,0 ) is calculating the median, while (0, 0, 0, 0, 1) is taking the minimum.
Notice that the sum of weights is required to be 1.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.
It must have the following form::

   
		  
			<alternativesValues>
			  <alternativeValue>
				<values>
					<value>
						<real>[...]</real>
					</value>
					<value>
						<integer>[...]</integer>
					</value>
					[...]
			  </alternativeValue>
			</alternativesValues>
		  
		  


------------------------



.. _OWAWeightsEntropy-URV_outputs:

Outputs
-------


- :ref:`owaMeasures <OWAWeightsEntropy-URV-output0>`
- :ref:`messages <OWAWeightsEntropy-URV-output1>`


.. _OWAWeightsEntropy-URV-output0:

owaMeasures
~~~~~~~~~~~

Result obtained from the analysis of the OWA weights.

The returned value is a XMCDA document whose main tag is ``<criterionValue>``.


------------------------


.. _OWAWeightsEntropy-URV-output1:

messages
~~~~~~~~

A status message.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/OWAWeightsEntropy-URV/description-wsDD.xml>`
