.. _DEASMAAvalueADDEfficiencies-PUT:

DEASMAAvalueADDEfficiencies
===========================

:Version: 1.0
:Provider: PUT
:SOAP service's name: ``DEASMAAvalueADDEfficiencies-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Computes efficiency scores for the given DMUs (alternatives) using SMAA-D method and additive Data Envelopment Analysis Model. For given number of buckets and samples, returns a matrix with alternatives in each row and buckets representing efficiency intervals in each column. Single cell indicates how many samples gave efficiency scores of respective alternative in respective bucket.

**Contact:** Malgorzata Napieraj (napieraj.malgorzata@gmail.com)

**Reference:** Gouveia M. C., Dias L. C., Antunes C. H., Additive DEA based on MCDA with imprecise information (2008).
			Lahdelma R., Salminen P., Stochastic multicriteria acceptability analysis using the data envelopment model (2004).


Inputs
------
(For outputs, see :ref:`below <DEASMAAvalueADDEfficiencies-PUT_outputs>`)


- :ref:`inputsOutputs <DEASMAAvalueADDEfficiencies-PUT-inputsOutputs>`
- :ref:`units <DEASMAAvalueADDEfficiencies-PUT-units>`
- :ref:`performanceTable <DEASMAAvalueADDEfficiencies-PUT-performanceTable>`
- :ref:`weightsLinearConstraints <DEASMAAvalueADDEfficiencies-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <DEASMAAvalueADDEfficiencies-PUT-methodParameters>`


.. _DEASMAAvalueADDEfficiencies-PUT-inputsOutputs:

inputsOutputs
~~~~~~~~~~~~~

A list of criteria with specified preference direction. List has to contains at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output).

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   <criteria>
                        <criterion>
							<scale>
								<quantitative>
									<preferenceDirection>
										[...]
									</preferenceDirection>
								</quantitative>
							</scale>
                            [...]
                        </criterion>
                        [...]
                    </criteria>


------------------------


.. _DEASMAAvalueADDEfficiencies-PUT-units:

units
~~~~~

A list of alternatives (DMUs).

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>


------------------------


.. _DEASMAAvalueADDEfficiencies-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A list of alternatives (DMUs) performances.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   <performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>


------------------------


.. _DEASMAAvalueADDEfficiencies-PUT-weightsLinearConstraints:

weightsLinearConstraints *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of criteria linear constraints.

The input value should be a valid XMCDA document whose main tag is ``<criteriaLinearConstraints>``.
It must have the following form::

   <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID>[...]</criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>


------------------------


.. _DEASMAAvalueADDEfficiencies-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~

"boundariesProvided" indicates if criteria values boundaries are given in criteria input or they have to be computed based on alternatives performances. "samplesNo" represents the number of samples to generate. "intervalsNo", represents the number of buckets which efficiency scores will be assigned to. "transformToUtilites" indicates if given alternatives values are utilities or if they have to be firstly transformed to utilities.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   <methodParameters>
							<parameter name="boundariesProvided">
								<value><boolean>%1</boolean></value>
							</parameter>
							<parameter name="samplesNo">
								<value><integer>%2</integer></value>
							</parameter>
							<parameter name="intervalsNo">
								<value><integer>%3</integer></value>
							</parameter>
							<parameter name="transformToUtilities">
								<value><boolean>%4</boolean></value>
							</parameter>
					</methodParameters>

where:

- **%1** is a parameter named "boundariesProvided". This is a boolean.
  The default value is false.

- **%2** is a parameter named "samplesNo". This is a int, and the value should conform to the following constraint: The value should be a positive integer..  More formally, the constraint is::

      %2 > 0 
  The default value is 100.

- **%3** is a parameter named "intervalsNo". This is a int, and the value should conform to the following constraint: The value should be a positive integer..  More formally, the constraint is::

      %3 > 0 
  The default value is 10.

- **%4** is a parameter named "transformToUtilities". This is a boolean.
  The default value is true.


------------------------



.. _DEASMAAvalueADDEfficiencies-PUT_outputs:

Outputs
-------


- :ref:`efficiencyDistribution <DEASMAAvalueADDEfficiencies-PUT-efficiencyDistribution>`
- :ref:`maxEfficiency <DEASMAAvalueADDEfficiencies-PUT-maxEfficiency>`
- :ref:`minEfficiency <DEASMAAvalueADDEfficiencies-PUT-minEfficiency>`
- :ref:`avgEfficiency <DEASMAAvalueADDEfficiencies-PUT-avgEfficiency>`
- :ref:`messages <DEASMAAvalueADDEfficiencies-PUT-messages>`


.. _DEASMAAvalueADDEfficiencies-PUT-efficiencyDistribution:

efficiencyDistribution
~~~~~~~~~~~~~~~~~~~~~~

A performance table for given alternatives. Single performance consists of attribute criterionID representing certain bucket, and a value representing ratio of efficiency scores in this bucket.

The returned value is a XMCDA document whose main tag is ``<performanceTable>``.
It has the following form::

   <performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>Bucket [...]</criterionID>
									<value>
									[...]
									</value>
							</performance>
							[...]
						</alternativePerformances>
					</performanceTable>


------------------------


.. _DEASMAAvalueADDEfficiencies-PUT-maxEfficiency:

maxEfficiency
~~~~~~~~~~~~~

A list of alternatives with maximum efficiency scores (obtained for given sample).

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   <alternativesValues mcdaConcept="efficiency">
						<alternativeValue>
						  <alternativeID>[...]</alternativeID>
						  <value>
							[...]
						  </value>
						</alternativeValue>
						[...]
					</alternativesValues>


------------------------


.. _DEASMAAvalueADDEfficiencies-PUT-minEfficiency:

minEfficiency
~~~~~~~~~~~~~

A list of alternatives with computed minimum efficiency scores (obtained for given sample).

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   <alternativesValues mcdaConcept="efficiency">
						<alternativeValue>
						  <alternativeID>[...]</alternativeID>
						  <value>
							[...]
						  </value>
						</alternativeValue>
						[...]
					</alternativesValues>


------------------------


.. _DEASMAAvalueADDEfficiencies-PUT-avgEfficiency:

avgEfficiency
~~~~~~~~~~~~~

A list of alternatives with average efficiency scores (obtained for given sample).

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   <alternativesValues mcdaConcept="efficiency">
						<alternativeValue>
						  <alternativeID>[...]</alternativeID>
						  <value>
							[...]
						  </value>
						</alternativeValue>
						[...]
					</alternativesValues>


------------------------


.. _DEASMAAvalueADDEfficiencies-PUT-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/DEASMAAvalueADDEfficiencies-PUT/description-wsDD.xml>`
