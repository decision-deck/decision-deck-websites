.. _RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT:

RORUTADIS-NecessaryAssignment-basedPreferenceRelation
=====================================================

:Version: 0.1
:Provider: PUT
:SOAP service's name: ``RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Compares necessary assignments of alternatives using Robust Ordinal Regression for value-based sorting. It is possible to provide an additional optional preference information: example alternatives assignments, assignment pairwise comparisons and desired class cardinalities. Service developed by Krzysztof Ciomek (Poznan University of Technology, under supervision of Milosz Kadzinski).

**Contact:** 
			Krzysztof Ciomek (k.ciomek@gmail.com),
			Milosz Kadzinski (milosz.kadzinski@cs.put.poznan.pl)
		

**Web page:** https://github.com/kciomek/rorutadis

**Reference:** None


Inputs
------
(For outputs, see :ref:`below <RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT_outputs>`)


- :ref:`criteria <RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-criteria>`
- :ref:`alternatives <RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-alternatives>`
- :ref:`categories <RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-categories>`
- :ref:`performanceTable <RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-performanceTable>`
- :ref:`assignmentExamples <RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-assignmentExamples>` *(optional)*
- :ref:`assignmentComparisons <RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-assignmentComparisons>` *(optional)*
- :ref:`categoriesCardinalities <RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-categoriesCardinalities>` *(optional)*
- :ref:`strictlyMonotonicValueFunctions <RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-strictlyMonotonicValueFunctions>`


.. _RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-criteria:

criteria
~~~~~~~~

A list of criteria (<criteria> tag) with information about preference direction (<criteriaValues mcdaConcept="preferenceDirection">, 0 - gain, 1 - cost) and number of characteristic points (<criteriaValues mcdaConcept="numberOfCharacteristicPoints">, 0 for the most general marginal utility function or integer grater or equal to 2) of each criterion.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
				
					<criteria>
						<criterion id="[...]" />
						[...]
					</criteria>

					<criteriaValues mcdaConcept="preferenceDirection">
						<criterionValue>
							<criterionID>[...]</criterionID>
							<value><integer>[...]</integer></value>
						</criterionValue>
						[...]
					</criteriaValues>

					<criteriaValues mcdaConcept="numberOfCharacteristicPoints">
						<criterionValue>
							<criterionID>[...]</criterionID>
							<value><integer>[0|integer greater or equal to 2]</integer></value>
						</criterionValue>
						[...]
					</criteriaValues>
				
			


------------------------


.. _RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-alternatives:

alternatives
~~~~~~~~~~~~

A list of alternatives.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
				
					<alternatives>
                        <alternative id="[...]">
                            <active>[...]</active>
                        </alternative>
                        [...]
                    </alternatives>
				
			


------------------------


.. _RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-categories:

categories
~~~~~~~~~~

A list of categories (classes). List must be sorted from the worst category to the best.

The input value should be a valid XMCDA document whose main tag is ``<categories>``.
It must have the following form::

   
				
					<categories>
                        <category id="[...]" />
                        [...]
                    </categories>
				
			


------------------------


.. _RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

The performances of the alternatives.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-assignmentExamples:

assignmentExamples *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of assignment examples of alternatives to intervals of categories (classes) or to a specific category (class).

The input value should be a valid XMCDA document whose main tag is ``<alternativesAffectations>``.
It must have the following form::

   
				
					<alternativesAffectations>
						<alternativeAffectation>
							<alternativeID>[...]</alternativeID>
							<categoryID>[...]</categoryID>
						</alternativeAffectation>
						[...]
						<alternativeAffectation>
							<alternativeID>[...]</alternativeID>
							<categoriesInterval>
								<lowerBound>
									<categoryID>[...]</categoryID>
								</lowerBound>
								<upperBound>
									<categoryID>[...]</categoryID>
								</upperBound>
							</categoriesInterval>
						</alternativeAffectation>
						[...]
						<alternativeAffectation>
							<alternativeID>[...]</alternativeID>
							<categoriesSet>
								<categoryID>[...]</categoryID>
								[...]
							</categoriesSet>
						</alternativeAffectation>
						[...]
					</alternativesAffectations>
				
			


------------------------


.. _RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-assignmentComparisons:

assignmentComparisons *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Two lists of assignment pairwise comparisons. A comparison from list with attribute mcdaConcept="atLeastAsGoodAs" indicates that some alternative should be assigned to class at least as good as class of some other alternative (k = 0) or at least better by k classes (k > 0). A comparison from list with attribute mcdaConcept="atMostAsGoodAs" indicates that some alternative should be assigned to class at most better by k classes (k > 0) then some other alternative.

The input value should be a valid XMCDA document whose main tag is ``<alternativesComparisons>``.
It must have the following form::

   
				
					<alternativesComparisons mcdaConcept="atLeastAsGoodAs">
						<pairs>
							<pair>
								<initial><alternativeID>[...]</alternativeID></initial>
								<terminal><alternativeID>[...]</alternativeID></terminal>
								<value><integer>k</integer></value>
							</pair>
							[...]
						</pairs>
					</alternativesComparisons>

					<alternativesComparisons mcdaConcept="atMostAsGoodAs">
						<pairs>
							[...]
						</pairs>
					</alternativesComparisons>
				
			


------------------------


.. _RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-categoriesCardinalities:

categoriesCardinalities *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of category (class) cardinality constraints. It allows to define minimal and/or maximal desired category (class) cardinalities.

The input value should be a valid XMCDA document whose main tag is ``<categoriesValues>``.
It must have the following form::

   
				
					<categoriesValues>
						<categoryValue>
							<categoryID>[...]</categoryID>
							<value>
								<interval>
									<lowerBound><integer>[...]</integer></lowerBound>
									<upperBound><integer>[...]</integer></upperBound>
								</interval>
							</value>
						</categoryValue>
						[...]
					</categoriesValues>
				
			


------------------------


.. _RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-strictlyMonotonicValueFunctions:

strictlyMonotonicValueFunctions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Whether marginal value functions strictly monotonic (true) or weakly monotonic (false).

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                
                    <methodParameters>
                        <parameter name="strictlyMonotonicValueFunctions">
                            <value>
                                <boolean>%1</boolean>
                            </value>
                        </parameter>
                    </methodParameters>
                
            

where:

- **%1** is a parameter named "strictlyMonotonicValueFunctions". This is a boolean.
  The default value is false.


------------------------



.. _RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT_outputs:

Outputs
-------


- :ref:`assignmentComparisons <RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-assignmentComparisons-out>`
- :ref:`messages <RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-messages>`


.. _RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-assignmentComparisons-out:

assignmentComparisons
~~~~~~~~~~~~~~~~~~~~~

Necessary assignment comparisons. This output is compatible with assignmentComparisons input for various RORUTADIS-... services. Possible to visualize with HasseDiagram service.

The returned value is a XMCDA document whose main tag is ``<alternativesComparisons>``.
It has the following form::

   
				
					<alternativesComparisons mcdaConcept="atLeastAsGoodAs">
						<pairs>
							<pair>
								<initial>
									<alternativeID>[...]</alternativeID>
								</initial>
								<terminal>
									<alternativeID>[...]</alternativeID>
								</terminal>
								<value>
									<real>0</real>
								</value>
							</pair>
							...
						</pairs>
					<alternativesComparisons/>
				
			


------------------------


.. _RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT-messages:

messages
~~~~~~~~

Messages generated by the program.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT/description-wsDD.xml>`
