.. _RORUTA-NecessaryAndPossiblePreferenceRelations-PUT:

RORUTA-NecessaryAndPossiblePreferenceRelations
==============================================

:Version: 1.0
:Provider: PUT
:SOAP service's name: ``RORUTA-NecessaryAndPossiblePreferenceRelations-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Finds all possible and necessary weak preference relations on the set of alternatives.

**Contact:** Pawel Rychly (pawelrychly@gmail.com).


Inputs
------
(For outputs, see :ref:`below <RORUTA-NecessaryAndPossiblePreferenceRelations-PUT_outputs>`)


- :ref:`criteria <RORUTA-NecessaryAndPossiblePreferenceRelations-PUT-criteria>`
- :ref:`alternatives <RORUTA-NecessaryAndPossiblePreferenceRelations-PUT-alternatives>`
- :ref:`performances <RORUTA-NecessaryAndPossiblePreferenceRelations-PUT-performances>`
- :ref:`characteristic-points <RORUTA-NecessaryAndPossiblePreferenceRelations-PUT-characteristic-points>` *(optional)*
- :ref:`criteria-preference-directions <RORUTA-NecessaryAndPossiblePreferenceRelations-PUT-criteria-preference-directions>` *(optional)*
- :ref:`preferences <RORUTA-NecessaryAndPossiblePreferenceRelations-PUT-preferences>` *(optional)*
- :ref:`intensities-of-preferences <RORUTA-NecessaryAndPossiblePreferenceRelations-PUT-intensities-of-preferences>` *(optional)*
- :ref:`rank-related-requirements <RORUTA-NecessaryAndPossiblePreferenceRelations-PUT-rank-related-requirements>` *(optional)*
- :ref:`parameters <RORUTA-NecessaryAndPossiblePreferenceRelations-PUT-parameters>`


.. _RORUTA-NecessaryAndPossiblePreferenceRelations-PUT-criteria:

criteria
~~~~~~~~

A list of all considered criteria. The input value should be a valid XMCDA document whose main tag is criteria.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
                
                    <criteria>
                        <criterion id="%1" name="%1"></criterion>
                        [...]
                    </criteria>
                    
            


------------------------


.. _RORUTA-NecessaryAndPossiblePreferenceRelations-PUT-alternatives:

alternatives
~~~~~~~~~~~~

The list of all considered alternatives. The input value should be a valid XMCDA document whose main tag is alternatives. Each alternative may be described using two attributes: id and name. While the first one denotes a machine readable name, the second represents a human readable name.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
                
                    <alternatives>
                        <alternative id="%1" name="%2" />
                        [...]
                    </alternatives>
                    
            


------------------------


.. _RORUTA-NecessaryAndPossiblePreferenceRelations-PUT-performances:

performances
~~~~~~~~~~~~

Description of evaluation of alternatives on different criteria. It is required to provide the IDs of both criteria and alternatives described previously. The input value should be provided as a valid XMCDA document whose main tag is performanceTable

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   
                
                    <performanceTable>
                        <alternativePerformances>
                            <alternativeID>%1</alternativeID>
                            <performance>
                                <criterionID>%2</criterionID>
                                <value>
                                    <real>%3</real>
                                </value>
                            </performance>
                            [...]
                        </alternativePerformances>
                        [...]
                    </performanceTable>
                    
            


------------------------


.. _RORUTA-NecessaryAndPossiblePreferenceRelations-PUT-characteristic-points:

characteristic-points *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A set of values associated with the criteria. This input allows to determine what type of value function should be used for the particular criterion. For each criterion that has an associated greater than one value, a piecewise linear value function is used. In this case, the mentioned value denotes a number of characteristic points of this value function. For the criteria that are not listed in this file, or for these for which the provided values are lower than two uses a general value function. The input value should be provided as a valid XMCDA document whose main tag is criteriaValues. Each element should contain both an id of the criterion, and value tag.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.
It must have the following form::

   
                
                     <criteriaValues>
                        <criterionValue>
                            <criterionID>%1</criterionID>
                            <value>
                                <integer>%2</integer>
                            </value>
                        </criterionValue>
                        [...]
                    </criteriaValues>
                    
            


------------------------


.. _RORUTA-NecessaryAndPossiblePreferenceRelations-PUT-criteria-preference-directions:

criteria-preference-directions *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A set of values associated with criteria that determine their preference direction (0 - gain, 1 - cost).

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.
It must have the following form::

   
                
                     <criteriaValues mcdaConcept="preferenceDirection">
                        <criterionValue>
                            <criterionID>%1</criterionID>
                            <value>
                                <integer>%2</integer>
                            </value>
                        </criterionValue>
                        [...]
                    </criteriaValues>
                    
            


------------------------


.. _RORUTA-NecessaryAndPossiblePreferenceRelations-PUT-preferences:

preferences *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~

Set of pairwise comparisons of reference alternatives. For a pair of alternatives three types of comparisons are supported. These are the strict preference, weak preference, and indifference. The input value should be provided as a valid XMCDA document whose main tag is alternativesComparisons. For each type of comparison, a separate alternativesComparisons tag should be used. Within these groups a mentioned types are denoted using a comparisonType tag by respectively strict, weak, and indif label. Comparisons should be provided as pairs of alternatives ids.

The input value should be a valid XMCDA document whose main tag is ``<alternativesComparisons>``.
It must have the following form::

   
                
                    <alternativesComparisons>
                        <comparisonType>
                            %1<!-- type of preference: strong, weak, or indif -->
                        </comparisonType>
                        <pairs>
                            <pair>
                                <initial>
                                    <alternativeID>%2</alternativeID>
                                </initial>
                                <terminal>
                                    <alternativeID>%3</alternativeID>
                                </terminal>
                            </pair>
                            [...]
                        </pairs>
                    </alternativesComparisons>
                    [...]
                    
            


------------------------


.. _RORUTA-NecessaryAndPossiblePreferenceRelations-PUT-intensities-of-preferences:

intensities-of-preferences *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Set of comparisons of intensities of preference. For a pair of preference relations three types of comparisons are supported. These are the strict preference, weak, preference, and indifference. The input value should be provided as a valid XMCDA document whose main tag is alternativesComparisons. For each type of comparison, a separate alternativesComparisons tag should be used. Within these groups  aforementioned types are denoted using a comparisonType tag by respectively strict, weak, and indif label. Comparisons should be provided as pairs of two elementary sets of alternatives ids. The following form is expected:

The input value should be a valid XMCDA document whose main tag is ``<alternativesComparisons>``.
It must have the following form::

   
                
                    <alternativesComparisons>
                        <comparisonType>%1</comparisonType>
                        <pairs>
                            <pair>
                                <initial>
                                    <alternativesSet>
                                        <element>
                                            <alternativeID>%2</alternativeID>
                                        </element>
                                        <element>
                                            <alternativeID>%3</alternativeID>
                                        </element>
                                    </alternativesSet>
                                </initial>
                                <terminal>
                                    <alternativesSet>
                                        <element>
                                            <alternativeID>%4</alternativeID>
                                        </element>
                                        <element>
                                            <alternativeID>%5</alternativeID>
                                        </element>
                                    </alternativesSet>
                                </terminal>
                            </pair>
                            [...]
                        </pairs>
                    </alternativesComparisons>
                    [...]
                    
            


------------------------


.. _RORUTA-NecessaryAndPossiblePreferenceRelations-PUT-rank-related-requirements:

rank-related-requirements *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Set of rank-related requirements. In other words it is a set of  ranges of possible positions in the final ranking for a chosen alternatives. The input value should be provided as a valid XMCDA document whose main tag is alternativesValues. Each requirement should contain both an id of the reffered alternative and a pair of values that denote the desired range. These information should be provided within a separate alternativesValue tag.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.
It must have the following form::

   
                
                    <alternativesValues>
                        <alternativeValue>
                            <alternativeID>%1</alternativeID>
                            <value>
                                <interval>
                                    <lowerBound><integer>%2</integer></lowerBound>
                                    <upperBound><integer>%3</integer></upperBound>
                                </interval>
                            </value>
                        </alternativeValue>
                        [...]
                    </alternativesValues>
                    
            


------------------------


.. _RORUTA-NecessaryAndPossiblePreferenceRelations-PUT-parameters:

parameters
~~~~~~~~~~

Single boolean value. Determines whether to use sctrictly increasing (true) or monotonously increasing (false) value functions

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                
                     <methodParameters>
						<parameter name="strict">
                            <value>
                                <boolean>%1</boolean>
                            </value>
                        </parameter>
					</methodParameters>
                    
            

where:

- **%1** is a parameter named "Use strictly increasing value functions?". This is a boolean.
  The default value is false.


------------------------



.. _RORUTA-NecessaryAndPossiblePreferenceRelations-PUT_outputs:

Outputs
-------


- :ref:`necessary-relations <RORUTA-NecessaryAndPossiblePreferenceRelations-PUT-necessary-relations>`
- :ref:`possible-relations <RORUTA-NecessaryAndPossiblePreferenceRelations-PUT-possible-relations>`
- :ref:`messages <RORUTA-NecessaryAndPossiblePreferenceRelations-PUT-messages>`


.. _RORUTA-NecessaryAndPossiblePreferenceRelations-PUT-necessary-relations:

necessary-relations
~~~~~~~~~~~~~~~~~~~

A list of all necessary weak preference relations on the set of alternatives. The returned value is an XMCDA document whose main tag is alternativesComparisons. Each relation is denoted as a pair of alternativesID

The returned value is a XMCDA document whose main tag is ``<alternativesComparisons>``.
It has the following form::

   
                
                    <alternativesComparisons mcdaConcept="necessaryRelations">
                        <pairs>
                          <pair>
                            <initial>
                              <alternativeID>a01</alternativeID>
                            </initial>
                            <terminal>
                              <alternativeID>a01</alternativeID>
                            </terminal>
                          </pair>
                          [...]
                      </pairs>
                    </alternativesComparisons>
                    
            


------------------------


.. _RORUTA-NecessaryAndPossiblePreferenceRelations-PUT-possible-relations:

possible-relations
~~~~~~~~~~~~~~~~~~

A list of all possible weak preference relations on the set of alternatives. The returned value is an XMCDA document whose main tag is alternativesComparisons. Each relation is denoted as a pair of alternativesID

The returned value is a XMCDA document whose main tag is ``<alternativesComparisons>``.
It has the following form::

   
                
                    <alternativesComparisons mcdaConcept="possibleRelations">
                        <pairs>
                          <pair>
                            <initial>
                              <alternativeID>a01</alternativeID>
                            </initial>
                            <terminal>
                              <alternativeID>a01</alternativeID>
                            </terminal>
                          </pair>
                          [...]
                      </pairs>
                    </alternativesComparisons>
                    
            


------------------------


.. _RORUTA-NecessaryAndPossiblePreferenceRelations-PUT-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/RORUTA-NecessaryAndPossiblePreferenceRelations-PUT/description-wsDD.xml>`
