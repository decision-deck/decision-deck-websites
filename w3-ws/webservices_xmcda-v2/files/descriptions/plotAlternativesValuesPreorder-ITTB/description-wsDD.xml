<?xml version='1.0' encoding='utf-8'?>
<program_description>
    <program provider="ITTB" name="plotAlternativesValuesPreorder" version="1.1" displayName="plotAlternativesValuesPreorder" />
    <documentation>
        <description>This web service generates a graph representing a preorder on the alternatives, according to numerical values taken by the alternatives (the "best" alternative has the highest value). Compared to the web service plotAlternativesValuesPreorder, some parameters are added. Colors can be used and the title of the graph can be typed. You can choose between an increasing or a decreasing order for the graph. It is also possible to show the name of the alternatives instead of the id, etc. The alternatives' evaluations are supposed to be real or integer numeric values.</description>
        <contact><![CDATA[Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)]]></contact>
    </documentation>
    <parameters>

        <input id="alternatives" name="alternatives" displayName="alternatives" isoptional="0">
            <documentation>
                <description>A list of alternatives. Alternatives can be activated or desactivated via the &lt;active&gt; tag (true or false). By default (no &lt;active&gt; tag), alternatives are considered as active.</description>
            </documentation>
            <xmcda tag="alternatives"><![CDATA[
                   
                    <alternatives>
                        <alternative>
                            <active>[...]</active>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
                    
               ]]></xmcda>
        </input>

        <input id="alternativesValues" name="alternativesValues" displayName="alternativesValues" isoptional="0">
            <documentation>
                <description>A list of &lt;alternativesValue&gt; representing a certain numeric quantity for each alternative, like, e.g., an overall value.</description>
            </documentation>
            <xmcda tag="alternativesValues" />
        </input>

        <input id="options" name="options" displayName="options" isoptional="0">
            <documentation>
                <description>Options of the plotted graph.</description>
            </documentation>
            <xmcda tag="methodParameters"><![CDATA[
                   
					<methodParameters>
						  <parameter id="plot_title" name="Plot Title">
							<value>
            					<label>%1</label>
       						 </value>
						</parameter>
						<parameter id="plot_order" name="Plot order">
							<value>
            					<label>%2</label>
       						 </value>
						</parameter>
						<parameter id="node_shape" name="Node shape">
							<value>
            					<label>%3</label>
       						 </value>
						</parameter>
						 <parameter id="show_names" name="Show alternatives names">
							<value>
            					<label>%4</label>
       						 </value>
						</parameter>
						 <parameter id="use_color" name="Colors in the graph">
							<value>
            					<label>%5</label>
       						 </value>
						</parameter>
						 <parameter id="selected_color" name="Selected color">
							<value>
            					<label>%6</label>
       						 </value>
						</parameter >
					</methodParameters>
				
               ]]></xmcda>
            <gui status="preferGUI">
                <entry id="%1" type="string" displayName="Chart title">
                    <documentation>
                        <description>Title of the graph.</description>
                    </documentation>
                </entry>
                <entry id="%2" type="enum" displayName="Order">
                    <documentation>
                        <description>Increasing or decreasing order w.r.t. the input values.</description>
                    </documentation>
                    <items>
                        <item id="increasing">
                            <description>Increasing</description>
                            <value>increasing</value>
                        </item>
                        <item id="decreasing">
                            <description>Decreasing</description>
                            <value>decreasing</value>
                        </item>
                    </items>
                    <defaultValue>decreasing</defaultValue>
                </entry>
                <entry id="%3" type="enum" displayName="Plot symbols">
                    <documentation>
                        <description>Symbol used in the plot.</description>
                    </documentation>
                    <items>
                        <item id="rectangle">
                            <description>Rectangle</description>
                            <value>Rectangle</value>
                        </item>
                        <item id="square">
                            <description>Square</description>
                            <value>Square</value>
                        </item>
                        <item id="ellipse">
                            <description>Ellipse</description>
                            <value>Ellipse</value>
                        </item>
                        <item id="circle">
                            <description>Circle</description>
                            <value>Circle</value>
                        </item>
                        <item id="diamond">
                            <description>Diamond</description>
                            <value>Diamond</value>
                        </item>
                    </items>
                    <defaultValue>rectangle</defaultValue>
                </entry>
                <entry id="%4" type="enum" displayName="Display">
                    <documentation>
                        <description>Display alternatives' names or IDs.</description>
                    </documentation>
                    <items>
                        <item id="true">
                            <description>Name</description>
                            <value>true</value>
                        </item>
                        <item id="false">
                            <description>ID</description>
                            <value>false</value>
                        </item>
                    </items>
                    <defaultValue>false</defaultValue>
                </entry>
                <entry id="%5" type="enum" displayName="Colored">
                    <documentation>
                        <description>Colored or bw graph.</description>
                    </documentation>
                    <items>
                        <item id="true">
                            <description>Yes</description>
                            <value>true</value>
                        </item>
                        <item id="false">
                            <description>No</description>
                            <value>false</value>
                        </item>
                    </items>
                    <defaultValue>false</defaultValue>
                </entry>
                <entry id="%6" type="enum" displayName="Color">
                    <documentation>
                        <description>Color of the graph. Choose between "Black", "Red", "Blue", "Green", "Yellow", "Magenta" and "Cyan".</description>
                    </documentation>
                    <items>
                        <item id="Black">
                            <description>Black</description>
                            <value>Black</value>
                        </item>
                        <item id="Red">
                            <description>Red</description>
                            <value>Red</value>
                        </item>
                        <item id="Blue">
                            <description>Blue</description>
                            <value>Blue</value>
                        </item>
                        <item id="Green">
                            <description>Green</description>
                            <value>Green</value>
                        </item>
                        <item id="Yellow">
                            <description>Yellow</description>
                            <value>Yellow</value>
                        </item>
                        <item id="Magenta">
                            <description>Magenta</description>
                            <value>Magenta</value>
                        </item>
                        <item id="Cyan">
                            <description>Cyan</description>
                            <value>Cyan</value>
                        </item>
                    </items>
                    <defaultValue>Black</defaultValue>
                </entry>
            </gui>
        </input>

        <output id="alternativesValuesPlot" name="alternativesValuesPlot" displayName="alternativesValuesPlot">
            <documentation>
                <description>A string containing the base64 representation of the png image of the generated graph.</description>
            </documentation>
            <xmcda tag="alternativeValue" />
        </output>

        <output id="messages" name="messages" displayName="messages">
            <documentation>
                <description>A list of messages generated by the algorithm.</description>
            </documentation>
            <xmcda tag="methodMessages" />
        </output>

    </parameters>
</program_description>
