<?xml version='1.0' encoding='utf-8'?>
<program_description>
    <program provider="ITTB" name="plotCriteriaValuesPreorder" version="1.1" displayName="plotCriteriaValuesPreorder" />
    <documentation>
        <description>This web service generates a graph representing a preorder on the criteria, according to numerical values taken by the criteria (the "best" criteria has the highest value). Compared to the web service plotCriteriaValuesPreorder, some parameters are added. Colors can be used and the title of the graph can be typed. You can choose between an increasing or a decreasing order for the graph. It is also possible to show the name of the criteria instead of the id, etc. The criteria evaluations are supposed to be real or integer numeric values.</description>
        <contact><![CDATA[Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)]]></contact>
    </documentation>
    <parameters>

        <input id="criteria" name="criteria" displayName="criteria" isoptional="0">
            <documentation>
                <description>A list of criteria. Criteria can be activated or desactivated via the &lt;active&gt; tag (true or false). By default (no &lt;active&gt; tag), criteria are considered as active.</description>
            </documentation>
            <xmcda tag="criteria"><![CDATA[
                   
                    <criteria>
                        <criterion>
                            <active>[...]</active>
                            [...]
                        </criterion>
                        [...]
                    </criteria>
                    
               ]]></xmcda>
        </input>

        <input id="criteriaValues" name="criteriaValues" displayName="criteriaValues" isoptional="0">
            <documentation>
                <description>A list of &lt;criterionValue&gt; representing a certain numeric quantity for each criterion, like, e.g., an importance value.</description>
            </documentation>
            <xmcda tag="criteriaValues"><![CDATA[

               ]]></xmcda>
        </input>

        <input id="options" name="options" displayName="options" isoptional="0">
            <documentation>
                <description>Generates a graph taking into account the proposed options.</description>
            </documentation>
            <xmcda tag="methodParameters"><![CDATA[
                   
					<methodParameters>
						  <parameter id="plot_title" name="Plot Title">
							<value>
            					<label>%1</label>
       						 </value>
						</parameter>
						<parameter id="plot_order" name="Plot order">
							<value>
            					<label>%2</label>
       						 </value>
						</parameter>
						<parameter id="node_shape" name="Node shape">
							<value>
            					<label>%3</label>
       						 </value>
						</parameter>
						 <parameter id="show_names" name="Show criteria names">
							<value>
            					<label>%4</label>
       						 </value>
						</parameter>
						 <parameter id="use_color" name="Colors in the graph">
							<value>
            					<label>%5</label>
       						 </value>
						</parameter>
						 <parameter id="selected_color" name="Selected color">
							<value>
            					<label>%6</label>
       						 </value>
						</parameter >
					</methodParameters>
				
               ]]></xmcda>
            <gui status="preferGUI">
                <entry id="%1" type="string" displayName="Chart title:">
                    <documentation>
                        <description>String for the title of the graph. The default value is an empty field.</description>
                    </documentation>
                </entry>
                <entry id="%2" type="enum" displayName="Order:">
                    <documentation>
                        <description>Increasing or decreasing. The default value is decreasing.</description>
                    </documentation>
                    <items>
                        <item id="increasing">
                            <description>Ascending</description>
                            <value>increasing</value>
                        </item>
                        <item id="decreasing">
                            <description>Descending</description>
                            <value>decreasing</value>
                        </item>
                    </items>
                    <defaultValue>decreasing</defaultValue>
                </entry>
                <entry id="%3" type="enum" displayName="Shape of the nodes?">
                    <documentation>
                        <description>Choose between rectangle, square, ellipse, circle or diamond.</description>
                    </documentation>
                    <items>
                        <item id="rectangle">
                            <description>Rectangle</description>
                            <value>Rectangle</value>
                        </item>
                        <item id="square">
                            <description>Square</description>
                            <value>Square</value>
                        </item>
                        <item id="ellipse">
                            <description>Ellipse</description>
                            <value>Ellipse</value>
                        </item>
                        <item id="circle">
                            <description>Circle</description>
                            <value>Circle</value>
                        </item>
                        <item id="diamond">
                            <description>Diamond</description>
                            <value>Diamond</value>
                        </item>
                    </items>
                    <defaultValue>rectangle</defaultValue>
                </entry>
                <entry id="%4" type="enum" displayName="Criterion name or id?">
                    <documentation>
                        <description>Display criteria names ot IDs.</description>
                    </documentation>
                    <items>
                        <item id="true">
                            <description>name</description>
                            <value>true</value>
                        </item>
                        <item id="false">
                            <description>id</description>
                            <value>false</value>
                        </item>
                    </items>
                    <defaultValue>false</defaultValue>
                </entry>
                <entry id="%5" type="enum" displayName="Use colors?">
                    <documentation>
                        <description>The use of colors: true for a colored graph.</description>
                    </documentation>
                    <items>
                        <item id="true">
                            <description>Yes</description>
                            <value>true</value>
                        </item>
                        <item id="false">
                            <description>No</description>
                            <value>false</value>
                        </item>
                    </items>
                    <defaultValue>false</defaultValue>
                </entry>
                <entry id="%6" type="enum" displayName="Choose color:">
                    <documentation>
                        <description>String that indicates the color of the graph.Choose between "Black", "Red", "Blue", "Green", "Yellow", "Magenta" and "Cyan".</description>
                    </documentation>
                    <items>
                        <item id="black">
                            <description>Black</description>
                            <value>Black</value>
                        </item>
                        <item id="red">
                            <description>Red</description>
                            <value>Red</value>
                        </item>
                        <item id="blue">
                            <description>Blue</description>
                            <value>Blue</value>
                        </item>
                        <item id="green">
                            <description>Green</description>
                            <value>Green</value>
                        </item>
                        <item id="yellow">
                            <description>Yellow</description>
                            <value>Yellow</value>
                        </item>
                        <item id="magenta">
                            <description>Magenta</description>
                            <value>Magenta</value>
                        </item>
                        <item id="cyan">
                            <description>Cyan</description>
                            <value>Cyan</value>
                        </item>
                    </items>
                    <defaultValue>black</defaultValue>
                </entry>
            </gui>
        </input>

        <output id="criteriaValuesPlot" name="criteriaValuesPlot" displayName="criteriaValuesPlot">
            <documentation>
                <description>A string containing the base64 representation of the png image of the generated graph.</description>
            </documentation>
            <xmcda tag="criterionValue" />
        </output>

        <output id="messages" name="messages" displayName="messages">
            <documentation>
                <description>A list of messages generated by the algorithm.</description>
            </documentation>
            <xmcda tag="methodMessages" />
        </output>

    </parameters>
</program_description>
