<?xml version='1.0' encoding='utf-8'?>
<program_description>
    <program provider="RXMCDA" name="additiveValueFunctionsIdentification" version="1.1" displayName="additiveValueFunctionsIdentification" />
    <documentation>
        <description>Identifies an set of piecewise linear additive value functions according to a ranking of the alternatives. If the number of segments is not given, a general additive value function is looked for.</description>
        <contact><![CDATA[Helene Schmitz and Patrick Meyer (patrick.meyer@telecom-bretagne.eu)]]></contact>
        <url />
    </documentation>
    <parameters>

        <input id="criteria" name="criteria" displayName="criteria" isoptional="0">
            <documentation>
                <description>A list of criteria. Criteria can be activated or desactivated via the &lt;active&gt; tag (true or false). By default (no &lt;active&gt; tag), criteria are considered as active.</description>
            </documentation>
            <xmcda tag="criteria"><![CDATA[
                
                    <criteria>
                        <criterion>
                            <active>[...]</active>
                            [...]
                        </criterion>
                        [...]
                    </criteria>
                    
            ]]></xmcda>
        </input>

        <input id="alternatives" name="alternatives" displayName="alternatives" isoptional="0">
            <documentation>
                <description>A list of alternatives. Alternatives can be activated or desactivated via the &lt;active&gt; tag (true or false). By default (no &lt;active&gt; tag), alternatives are considered as active.</description>
            </documentation>
            <xmcda tag="alternatives"><![CDATA[
                
                    <alternatives>
                        <alternative>
                            <active>[...]</active>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
                    
            ]]></xmcda>
        </input>

        <input id="performanceTable" name="performanceTable" displayName="performanceTable" isoptional="0">
            <documentation>
                <description>A performance table. The evaluations should be numeric values, i.e. &lt;real&gt;, &lt;integer&gt; or &lt;rational&gt;.</description>
            </documentation>
            <xmcda tag="performanceTable" />
        </input>

        <input id="alternativesRanks" name="alternativesRanks" displayName="alternativesRanks" isoptional="0">
            <documentation>
                <description>The ranking of the alternatives, the best alternative having the lowest rank.</description>
            </documentation>
            <xmcda tag="alternativesValues" />
        </input>

        <input id="separationThreshold" name="separationThreshold" displayName="separationThreshold" isoptional="0">
            <documentation>
                <description>Threshold value indicating the minimal difference in terms of the overall value between two neighbor alternatives in the given ranking.</description>
            </documentation>
            <xmcda tag="methodParameters"><![CDATA[
                
                    <methodParameters>
                        <parameter
                             name="separationThreshold"> <!-- REQUIRED  -->
                            <value>
                                <real>%1</real>
                            </value>
                        </parameter>
                    </methodParameters>
                    
            ]]></xmcda>
            <gui status="preferGUI">
                <entry id="%1" type="float" displayName="separationThreshold">
                    <documentation>
                        <description>Threshold value indicating the minimal difference in terms of the overall value between two neighbor alternatives in the given ranking.</description>
                    </documentation>
                    <constraint>
                        <description>The value should be a strictly positive float, less than the highest possible overall value.</description>
                        <code><![CDATA[ %1 > 0 ]]></code>
                    </constraint>
                </entry>
            </gui>
        </input>

        <input id="segments" name="segments" displayName="segments" isoptional="1">
            <documentation>
                <description>The number of segments for the additive value functions. If it is not given, then a general additive value function is searched for.</description>
            </documentation>
            <xmcda tag="methodParameters"><![CDATA[
                
                    <methodParameters>
                        <parameter
                             name="criteriaSegments"> <!-- REQUIRED  -->
                            <value>
                                <integer>%1</integer>
                            </value>
                        </parameter>
                    </methodParameters>
                    
            ]]></xmcda>
            <gui status="preferGUI">
                <entry id="%1" type="int" displayName="numberOfSegments">
                    <documentation>
                        <description>The number of segments for the additive value functions.</description>
                    </documentation>
                    <constraint>
                        <description>The value should be a strictly positive integer.</description>
                        <code><![CDATA[ %1 > 0 ]]></code>
                    </constraint>
                    <defaultValue>1</defaultValue>
                </entry>
            </gui>
        </input>

        <output id="valueFunctions" name="valueFunctions" displayName="valueFunctions">
            <documentation>
                <description>The value functions of the selected criteria.</description>
            </documentation>
            <xmcda tag="criteria"><![CDATA[
                
					<criteria mcdaConcept="valueFunctions">
						<criterion>
							<criterionID>[...]</criterionID>
							<criterionFunction>
								<points>
									<point>
										<abscissa><real>[...]</real></abscissa>
										<ordinate><real>[...]</real></ordinate>
									</point>
								</points>
							</criterionFunction>
						</criterion>
					</criteria>
				
            ]]></xmcda>
        </output>

        <output id="messages" name="messages" displayName="messages">
            <documentation>
                <description>A list of messages generated by the algorithm.</description>
            </documentation>
            <xmcda tag="methodMessages" />
        </output>

    </parameters>
</program_description>
