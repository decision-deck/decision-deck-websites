<?xml version='1.0' encoding='utf-8'?>
<program_description>
	<program provider="URV" name="OWAWeightsBalance" version="1.0" displayName="OWAWeightsBalance" />
	<documentation>
		<description>Balance of the weights given to the Ordered Weighted Average operator (OWA)

 The Ordered Weighted Averaging operators, commonly called OWA operators, provide a parameterized class of mean type aggregation operators.
 OWA was defined by R.R. Yager in 1988.
 The main characteristic of this aggregation operator is the fact that the weights are associated to the values instead to criteria.
 In this way, we can define different aggregation policies according to the importance that we associate to high and low performance scores.
 There exist different measures to characterise a set of weights associated to an OWA operator.
 In this module we implement the BALANCE measure.</description>
		<contact><![CDATA[Aida Valls <aida.valls@urv.cat>]]></contact>
		<reference>
		R.R. Yager, On Ordered Weighted Averaging Aggregation, IEEE Transactions on Systems, Man and Cybernetics, 18, pp. 119-145 (1988)
		</reference>
	</documentation>
	<parameters>

		<input id="input1" name="criteria" displayName="criteria" isoptional="0">
			<documentation>
				<description>A list of criteria. Criteria are always active.</description>
			</documentation>
			<xmcda tag="criteria"><![CDATA[
                            
                    <criteria>
                        <criterion>
                            <active>[...]</active>
                            [...]
                        </criterion>
                        [...]
                    </criteria>
                    
                     ]]></xmcda>
		</input>

		<input id="input2" name="weights" displayName="weights" isoptional="0">
			<documentation>
				<description>The weights are associated to the values of the alteranatives. Assuming that the values
on the alternative will be sorted from the best to the worst (e.g. 7, 5, 5, 3, 1), the list of weights must be ordered
according to the importance that is given to the values, from the highest to the lowest.
For example a list of weights as (0.5, 0.5, 0, 0, 0) is ignoring the 3 lowest values, and making an average of the
two highest ones. A list like (0, 0, 1, 0 ,0 ) is calculating the median, while (0, 0, 0, 0, 1) is taking the minimum.
Notice that the sum of weights is required to be 1.</description>
			</documentation>
			<xmcda tag="alternativesValues"><![CDATA[
		  
			<alternativesValues>
			  <alternativeValue>
				<values>
					<value>
						<real>[...]</real>
					</value>
					<value>
						<integer>[...]</integer>
					</value>
					[...]
			  </alternativeValue>
			</alternativesValues>
		  
		  ]]></xmcda>
		</input>

		<output id="output0" name="owaMeasures" displayName="owaMeasures">
			<documentation>
				<description>Result obtained from the analysis of the OWA weights.</description>
			</documentation>
			<xmcda tag="criterionValue" />
		</output>

		<output id="output1" name="messages" displayName="messages">
			<documentation>
				<description>A status message.</description>
			</documentation>
			<xmcda tag="methodMessages" />
		</output>

	</parameters>
</program_description>
