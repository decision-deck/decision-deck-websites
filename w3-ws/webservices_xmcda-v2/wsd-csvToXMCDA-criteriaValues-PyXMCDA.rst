.. _csvToXMCDA-criteriaValues-PyXMCDA:

csvToXMCDA-criteriaValues
=========================

:Version: 1.2
:Provider: PyXMCDA
:SOAP service's name: ``csvToXMCDA-criteriaValues-PyXMCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Transforms a file containing criteria values from a comma-separated values (CSV) file to two XMCDA compliant files, containing the corresponding criteria ids and their criteriaValues.

**Contact:** Sébastien Bigaret (sebastien.bigaret@telecom-bretagne.eu)

**Web page:** http://github.com/sbigaret/ws-PyXMCDA


Inputs
------
(For outputs, see :ref:`below <csvToXMCDA-criteriaValues-PyXMCDA_outputs>`)


- :ref:`criteriaValues.csv <csvToXMCDA-criteriaValues-PyXMCDA-csv>`


.. _csvToXMCDA-criteriaValues-PyXMCDA-csv:

criteriaValues.csv
~~~~~~~~~~~~~~~~~~

The criteria and their criteria values as a CSV file.

Example:

  ,cost,risk,employment,connection
  weights,1,2,3,4

The input value should be a valid XMCDA document whose main tag is ``<other>``.


------------------------



.. _csvToXMCDA-criteriaValues-PyXMCDA_outputs:

Outputs
-------


- :ref:`criteria <csvToXMCDA-criteriaValues-PyXMCDA-criteria>`
- :ref:`criteriaValues <csvToXMCDA-criteriaValues-PyXMCDA-criteriaValues>`
- :ref:`messages <csvToXMCDA-criteriaValues-PyXMCDA-messages>`


.. _csvToXMCDA-criteriaValues-PyXMCDA-criteria:

criteria
~~~~~~~~

The equivalent criteria ids.

The returned value is a XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _csvToXMCDA-criteriaValues-PyXMCDA-criteriaValues:

criteriaValues
~~~~~~~~~~~~~~

The equivalent criteria values.

The returned value is a XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _csvToXMCDA-criteriaValues-PyXMCDA-messages:

messages
~~~~~~~~

Status messages.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/csvToXMCDA-criteriaValues-PyXMCDA/description-wsDD.xml>`
