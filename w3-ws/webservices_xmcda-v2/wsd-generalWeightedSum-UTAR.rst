.. _generalWeightedSum-UTAR:

generalWeightedSum
==================

:Provider: UTAR
:Version: 1.0
:Name: generalWeightedSum
:Display name: generalWeightedSum

Description
-----------

 -**Contact:** Boris Leistedt (boris.leistedt@gmail.com)

  Computes the (possibly normalised) weighted sum of alternatives' evaluations, their average value or simply their sum.

Inputs
------

alternatives
~~~~~~~~~~~~

 -**ID:** alternatives

 -**Name:** alternatives

 -**Display name:** alternatives

 -**Optional:** False


Description: 
............

  
					A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active. 
				

XMCDA related:
..............

 -**Tag:** alternatives

 -**Code:**

  ::

   
				
					<alternatives>
						<alternative>
							<alternativeID> %1 </alternativeID>
						</alternative>
					</alternatives>
				
			

------------------------

norm
~~~~

 -**ID:** norm

 -**Name:** norm

 -**Display name:** normalized weights



Description: 
............

  
					Boolean for normalisation of the weights (true if the weights should be normalised).
				

XMCDA related:
..............

 -**Tag:** methodParameters

 -**Code:**

  ::

   
				
					<methodParameters>
						<parameter>
							<value>
            							<label> %1</label>
       						 	</value>
						</parameter>
					</methodParameters>
				
			

GUI and placeholder information:
................................

 -**GUI status:** preferGUI

 -**%1**

  -**Type:** boolean

  -**Display name:** norm


------------------------

avg
~~~

 -**ID:** avg

 -**Name:** avg

 -**Display name:** average operator



Description: 
............

  
					Boolean for the average operator. (true if you want to evaluate the average value of an alternative, for all criteria).
				

XMCDA related:
..............

 -**Tag:** methodParameters

 -**Code:**

  ::

   
				
					<methodParameters>
						<parameter>
							<value>
            					<label> %1</label>
       						 </value>
						</parameter>
					</methodParameters>
				
			

GUI and placeholder information:
................................

 -**GUI status:** preferGUI

 -**%1**

  -**Type:** boolean

  -**Display name:** avg


------------------------

criteria
~~~~~~~~

 -**ID:** criteria

 -**Name:** criteria

 -**Display name:** criteria

 -**Optional:** False


Description: 
............

  
					A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.
				

XMCDA related:
..............

 -**Tag:** criteria

 -**Code:**

  ::

   
				
					<criteria>
						<criterion>
							<criterionID> %1 </criterionID>
						</criterion>
					</criteria>
				
			

------------------------

performanceTable
~~~~~~~~~~~~~~~~

 -**ID:** performanceTable

 -**Name:** performanceTable

 -**Display name:** performanceTable

 -**Optional:** False


Description: 
............

  
					A performance table. The evaluations should be numeric values, i.e. <real>, <integer> or <rational>. It must contains IDs of both criteria and alternatives described previously.
				

XMCDA related:
..............

 -**Tag:** performanceTable

 -**Code:**

  ::

   
				
					<performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>[...]</criterionID>
								<value>
            								<real>[...]</real>
       						 		</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>
				
			

------------------------

weights
~~~~~~~

 -**ID:** criteriaWeights

 -**Name:** weights

 -**Display name:** weights

 -**Optional:** True


Description: 
............

  
					Containing the optional weights for criteria sum.
				

XMCDA related:
..............

 -**Tag:** criteriaValues

 -**Code:**

  ::

   
				
					<criteriaValues>
						<criterionValue>
							<criterionID>[...]</criterionID>
							<value>
								<real>[...]</real>
							</value>
						</criterionValue>
						[...]
					</criteriaValues>
				
			

------------------------



Outputs
-------

alternativesValues
~~~~~~~~~~~~~~~~~~

 -**ID:** alternativesValues

 -**Name:** alternativesValues

 -**Display name:** alternativesValues



Description: 
............

  
					Values (or utility) for different alternatives
				

XMCDA related:
..............

 -**Tag:** alternativesValues

 -**Code:**

  ::

   
				
					<alternativesValues> 
				    	<alternativeValue>
	                       	<alternativeID>[...]</alternativeID>
                        	<value>
	                          	<real>[...]</real>
                        	</value>
                    	</alternativeValue>
                    </alternativesValues>
                
			

------------------------

message
~~~~~~~

 -**ID:** messages

 -**Name:** message

 -**Display name:** messages



Description: 
............

  
					logMessage
				

XMCDA related:
..............

 -**Tag:** methodMessages

 -**Code:**

  ::

   
				
					<methodMessages mcdaConcept="methodMessage">
						<logMessage>
							<text> [...]</text>
						</logMessage>
						<errorMessage>
							<text> [...]</text>
						</errorMessage>
					</methodMessages>
				
			


