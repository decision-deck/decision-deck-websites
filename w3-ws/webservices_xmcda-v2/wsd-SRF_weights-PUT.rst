.. _SRF_weights-PUT:

SRFWeights
==========

:Version: 1.0.0
:Provider: PUT
:Name: SRF_weights
:SOAP service's name: ``SRF_weights-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

SRFWeights - computes weights of criteria using the revised Simos procedure

**Web page:** https://github.com/Yamadads/PrometheeDiviz


Inputs
------
(For outputs, see :ref:`below <SRF_weights-PUT_outputs>`)


- :ref:`criteria_ranking <SRF_weights-PUT-input1>`
- :ref:`method_parameters <SRF_weights-PUT-input2>`


.. _SRF_weights-PUT-input1:

criteria_ranking
~~~~~~~~~~~~~~~~

Criteria Values listing full ranking of criteria and containing information about blank cards.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.
It must have the following form::

   
        
        <criteriaValues mcdaConcept="Importance" name="ranking">
		<criterionValue>
			<criterionID>c01</criterionID>
			<value>
				<integer>1</integer>
			</value>
		</criterionValue>
		<criterionValue>
			<criterionID>c02</criterionID>
			<value>
				<integer>3</integer>
			</value>
		</criterionValue>
		<criterionValue>
			<criterionID>c03</criterionID>
			<value>
				<integer>4</integer>
			</value>
		</criterionValue>
		</criteriaValues>
        
      


------------------------


.. _SRF_weights-PUT-input2:

method_parameters
~~~~~~~~~~~~~~~~~

Simos? procedure needs some parameters from users.
          First parameter is telling how many times the last criterion is more important than the first one in the ranking
          Second parameter is telling how many decimal places can be used in weight value.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
        
        <methodParameters>
          <parameter name="criteria_weight_ratio">
            <value>
              <real>%1</real>
            </value>
          </parameter>
          <parameter name="decimal_places">
            <value>
              <label>%2</label>
            </value>
          </parameter>
        </methodParameters>
        
      

where:

- **%1** is a parameter named "criteria weight ratio". This is a float.
- **%2** is a parameter named "decimal places". It can have the following values:

  - ``0``: Integer value

  - ``1``: One decimal place

  - ``2``: Two decimal places

  The default value is item0.


------------------------



.. _SRF_weights-PUT_outputs:

Outputs
-------


- :ref:`weights <SRF_weights-PUT-output1>`
- :ref:`messages <SRF_weights-PUT-output2>`


.. _SRF_weights-PUT-output1:

weights
~~~~~~~

Weights for each criterion generated by Simos' procedure

The returned value is a XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _SRF_weights-PUT-output2:

messages
~~~~~~~~

Messages or errors generated by this module.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/SRF_weights-PUT/description-wsDD.xml>`
