.. _csvToXMCDA-valueFunctions-PyXMCDA:

csvToXMCDA-valueFunctions
=========================

:Version: 1.0
:Provider: PyXMCDA
:SOAP service's name: ``csvToXMCDA-valueFunctions-PyXMCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Transforms a file containing value functions from a comma-separated values (CSV) file to a XMCDA compliant file, containing the criteria ids with their names and criterion functions (points).

**Contact:** Sébastien Bigaret (sebastien.bigaret@telecom-bretagne.eu)

**Web page:** http://github.com/sbigaret/ws-PyXMCDA


Inputs
------
(For outputs, see :ref:`below <csvToXMCDA-valueFunctions-PyXMCDA_outputs>`)


- :ref:`valueFunctions.csv <csvToXMCDA-valueFunctions-PyXMCDA-valueFunctions_csv>`


.. _csvToXMCDA-valueFunctions-PyXMCDA-valueFunctions_csv:

valueFunctions.csv
~~~~~~~~~~~~~~~~~~

The value functions as a CSV file.

Example:

  g1,cost,0,0
  ,,21334,1
  g2,Acceleration,0,0
  ,,30.8,1
  g3,PickUp,0,0
  ,,41.6,1
  g4,Brakes,0,0
  ,,2.66,1
  g5,RoadHold,0,0
  ,,3.25,1

The input value should be a valid XMCDA document whose main tag is ``<other>``.


------------------------



.. _csvToXMCDA-valueFunctions-PyXMCDA_outputs:

Outputs
-------


- :ref:`valueFunctions <csvToXMCDA-valueFunctions-PyXMCDA-valueFunctions>`
- :ref:`messages <csvToXMCDA-valueFunctions-PyXMCDA-messages>`


.. _csvToXMCDA-valueFunctions-PyXMCDA-valueFunctions:

valueFunctions
~~~~~~~~~~~~~~

The equivalent value functions.

The returned value is a XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _csvToXMCDA-valueFunctions-PyXMCDA-messages:

messages
~~~~~~~~

Status messages.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/csvToXMCDA-valueFunctions-PyXMCDA/description-wsDD.xml>`
