.. _UTASTAR-UTAR:

UTASTAR
=======

:Version: 1.1
:Provider: UTAR
:SOAP service's name: ``UTASTAR-UTAR`` (see :ref:`soap-requests` for details)

Description
-----------

Computes UTASTAR method and if necessary uses post-optimality analysis among three well-known post-optimality methods

**Contact:** Boris Leistedt (boris.leistedt@gmail.com)


Inputs
------
(For outputs, see :ref:`below <UTASTAR-UTAR_outputs>`)


- :ref:`criteria <UTASTAR-UTAR-crit>`
- :ref:`alternatives <UTASTAR-UTAR-alternatives>`
- :ref:`performanceTable <UTASTAR-UTAR-perfTable>`
- :ref:`preferencesDirections <UTASTAR-UTAR-prefDir>`
- :ref:`segments <UTASTAR-UTAR-critSeg>`
- :ref:`alternativesRanking <UTASTAR-UTAR-altRank>` *(optional)*
- :ref:`alternativesPreferences <UTASTAR-UTAR-alternativesPreferences>` *(optional)*
- :ref:`alternativesIndifferences <UTASTAR-UTAR-alternativesIndifferences>` *(optional)*
- :ref:`method <UTASTAR-UTAR-method>` *(optional)*


.. _UTASTAR-UTAR-crit:

criteria
~~~~~~~~

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
				
					<criteria>
						<criterion>
							<criterionID>[...]</criterionID>
						</criterion>
					</criteria>
				
			


------------------------


.. _UTASTAR-UTAR-alternatives:

alternatives
~~~~~~~~~~~~

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
				
					<alternatives>
						<alternative>
							<alternativeID>[...]</alternativeID>
						</alternative>
					</alternatives>
				
			


------------------------


.. _UTASTAR-UTAR-perfTable:

performanceTable
~~~~~~~~~~~~~~~~

Values of criteria for different alternatives. It must contains IDs of both criteria and alternatives described previously.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   
				
					<performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>[...]</criterionID>
								<value>
            							<real>[...]</real>
       						 	</value>
							</performance>
						</alternativePerformances>
					</performanceTable>
				
			


------------------------


.. _UTASTAR-UTAR-prefDir:

preferencesDirections
~~~~~~~~~~~~~~~~~~~~~

Optimization direction for the selected criteria (min or max).

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.
It must have the following form::

   
				
					<criteriaValues>
						<criterionValue>
							<criterionID>[...]</criterionID>
							<value>
            							<label>[...]</label>
       						 	</value>
						</criterionValue>
					</criteriaValues>
				
			


------------------------


.. _UTASTAR-UTAR-critSeg:

segments
~~~~~~~~

Number of segments in each value function to be constructed by UTA.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.
It must have the following form::

   
				
					<criteriaValues>
						<criterionValue>
							<criterionID>[...]</criterionID>
							<value>
            							<integer>[...]</integer>
       						 	</value>
						</criterionValue>
					</criteriaValues>
				
			


------------------------


.. _UTASTAR-UTAR-altRank:

alternativesRanking *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ranking (preorder) of alternatives, corresponding to pariwize preference and indifference statements

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.
It must have the following form::

   
				
					<alternativesValues>
						<alternativeValue>
							<alternativeID>[...]</alternativeID>
							<value>
            							<integer>[...]</integer>
       						 	</value>
						</alternativeValue>
					</alternativesValues>
				
			


------------------------


.. _UTASTAR-UTAR-alternativesPreferences:

alternativesPreferences *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Asymmetric part of the preference relation, representing strict preference statements, under the form of paiwise comparisons of alternatives.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.
It must have the following form::

   
				
				<alternativesComparisons>
					<pairs>
						<pair>
							<initial>
								<alternativeID>[...]</alternativeID>
							</initial>
							<terminal>
								<alternativeID>[...]</alternativeID>
							</terminal>
						</pair>
						[...]
					</pairs>
				</alternativesComparisons>
				                			
			


------------------------


.. _UTASTAR-UTAR-alternativesIndifferences:

alternativesIndifferences *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Symmetric part of the preference relation, representing indifference statements, under the form of paiwise comparisons of alternatives.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.
It must have the following form::

   
				
				<alternativesComparisons>
					<pairs>
						<pair>
							<initial>
								<alternativeID>[...]</alternativeID>
							</initial>
							<terminal>
								<alternativeID>[...]</alternativeID>
							</terminal>
						</pair>
						[...]
					</pairs>
				</alternativesComparisons>
				                			
			


------------------------


.. _UTASTAR-UTAR-method:

method *(optional)*
~~~~~~~~~~~~~~~~~~~

Post-optimality method : if required, please choose between "ac" (analytic center), "mv" (mean value, geometric center), and "md" (utamp, delta maximization).

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
				
					<methodParameters>
						<parameter>
							<value>
            					<label>%1</label>
       						 </value>
						</parameter>
					</methodParameters>
				
			

where:

- **%1** is a parameter named "Post-optimality method". It can have the following values:

  - ``none``: None

  - ``ac``: Analytic center

  - ``mv``: Mean value

  - ``md``: Max delta

  The default value is none.


------------------------



.. _UTASTAR-UTAR_outputs:

Outputs
-------


- :ref:`valueFunctions <UTASTAR-UTAR-valueFunctions>`
- :ref:`message <UTASTAR-UTAR-logMessage>`


.. _UTASTAR-UTAR-valueFunctions:

valueFunctions
~~~~~~~~~~~~~~

Constructed value functions for the selected criteria and the provided rankings, using ACUTA method.

The returned value is a XMCDA document whose main tag is ``<criteria>``.
It has the following form::

   
				
					<criteria mcdaConcept="criteria">
						<criterion>
							<criterionID>[...]</criterionID>
							<criterionFunction>
								<points>
									<point>
										<abscissa><real>[...]</real></abscissa>
										<ordinate><real>[...]</real></ordinate>
									</point>
								</points>
							</criterionFunction>
						</criterion>
					</criteria>
				
			


------------------------


.. _UTASTAR-UTAR-logMessage:

message
~~~~~~~

logMessage

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.
It has the following form::

   
				
					<methodMessages mcdaConcept="methodMessage">
						<logMessage>
							<text>[...]</text>
						</logMessage>
						<errorMessage>
							<text>[...]</text>
						</errorMessage>
					</methodMessages>
				
			


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/UTASTAR-UTAR/description-wsDD.xml>`
