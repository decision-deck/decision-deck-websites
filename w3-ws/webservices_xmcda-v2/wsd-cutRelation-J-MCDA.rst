.. _cutRelation-J-MCDA:

cutRelation
===========

:Version: 0.5.5
:Provider: J-MCDA
:SOAP service's name: ``cutRelation-J-MCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Cuts a fuzzy relation (on alternatives) at a given threshold and produces a binary relation.

**Contact:** Olivier Cailloux <olivier.cailloux@ecp.fr>

**Web page:** http://sourceforge.net/projects/j-mcda/

**Reference:** Cailloux, Olivier. Electre and Promethee MCDA methods as reusable software components. In Proceedings of the 25th Mini-EURO Conference on Uncertainty and Robustness in Planning and Decision Making (URPDM 2010). Coimbra, Portugal, 2010.


Inputs
------
(For outputs, see :ref:`below <cutRelation-J-MCDA_outputs>`)


- :ref:`relation <cutRelation-J-MCDA-input0>`
- :ref:`alternatives <cutRelation-J-MCDA-input1>` *(optional)*
- :ref:`cut_threshold <cutRelation-J-MCDA-input2>`


.. _cutRelation-J-MCDA-input0:

relation
~~~~~~~~

The fuzzy relation to cut.

The input value should be a valid XMCDA document whose main tag is ``<alternativesComparisons>``.


------------------------


.. _cutRelation-J-MCDA-input1:

alternatives *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~

The alternatives to consider. Set some alternatives as inactive (or remove them) to ignore them.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _cutRelation-J-MCDA-input2:

cut_threshold
~~~~~~~~~~~~~

The threshold specifying where to cut the relation.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   

      <methodParameters>
         <parameter>
               <value>
                  <real>%1</real>
               </value>
         </parameter>
      </methodParameters>

			

where:

- **%1** is a parameter named "cut_threshold". This is a float.

------------------------



.. _cutRelation-J-MCDA_outputs:

Outputs
-------


- :ref:`binary_relation <cutRelation-J-MCDA-output0>`
- :ref:`messages <cutRelation-J-MCDA-output1>`


.. _cutRelation-J-MCDA-output0:

binary_relation
~~~~~~~~~~~~~~~

The binary relation resulting from the cut.

The returned value is a XMCDA document whose main tag is ``<alternativesComparisons>``.


------------------------


.. _cutRelation-J-MCDA-output1:

messages
~~~~~~~~

A status message.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/cutRelation-J-MCDA/description-wsDD.xml>`
