.. _criteriaDescriptiveStatistics-ITTB:

criteriaDescriptiveStatistics
=============================

:Version: 1.0
:Provider: ITTB
:SOAP service's name: ``criteriaDescriptiveStatistics-ITTB`` (see :ref:`soap-requests` for details)

Description
-----------

This web service provides statistics about the criteria. It computes for every criterion the average value of criteria's evaluations, the maximum value, the minimum value, the variance, the standard deviation as well as the median value.The criteria's evaluations are supposed to be real or integer numeric values.

**Contact:** Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)


Inputs
------
(For outputs, see :ref:`below <criteriaDescriptiveStatistics-ITTB_outputs>`)


- :ref:`criteria <criteriaDescriptiveStatistics-ITTB-criteria>`
- :ref:`alternatives <criteriaDescriptiveStatistics-ITTB-alternatives>`
- :ref:`performanceTable <criteriaDescriptiveStatistics-ITTB-performanceTable>`


.. _criteriaDescriptiveStatistics-ITTB-criteria:

criteria
~~~~~~~~

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
                            
                    <criteria>
                        <criterion>
                            <active>[...]</active>
                            [...]
                        </criterion>
                        [...]
                    </criteria>
                    
                     


------------------------


.. _criteriaDescriptiveStatistics-ITTB-alternatives:

alternatives
~~~~~~~~~~~~

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
                   
                    <alternatives>
                        <alternative>
                            <active>[...]</active>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
                    
               


------------------------


.. _criteriaDescriptiveStatistics-ITTB-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A performance table. The evaluations should be only real or integer numeric values, i.e. <real> or <integer>.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------



.. _criteriaDescriptiveStatistics-ITTB_outputs:

Outputs
-------


- :ref:`criteriaMeanValues <criteriaDescriptiveStatistics-ITTB-criteriaMeanValues>`
- :ref:`criteriaMaximumValues <criteriaDescriptiveStatistics-ITTB-criteriaMaximumValues>`
- :ref:`criteriaMinimumValues <criteriaDescriptiveStatistics-ITTB-criteriaMinimumValues>`
- :ref:`criteriaVariance <criteriaDescriptiveStatistics-ITTB-criteriaVariance>`
- :ref:`criteriaStandardDeviation <criteriaDescriptiveStatistics-ITTB-criteriaStandardDeviation>`
- :ref:`criteriaMedianValues <criteriaDescriptiveStatistics-ITTB-criteriaMedianValues>`
- :ref:`messages <criteriaDescriptiveStatistics-ITTB-messages>`


.. _criteriaDescriptiveStatistics-ITTB-criteriaMeanValues:

criteriaMeanValues
~~~~~~~~~~~~~~~~~~

Average values of criteria's evaluations

The returned value is a XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _criteriaDescriptiveStatistics-ITTB-criteriaMaximumValues:

criteriaMaximumValues
~~~~~~~~~~~~~~~~~~~~~

Maximum values of criteria's evaluations

The returned value is a XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _criteriaDescriptiveStatistics-ITTB-criteriaMinimumValues:

criteriaMinimumValues
~~~~~~~~~~~~~~~~~~~~~

Minimum values of criteria's evaluations

The returned value is a XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _criteriaDescriptiveStatistics-ITTB-criteriaVariance:

criteriaVariance
~~~~~~~~~~~~~~~~

Variance of criteria's evaluations

The returned value is a XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _criteriaDescriptiveStatistics-ITTB-criteriaStandardDeviation:

criteriaStandardDeviation
~~~~~~~~~~~~~~~~~~~~~~~~~

Standard deviation of criteria's evaluations

The returned value is a XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _criteriaDescriptiveStatistics-ITTB-criteriaMedianValues:

criteriaMedianValues
~~~~~~~~~~~~~~~~~~~~

Median values of criteria's evaluations

The returned value is a XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _criteriaDescriptiveStatistics-ITTB-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/criteriaDescriptiveStatistics-ITTB/description-wsDD.xml>`
