.. _csvToXMCDA-alternativesValues-PyXMCDA:

csvToXMCDA-alternativesValues
=============================

:Version: 1.0
:Provider: PyXMCDA
:SOAP service's name: ``csvToXMCDA-alternativesValues-PyXMCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Transforms a file containing alternatives values from a comma-separated values (CSV) file to two XMCDA compliant files, containing the corresponding alternatives ids and their alternativesValues.

**Contact:** Sébastien Bigaret (sebastien.bigaret@telecom-bretagne.eu)

**Web page:** http://github.com/sbigaret/ws-PyXMCDA


Inputs
------
(For outputs, see :ref:`below <csvToXMCDA-alternativesValues-PyXMCDA_outputs>`)


- :ref:`alternativesValues.csv <csvToXMCDA-alternativesValues-PyXMCDA-csv>`


.. _csvToXMCDA-alternativesValues-PyXMCDA-csv:

alternativesValues.csv
~~~~~~~~~~~~~~~~~~~~~~

The alternatives and their alternatives values as a CSV file.  The first line is made of two cells, the first one being empty, and the second one will be the content of the attribute "mcdaConcept" in the tag "<alternativesValues>", if supplied.

Example::

    ,ranks
    alt1,1
    alt2,2.7
    alt3,3

The input value should be a valid XMCDA document whose main tag is ``<other>``.


------------------------



.. _csvToXMCDA-alternativesValues-PyXMCDA_outputs:

Outputs
-------


- :ref:`alternatives <csvToXMCDA-alternativesValues-PyXMCDA-alternatives>`
- :ref:`alternativesValues <csvToXMCDA-alternativesValues-PyXMCDA-alternativesValues>`
- :ref:`messages <csvToXMCDA-alternativesValues-PyXMCDA-messages>`


.. _csvToXMCDA-alternativesValues-PyXMCDA-alternatives:

alternatives
~~~~~~~~~~~~

The equivalent alternatives ids.

The returned value is a XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _csvToXMCDA-alternativesValues-PyXMCDA-alternativesValues:

alternativesValues
~~~~~~~~~~~~~~~~~~

The equivalent alternatives values.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _csvToXMCDA-alternativesValues-PyXMCDA-messages:

messages
~~~~~~~~

Status messages.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/csvToXMCDA-alternativesValues-PyXMCDA/description-wsDD.xml>`
