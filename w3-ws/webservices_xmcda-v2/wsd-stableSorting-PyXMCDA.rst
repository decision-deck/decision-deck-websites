.. _stableSorting-PyXMCDA:

stableSorting
=============

:Version: 1.0
:Provider: PyXMCDA
:SOAP service's name: ``stableSorting-PyXMCDA`` (see :ref:`soap-requests` for details)

Description
-----------

This web service allows to compute the affectations of some alternatives in predefined categories, bounded with predefined profiles. It takes a stability relation, which inform on the dependency of the outranking statements to the preorder. Basically, a statement is said to be stable if it is only dependent on the preorder of the weights, not the precise fixation of them. A positive value (resp. negative one) means that the outranking statement is warranted (resp. is not warranted). A +1 or -1 value on a statement means that it is unstable: in that case, at it may be warranted or not, it can modify the affectation of the alternatives. We then consider here to give the list of contiguous categories such that the alternatives can be affected to every ones by modifying the weights without changing the preorder.

**Contact:** Veneziano Thomas (thomas.veneziano@uni.lu)


Inputs
------
(For outputs, see :ref:`below <stableSorting-PyXMCDA_outputs>`)


- :ref:`alternatives <stableSorting-PyXMCDA-alternatives>`
- :ref:`categories <stableSorting-PyXMCDA-categories>`
- :ref:`categoriesProfiles <stableSorting-PyXMCDA-categoriesProfiles>`
- :ref:`stabilityRelation <stableSorting-PyXMCDA-stabilityRelation>`
- :ref:`sortingMode <stableSorting-PyXMCDA-sortingMode>`


.. _stableSorting-PyXMCDA-alternatives:

alternatives
~~~~~~~~~~~~

A list of alternatives. Alternatives can be activated or deactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active. Alternatives can be also considered as fictive or not (using the tag <type>) and will not be affected in the categories. This is useful as the profiles have to be defined here. By default (no <type> tag), alternatives are considered as real.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
				
					<alternatives>
						<alternative>
							<active>[...]</active>   <!-- true or false -->
							<type>[...]</type> <!-- real or fictive -->
							[...]
						</alternative>
						[...]
					</alternatives>
				
			


------------------------


.. _stableSorting-PyXMCDA-categories:

categories
~~~~~~~~~~

A list of categories. They have to be ranked (using the tag rank).

The input value should be a valid XMCDA document whose main tag is ``<categories>``.
It must have the following form::

   
				
					<categories>
						<category id=[...]>
							<rank><integer>[...]</integer></rank>
						</category>
						[...]
					</categories>
				
			


------------------------


.. _stableSorting-PyXMCDA-categoriesProfiles:

categoriesProfiles
~~~~~~~~~~~~~~~~~~

A list of categoriesProfiles. They allow to know which profiles bound which categories. The profiles are defined under the tag alternatives.

The input value should be a valid XMCDA document whose main tag is ``<categories>``.
It must have the following form::

   
				
					<categoriesProfiles>
						<categoryProfile>
							<alternativeID>[...]</alternativeID> <!-- ID of the profile -->
							<limits>
								<lowerCategory><categoryID>[...]</categoryID></lowerCategory>
								<upperCategory><categoryID>[...]</categoryID></upperCategory>
							</limits>
						</categoryProfile>
						[...]
					</categoriesProfiles>
				
			


------------------------


.. _stableSorting-PyXMCDA-stabilityRelation:

stabilityRelation
~~~~~~~~~~~~~~~~~

The complete stability relation (at least, the relation that compare the alternatives and the profiles, not necessary the alternatives together). The value associated to each ordered pair (a,b) must be an integer between 3 and -3.

The input value should be a valid XMCDA document whose main tag is ``<alternativesComparisons>``.


------------------------


.. _stableSorting-PyXMCDA-sortingMode:

sortingMode
~~~~~~~~~~~

The type of sorting to use (optimistic or pessimistic).

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
				<methodParameters>
					<parameter name="sortingMode">
						<value>
							<label>%1</label>
						</value>
					</parameter>
				</methodParameters>
			

where:

- **%1** is a parameter named "sortingMode". It can have the following values:

  - ``optimistic``: Optimistic sorting.

  - ``pessimistic``: Pessimistic sorting.

  The default value is item0.


------------------------



.. _stableSorting-PyXMCDA_outputs:

Outputs
-------


- :ref:`alternativesAffectations <stableSorting-PyXMCDA-alternativesAffectations>`
- :ref:`messages <stableSorting-PyXMCDA-messages>`


.. _stableSorting-PyXMCDA-alternativesAffectations:

alternativesAffectations
~~~~~~~~~~~~~~~~~~~~~~~~

The affectations of the alternatives.

The returned value is a XMCDA document whose main tag is ``<alternativesAffectations>``.


------------------------


.. _stableSorting-PyXMCDA-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/stableSorting-PyXMCDA/description-wsDD.xml>`
