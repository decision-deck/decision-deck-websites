.. _PairwiseComparisons-HeuristicRatingEstimation-AGH:

PairwiseComparisons-HeuristicRatingEstimation
=============================================

:Version: 1.0
:Provider: AGH
:SOAP service's name: ``PairwiseComparisons-HeuristicRatingEstimation-AGH`` (see :ref:`soap-requests` for details)

Description
-----------

Computes HRE (Heuristic rating estimation) method - approach proposes a new way of using pairwise comparisons matrix.
Source code: https://github.com/djurasze/HRE-algo.
More info about algorithm: https://link.springer.com/article/10.1007/s10898-014-0253-4 and https://arxiv.org/pdf/1309.0386.pdf
For XMCDA input data version preferable is 3.0.0 but algorithm also support version 2.2.2

**Contact:** Dominik Juraszek (djurasze@gmail.com)


Inputs
------
(For outputs, see :ref:`below <PairwiseComparisons-HeuristicRatingEstimation-AGH_outputs>`)


- :ref:`alternatives <PairwiseComparisons-HeuristicRatingEstimation-AGH-alternatives>` *(optional)*
- :ref:`knownRankings <PairwiseComparisons-HeuristicRatingEstimation-AGH-alternativesValues>` *(optional)*
- :ref:`alternativesMatrix <PairwiseComparisons-HeuristicRatingEstimation-AGH-alternativesMatrix>`
- :ref:`parameters <PairwiseComparisons-HeuristicRatingEstimation-AGH-parameters>`


.. _PairwiseComparisons-HeuristicRatingEstimation-AGH-alternatives:

alternatives *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~

Alternatives that will be ranked. By default (no active tag), alternatives are considered as active. If absent, the program behaves as if it was supplied with all alternatives being active

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
                
    <alternatives>
        <alternative id="..."></alternative>
        [...]
    </alternatives>

            


------------------------


.. _PairwiseComparisons-HeuristicRatingEstimation-AGH-alternativesValues:

knownRankings *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~

Initially known rankings for alternatives.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.
It must have the following form::

   
                
    <alternativesValues>
        <alternativeValue>
            <alternativeID> [...] </alternativeID>
            <values>
                <value><real> [...] </real></value>
            </values>
        </alternativeValue>
        [...]
    </alternativesValues>

            


------------------------


.. _PairwiseComparisons-HeuristicRatingEstimation-AGH-alternativesMatrix:

alternativesMatrix
~~~~~~~~~~~~~~~~~~

Partial assessments pairs over the set of alternatives.

The input value should be a valid XMCDA document whose main tag is ``<alternativesMatrix>``.
It must have the following form::

   
                
    <alternativesMatrix>
        <row>
            <alternativeID>[...]</alternativeID>
            <column>
                <alternativeID>[...]</alternativeID>
                <values>
                    <value><real>[...]</real></value>
                </values>
            </column>
            [...]
        </row>
        [...]
    </alternativesMatrix>

            


------------------------


.. _PairwiseComparisons-HeuristicRatingEstimation-AGH-parameters:

parameters
~~~~~~~~~~

A set of method parameters to tune the algorithm:

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                

    <methodParameters>
        <parameter id="TryToReciprocalMatrix">
            <value>
                <boolean>%1</boolean>
            </value>
        </parameter>

        <parameter id="NormalizeResults">
            <value>
                <boolean>%2</boolean>
            </value>
        </parameter>

        <parameter id="CalculationMethod">
            <value>
                <label>%3</label>
            </value>
        </parameter>

        <parameter id="WithSmallestError">
            <value>
                <boolean>%4</boolean>
            </value>
        </parameter>

         <parameter id="MaxIterations">
            <value>
                <integer>%5</integer>
            </value>
        </parameter>
    </methodParameters>


            

where:

- **%1** is a parameter named "Try reciprocity restoration procedure for pairwise comparisons matrix?". This is a boolean.
  The default value is false.

- **%2** is a parameter named "Normalize results?". This is a boolean.
  The default value is true.

- **%3** is a parameter named "Calculation method". It can have the following values:

  - ``Arithmetic``: Arithmetic

  - ``Geometric``: Geometric

  - ``MinimizingEstimationError``: Minimizing estimation error with arithmetic approach

  The default value is item0.

- **%4** is a parameter named "Try minimal absolute estimation error?". This is a boolean.
  The default value is false.

- **%5** is a parameter named "Maximum iterations". This is a int, and the value should conform to the following constraint: Iterations should be positive.  More formally, the constraint is::

     
                             %1 > 0 
                        
  The default value is 100.


------------------------



.. _PairwiseComparisons-HeuristicRatingEstimation-AGH_outputs:

Outputs
-------


- :ref:`alternativesValues <PairwiseComparisons-HeuristicRatingEstimation-AGH-alternativesValues_out>`
- :ref:`messages <PairwiseComparisons-HeuristicRatingEstimation-AGH-logMessage>`


.. _PairwiseComparisons-HeuristicRatingEstimation-AGH-alternativesValues_out:

alternativesValues
~~~~~~~~~~~~~~~~~~

Calculated rankings for all alternatives.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   
                
    <alternativesValues>
        <alternativeValue>
            <alternativeID> [...] </alternativeID>
            <values>
                <value><real> [...] </real></value>
            </values>
        </alternativeValue>
        [...]
    </alternativesValues>

            


------------------------


.. _PairwiseComparisons-HeuristicRatingEstimation-AGH-logMessage:

messages
~~~~~~~~

Log message about program status. This can be 'ok' when algorithm finish successfully or 'error' when something goes wrong.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.
It has the following form::

   
                
    <methodMessages>
        <message>
            <text> [...] </text>
        </message>
    </methodMessages>

            


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/PairwiseComparisons-HeuristicRatingEstimation-AGH/description-wsDD.xml>`
