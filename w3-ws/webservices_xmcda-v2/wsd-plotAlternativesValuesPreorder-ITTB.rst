.. _plotAlternativesValuesPreorder-ITTB:

plotAlternativesValuesPreorder
==============================

:Version: 1.1
:Provider: ITTB
:SOAP service's name: ``plotAlternativesValuesPreorder-ITTB`` (see :ref:`soap-requests` for details)

Description
-----------

This web service generates a graph representing a preorder on the alternatives, according to numerical values taken by the alternatives (the "best" alternative has the highest value). Compared to the web service plotAlternativesValuesPreorder, some parameters are added. Colors can be used and the title of the graph can be typed. You can choose between an increasing or a decreasing order for the graph. It is also possible to show the name of the alternatives instead of the id, etc. The alternatives' evaluations are supposed to be real or integer numeric values.

**Contact:** Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)


Inputs
------
(For outputs, see :ref:`below <plotAlternativesValuesPreorder-ITTB_outputs>`)


- :ref:`alternatives <plotAlternativesValuesPreorder-ITTB-alternatives>`
- :ref:`alternativesValues <plotAlternativesValuesPreorder-ITTB-alternativesValues>`
- :ref:`options <plotAlternativesValuesPreorder-ITTB-options>`


.. _plotAlternativesValuesPreorder-ITTB-alternatives:

alternatives
~~~~~~~~~~~~

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
                   
                    <alternatives>
                        <alternative>
                            <active>[...]</active>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
                    
               


------------------------


.. _plotAlternativesValuesPreorder-ITTB-alternativesValues:

alternativesValues
~~~~~~~~~~~~~~~~~~

A list of <alternativesValue> representing a certain numeric quantity for each alternative, like, e.g., an overall value.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _plotAlternativesValuesPreorder-ITTB-options:

options
~~~~~~~

Options of the plotted graph.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                   
					<methodParameters>
						  <parameter id="plot_title" name="Plot Title">
							<value>
            					<label>%1</label>
       						 </value>
						</parameter>
						<parameter id="plot_order" name="Plot order">
							<value>
            					<label>%2</label>
       						 </value>
						</parameter>
						<parameter id="node_shape" name="Node shape">
							<value>
            					<label>%3</label>
       						 </value>
						</parameter>
						 <parameter id="show_names" name="Show alternatives names">
							<value>
            					<label>%4</label>
       						 </value>
						</parameter>
						 <parameter id="use_color" name="Colors in the graph">
							<value>
            					<label>%5</label>
       						 </value>
						</parameter>
						 <parameter id="selected_color" name="Selected color">
							<value>
            					<label>%6</label>
       						 </value>
						</parameter >
					</methodParameters>
				
               

where:

- **%1** is a parameter named "Chart title". This is a string.
- **%2** is a parameter named "Order". It can have the following values:

  - ``increasing``: Increasing

  - ``decreasing``: Decreasing

  The default value is decreasing.

- **%3** is a parameter named "Plot symbols". It can have the following values:

  - ``Rectangle``: Rectangle

  - ``Square``: Square

  - ``Ellipse``: Ellipse

  - ``Circle``: Circle

  - ``Diamond``: Diamond

  The default value is rectangle.

- **%4** is a parameter named "Display". It can have the following values:

  - ``true``: Name

  - ``false``: ID

  The default value is false.

- **%5** is a parameter named "Colored". It can have the following values:

  - ``true``: Yes

  - ``false``: No

  The default value is false.

- **%6** is a parameter named "Color". It can have the following values:

  - ``Black``: Black

  - ``Red``: Red

  - ``Blue``: Blue

  - ``Green``: Green

  - ``Yellow``: Yellow

  - ``Magenta``: Magenta

  - ``Cyan``: Cyan

  The default value is Black.


------------------------



.. _plotAlternativesValuesPreorder-ITTB_outputs:

Outputs
-------


- :ref:`alternativesValuesPlot <plotAlternativesValuesPreorder-ITTB-alternativesValuesPlot>`
- :ref:`messages <plotAlternativesValuesPreorder-ITTB-messages>`


.. _plotAlternativesValuesPreorder-ITTB-alternativesValuesPlot:

alternativesValuesPlot
~~~~~~~~~~~~~~~~~~~~~~

A string containing the base64 representation of the png image of the generated graph.

The returned value is a XMCDA document whose main tag is ``<alternativeValue>``.


------------------------


.. _plotAlternativesValuesPreorder-ITTB-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/plotAlternativesValuesPreorder-ITTB/description-wsDD.xml>`
