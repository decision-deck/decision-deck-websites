.. _csvToXMCDA-performanceTable-PyXMCDA:

csvToXMCDA-performanceTable
===========================

:Version: 1.0
:Provider: PyXMCDA
:SOAP service's name: ``csvToXMCDA-performanceTable-PyXMCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Transforms a file containing a performance table from a comma-separated values (CSV) file to three XMCDA compliant files, containing the corresponding criteria ids, alternatives' ids and the performance table.

**Contact:** Sébastien Bigaret (sebastien.bigaret@telecom-bretagne.eu)

**Web page:** http://github.com/sbigaret/ws-PyXMCDA


Inputs
------
(For outputs, see :ref:`below <csvToXMCDA-performanceTable-PyXMCDA_outputs>`)


- :ref:`performanceTable.csv <csvToXMCDA-performanceTable-PyXMCDA-input0>`


.. _csvToXMCDA-performanceTable-PyXMCDA-input0:

performanceTable.csv
~~~~~~~~~~~~~~~~~~~~

The performance table as a CSV file.

Example:

  ,cost,risk,employment,connection
  a11,17537,28.3,34.8,2.33
  a03,16973,29,34.9,2.66

The input value should be a valid XMCDA document whose main tag is ``<other>``.


------------------------



.. _csvToXMCDA-performanceTable-PyXMCDA_outputs:

Outputs
-------


- :ref:`criteria <csvToXMCDA-performanceTable-PyXMCDA-output1>`
- :ref:`alternatives <csvToXMCDA-performanceTable-PyXMCDA-output0>`
- :ref:`performanceTable <csvToXMCDA-performanceTable-PyXMCDA-output2>`
- :ref:`messages <csvToXMCDA-performanceTable-PyXMCDA-output3>`


.. _csvToXMCDA-performanceTable-PyXMCDA-output1:

criteria
~~~~~~~~

The equivalent criteria ids.

The returned value is a XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _csvToXMCDA-performanceTable-PyXMCDA-output0:

alternatives
~~~~~~~~~~~~

The equivalent alternative ids.

The returned value is a XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _csvToXMCDA-performanceTable-PyXMCDA-output2:

performanceTable
~~~~~~~~~~~~~~~~

The equivalent performances.

The returned value is a XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _csvToXMCDA-performanceTable-PyXMCDA-output3:

messages
~~~~~~~~

Status messages.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/csvToXMCDA-performanceTable-PyXMCDA/description-wsDD.xml>`
