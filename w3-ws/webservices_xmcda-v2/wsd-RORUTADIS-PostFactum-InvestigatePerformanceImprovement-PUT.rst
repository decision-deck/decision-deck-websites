.. _RORUTADIS-PostFactum-InvestigatePerformanceImprovement-PUT:

RORUTADIS-PostFactum-InvestigatePerformanceImprovement
======================================================

:Version: 0.3
:Provider: PUT
:SOAP service's name: ``RORUTADIS-PostFactum-InvestigatePerformanceImprovement-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Robust Ordinal Regression for value-based sorting: RORUTADIS-PostFactum-InvestigatePerformanceImprovement service calculates minimal value (rho) by which alternative evaluations on selected criteria have to be multiplied for that alternative to be possibly (or necessarily) assigned to at least some specific class (rho >= 1). Note! This function works for problems with only non-negative alternative evaluations It is possible to provide an additional optional preference information: example alternatives assignments, assignment pairwise comparisons and desired class cardinalities. Service developed by Krzysztof Ciomek (Poznan University of Technology, under supervision of Milosz Kadzinski).

**Contact:** 
			Krzysztof Ciomek (k.ciomek@gmail.com),
			Milosz Kadzinski (milosz.kadzinski@cs.put.poznan.pl)
		

**Web page:** https://github.com/kciomek/rorutadis

**Reference:** None


Inputs
------
(For outputs, see :ref:`below <RORUTADIS-PostFactum-InvestigatePerformanceImprovement-PUT_outputs>`)


- :ref:`criteria <RORUTADIS-PostFactum-InvestigatePerformanceImprovement-PUT-criteria>`
- :ref:`alternatives <RORUTADIS-PostFactum-InvestigatePerformanceImprovement-PUT-alternatives>`
- :ref:`categories <RORUTADIS-PostFactum-InvestigatePerformanceImprovement-PUT-categories>`
- :ref:`performanceTable <RORUTADIS-PostFactum-InvestigatePerformanceImprovement-PUT-performanceTable>`
- :ref:`criteriaManipulability <RORUTADIS-PostFactum-InvestigatePerformanceImprovement-PUT-criteriaManipulability>`
- :ref:`assignmentExamples <RORUTADIS-PostFactum-InvestigatePerformanceImprovement-PUT-assignmentExamples>` *(optional)*
- :ref:`assignmentComparisons <RORUTADIS-PostFactum-InvestigatePerformanceImprovement-PUT-assignmentComparisons>` *(optional)*
- :ref:`categoriesCardinalities <RORUTADIS-PostFactum-InvestigatePerformanceImprovement-PUT-categoriesCardinalities>` *(optional)*
- :ref:`methodParameters <RORUTADIS-PostFactum-InvestigatePerformanceImprovement-PUT-methodParameters>`


.. _RORUTADIS-PostFactum-InvestigatePerformanceImprovement-PUT-criteria:

criteria
~~~~~~~~

A list of criteria (<criteria> tag) with information about preference direction (<criteriaValues mcdaConcept="preferenceDirection">, 0 - gain, 1 - cost) and number of characteristic points (<criteriaValues mcdaConcept="numberOfCharacteristicPoints">, 0 for the most general marginal utility function or integer grater or equal to 2) of each criterion.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
				
					<criteria>
						<criterion id="[...]" />
						[...]
					</criteria>

					<criteriaValues mcdaConcept="preferenceDirection">
						<criterionValue>
							<criterionID>[...]</criterionID>
							<value><integer>[...]</integer></value>
						</criterionValue>
						[...]
					</criteriaValues>

					<criteriaValues mcdaConcept="numberOfCharacteristicPoints">
						<criterionValue>
							<criterionID>[...]</criterionID>
							<value><integer>[0|integer greater or equal to 2]</integer></value>
						</criterionValue>
						[...]
					</criteriaValues>
				
			


------------------------


.. _RORUTADIS-PostFactum-InvestigatePerformanceImprovement-PUT-alternatives:

alternatives
~~~~~~~~~~~~

A list of alternatives.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
				
					<alternatives>
                        <alternative id="[...]">
                            <active>[...]</active>
                        </alternative>
                        [...]
                    </alternatives>
				
			


------------------------


.. _RORUTADIS-PostFactum-InvestigatePerformanceImprovement-PUT-categories:

categories
~~~~~~~~~~

A list of categories (classes). List must be sorted from the worst category to the best.

The input value should be a valid XMCDA document whose main tag is ``<categories>``.
It must have the following form::

   
				
					<categories>
                        <category id="[...]" />
                        [...]
                    </categories>
				
			


------------------------


.. _RORUTADIS-PostFactum-InvestigatePerformanceImprovement-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

The performances of the alternatives.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _RORUTADIS-PostFactum-InvestigatePerformanceImprovement-PUT-criteriaManipulability:

criteriaManipulability
~~~~~~~~~~~~~~~~~~~~~~

A list of criteria values which denote whether multiplying by rho on corresponding criterion is allowed (value 1) or not (value 0). Values for all criteria have to be provided and at least one criterion has to be available for manipulation.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.
It must have the following form::

   
				
					<criteriaValues>
						<criterionValue>
							<criterionID>[...]</criterionID>
							<value><integer>[...]</integer></value>
						</criterionValue>
						[...]
					</criteriaValues>
				
			


------------------------


.. _RORUTADIS-PostFactum-InvestigatePerformanceImprovement-PUT-assignmentExamples:

assignmentExamples *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of assignment examples of alternatives to intervals of categories (classes) or to a specific category (class).

The input value should be a valid XMCDA document whose main tag is ``<alternativesAffectations>``.
It must have the following form::

   
				
					<alternativesAffectations>
						<alternativeAffectation>
							<alternativeID>[...]</alternativeID>
							<categoryID>[...]</categoryID>
						</alternativeAffectation>
						[...]
						<alternativeAffectation>
							<alternativeID>[...]</alternativeID>
							<categoriesInterval>
								<lowerBound>
									<categoryID>[...]</categoryID>
								</lowerBound>
								<upperBound>
									<categoryID>[...]</categoryID>
								</upperBound>
							</categoriesInterval>
						</alternativeAffectation>
						[...]
						<alternativeAffectation>
							<alternativeID>[...]</alternativeID>
							<categoriesSet>
								<categoryID>[...]</categoryID>
								[...]
							</categoriesSet>
						</alternativeAffectation>
						[...]
					</alternativesAffectations>
				
			


------------------------


.. _RORUTADIS-PostFactum-InvestigatePerformanceImprovement-PUT-assignmentComparisons:

assignmentComparisons *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Two lists of assignment pairwise comparisons. A comparison from list with attribute mcdaConcept="atLeastAsGoodAs" indicates that some alternative should be assigned to class at least as good as class of some other alternative (k = 0) or at least better by k classes (k > 0). A comparison from list with attribute mcdaConcept="atMostAsGoodAs" indicates that some alternative should be assigned to class at most better by k classes (k > 0) then some other alternative.

The input value should be a valid XMCDA document whose main tag is ``<alternativesComparisons>``.
It must have the following form::

   
				
					<alternativesComparisons mcdaConcept="atLeastAsGoodAs">
						<pairs>
							<pair>
								<initial><alternativeID>[...]</alternativeID></initial>
								<terminal><alternativeID>[...]</alternativeID></terminal>
								<value><integer>k</integer></value>
							</pair>
							[...]
						</pairs>
					</alternativesComparisons>

					<alternativesComparisons mcdaConcept="atMostAsGoodAs">
						<pairs>
							[...]
						</pairs>
					</alternativesComparisons>
				
			


------------------------


.. _RORUTADIS-PostFactum-InvestigatePerformanceImprovement-PUT-categoriesCardinalities:

categoriesCardinalities *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of category (class) cardinality constraints. It allows to define minimal and/or maximal desired category (class) cardinalities.

The input value should be a valid XMCDA document whose main tag is ``<categoriesValues>``.
It must have the following form::

   
				
					<categoriesValues>
						<categoryValue>
							<categoryID>[...]</categoryID>
							<value>
								<interval>
									<lowerBound><integer>[...]</integer></lowerBound>
									<upperBound><integer>[...]</integer></upperBound>
								</interval>
							</value>
						</categoryValue>
						[...]
					</categoriesValues>
				
			


------------------------


.. _RORUTADIS-PostFactum-InvestigatePerformanceImprovement-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~

Method parameters.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                
                    <methodParameters>
                        <parameter name="strictlyMonotonicValueFunctions">
                            <value>
                                <boolean>%1</boolean>
                            </value>
                        </parameter>
						<parameter name="alternative">
							<value><label>%2</label></value>
						</parameter>
                        <parameter name="necessary">
                            <value>
                                <boolean>%3</boolean>
                            </value>
                        </parameter>
						<parameter name="atLeastToClass">
							<value><label>%4</label></value>
						</parameter>
                    </methodParameters>
                
            

where:

- **%1** is a parameter named "strictlyMonotonicValueFunctions". This is a boolean.
  The default value is false.

- **%2** is a parameter named "alternative". This is a string, and the value should conform to the following constraint: This identifier cannot be empty.  More formally, the constraint is::

      ! %2.isEmpty() 
- **%3** is a parameter named "necessary". This is a boolean.
  The default value is false.

- **%4** is a parameter named "atLeastToClass". This is a string, and the value should conform to the following constraint: This identifier cannot be empty.  More formally, the constraint is::

      ! %4.isEmpty() 

------------------------



.. _RORUTADIS-PostFactum-InvestigatePerformanceImprovement-PUT_outputs:

Outputs
-------


- :ref:`values <RORUTADIS-PostFactum-InvestigatePerformanceImprovement-PUT-values>`
- :ref:`messages <RORUTADIS-PostFactum-InvestigatePerformanceImprovement-PUT-messages>`


.. _RORUTADIS-PostFactum-InvestigatePerformanceImprovement-PUT-values:

values
~~~~~~

Improvement value (value of rho).

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   
				
					<alternativesValues mcdaConcept="improvementValue">
						<alternativeValue>
							<alternativeID>[...]</alternativeID>
							<value>
								<real>[...]</real>
							</value>
						</alternativeValue>
					</alternativesValues>
				
			


------------------------


.. _RORUTADIS-PostFactum-InvestigatePerformanceImprovement-PUT-messages:

messages
~~~~~~~~

Messages generated by the program.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/RORUTADIS-PostFactum-InvestigatePerformanceImprovement-PUT/description-wsDD.xml>`
