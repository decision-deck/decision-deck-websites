.. _csvToXMCDA-criteriaThresholds-PyXMCDA:

csvToXMCDA-criteriaThresholds
=============================

:Version: 1.2
:Provider: PyXMCDA
:SOAP service's name: ``csvToXMCDA-criteriaThresholds-PyXMCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Transforms a file containing criteria discrimination thresholds and preference directions from a comma-separated values (CSV) file to an XMCDA compliant file, containing the criteria ids with their preference direction and related discrimination thresholds.

**Contact:** Sébastien Bigaret (sebastien.bigaret@telecom-bretagne.eu)

**Web page:** http://github.com/sbigaret/ws-PyXMCDA


Inputs
------
(For outputs, see :ref:`below <csvToXMCDA-criteriaThresholds-PyXMCDA_outputs>`)


- :ref:`thresholds.csv <csvToXMCDA-criteriaThresholds-PyXMCDA-csv>`


.. _csvToXMCDA-criteriaThresholds-PyXMCDA-csv:

thresholds.csv
~~~~~~~~~~~~~~

A csv with criteria' thresholds.

Example::

  ,cost,risks,employment,connection
  ind,1,2,3,4
  pref,2,3,4,5
  veto,3,4.6,5,6
  preferenceDirection,min,max,min,max

The labels of the separation thresholds ("ind", "pref", "veto") can be chosen freely in order to be in accordance with the selected outranking method. Further separation thresholds can also be added. The last line represents the preferenceDirection; its label must be "preferenceDirection".

The separator used in csv will be determined by examing the file (this means that it can be different than a comma: a semicolon, a tab or space character, etc.).

Thresholds values should be float; both decimal separator '.' and ',' are supported.  If a threshold value is left empty, the corresponding combination (criterion id, separation thresholds) is not present in the XMCDA output.  Same for "preferenceDirection": the corresponding tag 'scale' is present in the XMCDA output only if the preferenceDirection is supplied.

The input value should be a valid XMCDA document whose main tag is ``<other>``.


------------------------



.. _csvToXMCDA-criteriaThresholds-PyXMCDA_outputs:

Outputs
-------


- :ref:`criteria <csvToXMCDA-criteriaThresholds-PyXMCDA-criteria>`
- :ref:`messages <csvToXMCDA-criteriaThresholds-PyXMCDA-messages>`


.. _csvToXMCDA-criteriaThresholds-PyXMCDA-criteria:

criteria
~~~~~~~~

The equivalent XMCDA file containing criteria with their preference directions and discrimination thresholds.

The returned value is a XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _csvToXMCDA-criteriaThresholds-PyXMCDA-messages:

messages
~~~~~~~~

Status messages.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/csvToXMCDA-criteriaThresholds-PyXMCDA/description-wsDD.xml>`
