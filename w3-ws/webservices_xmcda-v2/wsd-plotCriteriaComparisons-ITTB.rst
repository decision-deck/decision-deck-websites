.. _plotCriteriaComparisons-ITTB:

plotCriteriaComparisons
=======================

:Version: 1.0
:Provider: ITTB
:SOAP service's name: ``plotCriteriaComparisons-ITTB`` (see :ref:`soap-requests` for details)

Description
-----------

This web service generates a graph representing a partial preorder on the criteria. Compared to the web service plotCriteriaComparisons, some parameters are added. The generated graph can be valued. It can also be transitive. Several shapes for the nodes are proposed and colors can be used.

**Contact:** Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)


Inputs
------
(For outputs, see :ref:`below <plotCriteriaComparisons-ITTB_outputs>`)


- :ref:`criteria <plotCriteriaComparisons-ITTB-criteria>`
- :ref:`criteriaComparisons <plotCriteriaComparisons-ITTB-criteriaComparisons>`
- :ref:`methodGraphOptions <plotCriteriaComparisons-ITTB-methodGraphOptions>`


.. _plotCriteriaComparisons-ITTB-criteria:

criteria
~~~~~~~~

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
                   
                    <criteria>
                        <criterion>
                            <active>[...]</active>
                            [...]
                        </criterion>
                        [...]
                    </criteria>
                    
               


------------------------


.. _plotCriteriaComparisons-ITTB-criteriaComparisons:

criteriaComparisons
~~~~~~~~~~~~~~~~~~~

A valued relation relative to comparisons of the criteria. A numeric <value> indicates a the valuation for each <pair> of the relation.

The input value should be a valid XMCDA document whose main tag is ``<criteriaComparisons>``.
It must have the following form::

   
               
                    <criteriaComparisons>
                        <pairs>
                            <pair>
                                <initial>
                                    <criterionID>[...]</criterionID>
                                </initial>
                                <terminal>
                                    <criterionID>[...]</criterionID>
                                </terminal>
                                <value>
                                    <real>[...]</real>
                                </value>
                            </pair>

                            [...]
                        </pairs>
                    </criteriaComparisons>
                    
                    


------------------------


.. _plotCriteriaComparisons-ITTB-methodGraphOptions:

methodGraphOptions
~~~~~~~~~~~~~~~~~~

Generates a graph taking into account the proposed options.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                   
					<methodParameters>
						<parameter id="valued_graph" name="Valued graph">
							<value>
            					<label>%1</label>
       						 </value>
						</parameter>
						<parameter id="transitive_reduction" name="Transitive reduction">
							<value>
            					<label>%2</label>
       						 </value>
						</parameter>
						<parameter id="node_shape" name="Node shape">
							<value>
            					<label>%3</label>
       						 </value>
						</parameter>

						<parameter id="use_color" name="Colors in the graph">
							<value>
            					<label>%4</label>
       						 </value>
						</parameter>
						
						<parameter id="selected_color" name="Selected color">
							<value>
            					<label>%5</label>
       						 </value>
						</parameter>
						
					</methodParameters>
				
               

where:

- **%1** is a parameter named "Graph type:". It can have the following values:

  - ``true``: Arcs and values

  - ``false``: Arcs only

  The default value is false.

- **%2** is a parameter named "With transitive reduction?". It can have the following values:

  - ``true``: Yes

  - ``false``: No

  The default value is false.

- **%3** is a parameter named "Shape of the nodes?". It can have the following values:

  - ``Rectangle``: Rectangle

  - ``Square``: Square

  - ``Ellipse``: Ellipse

  - ``Circle``: Circle

  - ``Diamond``: Diamond

  The default value is rectangle.

- **%4** is a parameter named "Use colors?". It can have the following values:

  - ``true``: Yes

  - ``false``: No

  The default value is false.

- **%5** is a parameter named "Choose color:". It can have the following values:

  - ``Black``: Black

  - ``Red``: Red

  - ``Blue``: Blue

  - ``Green``: Green

  - ``Yellow``: Yellow

  - ``Magenta``: Magenta

  - ``Cyan``: Cyan

  The default value is black.


------------------------



.. _plotCriteriaComparisons-ITTB_outputs:

Outputs
-------


- :ref:`criteriaComparisonsPlot <plotCriteriaComparisons-ITTB-criteriaComparisonsPlot>`
- :ref:`messages <plotCriteriaComparisons-ITTB-messages>`


.. _plotCriteriaComparisons-ITTB-criteriaComparisonsPlot:

criteriaComparisonsPlot
~~~~~~~~~~~~~~~~~~~~~~~

A string containing the base64 representation of the png image of the generated graph.

The returned value is a XMCDA document whose main tag is ``<criterionValue>``.


------------------------


.. _plotCriteriaComparisons-ITTB-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/plotCriteriaComparisons-ITTB/description-wsDD.xml>`
