.. _HierarchicalDEA-ValueAdditive_preferenceRelations-PUT:

HierarchicalDEA-ValueAdditive_preferenceRelations
=================================================

:Version: 1.0
:Provider: PUT
:SOAP service's name: ``HierarchicalDEA-ValueAdditive_preferenceRelations-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Computes necessary and possible preference relations for the pairs of DMUs (alternatives) using Additive Data Envelopment Analysis Model with hierarchical structure of inputs and outputs.

**Contact:** 
            Anna Labijak <anna.labijak@cs.put.poznan.pl>
        


Inputs
------
(For outputs, see :ref:`below <HierarchicalDEA-ValueAdditive_preferenceRelations-PUT_outputs>`)


- :ref:`units <HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-units>`
- :ref:`performanceTable <HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-performanceTable>`
- :ref:`inputsOutputs <HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-inputsOutputs>`
- :ref:`hierarchy <HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-hierarchy>`
- :ref:`weightsLinearConstraints <HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-methodParameters>`


.. _HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-units:

units
~~~~~

A list of alternatives (DMUs).

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
                <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
            


------------------------


.. _HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A list of alternatives (DMUs) performances (on leaf hierarchy criteria).

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   
                <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>
            


------------------------


.. _HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-inputsOutputs:

inputsOutputs
~~~~~~~~~~~~~

A list of performance criteria (hierarchy leafs) and their preference direction. List has to contain at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output) for each hierarchy category.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
                <criteria>
                        <criterion>
							<scale>
                                [...]
							</scale>
                            [...]
                        </criterion>
                        [...]
                    </criteria>
            


------------------------


.. _HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-hierarchy:

hierarchy
~~~~~~~~~

The hierarchical structure of criteria.

The input value should be a valid XMCDA document whose main tag is ``<hierarchy>``.
It must have the following form::

   
                
                <hierarchy>
                    <node>
                        <criterionID>[...]</criterionID>
                        <node>
                            <criterionID>[...]</criterionID>
                            <node>
                                [...]
                            </node>
                            [...]
                        </node>
                        [...]
                    </node>
                </hierarchy>
            


------------------------


.. _HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-weightsLinearConstraints:

weightsLinearConstraints *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of hierarchy criteria linear constraints.

The input value should be a valid XMCDA document whose main tag is ``<criteriaLinearConstraints>``.
It must have the following form::

   
                <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID> [...] </criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>
            


------------------------


.. _HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~

Represents parameters.
            "hierarchy node" is the ID of the hierarchy criterion for which the analysis should be performed;
            "transformToUtilities" means if data should be tranformed into values from range [0-1];
            "boundariesProvided" means if inputsOutputs file contains information about min and max data for each factor.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                
	<methodParameters>
		<parameter id="hierarchyNode">
            <value><label>%1</label></value>
        </parameter>
		<parameter id="transformToUtilities">
			<value><boolean>%2</boolean></value>
		</parameter>
		<parameter id="boundariesProvided">
			<value><boolean>%3</boolean></value>
		</parameter>
	</methodParameters>

            

where:

- **%1** is a parameter named "hierarchy node". This is a string.
  The default value is root.

- **%2** is a parameter named "transform to utilities". This is a boolean.
  The default value is true.

- **%3** is a parameter named "boundaries provided". This is a boolean.
  The default value is false.


------------------------



.. _HierarchicalDEA-ValueAdditive_preferenceRelations-PUT_outputs:

Outputs
-------


- :ref:`necessaryDominance <HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-necessaryDominance>`
- :ref:`possibleDominance <HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-possibleDominance>`
- :ref:`messages <HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-messages>`


.. _HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-necessaryDominance:

necessaryDominance
~~~~~~~~~~~~~~~~~~

A list of pairs of DMU related with necessary preference relation.

The returned value is a XMCDA document whose main tag is ``<alternativesComparisons>``.
It has the following form::

   
                <alternativesComparisons>
						<pairs>
              <pair>
                  <initial>
                    <alternativeID>[...]</alternativeID>
                  </initial>
                  <terminal>
                    <alternativeID>[...]</alternativeID>
                  </terminal>
                  <values>
                    <value>
                      <real>1</real>
                    <value>
                  </values>
              </pair>
						  [...]
            </pairs>
            [...]
					</alternativesComparisons>
            


------------------------


.. _HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-possibleDominance:

possibleDominance
~~~~~~~~~~~~~~~~~

A list of pairs of DMU related with possible preference relation.

The returned value is a XMCDA document whose main tag is ``<alternativesComparisons>``.
It has the following form::

   
                <alternativesComparisons>
						<pairs>
              <pair>
                <initial>
                  <alternativeID>[...]</alternativeID>
                </initial>
                <terminal>
                  <alternativeID>[...]</alternativeID>
                </terminal>
                <values>
                  <value>
                    <real>1</real>
                  </value>
                </value>
            </pair>
						[...]
          </pairs>
          [...]
					</alternativesComparisons>
            


------------------------


.. _HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/HierarchicalDEA-ValueAdditive_preferenceRelations-PUT/description-wsDD.xml>`
