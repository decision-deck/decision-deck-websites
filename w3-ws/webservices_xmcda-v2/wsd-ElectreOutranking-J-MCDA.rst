.. _ElectreOutranking-J-MCDA:

ElectreOutranking
=================

:Version: 0.5.5
:Provider: J-MCDA
:SOAP service's name: ``ElectreOutranking-J-MCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Computes an outranking relation.

**Contact:** Olivier Cailloux <olivier.cailloux@ecp.fr>

**Web page:** http://sourceforge.net/projects/j-mcda/

**Reference:** Cailloux, Olivier. Electre and Promethee MCDA methods as reusable software components. In Proceedings of the 25th Mini-EURO Conference on Uncertainty and Robustness in Planning and Decision Making (URPDM 2010). Coimbra, Portugal, 2010.


Inputs
------
(For outputs, see :ref:`below <ElectreOutranking-J-MCDA_outputs>`)


- :ref:`criteria <ElectreOutranking-J-MCDA-input1>` *(optional)*
- :ref:`alternatives <ElectreOutranking-J-MCDA-input0>` *(optional)*
- :ref:`discordances <ElectreOutranking-J-MCDA-input2>`
- :ref:`concordance <ElectreOutranking-J-MCDA-input3>`


.. _ElectreOutranking-J-MCDA-input1:

criteria *(optional)*
~~~~~~~~~~~~~~~~~~~~~

The criteria to consider, only used as filter. Set some criteria as inactive (or remove them) to ignore them. Please note that these criteria will only (possibly) filter out corresponding discordances. The concordances must have been computed with the same set of criteria, otherwise the results will be meaningless.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _ElectreOutranking-J-MCDA-input0:

alternatives *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~

The alternatives to consider. Set some alternatives as inactive (or remove them) to ignore them.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _ElectreOutranking-J-MCDA-input2:

discordances
~~~~~~~~~~~~

The discordance relations to use (one per criterion).

The input value should be a valid XMCDA document whose main tag is ``<alternativesComparisons>``.


------------------------


.. _ElectreOutranking-J-MCDA-input3:

concordance
~~~~~~~~~~~

The concordance relation to use.

The input value should be a valid XMCDA document whose main tag is ``<alternativesComparisons>``.


------------------------



.. _ElectreOutranking-J-MCDA_outputs:

Outputs
-------


- :ref:`outranking <ElectreOutranking-J-MCDA-output0>`
- :ref:`messages <ElectreOutranking-J-MCDA-output1>`


.. _ElectreOutranking-J-MCDA-output0:

outranking
~~~~~~~~~~

The outranking relation computed from the given data.

The returned value is a XMCDA document whose main tag is ``<alternativesComparisons>``.


------------------------


.. _ElectreOutranking-J-MCDA-output1:

messages
~~~~~~~~

A status message.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/ElectreOutranking-J-MCDA/description-wsDD.xml>`
