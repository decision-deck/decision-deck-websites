import os
ws_per_page = 10
ws_file_prefix = "wsd-"

def DeleteOldIndexFiles():
    for fname in os.listdir("./"):
        if fname.endswith(".rst") and fname.startswith("wsindex-"):
            os.remove(fname)

def GetWSData():
    webservices = []
    for fname in os.listdir("./"):
        if fname.endswith(".rst") and fname.startswith(ws_file_prefix):
            # read input file
            fi = open("./" + fname, "r")
            lines = fi.read().splitlines()
            fi.close()
            ws = {}
            ws['filename'] = fname[:-4]
            nameparts = ws['filename'][len(ws_file_prefix):].split('-')
            if len(nameparts) == 1:
                ws['name'] = nameparts[0]
            else:
                ws['name'] = '-'.join(nameparts[:-1])
            ws['version'] = ''
            for line in lines:
                if line.startswith(':Version:'):
                    ws['version'] = line.split()[1]
                    break
            ws['provider'] = ''
            for line in lines:
                if line.startswith(':Provider:'):
                    ws['provider'] = line.split()[1]
                    break
            ws['contact'] = ''
            for line in lines:
                if line.startswith('**Contact:**'):
                    ws['contact'] = line[12:]
                    break
            ws['description'] = ''
            for i in range(len(lines)):
                if lines[i].startswith('Description'):
                    j = i + 1
                    if lines[j].startswith('-'):
                        j = j + 1
                    else:
                        ws['description'] = lines[j]
                        break
                    if len(lines[j]) == 0:
                        j = j + 1
                    else:
                        ws['description'] = lines[j]
                        break
                    if len(lines[j]) != 0:
                        ws['description'] = lines[j]
                    break
            webservices.append(ws)
    return webservices

def GetHeader(letters, providers, filename_prefix = 'wsindex', current_page = ''):
    header = 'List of available XMCDA v2 web services\n------------------------------------\n\n.. raw:: html\n\n'
    # add first letter buttons
    header += '    <div class="btn-toolbar" style="margin-top: 20px;;margin-bottom: 5px;">\n'
    header += '      <div class="btn-group">\n'
    header += '        <a href="%s-1.html" class="btn btn-%s">All</a>\n'%(filename_prefix, 'info' if current_page == '' else 'default')
    header += '      </div>\n'
    header += '      <div class="btn-group">\n'
    for letter in letters:
        header += '        <a href="%s-%s-1.html" class="btn btn-%s">%s</a>\n'%(filename_prefix, letter, 'info' if current_page == letter else 'default', letter.upper())
    header += '      </div>\n'
    header += '    </div>\n'
    # add provider buttons
    header += '    <div class="btn-toolbar" style="margin-top: 5px;;margin-bottom: 20px;">\n'
    header += '      <div class="btn-group">\n'
    for provider in providers:
        header += '        <a href="%s-%s-1.html" class="btn btn-%s">%s</a>\n'%(filename_prefix, provider, 'info' if current_page == provider else 'default', provider)
    header += '      </div>\n'
    header += '    </div>\n'
    return header

def WriteIndexPages(webservices, webservicesnames, output_header, filename_prefix = 'index'):
    # build the index pages
    total_ws = len(webservicesnames)
    total_index_pages = total_ws / ws_per_page
    if total_ws % ws_per_page != 0:
        total_index_pages += 1
    ws_ct = 0
    ws_page_ct = 0
    index_ct = 0
    output_index = output_header
    for wsname in webservicesnames:
        for ws in webservices:
            if ws['name'].lower() == wsname:
                ws_ct += 1
                ws_page_ct += 1
                # add content to index page
                output_index += '    <p><a href="' + ws['filename'] + '.html" class = "h3">' + ws['name'] + '</a><span class="glyphicon glyphicon-minus"></span> <strong>Version:</strong> ' + ws['version'] +'<span class="glyphicon glyphicon-minus"></span> <strong>Provider:</strong> ' + ws['provider'] + '</p>\n'
                desc = ws['description']
                if len(desc) > 200:
                    desc = desc[:196] + ' ...'
                output_index += '    <p><strong>Description:</strong> ' + desc + '</p>\n'
                if ws_page_ct < ws_per_page and ws_ct < total_ws:
                    output_index += '    <hr>\n'
                else:
                    ws_page_ct = 0
                    # add pagination
                    output_index += '\n\n'
                    output_index += '  <ul class="pagination">\n'
                    prev_index = index_ct
                    if prev_index == 0:
                        output_index += '    <li class="disabled"><a href="#">&laquo;</a></li>\n'
                    else:
                        output_index += '    <li><a href="%s-%d.html">&laquo;</a></li>\n'%(filename_prefix, prev_index)
                    for i in range(total_index_pages):
                        if i == index_ct:
                            output_index += '    <li class="active"><a href="#">%d</a></li>\n'%(i+1)
                        else:
                            output_index += '    <li><a href="%s-%d.html">%d</a></li>\n'%(filename_prefix, i+1, i+1)
                    next_index = index_ct + 2
                    if next_index == total_index_pages + 1:
                        output_index += '    <li class="disabled"><a href="#">&raquo;</a></li>\n'
                    else:
                        output_index += '    <li><a href="%s-%d.html">&raquo;</a></li>\n'%(filename_prefix, next_index)
                    output_index += '  </ul>'
                    # write index file
                    ifname = '%s-%d.rst'%(filename_prefix, index_ct + 1)
                    fo = open('./' + ifname, "w")
                    fo.write(output_index)
                    fo.close()
                    output_index = output_header
                    index_ct += 1

# delete previous index files since they could be more than we will overwrite
DeleteOldIndexFiles()

# read wsd files and extract needed info
webservices = GetWSData()

# get ws name starting letters
letters = []
for ws in webservices:
    if not ws['name'][0].lower() in letters:
        letters.append(ws['name'][0].lower())
letters.sort()
# get ws providers
providers = []
for ws in webservices:
    if not ws['provider'].lower() in providers:
        providers.append(ws['provider'].lower())
providers.sort()
# get all ws names
wsnames = []
for ws in webservices:
    if not ws['name'].lower() in wsnames:
        wsnames.append(ws['name'].lower())
wsnames.sort()

# write index for all ws
WriteIndexPages(webservices, wsnames, GetHeader(letters, providers, 'wsindex'), 'wsindex')

# write index for ws of each letter
for letter in letters:
    temp_wsnames = []
    for wsname in wsnames:
        if wsname.startswith(letter):
            temp_wsnames.append(wsname)
    temp_wsnames.sort()
    WriteIndexPages(webservices, temp_wsnames, GetHeader(letters, providers, 'wsindex', letter), 'wsindex-%s'%letter)

# write index for ws of each provider
for provider in providers:
    temp_wsnames = []
    for ws in webservices:
        if ws['provider'].lower() == provider:
            temp_wsnames.append(ws['name'].lower())
    temp_wsnames.sort()
    WriteIndexPages(webservices, temp_wsnames, GetHeader(letters, providers, 'wsindex', provider), 'wsindex-%s'%provider)


