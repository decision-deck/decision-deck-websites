.. _DEASMAAvalueADDPreferenceRelations-PUT:

DEASMAAvalueADDPreferenceRelations
==================================

:Version: 1.0
:Provider: PUT
:SOAP service's name: ``DEASMAAvalueADDPreferenceRelations-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Determines dominance relations for the given DMUs (alternatives) using SMAA-D method and additive Data Envelopment Analysis Model. For given number of samples  returns a matrix with alternatives in each row and column. Single cell indicates how many samples of alternative in a row dominates alternative in a column.

**Contact:** Malgorzata Napieraj (napieraj.malgorzata@gmail.com)

**Reference:** Gouveia M. C., Dias L. C., Antunes C. H., Additive DEA based on MCDA with imprecise information (2008).
			Lahdelma R., Salminen P., Stochastic multicriteria acceptability analysis using the data envelopment model (2004).


Inputs
------
(For outputs, see :ref:`below <DEASMAAvalueADDPreferenceRelations-PUT_outputs>`)


- :ref:`inputsOutputs <DEASMAAvalueADDPreferenceRelations-PUT-inputsOutputs>`
- :ref:`units <DEASMAAvalueADDPreferenceRelations-PUT-units>`
- :ref:`performanceTable <DEASMAAvalueADDPreferenceRelations-PUT-performanceTable>`
- :ref:`weightsLinearConstraints <DEASMAAvalueADDPreferenceRelations-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <DEASMAAvalueADDPreferenceRelations-PUT-methodParameters>`


.. _DEASMAAvalueADDPreferenceRelations-PUT-inputsOutputs:

inputsOutputs
~~~~~~~~~~~~~

A list of criteria with specified preference direction. List has to contains at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output).

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   <criteria>
                        <criterion>
							<scale>
								<quantitative>
									<preferenceDirection>
										[...]
									</preferenceDirection>
								</quantitative>
							</scale>
                            [...]
                        </criterion>
                        [...]
                    </criteria>


------------------------


.. _DEASMAAvalueADDPreferenceRelations-PUT-units:

units
~~~~~

A list of alternatives (DMUs).

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>


------------------------


.. _DEASMAAvalueADDPreferenceRelations-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A list of alternatives (DMUs) performances.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   <performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>


------------------------


.. _DEASMAAvalueADDPreferenceRelations-PUT-weightsLinearConstraints:

weightsLinearConstraints *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of criteria linear constraints.

The input value should be a valid XMCDA document whose main tag is ``<criteriaLinearConstraints>``.
It must have the following form::

   <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID>[...]</criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>


------------------------


.. _DEASMAAvalueADDPreferenceRelations-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~

"boundariesProvided" indicates if criteria values boundaries are given in criteria input or they have to be computed based on alternatives performances. "samplesNo" represents the number of samples to generate. "transformToUtilites" indicates if given alternatives values are utilities or if they have to be firstly transformed to utilities.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   <methodParameters>
							<parameter name="boundariesProvided">
								<value><boolean>%1</boolean></value>
							</parameter>
							<parameter name="samplesNo">
								<value><integer>%2</integer></value>
							</parameter>
							<parameter name="transformToUtilities">
								<value><boolean>%3</boolean></value>
							</parameter>
					</methodParameters>

where:

- **%1** is a parameter named "boundariesProvided". This is a boolean.
  The default value is false.

- **%2** is a parameter named "samplesNo". This is a int, and the value should conform to the following constraint: The value should be a positive integer..  More formally, the constraint is::

      %2 > 0 
  The default value is 100.

- **%3** is a parameter named "transformToUtilities". This is a boolean.
  The default value is true.


------------------------



.. _DEASMAAvalueADDPreferenceRelations-PUT_outputs:

Outputs
-------


- :ref:`pairwiseOutrankingIndices <DEASMAAvalueADDPreferenceRelations-PUT-pairwiseOutrankingIndices>`
- :ref:`messages <DEASMAAvalueADDPreferenceRelations-PUT-messages>`


.. _DEASMAAvalueADDPreferenceRelations-PUT-pairwiseOutrankingIndices:

pairwiseOutrankingIndices
~~~~~~~~~~~~~~~~~~~~~~~~~

A performance table for given alternatives. Single performance consists of attribute criterionID representing dominated alternative, and a value representing ratio of samples dominating this alternative.

The returned value is a XMCDA document whose main tag is ``<performanceTable>``.
It has the following form::

   <performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>geq [...]</criterionID>
									<value>
									[...]
									</value>
							</performance>
							[...]
						</alternativePerformances>
					</performanceTable>


------------------------


.. _DEASMAAvalueADDPreferenceRelations-PUT-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/DEASMAAvalueADDPreferenceRelations-PUT/description-wsDD.xml>`
