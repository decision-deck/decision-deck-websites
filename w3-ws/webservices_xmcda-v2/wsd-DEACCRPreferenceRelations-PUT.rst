.. _DEACCRPreferenceRelations-PUT:

DEACCRPreferenceRelations
=========================

:Version: 1.0
:Provider: PUT
:SOAP service's name: ``DEACCRPreferenceRelations-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Determines necessary and possible dominance relations for the given DMUs (alternatives) using CCR Data Envelopment Analysis Model.

**Contact:** Malgorzata Napieraj (napieraj.malgorzata@gmail.com)

**Reference:** Salo A., Punkka A., Ranking Intervals and Dominance Relations for Ratio-Based Efficiency Analysis (2011).


Inputs
------
(For outputs, see :ref:`below <DEACCRPreferenceRelations-PUT_outputs>`)


- :ref:`inputsOutputs <DEACCRPreferenceRelations-PUT-inputsOutputs>`
- :ref:`units <DEACCRPreferenceRelations-PUT-units>`
- :ref:`performanceTable <DEACCRPreferenceRelations-PUT-performanceTable>`
- :ref:`weightsLinearConstraints <DEACCRPreferenceRelations-PUT-weightsLinearConstraints>` *(optional)*


.. _DEACCRPreferenceRelations-PUT-inputsOutputs:

inputsOutputs
~~~~~~~~~~~~~

A list of criteria with specified preference direction. List has to contains at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output).

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   <criteria>
                        <criterion>
							<scale>
								<quantitative>
									<preferenceDirection>
										[...]
									</preferenceDirection>
								</quantitative>
							</scale>
                            [...]
                        </criterion>
                        [...]
                    </criteria>


------------------------


.. _DEACCRPreferenceRelations-PUT-units:

units
~~~~~

A list of alternatives (DMUs).

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>


------------------------


.. _DEACCRPreferenceRelations-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A list of alternatives (DMUs) performances.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   <performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>


------------------------


.. _DEACCRPreferenceRelations-PUT-weightsLinearConstraints:

weightsLinearConstraints *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of criteria linear constraints.

The input value should be a valid XMCDA document whose main tag is ``<criteriaLinearConstraints>``.
It must have the following form::

   <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID>[...]</criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>


------------------------



.. _DEACCRPreferenceRelations-PUT_outputs:

Outputs
-------


- :ref:`possibleDominance <DEACCRPreferenceRelations-PUT-possibleDominance>`
- :ref:`necessaryDominance <DEACCRPreferenceRelations-PUT-necessaryDominance>`
- :ref:`messages <DEACCRPreferenceRelations-PUT-messages>`


.. _DEACCRPreferenceRelations-PUT-possibleDominance:

possibleDominance
~~~~~~~~~~~~~~~~~

A list of alternatives pairs being in a possible dominance relation. If list contains pair (A,B), it means that alternative A dominates alternative B.

The returned value is a XMCDA document whose main tag is ``<alternativesComparisons>``.
It has the following form::

   <alternativesComparisons mcdaConcept="possibleDominance">
						<pairs>
						  <pair>
							<initial>
							  <alternativeID>[...]</alternativeID>
							</initial>
							<terminal>
							  <alternativeID>[...]</alternativeID>
							</terminal>
							<value>
							  [...]
							</value>
						  </pair>	
						  [...]
						</pairs>
					</alternativesComparisons>


------------------------


.. _DEACCRPreferenceRelations-PUT-necessaryDominance:

necessaryDominance
~~~~~~~~~~~~~~~~~~

A list of alternatives pairs being in a necessary dominance relation. If list contains pair (A,B), it means that alternative A dominates alternative B.

The returned value is a XMCDA document whose main tag is ``<alternativesComparisons>``.
It has the following form::

   <alternativesComparisons mcdaConcept="necessaryDominance">
						<pairs>
						  <pair>
							<initial>
							  <alternativeID>[...]</alternativeID>
							</initial>
							<terminal>
							  <alternativeID>[...]</alternativeID>
							</terminal>
							<value>
							  [...]
							</value>
						  </pair>	
						  [...]
						</pairs>
					</alternativesComparisons>


------------------------


.. _DEACCRPreferenceRelations-PUT-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/DEACCRPreferenceRelations-PUT/description-wsDD.xml>`
