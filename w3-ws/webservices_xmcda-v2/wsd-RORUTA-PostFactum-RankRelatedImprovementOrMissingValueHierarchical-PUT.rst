.. _RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical-PUT:

RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical
==================================================================

:Version: 1.0
:Provider: PUT
:SOAP service's name: ``RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

For a given ranking position n and the alternative a which is worse than n-th, depending on the need, the module finds one of the two possible types of values. These are the minimal improvement of the performance of the alternative a or the minimal missing value of the comprehensive score of this alternative. In the first case, the procedure finds a minimal greater than one value that multiplied by the performances on chosen criteria allows to attain the desired n-th position for the alternative. In the other case, it finds the minimal value that need to be added to the comprehensive score of this alternative in order to attain at least n-th position. In both cases it is possible to consider the target for both all or at least one compatible value funtion. The module supports a hierarchical decomposition of the problem.

**Contact:** Pawel Rychly (pawelrychly@gmail.com).


Inputs
------
(For outputs, see :ref:`below <RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical-PUT_outputs>`)


- :ref:`criteria <RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical-PUT-criteria>`
- :ref:`selected-criteria <RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical-PUT-selected-criteria>` *(optional)*
- :ref:`alternatives <RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical-PUT-alternatives>`
- :ref:`hierarchy-of-criteria <RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical-PUT-hierarchy-of-criteria>`
- :ref:`performances <RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical-PUT-performances>`
- :ref:`characteristic-points <RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical-PUT-characteristic-points>` *(optional)*
- :ref:`preferences <RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical-PUT-preferences>` *(optional)*
- :ref:`intensities-of-preferences <RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical-PUT-intensities-of-preferences>` *(optional)*
- :ref:`target <RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical-PUT-target>`
- :ref:`rank-related-requirements <RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical-PUT-rank-related-requirements>` *(optional)*
- :ref:`parameters <RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical-PUT-parameters>`


.. _RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical-PUT-criteria:

criteria
~~~~~~~~

A list of all considered criteria. The input value should be a valid XMCDA document whose main tag is criteria.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
                
                    <criteria>
                        <criterion id="%1" name="%1"></criterion>
                        [...]
                    </criteria>
                    
            


------------------------


.. _RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical-PUT-selected-criteria:

selected-criteria *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A set of ids of the criteria that should be taken into account for modification of the performances. This parameter is used only if the modification of performances is searched.

The input value should be a valid XMCDA document whose main tag is ``<criteriaSets>``.
It must have the following form::

   
                
                  <criteriaSets>
                     <criteriaSet>
                        <element><criterionID>[...]</criterionID></element>
                        [...]
                     </criteriaSet>
                  </criteriaSets>
                    
            


------------------------


.. _RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical-PUT-alternatives:

alternatives
~~~~~~~~~~~~

The list of all considered alternatives. The input value should be a valid XMCDA document whose main tag is alternatives. Each alternative may be described using two attributes: id and name. While the first one denotes a machine readable name, the second represents a human readable name.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
                
                    <alternatives>
                        <alternative id="%1" name="%2" />
                        [...]
                    </alternatives>
                    
            


------------------------


.. _RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical-PUT-hierarchy-of-criteria:

hierarchy-of-criteria
~~~~~~~~~~~~~~~~~~~~~

Description of the hierarchical structure of criteria. Each node of this hierarchy needs to have a unique id attribute. The most nested nodes, should contain a set of criteria. The input value should be provided as a valid XMCDA document whose main tag is hierarchy

The input value should be a valid XMCDA document whose main tag is ``<hierarchy>``.
It must have the following form::

   
                
                    <hierarchy>
                        <node id="nodes">
                            <node id="nodes1">
                                <criteriaSet>
                                    <element><criterionID>%1</criterionID></element> [...]
                                </criteriaSet>
                            </node>
                            [...]
                        </node>
                        [...]
                    </hierarchy>
                    
            


------------------------


.. _RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical-PUT-performances:

performances
~~~~~~~~~~~~

Description of evaluation of alternatives on different criteria. It is required to provide the IDs of both criteria and alternatives described previously. The input value should be provided as a valid XMCDA document whose main tag is performanceTable

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   
                
                    <performanceTable>
                        <alternativePerformances>
                            <alternativeID>%1</alternativeID>
                            <performance>
                                <criterionID>%2</criterionID>
                                <value>
                                    <real>%3</real>
                                </value>
                            </performance>
                            [...]
                        </alternativePerformances>
                        [...]
                    </performanceTable>
                    
            


------------------------


.. _RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical-PUT-characteristic-points:

characteristic-points *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A set of values associated with the criteria. This input allows to determine what type of value function should be used for the particular criterion. For each criterion that has an associated greater than one value, a piecewise linear value function is used. In this case, the mentioned value denotes a number of characteristic points of this value function. For the criteria that are not listed in this file, or for these for which the provided values are lower than two uses a general value function. The input value should be provided as a valid XMCDA document whose main tag is criteriaValues. Each element should contain both an id of the criterion, and value tag.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.
It must have the following form::

   
                
                     <criteriaValues>
                        <criterionValue>
                            <criterionID>%1</criterionID>
                            <value>
                                <integer>%2</integer>
                            </value>
                        </criterionValue>
                        [...]
                    </criteriaValues>
                    
            


------------------------


.. _RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical-PUT-preferences:

preferences *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~

Set of pairwise comparisons of reference alternatives. For a pair of alternatives three types of comparisons are supported. These are the strict preference, weak preference, and indifference. Values linked to pairs indicate  ids of nodes in the hierarchy of criteria tree. If value is not given or if it is equal to 0 pairwise comparison is assumed to concern for the whole set of criteria. Otherwise, the preference relation applies only to a particular node. The input value should be provided as a valid XMCDA document whose main tag is alternativesComparisons. For each type of comparison, a separate alternativesComparisons tag should be used. Within these groups a mentioned types are denoted using a comparisonType tag by respectively strict, weak, and indif label. Comparisons should be provided as pairs of alternatives ids.

The input value should be a valid XMCDA document whose main tag is ``<alternativesComparisons>``.
It must have the following form::

   
                
                    <alternativesComparisons>
                        <comparisonType>
                            %1<!-- type of preference: strong, weak, or indif -->
                        </comparisonType>
                        <pairs>
                            <pair>
                                <initial>
                                    <alternativeID>%2</alternativeID>
                                </initial>
                                <terminal>
                                    <alternativeID>%3</alternativeID>
                                </terminal>
                                <value>
                                    <label>%4</label>
                                </value>
                            </pair>
                            [...]
                        </pairs>
                    </alternativesComparisons>
                    
            


------------------------


.. _RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical-PUT-intensities-of-preferences:

intensities-of-preferences *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Set of comparisons of intensities of preference. For a pair of preference relations three types of comparisons are supported. These are the strict preference, weak preference, and indifference. Values linked to pairs, determine ids of nodes in the hierarchy of criteria tree. If value is not given or if it is equal to 0 intensity of preference is assumed to concern for the whole set of criteria. Otherwise, the statement applies only to a particular node. The input value should be provided as a valid XMCDA document whose main tag is alternativesComparisons. For each type of comparison, a separate alternativesComparisons tag should be used. Within these groups aforementioned types are denoted using a comparisonType tag by respectively strict, weak, and indif label. Comparisons should be provided as pairs of two elementary sets of alternatives ids. The following form is expected:

The input value should be a valid XMCDA document whose main tag is ``<alternativesComparisons>``.
It must have the following form::

   
                
                    <alternativesComparisons>
                        <comparisonType>
                            %1<!-- type of preference: strong, weak, or indif -->
                        </comparisonType>
                        <pairs>
                            <pair>
                                <initial>
                                    <alternativesSet>
                                        <element>
                                            <alternativeID>%2</alternativeID>
                                        </element>
                                        <element>
                                            <alternativeID>%3</alternativeID>
                                        </element>
                                    </alternativesSet>
                                </initial>
                                <terminal>
                                    <alternativesSet>
                                        <element>
                                            <alternativeID>%4</alternativeID>
                                        </element>
                                        <element>
                                            <alternativeID>%5</alternativeID>
                                        </element>
                                    </alternativesSet>
                                </terminal>
                                <value>
                                    <label>%6</label>
                                </value>
                            </pair>
                        </pairs>
                    </alternativesComparisons>
                    
            


------------------------


.. _RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical-PUT-target:

target
~~~~~~

Description of the target to achieve which has a form of the worst possible position that may be attained by the particular alternative. The input value should be provided as a valid XMCDA document whose main tag is alternativeValue. It should contain an alternative Id and an integer value which denotes the desired ranking position.

The input value should be a valid XMCDA document whose main tag is ``<alternativeValue>``.
It must have the following form::

   
                
                    <alternativeValue>
                        <alternativeID>%1</alternativeID>
                        <value><integer>%2</integer></value>
                    </alternativeValue>
                    
            


------------------------


.. _RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical-PUT-rank-related-requirements:

rank-related-requirements *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Set of rank-related requirements. In other words it is a set of  ranges of possible positions in the final ranking for a chosen alternatives. The label values linked to the alternatives, determines an ids of nodes in the hierarchy of criteria tree. If value is not given or if it is equal to 0 rank related requirement is assumed to concern for the whole set of criteria, Otherwise, the preference relation applies only for a particular node. The input value should be provided as a valid XMCDA document whose main tag is alternativesValues. Each requirement should contain both an id of the reffered alternative and pair of values that denote the desired range. This information should be provided within a separate alternativesValue tag.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.
It must have the following form::

   
                
                    <alternativesValues>
                        <alternativeValue>
                            <alternativeID>%1</alternativeID>
                            <value>
                                <interval>
                                    <lowerBound><integer>%2</integer></lowerBound>
                                    <upperBound><integer>%3</integer></upperBound>
                                </interval>
                            </value>
                            <value>
                                <label>%4</label>
                            </value>
                        </alternativeValue>
                    </alternativesValues>
                    
            


------------------------


.. _RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical-PUT-parameters:

parameters
~~~~~~~~~~

Method parameters
                strict %1 - Single boolean value. Determines whether to use sctrictly increasing (true) or monotonously increasing (false) value functions.
                type_of_result %2 - Determines whether analyse the modification of performances or comprehensive score.
                precision %3 - A float value between 0 and 1 (bounds excluded). It describes the precision of expected result.
                possible_or_necessary %4 - One of the two label values (possible or necessary). This parameter determines does target should be satisfied by one (possible) or all(necessary) compatible value functions.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
            
                 <methodParameters>
                    <parameter name="strict">
                        <value>
                            <boolean>%1</boolean>
                        </value>
                    </parameter>
                    <parameter name="type_of_result">
                        <value><label>%2</label></value>
                    </parameter>
                    <parameter name="precision">
                        <value>
                            <real>%3</real>
                        </value>
                    </parameter>
                    <parameter name="possible_or_necessary">
                        <value><label>%4</label></value>
                    </parameter>
                </methodParameters>
            
            

where:

- **%1** is a parameter named "Use strictly increasing value functions?". This is a boolean.
  The default value is false.

- **%2** is a parameter named "type of result". It can have the following values:

  - ``improvement``: improvement

  - ``missing-value``: missing-value

  The default value is improvement.

- **%3** is a parameter named "precision of the result". This is a float, and the value should conform to the following constraint: A float value between 0 and 1 (bounds excluded). It denotes the expected precision of the result..  More formally, the constraint is::

      ((%3 > 0) && (%3 < 1)) 
  The default value is 0.005.

- **%4** is a parameter named "Whether the target should be achieved possibly or necessarily?". It can have the following values:

  - ``possible``: possible

  - ``necessary``: necessary

  The default value is necessary.


------------------------



.. _RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical-PUT_outputs:

Outputs
-------


- :ref:`result <RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical-PUT-result>`
- :ref:`messages <RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical-PUT-messages>`


.. _RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical-PUT-result:

result
~~~~~~

A set of real values associated with the id of the considered alternative.  Depending on chosen option, these values may denotes the improvements of performances of the alternative or the missing values of its comprehensive score. The output value should be provided as a valid XMCDA document whose main tag is alternativesValues. Each alternatives values group describes another node of the hierarchy tree. The id attributes denotes the ids of considered nodes.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   
                
                    <alternativesValues id=%1>
                        <alternativeValue>
                            <alternativeID>[...]</alternativeID>
                            <value>
                                <real>[...]</real>
                            </value>
                         </alternativeValue>
                    </alternativesValues>
                    
            


------------------------


.. _RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical-PUT-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical-PUT/description-wsDD.xml>`
