.. _defuzzificationOrdinal-URV:

defuzzificationOrdinal
======================

:Version: 1.0
:Provider: URV
:SOAP service's name: ``defuzzificationOrdinal-URV`` (see :ref:`soap-requests` for details)

Description
-----------

Implementation of a defuzzification of set of fuzzy labels according to their position. The first label is assigned the position 0.0, the second the position 1.0, and so on. No interpretation based on the fuzzy membership function is done in this method. Given a set of alternatives that are associated to a linguistic fuzzy term (alternativeValues file) a translation to the corresponding numerical value is made.

**Contact:** Aida Valls <aida.valls@urv.cat>


Inputs
------
(For outputs, see :ref:`below <defuzzificationOrdinal-URV_outputs>`)


- :ref:`linguisticScores <defuzzificationOrdinal-URV-input0>`
- :ref:`fuzzyNumbers <defuzzificationOrdinal-URV-input1>`


.. _defuzzificationOrdinal-URV-input0:

linguisticScores
~~~~~~~~~~~~~~~~

Linguistic label describing the performance of each alternative

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.
It must have the following form::

   
		  
			<alternativesValues>
			  <alternativeValue>
                                <alternativeID>
                                        identifier
                                </alernativeID>
				<value>
					<label> ... </label>
				</value>
			  </alternativeValue>
			
                          [... list of other alternatives with id and label ]

			</alternativesValues>
		  
		  


------------------------


.. _defuzzificationOrdinal-URV-input1:

fuzzyNumbers
~~~~~~~~~~~~

Definition of a fuzzy variable as a list of labels (trapezoidal)

The input value should be a valid XMCDA document whose main tag is ``<categoriesValues>``.
It must have the following form::

   
                    
			<categoriesValue>
			<categoryValue>
			   <values>
				<value id="xxx" name="xxxxxxxx">
						<fuzzyNumber>
							<trapezoidal>
								<point1>
									<abscissa>[...]</abscissa>
									<ordinate>[...]</ordinate>
								</point1>
								[...]
								<point4>
									[...]
								</point4>
							</trapezoidal>
						</fuzzyNumber>
				</value>
			    	[...]
        	            </values>
			</categoryValue>
			</categoriesValue>
                    
                     


------------------------



.. _defuzzificationOrdinal-URV_outputs:

Outputs
-------


- :ref:`alternativesValues <defuzzificationOrdinal-URV-output0>`
- :ref:`messages <defuzzificationOrdinal-URV-output1>`


.. _defuzzificationOrdinal-URV-output0:

alternativesValues
~~~~~~~~~~~~~~~~~~

Numerical score for each alternative according to the input fuzzy label.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _defuzzificationOrdinal-URV-output1:

messages
~~~~~~~~

A status message.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/defuzzificationOrdinal-URV/description-wsDD.xml>`
