.. _ElectreDiscordances-J-MCDA:

ElectreDiscordances
===================

:Version: 0.5.5
:Provider: J-MCDA
:SOAP service's name: ``ElectreDiscordances-J-MCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Computes a discordance relation per criteria.

**Contact:** Olivier Cailloux <olivier.cailloux@ecp.fr>

**Web page:** http://sourceforge.net/projects/j-mcda/

**Reference:** Cailloux, Olivier. Electre and Promethee MCDA methods as reusable software components. In Proceedings of the 25th Mini-EURO Conference on Uncertainty and Robustness in Planning and Decision Making (URPDM 2010). Coimbra, Portugal, 2010.


Inputs
------
(For outputs, see :ref:`below <ElectreDiscordances-J-MCDA_outputs>`)


- :ref:`criteria <ElectreDiscordances-J-MCDA-input1>`
- :ref:`alternatives <ElectreDiscordances-J-MCDA-input0>` *(optional)*
- :ref:`performances <ElectreDiscordances-J-MCDA-input2>`


.. _ElectreDiscordances-J-MCDA-input1:

criteria
~~~~~~~~

The criteria to consider, possibly with preference and veto thresholds. Each one must have a preference direction. Set some criteria as inactive (or remove them) to ignore them.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _ElectreDiscordances-J-MCDA-input0:

alternatives *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~

The alternatives to consider. Set some alternatives as inactive (or remove them) to ignore them.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _ElectreDiscordances-J-MCDA-input2:

performances
~~~~~~~~~~~~

The performances of the alternatives on the criteria to consider.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------



.. _ElectreDiscordances-J-MCDA_outputs:

Outputs
-------


- :ref:`discordances <ElectreDiscordances-J-MCDA-output0>`
- :ref:`messages <ElectreDiscordances-J-MCDA-output1>`


.. _ElectreDiscordances-J-MCDA-output0:

discordances
~~~~~~~~~~~~

The discordances relations (one relation per criterion) computed from the given input data.

The returned value is a XMCDA document whose main tag is ``<alternativesComparisons>``.


------------------------


.. _ElectreDiscordances-J-MCDA-output1:

messages
~~~~~~~~

A status message.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/ElectreDiscordances-J-MCDA/description-wsDD.xml>`
