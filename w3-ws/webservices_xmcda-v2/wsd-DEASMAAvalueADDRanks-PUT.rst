.. _DEASMAAvalueADDRanks-PUT:

DEASMAAvalueADDRanks
====================

:Version: 1.0
:Provider: PUT
:SOAP service's name: ``DEASMAAvalueADDRanks-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Computes rankings for the given DMUs (alternatives) using SMAA-D method and additive Data Envelopment Analysis Model. For given number of samples  returns a matrix with alternatives in each row and rankings in each column. Single cell indicates how many samples of respective alternative gave respective position in ranking.

**Contact:** Malgorzata Napieraj (napieraj.malgorzata@gmail.com)

**Reference:** Gouveia M. C., Dias L. C., Antunes C. H., Additive DEA based on MCDA with imprecise information (2008).
			Lahdelma R., Salminen P., Stochastic multicriteria acceptability analysis using the data envelopment model (2004).


Inputs
------
(For outputs, see :ref:`below <DEASMAAvalueADDRanks-PUT_outputs>`)


- :ref:`inputsOutputs <DEASMAAvalueADDRanks-PUT-inputsOutputs>`
- :ref:`units <DEASMAAvalueADDRanks-PUT-units>`
- :ref:`performanceTable <DEASMAAvalueADDRanks-PUT-performanceTable>`
- :ref:`weightsLinearConstraints <DEASMAAvalueADDRanks-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <DEASMAAvalueADDRanks-PUT-methodParameters>`


.. _DEASMAAvalueADDRanks-PUT-inputsOutputs:

inputsOutputs
~~~~~~~~~~~~~

A list of criteria with specified preference direction. List has to contains at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output).

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   <criteria>
                        <criterion>
							<scale>
								<quantitative>
									<preferenceDirection>
										[...]
									</preferenceDirection>
								</quantitative>
							</scale>
                            [...]
                        </criterion>
                        [...]
                    </criteria>


------------------------


.. _DEASMAAvalueADDRanks-PUT-units:

units
~~~~~

A list of alternatives (DMUs).

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>


------------------------


.. _DEASMAAvalueADDRanks-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A list of alternatives (DMUs) performances.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   <performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>


------------------------


.. _DEASMAAvalueADDRanks-PUT-weightsLinearConstraints:

weightsLinearConstraints *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of criteria linear constraints.

The input value should be a valid XMCDA document whose main tag is ``<criteriaLinearConstraints>``.
It must have the following form::

   <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID>[...]</criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>


------------------------


.. _DEASMAAvalueADDRanks-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~

"boundariesProvided" indicates if criteria values boundaries are given in criteria input or they have to be computed based on alternatives performances. "samplesNo" represents the number of samples to generate. "transformToUtilites" indicates if given alternatives values are utilities or if they have to be firstly transformed to utilities.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   <methodParameters>
							<parameter name="boundariesProvided">
								<value><boolean>%1</boolean></value>
							</parameter>
							<parameter name="samplesNo">
								<value><integer>%2</integer></value>
							</parameter>
							<parameter name="transformToUtilities">
								<value><boolean>%3</boolean></value>
							</parameter>
					</methodParameters>

where:

- **%1** is a parameter named "boundariesProvided". This is a boolean.
  The default value is false.

- **%2** is a parameter named "samplesNo". This is a int, and the value should conform to the following constraint: The value should be a positive integer..  More formally, the constraint is::

      %2 > 0 
  The default value is 100.

- **%3** is a parameter named "transformToUtilities". This is a boolean.
  The default value is true.


------------------------



.. _DEASMAAvalueADDRanks-PUT_outputs:

Outputs
-------


- :ref:`rankAcceptabilityIndices <DEASMAAvalueADDRanks-PUT-rankAcceptabilityIndices>`
- :ref:`avgRank <DEASMAAvalueADDRanks-PUT-avgRank>`
- :ref:`messages <DEASMAAvalueADDRanks-PUT-messages>`


.. _DEASMAAvalueADDRanks-PUT-rankAcceptabilityIndices:

rankAcceptabilityIndices
~~~~~~~~~~~~~~~~~~~~~~~~

A performance table for given alternatives. Single performance consists of attribute criterionID representing certain ranking, and a value representing ratio of samples attaining this ranking.

The returned value is a XMCDA document whose main tag is ``<performanceTable>``.
It has the following form::

   <performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>Rank [...]</criterionID>
									<value>
									[...]
									</value>
							</performance>
							[...]
						</alternativePerformances>
					</performanceTable>


------------------------


.. _DEASMAAvalueADDRanks-PUT-avgRank:

avgRank
~~~~~~~

A list of alternatives with average rank (obtained for given sample).

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   <alternativesValues mcdaConcept="efficiency">
						<alternativeValue>
						  <alternativeID>[...]</alternativeID>
						  <value>
							[...]
						  </value>
						</alternativeValue>
						[...]
					</alternativesValues>


------------------------


.. _DEASMAAvalueADDRanks-PUT-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/DEASMAAvalueADDRanks-PUT/description-wsDD.xml>`
