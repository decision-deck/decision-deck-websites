.. _HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT:

HierarchicalDEA-ValueAdditive-SMAA_ranks
========================================

:Version: 1.0
:Provider: PUT
:SOAP service's name: ``HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Computes ranks for the given DMUs (alternatives) using SMAA-D method and Additive Data Envelopment Analysis Model 
        with hierarchical structure of inputs and outputs. For given number of samples  returns a matrix with alternatives in each row and rankings in each column. Single cell indicates how many samples of respective alternative gave respective position in ranking.

**Contact:** 
            Anna Labijak <anna.labijak@cs.put.poznan.pl>
        


Inputs
------
(For outputs, see :ref:`below <HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT_outputs>`)


- :ref:`units <HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-units>`
- :ref:`performanceTable <HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-performanceTable>`
- :ref:`inputsOutputs <HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-inputsOutputs>`
- :ref:`hierarchy <HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-hierarchy>`
- :ref:`weightsLinearConstraints <HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-methodParameters>`


.. _HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-units:

units
~~~~~

A list of alternatives (DMUs).

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
                <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
            


------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A list of alternatives (DMUs) performances (on leaf hierarchy criteria).

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   
                <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>
            


------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-inputsOutputs:

inputsOutputs
~~~~~~~~~~~~~

A list of performance criteria (hierarchy leafs) and their preference direction. List has to contain at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output) for each hierarchy category.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
                <criteria>
                        <criterion>
							<scale>
                                [...]
							</scale>
                            [...]
                        </criterion>
                        [...]
                    </criteria>
            


------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-hierarchy:

hierarchy
~~~~~~~~~

The hierarchical structure of criteria.

The input value should be a valid XMCDA document whose main tag is ``<hierarchy>``.
It must have the following form::

   
                
                <hierarchy>
                    <node>
                        <criterionID>[...]</criterionID>
                        <node>
                            <criterionID>[...]</criterionID>
                            <node>
                                [...]
                            </node>
                            [...]
                        </node>
                        [...]
                    </node>
                </hierarchy>
            


------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-weightsLinearConstraints:

weightsLinearConstraints *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of hierarchy criteria linear constraints.

The input value should be a valid XMCDA document whose main tag is ``<criteriaLinearConstraints>``.
It must have the following form::

   
                <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID> [...] </criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>
            


------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~

Represents parameters.
            "number of samples" represents the number of samples to generate;
            "hierarchy node" is the ID of the hierarchy criterion for which the analysis should be performed;
            "transformToUtilities" means if data should be tranformed into values from range [0-1];
            "boundariesProvided" means if inputsOutputs file contains information about min and max data for each factor.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                
	<methodParameters>
		<parameter id="samplesNb">
			<value><integer>%1</integer></value>
		</parameter>
		<parameter id="hierarchyNode">
            <value><label>%2</label></value>
        </parameter>
		<parameter id="transformToUtilities">
			<value><boolean>%3</boolean></value>
		</parameter>
		<parameter id="boundariesProvided">
			<value><boolean>%4</boolean></value>
		</parameter>
        <parameter id="randomSeed">
			<value><integer>%5</integer></value>
		</parameter>
	</methodParameters>

            

where:

- **%1** is a parameter named "number of samples". This is a int, and the value should conform to the following constraint: The value should be a positive integer..  More formally, the constraint is::

     
                            %1 > 0
                        
  The default value is 100.

- **%2** is a parameter named "hierarchy node". This is a string.
  The default value is root.

- **%3** is a parameter named "transform to utilities". This is a boolean.
  The default value is true.

- **%4** is a parameter named "boundaries provided". This is a boolean.
  The default value is false.

- **%5** is a parameter named "random seed (-1 for default time-based seed)". This is a int, and the value should conform to the following constraint: The value should be a non-negative integer or -1 if no constant seed required..  More formally, the constraint is::

     
                            %5 >= -1
                        
  The default value is -1.


------------------------



.. _HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT_outputs:

Outputs
-------


- :ref:`rankAcceptabilityIndices <HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-rankAcceptabilityIndices>`
- :ref:`avgRank <HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-avgRank>`
- :ref:`messages <HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-messages>`


.. _HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-rankAcceptabilityIndices:

rankAcceptabilityIndices
~~~~~~~~~~~~~~~~~~~~~~~~

A performance table for given alternatives. Single performance consists of attribute criterionID representing certain ranking, and a value representing ratio of samples attaining this ranking.

The returned value is a XMCDA document whose main tag is ``<performanceTable>``.
It has the following form::

   
                <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID> Rank [...]</criterionID>
									<value>
									[...]
									</value>
							</performance>
							[...]
						</alternativePerformances>
					</performanceTable>
            


------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-avgRank:

avgRank
~~~~~~~

A list of alternatives with average rank (obtained for given sample).

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   
                <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
						  <values>
							  <value>
                [...]
                </value>
						  </values>
						</alternativeValue>
						[...]
					</alternativesValues>
            


------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT/description-wsDD.xml>`
