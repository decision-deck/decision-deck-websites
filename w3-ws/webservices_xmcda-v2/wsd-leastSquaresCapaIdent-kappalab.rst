.. _leastSquaresCapaIdent-kappalab:

leastSquaresCapaIdent
=====================

:Version: 1.0
:Provider: kappalab
:SOAP service's name: ``leastSquaresCapaIdent-kappalab`` (see :ref:`soap-requests` for details)

Description
-----------

Identifies a Mobius capacity by means of an approach grounded on least squares optimization. More precisely, given a set of overall values for each alternative, and possibly additional linear constraints expressing preferences, importance of criteria, etc., this algorithm determines, if it exists, a capacity minimising the sum of squared errors between overall scores as given by the data and the output of the Choquet integral for those data, and compatible with the additional linear constraints. The existence is ensured if no additional constraint is given. The problem is solved using quadratic programming.

**Contact:** Patrick Meyer (patrick.meyer@telecom-bretagne.eu)

**Reference:** J-L. Marichal and M. Roubens (2000), Determination of weights of interacting criteria from a reference set, European Journal of Operational Research 124, pages 641-650. 


Inputs
------
(For outputs, see :ref:`below <leastSquaresCapaIdent-kappalab_outputs>`)


- :ref:`criteria <leastSquaresCapaIdent-kappalab-criteria>`
- :ref:`alternatives <leastSquaresCapaIdent-kappalab-alternatives>`
- :ref:`performanceTable <leastSquaresCapaIdent-kappalab-performanceTable>`
- :ref:`shapleyPreorder <leastSquaresCapaIdent-kappalab-shapleyPreorder>` *(optional)*
- :ref:`interactionPreorder <leastSquaresCapaIdent-kappalab-interactionPreorder>` *(optional)*
- :ref:`shapleyInterval <leastSquaresCapaIdent-kappalab-shapleyInterval>` *(optional)*
- :ref:`interactionInterval <leastSquaresCapaIdent-kappalab-interactionInterval>` *(optional)*
- :ref:`overallValues <leastSquaresCapaIdent-kappalab-overallValues>`
- :ref:`kAdditivity <leastSquaresCapaIdent-kappalab-kAdditivity>`


.. _leastSquaresCapaIdent-kappalab-criteria:

criteria
~~~~~~~~

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
                
                    <criteria>
                        <criterion>
                            <active>[...]</active>
                            [...]
                        </criterion>
                        [...]
                    </criteria>
                    
            


------------------------


.. _leastSquaresCapaIdent-kappalab-alternatives:

alternatives
~~~~~~~~~~~~

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
                
                    <alternatives>
                        <alternative>
                            <active>[...]</active>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
                    
            


------------------------


.. _leastSquaresCapaIdent-kappalab-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A performance table. The evaluations should be numeric values, i.e. <real>, <integer> or <rational>.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _leastSquaresCapaIdent-kappalab-shapleyPreorder:

shapleyPreorder *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A valued relation on criteria expressing importance constraints on the critera. A numeric <value> indicates a minimal preference threshold for each <pair>. One <pair> represents an affirmation of the type "the Shapley importance index of criterion g1 is greater than the Shapley importance index of criterion g2 with preference threshold delta".

The input value should be a valid XMCDA document whose main tag is ``<criteriaComparisons>``.


------------------------


.. _leastSquaresCapaIdent-kappalab-interactionPreorder:

interactionPreorder *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A valued relation on pairs of criteria expressing constraints on value of the the Shapley interaction index. A numeric <value> indicates a minimal preference threshold for each <pair> of the relation. One <pair> represents a constraint of the type "the Shapley interaction index of the pair (g1,g2) of criteria is greater than the Shapley interaction index of the pair (g3,g4) of criteria with preference threshold delta".

The input value should be a valid XMCDA document whose main tag is ``<criteriaComparisons>``.


------------------------


.. _leastSquaresCapaIdent-kappalab-shapleyInterval:

shapleyInterval *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of <criterionValue> containing the constraints relative to the quantitative importance of the criteria. Each <criterionValue> contains an an <interval>. Each <criteriaValue> represents an affirmation of the type "the Shapley importance index of criterion g1 lies in the interval [a,b]".

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _leastSquaresCapaIdent-kappalab-interactionInterval:

interactionInterval *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of <criterionValue> containing the constraints relative to the type and the magnitude of the Shapley interaction index for pairs of criteria. Each <criterionValue> contains an an <interval>. Each <criteriaValue> represents an affirmation of the type "the Shapley interaction index of the pair (g1,g2) of criteria lies in the interval [a,b]".

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _leastSquaresCapaIdent-kappalab-overallValues:

overallValues
~~~~~~~~~~~~~

A list of <alternativeValue> containing the desired overall value of each alternative. The <value> should be a numeric value.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _leastSquaresCapaIdent-kappalab-kAdditivity:

kAdditivity
~~~~~~~~~~~

Indicates the level of k-additivity of the Mobius capacity (the Mobius transform of subsets whose cardinal is superior to k vanishes).

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                
                    <methodParameters>
                        <parameter
                             name="kAdditivity"> <!-- REQUIRED  -->
                            <value>
                                <integer>%1</integer>
                            </value>
                        </parameter>
                    </methodParameters>
                    
            

where:

- **%1** is a parameter named "kAdditivity". This is a int, and the value should conform to the following constraint: The value should be a positive integer, less than or equal to the number of criteria..  More formally, the constraint is::

      %1 > 0 
  The default value is 1.


------------------------



.. _leastSquaresCapaIdent-kappalab_outputs:

Outputs
-------


- :ref:`mobiusCapacity <leastSquaresCapaIdent-kappalab-mobiusCapacity>`
- :ref:`residuals <leastSquaresCapaIdent-kappalab-residuals>`
- :ref:`messages <leastSquaresCapaIdent-kappalab-messages>`


.. _leastSquaresCapaIdent-kappalab-mobiusCapacity:

mobiusCapacity
~~~~~~~~~~~~~~

The Mobius transform of a capacity.

The returned value is a XMCDA document whose main tag is ``<criteriaValues>``.
It has the following form::

   
                
                    <criteriaValues mcdaConcept="mobiusCapacity">
                        <criterionValue>
                            <criteriaSet>
                                <element>
                                    <criterionID>[...]</criterionID>
                                </element>
                                [...]
                            </criteriaSet>
                            <value>
                                <real>[...]</real>
                            </value>
                        </criterionValue>
                        [...]
                    </criteriaValues>
                    
            


------------------------


.. _leastSquaresCapaIdent-kappalab-residuals:

residuals
~~~~~~~~~

A list of <alternativeValue> representing the difference between the provided overall evaluations and those returned by the obtained model, for each alternative.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   
                
                    <alternativesValues mcdaConcept="mobiusCapacity">
                        <alternativeValue>
                            [...]
                        </alternativeValue>
                        [...]
                    </alternativesValues>
                    
            


------------------------


.. _leastSquaresCapaIdent-kappalab-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/leastSquaresCapaIdent-kappalab/description-wsDD.xml>`
