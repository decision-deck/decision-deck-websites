.. _plotAlternativesAssignments-ITTB:

plotAlternativesAssignments
===========================

:Version: 1.1
:Provider: ITTB
:SOAP service's name: ``plotAlternativesAssignments-ITTB`` (see :ref:`soap-requests` for details)

Description
-----------

This web service generates a plot representing the alternatives assignments. Colors can be used. You can specify how to display the different categories: by line, by column or in a grid. The plots can also be ordered: alphabetical order, its inverse or by categories.

**Contact:** Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)


Inputs
------
(For outputs, see :ref:`below <plotAlternativesAssignments-ITTB_outputs>`)


- :ref:`alternatives <plotAlternativesAssignments-ITTB-alternatives>`
- :ref:`categories <plotAlternativesAssignments-ITTB-categories>` *(optional)*
- :ref:`alternativesAffectations <plotAlternativesAssignments-ITTB-alternativesAffectations>`
- :ref:`options <plotAlternativesAssignments-ITTB-options>`


.. _plotAlternativesAssignments-ITTB-alternatives:

alternatives
~~~~~~~~~~~~

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   

                    <alternatives>
                        <alternative>
                            <active>[...]</active>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>

               


------------------------


.. _plotAlternativesAssignments-ITTB-categories:

categories *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~

The list of categories and their rank (required!).

The input value should be a valid XMCDA document whose main tag is ``<categories>``.
It must have the following form::

   

                    <categories>
                        <category id=[...]>
                            <active>[...]</active>
                            <rank>
								<integer>[...]</integer>
							</rank>
                        </category>
                        [...]
                    </categories>

               


------------------------


.. _plotAlternativesAssignments-ITTB-alternativesAffectations:

alternativesAffectations
~~~~~~~~~~~~~~~~~~~~~~~~

Assignment of reference alternatives

The input value should be a valid XMCDA document whose main tag is ``<alternativesAffectations>``.
It must have the following form::

   

                    <alternativesAffectations>
                        <alternativeAffectation>
                           <alternativeID>[...]</alternativeID>
                           <categoryID>[...]</categoryID>
                       </alternativeAffectation>
                        [...]
                      </alternativesAffectations>

               


------------------------


.. _plotAlternativesAssignments-ITTB-options:

options
~~~~~~~

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                   <methodParameters>
		<parameter id="plot_title" name="Unique plot">
			<value>
				<label>%2</label>
			</value>
		</parameter>
		<parameter id="unique_plot" name="Unique plot">
			<value>
				<label>%3</label>
			</value>
		</parameter>
		<parameter id="plots_display" name="Plots' display">
			<value>
				<label>%4</label>
			</value>
		</parameter>
		<parameter id="order" name="Order">
			<value>
				<label>%5</label>
			</value>
		</parameter>
		<parameter id="use_color" name="Colors in the plots">
			<value>
				<label>%6</label>
			 </value>
		</parameter>
		<parameter id="selected_color" name="Selected color">
			<value>
				<label>%7</label>
			 </value>
			 </parameter>
		<parameter id="categories_shape" name="Categories shape">
			<value>
				<label>%8</label>
			</value>
		</parameter>
	</methodParameters>
               

where:

- **%2** is a parameter named "Plot title:". This is a string.
- **%3** is a parameter named "Number of images". It can have the following values:

  - ``true``: 1 per plot

  - ``false``: Multiple images

  The default value is true.

- **%4** is a parameter named "Plots arrangement". It can have the following values:

  - ``Column``: Column

  - ``Line``: Line

  - ``Grid``: Grid

  The default value is column.

- **%5** is a parameter named "Order:". It can have the following values:

  - ``increasing``: increasing

  - ``decreasing``: decreasing

  - ``categories``: categories

  The default value is increasing.

- **%6** is a parameter named "Use Colors?". It can have the following values:

  - ``true``: Yes

  - ``false``: No

  The default value is false.

- **%7** is a parameter named "Choose color:". It can have the following values:

  - ``Black``: Black

  - ``Red``: Red

  - ``Blue``: Blue

  - ``Green``: Green

  - ``Yellow``: Yellow

  - ``Magenta``: Magenta

  - ``Cyan``: Cyan

  The default value is black.

- **%8** is a parameter named "Plot alternatives in:". It can have the following values:

  - ``rectangle``: rectangle

  - ``oval``: oval

  - ``diamond``: diamond

  The default value is oval.


------------------------



.. _plotAlternativesAssignments-ITTB_outputs:

Outputs
-------


- :ref:`alternativesAffectationsPlot <plotAlternativesAssignments-ITTB-alternativesAffectationsPlot>`
- :ref:`messages <plotAlternativesAssignments-ITTB-messages>`


.. _plotAlternativesAssignments-ITTB-alternativesAffectationsPlot:

alternativesAffectationsPlot
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A string containing the base64 representation of the png image of the generated plot.

The returned value is a XMCDA document whose main tag is ``<alternativeValue>``.


------------------------


.. _plotAlternativesAssignments-ITTB-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/plotAlternativesAssignments-ITTB/description-wsDD.xml>`
