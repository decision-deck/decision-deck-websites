.. _DEAvalueADDEfficiency-PUT:

DEAvalueADDEfficiency
=====================

:Version: 1.0
:Provider: PUT
:SOAP service's name: ``DEAvalueADDEfficiency-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Computes efficiency scores for the given DMUs (alternatives) using additive Data Envelopment Analysis Model.

**Contact:** Malgorzata Napieraj (napieraj.malgorzata@gmail.com)

**Reference:** Gouveia M. C., Dias L. C., Antunes C. H., Additive DEA based on MCDA with imprecise information (2008).


Inputs
------
(For outputs, see :ref:`below <DEAvalueADDEfficiency-PUT_outputs>`)


- :ref:`inputsOutputs <DEAvalueADDEfficiency-PUT-inputsOutputs>`
- :ref:`units <DEAvalueADDEfficiency-PUT-units>`
- :ref:`performanceTable <DEAvalueADDEfficiency-PUT-performanceTable>`
- :ref:`weightsLinearConstraints <DEAvalueADDEfficiency-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <DEAvalueADDEfficiency-PUT-methodParameters>`


.. _DEAvalueADDEfficiency-PUT-inputsOutputs:

inputsOutputs
~~~~~~~~~~~~~

A list of criteria with specified preference direction. List has to contains at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output). In addition, minimum and maximum possible value for each critetion can be defined. If not defined, they will be computed from alternatives performances on given criteria.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   <criteria>
                        <criterion>
							<scale>
								<quantitative>
									<preferenceDirection>
										[...]
									</preferenceDirection>
								</quantitative>
							</scale>
							<minimum>[...]</minimum>
							<maximum>[...]</maximum>
                            [...]
                        </criterion>
                        [...]
                    </criteria>


------------------------


.. _DEAvalueADDEfficiency-PUT-units:

units
~~~~~

A list of alternatives (DMUs).

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>


------------------------


.. _DEAvalueADDEfficiency-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A list of alternatives (DMUs) performances.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   <performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>


------------------------


.. _DEAvalueADDEfficiency-PUT-weightsLinearConstraints:

weightsLinearConstraints *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of criteria linear constraints.

The input value should be a valid XMCDA document whose main tag is ``<criteriaLinearConstraints>``.
It must have the following form::

   <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID>[...]</criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>


------------------------


.. _DEAvalueADDEfficiency-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~

"boundariesProvided" indicates if criteria values boundaries are given in criteria input or they have to be computed based on alternatives performances. "transformToUtilites" indicates if given alternatives values are utilities or if they have to be firstly transformed to utilities.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   <methodParameters>
							<parameter name="boundariesProvided">
								<value><boolean>%1</boolean></value>
							</parameter>
							<parameter name="transformToUtilities">
								<value><boolean>%2</boolean></value>
							</parameter>
					</methodParameters>

where:

- **%1** is a parameter named "boundariesProvided". This is a boolean.
  The default value is false.

- **%2** is a parameter named "transformToUtilities". This is a boolean.
  The default value is true.


------------------------



.. _DEAvalueADDEfficiency-PUT_outputs:

Outputs
-------


- :ref:`efficiency <DEAvalueADDEfficiency-PUT-efficiency>`
- :ref:`distance <DEAvalueADDEfficiency-PUT-distance>`
- :ref:`messages <DEAvalueADDEfficiency-PUT-messages>`


.. _DEAvalueADDEfficiency-PUT-efficiency:

efficiency
~~~~~~~~~~

A list of alternatives with computed efficiency scores.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   <alternativesValues mcdaConcept="efficiency">
						<alternativeValue>
						  <alternativeID>[...]</alternativeID>
						  <value>
							[...]
						  </value>
						</alternativeValue>
						[...]
					</alternativesValues>


------------------------


.. _DEAvalueADDEfficiency-PUT-distance:

distance
~~~~~~~~

A list of alternatives with computed distances from efficient DMUs.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   <alternativesValues mcdaConcept="distance">
						<alternativeValue>
						  <alternativeID>[...]</alternativeID>
						  <value>
							[...]
						  </value>
						</alternativeValue>
						[...]
					</alternativesValues>


------------------------


.. _DEAvalueADDEfficiency-PUT-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/DEAvalueADDEfficiency-PUT/description-wsDD.xml>`
