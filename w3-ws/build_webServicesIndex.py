import os
ws_file_prefix = "wsd-"

def GetWSData():
    webservices = []
    for fname in os.listdir("./"):
        if fname.endswith(".rst") and fname.startswith(ws_file_prefix):
            # read input file
            fi = open("./" + fname, "r")
            lines = fi.read().splitlines()
            fi.close()
            ws = {}
            ws['filename'] = fname[:-4]
            nameparts = ws['filename'][len(ws_file_prefix):].split('-')
            if len(nameparts) == 1:
                ws['name'] = nameparts[0]
            else:
                ws['name'] = '-'.join(nameparts[:-1])
            ws['version'] = ''
            for line in lines:
                if line.startswith(':Version:'):
                    ws['version'] = line.split()[1]
                    break
            ws['provider'] = ''
            for line in lines:
                if line.startswith(':Provider:'):
                    ws['provider'] = line.split()[1]
                    break
            ws['contact'] = ''
            for line in lines:
                if line.startswith('**Contact:**'):
                    ws['contact'] = line[12:]
                    break
            ws['description'] = ''
            for i in range(len(lines)):
                if lines[i].startswith('Description'):
                    j = i + 1
                    if lines[j].startswith('-'):
                        j = j + 1
                    else:
                        ws['description'] = lines[j]
                        break
                    if len(lines[j]) == 0:
                        j = j + 1
                    else:
                        ws['description'] = lines[j]
                        break
                    if len(lines[j]) != 0:
                        ws['description'] = lines[j]
                    break
            webservices.append(ws)
    return webservices

def WritePage(webservices, webservicesnames):
    output = 'List of available XMCDA web services\n------------------------------------\n\n.. raw:: html\n\n'
    # add search field
    output += '    <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for...">\n'
    # add table header
    output += '    <table id="myTable">\n'
    output += '      <tr class="header">\n'
    output += '        <th style="width:30%;">Name</th>\n'
    output += '        <th style="width:10%;">Version</th>\n'
    output += '        <th style="width:15%;">Provider</th>\n'
    output += '        <th style="width:45%;">Description</th>\n'
    output += '      </tr>\n'
    # add table content
    for wsname in webservicesnames:
        for ws in webservices:
            if ws['name'].lower() == wsname:
                output += '      <tr>\n'
                output += '        <td>\n'
                output += '          <div><a href="' + ws['filename'] + '.html">' + ws['name'] + '</a></div>\n'
                output += '        </td>\n'
                output += '        <td>\n'
                output += '          <div>' + ws['version'] + '</div>\n'
                output += '        </td>\n'
                output += '        <td>\n'
                output += '          <div>' + ws['provider'] + '</div>\n'
                output += '        </td>\n'
                desc = ws['description']
                if len(desc) > 100:
                    desc = desc[:96] + ' ...'
                output += '        <td>\n'
                output += '          <div>' + desc + '</div>\n'
                output += '        </td>\n'
                output += '      </tr>\n'
    output += '    </table>\n'
    output += '    <script>\n'
    output += '    function myFunction() {\n'
    output += '      var input, filter, table, tr, td, i;\n'
    output += '      input = document.getElementById("myInput");\n'
    output += '      filter = input.value.toUpperCase();\n'
    output += '      table = document.getElementById("myTable");\n'
    output += '      tr = table.getElementsByTagName("tr");\n'
    output += '      for (i = 0; i < tr.length; i++) {\n'
    output += '        td = tr[i].getElementsByTagName("td")[0];\n'
    output += '        if (td) {\n'
    output += '          if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {\n'
    output += '            tr[i].style.display = "";\n'
    output += '          } else {\n'
    output += '            tr[i].style.display = "none";\n'
    output += '          }\n'
    output += '        }\n'
    output += '      }\n'
    output += '    }\n'
    output += '    </script>\n'
    # write file
    fo = open('./webServices.rst', "w")
    fo.write(output)
    fo.close()

# read wsd files and extract needed info
webservices = GetWSData()

# get all ws names
wsnames = []
for ws in webservices:
    if not ws['name'].lower() in wsnames:
        wsnames.append(ws['name'].lower())
wsnames.sort()

# write index for all ws
WritePage(webservices, wsnames)

