.. index:: features

Features
========

The following observations can be made among the Multiple Criteria Decision Aid researchers:

  - they are often not computer scientists;
  - they have programmed their algorithm(s) in the programming language they know best;
  - they are generally not interested in reimplementing their algorithm(s) in an imposed programming language.

Consquently, the Decision Deck project offers an architecture dedicated to web services whose primary goal is to offer a place where MCDA algorithms, methods etc. can be hosted and made freely available to anyone.

2 main consequences are related to such a choice:

  **Programming language independence:** 

    The Decision Deck's web services framework allows to integrate any executable (either a binary or a script) which can be run on a Linux operating system;

  **GUI-less:** 

    The focus is exclusively made on the algorithmic part of an MCDA algorithm. Softwares like Decision Deck's `diviz <//www.diviz.org/index.html>`_ can be used to conveniently access the XMCDA web services;






