.. _ws.program_requirements:

How to create an XMCDA *web service-able* program?
==================================================

.. toctree::
   :maxdepth: 2

Requirements for programs
-------------------------

In order to prepare for making a program available as a web service, the
following requirements must be met:

1. The program must understand `XMCDA <//www.decision-deck.org/xmcda>`_ files, meaning that it accepts them as
   inputs, and that it is able to save its output(s) conforming
   to this standard as well.  If your program does not fulfil this point, this
   is nevertheless probably **not** a no-go situation: please read the section
   named "Conforming to the XMCDA standard", below.

2. the executable (either a binary or a script) must be available on Linux.

   Most of the time, even if you are working on a different platform (with
   e.g. a Windows OS or Mac OS X), this will not be a problem.

   For a binary program, we need to be able to compile it on a
   Intel-based linux system; 32-bits and 64-bits program are both supported.

   For programs written in a scripting language (python, perl, ruby, shell
   scripts, etc.) we need to know every relevant details: version of the
   script engine, required librairies etc.

3. The program must be callable through the command-line (at least).  This
   means that even if it also offers a graphical interface, it can also be
   launched directly on the command-line (or, if your prefer, in so-called
   *batch scripts*) and that, in this mode, it does not expect any interaction
   *at all* with the user (all the informations needed is supposed to be
   present in the inputs supplied in the command-line).

If you are not sure, just contact us, we will be glad to help!


Conforming to the XMCDA Standard
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It is possible that your program does not read and/or save files in the XMCDA
format.  Hopefully, adapting it will not be too difficult.  We give here some
possibilities to follow up.

Adapting the program itself:

  The first option is to directly adapt your program.  For example, you will
  add a new option, let's say ``--xmcda``, switching it into "xmcda mode".


  Of course, this means that you should be able to read and write xmcda files.
  To help you with this task, you should check whether you can use an
  `existing library
  </xmcda/developers.html>`_.

Writing translators:

  An other option is to leave your program untouched, and to write translators
  that transforms XMCDA input and parameters files into your own format, and
  your own outputs back to XMCDA files.


  Here again you should check whether such translators are not already available for your purpose in the Decision Deck project.

Standardising the program
~~~~~~~~~~~~~~~~~~~~~~~~~

*If you follow the following prescriptions, the integration of your program into Decision Deck's web services environment should go without any major problems.*

As already mentionned, your program should read its input elements from files compliant with `XMCDA <//www.decision-deck.org/xmcda>`_. Similarly, the output elements should be written according to this standard. Besides, we have opted for the following architecture:

- multiple input and output files (one per main `XMCDA <//www.decision-deck.org/xmcda>`_ tag);

- 2 distinct directories for the input and the output files.

.. You may wonder why we have chosen these options. Recall that one of the objectives of the web services is to to allow to easily combine various MCDA algorithms by chaining them. To ease this, the inputs and outputs of the programs have to be typed. In the context of the Decision Deck project these datatypes are selected from the `XMCDA <//www.decision-deck.org/xmcda>`_ markup language.

We require that (at least) two options can be sent to your program via the command line:

1. the name of the input directory;

2. the name of the output directory.

**Input directory:**

  The **input directory** contains all the input files necessary for your program to work. As already mentionned, each input file is compliant with the `XMCDA <//www.decision-deck.org/xmcda>`_ standard, and should contain at most one of the main XMCDA tags. Each file can freely be named according to your definition of its content (mandatory extension: ``.xml``). We recommend to use a name which is understandable by the potential users of your web service.

    *Example*: Consider a program which requires as inputs the description of the criteria and the weights associated with the criteria. The designer of the program decides that the criteria description is stored in a file named **criteria.xml** and that the weights are stored in a file named **importance.xml**. **criteria.xml** is a valid XMCDA file containing a ``<criteria>`` tag, whereas **importance.xml** is a valid XMCDA file containing a ``<criteriaValues>`` tag.

**Output directory:**

  The **output directory** contains all the output files which are produced by your program. They are also compliant with `XMCDA <//www.decision-deck.org/xmcda>`_. We stronlgy recommend that each program produces always one output file named **messages.xml** containing the messages that it has generated. Such messages are: log messages, an execution status, error messages, ...

    *Example*: Reconsider the program of the previous example, which calculates the sum of the weights associated with the criteria. The designer of the program decides that in any case the program should generate a file named **messages.xml**, and in case of a successful execution, it should write the value of the sum in the file **criteriaSum.xml**. If an error occurs during the execution of the program, the second file is not written, and **messages.xml** contains an ``<errorMessage>`` tag (under the ``<methodMessages>`` tag) describing the error. In case of a successful execution of the program, **messages.xml** contains a ``<logMessage>`` tag (under the ``<methodMessages>`` tag) informing that everything went well, and **criteriaSum.xml** contains a ``<criterionValue>`` tag with the value of the sum of the weights of the criteria.

To summarise, your program should somehow be callable as follows from command line (the exact syntax is up to you):

  ::

  > program -i inputDirectory -o outputDirectory

where "program" is the command used to run your program, and "inputDirectory" and "outputDirectory" are the names of the input and output directories where the XMCDA files are (or will be) located.

Your program then examines the content of the input directory (passed as a parameter to the program) for the files, loads them, executes the algorithm, and writes its outputs to the output directory (passed as a parameter to the program).


*Up to now we have described how your program should be adapted to be compliant with the Decision Deck's web services framework. To be able to install your program as a web service, we however need some more information on the input and output elements.*

Describing the input and output data elements
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In order to inform us correctly on the input and output elements of your program in view of its integration in Decision Deck's web services framework (and a GUI), we propose to submit us this information in a file named **description-wsDD.xml** written according to a standard which is detailed :ref:`here<specification>`.

Note that the **description-wsDD.xml** file will also be used to generate a documentation of your web service (see examples in the :ref:`list<webServices>` of available web services).

.. index:: web service, XMCDA, specification.xml, program submission
