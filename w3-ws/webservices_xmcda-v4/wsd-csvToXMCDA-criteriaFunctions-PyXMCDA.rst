.. _csvToXMCDA-criteriaFunctions-PyXMCDA:

csvToXMCDA-criteriaFunctions
============================

:Version: 2.0
:Provider: PyXMCDA
:SOAP service's name: ``csvToXMCDA-criteriaFunctions-PyXMCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Transforms a file containing criteria functions from a comma-separated values (CSV) file to two XMCDA compliant files, containing the criteria ids and their names, and their criterion functions (points).

**Contact:** Sébastien Bigaret (sebastien.bigaret@telecom-bretagne.eu)

**Web page:** https://gitlab.com/sbigaret/ws-pyxmcda


Inputs
------
(For outputs, see :ref:`below <csvToXMCDA-criteriaFunctions-PyXMCDA_outputs>`)


- :ref:`criteriaFunctions.csv <csvToXMCDA-criteriaFunctions-PyXMCDA-criteriaFunctions_csv>`
- :ref:`parameters <csvToXMCDA-criteriaFunctions-PyXMCDA-parameters>`


.. _csvToXMCDA-criteriaFunctions-PyXMCDA-criteriaFunctions_csv:

criteriaFunctions.csv
~~~~~~~~~~~~~~~~~~~~~

The discrete criteria functions as a CSV file.  The discrete functions are defined by a series of point.

Example:

  g1,cost,0,0
  ,,21334,1
  g2,Acceleration,0,0
  ,,30.8,1
  g3,PickUp,0,0
  ,,41.6,1
  g4,Brakes,0,0
  ,,2.66,1
  g5,RoadHold,0,0
  ,,3.25,1

defines for criterion `g1` a discrete function with two points, (0,0) and (21334,1).

The input value should be a valid XMCDA document whose main tag is ``<other>``.


------------------------


.. _csvToXMCDA-criteriaFunctions-PyXMCDA-parameters:

parameters
~~~~~~~~~~

Parameters of the method

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
    <programParameters>
        <parameter id="csv_delimiter">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </parameter>
        <parameter id="default_prefix">
            <values>
                <value>
                    <label>%2</label>
                </value>
            </values>
        </parameter>
        <parameter id="name_in_id">
            <values>
                <value>
                    <boolean>%3</boolean>
                </value>
            </values>
        </parameter>
    </programParameters>


where:

- **%1** is a parameter named "CSV delimiter". This is a string, and the value should conform to the following constraint: One character maximum.  More formally, the constraint is::

     %1.length() < 2
- **%2** is a parameter named "Default content". It can have the following values:

  - ``float``: float

  - ``label``: string

  - ``integer``: integer

  - ``boolean``: boolean

  The default value is float.

- **%3** is a parameter named "First column". It can have the following values:

  - ``false``: id

  - ``true``: id (name)

  The default value is id_and_name.


------------------------



.. _csvToXMCDA-criteriaFunctions-PyXMCDA_outputs:

Outputs
-------


- :ref:`criteria <csvToXMCDA-criteriaFunctions-PyXMCDA-criteria>`
- :ref:`criteriaFunctions <csvToXMCDA-criteriaFunctions-PyXMCDA-criteriaFunctions>`
- :ref:`messages <csvToXMCDA-criteriaFunctions-PyXMCDA-messages>`


.. _csvToXMCDA-criteriaFunctions-PyXMCDA-criteria:

criteria
~~~~~~~~

The criteria.

The returned value is a XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _csvToXMCDA-criteriaFunctions-PyXMCDA-criteriaFunctions:

criteriaFunctions
~~~~~~~~~~~~~~~~~

The value functions.

The returned value is a XMCDA document whose main tag is ``<criteriaFunctions>``.


------------------------


.. _csvToXMCDA-criteriaFunctions-PyXMCDA-messages:

messages
~~~~~~~~

Status messages.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/csvToXMCDA-criteriaFunctions-PyXMCDA/description-wsDD.xml>`
