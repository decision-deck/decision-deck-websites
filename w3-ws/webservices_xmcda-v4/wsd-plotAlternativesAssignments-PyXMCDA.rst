:orphan:

.. _plotAlternativesAssignments-PyXMCDA:

plotAlternativesAssignments
===========================

:Version: 1.1
:Provider: PyXMCDA
:SOAP service's name: ``plotAlternativesAssignments-PyXMCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Generate plots representing provided alternativesAssignments as well as the dot files generating these plots.
Alternatives can be assigned to either a category, a categories set or a categories interval.
Graphviz dot program and format are used by this service.

**Contact:** Nicolas Duminy (nicolas.duminy@telecom-bretagne.eu)

**Web page:** https://gitlab.com/nduminy/ws-pyxmcda


Inputs
------
(For outputs, see :ref:`below <plotAlternativesAssignments-PyXMCDA_outputs>`)


- :ref:`alternatives <plotAlternativesAssignments-PyXMCDA-alternatives>` *(optional)*
- :ref:`categories <plotAlternativesAssignments-PyXMCDA-categories>` *(optional)*
- :ref:`categoriesSets <plotAlternativesAssignments-PyXMCDA-categoriesSets>` *(optional)*
- :ref:`alternativesAssignments <plotAlternativesAssignments-PyXMCDA-alternativesAssignments>`
- :ref:`categoriesValues <plotAlternativesAssignments-PyXMCDA-categoriesValues>` *(optional)*
- :ref:`parameters <plotAlternativesAssignments-PyXMCDA-parameters>` *(optional)*


.. _plotAlternativesAssignments-PyXMCDA-alternatives:

alternatives *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~

The alternatives to be plotted. All are plotted if not provided.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _plotAlternativesAssignments-PyXMCDA-categories:

categories *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~

The categories to be plotted. All are plotted if not provided.

The input value should be a valid XMCDA document whose main tag is ``<categories>``.


------------------------


.. _plotAlternativesAssignments-PyXMCDA-categoriesSets:

categoriesSets *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~

The categories sets assignable. Mandatory if alternatives are assigned to categoriesSets.

The input value should be a valid XMCDA document whose main tag is ``<categoriesSets>``.


------------------------


.. _plotAlternativesAssignments-PyXMCDA-alternativesAssignments:

alternativesAssignments
~~~~~~~~~~~~~~~~~~~~~~~

The alternatives ssignments to be plotted.

The input value should be a valid XMCDA document whose main tag is ``<alternativesAssignments>``.


------------------------


.. _plotAlternativesAssignments-PyXMCDA-categoriesValues:

categoriesValues *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The categories values ranking provided categories. Mandatory if alternatives are assigned to categoriesIntervals.
                Used to order categories in plot. Only read if categories are supplied.

The input value should be a valid XMCDA document whose main tag is ``<categoriesValues>``.


------------------------


.. _plotAlternativesAssignments-PyXMCDA-parameters:

parameters *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~

Parameters of the method

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
    <programParameters>
        <programParameter id="color" name="Color">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="shape" name="Shape">
            <values>
                <value>
                    <label>%2</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="image_file_extension" name="Image file extension">
            <values>
                <value>
                    <label>%3</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="layout" name="Layout">
            <values>
                <value>
                    <label>%4</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="show_values" name="Show values">
            <values>
                <value>
                    <boolean>%5</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="naming_conventions" name="Naming conventions">
            <values>
                <value>
                    <label>%6</label>
                </value>
            </values>
        </programParameter>
    </programParameters>


where:

- **%1** is a parameter named "Color". It can have the following values:

  - ``black``: Black

  - ``white``: White

  - ``red``: Red

  - ``green``: Green

  - ``blue``: Blue

  - ``magenta``: Magenta

  - ``yellow``: Yellow

  - ``cyan``: Cyan

  - ``orange``: Orange

  - ``pink``: Pink

  - ``brown``: Brown

  - ``gray``: Gray

  The default value is black.

- **%2** is a parameter named "Shape". It can have the following values:

  - ``box``: Rectangle

  - ``circle``: Circle

  - ``diamond``: Diamond

  - ``ellipse``: Ellipse

  - ``oval``: Oval

  - ``polygon``: Polygon

  - ``triangle``: Triangle

  The default value is oval.

- **%3** is a parameter named "Image file extension". It can have the following values:

  - ``bmp``: .bmp (Windows Bitmap Format)

  - ``dia``: .dia (DIA Format)

  - ``fig``: .fig (FIG graphics format)

  - ``gif``: .gif (Graphics Interchange Format)

  - ``hpgl``: .hpgl (Hewlett Packard Graphic Language 2)

  - ``ico``: .ico (Icon Image File Format)

  - ``jpg``: .jpg (Joint Photographic Experts Group)

  - ``jpe``: .jpe (Joint Photographic Experts Group)

  - ``pdf``: .pdf (Portable Document Format)

  - ``png``: .png (Portable Network Graphics)

  - ``ps``: .ps (PostScript)

  - ``ps2``: .ps2 (PostScript for PDF)

  - ``svg``: .svg (Scalable Vector Graphics)

  - ``svgz``: .svgz (Compressed Scalable Vector Graphics)

  - ``tif``: .tif (Tagged Image File Format)

  The default value is png.

- **%4** is a parameter named "Layout". It can have the following values:

  - ``line``: Line

  - ``column``: Column

  - ``grid``: Grid

  The default value is line.

- **%5** is a parameter named "Show values". This is a boolean.
  The default value is false.

- **%6** is a parameter named "Naming conventions". It can have the following values:

  - ``id``: Only ids are shown

  - ``name``: Only names are shown (disambiguated by appending the ids, if needed)

  - ``name (id)``: Names and ids are shown in that order

  - ``id (name)``: Ids and names are shown in that order

  The default value is name.


------------------------



.. _plotAlternativesAssignments-PyXMCDA_outputs:

Outputs
-------


- :ref:`glob:alternativesAssignments.{bmp,dia,fig,gif,hpgl,ico,jpg,jpe,pdf,png,ps,ps2,svg,svgz,tif} <plotAlternativesAssignments-PyXMCDA-alternativesAssignmentsPlot>`
- :ref:`alternativesAssignments.dot <plotAlternativesAssignments-PyXMCDA-alternativesAssignmentsPlotScript>`
- :ref:`messages <plotAlternativesAssignments-PyXMCDA-messages>`


.. _plotAlternativesAssignments-PyXMCDA-alternativesAssignmentsPlot:

glob:alternativesAssignments.{bmp,dia,fig,gif,hpgl,ico,jpg,jpe,pdf,png,ps,ps2,svg,svgz,tif}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Image containing all selected alternatives assignments plots. Format corresponds to the one given in parameters (default is .png).

The returned value is a XMCDA document whose main tag is ``<None>``.


------------------------


.. _plotAlternativesAssignments-PyXMCDA-alternativesAssignmentsPlotScript:

alternativesAssignments.dot
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Generated graphviz dot script that made the image. Given to enable users to later customize the appearance of the plots.

The returned value is a XMCDA document whose main tag is ``<None>``.


------------------------


.. _plotAlternativesAssignments-PyXMCDA-messages:

messages
~~~~~~~~

Status messages.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/plotAlternativesAssignments-PyXMCDA/description-wsDD.xml>`
