.. _plotClassAssignments-png-PUT:

plotClassAssignments-png
========================

:Version: 1.0.0
:Provider: PUT
:SOAP service's name: ``plotClassAssignments-png-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Visualizes class assignments for given alternatives as an image.

**Contact:** Maciej Uniejewski <maciej.uniejewski@gmail.com>

**Web page:** https://github.com/maciej7777/PrometheeDiviz


Inputs
------
(For outputs, see :ref:`below <plotClassAssignments-png-PUT_outputs>`)


- :ref:`alternatives <plotClassAssignments-png-PUT-input1>`
- :ref:`categories <plotClassAssignments-png-PUT-input2>`
- :ref:`assignments <plotClassAssignments-png-PUT-input4>`
- :ref:`categories_values <plotClassAssignments-png-PUT-input3>`
- :ref:`method_parameters <plotClassAssignments-png-PUT-input5>`


.. _plotClassAssignments-png-PUT-input1:

alternatives
~~~~~~~~~~~~

Alternatives to consider.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _plotClassAssignments-png-PUT-input2:

categories
~~~~~~~~~~

Definitions of categories.

The input value should be a valid XMCDA document whose main tag is ``<categories>``.


------------------------


.. _plotClassAssignments-png-PUT-input4:

assignments
~~~~~~~~~~~

Assignments of alternatives to given classes.

The input value should be a valid XMCDA document whose main tag is ``<alternativesAssignments>``.


------------------------


.. _plotClassAssignments-png-PUT-input3:

categories_values
~~~~~~~~~~~~~~~~~

Marks of categories (higher mark means better category). Each category need to have unique mark from 1 to C, where C is a number of categories.

The input value should be a valid XMCDA document whose main tag is ``<categoriesValues>``.


------------------------


.. _plotClassAssignments-png-PUT-input5:

method_parameters
~~~~~~~~~~~~~~~~~

A set of parameters provided to tune up the module's operation.

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
                
                <programParameters>
                    <parameter id="visualizationType">
                        <values>
                            <value>
                                <label>%1</label>
                            </value>
                        </values>
                    </parameter>
                </programParameters>
       
            

where:

- **%1** is a parameter named "Visualization type". It can have the following values:

  - ``normal``: normal

  - ``dashed``: dashed

  The default value is normal.


------------------------



.. _plotClassAssignments-png-PUT_outputs:

Outputs
-------


- :ref:`assignments.png <plotClassAssignments-png-PUT-assignments_png>`
- :ref:`messages <plotClassAssignments-png-PUT-output2>`


.. _plotClassAssignments-png-PUT-assignments_png:

assignments.png
~~~~~~~~~~~~~~~

Visualization of assignments as an image.

The returned value is a XMCDA document whose main tag is ``<None>``.


------------------------


.. _plotClassAssignments-png-PUT-output2:

messages
~~~~~~~~

Messages or errors generated by this module.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/plotClassAssignments-png-PUT/description-wsDD.xml>`
