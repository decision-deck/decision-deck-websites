.. _RORUTA-PostFactum-RankRelatedImprovementOrMissingValue-PUT:

RORUTA-PostFactum-RankRelatedImprovementOrMissingValue
======================================================

:Version: 1.0
:Provider: PUT
:SOAP service's name: ``RORUTA-PostFactum-RankRelatedImprovementOrMissingValue-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

For a given ranking position n and the alternative a which is worse than n-th, depending on the need, the module finds one of the two possible types of values. These are the minimal improvement of the performance of the alternative a or the minimal missing value of the comprehensive score of this alternative. In the first case, the procedure finds a minimal greater than one value that multiplied by the performances on chosen criteria allows to attain the desired n-th position for the alternative. In the other case, it finds the minimal value that need to be added to the comprehensive score of this alternative in order to attain at least n-th position. In both cases it is possible to consider the target for both all or at least one compatible value funtion.

**Contact:** Pawel Rychly (pawelrychly@gmail.com).


Inputs
------
(For outputs, see :ref:`below <RORUTA-PostFactum-RankRelatedImprovementOrMissingValue-PUT_outputs>`)


- :ref:`criteria <RORUTA-PostFactum-RankRelatedImprovementOrMissingValue-PUT-criteria>`
- :ref:`selected-criteria <RORUTA-PostFactum-RankRelatedImprovementOrMissingValue-PUT-selected-criteria>` *(optional)*
- :ref:`alternatives <RORUTA-PostFactum-RankRelatedImprovementOrMissingValue-PUT-alternatives>`
- :ref:`performances <RORUTA-PostFactum-RankRelatedImprovementOrMissingValue-PUT-performances>`
- :ref:`characteristic-points <RORUTA-PostFactum-RankRelatedImprovementOrMissingValue-PUT-characteristic-points>` *(optional)*
- :ref:`preferences <RORUTA-PostFactum-RankRelatedImprovementOrMissingValue-PUT-preferences>` *(optional)*
- :ref:`intensities-of-preferences <RORUTA-PostFactum-RankRelatedImprovementOrMissingValue-PUT-intensities-of-preferences>` *(optional)*
- :ref:`target <RORUTA-PostFactum-RankRelatedImprovementOrMissingValue-PUT-target>`
- :ref:`rank-related-requirements <RORUTA-PostFactum-RankRelatedImprovementOrMissingValue-PUT-rank-related-requirements>` *(optional)*
- :ref:`parameters <RORUTA-PostFactum-RankRelatedImprovementOrMissingValue-PUT-parameters>`


.. _RORUTA-PostFactum-RankRelatedImprovementOrMissingValue-PUT-criteria:

criteria
~~~~~~~~

A list of all considered criteria. The input value should be a valid XMCDA document whose main tag is criteria.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
                
                    <criteria>
                        <criterion id="%1" name="%1"></criterion>
                        [...]
                    </criteria>
                    
            


------------------------


.. _RORUTA-PostFactum-RankRelatedImprovementOrMissingValue-PUT-selected-criteria:

selected-criteria *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A set of ids of the criteria that should be taken into account for modification of the performances. This parameter is used only if the modification of performances is searched.

The input value should be a valid XMCDA document whose main tag is ``<criteriaSets>``.
It must have the following form::

   
	<criteriaSets>
		<criteriaSet>
			<element>
				<criterionID>...</criterionID>
			</element>
			<element>
				<criterionID>...</criterionID>
			</element>
			[...]
		</criteriaSet>
	</criteriaSets>



------------------------


.. _RORUTA-PostFactum-RankRelatedImprovementOrMissingValue-PUT-alternatives:

alternatives
~~~~~~~~~~~~

The list of all considered alternatives. The input value should be a valid XMCDA document whose main tag is alternatives. Each alternative may be described using two attributes: id and name. While the first one denotes a machine readable name, the second represents a human readable name.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
<alternatives>
	<alternative id="..." name="..." />
	[...]
</alternatives>



------------------------


.. _RORUTA-PostFactum-RankRelatedImprovementOrMissingValue-PUT-performances:

performances
~~~~~~~~~~~~

Description of evaluation of alternatives on different criteria. It is required to provide the IDs of both criteria and alternatives described previously. The input value should be provided as a valid XMCDA document whose main tag is performanceTable

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   
<performanceTable>
	<alternativePerformances>
		<alternativeID>a01</alternativeID>
		<performance>
			<criterionID>c01</criterionID>
			<values>
				<value>
					<real>1.0</real>
				</value>
			</values>
		</performance>
		[...]
	</alternativePerformances>
	[...]
<performanceTable>



------------------------


.. _RORUTA-PostFactum-RankRelatedImprovementOrMissingValue-PUT-characteristic-points:

characteristic-points *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A set of values associated with the criteria. This input allows to determine what type of value function should be used for the particular criterion. For each criterion that has an associated greater than one value, a piecewise linear value function is used. In this case, the mentioned value denotes a number of characteristic points of this value function. For the criteria that are not listed in this file, or for these for which the provided values are lower than two uses a general value function. The input value should be provided as a valid XMCDA document whose main tag is criteriaValues. Each element should contain both an id of the criterion, and value tag.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.
It must have the following form::

   
<criteriaValues mcdaConcept="characteristicPoints">
	<criterionValue>
		<criterionID>c01</criterionID>
		<values>
			<value>
				<integer>2</integer>
			</value>
		</values>
	</criterionValue>
	[...]
</criteriaValues>



------------------------


.. _RORUTA-PostFactum-RankRelatedImprovementOrMissingValue-PUT-preferences:

preferences *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~

Set of pairwise comparisons of reference alternatives. For a pair of alternatives three types of comparisons are supported. These are the strict preference, weak preference, and indifference. The input value should be provided as a valid XMCDA document whose main tag is alternativesComparisons. For each type of comparison, a separate alternativesComparisons tag should be used. Within these groups a mentioned types are denoted using a comparisonType tag by respectively strict, weak, and indif label. Comparisons should be provided as pairs of alternatives ids.

The input value should be a valid XMCDA document whose main tag is ``<alternativesMatrix>``.
It must have the following form::

   
                
                    <alternativesComparisons>
                        <comparisonType>
                            %1<!-- type of preference: strong, weak, or indif -->
                        </comparisonType>
                        <pairs>
                            <pair>
                                <initial>
                                    <alternativeID>%2</alternativeID>
                                </initial>
                                <terminal>
                                    <alternativeID>%3</alternativeID>
                                </terminal>
                            </pair>
                            [...]
                        </pairs>
                    </alternativesComparisons>
                    [...]
                    
            


------------------------


.. _RORUTA-PostFactum-RankRelatedImprovementOrMissingValue-PUT-intensities-of-preferences:

intensities-of-preferences *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Set of comparisons of intensities of preference. For a pair of preference relations three types of comparisons are supported. These are the strict preference, weak, preference, and indifference. The input value should be provided as a valid XMCDA document whose main tag is alternativesComparisons. For each type of comparison, a separate alternativesComparisons tag should be used. Within these groups  aforementioned types are denoted using a comparisonType tag by respectively strict, weak, and indif label. Comparisons should be provided as pairs of two elementary sets of alternatives ids. The following form is expected:

The input value should be a valid XMCDA document whose main tag is ``<alternativesMatrix>``.
It must have the following form::

   
                
                    <alternativesComparisons>
                        <comparisonType>%1</comparisonType>
                        <pairs>
                            <pair>
                                <initial>
                                    <alternativesSet>
                                        <element>
                                            <alternativeID>%2</alternativeID>
                                        </element>
                                        <element>
                                            <alternativeID>%3</alternativeID>
                                        </element>
                                    </alternativesSet>
                                </initial>
                                <terminal>
                                    <alternativesSet>
                                        <element>
                                            <alternativeID>%4</alternativeID>
                                        </element>
                                        <element>
                                            <alternativeID>%5</alternativeID>
                                        </element>
                                    </alternativesSet>
                                </terminal>
                            </pair>
                            [...]
                        </pairs>
                    </alternativesComparisons>
                    [...]
                    
            


------------------------


.. _RORUTA-PostFactum-RankRelatedImprovementOrMissingValue-PUT-target:

target
~~~~~~

Description of the target to achieve which has a form of the worst possible position that may be attained by the particular alternative. The input value should be provided as a valid XMCDA document whose main tag is alternativesValues. It should contain one alternative Id and one integer value which denotes the desired ranking position.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.
It must have the following form::

   
<alternativesValues>
	<alternativeValue>
		<alternativeID>...</alternativeID>
		<values>
			<value>
				<integer>...</integer>
			</value>
		</values>
	</alternativeValue>
</alternativesValues>



------------------------


.. _RORUTA-PostFactum-RankRelatedImprovementOrMissingValue-PUT-rank-related-requirements:

rank-related-requirements *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Set of rank-related requirements. In other words it is a set of  ranges of possible positions in the final ranking for a chosen alternatives. The input value should be provided as a valid XMCDA document whose main tag is alternativesValues. Each requirement should contain both an id of the reffered alternative and a pair of values that denote the desired range. These information should be provided within a separate alternativesValue tag.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.
It must have the following form::

   
<alternativesValues>
	<alternativeValue>
		<alternativeID>...</alternativeID>
		<value>
			<interval>
				<lowerBound><integer>...</integer></lowerBound>
				<upperBound><integer>...</integer></upperBound>
			</interval>
		</value>
	</alternativeValue>
	[...]
 </alternativesValues>



------------------------


.. _RORUTA-PostFactum-RankRelatedImprovementOrMissingValue-PUT-parameters:

parameters
~~~~~~~~~~

Method parameters
                strict %1 - Single boolean value. Determines whether to use sctrictly increasing (true) or monotonously increasing (false) value functions.
                type_of_result %2 - Determines whether analyse the modification of performances or comprehensive score.
                precision %3 - A float value between 0 and 1 (bounds excluded). It describes the precision of expected result.
                possible_or_necessary %4 - One of the two label values (possible or necessary). This parameter determines does target should be satisfied by one (possible) or all(necessary) compatible value functions.

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
<programParameters>
	<parameter name="strict">
		<values>
			<value>
				<boolean>%1</boolean>
			</value>
		</values>
	</parameter>
	<parameter name="type_of_result">
		<values>
			<value>
				<label>%2</label>
			</value>
		</values>
	</parameter>
	<parameter name="precision">
		<values>
			<value>
				<real>%3</real>
			</value>
		</values>
	</parameter>
	<parameter name="possible_or_necessary">
		<values>
			<value>
				<label>%4</label>
			</value>
		</values>
	</parameter>
</programParameters>


where:

- **%1** is a parameter named "Use strictly increasing value functions?". This is a boolean.
  The default value is false.

- **%2** is a parameter named "type of result". It can have the following values:

  - ``improvement``: improvement

  - ``missing-value``: missing-value

  The default value is improvement.

- **%3** is a parameter named "precision of the result". This is a float, and the value should conform to the following constraint: A float value between 0 and 1 (bounds excluded). It denotes the expected precision of the result..  More formally, the constraint is::

      ((%3 > 0) && (%3 < 1)) 
  The default value is 0.005.

- **%4** is a parameter named "Whether the target should be achieved possibly or necessarily?". It can have the following values:

  - ``possible``: possible

  - ``necessary``: necessary

  The default value is necessary.


------------------------



.. _RORUTA-PostFactum-RankRelatedImprovementOrMissingValue-PUT_outputs:

Outputs
-------


- :ref:`result <RORUTA-PostFactum-RankRelatedImprovementOrMissingValue-PUT-result>`
- :ref:`messages <RORUTA-PostFactum-RankRelatedImprovementOrMissingValue-PUT-messages>`


.. _RORUTA-PostFactum-RankRelatedImprovementOrMissingValue-PUT-result:

result
~~~~~~

A real value associated with the id of the considered alternative. Depending on chosen option, it may denotes the improvement of performances of this alternative or the missing value of its comprehensive score. The output value is provided as a valid XMCDA document whose main tag is alternativesValues.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   
<alternativesValues>
	<alternativeValue>
		<alternativeID>...</alternativeID>
		<values>
			<value>
				<real>...</real>
			</value>
		</values>
	</alternativeValue>
</alternativesValues>



------------------------


.. _RORUTA-PostFactum-RankRelatedImprovementOrMissingValue-PUT-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/RORUTA-PostFactum-RankRelatedImprovementOrMissingValue-PUT/description-wsDD.xml>`
