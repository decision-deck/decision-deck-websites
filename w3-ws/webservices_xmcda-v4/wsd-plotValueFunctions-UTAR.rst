.. _plotValueFunctions-UTAR:

plotValueFunctions
==================

:Version: 1.0
:Provider: UTAR
:SOAP service's name: ``plotValueFunctions-UTAR`` (see :ref:`soap-requests` for details)

Description
-----------

Plot utility functions

**Contact:** Boris Leistedt (boris.leistedt@gmail.com)


Inputs
------
(For outputs, see :ref:`below <plotValueFunctions-UTAR_outputs>`)


- :ref:`criteria <plotValueFunctions-UTAR-crit>`
- :ref:`valueFunctions <plotValueFunctions-UTAR-valueFunctions>`


.. _plotValueFunctions-UTAR-crit:

criteria
~~~~~~~~

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
				
					<criteria>
						<criterion>
							<criterionID>[...]</criterionID>
						</criterion>
					</criteria>
				
			


------------------------


.. _plotValueFunctions-UTAR-valueFunctions:

valueFunctions
~~~~~~~~~~~~~~

Values of utilities of chosen criteria abscissa - Utility functions

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
				
					<criteria>
						<criterion>
							<criterionID>[...]</criterionID>
							<criterionFunction>
								<points>
									<point>
										<abscissa><real>[...]</real></abscissa>
										<ordinate><real>[...]</real></ordinate>
									</point>
								</points>
							</criterionFunction>
						</criterion>
					</criteria>
				
			


------------------------



.. _plotValueFunctions-UTAR_outputs:

Outputs
-------


- :ref:`valueFunctionsPlot <plotValueFunctions-UTAR-valueFunctionsPlot>`
- :ref:`message <plotValueFunctions-UTAR-logMessage>`


.. _plotValueFunctions-UTAR-valueFunctionsPlot:

valueFunctionsPlot
~~~~~~~~~~~~~~~~~~

A string containing the base64 representation of the png image of the multi-subplot generated by the R statistical software.

The returned value is a XMCDA document whose main tag is ``<criterionValue>``.


------------------------


.. _plotValueFunctions-UTAR-logMessage:

message
~~~~~~~

logMessage

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.
It has the following form::

   
				
					<methodMessages mcdaConcept="methodMessage">
						<logMessage>
							<text>[...]</text>
						</logMessage>
						<errorMessage>
							<text>[...]</text>
						</errorMessage>
					</methodMessages>
				
			


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/plotValueFunctions-UTAR/description-wsDD.xml>`
