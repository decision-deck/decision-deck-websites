.. _plotAlternativesValuesPreorder-ITTB:

plotAlternativesValuesPreorder
==============================

:Version: 2.0
:Provider: ITTB
:SOAP service's name: ``plotAlternativesValuesPreorder-ITTB`` (see :ref:`soap-requests` for details)

Description
-----------

This web service generates a graph representing a preorder on the alternatives, according to numerical values taken by the alternatives (the "best" alternative has the highest value). Compared to the web service plotAlternativesValuesPreorder, some parameters are added. Colors can be used and the title of the graph can be typed. You can choose between an increasing or a decreasing order for the graph. It is also possible to show the name of the alternatives instead of the id, etc. The alternatives' evaluations are supposed to be real or integer numeric values.

**Contact:** Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)


Inputs
------
(For outputs, see :ref:`below <plotAlternativesValuesPreorder-ITTB_outputs>`)


- :ref:`alternatives <plotAlternativesValuesPreorder-ITTB-alternatives>`
- :ref:`alternativesValues <plotAlternativesValuesPreorder-ITTB-alternativesValues>`
- :ref:`parameters <plotAlternativesValuesPreorder-ITTB-parameters>`


.. _plotAlternativesValuesPreorder-ITTB-alternatives:

alternatives
~~~~~~~~~~~~

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
	<alternatives>
		<alternative>
			<active>[...]</active>
			[...]
		</alternative>
		[...]
	</alternatives>



------------------------


.. _plotAlternativesValuesPreorder-ITTB-alternativesValues:

alternativesValues
~~~~~~~~~~~~~~~~~~

A list of <alternativesValue> representing a certain numeric quantity for each alternative, like, e.g., an overall value.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _plotAlternativesValuesPreorder-ITTB-parameters:

parameters
~~~~~~~~~~

Generates a graph taking into account the proposed options.

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
	<programParameters>
		<parameter id="plot_title" name="Plot Title">
			<values>
				<value>
					<label>%1</label>
				</value>
			</values>
		</parameter>
		<parameter id="plot_order" name="Plot order">
			<values>
				<value>
					<label>%2</label>
				</value>
			</values>
		</parameter>
		<parameter id="node_shape" name="Node shape">
			<values>
				<value>
					<label>%3</label>
				</value>
			</values>
		</parameter>
		<parameter id="show_names" name="Show alternatives names">
			<values>
				<value>
					<label>%4</label>
				</value>
			</values>
		</parameter>
		<parameter id="use_color" name="Colors in the graph">
			<values>
				<value>
					<label>%5</label>
				</value>
			</values>
		</parameter>
		<parameter id="selected_color" name="Selected color">
			<values>
				<value>
					<label>%6</label>
				</value>
			</values>
		</parameter >
	</programParameters>


where:

- **%1** is a parameter named "Chart title:". This is a string.
- **%2** is a parameter named "Order:". It can have the following values:

  - ``increasing``: Ascending

  - ``decreasing``: Descending

  The default value is decreasing.

- **%3** is a parameter named "Shape of the nodes?". It can have the following values:

  - ``Rectangle``: Rectangle

  - ``Square``: Square

  - ``Ellipse``: Ellipse

  - ``Circle``: Circle

  - ``Diamond``: Diamond

  The default value is rectangle.

- **%4** is a parameter named "Alternative name or id?". It can have the following values:

  - ``true``: Display name

  - ``false``: Display id

  The default value is false.

- **%5** is a parameter named "Use colors?". It can have the following values:

  - ``true``: Yes

  - ``false``: No

  The default value is false.

- **%6** is a parameter named "Choose color:". It can have the following values:

  - ``Black``: Black

  - ``Red``: Red

  - ``Blue``: Blue

  - ``Green``: Green

  - ``Yellow``: Yellow

  - ``Magenta``: Magenta

  - ``Cyan``: Cyan

  - ``White``: White

  The default value is Black.


------------------------



.. _plotAlternativesValuesPreorder-ITTB_outputs:

Outputs
-------


- :ref:`alternativesValuesPreorder.dot <plotAlternativesValuesPreorder-ITTB-dot>`
- :ref:`alternativesValuesPreorder.png <plotAlternativesValuesPreorder-ITTB-png>`
- :ref:`messages <plotAlternativesValuesPreorder-ITTB-messages>`


.. _plotAlternativesValuesPreorder-ITTB-dot:

alternativesValuesPreorder.dot
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The dot file used to generate the PNG image.

The returned value is a XMCDA document whose main tag is ``<other>``.


------------------------


.. _plotAlternativesValuesPreorder-ITTB-png:

alternativesValuesPreorder.png
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The pre-order as a PNG image.

The returned value is a XMCDA document whose main tag is ``<other>``.


------------------------


.. _plotAlternativesValuesPreorder-ITTB-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/plotAlternativesValuesPreorder-ITTB/description-wsDD.xml>`
