.. _DEACCRCrossEfficiency-PUT:

DEACCRCrossEfficiency
=====================

:Version: 1.0
:Provider: PUT
:SOAP service's name: ``DEACCRCrossEfficiency-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Computes cross efficiency scores for the given DMUs (alternatives) using CCR Data Envelopment Analysis Model.

**Contact:** Malgorzata Napieraj (napieraj.malgorzata@gmail.com)

**Reference:** Gavgani S., Zohrehbandian M., Khorram E., Secondary Goal in DEA Cross Efficiency Evaluation (2013).


Inputs
------
(For outputs, see :ref:`below <DEACCRCrossEfficiency-PUT_outputs>`)


- :ref:`inputsOutputs <DEACCRCrossEfficiency-PUT-inputsOutputs>`
- :ref:`units <DEACCRCrossEfficiency-PUT-units>`
- :ref:`performanceTable <DEACCRCrossEfficiency-PUT-performanceTable>`
- :ref:`weightsLinearConstraints <DEACCRCrossEfficiency-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <DEACCRCrossEfficiency-PUT-methodParameters>`


.. _DEACCRCrossEfficiency-PUT-inputsOutputs:

inputsOutputs
~~~~~~~~~~~~~

A list of criteria with specified preference direction. List has to contains at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output).

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   <criteria>
                        <criterion>
							<scale>
								<quantitative>
									<preferenceDirection>
										[...]
									</preferenceDirection>
								</quantitative>
							</scale>
                            [...]
                        </criterion>
                        [...]
                    </criteria>


------------------------


.. _DEACCRCrossEfficiency-PUT-units:

units
~~~~~

A list of alternatives (DMUs).

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>


------------------------


.. _DEACCRCrossEfficiency-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A list of alternatives (DMUs) performances.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   <performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>


------------------------


.. _DEACCRCrossEfficiency-PUT-weightsLinearConstraints:

weightsLinearConstraints *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of criteria linear constraints.

The input value should be a valid XMCDA document whose main tag is ``<criteriaLinearConstraints>``.
It must have the following form::

   <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID>[...]</criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>


------------------------


.. _DEACCRCrossEfficiency-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~

There is one possible method parameter -- "type". It indicates model formulation for cross efficiency evaluation -- aggressive or benevolent.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   <methodParameters>
						<parameter name="type">
							<value><label>%1</label></value>
						</parameter>
					</methodParameters>

where:

- **%1** is a parameter named "type". It can have the following values:

  - ``aggressive``: Aggressive

  - ``benevolent``: Benevolent

  The default value is aggressive.


------------------------



.. _DEACCRCrossEfficiency-PUT_outputs:

Outputs
-------


- :ref:`crossEfficiency <DEACCRCrossEfficiency-PUT-crossEfficiency>`
- :ref:`messages <DEACCRCrossEfficiency-PUT-messages>`


.. _DEACCRCrossEfficiency-PUT-crossEfficiency:

crossEfficiency
~~~~~~~~~~~~~~~

A list of alternatives with computed cross efficiency scores.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   <alternativesValues mcdaConcept="crossEfficiency">
						<alternativeValue>
						  <alternativeID>[...]</alternativeID>
						  <value>
							[...]
						  </value>
						</alternativeValue>
						[...]
					</alternativesValues>


------------------------


.. _DEACCRCrossEfficiency-PUT-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/DEACCRCrossEfficiency-PUT/description-wsDD.xml>`
