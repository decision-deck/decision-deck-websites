.. _PROMETHEE-II-GDSS_flows-PUT:

PROMETHEE-II-GDSS_flows
=======================

:Version: 1.0.0
:Provider: PUT
:SOAP service's name: ``PROMETHEE-II-GDSS_flows-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Module for calculation PROMETHEE group ranking as a weighted sum of flows from every decision maker for each alternative.

**Contact:** Magdalena Dziecielska <magdalenadziecielska6@gmail.com>

**Web page:** https://github.com/MagdalenaDziecielska/PrometheeDiviz

**Reference:** C. Macharis, J.-P. Brans and B. Mareschal, 1998. The GDSS PROMETHEE Procedure. Journal of Decision Systems, Vol. 7-SI/1998, 283-307.


Inputs
------
(For outputs, see :ref:`below <PROMETHEE-II-GDSS_flows-PUT_outputs>`)


- :ref:`alternatives <PROMETHEE-II-GDSS_flows-PUT-input1>`
- :ref:`flows_1 <PROMETHEE-II-GDSS_flows-PUT-flows_1>`
- :ref:`flows_2 <PROMETHEE-II-GDSS_flows-PUT-flows_2>`
- :ref:`flows_3 <PROMETHEE-II-GDSS_flows-PUT-flows_3>` *(optional)*
- :ref:`flows_4 <PROMETHEE-II-GDSS_flows-PUT-flows_4>` *(optional)*
- :ref:`flows_5 <PROMETHEE-II-GDSS_flows-PUT-flows_5>` *(optional)*
- :ref:`flows_6 <PROMETHEE-II-GDSS_flows-PUT-flows_6>` *(optional)*
- :ref:`flows_7 <PROMETHEE-II-GDSS_flows-PUT-flows_7>` *(optional)*
- :ref:`flows_8 <PROMETHEE-II-GDSS_flows-PUT-flows_8>` *(optional)*
- :ref:`flows_9 <PROMETHEE-II-GDSS_flows-PUT-flows_9>` *(optional)*
- :ref:`flows_10 <PROMETHEE-II-GDSS_flows-PUT-flows_10>` *(optional)*
- :ref:`nbDM <PROMETHEE-II-GDSS_flows-PUT-nbDM>`
- :ref:`method_parameters <PROMETHEE-II-GDSS_flows-PUT-method_parameters>`


.. _PROMETHEE-II-GDSS_flows-PUT-input1:

alternatives
~~~~~~~~~~~~

Alternatives to consider.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _PROMETHEE-II-GDSS_flows-PUT-flows_1:

flows_1
~~~~~~~

Flows for decision maker 1.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _PROMETHEE-II-GDSS_flows-PUT-flows_2:

flows_2
~~~~~~~

Flows for decision maker 2.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _PROMETHEE-II-GDSS_flows-PUT-flows_3:

flows_3 *(optional)*
~~~~~~~~~~~~~~~~~~~~

Flows for decision maker 3.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _PROMETHEE-II-GDSS_flows-PUT-flows_4:

flows_4 *(optional)*
~~~~~~~~~~~~~~~~~~~~

Flows for decision maker 4.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _PROMETHEE-II-GDSS_flows-PUT-flows_5:

flows_5 *(optional)*
~~~~~~~~~~~~~~~~~~~~

Flows for decision maker 5.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _PROMETHEE-II-GDSS_flows-PUT-flows_6:

flows_6 *(optional)*
~~~~~~~~~~~~~~~~~~~~

Flows for decision maker 6.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _PROMETHEE-II-GDSS_flows-PUT-flows_7:

flows_7 *(optional)*
~~~~~~~~~~~~~~~~~~~~

Flows for decision maker 7.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _PROMETHEE-II-GDSS_flows-PUT-flows_8:

flows_8 *(optional)*
~~~~~~~~~~~~~~~~~~~~

Flows for decision maker 8.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _PROMETHEE-II-GDSS_flows-PUT-flows_9:

flows_9 *(optional)*
~~~~~~~~~~~~~~~~~~~~

Flows for decision maker 9.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _PROMETHEE-II-GDSS_flows-PUT-flows_10:

flows_10 *(optional)*
~~~~~~~~~~~~~~~~~~~~~

Flows for decision maker 10.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _PROMETHEE-II-GDSS_flows-PUT-nbDM:

nbDM
~~~~

Number of decision makers (2-10)

The input value should be a valid XMCDA document whose main tag is ``<parameter>``.
It must have the following form::

   %1

where:

- **%1** is a parameter named "Number of decision makers". This is a int, and the value should conform to the following constraint: An integer value between 2 and 10 (inclusive)..  More formally, the constraint is::

      %1 >= 2 && %1 <= 10 
  The default value is 2.


------------------------


.. _PROMETHEE-II-GDSS_flows-PUT-method_parameters:

method_parameters
~~~~~~~~~~~~~~~~~

A set of parameters provided to tune up the module's operation.

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
			  
                     <parameter id="decisionMaker1">
                        <value>
                            <real>%1</real>
                        </value>
                    </parameter>
                    <parameter id="decisionMaker2">
                        <value>
                            <real>%2</real>
                        </value>
                    </parameter>
                    <parameter id="decisionMaker3">
                        <value>
                            <real>%3</real>
                        </value>
                    </parameter>
                    <parameter id="decisionMaker4">
                        <value>
                            <real>%4</real>
                        </value>
                    </parameter>
                    <parameter id="decisionMaker5">
                        <value>
                            <real>%5</real>
                        </value>
                    </parameter>
                    <parameter id="decisionMaker6">
                        <value>
                            <real>%6</real>
                        </value>
                    </parameter>
                    <parameter id="decisionMaker7">
                        <value>
                            <real>%7</real>
                        </value>
                    </parameter>
                    <parameter id="decisionMaker8">
                        <value>
                            <real>%8</real>
                        </value>
                    </parameter>
                    <parameter id="decisionMaker9">
                        <value>
                            <real>%9</real>
                        </value>
                    </parameter>
                    <parameter id="decisionMaker10">
                        <value>
                            <real>%10</real>
                        </value>
                    </parameter>
                </programParameters>
	        
		  

where:

- **%1** is a parameter named "decision maker 1 weight". This is a float, and the value should conform to the following constraint: An integer value..  More formally, the constraint is::

      %1 >= 0 
  The default value is 0.0.

- **%2** is a parameter named "decision maker 2 weight:". This is a float, and the value should conform to the following constraint: An integer value..  More formally, the constraint is::

      %2 >= 0 
  The default value is 0.0.

- **%3** is a parameter named "decision maker 3 weight:". This is a float, and the value should conform to the following constraint: An integer value. Choose 0.0 when decision maker does not exist..  More formally, the constraint is::

      %3 >= 0 
  The default value is 0.0.

- **%4** is a parameter named "decision maker 4 weight". This is a float, and the value should conform to the following constraint: An integer value. Choose 0.0 when decision maker does not exist..  More formally, the constraint is::

      %4 >= 0 
  The default value is 0.0.

- **%5** is a parameter named "decision maker 5 weight". This is a float, and the value should conform to the following constraint: An integer value. Choose 0.0 when decision maker does not exist..  More formally, the constraint is::

      %5 >= 0 
  The default value is 0.0.

- **%6** is a parameter named "decision maker 6 weight". This is a float, and the value should conform to the following constraint: An integer value. Choose 0.0 when decision maker does not exist..  More formally, the constraint is::

      %6 >= 0 
  The default value is 0.0.

- **%7** is a parameter named "decision maker 7 weight". This is a float, and the value should conform to the following constraint: An integer value. Choose 0.0 when decision maker does not exist..  More formally, the constraint is::

      %7 >= 0 
  The default value is 0.0.

- **%8** is a parameter named "decision maker 8 weight". This is a float, and the value should conform to the following constraint: An integer value. Choose 0.0 when decision maker does not exist..  More formally, the constraint is::

      %8 >= 0 
  The default value is 0.0.

- **%9** is a parameter named "decision maker 9 weight". This is a float, and the value should conform to the following constraint: An integer value. Choose 0.0 when decision maker does not exist..  More formally, the constraint is::

      %9 >= 0 
  The default value is 0.0.

- **%10** is a parameter named "decision maker 10 weight". This is a float, and the value should conform to the following constraint: An integer value. Choose 0.0 when decision maker does not exist..  More formally, the constraint is::

      %10 >= 0 
  The default value is 0.0.


------------------------



.. _PROMETHEE-II-GDSS_flows-PUT_outputs:

Outputs
-------


- :ref:`aggregated_flows <PROMETHEE-II-GDSS_flows-PUT-output1>`
- :ref:`ranking <PROMETHEE-II-GDSS_flows-PUT-output2>`
- :ref:`messages <PROMETHEE-II-GDSS_flows-PUT-output3>`


.. _PROMETHEE-II-GDSS_flows-PUT-output1:

aggregated_flows
~~~~~~~~~~~~~~~~

Aggregated flows and weights for decision makers.

The returned value is a XMCDA document whose main tag is ``<alternativesMatrix>``.


------------------------


.. _PROMETHEE-II-GDSS_flows-PUT-output2:

ranking
~~~~~~~

Final group ranking

The returned value is a XMCDA document whose main tag is ``<alternativesMatrix>``.


------------------------


.. _PROMETHEE-II-GDSS_flows-PUT-output3:

messages
~~~~~~~~

Messages or errors generated by this module.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/PROMETHEE-II-GDSS_flows-PUT/description-wsDD.xml>`
