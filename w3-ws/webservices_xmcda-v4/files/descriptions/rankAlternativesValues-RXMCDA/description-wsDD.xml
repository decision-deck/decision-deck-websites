<?xml version='1.0' encoding='utf-8'?>
<program_description>
    <program provider="RXMCDA" name="rankAlternativesValues" version="1.1" displayName="rankAlternativesValues" />
    <documentation>
        <description>Calculates the rank of alternatives via their overall values. A parameter named maxMin allows to determine if the best value is the highest or the lowest one (by default, the lowest value is ranked first).</description>
        <contact><![CDATA[Patrick Meyer (patrick.meyer@telecom-bretagne.eu)]]></contact>
    </documentation>
    <parameters>

        <input id="alternatives" name="alternatives" displayName="alternatives" isoptional="0">
            <documentation>
                <description>A list of alternatives. Alternatives can be activated or desactivated via the &lt;active&gt; tag (true or false). By default (no &lt;active&gt; tag), alternatives are considered as active.</description>
            </documentation>
            <xmcda tag="alternatives"><![CDATA[
	<alternatives>
		<alternative id="..." [...]>
			<active>[...]</active>
		</alternative>
		[...]
	</alternatives>
]]></xmcda>
        </input>

        <input id="overallValues" name="overallValues" displayName="alternativesValues" isoptional="0">
            <documentation>
                <description>A list of &lt;alternativeValue&gt; in an &lt;alternativesValues&gt; containing the overall value of each alternative. The &lt;value&gt; should be a numeric value.</description>
            </documentation>
            <xmcda tag="alternativesValues" />
        </input>

        <input id="maxMin" name="maxMin" displayName="maxMin" isoptional="1">
            <documentation>
                <description>The parameter which says if the highest or lowest value is to be ranked first.</description>
            </documentation>
            <xmcda tag="programParameters"><![CDATA[
	<programParameters>
		<parameter name="maxMin"> <!-- REQUIRED  -->
			<values>
				<value>
					<label>%1</label>
				</value>
			</values>
		</parameter>
	</programParameters>
]]></xmcda>
            <gui status="preferGUI">
                <entry id="%1" type="enum" displayName="maxMin">
                    <documentation>
                        <description>The parameter which says if the highest or lowest value is to be ranked first.</description>
                    </documentation>
                    <items>
                        <item id="min">
                            <description>The lowest value is ranked first</description>
                            <value>min</value>
                        </item>
                        <item id="max">
                            <description>The highest value is ranked first</description>
                            <value>max</value>
                        </item>
                    </items>
                    <defaultValue>min</defaultValue>
                </entry>
            </gui>
        </input>

        <output id="alternativesRanks-output" name="alternativesRanks" displayName="alternativesRanks">
            <documentation>
                <description>A list of &lt;alternativeValue&gt; in an &lt;alternativesValues&gt; containing the rank of each alternative.</description>
            </documentation>
            <xmcda tag="alternativesValues"><![CDATA[
	<alternativesValues mcdaConcept="alternativesRanks">
		<alternativeValue>
			[...]
		</alternativeValue>
		[...]
	</alternativesValues>
]]></xmcda>
        </output>

        <output id="messages" name="messages" displayName="messages">
            <documentation>
                <description>A list of messages generated by the algorithm.</description>
            </documentation>
            <xmcda tag="programExecutionResult" />
        </output>

    </parameters>
</program_description>
