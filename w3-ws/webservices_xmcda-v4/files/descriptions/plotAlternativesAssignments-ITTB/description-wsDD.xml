<?xml version="1.0" encoding="ISO-8859-1"?>
<program_description xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://www.decision-deck.org/ws/_downloads/description.xsd">
    <program provider="ITTB" name="plotAlternativesAssignments" displayName="plotAlternativesAssignments" version="2.0"/>
    <documentation>
        <description>This web service generates a plot representing the alternatives assignments. Colors can be used. You can specify how to display the different categories: by line, by column or in a grid. The plots can also be ordered: alphabetical order, its inverse or by categories.</description>
        <contact>Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)</contact>
    </documentation>

    <parameters>

        <input id="alternatives" name="alternatives" displayName="alternatives" isoptional="0">
            <documentation>
                <description>A list of alternatives. Alternatives can be activated or desactivated via the &lt;active&gt; tag (true or false). By default (no &lt;active&gt; tag), alternatives are considered as active. </description>
            </documentation>
            <xmcda tag="alternatives"/>
        </input>

        <input id="categories" name="categories" displayName="categories" isoptional="1">
            <documentation>
                <description>The list of categories and their rank (required!).</description>
            </documentation>
            <xmcda tag="categories"><![CDATA[
	<categories>
		<category id=[...]>
			<active>[...]</active>
			<rank>
				<integer>[...]</integer>
			</rank>
		</category>
		[...]
	</categories>
]]></xmcda>
        </input>

        <input id="alternativesAssignments" name="alternativesAssignments" displayName="alternatives assignments" isoptional="0">
            <documentation>
                <description>Assignment of reference alternatives</description>
            </documentation>
            <xmcda tag="alternativesAssignments"/>
        </input>

        <input id="parameters" name="parameters" displayName="parameters" isoptional="0">
            <documentation>
                <description/>
            </documentation>
            <xmcda tag="programParameters"><![CDATA[
	<programParameters>
		<parameter id="plot_title" name="Plot title">
			<values>
				<value>
					<label>%2</label>
				</value>
			</values>
		</parameter>
		<parameter id="unique_plot" name="Unique plot">
			<values>
				<value>
					<label>%3</label>
				</value>
			</values>
		</parameter>
		<parameter id="plots_display" name="Plots' display">
			<values>
				<value>
					<label>%4</label>
				</value>
			</values>
		</parameter>
		<parameter id="order" name="Order">
			<values>
				<value>
					<label>%5</label>
				</value>
			</values>
		</parameter>
		<parameter id="selected_color" name="Selected color">
			<values>
				<value>
					<label>%7</label>
				</value>
			</values>
		</parameter>
		<parameter id="categories_shape" name="Categories shape">
			<values>
				<value>
					<label>%8</label>
				</value>
			</values>
		</parameter>
	</programParameters>]]>
            </xmcda>
            <gui status="preferGUI">

                <entry id="%2" type="string" displayName="Plot title:">
                    <documentation>
                        <description>String for the title of the plot. The default value is an empty field.</description>
                    </documentation>
                    <defaultValue></defaultValue>
                </entry>

                <entry id="%3" type="enum" displayName="Unique plot?">
                    <documentation>
                        <description>In a unique plot, only one image is generated containing all the alternatives affectations. Multiple plots can be obtained. The default value is true.</description>
                    </documentation>
                    <items>
                        <item id="true">
                            <description>Yes</description>
                            <value>true</value>
                        </item>
                        <item id="false">
                            <description>No</description>
                            <value>false</value>
                        </item>
                    </items>
                    <defaultValue>true</defaultValue>
                </entry>

                <entry id="%4" type="enum" displayName="Display assignments by:">
                    <documentation>
                        <description>In the case of a unique plot, you can specify how to display the categories: by line, by column or in a grid. The default value is by column.</description>
                    </documentation>
                    <!-- %4 is active if the entry id=%3 takes the value true -->
                    <dependency><![CDATA[%3:type="true"]]></dependency>
                    <items>
                        <item id="column">
                            <description>Column</description>
                            <value>Column</value>
                        </item>
                        <item id="line">
                            <description>Line</description>
                            <value>Line</value>
                        </item>
                        <item id="grid">
                            <description>Grid</description>
                            <value>Grid</value>
                        </item>
                    </items>
                    <defaultValue>column</defaultValue>
                </entry>

                <entry id="%5" type="enum" displayName="Order:">
                    <documentation>
                        <description>The parameter which says if the categories are sorted out. Choose between "increasing" (alphabetical order), "decreasing" or "categories".</description>
                    </documentation>
                    <items>
                        <item id="increasing">
                            <description>increasing</description>
                            <value>increasing</value>
                        </item>
                        <item id="decreasing">
                            <description>decreasing</description>
                            <value>decreasing</value>
                        </item>
                        <item id="categories">
                            <description>categories</description>
                            <value>categories</value>
                        </item>
                    </items>
                    <defaultValue>increasing</defaultValue>
                </entry>

                <entry id="%7" type="enum" displayName="Choose color:">
                    <documentation>
                        <description>Choose between "Black", "Red", "Blue", "Green", "Yellow", "Magenta" and "Cyan".</description>
                    </documentation>
                    <items>
                        <item id="black">
                            <description>Black</description>
                            <value>Black</value>
                        </item>
                        <item id="red">
                            <description>Red</description>
                            <value>Red</value>
                        </item>
                        <item id="blue">
                            <description>Blue</description>
                            <value>Blue</value>
                        </item>
                        <item id="green">
                            <description>Green</description>
                            <value>Green</value>
                        </item>
                        <item id="yellow">
                            <description>Yellow</description>
                            <value>Yellow</value>
                        </item>
                        <item id="magenta">
                            <description>Magenta</description>
                            <value>Magenta</value>
                        </item>
                        <item id="cyan">
                            <description>Cyan</description>
                            <value>Cyan</value>
                        </item>
                    </items>
                    <defaultValue>black</defaultValue>
                </entry>

                <entry id="%8" type="enum" displayName="Plot alternatives in:">
                    <documentation>
                        <description>
                            The categories can be plotted in a rectangle, an oval or a diamond.
                        </description>
                    </documentation>
                    <items>
                        <item id="rectangle">
                            <description>rectangle</description>
                            <value>rectangle</value>
                        </item>
                        <item id="oval">
                            <description>oval</description>
                            <value>oval</value>
                        </item>
                        <item id="diamond">
                            <description>diamond</description>
                            <value>diamond</value>
                        </item>
                    </items>
                    <defaultValue>oval</defaultValue>
                </entry>
            </gui>
        </input>
<!--
        <output id="dot" name="alternativesAssignments.dot" displayName="alternativesAssignments (dot)">
            <documentation>
                <description>A string containing the base64 representation of the png image of the generated plot.</description>
            </documentation>
            <xmcda tag="other"/>
        </output>
-->
         <output id="png" name="alternativesAssignments.png" displayName="alternativesAssignments (png)">
            <documentation>
                <description>A string containing the base64 representation of the png image of the generated plot.</description>
            </documentation>
            <xmcda tag="other"/>
        </output>

        <output id="messages" name="messages" displayName="messages">
            <documentation>
                <description>A list of messages generated by the algorithm.</description>
            </documentation>
            <xmcda tag="methodMessages"/>
        </output>

    </parameters>
</program_description>
