<?xml version="1.0" encoding="UTF-8"?>
<program_description>
  <program provider="PyXMCDA"
       name="csvToXMCDA-categoriesProfiles"
       displayName="csvToXMCDA-categoriesProfiles"
       version="2.0" />
     <documentation>
        <description>Transforms a file containing categories profiles from a comma-separated values (CSV) file to XMCDA compliant files.</description>
        <contact>Sébastien Bigaret (sebastien.bigaret@telecom-bretagne.eu)</contact>
        <url>https://gitlab.com/sbigaret/ws-pyxmcda</url>
     </documentation>

     <parameters>
        <input displayName="categories profiles (csv)" name="categoriesProfiles.csv" id="categoriesProfiles_csv" isoptional="0">
            <documentation>
                <description>The categories profiles as a CSV file.

Example::

  cat1 (Bad),,a1
  cat22 (Medium),a1,a2
  cat3 (Good),a2,

The first column consists in categories ids (and names), in the form `id (name)` --the name between parenthesis is optional, and there is a parameter to disable the lookup of names altogether.

Each lines consists in 3 elements: the category, then the ids of the alternatives being respectively the lower bound and the upper bound of the category's profile.  Either one may be empty (but not both).

</description>
            </documentation>
            <xmcda tag="other" /> <!-- Not a real xmcda tag -->
        </input>

        <input id="parameters" name="parameters" displayName="parameters" isoptional="0">
          <documentation>
            <description>Parameters of the program</description>
          </documentation>
          <xmcda tag="programParameters"><![CDATA[
    <programParameters>
        <parameter id="csv_delimiter">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </parameter>
        <parameter id="name_in_id">
            <values>
                <value>
                    <boolean>%2</boolean>
                </value>
            </values>
        </parameter>
    </programParameters>
]]></xmcda>
          <gui status="preferGUI">
            <entry id="%1" type="string" displayName="CSV delimiter">
              <documentation>
                <description>Indicates the delimiter to use. Leave blank for auto-detection.  It is especially handful when the auto-detection fails to determine the csv delimiter.</description>
              </documentation>
              <constraint>
                <description>One character maximum</description>
                <code><![CDATA[%1.length() < 2]]></code>
              </constraint>
              <defaultValue></defaultValue> <!-- default is blank: auto-detection -->
            </entry>
            <entry id="%2" type="enum" displayName="First column">
              <documentation>
                <description>Content of the first column</description>
              </documentation>
              <items>
                  <item id="id_only">
                      <description>id</description>
                      <value>false</value>
                  </item>
                  <item id="id_and_name">
                      <description>id (name)</description>
                      <value>true</value>
                  </item>
	      </items>
              <defaultValue>id_and_name</defaultValue>
            </entry>
          </gui>
        </input>

        <output displayName="categories profiles" name="categoriesProfiles" id="categoriesProfiles">
            <documentation>
                <description>The equivalent categories profiles.</description>
            </documentation>
            <xmcda tag="categoriesProfiles" />
        </output>

        <output displayName="messages" name="messages" id="messages">
            <documentation>
                <description>Status messages.</description>
            </documentation>
            <xmcda tag="programExecutionResult" />
        </output>

     </parameters>
</program_description>
