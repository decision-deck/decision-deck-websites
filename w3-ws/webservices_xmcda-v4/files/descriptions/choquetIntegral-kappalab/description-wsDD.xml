<?xml version='1.0' encoding='utf-8'?>
<program_description>
    <program provider="kappalab" name="choquetIntegral" version="1.0" displayName="choquetIntegral" />
    <documentation>
        <description>Computes the Choquet integral from a normalised Mobius transform of a capacity.</description>
        <contact><![CDATA[Patrick Meyer (patrick.meyer@telecom-bretagne.eu)]]></contact>
    </documentation>
    <parameters>

        <input id="criteria" name="criteria" displayName="criteria" isoptional="0">
            <documentation>
                <description>A list of criteria. Criteria can be activated or desactivated via the &lt;active&gt; tag (true or false). By default (no &lt;active&gt; tag), criteria are considered as active.</description>
            </documentation>
            <xmcda tag="criteria"><![CDATA[
	<criteria>
		<criterion>
			<active>[...]</active>
			[...]
		</criterion>
	    [...]
	</criteria>
]]></xmcda>
        </input>

        <input id="alternatives" name="alternatives" displayName="alternatives" isoptional="0">
            <documentation>
                <description>A list of alternatives. Alternatives can be activated or desactivated via the &lt;active&gt; tag (true or false). By default (no &lt;active&gt; tag), alternatives are considered as active.</description>
            </documentation>
            <xmcda tag="alternatives"><![CDATA[
	<alternatives>
		<alternative id="..." [...]>
			<active>[...]</active>
		</alternative>
		[...]
	</alternatives>
]]></xmcda>
        </input>

        <input id="performanceTable" name="performanceTable" displayName="performanceTable" isoptional="0">
            <documentation>
                <description>A performance table. The evaluations should be numeric values, i.e. &lt;real&gt;, &lt;integer&gt; or &lt;rational&gt;.</description>
            </documentation>
            <xmcda tag="performanceTable" />
        </input>

        <input id="mobiusCapacity" name="mobiusCapacity" displayName="mobiusCapacity" isoptional="0">
            <documentation>
                <description>The Mobius transform of a capacity.</description>
            </documentation>
            <xmcda tag="criteriaSets,criteriaSetsValues"><![CDATA[
	<criteriaSets>
		<criteriaSet id="criteriaSet_1">
			<element>
				<criterionID>...</criterionID>
				[...]
			</element>
		</criteriaSet>
		[...]
	</criteriaSets>
	<criteriaSetsValues mcdaConcept="capacityMoebius">
		<criteriaSetValue>
			<criteriaSetID>criteriaSet_1</criteriaSetID>
			<values>
				<value>
					<real>...</real>
				</value>
			</values>
		</criteriaSetValue>
		[...]
	</criteriaSetsValues>
]]></xmcda>
        </input>

        <input id="kAdditivity" name="kAdditivity" displayName="kAdditivity" isoptional="0">
            <documentation>
                <description>Indicates the level of k-additivity of the Mobius capacity (the Mobius transform of subsets whose cardinal is superior to k vanishes).</description>
            </documentation>
            <xmcda tag="programParameters"><![CDATA[
	<programParameters>
		<parameter name="kAdditivity"> <!-- name: REQUIRED -->
			<values>
				<value>
					<integer>%1</integer>
				</value>
			</values>
		</parameter>
	</programParameters>
]]></xmcda>
            <gui status="preferFile">
                <entry id="%1" type="int" displayName="kAdditivity">
                    <documentation>
                        <description>Indicates the level of k-additivity of the Mobius capacity (the Mobius transform of subsets whose cardinal is superior to k vanishes).</description>
                    </documentation>
                    <constraint>
                        <description>The value should be a positive integer, less than or equal to the number of criteria.</description>
                        <code><![CDATA[ %1 > 0 ]]></code>
                    </constraint>
                    <defaultValue>1</defaultValue>
                </entry>
            </gui>
        </input>

        <output id="overallValues" name="overallValues" displayName="overallValues">
            <documentation>
                <description>The overall values of the alternatives.</description>
            </documentation>
            <xmcda tag="alternativesValues" />
        </output>

        <output id="messages" name="messages" displayName="messages">
            <documentation>
                <description>A list of messages generated by the algorithm.</description>
            </documentation>
            <xmcda tag="programExecutionResult" />
        </output>

    </parameters>
</program_description>
