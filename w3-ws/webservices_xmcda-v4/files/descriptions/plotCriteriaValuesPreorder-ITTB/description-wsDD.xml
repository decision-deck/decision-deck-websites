<?xml version="1.0" encoding="ISO-8859-1"?>
<program_description xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://www.decision-deck.org/ws/_downloads/description.xsd">
    <program provider="ITTB" name="plotCriteriaValuesPreorder" displayName="plotCriteriaValuesPreorder" version="2.0"/>
       <documentation>
           <description>This web service generates a graph representing a preorder on the criteria, according to numerical values taken by the criteria (the "best" criteria has the highest value). Colors can be used and the graph can have a title. You can choose between an increasing or a decreasing order for the graph. It is also possible to show the name of the criteria instead of the id, etc. The criteria evaluations are supposed to be real or integer numeric values.</description>
              <contact>Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)</contact>
       </documentation>

       <parameters>

           <input id="criteria" name="criteria" displayName="criteria" isoptional="0">
               <documentation>
                   <description>A list of criteria. Criteria can be activated or desactivated via the &lt;active&gt; tag (true or false). By default (no &lt;active&gt; tag), criteria are considered as active.</description>
               </documentation>
               <xmcda tag="criteria"><![CDATA[
	<criteria>
		<criterion>
			<active>[...]</active>
			[...]
		</criterion>
		[...]
	</criteria>
]]></xmcda>
           </input>

           <input id="criteriaValues" name="criteriaValues" displayName="criteriaValues" isoptional="0">
               <documentation>
                   <description>A list of &lt;criterionValue&gt; representing a certain numeric quantity for each criterion, like, e.g., an importance value.</description>
               </documentation>
               <xmcda tag="criteriaValues"/>
           </input>

           <input id="parameters" name="parameters" displayName="parameters" isoptional="0">
               <documentation>
                   <description>Generates a graph taking into account the proposed options.</description>
               </documentation>
               <xmcda tag="programParameters"><![CDATA[
	<programParameters>
		  <parameter id="plot_title" name="Plot Title">
			<values>
				<value>
					<label>%1</label>
				</value>
			</values>
		</parameter>
		<parameter id="plot_order" name="Plot order">
			<values>
				<value>
					<label>%2</label>
				</value>
			</values>
		</parameter>
		<parameter id="node_shape" name="Node shape">
			<values>
				<value>
					<label>%3</label>
				</value>
			</values>
		</parameter>
		<parameter id="show_names" name="Show criteria names">
			<values>
				<value>
					<label>%4</label>
				</value>
			</values>
		</parameter>
		<parameter id="use_color" name="Colors in the graph">
			<values>
				<value>
					<label>%5</label>
				</value>
			</values>
		</parameter>
		<parameter id="selected_color" name="Selected color">
			<values>
				<value>
					<label>%6</label>
				</value>
			</values>
		</parameter >
	</programParameters>
]]></xmcda>
               <gui status="preferGUI">
                   <entry id="%1" type="string" displayName="Chart title:">
                       <documentation>
                       <description>String for the title of the graph. The default value is an empty field.</description>
                           </documentation>
                       <defaultValue></defaultValue>
                   </entry>
                   <entry id="%2" type="enum" displayName="Order:">
                       <documentation>
                           <description>Increasing or decreasing. The default value is decreasing.</description>
                       </documentation>
                       <items>
                           <item id="increasing">
                               <description>Ascending</description>
                               <value>increasing</value>
                           </item>
                           <item id="decreasing">
                               <description>Descending</description>
                               <value>decreasing</value>
                           </item>
                       </items>
                       <defaultValue>decreasing</defaultValue>
                   </entry>
                   <entry id="%3" type="enum" displayName="Shape of the nodes?">
                       <documentation>
                           <description>Choose between rectangle, square, ellipse, circle or diamond.</description>
                       </documentation>
                       <items>
                           <item id="rectangle">
                               <description>Rectangle</description>
                               <value>Rectangle</value>
                           </item>
                           <item id="square">
                               <description>Square</description>
                               <value>Square</value>
                           </item>
                           <item id="ellipse">
                               <description>Ellipse</description>
                               <value>Ellipse</value>
                           </item>
                           <item id="circle">
                               <description>Circle</description>
                               <value>Circle</value>
                           </item>
                           <item id="diamond">
                               <description>Diamond</description>
                               <value>Diamond</value>
                           </item>
                       </items>
                       <defaultValue>rectangle</defaultValue>
                   </entry>
                   <entry id="%4" type="enum" displayName="Criterion name or id?">
                       <documentation>
                           <description>Display criteria names ot IDs.</description>
                       </documentation>
                       <items>
                           <item id="true">
                               <description>name</description>
                               <value>true</value>
                           </item>
                           <item id="false">
                               <description>id</description>
                               <value>false</value>
                           </item>
                       </items>
                       <defaultValue>false</defaultValue>
                   </entry>
                   <entry id="%5" type="enum" displayName="Use colors?">
                       <documentation>
                       <description>The use of colors: true for a colored graph.</description>
                       </documentation>
                       <items>
                           <item id="true">
                               <description>Yes</description>
                               <value>true</value>
                           </item>
                           <item id="false">
                               <description>No</description>
                               <value>false</value>
                           </item>
                       </items>
                       <defaultValue>false</defaultValue>
                   </entry>
                   <entry id="%6" type="enum" displayName="Choose color:">
                       <documentation>
                           <description>String that indicates the color of the graph.Choose between "Black", "Red", "Blue", "Green", "Yellow", "Magenta" and "Cyan".</description>
                       </documentation>
                       <!-- %6 is active if the entry id=%5 takes the value true -->
                       <dependency><![CDATA[%5:type="true"]]></dependency>
                       <items>
                           <item id="black">
                               <description>Black</description>
                               <value>Black</value>
                           </item>
                           <item id="red">
                               <description>Red</description>
                               <value>Red</value>
                           </item>
                           <item id="blue">
                               <description>Blue</description>
                               <value>Blue</value>
                           </item>
                           <item id="green">
                               <description>Green</description>
                               <value>Green</value>
                           </item>
                           <item id="yellow">
                               <description>Yellow</description>
                               <value>Yellow</value>
                           </item>
                           <item id="magenta">
                               <description>Magenta</description>
                               <value>Magenta</value>
                           </item>
                           <item id="cyan">
                               <description>Cyan</description>
                               <value>Cyan</value>
                           </item>
                       </items>
                       <defaultValue>black</defaultValue>
                   </entry>
               </gui>
           </input>

           <output id="dot" name="criteriaValuesPreorder.dot" displayName="criteria value spreorder (dot)">
                     <documentation>
                         <description>The dot file used to generate the PNG image. </description>
                     </documentation>
               <xmcda tag="other"/>
           </output>

           <output id="png" name="criteriaValuesPreorder.png" displayName="criteria value spreorder (png)">
                     <documentation>
                         <description>The preorder as a PNG image. </description>
                     </documentation>
               <xmcda tag="other"/>
           </output>

           <output id="messages" name="messages" displayName="messages">
               <documentation>
                   <description>A list of messages generated by the algorithm.</description>
               </documentation>
               <xmcda tag="programExecutionResult"/>
           </output>

       </parameters>
</program_description>
