<?xml version="1.0" encoding="UTF-8"?>
<program_description>
  <program provider="PyXMCDA"
       name="ACUTA"
       displayName="ACUTA"
       version="1.0" />

  <documentation>
    <description>Perform ACUTA computation on provided inputs.
Decision Maker's Preferences can be provided using either a global ranking of reference alternatives, or the pair-wise alternatives preferences and indifferences.
Outputs optimal valueFunctions along their valuesErrors.

The service generate discrete functions for criteria with qualitative scales.
Number of segments will be still be used to compute a continuous marginal utility function.
It is therefore advised to use (n - 1) as the number of segments (with n the number of labels of the qualitative scale), while using incremented integer values in such scale definition (e.g. \{ 1, 2, 3,..., n\}).
Labels of the scale are used as the abscissa.

This service raises an error if the solution found does not give the same ranking of reference alternatives than in inputs (even with application of the values errors, called sigma in (Munda, 2005).

Also it outputs the UTA* optimum solution if analytic center computation fails.

The implementation and indexing conventions are based on:

    Munda, G. (2005). UTA Methods.
    In Multiple criteria decision analysis: State of the art surveys
    (pp 297-343). Springer, New York, NY.

N.B: This service uses the python module Pulp for representing and solving UTA problems.
    </description>
    <contact>Nicolas Duminy (nicolas.duminy@telecom-bretagne.eu)</contact>
    <url>https://gitlab.com/nduminy/ws-pyxmcda</url>
  </documentation>

  <parameters>
      <input displayName="alternatives" name="alternatives" id="alternatives" isoptional="1">
        <documentation>
            <description>The alternatives. Only used to exclude inactive alternatives from computations (all are considered if not provided).</description>
        </documentation>
        <xmcda tag="alternatives" />
      </input>

      <input displayName="criteria" name="criteria" id="criteria" isoptional="1">
        <documentation>
            <description>The criteria. Only used to exclude inactive criteria from computations (all are considered if not provided).</description>
        </documentation>
        <xmcda tag="criteria" />
      </input>

      <input displayName="performance table" name="performanceTable" id="performanceTable" isoptional="0">
        <documentation>
            <description>The performance table.</description>
        </documentation>
        <xmcda tag="performanceTable" />
      </input>

      <input displayName="alternatives preferences" name="alternativesPreferences" id="alternativesPreferences" isoptional="1">
        <documentation>
            <description>
                The alternatives preferences used to infer utility functions.
                If alternatives is provided, all reference alternatives set as inactive are not considered.
            </description>
        </documentation>
        <xmcda tag="alternativesMatrix" />
        <dependency><![CDATA[%N:type="relations"]]></dependency>
      </input>

      <input displayName="alternatives indifferences" name="alternativesIndifferences" id="alternativesIndifferences" isoptional="1">
        <documentation>
            <description>
                The alternatives indifferences used to infer utility functions.
                If alternatives is provided, all reference alternatives set as inactive are not considered.
            </description>
        </documentation>
        <xmcda tag="alternativesMatrix" />
        <dependency><![CDATA[%N:type="relations"]]></dependency>
      </input>

      <input displayName="alternatives ranks" name="alternativesRanks" id="alternativesRanks" isoptional="1">
        <documentation>
            <description>
                Ranking of reference alternatives used to infer utility functions.
                If alternatives is provided, all reference alternatives set as inactive are not considered.
            </description>
        </documentation>
        <xmcda tag="alternativesValues" />
        <dependency><![CDATA[%N:type="ranking"]]></dependency>
      </input>

      <input displayName="criteria scales" name="criteriaScales" id="criteriaScales" isoptional="1">
        <documentation>
            <description>
                The criteria scales. Mandatory if qualitative values are in performance table.
                If not provided, minimum/maximum values will be extracted from performance table,
                and preference direction will be set to max.
                Nominal and fuzzy qualitative scales are forbidden.
            </description>
        </documentation>
        <xmcda tag="criteriaScales" />
      </input>

      <input displayName="criteria segments" name="criteriaSegments" id="criteriaSegments" isoptional="0">
        <documentation>
            <description>Number of segments per marginal utility function.</description>
        </documentation>
        <xmcda tag="criteriaValues" />
      </input>

    <input id="parameters" name="parameters" displayName="parameters" isoptional="0">
          <documentation>
            <description>Parameters of the method</description>
          </documentation>
          <xmcda tag="programParameters"><![CDATA[
    <programParameters>
        <!-- %N -->
        <programParameter id="discrimination_threshold" name="Discrimination threshold">
	        <values>
                <value>
		            <real>%2</real>
                </value>
	        </values>
        </programParameter>
        <programParameter id="monotonicity_threshold" name="Monotonicity threshold">
	        <values>
                <value>
		            <real>%3</real>
                </value>
	        </values>
        </programParameter>
        <programParameter id="ac_epsilon" name="Analytic center epsilon">
	        <values>
                <value>
		            <real>%7</real>
                </value>
	        </values>
        </programParameter>
        <programParameter id="newton_step_coeff" name="Newton step coefficient">
	        <values>
                <value>
		            <real>%8</real>
                </value>
	        </values>
        </programParameter>
        <programParameter id="significative_figures" name="Significative figures">
	        <values>
                <value>
		            <integer>%5</integer>
                </value>
	        </values>
        </programParameter>
        <programParameter id="atol" name="Absolute tolerance">
	        <values>
                <value>
		            <real>%6</real>
                </value>
	        </values>
        </programParameter>
        <programParameter id="solver" name="Solver">
	        <values>
                <value>
		            <label>%4</label>
                </value>
	        </values>
        </programParameter>
    </programParameters>
]]></xmcda>
          <gui status="preferGUI">
            <entry id="%N" type="enum" displayName="Inputs alternatives">
              <documentation>
                <description>How to provide alternatives hierarchy</description>
              </documentation>
              <items>
                  <item id="ranking">
                      <description>Use ranking of alternatives</description>
                      <value>ranking</value>
                  </item>
                  <item id="relations">
                      <description>Use pairwise preferences and indifferences</description>
                      <value>relations</value>
                  </item>
              </items>
              <defaultValue>ranking</defaultValue>
            </entry>
            <entry id="%2" type="float" displayName="Discrimination threshold">
              <documentation>
                <description>Discrimination threshold value.</description>
              </documentation>
              <constraint>
                <description>Bigger than 0.0.</description>
                <code><![CDATA[%2 > 0]]></code>
              </constraint>
              <defaultValue>0.001</defaultValue>
            </entry>
            <entry id="%3" type="float" displayName="Monotonicity threshold">
              <documentation>
                <description>Monotonicity threshold value.</description>
              </documentation>
              <constraint>
                <description>Bigger than or equal to 0.0.</description>
                <code><![CDATA[%3 >= 0]]></code>
              </constraint>
              <defaultValue>0.001</defaultValue>
            </entry>
            <entry id="%7" type="float" displayName="Analytic center epsilon">
              <documentation>
                <description>Absolute tolerance for analytic center computation (closeness to optimal solution).</description>
              </documentation>
              <constraint>
                <description>Bigger than 0.0.</description>
                <code><![CDATA[%7 > 0]]></code>
              </constraint>
              <defaultValue>1e-6</defaultValue>
            </entry>
            <entry id="%8" type="float" displayName="Newton step coefficient">
              <documentation>
                <description>Coefficient applied to compute newton steps in analytic center computation.</description>
              </documentation>
              <constraint>
                <description>Bigger than 0.0.</description>
                <code><![CDATA[%8 > 0 && %8 < 1]]></code>
              </constraint>
              <defaultValue>0.9</defaultValue>
            </entry>
            <entry id="%5" type="int" displayName="Significative figures">
              <documentation>
                <description>Number of significative figures in outputs values.</description>
              </documentation>
              <constraint>
                <description>Bigger than 0.</description>
                <code><![CDATA[%5 > 0]]></code>
              </constraint>
              <defaultValue>3</defaultValue>
            </entry>
            <entry id="%6" type="float" displayName="Absolute tolerance">
              <documentation>
                <description>Float absolute tolerance used when computing Kendall tau.</description>
              </documentation>
              <constraint>
                <description>Bigger than 0.0.</description>
                <code><![CDATA[%6 > 0]]></code>
              </constraint>
              <defaultValue>1e-6</defaultValue>
            </entry>
            <entry id="%4" type="enum" displayName="Solver">
              <documentation>
                <description>Which solver is used.</description>
              </documentation>
              <items>
                  <item id="cbc">
                      <description>Coin-Or CBC (version included in Pulp)</description>
                      <value>cbc</value>
                  </item>
                  <item id="choco">
                      <description>Choco (version included in Pulp)</description>
                      <value>choco</value>
                  </item>
                  <item id="glpk">
                      <description>GLPK</description>
                      <value>glpk</value>
                  </item>
              </items>
              <defaultValue>cbc</defaultValue>
            </entry>
          </gui>
        </input>

    <output displayName="value functions" name="valueFunctions" id="valueFunctions">
        <documentation>
            <description>Optimal value functions found by the service.</description>
        </documentation>
        <xmcda tag="criteriaFunctions" />
    </output>

    <output displayName="values errors" name="valuesErrors" id="valuesErrors">
        <documentation>
            <description>
                Optimal values errors found by the service for the reference alternatives.
                The 2 values for each alternatives are respectively sigma+ and sigma-.
                As noted in (Munda, 2005).
            </description>
        </documentation>
        <xmcda tag="alternativesValues" />
    </output>

    <output displayName="messages" name="messages" id="messages">
        <documentation>
            <description>Status messages.</description>
        </documentation>
        <xmcda tag="programExecutionResult" />
    </output>
  </parameters>
</program_description>
