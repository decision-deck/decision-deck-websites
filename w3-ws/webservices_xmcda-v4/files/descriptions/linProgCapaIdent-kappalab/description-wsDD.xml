<?xml version='1.0' encoding='utf-8'?>
<program_description>
    <program provider="kappalab" name="linProgCapaIdent" version="1.0" displayName="linProgCapaIdent" />
    <documentation>
        <description>Identifies a Mobius capacity by means of an approach using the linear programming approach proposed by Marichal and Roubens. Roughly speaking, this function determines, if it exists, the capacity compatible with a set of linear constraints that "separates" the most the provided alternatives.</description>
        <contact><![CDATA[Patrick Meyer (patrick.meyer@telecom-bretagne.eu)]]></contact>
        <reference>M. Grabisch and M. Roubens (2000), Application of the Choquet Integral in Multicriteria Decision Making, in: Fuzzy Measures and Integrals: Theory and Applications, M. Grabisch, T. Murofushi, and M. Sugeno Eds, Physica Verlag, pages 415-434. </reference>
    </documentation>
    <parameters>

        <input id="criteria" name="criteria" displayName="criteria" isoptional="0">
            <documentation>
                <description>A list of criteria. Criteria can be activated or desactivated via the &lt;active&gt; tag (true or false). By default (no &lt;active&gt; tag), criteria are considered as active.</description>
            </documentation>
            <xmcda tag="criteria"><![CDATA[
	<criteria>
		<criterion>
			<active>[...]</active>
			[...]
		</criterion>
	    [...]
	</criteria>
]]></xmcda>
        </input>

        <input id="alternatives" name="alternatives" displayName="alternatives" isoptional="0">
            <documentation>
                <description>A list of alternatives. Alternatives can be activated or desactivated via the &lt;active&gt; tag (true or false). By default (no &lt;active&gt; tag), alternatives are considered as active.</description>
            </documentation>
            <xmcda tag="alternatives"><![CDATA[
	<alternatives>
		<alternative id="..." [...]>
			<active>[...]</active>
		</alternative>
		[...]
	</alternatives>
]]></xmcda>
        </input>

        <input id="performanceTable" name="performanceTable" displayName="performanceTable" isoptional="0">
            <documentation>
                <description>A performance table. The evaluations should be numeric values, i.e. &lt;real&gt;, &lt;integer&gt; or &lt;rational&gt;.</description>
            </documentation>
            <xmcda tag="performanceTable" />
        </input>

        <input id="shapleyPreorder" name="shapleyPreorder" displayName="shapleyPreorder" isoptional="1">
            <documentation>
                <description>A valued relation on criteria expressing importance constraints on the critera. A numeric &lt;value&gt; indicates a minimal preference threshold for each &lt;pair&gt;. One &lt;pair&gt; represents an affirmation of the type "the Shapley importance index of criterion g1 is greater than the Shapley importance index of criterion g2 with preference threshold delta".</description>
            </documentation>
            <xmcda tag="criteriaMatrix" />
        </input>

        <input id="interactionPreorder" name="interactionPreorder" displayName="interactionPreorder" isoptional="1">
            <documentation>
                <description>A valued relation on pairs of criteria expressing constraints on value of the the Shapley interaction index. A numeric &lt;value&gt; indicates a minimal preference threshold for each &lt;pair&gt; of the relation. One &lt;pair&gt; represents a constraint of the type "the Shapley interaction index of the pair (g1,g2) of criteria is greater than the Shapley interaction index of the pair (g3,g4) of criteria with preference threshold delta".</description>
            </documentation>
            <xmcda tag="criteriaSets,criteriaSetsMatrix" />
        </input>

        <input id="shapleyInterval" name="shapleyInterval" displayName="shapleyInterval" isoptional="1">
            <documentation>
                <description>A list of &lt;criterionValue&gt; containing the constraints relative to the quantitative importance of the criteria. Each &lt;criterionValue&gt; contains an an &lt;interval&gt;. Each &lt;criteriaValue&gt; represents an affirmation of the type "the Shapley importance index of criterion g1 lies in the interval [a,b]".</description>
            </documentation>
            <xmcda tag="criteriaValues" />
        </input>

        <input id="interactionInterval" name="interactionInterval" displayName="interactionInterval" isoptional="1">
            <documentation>
                <description>A list of &lt;criterionValue&gt; containing the constraints relative to the type and the magnitude of the Shapley interaction index for pairs of criteria. Each &lt;criterionValue&gt; contains an an &lt;interval&gt;. Each &lt;criteriaValue&gt; represents an affirmation of the type "the Shapley interaction index of the pair (g1,g2) of criteria lies in the interval [a,b]".</description>
            </documentation>
            <xmcda tag="criteriaSets,criteriaSetsValues" />
        </input>

        <input id="alternativesPreorder" name="alternativesPreorder" displayName="alternativesPreorder" isoptional="0">
            <documentation>
                <description>A valued relation relative to the preorder of the alternatives. A numeric &lt;value&gt; indicates a minimal preference threshold for each &lt;pair&gt; of the relation. One &lt;pair&gt; represents a constraint of the type "alternative a is preferred to alternative b with preference threshold delta".</description>
            </documentation>
            <xmcda tag="alternativesMatrix" />
        </input>

        <input id="kAdditivity" name="kAdditivity" displayName="kAdditivity" isoptional="0">
            <documentation>
                <description>Indicates the level of k-additivity of the Mobius capacity (the Mobius transform of subsets whose cardinal is superior to k vanishes).</description>
            </documentation>
            <xmcda tag="programParameters"><![CDATA[
	<programParameters>
		<parameter name="kAdditivity"> <!-- name: REQUIRED -->
			<values>
				<value>
					<integer>%1</integer>
				</value>
			</values>
		</parameter>
	</programParameters>
]]></xmcda>
            <gui status="preferFile">
                <entry id="%1" type="int" displayName="kAdditivity">
                    <documentation>
                        <description>Indicates the level of k-additivity of the Mobius capacity (the Mobius transform of subsets whose cardinal is superior to k vanishes).</description>
                    </documentation>
                    <constraint>
                        <description>The value should be a positive integer, less than or equal to the number of criteria.</description>
                        <code><![CDATA[ %1 > 0 ]]></code>
                    </constraint>
                    <defaultValue>1</defaultValue>
                </entry>
            </gui>
        </input>

        <output id="mobiusCapacity" name="mobiusCapacity" displayName="mobiusCapacity">
            <documentation>
                <description>The Mobius transform of a capacity.</description>
            </documentation>
            <xmcda tag="criteriaSets,criteriaSetsValues" />
        </output>

        <output id="messages" name="messages" displayName="messages">
            <documentation>
                <description>A list of messages generated by the algorithm.</description>
            </documentation>
            <xmcda tag="programExecutionResult" />
        </output>

    </parameters>
</program_description>
