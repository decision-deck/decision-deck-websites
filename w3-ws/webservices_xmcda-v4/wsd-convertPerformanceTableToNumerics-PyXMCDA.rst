:orphan:

.. _convertPerformanceTableToNumerics-PyXMCDA:

convertPerformanceTableToNumerics
=================================

:Version: 1.1
:Provider: PyXMCDA
:SOAP service's name: ``convertPerformanceTableToNumerics-PyXMCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Numerize performance table using provided criteriaScales.

N.B: Nominal scales are not supported.

**Contact:** Nicolas Duminy (nicolas.duminy@telecom-bretagne.eu)

**Web page:** https://gitlab.com/nduminy/ws-pyxmcda


Inputs
------
(For outputs, see :ref:`below <convertPerformanceTableToNumerics-PyXMCDA_outputs>`)


- :ref:`performanceTable <convertPerformanceTableToNumerics-PyXMCDA-performanceTable>`
- :ref:`criteriaScales <convertPerformanceTableToNumerics-PyXMCDA-criteriaScales>`
- :ref:`parameters <convertPerformanceTableToNumerics-PyXMCDA-parameters>` *(optional)*


.. _convertPerformanceTableToNumerics-PyXMCDA-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

The performance table.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _convertPerformanceTableToNumerics-PyXMCDA-criteriaScales:

criteriaScales
~~~~~~~~~~~~~~

The criteria scales defined.

The input value should be a valid XMCDA document whose main tag is ``<criteriaScales>``.


------------------------


.. _convertPerformanceTableToNumerics-PyXMCDA-parameters:

parameters *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~

Parameters of the method

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
    <programParameters>
        <programParameter id="normalize" name="Normalize">
            <values>
                <value>
                    <boolean>%1</boolean>
                </value>
            </values>
        </programParameter>
    </programParameters>


where:

- **%1** is a parameter named "Normalize". This is a boolean.
  The default value is false.


------------------------



.. _convertPerformanceTableToNumerics-PyXMCDA_outputs:

Outputs
-------


- :ref:`performanceTable <convertPerformanceTableToNumerics-PyXMCDA-numericPerformanceTable>`
- :ref:`messages <convertPerformanceTableToNumerics-PyXMCDA-messages>`


.. _convertPerformanceTableToNumerics-PyXMCDA-numericPerformanceTable:

performanceTable
~~~~~~~~~~~~~~~~

The numerized performance table.

The returned value is a XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _convertPerformanceTableToNumerics-PyXMCDA-messages:

messages
~~~~~~~~

Status messages.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/convertPerformanceTableToNumerics-PyXMCDA/description-wsDD.xml>`
