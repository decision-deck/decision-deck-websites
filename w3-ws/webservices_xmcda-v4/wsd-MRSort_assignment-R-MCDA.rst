.. _MRSort_assignment-R-MCDA:

MRSort_assignment
=================

:Version: 1.0
:Provider: R-MCDA
:SOAP service's name: ``MRSort_assignment-R-MCDA`` (see :ref:`soap-requests` for details)

Description
-----------

This simplification of the Electre TRI method uses the pessimistic assignment rule, without indifference or preference thresholds attached to criteria.
Only a binary discordance condition is considered, i.e. a veto forbids an outranking in any possible concordance situation, or not.

**Contact:** Alexandru Olteanu (alexandru.olteanu@univ-ubs.fr)

**Reference:** Bouyssou, D. and Marchant, T. An axiomatic approach to noncompensatory sorting methods in MCDM, II: more than two categories. European Journal of Operational Research, 178(1): 246--276, 2007.


Inputs
------
(For outputs, see :ref:`below <MRSort_assignment-R-MCDA_outputs>`)


- :ref:`alternatives <MRSort_assignment-R-MCDA-inalt>`
- :ref:`categories <MRSort_assignment-R-MCDA-incateg>`
- :ref:`performanceTable <MRSort_assignment-R-MCDA-inperf>`
- :ref:`categoriesProfilesPerformanceTable <MRSort_assignment-R-MCDA-incatprofpt>`
- :ref:`vetoProfilesPerformanceTable <MRSort_assignment-R-MCDA-invetoprofpt>` *(optional)*
- :ref:`criteria <MRSort_assignment-R-MCDA-incrit>`
- :ref:`criteriaWeights <MRSort_assignment-R-MCDA-weights>`
- :ref:`categoriesProfiles <MRSort_assignment-R-MCDA-incatprof>`
- :ref:`vetoProfiles <MRSort_assignment-R-MCDA-invetoprof>` *(optional)*
- :ref:`categoriesRanks <MRSort_assignment-R-MCDA-incategval>`
- :ref:`majorityThreshold <MRSort_assignment-R-MCDA-majority>`


.. _MRSort_assignment-R-MCDA-inalt:

alternatives
~~~~~~~~~~~~

A complete list of alternatives to be considered by the MR-Sort method.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _MRSort_assignment-R-MCDA-incateg:

categories
~~~~~~~~~~

A list of categories to which the alternatives will be assigned.

The input value should be a valid XMCDA document whose main tag is ``<categories>``.


------------------------


.. _MRSort_assignment-R-MCDA-inperf:

performanceTable
~~~~~~~~~~~~~~~~

The evaluations of the alternatives on the set of criteria.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _MRSort_assignment-R-MCDA-incatprofpt:

categoriesProfilesPerformanceTable
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The evaluations of the category profiles.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _MRSort_assignment-R-MCDA-invetoprofpt:

vetoProfilesPerformanceTable *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The evaluations of the veto profiles.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _MRSort_assignment-R-MCDA-incrit:

criteria
~~~~~~~~

A list of criteria on which the alternatives are evaluated. For each criterion, the preference direction should be provided.

The input value should be a valid XMCDA document whose main tag is ``<criteriaScales>``.


------------------------


.. _MRSort_assignment-R-MCDA-weights:

criteriaWeights
~~~~~~~~~~~~~~~

The criteria weights.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _MRSort_assignment-R-MCDA-incatprof:

categoriesProfiles
~~~~~~~~~~~~~~~~~~

The categories delimiting profiles.

The input value should be a valid XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _MRSort_assignment-R-MCDA-invetoprof:

vetoProfiles *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~

The categories veto profiles.

The input value should be a valid XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _MRSort_assignment-R-MCDA-incategval:

categoriesRanks
~~~~~~~~~~~~~~~

A list of categories ranks, 1 stands for the most preferred category and the higher the number the lower the preference for that category.

The input value should be a valid XMCDA document whose main tag is ``<categoriesValues>``.


------------------------


.. _MRSort_assignment-R-MCDA-majority:

majorityThreshold
~~~~~~~~~~~~~~~~~

The majority threshold.

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.


------------------------



.. _MRSort_assignment-R-MCDA_outputs:

Outputs
-------


- :ref:`alternativesAssignments <MRSort_assignment-R-MCDA-outaffect>`
- :ref:`messages <MRSort_assignment-R-MCDA-msg>`


.. _MRSort_assignment-R-MCDA-outaffect:

alternativesAssignments
~~~~~~~~~~~~~~~~~~~~~~~

The alternatives assignments to categories.

The returned value is a XMCDA document whose main tag is ``<alternativesAssignments>``.


------------------------


.. _MRSort_assignment-R-MCDA-msg:

messages
~~~~~~~~

Messages from the execution of the webservice. Possible errors in the input data will be given here.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/MRSort_assignment-R-MCDA/description-wsDD.xml>`
