.. _DEASMAACCRRanks-PUT:

DEASMAACCRRanks
===============

:Version: 1.0
:Provider: PUT
:SOAP service's name: ``DEASMAACCRRanks-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Computes rankings for the given DMUs (alternatives) using SMAA-D method and CCR Data Envelopment Analysis Model. For given number of samples  returns a matrix with alternatives in each row and rankings in each column. Single cell indicates how many samples of respective alternative gave respective position in ranking.

**Contact:** Malgorzata Napieraj (napieraj.malgorzata@gmail.com)

**Reference:** Cooper W., Seiford L., Tone K., Data Envelopment Analysis: A Comprehensive Text with Models, Applications, References and DEA-Solver (2007).

**Reference:** Lahdelma R., Salminen P., Stochastic multicriteria acceptability analysis using the data envelopment model (2004).


Inputs
------
(For outputs, see :ref:`below <DEASMAACCRRanks-PUT_outputs>`)


- :ref:`inputsOutputs <DEASMAACCRRanks-PUT-inputsOutputs>`
- :ref:`units <DEASMAACCRRanks-PUT-units>`
- :ref:`performanceTable <DEASMAACCRRanks-PUT-performanceTable>`
- :ref:`weightsLinearConstraints <DEASMAACCRRanks-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <DEASMAACCRRanks-PUT-methodParameters>`


.. _DEASMAACCRRanks-PUT-inputsOutputs:

inputsOutputs
~~~~~~~~~~~~~

A list of criteria with specified preference direction. List has to contains at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output).

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   <criteria>
                        <criterion>
							<scale>
								<quantitative>
									<preferenceDirection>
										[...]
									</preferenceDirection>
								</quantitative>
							</scale>
                            [...]
                        </criterion>
                        [...]
                    </criteria>


------------------------


.. _DEASMAACCRRanks-PUT-units:

units
~~~~~

A list of alternatives (DMUs).

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>


------------------------


.. _DEASMAACCRRanks-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A list of alternatives (DMUs) performances.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   <performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>


------------------------


.. _DEASMAACCRRanks-PUT-weightsLinearConstraints:

weightsLinearConstraints *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of criteria linear constraints.

The input value should be a valid XMCDA document whose main tag is ``<criteriaLinearConstraints>``.
It must have the following form::

   <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID>[...]</criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>


------------------------


.. _DEASMAACCRRanks-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~

"samplesNo" represents the number of samples to generate.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   <methodParameters>
							<parameter name="samplesNo">
								<value><integer>%1</integer></value>
							</parameter>
					</methodParameters>

where:

- **%1** is a parameter named "samplesNo". This is a int, and the value should conform to the following constraint: The value should be a positive integer..  More formally, the constraint is::

      %1 > 0 
  The default value is 100.


------------------------



.. _DEASMAACCRRanks-PUT_outputs:

Outputs
-------


- :ref:`rankAcceptabilityIndices <DEASMAACCRRanks-PUT-rankAcceptabilityIndices>`
- :ref:`avgRank <DEASMAACCRRanks-PUT-avgRank>`
- :ref:`messages <DEASMAACCRRanks-PUT-messages>`


.. _DEASMAACCRRanks-PUT-rankAcceptabilityIndices:

rankAcceptabilityIndices
~~~~~~~~~~~~~~~~~~~~~~~~

A performance table for given alternatives. Single performance consists of attribute criterionID representing certain ranking, and a value representing ratio of samples attaining this ranking.

The returned value is a XMCDA document whose main tag is ``<performanceTable>``.
It has the following form::

   <performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>Rank [...]</criterionID>
									<value>
									[...]
									</value>
							</performance>
							[...]
						</alternativePerformances>
					</performanceTable>


------------------------


.. _DEASMAACCRRanks-PUT-avgRank:

avgRank
~~~~~~~

A list of alternatives with average rank (obtained for given sample).

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   <alternativesValues mcdaConcept="efficiency">
						<alternativeValue>
						  <alternativeID>[...]</alternativeID>
						  <value>
							[...]
						  </value>
						</alternativeValue>
						[...]
					</alternativesValues>


------------------------


.. _DEASMAACCRRanks-PUT-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/DEASMAACCRRanks-PUT/description-wsDD.xml>`
