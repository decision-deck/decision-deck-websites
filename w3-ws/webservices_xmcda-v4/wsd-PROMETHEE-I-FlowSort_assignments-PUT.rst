.. _PROMETHEE-I-FlowSort_assignments-PUT:

PROMETHEE-I-FlowSort_assignments
================================

:Version: 1.0.0
:Provider: PUT
:SOAP service's name: ``PROMETHEE-I-FlowSort_assignments-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Computes class assignment for given alternatives using FlowSort method based on Promethee I.

**Contact:** Maciej Uniejewski <maciej.uniejewski@gmail.com>

**Web page:** https://github.com/maciej7777/PrometheeDiviz


Inputs
------
(For outputs, see :ref:`below <PROMETHEE-I-FlowSort_assignments-PUT_outputs>`)


- :ref:`criteria <PROMETHEE-I-FlowSort_assignments-PUT-input6>`
- :ref:`alternatives <PROMETHEE-I-FlowSort_assignments-PUT-input1>`
- :ref:`categories <PROMETHEE-I-FlowSort_assignments-PUT-input2>`
- :ref:`performance_table <PROMETHEE-I-FlowSort_assignments-PUT-input5>`
- :ref:`criteria_scales <PROMETHEE-I-FlowSort_assignments-PUT-input7>`
- :ref:`positive_flows <PROMETHEE-I-FlowSort_assignments-PUT-input8>`
- :ref:`negative_flows <PROMETHEE-I-FlowSort_assignments-PUT-input9>`
- :ref:`categories_profiles <PROMETHEE-I-FlowSort_assignments-PUT-input3>`
- :ref:`categories_values <PROMETHEE-I-FlowSort_assignments-PUT-input4>`
- :ref:`method_parameters <PROMETHEE-I-FlowSort_assignments-PUT-input10>`


.. _PROMETHEE-I-FlowSort_assignments-PUT-input6:

criteria
~~~~~~~~

Criteria to consider.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _PROMETHEE-I-FlowSort_assignments-PUT-input1:

alternatives
~~~~~~~~~~~~

Alternatives to consider.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _PROMETHEE-I-FlowSort_assignments-PUT-input2:

categories
~~~~~~~~~~

Definitions of categories.

The input value should be a valid XMCDA document whose main tag is ``<categories>``.


------------------------


.. _PROMETHEE-I-FlowSort_assignments-PUT-input5:

performance_table
~~~~~~~~~~~~~~~~~

The performances of profiles.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _PROMETHEE-I-FlowSort_assignments-PUT-input7:

criteria_scales
~~~~~~~~~~~~~~~

Scales of considered criteria.

The input value should be a valid XMCDA document whose main tag is ``<criteriaScales>``.


------------------------


.. _PROMETHEE-I-FlowSort_assignments-PUT-input8:

positive_flows
~~~~~~~~~~~~~~

Positive flows of given alternatives and profiles.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _PROMETHEE-I-FlowSort_assignments-PUT-input9:

negative_flows
~~~~~~~~~~~~~~

Negative flows of given alternatives and profiles.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _PROMETHEE-I-FlowSort_assignments-PUT-input3:

categories_profiles
~~~~~~~~~~~~~~~~~~~

Definitions of profiles which should be used for classes (categories) representation.

The input value should be a valid XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _PROMETHEE-I-FlowSort_assignments-PUT-input4:

categories_values
~~~~~~~~~~~~~~~~~

Marks of categories (higher mark means better category). Each category need to have unique mark from 1 to C, where C is a number of categories.

The input value should be a valid XMCDA document whose main tag is ``<categoriesValues>``.


------------------------


.. _PROMETHEE-I-FlowSort_assignments-PUT-input10:

method_parameters
~~~~~~~~~~~~~~~~~

A set of parameters provided to tune up the module's operation.

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
                
                <programParameters>
                    <parameter id="comparisonWithProfiles">
                        <values>
                            <value>
                                <label>%1</label>
                            </value>
                        </values>
                    </parameter>
                </programParameters>
	        
            

where:

- **%1** is a parameter named "comparison with profiles". It can have the following values:

  - ``central``: central profiles

  - ``bounding``: boundary profiles

  The default value is central.


------------------------



.. _PROMETHEE-I-FlowSort_assignments-PUT_outputs:

Outputs
-------


- :ref:`assignments <PROMETHEE-I-FlowSort_assignments-PUT-output1>`
- :ref:`messages <PROMETHEE-I-FlowSort_assignments-PUT-output2>`


.. _PROMETHEE-I-FlowSort_assignments-PUT-output1:

assignments
~~~~~~~~~~~

Final assignments made in a FlowSort (Promethee I) process.

The returned value is a XMCDA document whose main tag is ``<alternativesAssignments>``.


------------------------


.. _PROMETHEE-I-FlowSort_assignments-PUT-output2:

messages
~~~~~~~~

Messages or errors generated by this module.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/PROMETHEE-I-FlowSort_assignments-PUT/description-wsDD.xml>`
