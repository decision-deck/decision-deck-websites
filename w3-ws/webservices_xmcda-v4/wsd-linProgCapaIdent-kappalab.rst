.. _linProgCapaIdent-kappalab:

linProgCapaIdent
================

:Version: 1.0
:Provider: kappalab
:SOAP service's name: ``linProgCapaIdent-kappalab`` (see :ref:`soap-requests` for details)

Description
-----------

Identifies a Mobius capacity by means of an approach using the linear programming approach proposed by Marichal and Roubens. Roughly speaking, this function determines, if it exists, the capacity compatible with a set of linear constraints that "separates" the most the provided alternatives.

**Contact:** Patrick Meyer (patrick.meyer@telecom-bretagne.eu)

**Reference:** M. Grabisch and M. Roubens (2000), Application of the Choquet Integral in Multicriteria Decision Making, in: Fuzzy Measures and Integrals: Theory and Applications, M. Grabisch, T. Murofushi, and M. Sugeno Eds, Physica Verlag, pages 415-434. 


Inputs
------
(For outputs, see :ref:`below <linProgCapaIdent-kappalab_outputs>`)


- :ref:`criteria <linProgCapaIdent-kappalab-criteria>`
- :ref:`alternatives <linProgCapaIdent-kappalab-alternatives>`
- :ref:`performanceTable <linProgCapaIdent-kappalab-performanceTable>`
- :ref:`shapleyPreorder <linProgCapaIdent-kappalab-shapleyPreorder>` *(optional)*
- :ref:`interactionPreorder <linProgCapaIdent-kappalab-interactionPreorder>` *(optional)*
- :ref:`shapleyInterval <linProgCapaIdent-kappalab-shapleyInterval>` *(optional)*
- :ref:`interactionInterval <linProgCapaIdent-kappalab-interactionInterval>` *(optional)*
- :ref:`alternativesPreorder <linProgCapaIdent-kappalab-alternativesPreorder>`
- :ref:`kAdditivity <linProgCapaIdent-kappalab-kAdditivity>`


.. _linProgCapaIdent-kappalab-criteria:

criteria
~~~~~~~~

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
	<criteria>
		<criterion>
			<active>[...]</active>
			[...]
		</criterion>
	    [...]
	</criteria>



------------------------


.. _linProgCapaIdent-kappalab-alternatives:

alternatives
~~~~~~~~~~~~

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
	<alternatives>
		<alternative id="..." [...]>
			<active>[...]</active>
		</alternative>
		[...]
	</alternatives>



------------------------


.. _linProgCapaIdent-kappalab-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A performance table. The evaluations should be numeric values, i.e. <real>, <integer> or <rational>.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _linProgCapaIdent-kappalab-shapleyPreorder:

shapleyPreorder *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A valued relation on criteria expressing importance constraints on the critera. A numeric <value> indicates a minimal preference threshold for each <pair>. One <pair> represents an affirmation of the type "the Shapley importance index of criterion g1 is greater than the Shapley importance index of criterion g2 with preference threshold delta".

The input value should be a valid XMCDA document whose main tag is ``<criteriaMatrix>``.


------------------------


.. _linProgCapaIdent-kappalab-interactionPreorder:

interactionPreorder *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A valued relation on pairs of criteria expressing constraints on value of the the Shapley interaction index. A numeric <value> indicates a minimal preference threshold for each <pair> of the relation. One <pair> represents a constraint of the type "the Shapley interaction index of the pair (g1,g2) of criteria is greater than the Shapley interaction index of the pair (g3,g4) of criteria with preference threshold delta".

The input value should be a valid XMCDA document whose main tag is ``<criteriaSets,criteriaSetsMatrix>``.


------------------------


.. _linProgCapaIdent-kappalab-shapleyInterval:

shapleyInterval *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of <criterionValue> containing the constraints relative to the quantitative importance of the criteria. Each <criterionValue> contains an an <interval>. Each <criteriaValue> represents an affirmation of the type "the Shapley importance index of criterion g1 lies in the interval [a,b]".

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _linProgCapaIdent-kappalab-interactionInterval:

interactionInterval *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of <criterionValue> containing the constraints relative to the type and the magnitude of the Shapley interaction index for pairs of criteria. Each <criterionValue> contains an an <interval>. Each <criteriaValue> represents an affirmation of the type "the Shapley interaction index of the pair (g1,g2) of criteria lies in the interval [a,b]".

The input value should be a valid XMCDA document whose main tag is ``<criteriaSets,criteriaSetsValues>``.


------------------------


.. _linProgCapaIdent-kappalab-alternativesPreorder:

alternativesPreorder
~~~~~~~~~~~~~~~~~~~~

A valued relation relative to the preorder of the alternatives. A numeric <value> indicates a minimal preference threshold for each <pair> of the relation. One <pair> represents a constraint of the type "alternative a is preferred to alternative b with preference threshold delta".

The input value should be a valid XMCDA document whose main tag is ``<alternativesMatrix>``.


------------------------


.. _linProgCapaIdent-kappalab-kAdditivity:

kAdditivity
~~~~~~~~~~~

Indicates the level of k-additivity of the Mobius capacity (the Mobius transform of subsets whose cardinal is superior to k vanishes).

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
	<programParameters>
		<parameter name="kAdditivity"> <!-- name: REQUIRED -->
			<values>
				<value>
					<integer>%1</integer>
				</value>
			</values>
		</parameter>
	</programParameters>


where:

- **%1** is a parameter named "kAdditivity". This is a int, and the value should conform to the following constraint: The value should be a positive integer, less than or equal to the number of criteria..  More formally, the constraint is::

      %1 > 0 
  The default value is 1.


------------------------



.. _linProgCapaIdent-kappalab_outputs:

Outputs
-------


- :ref:`mobiusCapacity <linProgCapaIdent-kappalab-mobiusCapacity>`
- :ref:`messages <linProgCapaIdent-kappalab-messages>`


.. _linProgCapaIdent-kappalab-mobiusCapacity:

mobiusCapacity
~~~~~~~~~~~~~~

The Mobius transform of a capacity.

The returned value is a XMCDA document whose main tag is ``<criteriaSets,criteriaSetsValues>``.


------------------------


.. _linProgCapaIdent-kappalab-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/linProgCapaIdent-kappalab/description-wsDD.xml>`
