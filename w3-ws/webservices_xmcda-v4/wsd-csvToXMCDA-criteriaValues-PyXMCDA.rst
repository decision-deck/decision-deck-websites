.. _csvToXMCDA-criteriaValues-PyXMCDA:

csvToXMCDA-criteriaValues
=========================

:Version: 2.0
:Provider: PyXMCDA
:SOAP service's name: ``csvToXMCDA-criteriaValues-PyXMCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Transforms a file containing criteria values from a comma-separated values (CSV) file to two XMCDA compliant files, containing the corresponding criteria ids and their criteriaValues.

**Contact:** Sébastien Bigaret (sebastien.bigaret@telecom-bretagne.eu)

**Web page:** https://gitlab.com/sbigaret/ws-pyxmcda


Inputs
------
(For outputs, see :ref:`below <csvToXMCDA-criteriaValues-PyXMCDA_outputs>`)


- :ref:`criteriaValues.csv <csvToXMCDA-criteriaValues-PyXMCDA-csv>`
- :ref:`parameters <csvToXMCDA-criteriaValues-PyXMCDA-parameters>`


.. _csvToXMCDA-criteriaValues-PyXMCDA-csv:

criteriaValues.csv
~~~~~~~~~~~~~~~~~~

The criteria and their criteria values as a CSV file.  The first line is made of two cells, the first one being empty, and the second one will be the content of the attribute "mcdaConcept" in the tag "<criteriaValues>", if supplied.  The following lines are made of at least two cells, with the first cell being an criterion' id (and name, see below), and the remaining cells their associated values.

Example::

    ,weights
    c1 (color),1,2,3,4,5
    c2 (price),3.14159

The first column contains the criteria' ids. Additionally, the criteria' names are also extracted when the cells are formatted like `id (name)`.  Set the parameter "First column" to "id" to deactivate the extraction of criteria' names.

By default the values are considered as float numbers.  This can be changed using the parameter "Default content".  It is possible to specify the type of a value by prepending it with a prefix:

- `float:` for floats (ex.: `1`, `1.2`, `1.2e3`)
- `integer:` for integers (decimal representation: `127`, hexadecimal: `0x7f`, octal: `0o177`, binary: `0b1111111`)
- `string:` for strings (note that a string with a colon should always be prefixed by `string:`, no matter what the default prefix is).
- `boolean:` for booleans: 1 or 'true' (case insensitive) are True values, everything else is false.
- `na:` for N/A (everything after the colon is ignored)

Example::

    ,weights
    c1,float:1.0
    c2,integer:2
    c3,3.03
    c4,string:a label
    c4b,string:another label
    c5,na:content ignored for N/A
    c6,boolean:1



The criteria and their criteria values as a CSV file.

Example (here, 'cost', 'risk' etc. are criteria ids)::

  ,cost,risk,employment,connection
  weights,1,2,3,4

or, when both criteria' ids and names are given (in the form: "id (name)")::

  ,c01 (Cost), c02 (risk), c03 (employment), c04 (connection)
  weights,1,2,3,4

The input value should be a valid XMCDA document whose main tag is ``<other>``.


------------------------


.. _csvToXMCDA-criteriaValues-PyXMCDA-parameters:

parameters
~~~~~~~~~~

Parameters of the method

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
    <programParameters>
        <parameter id="csv_delimiter">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </parameter>
        <parameter id="default_prefix">
            <values>
                <value>
                    <label>%2</label>
                </value>
            </values>
        </parameter>
        <parameter id="name_in_id">
            <values>
                <value>
                    <boolean>%3</boolean>
                </value>
            </values>
        </parameter>
    </programParameters>


where:

- **%1** is a parameter named "CSV delimiter". This is a string, and the value should conform to the following constraint: One character maximum.  More formally, the constraint is::

     %1.length() < 2
- **%2** is a parameter named "Default content". It can have the following values:

  - ``float``: float

  - ``label``: string

  - ``integer``: integer

  - ``boolean``: boolean

  The default value is float.

- **%3** is a parameter named "First column". It can have the following values:

  - ``false``: id

  - ``true``: id (name)

  The default value is id_and_name.


------------------------



.. _csvToXMCDA-criteriaValues-PyXMCDA_outputs:

Outputs
-------


- :ref:`criteria <csvToXMCDA-criteriaValues-PyXMCDA-criteria>`
- :ref:`criteriaValues <csvToXMCDA-criteriaValues-PyXMCDA-criteriaValues>`
- :ref:`messages <csvToXMCDA-criteriaValues-PyXMCDA-messages>`


.. _csvToXMCDA-criteriaValues-PyXMCDA-criteria:

criteria
~~~~~~~~

The equivalent criteria ids.

The returned value is a XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _csvToXMCDA-criteriaValues-PyXMCDA-criteriaValues:

criteriaValues
~~~~~~~~~~~~~~

The equivalent criteria values.

The returned value is a XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _csvToXMCDA-criteriaValues-PyXMCDA-messages:

messages
~~~~~~~~

Status messages.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/csvToXMCDA-criteriaValues-PyXMCDA/description-wsDD.xml>`
