.. _ElectreDistillationRank-PUT:

ElectreDistillationRank
=======================

:Version: 0.1.0
:Provider: PUT
:SOAP service's name: ``ElectreDistillationRank-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

This module provides three types of resultes performed on downwards and upwards distillation: intersection (outranking relation), final rank (distance to the best solution), median rank.

**Web page:** https://github.com/MTomczyk/ElectreDiviz


Inputs
------
(For outputs, see :ref:`below <ElectreDistillationRank-PUT_outputs>`)


- :ref:`alternatives <ElectreDistillationRank-PUT-input1>`
- :ref:`downwards <ElectreDistillationRank-PUT-input2>`
- :ref:`upwards <ElectreDistillationRank-PUT-input3>`


.. _ElectreDistillationRank-PUT-input1:

alternatives
~~~~~~~~~~~~

Alternatives to consider

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _ElectreDistillationRank-PUT-input2:

downwards
~~~~~~~~~

Product of downwards distillation

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _ElectreDistillationRank-PUT-input3:

upwards
~~~~~~~

Product of upwards distillation

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------



.. _ElectreDistillationRank-PUT_outputs:

Outputs
-------


- :ref:`intersection <ElectreDistillationRank-PUT-output1>`
- :ref:`rank <ElectreDistillationRank-PUT-output2>`
- :ref:`median <ElectreDistillationRank-PUT-output3>`
- :ref:`messages <ElectreDistillationRank-PUT-output4>`


.. _ElectreDistillationRank-PUT-output1:

intersection
~~~~~~~~~~~~

Intersection of downwards and upwards distillation

The returned value is a XMCDA document whose main tag is ``<alternativesMatrix>``.


------------------------


.. _ElectreDistillationRank-PUT-output2:

rank
~~~~

Ranks of alternatives calculated from intersection

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _ElectreDistillationRank-PUT-output3:

median
~~~~~~

Median ranking

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _ElectreDistillationRank-PUT-output4:

messages
~~~~~~~~

Messages or errors generated by this module.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/ElectreDistillationRank-PUT/description-wsDD.xml>`
