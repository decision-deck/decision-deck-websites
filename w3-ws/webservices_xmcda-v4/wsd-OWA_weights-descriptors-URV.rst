.. _OWA_weights-descriptors-URV:

OWADescriptors
==============

:Version: 4.0
:Provider: URV
:Name: OWA_weights-descriptors
:SOAP service's name: ``OWA_weights-descriptors-URV`` (see :ref:`soap-requests` for details)

Description
-----------

Compute measures of weights given to the Ordered Weighted Average. The Ordered Weighted Averaging operators, commonly called OWA operators, provide a parameterized class of mean type aggregation operators.
For OWA wights exists different measures to characterise a set of weights associated to an OWA operator. In this module we implement the measures: balance, divergence, entropy and ornes.

**Contact:** Aida Valls <aida.valls@urv.cat>

**Reference:** A. Valls, The Ordered Weighted Averaging Operator, In: Proc. IEEE International Conference on Fuzzy Systems, FUZZ-IEEE 2010, IEEE Computer Society, Barcelona, Catalonia, 2010, pp. 3063-3070.


Inputs
------
(For outputs, see :ref:`below <OWA_weights-descriptors-URV_outputs>`)


- :ref:`weightsOWA <OWA_weights-descriptors-URV-input0>`


.. _OWA_weights-descriptors-URV-input0:

weightsOWA
~~~~~~~~~~

The weights are associated to the values of the alternatives rather than to the criteria. In this way they can define different aggregation policies. Assuming that the values on the alternative will be sorted from the best to the worst, the list of weights must be ordered according to the importance that is given to the values, from the highest to the lowest. 
For example a list of weights as (0.5, 0.5, 0, 0, 0) is ignoring the 3 lowest values, and making an average of the two highest ones. A list like (0, 0, 1, 0 ,0 ) is calculating the median, while (0, 0, 0, 0, 1) is taking the minimum.
Notice that the sum of weights is required to be 1. In version 4.0 the weights have only one structure, where the user can specify her/his weights without using any identifier for each weight.

The input value should be a valid XMCDA document whose main tag is ``<criteriaSetsValues>``.
It must have the following form::

   
			
			<criteriaSetsValues>
				<criteriaSetValues id="owa-weights" mcdaConcept="OWA weights">
					<criteriaSetID>...</criteriaSetID>
					<values>
						<value>
							<real>...</real>
						</value>
						<value>
							[...]
						</value>
						[....]
					</values>
				</criteriaSetValues>
			</criteriaSetsValues>
		  	
			


------------------------



.. _OWA_weights-descriptors-URV_outputs:

Outputs
-------


- :ref:`balance <OWA_weights-descriptors-URV-output0>`
- :ref:`divergence <OWA_weights-descriptors-URV-output1>`
- :ref:`entropy <OWA_weights-descriptors-URV-output2>`
- :ref:`ornes <OWA_weights-descriptors-URV-output3>`
- :ref:`messages <OWA_weights-descriptors-URV-output4>`


.. _OWA_weights-descriptors-URV-output0:

balance
~~~~~~~

Result obtained apply balance calculation in weights table. It will be a single numeric value.

The returned value is a XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _OWA_weights-descriptors-URV-output1:

divergence
~~~~~~~~~~

Result obtained apply divergence calculation in weights table. It will be a single numeric value.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _OWA_weights-descriptors-URV-output2:

entropy
~~~~~~~

Result obtained apply entropy calculation in weights table. It will be a single numeric value.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _OWA_weights-descriptors-URV-output3:

ornes
~~~~~

Result obtained apply ornes calculation in weights table. It will be a single numeric value.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _OWA_weights-descriptors-URV-output4:

messages
~~~~~~~~

A status message.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/OWA_weights-descriptors-URV/description-wsDD.xml>`
