.. _HierarchicalDEA-CCR_extremeRanks-PUT:

HierarchicalDEA-CCR_extremeRanks
================================

:Version: 1.0
:Provider: PUT
:SOAP service's name: ``HierarchicalDEA-CCR_extremeRanks-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Computes the extreme efficiency ranks for the given DMUs (alternatives) using CCR Data Envelopment Analysis Model with hierarchical structure of outputs.

**Contact:** 
            Anna Labijak <anna.labijak@cs.put.poznan.pl>
        


Inputs
------
(For outputs, see :ref:`below <HierarchicalDEA-CCR_extremeRanks-PUT_outputs>`)


- :ref:`units <HierarchicalDEA-CCR_extremeRanks-PUT-units>`
- :ref:`performanceTable <HierarchicalDEA-CCR_extremeRanks-PUT-performanceTable>`
- :ref:`hierarchy <HierarchicalDEA-CCR_extremeRanks-PUT-hierarchy>`
- :ref:`weightsLinearConstraints <HierarchicalDEA-CCR_extremeRanks-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <HierarchicalDEA-CCR_extremeRanks-PUT-methodParameters>`


.. _HierarchicalDEA-CCR_extremeRanks-PUT-units:

units
~~~~~

A list of alternatives (DMUs).

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
                <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
            


------------------------


.. _HierarchicalDEA-CCR_extremeRanks-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A list of alternatives (DMUs) performances (on leaf hierarchy criteria).

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   
                <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<values><value>[...]</value></values>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>
            


------------------------


.. _HierarchicalDEA-CCR_extremeRanks-PUT-hierarchy:

hierarchy
~~~~~~~~~

The hierarchical structure of criteria.

The input value should be a valid XMCDA document whose main tag is ``<criteriaHierarchy>``.
It must have the following form::

   
                <criteriaHierarchy>
						<nodes>
                            <node>
                                <criterionID>[...]</criterionID>
                                <nodes>
                                    <node>
                                        <criterionID>[...]</criterionID>
                                        [...]
                                    </node>
                                    [...]
                                </nodes>
                            </node>
                        <nodes>
					</criteriaHierarchy>
            


------------------------


.. _HierarchicalDEA-CCR_extremeRanks-PUT-weightsLinearConstraints:

weightsLinearConstraints *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of hierarchy criteria linear constraints.

The input value should be a valid XMCDA document whose main tag is ``<criteriaLinearConstraints>``.
It must have the following form::

   
                
            <criteriaLinearConstraints>
                <constraints>
                    <constraint>
                        <elements>
                            <element>
                                <criterionID> [...] </criterionID>
                                <coefficient>
                                    [...]
                                </coefficient>
                            </element>
                            [...]
                        </elements>
                        <operator>[...]</operator>
                        <rhs>
                            [...]
                        </rhs>
                    </constraint>
                    [...]
                </constraints>
            </criteriaLinearConstraints>
            


------------------------


.. _HierarchicalDEA-CCR_extremeRanks-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~

Represents parameters (hierarchyNode).

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
                
    <programParameters>
        <parameter id="hierarchyNode">
            <values>
                <value><label>%1</label></value>
            </values>
        </parameter>
    </programParameters>
            

where:

- **%1** is a parameter named "hierarchyNode". This is a string.
  The default value is root.


------------------------



.. _HierarchicalDEA-CCR_extremeRanks-PUT_outputs:

Outputs
-------


- :ref:`bestRank <HierarchicalDEA-CCR_extremeRanks-PUT-bestRank>`
- :ref:`worstRank <HierarchicalDEA-CCR_extremeRanks-PUT-worstRank>`
- :ref:`messages <HierarchicalDEA-CCR_extremeRanks-PUT-messages>`


.. _HierarchicalDEA-CCR_extremeRanks-PUT-bestRank:

bestRank
~~~~~~~~

A list of alternatives with computed best rank for each of them.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   
                <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
						  <values>
							 <value>[...]</value>
						  </values>
						</alternativeValue>
						[...]
					</alternativesValues>
            


------------------------


.. _HierarchicalDEA-CCR_extremeRanks-PUT-worstRank:

worstRank
~~~~~~~~~

A list of alternatives with computed worst rank for each of them.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   
                <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
						  <values>
							 <value>[...]</value>
						  </values>
						</alternativeValue>
						[...]
					</alternativesValues>
            


------------------------


.. _HierarchicalDEA-CCR_extremeRanks-PUT-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/HierarchicalDEA-CCR_extremeRanks-PUT/description-wsDD.xml>`
