.. _csvToXMCDA-categoriesProfiles-PyXMCDA:

csvToXMCDA-categoriesProfiles
=============================

:Version: 2.0
:Provider: PyXMCDA
:SOAP service's name: ``csvToXMCDA-categoriesProfiles-PyXMCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Transforms a file containing categories profiles from a comma-separated values (CSV) file to XMCDA compliant files.

**Contact:** Sébastien Bigaret (sebastien.bigaret@telecom-bretagne.eu)

**Web page:** https://gitlab.com/sbigaret/ws-pyxmcda


Inputs
------
(For outputs, see :ref:`below <csvToXMCDA-categoriesProfiles-PyXMCDA_outputs>`)


- :ref:`categoriesProfiles.csv <csvToXMCDA-categoriesProfiles-PyXMCDA-categoriesProfiles_csv>`
- :ref:`parameters <csvToXMCDA-categoriesProfiles-PyXMCDA-parameters>`


.. _csvToXMCDA-categoriesProfiles-PyXMCDA-categoriesProfiles_csv:

categoriesProfiles.csv
~~~~~~~~~~~~~~~~~~~~~~

The categories profiles as a CSV file.

Example::

  cat1 (Bad),,a1
  cat22 (Medium),a1,a2
  cat3 (Good),a2,

The first column consists in categories ids (and names), in the form `id (name)` --the name between parenthesis is optional, and there is a parameter to disable the lookup of names altogether.

Each lines consists in 3 elements: the category, then the ids of the alternatives being respectively the lower bound and the upper bound of the category's profile.  Either one may be empty (but not both).

The input value should be a valid XMCDA document whose main tag is ``<other>``.


------------------------


.. _csvToXMCDA-categoriesProfiles-PyXMCDA-parameters:

parameters
~~~~~~~~~~

Parameters of the program

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
    <programParameters>
        <parameter id="csv_delimiter">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </parameter>
        <parameter id="name_in_id">
            <values>
                <value>
                    <boolean>%2</boolean>
                </value>
            </values>
        </parameter>
    </programParameters>


where:

- **%1** is a parameter named "CSV delimiter". This is a string, and the value should conform to the following constraint: One character maximum.  More formally, the constraint is::

     %1.length() < 2
- **%2** is a parameter named "First column". It can have the following values:

  - ``false``: id

  - ``true``: id (name)

  The default value is id_and_name.


------------------------



.. _csvToXMCDA-categoriesProfiles-PyXMCDA_outputs:

Outputs
-------


- :ref:`categoriesProfiles <csvToXMCDA-categoriesProfiles-PyXMCDA-categoriesProfiles>`
- :ref:`messages <csvToXMCDA-categoriesProfiles-PyXMCDA-messages>`


.. _csvToXMCDA-categoriesProfiles-PyXMCDA-categoriesProfiles:

categoriesProfiles
~~~~~~~~~~~~~~~~~~

The equivalent categories profiles.

The returned value is a XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _csvToXMCDA-categoriesProfiles-PyXMCDA-messages:

messages
~~~~~~~~

Status messages.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/csvToXMCDA-categoriesProfiles-PyXMCDA/description-wsDD.xml>`
