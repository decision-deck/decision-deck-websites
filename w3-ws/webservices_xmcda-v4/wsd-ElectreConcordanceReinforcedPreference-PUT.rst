.. _ElectreConcordanceReinforcedPreference-PUT:

ElectreConcordanceReinforcedPreference
======================================

:Version: 0.1.0
:Provider: PUT
:SOAP service's name: ``ElectreConcordanceReinforcedPreference-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Computes concordance matrix using procedure which is common to the most methods from the Electre family.

This module is an extended version of 'ElectreConcordance' - it brings the concept of 'reinforced_preference', which boils down to the new threshold of the same name and a new input file where the 'reinforcement factors' are defined (one for each criterion where 'reinforced_preference' threshold is present).

**Web page:** http://github.com/xor-xor/electre_diviz

**Reference:** Bernard Roy and Roman Słowiński; Handling effects of reinforced preference and counter-veto in credibility of outranking; European Journal of Operational Research 188(1):185–190; 2008; doi:10.1016/j.ejor.2007.04.005


Inputs
------
(For outputs, see :ref:`below <ElectreConcordanceReinforcedPreference-PUT_outputs>`)


- :ref:`alternatives <ElectreConcordanceReinforcedPreference-PUT-input1>`
- :ref:`criteria <ElectreConcordanceReinforcedPreference-PUT-input3>`
- :ref:`criteria_scales <ElectreConcordanceReinforcedPreference-PUT-input3b>`
- :ref:`criteria_thresholds <ElectreConcordanceReinforcedPreference-PUT-input3c>` *(optional)*
- :ref:`performance_table <ElectreConcordanceReinforcedPreference-PUT-input4>`
- :ref:`profiles_performance_table <ElectreConcordanceReinforcedPreference-PUT-input5>` *(optional)*
- :ref:`weights <ElectreConcordanceReinforcedPreference-PUT-input6>`
- :ref:`reinforcement_factors <ElectreConcordanceReinforcedPreference-PUT-input7>` *(optional)*
- :ref:`classes_profiles <ElectreConcordanceReinforcedPreference-PUT-input2>` *(optional)*
- :ref:`method_parameters <ElectreConcordanceReinforcedPreference-PUT-input8>`


.. _ElectreConcordanceReinforcedPreference-PUT-input1:

alternatives
~~~~~~~~~~~~

Alternatives to consider.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _ElectreConcordanceReinforcedPreference-PUT-input3:

criteria
~~~~~~~~

Criteria to consider, possibly with 'preference', 'indifference' and 'reinforced preference' thresholds (see also 'reinforcement_factors' input below). Each criterion must have a preference direction specified (min or max). It is worth mentioning that this module allows to define thresholds as constants as well as linear functions.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _ElectreConcordanceReinforcedPreference-PUT-input3b:

criteria_scales
~~~~~~~~~~~~~~~

The scales of the criteria to consider. Each criterion must have a preference direction specified (min or max).

The input value should be a valid XMCDA document whose main tag is ``<criteriaScales>``.


------------------------


.. _ElectreConcordanceReinforcedPreference-PUT-input3c:

criteria_thresholds *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The criteria' 'preference', 'indifference' and 'reinforced preference' thresholds (see also 'reinforcement_factors' input below). It is worth mentioning that this module allows to define thresholds as constants as well as linear functions.

The input value should be a valid XMCDA document whose main tag is ``<criteriaThresholds>``.


------------------------


.. _ElectreConcordanceReinforcedPreference-PUT-input4:

performance_table
~~~~~~~~~~~~~~~~~

The performance of alternatives.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _ElectreConcordanceReinforcedPreference-PUT-input5:

profiles_performance_table *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The performance of profiles (boundary or central).

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _ElectreConcordanceReinforcedPreference-PUT-input6:

weights
~~~~~~~

Weights of criteria to consider.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _ElectreConcordanceReinforcedPreference-PUT-input7:

reinforcement_factors *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Definitions of so-called 'reinforcement factors', one per each criterion for which 'reinforcement threshold' has been defined. For more regarding these concepts see the paper from 'Reference' section.

Technically, this input is optional, but it doesn't make much sense to use 'ElectreConcordanceReinforcedPreference' without it, since in such scenario it will fall back to 'ElectreConcordance'.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _ElectreConcordanceReinforcedPreference-PUT-input2:

classes_profiles *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Definitions of profiles (boundary or central) which should be used for classes (categories) representation.

The input value should be a valid XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _ElectreConcordanceReinforcedPreference-PUT-input8:

method_parameters
~~~~~~~~~~~~~~~~~

This parameter specifies the type of elements provided for comparison.

Choosing 'boundary_profiles' or 'central_profiles' requires providing inputs 'classes_profiles' and 'profiles_performance_table' as well (which are optional by default).

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
<programParameters>
	<parameter name="comparison_with">
		<values>
			<value>
				<label>%1</label>
			</value>
		</values>
	</parameter>
</programParameters>


where:

- **%1** is a parameter named "comparison_with". It can have the following values:

  - ``alternatives``: alternatives vs alternatives

  - ``boundary_profiles``: alternatives vs boundary profiles

  - ``central_profiles``: alternatives vs central (characteristic) profiles

  The default value is item0.


------------------------



.. _ElectreConcordanceReinforcedPreference-PUT_outputs:

Outputs
-------


- :ref:`concordance <ElectreConcordanceReinforcedPreference-PUT-output1>`
- :ref:`messages <ElectreConcordanceReinforcedPreference-PUT-output2>`


.. _ElectreConcordanceReinforcedPreference-PUT-output1:

concordance
~~~~~~~~~~~

Concordance matrix computed from the given data. This matrix aggregates partial concordances from all criteria into single concordance index per pair of alternatives or alternatives/profiles.

The returned value is a XMCDA document whose main tag is ``<alternativesMatrix>``.


------------------------


.. _ElectreConcordanceReinforcedPreference-PUT-output2:

messages
~~~~~~~~

Messages or errors generated by this module.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/ElectreConcordanceReinforcedPreference-PUT/description-wsDD.xml>`
