.. _groupClassAcceptabilities-PUT:

groupClassAcceptabilities
=========================

:Version: 1.0.0
:Provider: PUT
:SOAP service's name: ``groupClassAcceptabilities-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Module for calculation group class acceptabilities basing on the assignments to classes of each decision maker.

**Contact:** Magdalena Dziecielska <magdalenadziecielska6@gmail.com>

**Web page:** https://github.com/MagdalenaDziecielska/PrometheeDiviz

**Reference:** 
      S. Damart, L.C. Dias and V. Mousseau; Supporting groups in sorting
      decisions: Methodology and use of a multi-criteria aggregation/disaggregation DSS.;
      Decision Support Systems 43 (2007) 1464?1475
    


Inputs
------
(For outputs, see :ref:`below <groupClassAcceptabilities-PUT_outputs>`)


- :ref:`alternatives <groupClassAcceptabilities-PUT-input1>`
- :ref:`assignments_1 <groupClassAcceptabilities-PUT-input2>`
- :ref:`assignments_2 <groupClassAcceptabilities-PUT-input3>`
- :ref:`assignments_3 <groupClassAcceptabilities-PUT-input4>` *(optional)*
- :ref:`assignments_4 <groupClassAcceptabilities-PUT-input5>` *(optional)*
- :ref:`assignments_5 <groupClassAcceptabilities-PUT-input6>` *(optional)*
- :ref:`assignments_6 <groupClassAcceptabilities-PUT-input7>` *(optional)*
- :ref:`assignments_7 <groupClassAcceptabilities-PUT-input8>` *(optional)*
- :ref:`assignments_8 <groupClassAcceptabilities-PUT-input9>` *(optional)*
- :ref:`assignments_9 <groupClassAcceptabilities-PUT-input10>` *(optional)*
- :ref:`assignments_10 <groupClassAcceptabilities-PUT-input11>` *(optional)*
- :ref:`categories <groupClassAcceptabilities-PUT-input12>` *(optional)*
- :ref:`categories_values <groupClassAcceptabilities-PUT-input13>`


.. _groupClassAcceptabilities-PUT-input1:

alternatives
~~~~~~~~~~~~

Alternatives to consider.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _groupClassAcceptabilities-PUT-input2:

assignments_1
~~~~~~~~~~~~~

Assignments of each alternative to the categories interval as an upper and lower bound for decision maker 1.

The input value should be a valid XMCDA document whose main tag is ``<alternativesAssignments>``.


------------------------


.. _groupClassAcceptabilities-PUT-input3:

assignments_2
~~~~~~~~~~~~~

Assignments of each alternative to the categories interval as an upper and lower bound for decision maker 2.

The input value should be a valid XMCDA document whose main tag is ``<alternativesAssignments>``.


------------------------


.. _groupClassAcceptabilities-PUT-input4:

assignments_3 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~

Assignments of each alternative to the categories interval as an upper and lower bound for decision maker 3.

The input value should be a valid XMCDA document whose main tag is ``<alternativesAssignments>``.


------------------------


.. _groupClassAcceptabilities-PUT-input5:

assignments_4 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~

Assignments of each alternative to the categories interval as an upper and lower bound for decision maker 4.

The input value should be a valid XMCDA document whose main tag is ``<alternativesAssignments>``.


------------------------


.. _groupClassAcceptabilities-PUT-input6:

assignments_5 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~

Assignments of each alternative to the categories interval as an upper and lower bound for decision maker 5.

The input value should be a valid XMCDA document whose main tag is ``<alternativesAssignments>``.


------------------------


.. _groupClassAcceptabilities-PUT-input7:

assignments_6 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~

Assignments of each alternative to the categories interval as an upper and lower bound for decision maker 6.

The input value should be a valid XMCDA document whose main tag is ``<alternativesAssignments>``.


------------------------


.. _groupClassAcceptabilities-PUT-input8:

assignments_7 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~

Assignments of each alternative to the categories interval as an upper and lower bound for decision maker 7.

The input value should be a valid XMCDA document whose main tag is ``<alternativesAssignments>``.


------------------------


.. _groupClassAcceptabilities-PUT-input9:

assignments_8 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~

Assignments of each alternative to the categories interval as an upper and lower bound for decision maker 8.

The input value should be a valid XMCDA document whose main tag is ``<alternativesAssignments>``.


------------------------


.. _groupClassAcceptabilities-PUT-input10:

assignments_9 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~

Assignments of each alternative to the categories interval as an upper and lower bound for decision maker 9.

The input value should be a valid XMCDA document whose main tag is ``<alternativesAssignments>``.


------------------------


.. _groupClassAcceptabilities-PUT-input11:

assignments_10 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Assignments of each alternative to the categories interval as an upper and lower bound for decision maker 10.

The input value should be a valid XMCDA document whose main tag is ``<alternativesAssignments>``.


------------------------


.. _groupClassAcceptabilities-PUT-input12:

categories *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~

Definitions of categories.

The input value should be a valid XMCDA document whose main tag is ``<categoriesValues>``.


------------------------


.. _groupClassAcceptabilities-PUT-input13:

categories_values
~~~~~~~~~~~~~~~~~

Definitions of categories and their marks (higher mark means better category).

The input value should be a valid XMCDA document whose main tag is ``<categoriesValues>``.


------------------------



.. _groupClassAcceptabilities-PUT_outputs:

Outputs
-------


- :ref:`alternatives_support <groupClassAcceptabilities-PUT-output1>`
- :ref:`unimodal_alternatives_support <groupClassAcceptabilities-PUT-output2>`
- :ref:`messages <groupClassAcceptabilities-PUT-output3>`


.. _groupClassAcceptabilities-PUT-output1:

alternatives_support
~~~~~~~~~~~~~~~~~~~~

Alternatives support represented as percentage value

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _groupClassAcceptabilities-PUT-output2:

unimodal_alternatives_support
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Unimodal alternatives support represented as percentage value

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _groupClassAcceptabilities-PUT-output3:

messages
~~~~~~~~

Messages or errors generated by this module.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/groupClassAcceptabilities-PUT/description-wsDD.xml>`
