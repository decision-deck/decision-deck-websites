.. _RORUTADIS-StochasticResults-PUT:

RORUTADIS-StochasticResults
===========================

:Version: 0.1
:Provider: PUT
:SOAP service's name: ``RORUTADIS-StochasticResults-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Calculates stochastic results for alternative assignments, assignment-based preference relation and class cardinalities. The results are computed by sampling the space of compatible models.

**Contact:** 
			Krzysztof Ciomek (k.ciomek@gmail.com),
			Milosz Kadzinski (milosz.kadzinski@cs.put.poznan.pl)
		

**Web page:** https://github.com/kciomek/rorutadis

**Reference:** None


Inputs
------
(For outputs, see :ref:`below <RORUTADIS-StochasticResults-PUT_outputs>`)


- :ref:`criteria <RORUTADIS-StochasticResults-PUT-criteria>`
- :ref:`alternatives <RORUTADIS-StochasticResults-PUT-alternatives>`
- :ref:`categories <RORUTADIS-StochasticResults-PUT-categories>`
- :ref:`performanceTable <RORUTADIS-StochasticResults-PUT-performanceTable>`
- :ref:`assignmentExamples <RORUTADIS-StochasticResults-PUT-assignmentExamples>` *(optional)*
- :ref:`assignmentComparisons <RORUTADIS-StochasticResults-PUT-assignmentComparisons>` *(optional)*
- :ref:`categoriesCardinalities <RORUTADIS-StochasticResults-PUT-categoriesCardinalities>` *(optional)*
- :ref:`methodParameters <RORUTADIS-StochasticResults-PUT-methodParameters>`


.. _RORUTADIS-StochasticResults-PUT-criteria:

criteria
~~~~~~~~

A list of criteria (<criteria> tag) with information about preference direction (<criteriaValues mcdaConcept="preferenceDirection">, 0 - gain, 1 - cost) and number of characteristic points (<criteriaValues mcdaConcept="numberOfCharacteristicPoints">, 0 for the most general marginal utility function or integer grater or equal to 2) of each criterion.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
<criteria>
	<criterion id="[...]" />
	[...]
</criteria>

<criteriaValues mcdaConcept="preferenceDirection">
	<criterionValue>
		<criterionID>[...]</criterionID>
		<value><integer>[...]</integer></value>
	</criterionValue>
	[...]
</criteriaValues>

<criteriaValues mcdaConcept="numberOfCharacteristicPoints">
	<criterionValue>
		<criterionID>[...]</criterionID>
		<value><integer>[0|integer greater or equal to 2]</integer></value>
	</criterionValue>
	[...]
</criteriaValues>



------------------------


.. _RORUTADIS-StochasticResults-PUT-alternatives:

alternatives
~~~~~~~~~~~~

A list of alternatives.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
<alternatives>
	<alternative id="...">
		<active>[...]</active>
	</alternative>
</alternatives>



------------------------


.. _RORUTADIS-StochasticResults-PUT-categories:

categories
~~~~~~~~~~

A list of categories (classes). List must be sorted from the worst category to the best.

The input value should be a valid XMCDA document whose main tag is ``<categories>``.
It must have the following form::

   
<categories>
	<category id="[...]" />
	[...]
</categories>



------------------------


.. _RORUTADIS-StochasticResults-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

The performances of the alternatives.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _RORUTADIS-StochasticResults-PUT-assignmentExamples:

assignmentExamples *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of assignment examples of alternatives to intervals of categories (classes) or to a specific category (class).

The input value should be a valid XMCDA document whose main tag is ``<alternativesAssignments>``.
It must have the following form::

   
<alternativesAssignments>
	<alternativeAssignment>
		<alternativeID>[...]</alternativeID>
		<categoryID>[...]</categoryID>
	</alternativeAssignment>
	[...]
	<alternativeAssignment>
		<alternativeID>[...]</alternativeID>
		<categoriesInterval>
			<lowerBound>
				<categoryID>[...]</categoryID>
			</lowerBound>
			<upperBound>
				<categoryID>[...]</categoryID>
			</upperBound>
		</categoriesInterval>
	</alternativeAssignment>
	[...]
	<alternativeAssignment>
		<alternativeID>[...]</alternativeID>
		<categoriesSet>
			<categoryID>[...]</categoryID>
			[...]
		</categoriesSet>
	</alternativeAssignment>
	[...]
</alternativesAssignments>



------------------------


.. _RORUTADIS-StochasticResults-PUT-assignmentComparisons:

assignmentComparisons *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Two lists of assignment pairwise comparisons. A comparison from list with attribute mcdaConcept="atLeastAsGoodAs" indicates that some alternative should be assigned to class at least as good as class of some other alternative (k = 0) or at least better by k classes (k > 0). A comparison from list with attribute mcdaConcept="atMostAsGoodAs" indicates that some alternative should be assigned to class at most better by k classes (k > 0) then some other alternative. Note: usage of this kind of preference information significantly slows down computations.

The input value should be a valid XMCDA document whose main tag is ``<alternativesMatrix>``.
It must have the following form::

   
<alternativesMatrix mcdaConcept="atLeastAsGoodAs">
	<row>
		<alternativeID>[...]</alternativeID>
		<column>
			<alternativeID>[...]</alternativeID>
			<values>
				<value>
					<integer>k</integer>
				</value>
			</values>
		</column>
	</row>
	[...]
</alternativesMatrix>

<alternativesMatrix mcdaConcept="atMostAsGoodAs">
	<row>[...]</row>
	[...]
</alternativesMatrix>



------------------------


.. _RORUTADIS-StochasticResults-PUT-categoriesCardinalities:

categoriesCardinalities *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of category (class) cardinality constraints. It allows to define minimal and/or maximal desired category (class) cardinalities. Note: usage of this kind of preference information significantly slows down computations.

The input value should be a valid XMCDA document whose main tag is ``<categoriesValues>``.
It must have the following form::

   
<categoriesValues>
	<categoryValue>
		<categoryID>[...]</categoryID>
		<values>
			<value>
				<interval>
					<lowerBound><integer>[...]</integer></lowerBound>
					<upperBound><integer>[...]</integer></upperBound>
				</interval>
			</value>
		</values>
	</categoryValue>
	[...]
</categoriesValues>



------------------------


.. _RORUTADIS-StochasticResults-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~

Method parameters.

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
<programParameters>
	<parameter name="strictlyMonotonicValueFunctions">
		<values>
			<value><boolean>%1</boolean></value>
		</values>
	</parameter>
	<parameter name="numberOfSamples">
		<values>
			<value><integer>%2</integer></value>
		</values>
	</parameter>
<!-- The following is OPTIONAL - we comment it out because we do not want to
     change the WS nor the diviz program for now, we just want to be able to
     use it in automated tests.
--><!--
	<parameter name="seed">
		<value>
			<integer>%3</integer>
		</value>
	</parameter>
-->
</programParameters>


where:

- **%1** is a parameter named "strictlyMonotonicValueFunctions". This is a boolean.
  The default value is false.

- **%2** is a parameter named "numberOfSamples". This is a int.
  The default value is 100.


------------------------



.. _RORUTADIS-StochasticResults-PUT_outputs:

Outputs
-------


- :ref:`stochasticAssignments <RORUTADIS-StochasticResults-PUT-stochasticAssignments>`
- :ref:`stochasticPreferenceRelation <RORUTADIS-StochasticResults-PUT-stochasticPreferenceRelation>`
- :ref:`stochasticClassCardinalities <RORUTADIS-StochasticResults-PUT-stochasticClassCardinalities>`
- :ref:`messages <RORUTADIS-StochasticResults-PUT-messages>`


.. _RORUTADIS-StochasticResults-PUT-stochasticAssignments:

stochasticAssignments
~~~~~~~~~~~~~~~~~~~~~

Stochastic assignments. The value for alternative a_i and category c_j equals to the rate of samples, for which alternative a_i was assigned to category C_j.

The returned value is a XMCDA document whose main tag is ``<alternativesAssignments>``.
It has the following form::

   
<alternativesAssignments>
	<alternativeAffectation>
		<alternativeID>[...]</alternativeID>
		<categoryID>[...]</categoryID>
		<value>
			<real>[...]</real>
		</value>
	</alternativeAffectation>
	[...]
</alternativesAssignments>



------------------------


.. _RORUTADIS-StochasticResults-PUT-stochasticPreferenceRelation:

stochasticPreferenceRelation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Stochastic preference relation. Each value for pair (a_i, a_j) describes the rate of samples, for which alternative a_i was assigned to class at least as good as class of a_j.

The returned value is a XMCDA document whose main tag is ``<alternativesMatrix>``.
It has the following form::

   
<alternativesMatrix mcdaConcept="atLeastAsGoodAs">
	<row>
		<alternativeID>...</alternativeID>
		<column>
			<alternativeID>...</alternativeID>
			<values>
				<value>
					<real>1.0</real>
				</value>
			</values>
		</column>
		[...]
	<row>
	[...]
</alternativesMatrix>



------------------------


.. _RORUTADIS-StochasticResults-PUT-stochasticClassCardinalities:

stochasticClassCardinalities
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Stochastic class cardinalities. Each category has sequence of n + 1 values, where n is the number of considered alternatives. First value corresponds to the cardinality 0, and next n values to subsequent cardinalities from 1 to n. i-th value for c_j class describes the rate of samples, for which i-1 alternatives were assigned to class c_j.

The returned value is a XMCDA document whose main tag is ``<categoriesValues>``.
It has the following form::

   
<categoriesValues>
	<categoryValue>
		<categoryID>c01</categoryID>
		<values>
			<value>
				<real>[...]</real>
			</value>
			<value>
				<real>[...]</real>
			</value>
			[...]
		</values>
	</categoryValue>
	[...]
</categoriesValues>



------------------------


.. _RORUTADIS-StochasticResults-PUT-messages:

messages
~~~~~~~~

Messages generated by the program.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/RORUTADIS-StochasticResults-PUT/description-wsDD.xml>`
