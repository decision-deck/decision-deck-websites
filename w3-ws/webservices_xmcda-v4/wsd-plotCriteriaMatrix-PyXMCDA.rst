:orphan:

.. _plotCriteriaMatrix-PyXMCDA:

plotCriteriaMatrix
==================

:Version: 1.1
:Provider: PyXMCDA
:SOAP service's name: ``plotCriteriaMatrix-PyXMCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Generate graph representing provided criteriaMatrix as well as the dot files generating this graph.
Values associated to transitions can be shown without any preprocessing.
Graphviz dot program and format are used by this service.

**Contact:** Nicolas Duminy (nicolas.duminy@telecom-bretagne.eu)

**Web page:** https://gitlab.com/nduminy/ws-pyxmcda


Inputs
------
(For outputs, see :ref:`below <plotCriteriaMatrix-PyXMCDA_outputs>`)


- :ref:`criteria <plotCriteriaMatrix-PyXMCDA-criteria>` *(optional)*
- :ref:`criteriaMatrix <plotCriteriaMatrix-PyXMCDA-criteriaMatrix>`
- :ref:`parameters <plotCriteriaMatrix-PyXMCDA-parameters>` *(optional)*


.. _plotCriteriaMatrix-PyXMCDA-criteria:

criteria *(optional)*
~~~~~~~~~~~~~~~~~~~~~

The criteria to be plotted. All are plotted if not provided.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _plotCriteriaMatrix-PyXMCDA-criteriaMatrix:

criteriaMatrix
~~~~~~~~~~~~~~

The criteria matrix to be plotted.

The input value should be a valid XMCDA document whose main tag is ``<criteriaMatrix>``.


------------------------


.. _plotCriteriaMatrix-PyXMCDA-parameters:

parameters *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~

Parameters of the method

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
    <programParameters>
        <programParameter id="color" name="Color">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="shape" name="Shape">
            <values>
                <value>
                    <label>%2</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="image_file_extension" name="Image file extension">
            <values>
                <value>
                    <label>%3</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="show_values" name="Show values">
            <values>
                <value>
                    <boolean>%4</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="directed_graph" name="Directed graph">
            <values>
                <value>
                    <boolean>%7</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="transitive_reduction" name="Transitive reduction">
            <values>
                <value>
                    <boolean>%5</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="transitive_closure" name="Transitive closure">
            <values>
                <value>
                    <boolean>%6</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="naming_conventions" name="Naming conventions">
            <values>
                <value>
                    <label>%8</label>
                </value>
            </values>
        </programParameter>
    </programParameters>


where:

- **%1** is a parameter named "Color". It can have the following values:

  - ``black``: Black

  - ``white``: White

  - ``red``: Red

  - ``green``: Green

  - ``blue``: Blue

  - ``magenta``: Magenta

  - ``yellow``: Yellow

  - ``cyan``: Cyan

  - ``orange``: Orange

  - ``pink``: Pink

  - ``brown``: Brown

  - ``gray``: Gray

  The default value is black.

- **%2** is a parameter named "Shape". It can have the following values:

  - ``box``: Rectangle

  - ``circle``: Circle

  - ``diamond``: Diamond

  - ``ellipse``: Ellipse

  - ``oval``: Oval

  - ``polygon``: Polygon

  - ``triangle``: Triangle

  The default value is oval.

- **%3** is a parameter named "Image file extension". It can have the following values:

  - ``bmp``: .bmp (Windows Bitmap Format)

  - ``dia``: .dia (DIA Format)

  - ``fig``: .fig (FIG graphics format)

  - ``gif``: .gif (Graphics Interchange Format)

  - ``hpgl``: .hpgl (Hewlett Packard Graphic Language 2)

  - ``ico``: .ico (Icon Image File Format)

  - ``jpg``: .jpg (Joint Photographic Experts Group)

  - ``jpe``: .jpe (Joint Photographic Experts Group)

  - ``pdf``: .pdf (Portable Document Format)

  - ``png``: .png (Portable Network Graphics)

  - ``ps``: .ps (PostScript)

  - ``ps2``: .ps2 (PostScript for PDF)

  - ``svg``: .svg (Scalable Vector Graphics)

  - ``svgz``: .svgz (Compressed Scalable Vector Graphics)

  - ``tif``: .tif (Tagged Image File Format)

  The default value is png.

- **%4** is a parameter named "Show values". This is a boolean.
  The default value is false.

- **%7** is a parameter named "Directed graph". It can have the following values:

  - ``true``: Directed

  - ``false``: Undirected

  The default value is true.

- **%5** is a parameter named "Transitive reduction". This is a boolean.
  The default value is false.

- **%6** is a parameter named "Transitive closure". This is a boolean.
  The default value is false.

- **%8** is a parameter named "Naming conventions". It can have the following values:

  - ``id``: Only ids are shown

  - ``name``: Only names are shown (disambiguated by appending the ids, if needed)

  - ``name (id)``: Names and ids are shown in that order

  - ``id (name)``: Ids and names are shown in that order

  The default value is name.


------------------------



.. _plotCriteriaMatrix-PyXMCDA_outputs:

Outputs
-------


- :ref:`glob:criteriaMatrix.{bmp,dia,fig,gif,hpgl,ico,jpg,jpe,pdf,png,ps,ps2,svg,svgz,tif} <plotCriteriaMatrix-PyXMCDA-criteriaMatrixPlot>`
- :ref:`criteriaMatrix.dot <plotCriteriaMatrix-PyXMCDA-criteriaMatrixPlotScript>`
- :ref:`messages <plotCriteriaMatrix-PyXMCDA-messages>`


.. _plotCriteriaMatrix-PyXMCDA-criteriaMatrixPlot:

glob:criteriaMatrix.{bmp,dia,fig,gif,hpgl,ico,jpg,jpe,pdf,png,ps,ps2,svg,svgz,tif}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Image containing criteria matrix graph. Format corresponds to the one given in parameters (default is .png).

The returned value is a XMCDA document whose main tag is ``<None>``.


------------------------


.. _plotCriteriaMatrix-PyXMCDA-criteriaMatrixPlotScript:

criteriaMatrix.dot
~~~~~~~~~~~~~~~~~~

Generated graphviz dot script that made the image. Given to enable users to later customize the appearance of the graph.

The returned value is a XMCDA document whose main tag is ``<None>``.


------------------------


.. _plotCriteriaMatrix-PyXMCDA-messages:

messages
~~~~~~~~

Status messages.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/plotCriteriaMatrix-PyXMCDA/description-wsDD.xml>`
