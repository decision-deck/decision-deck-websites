.. _generatePerformanceTableFromBayesianNet-RXMCDA:

generatePerformanceTableFromBayesianNet
=======================================

:Version: 1.0
:Provider: RXMCDA
:SOAP service's name: ``generatePerformanceTableFromBayesianNet-RXMCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Generates a random performance table, given a weighted bayesian net representing the interactions (and their correlation indexes) between the criteria, together with desired mean and standard deviation values for each criterion. The number of alternatives to generate can be specified, as well as the prefix to be used for the ids of these alternatives. An optional seed can also be given.

**Contact:** Patrick Meyer (patrick.meyer@telecom-bretagne.eu) and Marc Pirlot (marc.pirlot@umons.be).


Inputs
------
(For outputs, see :ref:`below <generatePerformanceTableFromBayesianNet-RXMCDA_outputs>`)


- :ref:`criteria <generatePerformanceTableFromBayesianNet-RXMCDA-in4>`
- :ref:`graph <generatePerformanceTableFromBayesianNet-RXMCDA-in5>`
- :ref:`means <generatePerformanceTableFromBayesianNet-RXMCDA-in6>`
- :ref:`standardDeviations <generatePerformanceTableFromBayesianNet-RXMCDA-in7>`
- :ref:`parameters <generatePerformanceTableFromBayesianNet-RXMCDA-parameters>`


.. _generatePerformanceTableFromBayesianNet-RXMCDA-in4:

criteria
~~~~~~~~

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
                
                    <criteria>
                        <criterion id="...">
                            <active>[...]</active>
                            [...]
                        </criterion>
                        [...]
                    </criteria>
                    
            


------------------------


.. _generatePerformanceTableFromBayesianNet-RXMCDA-in5:

graph
~~~~~

The bayesian net, weighted by the desired correlations between the criteria.

The input value should be a valid XMCDA document whose main tag is ``<criteriaComparisons>``.
It must have the following form::

   
                
                    <criteriaComparisons>
                        <pairs>
                            <pair>
                                <initial>
                                    <criterionID>[...]</criterionID>
                                </initial>
                                <terminal>
                                    <criterionID>[...]</criterionID>
                                </terminal>
                                <value>
                                    <real>[...]</real>
                                </value>
                          </pair>
                          [...]
                      </pairs>
                   </criteriaComparisons>
                    
            


------------------------


.. _generatePerformanceTableFromBayesianNet-RXMCDA-in6:

means
~~~~~

The desired means of the criteria of the generated performance table.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.
It must have the following form::

   
                
                    <criteriaValues>
                       <criterionValue>
                           <criterionID>[...]</criterionID>
                               <value>
                                   <real>[...]</real>
                                </value>
                        </criterionValue>
                        [...]
                    </criteriaValues>
                    
            


------------------------


.. _generatePerformanceTableFromBayesianNet-RXMCDA-in7:

standardDeviations
~~~~~~~~~~~~~~~~~~

The desired standard deviations of the criteria of the generated performance table.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.
It must have the following form::

   
                
                    <criteriaValues>
                       <criterionValue>
                           <criterionID>[...]</criterionID>
                               <value>
                                   <real>[...]</real>
                                </value>
                        </criterionValue>
                        [...]
                    </criteriaValues>
                    
            


------------------------


.. _generatePerformanceTableFromBayesianNet-RXMCDA-parameters:

parameters
~~~~~~~~~~

The seed for the random numbers generator (integer).

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                
                    <methodParameters>
                        <parameter
                             name="seed"> <!-- REQUIRED  -->
                            <value>
                                <integer>%1</integer>
                            </value>
                        </parameter>
                         <parameter
                             name="numAlt"> <!-- REQUIRED  -->
                            <value>
                                <integer>%2</integer>
                            </value>
                        </parameter>
                        <parameter
                             name="prefix"> <!-- REQUIRED  -->
                            <value>
                                <label>%3</label>
                            </value>
                        </parameter>
                   </methodParameters>
                    
            

where:

- **%1** is a parameter named "seed". This is a int, and the value should conform to the following constraint: An integer value..  More formally, the constraint is::

      %1 > 0 
  The default value is 1.

- **%2** is a parameter named "number of alternatives to generate". This is a int, and the value should conform to the following constraint: An integer value..  More formally, the constraint is::

      %2 > 0 
  The default value is 2.

- **%3** is a parameter named "prefix for the alternatives ids". This is a string.
  The default value is x.


------------------------



.. _generatePerformanceTableFromBayesianNet-RXMCDA_outputs:

Outputs
-------


- :ref:`alternatives <generatePerformanceTableFromBayesianNet-RXMCDA-out3>`
- :ref:`performanceTable <generatePerformanceTableFromBayesianNet-RXMCDA-out2>`
- :ref:`messages <generatePerformanceTableFromBayesianNet-RXMCDA-out1>`


.. _generatePerformanceTableFromBayesianNet-RXMCDA-out3:

alternatives
~~~~~~~~~~~~

The ids of the generated alternatives.

The returned value is a XMCDA document whose main tag is ``<alternatives>``.
It has the following form::

   
                
                    <alternatives>
                        <alternative id="..."/>
                        [...]
                    </alternatives>
                    
            


------------------------


.. _generatePerformanceTableFromBayesianNet-RXMCDA-out2:

performanceTable
~~~~~~~~~~~~~~~~

The generated performance table.

The returned value is a XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _generatePerformanceTableFromBayesianNet-RXMCDA-out1:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/generatePerformanceTableFromBayesianNet-RXMCDA/description-wsDD.xml>`
