:orphan:

.. _plotFuzzyCriteriaScales-PyXMCDA:

plotFuzzyCriteriaScales
=======================

:Version: 1.3
:Provider: PyXMCDA
:SOAP service's name: ``plotFuzzyCriteriaScales-PyXMCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Generate plots from provided fuzzy scales as well as the scripts generating these plots.

Colormap can be defined by the user, by giving a list of colors in the parameters.xml file.
The number of colors is not restrained, and the colormap will linearly distribute the color in their provided order and interpolate between them.
If only one is provided, it will be used for all data plot.
Each color is either one of the color names predefined in matplotlib (See https://matplotlib.org/stable/gallery/color/named_colors.html#sphx-glr-gallery-color-named-colors-py) or a RGB color defined in hexadecimal '#RRGGBB'.

**Contact:** Nicolas Duminy (nicolas.duminy@telecom-bretagne.eu)

**Web page:** https://gitlab.com/nduminy/ws-pyxmcda


Inputs
------
(For outputs, see :ref:`below <plotFuzzyCriteriaScales-PyXMCDA_outputs>`)


- :ref:`criteria <plotFuzzyCriteriaScales-PyXMCDA-criteria>` *(optional)*
- :ref:`criteriaScales <plotFuzzyCriteriaScales-PyXMCDA-criteriaScales>`
- :ref:`parameters <plotFuzzyCriteriaScales-PyXMCDA-parameters>`


.. _plotFuzzyCriteriaScales-PyXMCDA-criteria:

criteria *(optional)*
~~~~~~~~~~~~~~~~~~~~~

The criteria to be plotted. All are plotted if not provided.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _plotFuzzyCriteriaScales-PyXMCDA-criteriaScales:

criteriaScales
~~~~~~~~~~~~~~

The criteria and their criteria scales defined. Only the fuzzy ones can be considered.
                Unidentified criteria scales are accepted as well (they won't be labelled though).

The input value should be a valid XMCDA document whose main tag is ``<criteriaScales>``.


------------------------


.. _plotFuzzyCriteriaScales-PyXMCDA-parameters:

parameters
~~~~~~~~~~

Parameters of the method

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
    <programParameters>
        <programParameter id="x_axis" name="X-axis label">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="y_axis" name="Y-axis label">
            <values>
                <value>
                    <label>%2</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="colors" name="Colors">
            <!-- colormap: %colormap / nb colors: %nb_colors -->
            <values>%color-mono%color-A%color-B%color-a%color-b%color-c%color-d%color-e%color-f%color-g%color-h%color-i%color-j
            </values>
        </programParameter>
        <programParameter id="use_markers">
            <values>
                <value>
                    <boolean>%6</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="linestyle" name="Linestyle">
            <values>
                <value>
                    <label>%7</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="show_grid">
            <values>
                <value>
                    <boolean>%8</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="image_file_extension" name="Image file extension">
            <values>
                <value>
                    <label>%9a%9b</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="plotter" name="Plotter">
            <values>
                <value>
                    <label>%10</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="naming_conventions" name="Naming conventions">
            <values>
                <value>
                    <label>%11</label>
                </value>
            </values>
        </programParameter>
    </programParameters>


where:

- **%1** is a parameter named "X-axis label". This is a string.
  The default value is series.

- **%2** is a parameter named "Y-axis label". This is a string.
  The default value is fuzzy numbers.

- **%colormap** is a parameter named "Colormap". It can have the following values:

  - ``monochrome``: Monochrome

  - ``bicolor``: Bicolor

  - ``multicolor (advanced)``: Multi-color (advanced)

  The default value is monochrome.

- **%nb_colors** is a parameter named "Nb of colors". This is a int, and the value should conform to the following constraint: The value should be between 2 and 10..  More formally, the constraint is::

     %nb_colors > 1 && %nb_colors <= 10
  The default value is 2.

- **%color-mono** is a parameter named "Color". It can have the following values:

  - ``
                <value>
                    <label>black</label>
                </value>``: black

  - ``
                <value>
                    <label>red</label>
                </value>``: red

  - ``
                <value>
                    <label>green</label>
                </value>``: green

  - ``
                <value>
                    <label>blue</label>
                </value>``: blue

  - ``
                <value>
                    <label>orange</label>
                </value>``: orange

  - ``
                <value>
                    <label>green</label>
                </value>``: green

  - ``
                <value>
                    <label>purple</label>
                </value>``: purple

  - ``
                <value>
                    <label>cyan</label>
                </value>``: cyan

  - ``
                <value>
                    <label>magenta</label>
                </value>``: magenta

  - ``
                <value>
                    <label>yellow</label>
                </value>``: yellow

  - ``
                <value>
                    <label>salmon</label>
                </value>``: salmon

  - ``
                <value>
                    <label>orangered</label>
                </value>``: orangered

  - ``
                <value>
                    <label>chocolate</label>
                </value>``: chocolate

  - ``
                <value>
                    <label>greenyellow</label>
                </value>``: greenyellow

  - ``
                <value>
                    <label>aquamarine</label>
                </value>``: aquamarine

  - ``
                <value>
                    <label>pink</label>
                </value>``: pink

  - ``
                <value>
                    <label>grey</label>
                </value>``: grey

  - ``
                <value>
                    <label>whitesmoke</label>
                </value>``: whitesmoke

  The default value is black.

- **%color-A** is a parameter named "1st color". It can have the following values:

  - ``
                <value>
                    <label>black</label>
                </value>``: black

  - ``
                <value>
                    <label>red</label>
                </value>``: red

  - ``
                <value>
                    <label>green</label>
                </value>``: green

  - ``
                <value>
                    <label>blue</label>
                </value>``: blue

  - ``
                <value>
                    <label>orange</label>
                </value>``: orange

  - ``
                <value>
                    <label>green</label>
                </value>``: green

  - ``
                <value>
                    <label>purple</label>
                </value>``: purple

  - ``
                <value>
                    <label>cyan</label>
                </value>``: cyan

  - ``
                <value>
                    <label>magenta</label>
                </value>``: magenta

  - ``
                <value>
                    <label>yellow</label>
                </value>``: yellow

  - ``
                <value>
                    <label>salmon</label>
                </value>``: salmon

  - ``
                <value>
                    <label>orangered</label>
                </value>``: orange-red

  - ``
                <value>
                    <label>chocolate</label>
                </value>``: chocolate

  - ``
                <value>
                    <label>greenyellow</label>
                </value>``: green-yellow

  - ``
                <value>
                    <label>aquamarine</label>
                </value>``: aquamarine

  - ``
                <value>
                    <label>pink</label>
                </value>``: pink

  - ``
                <value>
                    <label>grey</label>
                </value>``: grey

  - ``
                <value>
                    <label>whitesmoke</label>
                </value>``: whitesmoke

  - ``
                <value>
                    <label>white</label>
                </value>``: white

  The default value is black.

- **%color-B** is a parameter named "2nd color". It can have the following values:

  - ``
                <value>
                    <label>black</label>
                </value>``: black

  - ``
                <value>
                    <label>red</label>
                </value>``: red

  - ``
                <value>
                    <label>green</label>
                </value>``: green

  - ``
                <value>
                    <label>blue</label>
                </value>``: blue

  - ``
                <value>
                    <label>orange</label>
                </value>``: orange

  - ``
                <value>
                    <label>green</label>
                </value>``: green

  - ``
                <value>
                    <label>purple</label>
                </value>``: purple

  - ``
                <value>
                    <label>cyan</label>
                </value>``: cyan

  - ``
                <value>
                    <label>magenta</label>
                </value>``: magenta

  - ``
                <value>
                    <label>yellow</label>
                </value>``: yellow

  - ``
                <value>
                    <label>salmon</label>
                </value>``: salmon

  - ``
                <value>
                    <label>orangered</label>
                </value>``: orangered

  - ``
                <value>
                    <label>chocolate</label>
                </value>``: chocolate

  - ``
                <value>
                    <label>greenyellow</label>
                </value>``: greenyellow

  - ``
                <value>
                    <label>aquamarine</label>
                </value>``: aquamarine

  - ``
                <value>
                    <label>pink</label>
                </value>``: pink

  - ``
                <value>
                    <label>grey</label>
                </value>``: grey

  - ``
                <value>
                    <label>whitesmoke</label>
                </value>``: whitesmoke

  The default value is black.

- **%color-a** is a parameter named "Color 1 (ex.: cyan or #00FFFF)". This is a string.
  The default value is black.

- **%color-b** is a parameter named "Color 2". This is a string.
  The default value is black.

- **%color-c** is a parameter named "Color 3". This is a string.
  The default value is black.

- **%color-d** is a parameter named "Color 4". This is a string.
  The default value is black.

- **%color-e** is a parameter named "Color 5". This is a string.
  The default value is black.

- **%color-f** is a parameter named "Color 6". This is a string.
  The default value is black.

- **%color-g** is a parameter named "Color 7". This is a string.
  The default value is black.

- **%color-h** is a parameter named "Color 8". This is a string.
  The default value is black.

- **%color-i** is a parameter named "Color 9". This is a string.
  The default value is black.

- **%color-j** is a parameter named "Color 10". This is a string.
  The default value is black.

- **%6** is a parameter named "Use markers". This is a boolean.
  The default value is false.

- **%7** is a parameter named "Linestyle". It can have the following values:

  - ``solid``: solid line

  - ``dashed``: dashed line

  - ``dashdot``: dashed and dotted line

  - ``dotted``: dotted line

  - ``None``: no line

  The default value is solid.

- **%8** is a parameter named "Show grid". This is a boolean.
  The default value is false.

- **%9a** is a parameter named "Image file extension". It can have the following values:

  - ``eps``: .eps (Encapsulated PostScript)

  - ``jpg``: .jpg (Joint Photographic Experts Group)

  - ``pdf``: .pdf (Portable Document Format)

  - ``pgf``: .pgf (Progressive Graphics File)

  - ``png``: .png (Portable Network Graphics)

  - ``ps``: .ps (PostScript)

  - ``raw``: .raw (Raw RGBA bitmap)

  - ``rgba``: .rgba (Silicon Graphics RGB)

  - ``svg``: .svg (Scalable Vector Graphics)

  - ``svgz``: .svgz (Compressed Scalable Vector Graphics)

  - ``tif``: .tif (Tagged Image File Format)

  The default value is png.

- **%9b** is a parameter named "Image file extension". It can have the following values:

  - ``eps``: .eps (Encapsulated PostScript)

  - ``jpg``: .jpg (Joint Photographic Experts Group)

  - ``pdf``: .pdf (Portable Document Format)

  - ``png``: .png (Portable Network Graphics)

  - ``svg``: .svg (Scalable Vector Graphics)

  The default value is png.

- **%10** is a parameter named "Plotter". It can have the following values:

  - ``matplotlib``: Matplotlib

  - ``gnuplot``: Gnuplot

  The default value is matplotlib.

- **%11** is a parameter named "Naming conventions". It can have the following values:

  - ``id``: Only ids are shown

  - ``name``: Only names are shown (disambiguated by appending the ids, if needed)

  - ``name (id)``: Names and ids are shown in that order

  - ``id (name)``: Ids and names are shown in that order

  The default value is name.


------------------------



.. _plotFuzzyCriteriaScales-PyXMCDA_outputs:

Outputs
-------


- :ref:`glob:fuzzyCriteriaScales.{bmp,dia,fig,gif,hpgl,ico,jpg,jpe,pdf,png,ps,ps2,svg,svgz,tif} <plotFuzzyCriteriaScales-PyXMCDA-fuzzyCriteriaScalesPlot>`
- :ref:`plotFuzzyCriteriaScales.py <plotFuzzyCriteriaScales-PyXMCDA-fuzzyCriteriaScalesPlotScript>`
- :ref:`messages <plotFuzzyCriteriaScales-PyXMCDA-messages>`


.. _plotFuzzyCriteriaScales-PyXMCDA-fuzzyCriteriaScalesPlot:

glob:fuzzyCriteriaScales.{bmp,dia,fig,gif,hpgl,ico,jpg,jpe,pdf,png,ps,ps2,svg,svgz,tif}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Image containing all selected fuzzy criteriaScales plots. Format corresponds to the one given in parameters (default is .png).

The returned value is a XMCDA document whose main tag is ``<None>``.


------------------------


.. _plotFuzzyCriteriaScales-PyXMCDA-fuzzyCriteriaScalesPlotScript:

plotFuzzyCriteriaScales.py
~~~~~~~~~~~~~~~~~~~~~~~~~~

Generated Python script that made the image. Given to enable users to later customize the appearance of the plots.

The returned value is a XMCDA document whose main tag is ``<None>``.


------------------------


.. _plotFuzzyCriteriaScales-PyXMCDA-messages:

messages
~~~~~~~~

Status messages.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/plotFuzzyCriteriaScales-PyXMCDA/description-wsDD.xml>`
