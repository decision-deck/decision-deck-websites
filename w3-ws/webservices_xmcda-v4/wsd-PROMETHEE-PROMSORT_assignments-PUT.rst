.. _PROMETHEE-PROMSORT_assignments-PUT:

PROMETHEE-PROMSORT_assignments
==============================

:Version: 1.0.0
:Provider: PUT
:SOAP service's name: ``PROMETHEE-PROMSORT_assignments-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Computes class assignment for given alternatives using PromSort method. This method consists of two separated steps. In the first step alternatives are being assigned to categories basing on their relations with boundary profiles. This relations are computed using positive and negative flows. After first step alternative can be assigned to one or two categories. In the second step final assignment is calculated for each alternative assigned to two categories as a distance function between this alternative and alternatives assigned in first step to exactly one category. For other alternatives assignment from first step is final.

**Contact:** Maciej Uniejewski <maciej.uniejewski@gmail.com>

**Web page:** https://github.com/maciej7777/PrometheeDiviz


Inputs
------
(For outputs, see :ref:`below <PROMETHEE-PROMSORT_assignments-PUT_outputs>`)


- :ref:`criteria <PROMETHEE-PROMSORT_assignments-PUT-input6>`
- :ref:`alternatives <PROMETHEE-PROMSORT_assignments-PUT-input1>`
- :ref:`categories <PROMETHEE-PROMSORT_assignments-PUT-input2>`
- :ref:`performance_table <PROMETHEE-PROMSORT_assignments-PUT-input5>`
- :ref:`criteria_scales <PROMETHEE-PROMSORT_assignments-PUT-input7>`
- :ref:`criteria_thresholds <PROMETHEE-PROMSORT_assignments-PUT-input8>`
- :ref:`positive_flows <PROMETHEE-PROMSORT_assignments-PUT-input9>`
- :ref:`negative_flows <PROMETHEE-PROMSORT_assignments-PUT-input10>`
- :ref:`categories_profiles <PROMETHEE-PROMSORT_assignments-PUT-input3>`
- :ref:`categories_values <PROMETHEE-PROMSORT_assignments-PUT-input4>`
- :ref:`method_parameters <PROMETHEE-PROMSORT_assignments-PUT-input11>`


.. _PROMETHEE-PROMSORT_assignments-PUT-input6:

criteria
~~~~~~~~

Criteria to consider.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _PROMETHEE-PROMSORT_assignments-PUT-input1:

alternatives
~~~~~~~~~~~~

Alternatives to consider.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _PROMETHEE-PROMSORT_assignments-PUT-input2:

categories
~~~~~~~~~~

Definitions of categories.

The input value should be a valid XMCDA document whose main tag is ``<categories>``.


------------------------


.. _PROMETHEE-PROMSORT_assignments-PUT-input5:

performance_table
~~~~~~~~~~~~~~~~~

The performances of boundary profiles.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _PROMETHEE-PROMSORT_assignments-PUT-input7:

criteria_scales
~~~~~~~~~~~~~~~

Scales of considered criteria.

The input value should be a valid XMCDA document whose main tag is ``<criteriaScales>``.


------------------------


.. _PROMETHEE-PROMSORT_assignments-PUT-input8:

criteria_thresholds
~~~~~~~~~~~~~~~~~~~

Preference thresholds of considered criteria.

The input value should be a valid XMCDA document whose main tag is ``<criteriaThresholds>``.


------------------------


.. _PROMETHEE-PROMSORT_assignments-PUT-input9:

positive_flows
~~~~~~~~~~~~~~

Positive flows of given alternatives and boundary profiles.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _PROMETHEE-PROMSORT_assignments-PUT-input10:

negative_flows
~~~~~~~~~~~~~~

Negative flows of given alternatives and boundary profiles.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _PROMETHEE-PROMSORT_assignments-PUT-input3:

categories_profiles
~~~~~~~~~~~~~~~~~~~

Definitions of boundary profiles which should be used for classes (categories) representation.

The input value should be a valid XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _PROMETHEE-PROMSORT_assignments-PUT-input4:

categories_values
~~~~~~~~~~~~~~~~~

Marks of categories (higher mark means better category). Each category need to have unique mark from 1 to C, where C is a number of categories.

The input value should be a valid XMCDA document whose main tag is ``<categoriesValues>``.


------------------------


.. _PROMETHEE-PROMSORT_assignments-PUT-input11:

method_parameters
~~~~~~~~~~~~~~~~~

A set of parameters provided to tune up the module's operation.

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
                
                <programParameters>
                    <parameter id="cutPoint">
                        <values>
                            <value>
                                <real>%1</real>
                            </value>
                        </values>
                    </parameter>
                    <parameter id="assignToABetterClass">
                        <values>
                            <value>
                                <boolean>%2</boolean>
                            </value>
                        </values>
                    </parameter>
                </programParameters>
	        
            

where:

- **%1** is a parameter named "cut point". This is a float, and the value should conform to the following constraint: The value should be between -1.0 and 1.0 (both included)..  More formally, the constraint is::

      %1 <= 1.0 && %1 >= -1.0 
- **%2** is a parameter named "assign to a better class". This is a boolean.
  The default value is true.


------------------------



.. _PROMETHEE-PROMSORT_assignments-PUT_outputs:

Outputs
-------


- :ref:`first_step_assignments <PROMETHEE-PROMSORT_assignments-PUT-output1>`
- :ref:`final_assignments <PROMETHEE-PROMSORT_assignments-PUT-output2>`
- :ref:`messages <PROMETHEE-PROMSORT_assignments-PUT-output3>`


.. _PROMETHEE-PROMSORT_assignments-PUT-output1:

first_step_assignments
~~~~~~~~~~~~~~~~~~~~~~

Assignments made in a first step of PromSort method. The assignment is imprecise, as some of alternatives can need next steps of PromSort method to return the final (precise) assignment.

The returned value is a XMCDA document whose main tag is ``<alternativesAssignments>``.


------------------------


.. _PROMETHEE-PROMSORT_assignments-PUT-output2:

final_assignments
~~~~~~~~~~~~~~~~~

Final assignments made in a PromSort method.

The returned value is a XMCDA document whose main tag is ``<alternativesAssignments>``.


------------------------


.. _PROMETHEE-PROMSORT_assignments-PUT-output3:

messages
~~~~~~~~

Messages or errors generated by this module.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/PROMETHEE-PROMSORT_assignments-PUT/description-wsDD.xml>`
