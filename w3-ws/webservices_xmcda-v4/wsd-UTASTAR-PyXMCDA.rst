:orphan:

.. _UTASTAR-PyXMCDA:

UTASTAR
=======

:Version: 1.0
:Provider: PyXMCDA
:SOAP service's name: ``UTASTAR-PyXMCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Perform UTASTAR computation on provided inputs.
Decision Maker's Preferences can be provided using either a global ranking of reference alternatives, or the pair-wise alternatives preferences and indifferences.
Outputs optimal valueFunctions along their valuesErrors.
Can also apply post-optimality analysis for a more balanced output.

The service generate discrete functions for criteria with qualitative scales.
Number of segments will be still be used to compute a continuous marginal utility function.
It is therefore advised to use (n - 1) as the number of segments  (with n the number of labels of the qualitative scale), while using incremented integer values in such scale definition (e.g. \{ 1, 2, 3,..., n\}).
Labels of the scale are used as the abscissa.

This service raises an error if the solution found does not give the same ranking of reference alternatives than in inputs (even with application of the values errors, called sigma in (Munda, 2005).

The implementation and indexing conventions are based on:

    Munda, G. (2005). UTA Methods.
    In Multiple criteria decision analysis: State of the art surveys
    (pp 297-343). Springer, New York, NY.

N.B: This service uses the python module Pulp for representing and solving UTA problems.

**Contact:** Nicolas Duminy (nicolas.duminy@telecom-bretagne.eu)

**Web page:** https://gitlab.com/nduminy/ws-pyxmcda


Inputs
------
(For outputs, see :ref:`below <UTASTAR-PyXMCDA_outputs>`)


- :ref:`alternatives <UTASTAR-PyXMCDA-alternatives>` *(optional)*
- :ref:`criteria <UTASTAR-PyXMCDA-criteria>` *(optional)*
- :ref:`performanceTable <UTASTAR-PyXMCDA-performanceTable>`
- :ref:`alternativesPreferences <UTASTAR-PyXMCDA-alternativesPreferences>` *(optional)*
- :ref:`alternativesIndifferences <UTASTAR-PyXMCDA-alternativesIndifferences>` *(optional)*
- :ref:`alternativesRanks <UTASTAR-PyXMCDA-alternativesRanks>` *(optional)*
- :ref:`criteriaScales <UTASTAR-PyXMCDA-criteriaScales>` *(optional)*
- :ref:`criteriaSegments <UTASTAR-PyXMCDA-criteriaSegments>`
- :ref:`parameters <UTASTAR-PyXMCDA-parameters>`


.. _UTASTAR-PyXMCDA-alternatives:

alternatives *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~

The alternatives. Only used to exclude inactive alternatives from computations (all are considered if not provided).

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _UTASTAR-PyXMCDA-criteria:

criteria *(optional)*
~~~~~~~~~~~~~~~~~~~~~

The criteria. Only used to exclude inactive criteria from computations (all are considered if not provided).

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _UTASTAR-PyXMCDA-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

The performance table.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _UTASTAR-PyXMCDA-alternativesPreferences:

alternativesPreferences *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The alternatives preferences used to infer utility functions.
                If alternatives is provided, all reference alternatives set as inactive are not considered.

The input value should be a valid XMCDA document whose main tag is ``<alternativesMatrix>``.


------------------------


.. _UTASTAR-PyXMCDA-alternativesIndifferences:

alternativesIndifferences *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The alternatives indifferences used to infer utility functions.
                If alternatives is provided, all reference alternatives set as inactive are not considered.

The input value should be a valid XMCDA document whose main tag is ``<alternativesMatrix>``.


------------------------


.. _UTASTAR-PyXMCDA-alternativesRanks:

alternativesRanks *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ranking of reference alternatives used to infer utility functions.
                If alternatives is provided, all reference alternatives set as inactive are not considered.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _UTASTAR-PyXMCDA-criteriaScales:

criteriaScales *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~

The criteria scales. Mandatory if qualitative values are in performance table.
                If not provided, minimum/maximum values will be extracted from performance table,
                and preference direction will be set to max.
                Nominal and fuzzy qualitative scales are forbidden.

The input value should be a valid XMCDA document whose main tag is ``<criteriaScales>``.


------------------------


.. _UTASTAR-PyXMCDA-criteriaSegments:

criteriaSegments
~~~~~~~~~~~~~~~~

Number of segments per marginal utility function.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _UTASTAR-PyXMCDA-parameters:

parameters
~~~~~~~~~~

Parameters of the method

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
    <programParameters>
        <!-- %N -->
        <programParameter id="discrimination_threshold" name="Discrimination threshold">
	        <values>
                <value>
		            <real>%2</real>
                </value>
	        </values>
        </programParameter>
        <programParameter id="post_optimality" name="Post optimality">
	        <values>
                <value>
		            <boolean>%1</boolean>
                </value>
	        </values>
        </programParameter>%5<programParameter id="significative_figures" name="Significative figures">
	        <values>
                <value>
		            <integer>%4</integer>
                </value>
	        </values>
        </programParameter>
        <programParameter id="atol" name="Absolute tolerance">
	        <values>
                <value>
		            <real>%6</real>
                </value>
	        </values>
        </programParameter>
        <programParameter id="solver" name="Solver">
	        <values>
                <value>
		            <label>%3</label>
                </value>
	        </values>
        </programParameter>
    </programParameters>


where:

- **%N** is a parameter named "Inputs alternatives". It can have the following values:

  - ``ranking``: Use ranking of alternatives

  - ``relations``: Use pairwise preferences and indifferences

  The default value is ranking.

- **%2** is a parameter named "Discrimination threshold". This is a float, and the value should conform to the following constraint: Bigger than 0.0..  More formally, the constraint is::

     %2 > 0
  The default value is 0.001.

- **%1** is a parameter named "Post optimality". It can have the following values:

  - ``true``: Applied

  - ``false``: Not applied

  The default value is false.

- **%5** is a parameter named "Post-optimality threshold". This is a float, and the value should conform to the following constraint: Bigger or equal to 0.0..  More formally, the constraint is::

     %5 >= 0
  The default value is 0.1.

- **%4** is a parameter named "Significative figures". This is a int, and the value should conform to the following constraint: Bigger than 0..  More formally, the constraint is::

     %4 > 0
  The default value is 3.

- **%6** is a parameter named "Absolute tolerance". This is a float, and the value should conform to the following constraint: Bigger than 0.0..  More formally, the constraint is::

     %6 > 0
  The default value is 1e-6.

- **%3** is a parameter named "Solver". It can have the following values:

  - ``cbc``: Coin-Or CBC (version included in Pulp)

  - ``choco``: Choco (version included in Pulp)

  - ``glpk``: GLPK

  The default value is cbc.


------------------------



.. _UTASTAR-PyXMCDA_outputs:

Outputs
-------


- :ref:`valueFunctions <UTASTAR-PyXMCDA-valueFunctions>`
- :ref:`valuesErrors <UTASTAR-PyXMCDA-valuesErrors>`
- :ref:`messages <UTASTAR-PyXMCDA-messages>`


.. _UTASTAR-PyXMCDA-valueFunctions:

valueFunctions
~~~~~~~~~~~~~~

Optimal value functions found by the service.

The returned value is a XMCDA document whose main tag is ``<criteriaFunctions>``.


------------------------


.. _UTASTAR-PyXMCDA-valuesErrors:

valuesErrors
~~~~~~~~~~~~

Optimal values errors found by the service for the reference alternatives.
                The 2 values for each alternatives are respectively sigma+ and sigma-.
                As noted in (Munda, 2005).

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _UTASTAR-PyXMCDA-messages:

messages
~~~~~~~~

Status messages.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/UTASTAR-PyXMCDA/description-wsDD.xml>`
