.. _HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT:

HierarchicalDEA-ValueAdditive-SMAA_efficiencies
===============================================

:Version: 1.0
:Provider: PUT
:SOAP service's name: ``HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Computes efficiency scores for the given DMUs (alternatives) using SMAA-D method and A Data Envelopment Analysis Model with hierarchical structure of outputs. For given number of buckets and samples, returns a matrix with alternatives in each row and buckets representing efficiency intervals in each column. Single cell indicates how many samples gave efficiency scores of respective alternative in respective bucket.

**Contact:** 
            Anna Labijak <anna.labijak@cs.put.poznan.pl>
        


Inputs
------
(For outputs, see :ref:`below <HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT_outputs>`)


- :ref:`units <HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-units>`
- :ref:`inputsOutputsScales <HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-inputsOutputsScales>`
- :ref:`performanceTable <HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-performanceTable>`
- :ref:`hierarchy <HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-hierarchy>`
- :ref:`weightsLinearConstraints <HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-methodParameters>`


.. _HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-units:

units
~~~~~

A list of alternatives (DMUs).

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
                <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
            


------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-inputsOutputsScales:

inputsOutputsScales
~~~~~~~~~~~~~~~~~~~

Information about inputs and outpus (leaf criteria) scales (preference directions) and optionally about boundaries

The input value should be a valid XMCDA document whose main tag is ``<criteriaScales>``.
It must have the following form::

   
                
<criteriaScales>
    <criterionScale>
      <criterionID>[...]</criterionID>
      <scales>
        <scale>
          [...]
        </scale>
      </scales>
    </criterionScale>
    [...]
</criteriaScales>
            


------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A list of alternatives (DMUs) performances (on leaf hierarchy criteria).

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   
                <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<values><value>[...]</value></values>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>
            


------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-hierarchy:

hierarchy
~~~~~~~~~

The hierarchical structure of criteria.

The input value should be a valid XMCDA document whose main tag is ``<criteriaHierarchy>``.
It must have the following form::

   
                <criteriaHierarchy>
						<nodes>
                            <node>
                                <criterionID>[...]</criterionID>
                                <nodes>
                                    <node>
                                        <criterionID>[...]</criterionID>
                                        [...]
                                    </node>
                                    [...]
                                </nodes>
                            </node>
                        <nodes>
					</criteriaHierarchy>
            


------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-weightsLinearConstraints:

weightsLinearConstraints *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of hierarchy criteria linear constraints.

The input value should be a valid XMCDA document whose main tag is ``<criteriaLinearConstraints>``.
It must have the following form::

   
                
            <criteriaLinearConstraints>
                <constraints>
                    <constraint>
                        <elements>
                            <element>
                                <criterionID> [...] </criterionID>
                                <coefficient>
                                    [...]
                                </coefficient>
                            </element>
                            [...]
                        </elements>
                        <operator>[...]</operator>
                        <rhs>
                            [...]
                        </rhs>
                    </constraint>
                    [...]
                </constraints>
            </criteriaLinearConstraints>
            


------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~

Represents method parameters.
                "number of samples" represents the number of samples to generate;
                "number of buckets" represents the number of buckets which efficiency scores will be assigned to;
                "hierarchy node" is the ID of the hierarchy criterion for which the analysis should be performed;
                "transformToUtilities" means if data should be tranformed into values from range [0-1];
                "boundariesProvided" means if inputsOutputs file contains information about min and max data for each factor.

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
                
    <programParameters>
        <parameter id="samplesNb">
            <values>
                <value><integer>%1</integer></value>
            </values>
        </parameter>
        <parameter id="intervalsNb">
            <values>
                <value><integer>%2</integer></value>
            </values>
        </parameter>
        <parameter id="hierarchyNode">
            <values>
                <value><label>%3</label></value>
            </values>
        </parameter>
        <parameter id="transformToUtilities">
      <values>
        <value><boolean>%4</boolean></value>
      </values>
		</parameter>
		<parameter id="boundariesProvided">
      <values>
        <value><boolean>%5</boolean></value>
      </values>
		</parameter>
      <parameter id="randomSeed">
            <values>
                <value><integer>%6</integer></value>
            </values>
        </parameter>
    </programParameters>
            

where:

- **%1** is a parameter named "number of samples". This is a int, and the value should conform to the following constraint: The value should be a positive integer..  More formally, the constraint is::

     
                            %1 > 0
                        
  The default value is 100.

- **%2** is a parameter named "number of buckets". This is a int, and the value should conform to the following constraint: The value should be a positive integer..  More formally, the constraint is::

     
                            %2 > 0
                        
  The default value is 10.

- **%3** is a parameter named "hierarchy node". This is a string.
  The default value is root.

- **%4** is a parameter named "transform to utilities". This is a boolean.
  The default value is true.

- **%5** is a parameter named "boundaries provided". This is a boolean.
  The default value is false.

- **%6** is a parameter named "random seed (-1 for default time-based seed)". This is a int, and the value should conform to the following constraint: The value should be a non-negative integer or -1 if no constant seed required..  More formally, the constraint is::

     
                            %6 >= -1
                        
  The default value is -1.


------------------------



.. _HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT_outputs:

Outputs
-------


- :ref:`efficiencyDistribution <HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-efficiencyDistribution>`
- :ref:`maxEfficiency <HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-maxEfficiency>`
- :ref:`minEfficiency <HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-minEfficiency>`
- :ref:`avgEfficiency <HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-avgEfficiency>`
- :ref:`messages <HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-messages>`


.. _HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-efficiencyDistribution:

efficiencyDistribution
~~~~~~~~~~~~~~~~~~~~~~

A performance table for given alternatives. Single performance consists of attribute criterionID representing certain bucket, and a value representing the ratio of efficiency scores in this bucket.

The returned value is a XMCDA document whose main tag is ``<performanceTable>``.
It has the following form::

   
                <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>[...]</criterionID>
									<values>
                                        <value>[...]</value>
                                    </values>
							</performance>
							[...]
						</alternativePerformances>
                        [...]
					</performanceTable>
            


------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-maxEfficiency:

maxEfficiency
~~~~~~~~~~~~~

A list of alternatives with maximum efficiency scores (obtained with sampling).

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   
                <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
                          <values>
                                <value>
                                    [...]
                                </value>
                          </values>
						</alternativeValue>
						[...]
					</alternativesValues>
            


------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-minEfficiency:

minEfficiency
~~~~~~~~~~~~~

A list of alternatives with computed minimum efficiency scores (obtained with sampling).

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   
                <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
                          <values>
                                <value>
                                    [...]
                                </value>
                          </values>
						</alternativeValue>
						[...]
					</alternativesValues>
            


------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-avgEfficiency:

avgEfficiency
~~~~~~~~~~~~~

A list of alternatives with average efficiency scores (obtained with sampling).

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   
                <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
                          <values>
                                <value>
                                    [...]
                                </value>
                          </values>
						</alternativeValue>
						[...]
					</alternativesValues>
            


------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT/description-wsDD.xml>`
