.. _HierarchicalDEA-ValueAdditive-SMAA_preferenceRelations-PUT:

HierarchicalDEA-ValueAdditive-SMAA_preferenceRelations
======================================================

:Version: 1.0
:Provider: PUT
:SOAP service's name: ``HierarchicalDEA-ValueAdditive-SMAA_preferenceRelations-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Determines dominance relations for the given DMUs (alternatives) using SMAA-D method and Additive Data Envelopment Analysis Model with hierarchical structure of inputs and outputs. For given number of samples  returns a matrix with alternatives in each row and column. Single cell indicates how many samples of alternative in a row dominates alternative in a column.

**Contact:** Anna Labijak <anna.labijak@cs.put.poznan.pl>


Inputs
------
(For outputs, see :ref:`below <HierarchicalDEA-ValueAdditive-SMAA_preferenceRelations-PUT_outputs>`)


- :ref:`units <HierarchicalDEA-ValueAdditive-SMAA_preferenceRelations-PUT-units>`
- :ref:`inputsOutputsScales <HierarchicalDEA-ValueAdditive-SMAA_preferenceRelations-PUT-inputsOutputsScales>`
- :ref:`performanceTable <HierarchicalDEA-ValueAdditive-SMAA_preferenceRelations-PUT-performanceTable>`
- :ref:`hierarchy <HierarchicalDEA-ValueAdditive-SMAA_preferenceRelations-PUT-hierarchy>`
- :ref:`weightsLinearConstraints <HierarchicalDEA-ValueAdditive-SMAA_preferenceRelations-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <HierarchicalDEA-ValueAdditive-SMAA_preferenceRelations-PUT-methodParameters>`


.. _HierarchicalDEA-ValueAdditive-SMAA_preferenceRelations-PUT-units:

units
~~~~~

A list of alternatives (DMUs).

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
                <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
            


------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_preferenceRelations-PUT-inputsOutputsScales:

inputsOutputsScales
~~~~~~~~~~~~~~~~~~~

Information about inputs and outpus (leaf criteria) scales (preference directions) and optionally about boundaries

The input value should be a valid XMCDA document whose main tag is ``<criteriaScales>``.
It must have the following form::

   
                
<criteriaScales>
    <criterionScale>
      <criterionID>[...]</criterionID>
      <scales>
        <scale>
          [...]
        </scale>
      </scales>
    </criterionScale>
    [...]
</criteriaScales>
            


------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_preferenceRelations-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A list of alternatives (DMUs) performances (on leaf hierarchy criteria).

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   
                <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<values><value>[...]</value></values>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>
            


------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_preferenceRelations-PUT-hierarchy:

hierarchy
~~~~~~~~~

The hierarchical structure of criteria.

The input value should be a valid XMCDA document whose main tag is ``<criteriaHierarchy>``.
It must have the following form::

   
                <criteriaHierarchy>
						<nodes>
                            <node>
                                <criterionID>[...]</criterionID>
                                <nodes>
                                    <node>
                                        <criterionID>[...]</criterionID>
                                        [...]
                                    </node>
                                    [...]
                                </nodes>
                            </node>
                        <nodes>
					</criteriaHierarchy>
            


------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_preferenceRelations-PUT-weightsLinearConstraints:

weightsLinearConstraints *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of hierarchy criteria linear constraints.

The input value should be a valid XMCDA document whose main tag is ``<criteriaLinearConstraints>``.
It must have the following form::

   
                
            <criteriaLinearConstraints>
                <constraints>
                    <constraint>
                        <elements>
                            <element>
                                <criterionID> [...] </criterionID>
                                <coefficient>
                                    [...]
                                </coefficient>
                            </element>
                            [...]
                        </elements>
                        <operator>[...]</operator>
                        <rhs>
                            [...]
                        </rhs>
                    </constraint>
                    [...]
                </constraints>
            </criteriaLinearConstraints>
            


------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_preferenceRelations-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~

Represents parameters.
            "hierarchy node" is the ID of the hierarchy criterion for which the analysis should be performed,
            "transform to utilities" means if data should be tranformed into values from range [0-1],
            "boundaries provided" means if inputsOutputs file contains information about min and max data for each factor,
            "number of samples" determines number of samples used to calculate results.

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
                
	<programParameters>
		<parameter id="samplesNb">
	    <values>
         <value><integer>%1</integer></value>
       </values>
		</parameter>
		<parameter id="hierarchyNode">
			<values>
			<value><label>%2</label></value>
			</values>
		</parameter>
		<parameter id="transformToUtilities">
      <values>
			  <value><boolean>%3</boolean></value>
      </values>
		</parameter>
		<parameter id="boundariesProvided">
      <values>
			  <value><boolean>%4</boolean></value>
      </values>
		</parameter>
         <parameter id="randomSeed">
            <values>
                <value><integer>%5</integer></value>
            </values>
        </parameter>
	</programParameters>

            

where:

- **%1** is a parameter named "number of samples". This is a int, and the value should conform to the following constraint: The value should be a positive integer..  More formally, the constraint is::

     
                            %1 > 0
                        
  The default value is 100.

- **%2** is a parameter named "hierarchy node". This is a string.
  The default value is root.

- **%3** is a parameter named "transform to utilities". This is a boolean.
  The default value is true.

- **%4** is a parameter named "boundaries provided". This is a boolean.
  The default value is false.

- **%5** is a parameter named "random seed (-1 for default time-based seed)". This is a int, and the value should conform to the following constraint: The value should be a non-negative integer or -1 if no constant seed required..  More formally, the constraint is::

     
                        %5 >= -1
                    
  The default value is -1.


------------------------



.. _HierarchicalDEA-ValueAdditive-SMAA_preferenceRelations-PUT_outputs:

Outputs
-------


- :ref:`pairwiseOutrankingIndices <HierarchicalDEA-ValueAdditive-SMAA_preferenceRelations-PUT-pairwiseOutrankingIndices>`
- :ref:`messages <HierarchicalDEA-ValueAdditive-SMAA_preferenceRelations-PUT-messages>`


.. _HierarchicalDEA-ValueAdditive-SMAA_preferenceRelations-PUT-pairwiseOutrankingIndices:

pairwiseOutrankingIndices
~~~~~~~~~~~~~~~~~~~~~~~~~

A performance table for given alternatives. Single performance consists of attribute criterionID representing dominated alternative, and a value representing ratio of samples dominating this alternative.

The returned value is a XMCDA document whose main tag is ``<performanceTable>``.
It has the following form::

   
                <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID> geq [...]</criterionID>
								<values>
                    <value>[...]</value>
                </values>
							</performance>
							[...]
						</alternativePerformances>
            [...]
					</performanceTable>
            


------------------------


.. _HierarchicalDEA-ValueAdditive-SMAA_preferenceRelations-PUT-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/HierarchicalDEA-ValueAdditive-SMAA_preferenceRelations-PUT/description-wsDD.xml>`
