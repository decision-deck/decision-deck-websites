.. _csvToXMCDA-criteriaThresholds-PyXMCDA:

csvToXMCDA-criteriaThresholds
=============================

:Version: 2.0
:Provider: PyXMCDA
:SOAP service's name: ``csvToXMCDA-criteriaThresholds-PyXMCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Transforms a file containing criteria discrimination thresholds and preference directions from a comma-separated values (CSV) file to three XMCDA compliant files, containing resp. the criteria ids, their preference direction and the related discrimination thresholds.

**Contact:** Sébastien Bigaret (sebastien.bigaret@telecom-bretagne.eu)

**Web page:** https://gitlab.com/sbigaret/ws-pyxmcda


Inputs
------
(For outputs, see :ref:`below <csvToXMCDA-criteriaThresholds-PyXMCDA_outputs>`)


- :ref:`thresholds.csv <csvToXMCDA-criteriaThresholds-PyXMCDA-csv>`
- :ref:`parameters <csvToXMCDA-criteriaThresholds-PyXMCDA-parameters>`


.. _csvToXMCDA-criteriaThresholds-PyXMCDA-csv:

thresholds.csv
~~~~~~~~~~~~~~

A csv with criteria' scales and thresholds.

Example:
  ,cost,risks,employment,connection
  ind,1,2,3,4
  pref,2,3,4,5
  veto,3,4.6,5,6
  preferenceDirection,min,max,min,max

The labels of the separation thresholds ("ind", "pref", "veto") can be chosen freely in order to be in accordance with the selected outranking method. Further separation thresholds can also be added. The last line always represents the preferenceDirection; its label must be "preferenceDirection".

The separator used in csv will be determined by examing the file (this means that it can be different than a comma: a semicolon, a tab or space character, etc.).

Thresholds values should be float; both decimal separator '.' and ',' are supported.  If a threshold value is left empty, the corresponding combination (criterion id, separation thresholds) is not present in the XMCDA output.  Same for "preferenceDirection": the corresponding tag 'scale' is present in the XMCDA output only if the preferenceDirection is supplied.

The input value should be a valid XMCDA document whose main tag is ``<other>``.


------------------------


.. _csvToXMCDA-criteriaThresholds-PyXMCDA-parameters:

parameters
~~~~~~~~~~

Parameters of the method

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
    <programParameters>
        <parameter id="csv_delimiter">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </parameter>
        <parameter id="default_prefix">
            <values>
                <value>
                    <label>%2</label>
                </value>
            </values>
        </parameter>
        <parameter id="name_in_id">
            <values>
                <value>
                    <boolean>%3</boolean>
                </value>
            </values>
        </parameter>
    </programParameters>


where:

- **%1** is a parameter named "CSV delimiter". This is a string, and the value should conform to the following constraint: One character maximum.  More formally, the constraint is::

     %1.length() < 2
- **%2** is a parameter named "Default content". It can have the following values:

  - ``float``: float

  - ``integer``: integer

  The default value is float.

- **%3** is a parameter named "First line". It can have the following values:

  - ``false``: id

  - ``true``: id (name)

  The default value is id_and_name.


------------------------



.. _csvToXMCDA-criteriaThresholds-PyXMCDA_outputs:

Outputs
-------


- :ref:`criteria <csvToXMCDA-criteriaThresholds-PyXMCDA-criteria>`
- :ref:`criteriaScales <csvToXMCDA-criteriaThresholds-PyXMCDA-criteria_scales>`
- :ref:`criteriaThresholds <csvToXMCDA-criteriaThresholds-PyXMCDA-criteria_thresholds>`
- :ref:`messages <csvToXMCDA-criteriaThresholds-PyXMCDA-messages>`


.. _csvToXMCDA-criteriaThresholds-PyXMCDA-criteria:

criteria
~~~~~~~~

The equivalent XMCDA file containing the criteria declared in the csv.

The returned value is a XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _csvToXMCDA-criteriaThresholds-PyXMCDA-criteria_scales:

criteriaScales
~~~~~~~~~~~~~~

The equivalent XMCDA file containing the preference directions of the criteria declared in the csv.

The returned value is a XMCDA document whose main tag is ``<criteriaScales>``.


------------------------


.. _csvToXMCDA-criteriaThresholds-PyXMCDA-criteria_thresholds:

criteriaThresholds
~~~~~~~~~~~~~~~~~~

The equivalent XMCDA file containing the discrimination thresholds.

The returned value is a XMCDA document whose main tag is ``<criteriaThresholds>``.


------------------------


.. _csvToXMCDA-criteriaThresholds-PyXMCDA-messages:

messages
~~~~~~~~

Status messages.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/csvToXMCDA-criteriaThresholds-PyXMCDA/description-wsDD.xml>`
