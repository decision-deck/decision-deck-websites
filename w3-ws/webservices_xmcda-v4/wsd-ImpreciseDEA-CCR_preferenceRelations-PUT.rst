.. _ImpreciseDEA-CCR_preferenceRelations-PUT:

ImpreciseDEA-CCR_preferenceRelations
====================================

:Version: 1.0
:Provider: PUT
:SOAP service's name: ``ImpreciseDEA-CCR_preferenceRelations-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Computes extrene efficiency ranks for the given DMUs (alternatives) using CCR Data Envelopment Analysis Model.

**Contact:** Anna Labijak <support@decision-deck.org>


Inputs
------
(For outputs, see :ref:`below <ImpreciseDEA-CCR_preferenceRelations-PUT_outputs>`)


- :ref:`units <ImpreciseDEA-CCR_preferenceRelations-PUT-units>`
- :ref:`inputsOutputs <ImpreciseDEA-CCR_preferenceRelations-PUT-inputsOutputs>`
- :ref:`inputsOutputsScales <ImpreciseDEA-CCR_preferenceRelations-PUT-inputsOutputsScales>`
- :ref:`performanceTable <ImpreciseDEA-CCR_preferenceRelations-PUT-performanceTable>`
- :ref:`maxPerformanceTable <ImpreciseDEA-CCR_preferenceRelations-PUT-maxPerformanceTable>` *(optional)*
- :ref:`weightsLinearConstraints <ImpreciseDEA-CCR_preferenceRelations-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <ImpreciseDEA-CCR_preferenceRelations-PUT-methodParameters>`


.. _ImpreciseDEA-CCR_preferenceRelations-PUT-units:

units
~~~~~

A list of alternatives (DMUs).

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>


------------------------


.. _ImpreciseDEA-CCR_preferenceRelations-PUT-inputsOutputs:

inputsOutputs
~~~~~~~~~~~~~

A list of criteria.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   <criteria>
                        <criterion>[...]</criterion>
                        [...]
                    </criteria>


------------------------


.. _ImpreciseDEA-CCR_preferenceRelations-PUT-inputsOutputsScales:

inputsOutputsScales
~~~~~~~~~~~~~~~~~~~

Informations about inputs and outputs scales and optionally about boundaries

The input value should be a valid XMCDA document whose main tag is ``<criteriaScales>``.
It must have the following form::

   
<criteriaScales>
    <criterionScale>
      <criterionID>[...]</criterionID>
      <scales>
        <scale>
          [...]
        </scale>
      </scales>
    </criterionScale>
    [...]
</criteriaScales>


------------------------


.. _ImpreciseDEA-CCR_preferenceRelations-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A list of alternatives (DMUs) minimal performances (or exact performances if intervals should be created by tolerance).

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<values><value>[...]</value></values>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>


------------------------


.. _ImpreciseDEA-CCR_preferenceRelations-PUT-maxPerformanceTable:

maxPerformanceTable *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of alternatives (DMUs) maximal performances.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<values><value>[...]</value></values>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>


------------------------


.. _ImpreciseDEA-CCR_preferenceRelations-PUT-weightsLinearConstraints:

weightsLinearConstraints *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of criteria linear constraints.

The input value should be a valid XMCDA document whose main tag is ``<criteriaLinearConstraints>``.
It must have the following form::

   
            <criteriaLinearConstraints>
                <constraints>
                    <constraint>
                        <elements>
                            <element>
                                <criterionID> [...] </criterionID>
                                <coefficient>
                                    [...]
                                </coefficient>
                            </element>
                            [...]
                        </elements>
                        <operator>[...]</operator>
                        <rhs>
                            [...]
                        </rhs>
                    </constraint>
                    [...]
                </constraints>
            </criteriaLinearConstraints>


------------------------


.. _ImpreciseDEA-CCR_preferenceRelations-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~

Represents method parameters (tolerance).

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
    <programParameters>
        <parameter id="tolerance">
            <values>
                <value><real>%1</real></value>
            </values>
        </parameter>
    </programParameters>

where:

- **%1** is a parameter named "tolerance". This is a float, and the value should conform to the following constraint: The value should be a non-negative number..  More formally, the constraint is::

     %1 >= 0
  The default value is 0.00.


------------------------



.. _ImpreciseDEA-CCR_preferenceRelations-PUT_outputs:

Outputs
-------


- :ref:`necessaryDominance <ImpreciseDEA-CCR_preferenceRelations-PUT-necessaryDominance>`
- :ref:`possibleDominance <ImpreciseDEA-CCR_preferenceRelations-PUT-possibleDominance>`
- :ref:`messages <ImpreciseDEA-CCR_preferenceRelations-PUT-messages>`


.. _ImpreciseDEA-CCR_preferenceRelations-PUT-necessaryDominance:

necessaryDominance
~~~~~~~~~~~~~~~~~~

A list of pairs of pairs of DMU related with necessary preference relation.

The returned value is a XMCDA document whose main tag is ``<alternativesMatrix>``.
It has the following form::

   
                    <alternativesMatrix>
						<row>
                            <alternativeID>[...]</alternativeID>
                            <column>
                                <alternativeID>[...]</alternativeID>
                                <values>
                                    <value><integer>1</integer></value>
                                </values>
                            </column>
                            [...]
                        </row>
                        [...]
					</alternativesMatrix>
                    


------------------------


.. _ImpreciseDEA-CCR_preferenceRelations-PUT-possibleDominance:

possibleDominance
~~~~~~~~~~~~~~~~~

A list of pairs of pairs of DMU related with possible preference relation.

The returned value is a XMCDA document whose main tag is ``<alternativesMatrix>``.
It has the following form::

   
                    <alternativesMatrix>
						<row>
                            <alternativeID>[...]</alternativeID>
                            <column>
                                <alternativeID>[...]</alternativeID>
                                <values>
                                    <value><integer>1</integer></value>
                                </values>
                            </column>
                            [...]
                        </row>
                        [...]
					</alternativesMatrix>
                    


------------------------


.. _ImpreciseDEA-CCR_preferenceRelations-PUT-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/ImpreciseDEA-CCR_preferenceRelations-PUT/description-wsDD.xml>`
