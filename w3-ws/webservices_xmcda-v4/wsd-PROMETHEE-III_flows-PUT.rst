.. _PROMETHEE-III_flows-PUT:

PROMETHEE-III_flows
===================

:Version: 1.0.0
:Provider: PUT
:SOAP service's name: ``PROMETHEE-III_flows-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

This module is calculating ranking using Promethee III method. The result is shown as the table of outranking relationship, where the relationship is a weak preference. The second output presents the limits of the interval for every alternative.

**Contact:** Magdalena Dzi?cielska <magdalenadziecielska6@gmail.com>

**Web page:** https://github.com/MagdalenaDziecielska/PrometheeDiviz


Inputs
------
(For outputs, see :ref:`below <PROMETHEE-III_flows-PUT_outputs>`)


- :ref:`alternatives <PROMETHEE-III_flows-PUT-input1>`
- :ref:`preferences <PROMETHEE-III_flows-PUT-input2>`
- :ref:`parameters <PROMETHEE-III_flows-PUT-input3>`


.. _PROMETHEE-III_flows-PUT-input1:

alternatives
~~~~~~~~~~~~

Alternatives to consider.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _PROMETHEE-III_flows-PUT-input2:

preferences
~~~~~~~~~~~

Aggregated preferences.

The input value should be a valid XMCDA document whose main tag is ``<alternativesMatrix>``.


------------------------


.. _PROMETHEE-III_flows-PUT-input3:

parameters
~~~~~~~~~~

Parameter alpha required in Promethee III flows calculation.

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
			
				<programParameters>
					<parameter id="alpha">
						<values>
							<value>
								<real>%1</real>
							</value>
						</values>
					</parameter>
				</programParameters>
			
		

where:

- **%1** is a parameter named "alpha". This is a float.
  The default value is 0.0.


------------------------



.. _PROMETHEE-III_flows-PUT_outputs:

Outputs
-------


- :ref:`intervals <PROMETHEE-III_flows-PUT-output2>`
- :ref:`ranking <PROMETHEE-III_flows-PUT-output1>`
- :ref:`messages <PROMETHEE-III_flows-PUT-output3>`


.. _PROMETHEE-III_flows-PUT-output2:

intervals
~~~~~~~~~

Flows final intervals.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _PROMETHEE-III_flows-PUT-output1:

ranking
~~~~~~~

Result computed from the given data.

The returned value is a XMCDA document whose main tag is ``<alternativesMatrix>``.


------------------------


.. _PROMETHEE-III_flows-PUT-output3:

messages
~~~~~~~~

Messages or errors generated by this module.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/PROMETHEE-III_flows-PUT/description-wsDD.xml>`
