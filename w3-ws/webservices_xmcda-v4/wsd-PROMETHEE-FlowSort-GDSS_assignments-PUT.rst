.. _PROMETHEE-FlowSort-GDSS_assignments-PUT:

PROMETHEE-FlowSort-GDSS_assignments
===================================

:Version: 1.0.0
:Provider: PUT
:SOAP service's name: ``PROMETHEE-FlowSort-GDSS_assignments-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Computes group class assignment for given data using FlowSortGDSS method.

**Contact:** Maciej Uniejewski <maciej.uniejewski@gmail.com>

**Web page:** https://github.com/maciej7777/PrometheeDiviz


Inputs
------
(For outputs, see :ref:`below <PROMETHEE-FlowSort-GDSS_assignments-PUT_outputs>`)


- :ref:`criteria <PROMETHEE-FlowSort-GDSS_assignments-PUT-input14>`
- :ref:`alternatives <PROMETHEE-FlowSort-GDSS_assignments-PUT-input1>`
- :ref:`categories <PROMETHEE-FlowSort-GDSS_assignments-PUT-input2>`
- :ref:`performance_table1 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input26>`
- :ref:`preferences1 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input36>`
- :ref:`flows1 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input16>`
- :ref:`categories_profiles1 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input3>`
- :ref:`performance_table2 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input27>`
- :ref:`preferences2 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input37>`
- :ref:`flows2 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input17>`
- :ref:`categories_profiles2 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input4>`
- :ref:`performance_table3 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input28>` *(optional)*
- :ref:`preferences3 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input38>` *(optional)*
- :ref:`flows3 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input18>` *(optional)*
- :ref:`categories_profiles3 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input5>` *(optional)*
- :ref:`performance_table4 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input29>` *(optional)*
- :ref:`preferences4 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input39>` *(optional)*
- :ref:`flows4 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input19>` *(optional)*
- :ref:`categories_profiles4 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input6>` *(optional)*
- :ref:`performance_table5 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input30>` *(optional)*
- :ref:`preferences5 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input40>` *(optional)*
- :ref:`flows5 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input20>` *(optional)*
- :ref:`categories_profiles5 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input7>` *(optional)*
- :ref:`performance_table6 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input31>` *(optional)*
- :ref:`preferences6 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input41>` *(optional)*
- :ref:`flows6 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input21>` *(optional)*
- :ref:`categories_profiles6 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input8>` *(optional)*
- :ref:`performance_table7 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input32>` *(optional)*
- :ref:`preferences7 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input42>` *(optional)*
- :ref:`flows7 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input22>` *(optional)*
- :ref:`categories_profiles7 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input9>` *(optional)*
- :ref:`performance_table8 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input33>` *(optional)*
- :ref:`preferences8 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input43>` *(optional)*
- :ref:`flows8 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input23>` *(optional)*
- :ref:`categories_profiles8 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input10>` *(optional)*
- :ref:`performance_table9 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input34>` *(optional)*
- :ref:`preferences9 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input44>` *(optional)*
- :ref:`flows9 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input24>` *(optional)*
- :ref:`categories_profiles9 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input11>` *(optional)*
- :ref:`performance_table10 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input35>` *(optional)*
- :ref:`preferences10 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input45>` *(optional)*
- :ref:`flows10 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input25>` *(optional)*
- :ref:`categories_profiles10 <PROMETHEE-FlowSort-GDSS_assignments-PUT-input12>` *(optional)*
- :ref:`criteria_scales <PROMETHEE-FlowSort-GDSS_assignments-PUT-input15>`
- :ref:`profiles_flows <PROMETHEE-FlowSort-GDSS_assignments-PUT-input46>`
- :ref:`criteria_scales <PROMETHEE-FlowSort-GDSS_assignments-PUT-input15>`
- :ref:`profiles_flows <PROMETHEE-FlowSort-GDSS_assignments-PUT-input46>`
- :ref:`categories_values <PROMETHEE-FlowSort-GDSS_assignments-PUT-input13>`
- :ref:`method_parameters <PROMETHEE-FlowSort-GDSS_assignments-PUT-input47>`


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input14:

criteria
~~~~~~~~

Criteria to consider.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input1:

alternatives
~~~~~~~~~~~~

Alternatives to consider.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input2:

categories
~~~~~~~~~~

Definitions of categories.

The input value should be a valid XMCDA document whose main tag is ``<categories>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input26:

performance_table1
~~~~~~~~~~~~~~~~~~

The performances of profiles for decision maker 1.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input36:

preferences1
~~~~~~~~~~~~

The preference matrix computed for profiles and alternatives given by decision maker 1.

The input value should be a valid XMCDA document whose main tag is ``<alternativesMatrix>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input16:

flows1
~~~~~~

Normalised flows of given alternatives for decision maker 1.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input3:

categories_profiles1
~~~~~~~~~~~~~~~~~~~~

Definitions of boundary profiles for decision maker 1, which should be used for classes (categories) representation.

The input value should be a valid XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input27:

performance_table2
~~~~~~~~~~~~~~~~~~

The performances of profiles for decision maker 2.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input37:

preferences2
~~~~~~~~~~~~

The preference matrix computed for profiles and alternatives given by decision maker 2.

The input value should be a valid XMCDA document whose main tag is ``<alternativesMatrix>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input17:

flows2
~~~~~~

Normalised flows of given alternatives for decision maker 2.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input4:

categories_profiles2
~~~~~~~~~~~~~~~~~~~~

Definitions of boundary profiles for decision maker 2, which should be used for classes (categories) representation.

The input value should be a valid XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input28:

performance_table3 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The performances of profiles for decision maker 3.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input38:

preferences3 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~

The preference matrix computed for profiles and alternatives given by decision maker 3.

The input value should be a valid XMCDA document whose main tag is ``<alternativesMatrix>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input18:

flows3 *(optional)*
~~~~~~~~~~~~~~~~~~~

Normalised flows of given alternatives for decision maker 3.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input5:

categories_profiles3 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Definitions of boundary profiles for decision maker 3, which should be used for classes (categories) representation.

The input value should be a valid XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input29:

performance_table4 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The performances of profiles for decision maker 4.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input39:

preferences4 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~

The preference matrix computed for profiles and alternatives given by decision maker 4.

The input value should be a valid XMCDA document whose main tag is ``<alternativesMatrix>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input19:

flows4 *(optional)*
~~~~~~~~~~~~~~~~~~~

Normalised flows of given alternatives for decision maker 4.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input6:

categories_profiles4 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Definitions of boundary profiles for decision maker 4, which should be used for classes (categories) representation.

The input value should be a valid XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input30:

performance_table5 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The performances of profiles for decision maker 5.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input40:

preferences5 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~

The preference matrix computed for profiles and alternatives given by decision maker 5.

The input value should be a valid XMCDA document whose main tag is ``<alternativesMatrix>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input20:

flows5 *(optional)*
~~~~~~~~~~~~~~~~~~~

Normalised flows of given alternatives for decision maker 5.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input7:

categories_profiles5 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Definitions of boundary profiles for decision maker 5, which should be used for classes (categories) representation.

The input value should be a valid XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input31:

performance_table6 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The performances of profiles for decision maker 6.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input41:

preferences6 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~

The preference matrix computed for profiles and alternatives given by decision maker 6.

The input value should be a valid XMCDA document whose main tag is ``<alternativesMatrix>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input21:

flows6 *(optional)*
~~~~~~~~~~~~~~~~~~~

Normalised flows of given alternatives for decision maker 6.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input8:

categories_profiles6 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Definitions of boundary profiles for decision maker 6, which should be used for classes (categories) representation.

The input value should be a valid XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input32:

performance_table7 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The performances of profiles for decision maker 7.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input42:

preferences7 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~

The preference matrix computed for profiles and alternatives given by decision maker 7.

The input value should be a valid XMCDA document whose main tag is ``<alternativesMatrix>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input22:

flows7 *(optional)*
~~~~~~~~~~~~~~~~~~~

Normalised flows of given alternatives for decision maker 7.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input9:

categories_profiles7 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Definitions of boundary profiles for decision maker 7, which should be used for classes (categories) representation.

The input value should be a valid XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input33:

performance_table8 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The performances of profiles for decision maker 8.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input43:

preferences8 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~

The preference matrix computed for profiles and alternatives given by decision maker 8.

The input value should be a valid XMCDA document whose main tag is ``<alternativesMatrix>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input23:

flows8 *(optional)*
~~~~~~~~~~~~~~~~~~~

Normalised flows of given alternatives for decision maker 8.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input10:

categories_profiles8 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Definitions of boundary profiles for decision maker 8, which should be used for classes (categories) representation.

The input value should be a valid XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input34:

performance_table9 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The performances of profiles for decision maker 9.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input44:

preferences9 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~

The preference matrix computed for profiles and alternatives given by decision maker 9.

The input value should be a valid XMCDA document whose main tag is ``<alternativesMatrix>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input24:

flows9 *(optional)*
~~~~~~~~~~~~~~~~~~~

Normalised flows of given alternatives for decision maker 9.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input11:

categories_profiles9 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Definitions of boundary profiles for decision maker 9, which should be used for classes (categories) representation.

The input value should be a valid XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input35:

performance_table10 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The performances of profiles for decision maker 10.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input45:

preferences10 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~

The preference matrix computed for profiles and alternatives given by decision maker 10.

The input value should be a valid XMCDA document whose main tag is ``<alternativesMatrix>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input25:

flows10 *(optional)*
~~~~~~~~~~~~~~~~~~~~

Normalised flows of given alternatives for decision maker 10.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input12:

categories_profiles10 *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Definitions of boundary profiles for decision maker 10, which should be used for classes (categories) representation.

The input value should be a valid XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input15:

criteria_scales
~~~~~~~~~~~~~~~

Scales of considered criteria.

The input value should be a valid XMCDA document whose main tag is ``<criteriaScales>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input46:

profiles_flows
~~~~~~~~~~~~~~

Normalised flows of given profiles counted for all decision makers together.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input15:

criteria_scales
~~~~~~~~~~~~~~~

Scales of considered criteria.

The input value should be a valid XMCDA document whose main tag is ``<criteriaScales>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input46:

profiles_flows
~~~~~~~~~~~~~~

Normalised flows of given profiles counted for all decision makers together.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input13:

categories_values
~~~~~~~~~~~~~~~~~

Marks of categories (higher mark means better category). Each category need to have unique mark from 1 to C, where C is a number of categories.

The input value should be a valid XMCDA document whose main tag is ``<categoriesValues>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-input47:

method_parameters
~~~~~~~~~~~~~~~~~

A set of parameters provided to tune up the module's operation.

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
                
                <programParameters>
                    <parameter id="comparisonWithProfiles">
                    	<values>
                            <value>
                                <label>%1</label>
                            </value>
                        </values>
                    </parameter>
                    <parameter id="assignToABetterClass">
                    	<values>
                            <value>
                                <boolean>%2</boolean>
                            </value>
                        </values>
                    </parameter>
                    <parameter id="decisionMaker1">
                    	<values>
                            <value>
                                <real>%3</real>
                            </value>
                        </values>
                    </parameter>
                    <parameter id="decisionMaker2">
                    	<values>
                            <value>
                                <real>%4</real>
                            </value>
                        </values>
                    </parameter>
                    <parameter id="decisionMaker3">
                    	<values>
                            <value>
                                <real>%5</real>
                            </value>
                        </values>
                    </parameter>
                    <parameter id="decisionMaker4">
                    	<values>
                            <value>
                                <real>%6</real>
                            </value>
                        </values>
                    </parameter>
                    <parameter id="decisionMaker5">
                    	<values>
                            <value>
                                <real>%7</real>
                            </value>
                        </values>
                    </parameter>
                    <parameter id="decisionMaker6">
                    	<values>
                            <value>
                                <real>%8</real>
                            </value>
                        </values>
                    </parameter>
                    <parameter id="decisionMaker7">
                    	<values>
                            <value>
                                <real>%9</real>
                            </value>
                        </values>
                    </parameter>
                    <parameter id="decisionMaker8">
                    	<values>
                            <value>
                                <real>%10</real>
                            </value>
                        </values>
                    </parameter>
                    <parameter id="decisionMaker9">
                    	<values>
                            <value>
                                <real>%11</real>
                            </value>
                        </values>
                    </parameter>
                    <parameter id="decisionMaker10">
                    	<values>
                            <value>
                                <real>%12</real>
                            </value>
                        </values>
                    </parameter>
                </programParameters>
	        
            

where:

- **%1** is a parameter named "comparison with profiles". It can have the following values:

  - ``central``: central profiles

  - ``bounding``: boundary profiles

  The default value is central.

- **%2** is a parameter named "assign to a better class". This is a boolean.
  The default value is true.

- **%3** is a parameter named "decision maker 1 weight". This is a float, and the value should conform to the following constraint: A float value..  More formally, the constraint is::

      %3 >= 0 
  The default value is 0.0.

- **%4** is a parameter named "decision maker 2 weight:". This is a float, and the value should conform to the following constraint: A float value..  More formally, the constraint is::

      %4 >= 0 
  The default value is 0.0.

- **%5** is a parameter named "decision maker 3 weight:". This is a float, and the value should conform to the following constraint: A float value. Choose 0.0 when decision maker does not exist..  More formally, the constraint is::

      %5 >= 0 
  The default value is 0.0.

- **%6** is a parameter named "decision maker 4 weight". This is a float, and the value should conform to the following constraint: A float value. Choose 0.0 when decision maker does not exist..  More formally, the constraint is::

      %6 >= 0 
  The default value is 0.0.

- **%7** is a parameter named "decision maker 5 weight". This is a float, and the value should conform to the following constraint: A float value. Choose 0.0 when decision maker does not exist..  More formally, the constraint is::

      %7 >= 0 
  The default value is 0.0.

- **%8** is a parameter named "decision maker 6 weight". This is a float, and the value should conform to the following constraint: A float value. Choose 0.0 when decision maker does not exist..  More formally, the constraint is::

      %8 >= 0 
  The default value is 0.0.

- **%9** is a parameter named "decision maker 7 weight". This is a float, and the value should conform to the following constraint: A float value. Choose 0.0 when decision maker does not exist..  More formally, the constraint is::

      %9 >= 0 
  The default value is 0.0.

- **%10** is a parameter named "decision maker 8 weight". This is a float, and the value should conform to the following constraint: A float value. Choose 0.0 when decision maker does not exist..  More formally, the constraint is::

      %10 >= 0 
  The default value is 0.0.

- **%11** is a parameter named "decision maker 9 weight". This is a float, and the value should conform to the following constraint: A float value. Choose 0.0 when decision maker does not exist..  More formally, the constraint is::

      %11 >= 0 
  The default value is 0.0.

- **%12** is a parameter named "decision maker 10 weight". This is a float, and the value should conform to the following constraint: A float value. Choose 0.0 when decision maker does not exist..  More formally, the constraint is::

      %12 >= 0 
  The default value is 0.0.


------------------------



.. _PROMETHEE-FlowSort-GDSS_assignments-PUT_outputs:

Outputs
-------


- :ref:`first_step_assignments <PROMETHEE-FlowSort-GDSS_assignments-PUT-output1>`
- :ref:`final_assignments <PROMETHEE-FlowSort-GDSS_assignments-PUT-output2>`
- :ref:`messages <PROMETHEE-FlowSort-GDSS_assignments-PUT-output3>`


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-output1:

first_step_assignments
~~~~~~~~~~~~~~~~~~~~~~

Assignments made in a first step of FlowSortGDSS process. They shows for which alternatives decision makers were not able to make an unanimous assignment.

The returned value is a XMCDA document whose main tag is ``<alternativesAssignments>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-output2:

final_assignments
~~~~~~~~~~~~~~~~~

Final assignments made in a FlowSortGDSS process.

The returned value is a XMCDA document whose main tag is ``<alternativesAssignments>``.


------------------------


.. _PROMETHEE-FlowSort-GDSS_assignments-PUT-output3:

messages
~~~~~~~~

Messages or errors generated by this module.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/PROMETHEE-FlowSort-GDSS_assignments-PUT/description-wsDD.xml>`
