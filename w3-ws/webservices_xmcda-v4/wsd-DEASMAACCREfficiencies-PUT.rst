.. _DEASMAACCREfficiencies-PUT:

DEASMAACCREfficiencies
======================

:Version: 1.0
:Provider: PUT
:SOAP service's name: ``DEASMAACCREfficiencies-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Computes efficiency scores for the given DMUs (alternatives) using SMAA-D method and CCR Data Envelopment Analysis Model. For given number of buckets and samples, returns a matrix with alternatives in each row and buckets representing efficiency intervals in each column. Single cell indicates how many samples gave efficiency scores of respective alternative in respective bucket.

**Contact:** Malgorzata Napieraj (napieraj.malgorzata@gmail.com)

**Reference:** Cooper W., Seiford L., Tone K., Data Envelopment Analysis: A Comprehensive Text with Models, Applications, References and DEA-Solver (2007).

**Reference:** Lahdelma R., Salminen P., Stochastic multicriteria acceptability analysis using the data envelopment model (2004).


Inputs
------
(For outputs, see :ref:`below <DEASMAACCREfficiencies-PUT_outputs>`)


- :ref:`inputsOutputs <DEASMAACCREfficiencies-PUT-inputsOutputs>`
- :ref:`units <DEASMAACCREfficiencies-PUT-units>`
- :ref:`performanceTable <DEASMAACCREfficiencies-PUT-performanceTable>`
- :ref:`weightsLinearConstraints <DEASMAACCREfficiencies-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <DEASMAACCREfficiencies-PUT-methodParameters>`


.. _DEASMAACCREfficiencies-PUT-inputsOutputs:

inputsOutputs
~~~~~~~~~~~~~

A list of criteria with specified preference direction. List has to contains at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output).

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   <criteria>
                        <criterion>
							<scale>
								<quantitative>
									<preferenceDirection>
										[...]
									</preferenceDirection>
								</quantitative>
							</scale>
                            [...]
                        </criterion>
                        [...]
                    </criteria>


------------------------


.. _DEASMAACCREfficiencies-PUT-units:

units
~~~~~

A list of alternatives (DMUs).

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>


------------------------


.. _DEASMAACCREfficiencies-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A list of alternatives (DMUs) performances.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   <performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>


------------------------


.. _DEASMAACCREfficiencies-PUT-weightsLinearConstraints:

weightsLinearConstraints *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of criteria linear constraints.

The input value should be a valid XMCDA document whose main tag is ``<criteriaLinearConstraints>``.
It must have the following form::

   <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID>[...]</criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>


------------------------


.. _DEASMAACCREfficiencies-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~

"samplesNo" represents the number of samples to generate; "intervalsNo" represents the number of buckets which efficiency scores will be assigned to.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   <methodParameters>
							<parameter name="samplesNo">
								<value><integer>%1</integer></value>
							</parameter>
							<parameter name="intervalsNo">
								<value><integer>%2</integer></value>
							</parameter>
					</methodParameters>

where:

- **%1** is a parameter named "samplesNo". This is a int, and the value should conform to the following constraint: The value should be a positive integer..  More formally, the constraint is::

      %1 > 0 
  The default value is 100.

- **%2** is a parameter named "intervalsNo". This is a int, and the value should conform to the following constraint: The value should be a positive integer..  More formally, the constraint is::

      %2 > 0 
  The default value is 10.


------------------------



.. _DEASMAACCREfficiencies-PUT_outputs:

Outputs
-------


- :ref:`efficiencyDistribution <DEASMAACCREfficiencies-PUT-efficiencyDistribution>`
- :ref:`maxEfficiency <DEASMAACCREfficiencies-PUT-maxEfficiency>`
- :ref:`minEfficiency <DEASMAACCREfficiencies-PUT-minEfficiency>`
- :ref:`avgEfficiency <DEASMAACCREfficiencies-PUT-avgEfficiency>`
- :ref:`messages <DEASMAACCREfficiencies-PUT-messages>`


.. _DEASMAACCREfficiencies-PUT-efficiencyDistribution:

efficiencyDistribution
~~~~~~~~~~~~~~~~~~~~~~

A performance table for given alternatives. Single performance consists of attribute criterionID representing certain bucket, and a value representing the ratio of efficiency scores in this bucket.

The returned value is a XMCDA document whose main tag is ``<performanceTable>``.
It has the following form::

   <performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>Bucket [...]</criterionID>
									<value>
									[...]
									</value>
							</performance>
							[...]
						</alternativePerformances>
					</performanceTable>


------------------------


.. _DEASMAACCREfficiencies-PUT-maxEfficiency:

maxEfficiency
~~~~~~~~~~~~~

A list of alternatives with maximum efficiency scores (obtained for given sample).

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   <alternativesValues mcdaConcept="efficiency">
						<alternativeValue>
						  <alternativeID>[...]</alternativeID>
						  <value>
							[...]
						  </value>
						</alternativeValue>
						[...]
					</alternativesValues>


------------------------


.. _DEASMAACCREfficiencies-PUT-minEfficiency:

minEfficiency
~~~~~~~~~~~~~

A list of alternatives with computed minimum efficiency scores (obtained for given sample).

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   <alternativesValues mcdaConcept="efficiency">
						<alternativeValue>
						  <alternativeID>[...]</alternativeID>
						  <value>
							[...]
						  </value>
						</alternativeValue>
						[...]
					</alternativesValues>


------------------------


.. _DEASMAACCREfficiencies-PUT-avgEfficiency:

avgEfficiency
~~~~~~~~~~~~~

A list of alternatives with average efficiency scores (obtained for given sample).

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   <alternativesValues mcdaConcept="efficiency">
						<alternativeValue>
						  <alternativeID>[...]</alternativeID>
						  <value>
							[...]
						  </value>
						</alternativeValue>
						[...]
					</alternativesValues>


------------------------


.. _DEASMAACCREfficiencies-PUT-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/DEASMAACCREfficiencies-PUT/description-wsDD.xml>`
