.. _ElectreConcordance-PUT:

ElectreConcordance
==================

:Version: 0.2.0
:Provider: PUT
:SOAP service's name: ``ElectreConcordance-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Computes concordance matrix using procedure which is common to the most methods from the Electre family.

The key feature of this module is its flexibility in terms of the types of elements allowed to compare, i.e. alternatives vs alternatives, alternatives vs boundary profiles and alternatives vs central (characteristic) profiles.

**Web page:** http://github.com/xor-xor/electre_diviz


Inputs
------
(For outputs, see :ref:`below <ElectreConcordance-PUT_outputs>`)


- :ref:`alternatives <ElectreConcordance-PUT-input1>`
- :ref:`criteria <ElectreConcordance-PUT-input3>`
- :ref:`criteriaScales <ElectreConcordance-PUT-criteriaScales>`
- :ref:`criteriaThresholds <ElectreConcordance-PUT-criteriaThresholds>` *(optional)*
- :ref:`performance_table <ElectreConcordance-PUT-input4>`
- :ref:`profiles_performance_table <ElectreConcordance-PUT-input5>` *(optional)*
- :ref:`weights <ElectreConcordance-PUT-input6>`
- :ref:`classes_profiles <ElectreConcordance-PUT-input2>` *(optional)*
- :ref:`method_parameters <ElectreConcordance-PUT-input7>`


.. _ElectreConcordance-PUT-input1:

alternatives
~~~~~~~~~~~~

Alternatives to consider.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _ElectreConcordance-PUT-input3:

criteria
~~~~~~~~

Criteria to consider, possibly with preference and indifference thresholds. Each criterion must have a preference direction specified (min or max). It is worth mentioning that this module allows to define thresholds as constants as well as linear functions.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _ElectreConcordance-PUT-criteriaScales:

criteriaScales
~~~~~~~~~~~~~~

The scales of the criteria to consider. Each criterion must have a preference direction specified (min or max).

The input value should be a valid XMCDA document whose main tag is ``<criteriaScales>``.


------------------------


.. _ElectreConcordance-PUT-criteriaThresholds:

criteriaThresholds *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The criteria' preference and indifference thresholds. It is worth mentioning that this module allows to define thresholds as constants as well as linear functions.

The input value should be a valid XMCDA document whose main tag is ``<criteriaThresholds>``.


------------------------


.. _ElectreConcordance-PUT-input4:

performance_table
~~~~~~~~~~~~~~~~~

The performance of alternatives.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _ElectreConcordance-PUT-input5:

profiles_performance_table *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The performance of profiles (boundary or central).

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _ElectreConcordance-PUT-input6:

weights
~~~~~~~

Weights of criteria to consider.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _ElectreConcordance-PUT-input2:

classes_profiles *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Definitions of profiles (boundary or central) which should be used for classes (categories) representation.

The input value should be a valid XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _ElectreConcordance-PUT-input7:

method_parameters
~~~~~~~~~~~~~~~~~

This parameter specifies the type of elements provided for comparison.

Choosing 'boundary_profiles' or 'central_profiles' requires providing inputs 'classes_profiles' and 'profiles_performance_table' as well (which are optional by default).

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
<programParameters>
	<parameter name="comparison_with">
		<values>
			<value>
				<label>%1</label>
			</value>
		</values>
	</parameter>
</programParameters>


where:

- **%1** is a parameter named "comparison_with". It can have the following values:

  - ``alternatives``: alternatives vs alternatives

  - ``boundary_profiles``: alternatives vs boundary profiles

  - ``central_profiles``: alternatives vs central (characteristic) profiles

  The default value is item0.


------------------------



.. _ElectreConcordance-PUT_outputs:

Outputs
-------


- :ref:`concordance <ElectreConcordance-PUT-output1>`
- :ref:`messages <ElectreConcordance-PUT-output2>`


.. _ElectreConcordance-PUT-output1:

concordance
~~~~~~~~~~~

Concordance matrix computed from the given data. This matrix aggregates partial concordances from all criteria into single concordance index per pair of alternatives or alternatives/profiles.

The returned value is a XMCDA document whose main tag is ``<alternativesMatrix>``.


------------------------


.. _ElectreConcordance-PUT-output2:

messages
~~~~~~~~

Messages or errors generated by this module.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/ElectreConcordance-PUT/description-wsDD.xml>`
