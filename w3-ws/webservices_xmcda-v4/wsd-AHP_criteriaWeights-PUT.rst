.. _AHP_criteriaWeights-PUT:

AHP_criteriaWeights
===================

:Version: 1.1.0
:Provider: PUT
:SOAP service's name: ``AHP_criteriaWeights-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Computes criteria weights using AHP method.

**Contact:** Witold Kupś <witkups@gmail.com>

**Web page:** https://github.com/Azbesciak/DecisionDeck


Inputs
------
(For outputs, see :ref:`below <AHP_criteriaWeights-PUT_outputs>`)


- :ref:`criteria <AHP_criteriaWeights-PUT-input1>`
- :ref:`criteria_comparisons <AHP_criteriaWeights-PUT-input2>`


.. _AHP_criteriaWeights-PUT-input1:

criteria
~~~~~~~~

Criteria for comparison on all hierarchy level - must include root criterion also

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _AHP_criteriaWeights-PUT-input2:

criteria_comparisons
~~~~~~~~~~~~~~~~~~~~

Matrix of pairwise comparisons between criteria.
The rows and the columns are named according to the IDs of the criterion.
Each cell of the matrix contains an intensity of importance on the AHP scale, which is an integer.
The inverse intensity is calculated automatically.

The input value should be a valid XMCDA document whose main tag is ``<criteriaMatrix>``.


------------------------



.. _AHP_criteriaWeights-PUT_outputs:

Outputs
-------


- :ref:`weights <AHP_criteriaWeights-PUT-output1>`
- :ref:`messages <AHP_criteriaWeights-PUT-output2>`


.. _AHP_criteriaWeights-PUT-output1:

weights
~~~~~~~

Weights for criteria

The returned value is a XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _AHP_criteriaWeights-PUT-output2:

messages
~~~~~~~~

Messages or errors generated by this module

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/AHP_criteriaWeights-PUT/description-wsDD.xml>`
