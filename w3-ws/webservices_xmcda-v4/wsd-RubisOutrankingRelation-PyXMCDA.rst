.. _RubisOutrankingRelation-PyXMCDA:

RubisOutrankingRelation
=======================

:Version: 1.1
:Provider: PyXMCDA
:SOAP service's name: ``RubisOutrankingRelation-PyXMCDA`` (see :ref:`soap-requests` for details)

Description
-----------

This web service allows to compute an outranking relation as defined in the Rubis methodology.

**Contact:** Thomas Veneziano (thomas.veneziano@uni.lu)

**Reference:** R. Bisdorff, P. Meyer, M. Roubens, Rubis: a bipolar-valued outranking method for the best choice decision problem, 4OR, 6 (2), June 2008, Springer (doi:10.1007/s10288-007-0045-5).


Inputs
------
(For outputs, see :ref:`below <RubisOutrankingRelation-PyXMCDA_outputs>`)


- :ref:`criteria <RubisOutrankingRelation-PyXMCDA-criteria>`
- :ref:`alternatives <RubisOutrankingRelation-PyXMCDA-alternatives>`
- :ref:`performanceTable <RubisOutrankingRelation-PyXMCDA-performanceTable>`
- :ref:`criteriaWeights <RubisOutrankingRelation-PyXMCDA-criteriaWeights>`
- :ref:`valuationDomain <RubisOutrankingRelation-PyXMCDA-valuationDomain>` *(optional)*


.. _RubisOutrankingRelation-PyXMCDA-criteria:

criteria
~~~~~~~~

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.
Using thresholds is optional, only the constant ones with mcdaConcept equals to "indifference", "preference" or "veto" will be considered.
Preference directions can be specified with scales; if absent, they default to 'max'.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
<criteria>
	<criterion id="[...]">
		<active>[...]</active>
	</criterion>
	[...]
</criteria>

<criteriaScales>
	<criterionScale>
		<criterionID>...</criterionID>
		<scales>
			<scale>
				<quantitative>
					<preferenceDirection>min</preferenceDirection>
				</quantitative>
			</scale>
		</scales>
	</criterionScale>
</criteriaScales>

<criteriaThresholds>
	<criterionThreshold>
		<criterionID>PrixAchat</criterionID>
		<thresholds>
			<threshold mcdaConcept="weakVeto"> <!-- REQUIRED, must be indifference, preference, veto or weakVeto -->
				<constant>
					<real>7000.0</real>
				</constant>
			</threshold>
			<threshold mcdaConcept="veto">
				<constant>
					<real>10000.0</real>
				</constant>
			</threshold>
		</thresholds>
	</criterionThreshold>
</criteriaThresholds>



------------------------


.. _RubisOutrankingRelation-PyXMCDA-alternatives:

alternatives
~~~~~~~~~~~~

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
<alternatives>
	<alternative>
		<active>[...]</active>
	</alternative>
	[...]
</alternatives>



------------------------


.. _RubisOutrankingRelation-PyXMCDA-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A performance table. The evaluations should be numeric values, i.e. <real>, <integer> or <rational>.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _RubisOutrankingRelation-PyXMCDA-criteriaWeights:

criteriaWeights
~~~~~~~~~~~~~~~

The set of criteria weights.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _RubisOutrankingRelation-PyXMCDA-valuationDomain:

valuationDomain *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Indicates the minimal and the maximal values of the valuation domain for computing the outranking relation. By default the valuation domain is {0,0.5,1}. The median indetermination value is computed as the average of the minimal and the maximal values.

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
<programParameters name="valuationDomain"> <!-- name REQUIRED  -->
	<parameter name="min"> <!-- name REQUIRED  -->
		<values>
			<value>
				<real>%1</real>
			</value>
		</values>
	</parameter>
	<parameter name="max"> <!-- name REQUIRED  -->
		<values>
			<value>
				<real>%2</real>
			</value>
		</values>
	</parameter>
</programParameters>


where:

- **%1** is a parameter named "min". This is a float.
  The default value is 0.

- **%2** is a parameter named "max". This is a float.
  The default value is 1.


------------------------



.. _RubisOutrankingRelation-PyXMCDA_outputs:

Outputs
-------


- :ref:`alternativesComparisons <RubisOutrankingRelation-PyXMCDA-alternativesComparisons>`
- :ref:`messages <RubisOutrankingRelation-PyXMCDA-messages>`


.. _RubisOutrankingRelation-PyXMCDA-alternativesComparisons:

alternativesComparisons
~~~~~~~~~~~~~~~~~~~~~~~

The ogniknartu relation.

The returned value is a XMCDA document whose main tag is ``<alternativesMatrix>``.


------------------------


.. _RubisOutrankingRelation-PyXMCDA-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/RubisOutrankingRelation-PyXMCDA/description-wsDD.xml>`
