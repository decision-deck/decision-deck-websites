.. _plotAlternativesValuesPreorder-PyXMCDA:

plotAlternativesValuesPreorder
==============================

:Version: 1.1
:Provider: PyXMCDA
:SOAP service's name: ``plotAlternativesValuesPreorder-PyXMCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Generate directed graph from provided alternativesValues as well as the dot script generating the graph.

**Contact:** Nicolas Duminy (nicolas.duminy@telecom-bretagne.eu)

**Web page:** https://gitlab.com/nduminy/ws-pyxmcda


Inputs
------
(For outputs, see :ref:`below <plotAlternativesValuesPreorder-PyXMCDA_outputs>`)


- :ref:`alternatives <plotAlternativesValuesPreorder-PyXMCDA-alternatives>` *(optional)*
- :ref:`alternativesValues <plotAlternativesValuesPreorder-PyXMCDA-alternativesValues>`
- :ref:`parameters <plotAlternativesValuesPreorder-PyXMCDA-parameters>` *(optional)*


.. _plotAlternativesValuesPreorder-PyXMCDA-alternatives:

alternatives *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~

The alternatives to be plotted. All are plotted if not provided.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _plotAlternativesValuesPreorder-PyXMCDA-alternativesValues:

alternativesValues
~~~~~~~~~~~~~~~~~~

The alternatives values.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _plotAlternativesValuesPreorder-PyXMCDA-parameters:

parameters *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~

Parameters of the method

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
    <programParameters>
        <programParameter id="chart_title" name="Chart title">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="color" name="Color">
            <values>
                <value>
                    <label>%2</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="shape" name="Shape">
            <values>
                <value>
                    <label>%3</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="image_file_extension" name="Image file extension">
            <values>
                <value>
                    <label>%4</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="layout" name="Layout">
            <values>
                <value>
                    <label>%5</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="reverse_order" name="Reverse order">
            <values>
                <value>
                    <boolean>%6</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="naming_conventions" name="Naming conventions">
            <values>
                <value>
                    <label>%7</label>
                </value>
            </values>
        </programParameter>
    </programParameters>


where:

- **%1** is a parameter named "Chart title". This is a string.
  The default value is Alternatives Values preorder.

- **%2** is a parameter named "Color". It can have the following values:

  - ``black``: Black

  - ``white``: White

  - ``red``: Red

  - ``green``: Green

  - ``blue``: Blue

  - ``magenta``: Magenta

  - ``yellow``: Yellow

  - ``cyan``: Cyan

  - ``orange``: Orange

  - ``pink``: Pink

  - ``brown``: Brown

  - ``gray``: Gray

  The default value is black.

- **%3** is a parameter named "Shape". It can have the following values:

  - ``box``: Rectangle

  - ``circle``: Circle

  - ``diamond``: Diamond

  - ``ellipse``: Ellipse

  - ``oval``: Oval

  - ``polygon``: Polygon

  - ``triangle``: Triangle

  The default value is oval.

- **%4** is a parameter named "Image file extension". It can have the following values:

  - ``bmp``: .bmp (Windows Bitmap Format)

  - ``dia``: .dia (DIA Format)

  - ``fig``: .fig (FIG graphics format)

  - ``gif``: .gif (Graphics Interchange Format)

  - ``hpgl``: .hpgl (Hewlett Packard Graphic Language 2)

  - ``ico``: .ico (Icon Image File Format)

  - ``jpg``: .jpg (Joint Photographic Experts Group)

  - ``jpeg``: .jpeg (Joint Photographic Experts Group)

  - ``jpe``: .jpe (Joint Photographic Experts Group)

  - ``pdf``: .pdf (Portable Document Format)

  - ``png``: .png (Portable Network Graphics)

  - ``ps``: .ps (PostScript)

  - ``ps2``: .ps2 (PostScript for PDF)

  - ``svg``: .svg (Scalable Vector Graphics)

  - ``svgz``: .svgz (Compressed Scalable Vector Graphics)

  - ``tif``: .tif (Tagged Image File Format)

  - ``tiff``: .tiff (Tagged Image File Format)

  The default value is png.

- **%5** is a parameter named "Layout". It can have the following values:

  - ``line``: Line

  - ``column``: Column

  The default value is column.

- **%6** is a parameter named "Reverse order". This is a boolean.
  The default value is false.

- **%7** is a parameter named "Naming conventions". It can have the following values:

  - ``id``: Only ids are shown

  - ``name``: Only names are shown (disambiguated by appending the ids, if needed)

  - ``name (id)``: Names and ids are shown in that order

  - ``id (name)``: Ids and names are shown in that order

  The default value is name.


------------------------



.. _plotAlternativesValuesPreorder-PyXMCDA_outputs:

Outputs
-------


- :ref:`glob:alternativesValuesPreorder.{bmp,dia,fig,gif,hpgl,ico,jpg,jpeg,jpe,pdf,png,ps,ps2,svg,svgz,tif,tiff} <plotAlternativesValuesPreorder-PyXMCDA-alternativesValuesPreorderPlot>`
- :ref:`alternativesValuesPreorder.dot <plotAlternativesValuesPreorder-PyXMCDA-alternativesValuesPreorderPlotScript>`
- :ref:`messages <plotAlternativesValuesPreorder-PyXMCDA-messages>`


.. _plotAlternativesValuesPreorder-PyXMCDA-alternativesValuesPreorderPlot:

glob:alternativesValuesPreorder.{bmp,dia,fig,gif,hpgl,ico,jpg,jpeg,jpe,pdf,png,ps,ps2,svg,svgz,tif,tiff}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Image containing all selected alternatives values. Format corresponds to the one given in parameters (default is .png).

The returned value is a XMCDA document whose main tag is ``<None>``.


------------------------


.. _plotAlternativesValuesPreorder-PyXMCDA-alternativesValuesPreorderPlotScript:

alternativesValuesPreorder.dot
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Generated graphviz dot script that made the graph. Given to enable users to later customize the appearance of the plots.

The returned value is a XMCDA document whose main tag is ``<None>``.


------------------------


.. _plotAlternativesValuesPreorder-PyXMCDA-messages:

messages
~~~~~~~~

Status messages.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/plotAlternativesValuesPreorder-PyXMCDA/description-wsDD.xml>`
