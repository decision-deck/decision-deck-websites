.. _plotCriteriaValuesPreorder-RXMCDA:

plotCriteriaValuesPreorder
==========================

:Version: 1.0
:Provider: RXMCDA
:SOAP service's name: ``plotCriteriaValuesPreorder-RXMCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Generates a graph representing a preorder on the criteria, according to numerical values taken by the criteria.

Please note: this program is deprecated and it is replaced by plotCriteriaValuesPreorder in package ITTB.

**Contact:** Patrick Meyer (patrick.meyer@telecom-bretagne.eu)


Inputs
------
(For outputs, see :ref:`below <plotCriteriaValuesPreorder-RXMCDA_outputs>`)


- :ref:`criteria <plotCriteriaValuesPreorder-RXMCDA-criteria>`
- :ref:`criteriaRanks <plotCriteriaValuesPreorder-RXMCDA-criteriaRanks>`


.. _plotCriteriaValuesPreorder-RXMCDA-criteria:

criteria
~~~~~~~~

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
                
                    <criteria>
                        <criterion>
                            <active>[...]</active>
                            [...]
                        </criterion>
                        [...]
                    </criteria>
                    
            


------------------------


.. _plotCriteriaValuesPreorder-RXMCDA-criteriaRanks:

criteriaRanks
~~~~~~~~~~~~~

A list of <criterionValue> representing a value taken by each criterion.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.


------------------------



.. _plotCriteriaValuesPreorder-RXMCDA_outputs:

Outputs
-------


- :ref:`criteriaValuesPlot <plotCriteriaValuesPreorder-RXMCDA-criteriaValuesPlot-output>`
- :ref:`messages <plotCriteriaValuesPreorder-RXMCDA-messages>`


.. _plotCriteriaValuesPreorder-RXMCDA-criteriaValuesPlot-output:

criteriaValuesPlot
~~~~~~~~~~~~~~~~~~

A string containing the base64 representation of the png image of the graph generated by the R statistical software.

The returned value is a XMCDA document whose main tag is ``<criterionValue>``.


------------------------


.. _plotCriteriaValuesPreorder-RXMCDA-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/plotCriteriaValuesPreorder-RXMCDA/description-wsDD.xml>`
