.. _csvToXMCDA-alternativesValues-PyXMCDA:

csvToXMCDA-alternativesValues
=============================

:Version: 2.0
:Provider: PyXMCDA
:SOAP service's name: ``csvToXMCDA-alternativesValues-PyXMCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Transforms a file containing alternatives values from a comma-separated values (CSV) file to two XMCDA compliant files, containing the corresponding alternatives ids and their alternativesValues.

**Contact:** Sébastien Bigaret (sebastien.bigaret@telecom-bretagne.eu)

**Web page:** https://gitlab.com/sbigaret/ws-pyxmcda


Inputs
------
(For outputs, see :ref:`below <csvToXMCDA-alternativesValues-PyXMCDA_outputs>`)


- :ref:`alternativesValues.csv <csvToXMCDA-alternativesValues-PyXMCDA-csv>`
- :ref:`parameters <csvToXMCDA-alternativesValues-PyXMCDA-parameters>`


.. _csvToXMCDA-alternativesValues-PyXMCDA-csv:

alternativesValues.csv
~~~~~~~~~~~~~~~~~~~~~~

The alternatives and their alternatives values as a CSV file.  The first line is made of two cells, the first one being empty, and the second one will be the content of the attribute "mcdaConcept" in the tag "<alternativesValues>", if supplied.  The following lines are made of at least two cells, with the first cell being an alternative' id (and name, see below), and the remaining cells their associated values.

Example::

    ,ranks
    a1 (eggs),1,2,3,4,5
    a2 (spam),3.14159
    a3 (parrot),int:0xDEAD

The first column contains the alternatives' ids. Additionally, the alternatives' names are also extracted when the cells are formatted like `id (name)`.  Set the parameter "First column" to "id" to deactivate the extraction of alternatives' names.

By default the values are considered as float numbers.  This can be changed using the parameter "Default content".  It is possible to specify the type of a value by prepending it with a prefix:

- `float:` for floats (ex.: `1`, `1.2`, `1.2e3`)
- `integer:` for integers (decimal representation: `127`, hexadecimal: `0x7f`, octal: `0o177`, binary: `0b1111111`)
- `string:` for strings (note that a string with a colon should always be prefixed by `string:`, no matter what the default prefix is).
- `boolean:` for booleans: 1 or 'true' (case insensitive) are True values, everything else is false.
- `na:` for N/A (everything after the colon is ignored)

Example::

    ,values
    a1,float:1.0
    a2,integer:2
    a3,3.03
    a4,string:a label
    a4b,string:another label
    a5,na:content ignored for N/A
    a6,boolean:1

The input value should be a valid XMCDA document whose main tag is ``<other>``.


------------------------


.. _csvToXMCDA-alternativesValues-PyXMCDA-parameters:

parameters
~~~~~~~~~~

Parameters of the method

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
    <programParameters>
        <parameter id="csv_delimiter">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </parameter>
        <parameter id="default_prefix">
            <values>
                <value>
                    <label>%2</label>
                </value>
            </values>
        </parameter>
        <parameter id="name_in_id">
            <values>
                <value>
                    <boolean>%3</boolean>
                </value>
            </values>
        </parameter>
    </programParameters>


where:

- **%1** is a parameter named "CSV delimiter". This is a string, and the value should conform to the following constraint: One character maximum.  More formally, the constraint is::

     %1.length() < 2
- **%2** is a parameter named "Default content". It can have the following values:

  - ``float``: float

  - ``label``: string

  - ``integer``: integer

  - ``boolean``: boolean

  The default value is float.

- **%3** is a parameter named "First column". It can have the following values:

  - ``false``: id

  - ``true``: id (name)

  The default value is id_and_name.


------------------------



.. _csvToXMCDA-alternativesValues-PyXMCDA_outputs:

Outputs
-------


- :ref:`alternatives <csvToXMCDA-alternativesValues-PyXMCDA-alternatives>`
- :ref:`alternativesValues <csvToXMCDA-alternativesValues-PyXMCDA-alternativesValues>`
- :ref:`messages <csvToXMCDA-alternativesValues-PyXMCDA-messages>`


.. _csvToXMCDA-alternativesValues-PyXMCDA-alternatives:

alternatives
~~~~~~~~~~~~

The equivalent alternatives ids.

The returned value is a XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _csvToXMCDA-alternativesValues-PyXMCDA-alternativesValues:

alternativesValues
~~~~~~~~~~~~~~~~~~

The equivalent alternatives values.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _csvToXMCDA-alternativesValues-PyXMCDA-messages:

messages
~~~~~~~~

Status messages.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/csvToXMCDA-alternativesValues-PyXMCDA/description-wsDD.xml>`
