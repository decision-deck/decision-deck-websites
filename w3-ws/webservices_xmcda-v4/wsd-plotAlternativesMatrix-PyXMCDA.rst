:orphan:

.. _plotAlternativesMatrix-PyXMCDA:

plotAlternativesMatrix
======================

:Version: 1.1
:Provider: PyXMCDA
:SOAP service's name: ``plotAlternativesMatrix-PyXMCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Generate graph representing provided alternativesMatrix as well as the dot files generating this graph.
Values associated to transitions can be shown without any preprocessing.
Graphviz dot program and format are used by this service.

**Contact:** Nicolas Duminy (nicolas.duminy@telecom-bretagne.eu)

**Web page:** https://gitlab.com/nduminy/ws-pyxmcda


Inputs
------
(For outputs, see :ref:`below <plotAlternativesMatrix-PyXMCDA_outputs>`)


- :ref:`alternatives <plotAlternativesMatrix-PyXMCDA-alternatives>` *(optional)*
- :ref:`alternativesMatrix <plotAlternativesMatrix-PyXMCDA-alternativesMatrix>`
- :ref:`parameters <plotAlternativesMatrix-PyXMCDA-parameters>` *(optional)*


.. _plotAlternativesMatrix-PyXMCDA-alternatives:

alternatives *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~

The alternatives to be plotted. All are plotted if not provided.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _plotAlternativesMatrix-PyXMCDA-alternativesMatrix:

alternativesMatrix
~~~~~~~~~~~~~~~~~~

The alternatives matrix to be plotted.

The input value should be a valid XMCDA document whose main tag is ``<alternativesMatrix>``.


------------------------


.. _plotAlternativesMatrix-PyXMCDA-parameters:

parameters *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~

Parameters of the method

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
    <programParameters>
        <programParameter id="color" name="Color">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="shape" name="Shape">
            <values>
                <value>
                    <label>%2</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="image_file_extension" name="Image file extension">
            <values>
                <value>
                    <label>%3</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="show_values" name="Show values">
            <values>
                <value>
                    <boolean>%4</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="directed_graph" name="Directed graph">
            <values>
                <value>
                    <boolean>%7</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="transitive_reduction" name="Transitive reduction">
            <values>
                <value>
                    <boolean>%5</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="transitive_closure" name="Transitive closure">
            <values>
                <value>
                    <boolean>%6</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="naming_conventions" name="Naming conventions">
            <values>
                <value>
                    <label>%8</label>
                </value>
            </values>
        </programParameter>
    </programParameters>


where:

- **%1** is a parameter named "Color". It can have the following values:

  - ``black``: Black

  - ``white``: White

  - ``red``: Red

  - ``green``: Green

  - ``blue``: Blue

  - ``magenta``: Magenta

  - ``yellow``: Yellow

  - ``cyan``: Cyan

  - ``orange``: Orange

  - ``pink``: Pink

  - ``brown``: Brown

  - ``gray``: Gray

  The default value is black.

- **%2** is a parameter named "Shape". It can have the following values:

  - ``box``: Rectangle

  - ``circle``: Circle

  - ``diamond``: Diamond

  - ``ellipse``: Ellipse

  - ``oval``: Oval

  - ``polygon``: Polygon

  - ``triangle``: Triangle

  The default value is oval.

- **%3** is a parameter named "Image file extension". It can have the following values:

  - ``bmp``: .bmp (Windows Bitmap Format)

  - ``dia``: .dia (DIA Format)

  - ``fig``: .fig (FIG graphics format)

  - ``gif``: .gif (Graphics Interchange Format)

  - ``hpgl``: .hpgl (Hewlett Packard Graphic Language 2)

  - ``ico``: .ico (Icon Image File Format)

  - ``jpg``: .jpg (Joint Photographic Experts Group)

  - ``jpe``: .jpe (Joint Photographic Experts Group)

  - ``pdf``: .pdf (Portable Document Format)

  - ``png``: .png (Portable Network Graphics)

  - ``ps``: .ps (PostScript)

  - ``ps2``: .ps2 (PostScript for PDF)

  - ``svg``: .svg (Scalable Vector Graphics)

  - ``svgz``: .svgz (Compressed Scalable Vector Graphics)

  - ``tif``: .tif (Tagged Image File Format)

  The default value is png.

- **%4** is a parameter named "Show values". This is a boolean.
  The default value is false.

- **%7** is a parameter named "Directed graph". It can have the following values:

  - ``true``: Directed

  - ``false``: Undirected

  The default value is true.

- **%5** is a parameter named "Transitive reduction". This is a boolean.
  The default value is false.

- **%6** is a parameter named "Transitive closure". This is a boolean.
  The default value is false.

- **%8** is a parameter named "Naming conventions". It can have the following values:

  - ``id``: Only ids are shown

  - ``name``: Only names are shown (disambiguated by appending the ids, if needed)

  - ``name (id)``: Names and ids are shown in that order

  - ``id (name)``: Ids and names are shown in that order

  The default value is name.


------------------------



.. _plotAlternativesMatrix-PyXMCDA_outputs:

Outputs
-------


- :ref:`glob:alternativesMatrix.{bmp,dia,fig,gif,hpgl,ico,jpg,jpe,pdf,png,ps,ps2,svg,svgz,tif} <plotAlternativesMatrix-PyXMCDA-alternativesMatrixPlot>`
- :ref:`alternativesMatrix.dot <plotAlternativesMatrix-PyXMCDA-alternativesMatrixPlotScript>`
- :ref:`messages <plotAlternativesMatrix-PyXMCDA-messages>`


.. _plotAlternativesMatrix-PyXMCDA-alternativesMatrixPlot:

glob:alternativesMatrix.{bmp,dia,fig,gif,hpgl,ico,jpg,jpe,pdf,png,ps,ps2,svg,svgz,tif}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Image containing alternatives matrix graph. Format corresponds to the one given in parameters (default is .png).

The returned value is a XMCDA document whose main tag is ``<None>``.


------------------------


.. _plotAlternativesMatrix-PyXMCDA-alternativesMatrixPlotScript:

alternativesMatrix.dot
~~~~~~~~~~~~~~~~~~~~~~

Generated graphviz dot script that made the image. Given to enable users to later customize the appearance of the graph.

The returned value is a XMCDA document whose main tag is ``<None>``.


------------------------


.. _plotAlternativesMatrix-PyXMCDA-messages:

messages
~~~~~~~~

Status messages.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/plotAlternativesMatrix-PyXMCDA/description-wsDD.xml>`
