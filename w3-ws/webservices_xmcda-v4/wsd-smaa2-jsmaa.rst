.. _smaa2-jsmaa:

smaa2
=====

:Version: 0.2
:Provider: jsmaa
:SOAP service's name: ``smaa2-jsmaa`` (see :ref:`soap-requests` for details)

Description
-----------

JSMAA adapter for DD. Currently implements only SMAA-2 without preference information and only cardinal interval measurements of reals.

**Contact:** Tommi Tervonen (t.p.tervonen@rug.nl)

**Web page:** http://www.smaa.fi


Inputs
------
(For outputs, see :ref:`below <smaa2-jsmaa_outputs>`)


- :ref:`criteria <smaa2-jsmaa-criteria>`
- :ref:`alternatives <smaa2-jsmaa-alternatives>`
- :ref:`performanceTable <smaa2-jsmaa-performanceTable>`


.. _smaa2-jsmaa-criteria:

criteria
~~~~~~~~

A list of criteria.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
               
                   <criteria>
                       <criterion>
                           [...]
                       </criterion>
                       [...]
                   </criteria>
                   
           


------------------------


.. _smaa2-jsmaa-alternatives:

alternatives
~~~~~~~~~~~~

A list of alternatives.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
               
                   <alternatives>
                       <alternative>
                           [...]
                       </alternative>
                       [...]
                   </alternatives>
                   
           


------------------------


.. _smaa2-jsmaa-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A performance table. The evaluations should be numeric values, i.e. <real>, <integer> or <rational>.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------



.. _smaa2-jsmaa_outputs:

Outputs
-------


- :ref:`confidenceFactors <smaa2-jsmaa-confidenceFactors>`
- :ref:`rankAcceptabilities <smaa2-jsmaa-rankAcceptabilities>`
- :ref:`centralWeights <smaa2-jsmaa-centralWeights>`
- :ref:`messages <smaa2-jsmaa-messages>`


.. _smaa2-jsmaa-confidenceFactors:

confidenceFactors
~~~~~~~~~~~~~~~~~

Confidence factors of SMAA-2 computation.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _smaa2-jsmaa-rankAcceptabilities:

rankAcceptabilities
~~~~~~~~~~~~~~~~~~~

Rank acceptabilities of SMAA-2 computation.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _smaa2-jsmaa-centralWeights:

centralWeights
~~~~~~~~~~~~~~

Central weights of SMAA-2 computation.

The returned value is a XMCDA document whose main tag is ``<other>``.


------------------------


.. _smaa2-jsmaa-messages:

messages
~~~~~~~~

Output of messages. Currently always outputs execution succesful :)

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/smaa2-jsmaa/description-wsDD.xml>`
