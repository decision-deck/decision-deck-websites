.. _ImpreciseDEA-CCR-SMAA_efficiencies-PUT:

ImpreciseDEA-CCR-SMAA_efficiencies
==================================

:Version: 1.0
:Provider: PUT
:SOAP service's name: ``ImpreciseDEA-CCR-SMAA_efficiencies-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Computes efficiency scores for the given DMUs (alternatives) using SMAA-D method and CCR Data Envelopment Analysis Model with imprecise data. For given number of buckets and samples, returns a matrix with alternatives in each row and buckets representing efficiency intervals in each column. Single cell indicates how many samples gave efficiency scores of respective alternative in respective bucket.

**Contact:** Anna Labijak <support@decision-deck.org>


Inputs
------
(For outputs, see :ref:`below <ImpreciseDEA-CCR-SMAA_efficiencies-PUT_outputs>`)


- :ref:`units <ImpreciseDEA-CCR-SMAA_efficiencies-PUT-units>`
- :ref:`inputsOutputs <ImpreciseDEA-CCR-SMAA_efficiencies-PUT-inputsOutputs>`
- :ref:`inputsOutputsScales <ImpreciseDEA-CCR-SMAA_efficiencies-PUT-inputsOutputsScales>`
- :ref:`performanceTable <ImpreciseDEA-CCR-SMAA_efficiencies-PUT-performanceTable>`
- :ref:`maxPerformanceTable <ImpreciseDEA-CCR-SMAA_efficiencies-PUT-maxPerformanceTable>` *(optional)*
- :ref:`weightsLinearConstraints <ImpreciseDEA-CCR-SMAA_efficiencies-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <ImpreciseDEA-CCR-SMAA_efficiencies-PUT-methodParameters>`


.. _ImpreciseDEA-CCR-SMAA_efficiencies-PUT-units:

units
~~~~~

A list of alternatives (DMUs).

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>


------------------------


.. _ImpreciseDEA-CCR-SMAA_efficiencies-PUT-inputsOutputs:

inputsOutputs
~~~~~~~~~~~~~

A list of criteria.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   <criteria>
                        <criterion>[...]</criterion>
                        [...]
                    </criteria>


------------------------


.. _ImpreciseDEA-CCR-SMAA_efficiencies-PUT-inputsOutputsScales:

inputsOutputsScales
~~~~~~~~~~~~~~~~~~~

Informations about inputs and outputs scales and optionally about boundaries

The input value should be a valid XMCDA document whose main tag is ``<criteriaScales>``.
It must have the following form::

   
<criteriaScales>
    <criterionScale>
      <criterionID>[...]</criterionID>
      <scales>
        <scale>
          [...]
        </scale>
      </scales>
    </criterionScale>
    [...]
</criteriaScales>


------------------------


.. _ImpreciseDEA-CCR-SMAA_efficiencies-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A list of alternatives (DMUs) minimal performances (or exact performances if intervals should be created by tolerance).

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<values><value>[...]</value></values>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>


------------------------


.. _ImpreciseDEA-CCR-SMAA_efficiencies-PUT-maxPerformanceTable:

maxPerformanceTable *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of alternatives (DMUs) maximal performances.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<values><value>[...]</value></values>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>


------------------------


.. _ImpreciseDEA-CCR-SMAA_efficiencies-PUT-weightsLinearConstraints:

weightsLinearConstraints *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of criteria linear constraints.

The input value should be a valid XMCDA document whose main tag is ``<criteriaLinearConstraints>``.
It must have the following form::

   
            <criteriaLinearConstraints>
                <constraints>
                    <constraint>
                        <elements>
                            <element>
                                <criterionID> [...] </criterionID>
                                <coefficient>
                                    [...]
                                </coefficient>
                            </element>
                            [...]
                        </elements>
                        <operator>[...]</operator>
                        <rhs>
                            [...]
                        </rhs>
                    </constraint>
                    [...]
                </constraints>
            </criteriaLinearConstraints>


------------------------


.. _ImpreciseDEA-CCR-SMAA_efficiencies-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~

"samplesNb" represents the number of samples to generate; "intervalsNo" represents the number of buckets which efficiency scores will be assigned to.

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
    <programParameters>
        <parameter id="samplesNb">
            <values>
                <value><integer>%1</integer></value>
            </values>
        </parameter>
        <parameter id="intervalsNb">
            <values>
                <value><integer>%2</integer></value>
            </values>
        </parameter>
        <parameter id="tolerance">
            <values>
                <value><real>%3</real></value>
            </values>
        </parameter>
    </programParameters>

where:

- **%1** is a parameter named "number of samples". This is a int, and the value should conform to the following constraint: The value should be a positive integer..  More formally, the constraint is::

     %1 > 0
  The default value is 100.

- **%2** is a parameter named "number of buckets". This is a int, and the value should conform to the following constraint: The value should be a positive integer..  More formally, the constraint is::

     %2 > 0
  The default value is 10.

- **%3** is a parameter named "tolerance". This is a float, and the value should conform to the following constraint: The value should be a non-negative number..  More formally, the constraint is::

     %3 >= 0
  The default value is 0.0.


------------------------



.. _ImpreciseDEA-CCR-SMAA_efficiencies-PUT_outputs:

Outputs
-------


- :ref:`efficiencyDistribution <ImpreciseDEA-CCR-SMAA_efficiencies-PUT-efficiencyDistribution>`
- :ref:`maxEfficiency <ImpreciseDEA-CCR-SMAA_efficiencies-PUT-maxEfficiency>`
- :ref:`minEfficiency <ImpreciseDEA-CCR-SMAA_efficiencies-PUT-minEfficiency>`
- :ref:`avgEfficiency <ImpreciseDEA-CCR-SMAA_efficiencies-PUT-avgEfficiency>`
- :ref:`messages <ImpreciseDEA-CCR-SMAA_efficiencies-PUT-messages>`


.. _ImpreciseDEA-CCR-SMAA_efficiencies-PUT-efficiencyDistribution:

efficiencyDistribution
~~~~~~~~~~~~~~~~~~~~~~

A performance table for given alternatives. Single performance consists of attribute criterionID representing certain bucket, and a value representing the ratio of efficiency scores in this bucket.

The returned value is a XMCDA document whose main tag is ``<performanceTable>``.
It has the following form::

   <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>[...]</criterionID>
									<values>
                                        <value>[...]</value>
                                    </values>
							</performance>
							[...]
						</alternativePerformances>
                        [...]
					</performanceTable>


------------------------


.. _ImpreciseDEA-CCR-SMAA_efficiencies-PUT-maxEfficiency:

maxEfficiency
~~~~~~~~~~~~~

A list of alternatives with maximum efficiency scores (obtained for given sample).

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
                          <values>
                                <value>
                                    [...]
                                </value>
                          </values>
						</alternativeValue>
						[...]
					</alternativesValues>


------------------------


.. _ImpreciseDEA-CCR-SMAA_efficiencies-PUT-minEfficiency:

minEfficiency
~~~~~~~~~~~~~

A list of alternatives with computed minimum efficiency scores (obtained for given sample).

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
                          <values>
                                <value>
                                    [...]
                                </value>
                          </values>
						</alternativeValue>
						[...]
					</alternativesValues>


------------------------


.. _ImpreciseDEA-CCR-SMAA_efficiencies-PUT-avgEfficiency:

avgEfficiency
~~~~~~~~~~~~~

A list of alternatives with average efficiency scores (obtained for given sample).

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   <alternativesValues>
						<alternativeValue>
						  <alternativeID> [...] </alternativeID>
                          <values>
                                <value>
                                    [...]
                                </value>
                          </values>
						</alternativeValue>
						[...]
					</alternativesValues>


------------------------


.. _ImpreciseDEA-CCR-SMAA_efficiencies-PUT-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/ImpreciseDEA-CCR-SMAA_efficiencies-PUT/description-wsDD.xml>`
