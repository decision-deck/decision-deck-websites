.. _plotPerformanceTable_starGraph-ITTB:

plotStarGraphPerformanceTable
=============================

:Version: 2.0
:Provider: ITTB
:Name: plotPerformanceTable_starGraph
:SOAP service's name: ``plotPerformanceTable_starGraph-ITTB`` (see :ref:`soap-requests` for details)

Description
-----------

This web service generates, for each alternative, a plot representing the performance table as a  star graph. Colors can be used. You can specify how to display the star graphs: by line, by column or in a grid. The star graphs can also be ordered by name or by id (of the alternatives).

**Contact:** Dhouha Kbaier (dhouha.kbaier@telecom-bretagne.eu)


Inputs
------
(For outputs, see :ref:`below <plotPerformanceTable_starGraph-ITTB_outputs>`)


- :ref:`alternatives <plotPerformanceTable_starGraph-ITTB-alternatives>`
- :ref:`criteria <plotPerformanceTable_starGraph-ITTB-criteria>`
- :ref:`criteriaScales <plotPerformanceTable_starGraph-ITTB-criteriaScales>` *(optional)*
- :ref:`performanceTable <plotPerformanceTable_starGraph-ITTB-performanceTable>`
- :ref:`parameters <plotPerformanceTable_starGraph-ITTB-parameters>`


.. _plotPerformanceTable_starGraph-ITTB-alternatives:

alternatives
~~~~~~~~~~~~

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
	<alternatives>
		<alternative>
			<active>[...]</active>
			[...]
		</alternative>
		[...]
	</alternatives>



------------------------


.. _plotPerformanceTable_starGraph-ITTB-criteria:

criteria
~~~~~~~~

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
	<criteria>
		<criterion>
			<active>[...]</active>
		</criterion>
		[...]
	</criteria>



------------------------


.. _plotPerformanceTable_starGraph-ITTB-criteriaScales:

criteriaScales *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Criteria scales. Preference direction for the selected criteria can be provided (min or max). In this web service, the default value is set to max.

The input value should be a valid XMCDA document whose main tag is ``<criteriaScales>``.
It must have the following form::

   
        <criteriaScales>
                <criterionScale>
                        <criterionID>...</criterionID>
                        <scales>
                                <scale>
                                        <quantitative>
                                                <preferenceDirection>min</preferenceDirection>
                                        </quantitative>
                                </scale>
                        </scales>
                </criterionScale>
		[...]
	</criteriaScales>



------------------------


.. _plotPerformanceTable_starGraph-ITTB-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A performance table. The evaluations should be only real or integer numeric values, i.e. <real> or <integer>.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _plotPerformanceTable_starGraph-ITTB-parameters:

parameters
~~~~~~~~~~

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
	<programParameters>
		<parameter id="preference_direction" name="Use preference direction">
			<values>
				<value>
					<label>%1</label>
				</value>
			</values>
		</parameter>
		<parameter id="unique_plot" name="Unique plot">
			<values>
				<value>
					<label>%2</label>
				</value>
			</values>
		</parameter>
		<parameter id="plots_display" name="Plots' display">
			<values>
				<value>
					<label>%3</label>
				</value>
			</values>
		</parameter>
		<parameter id="order_by" name="Order by">
			<values>
				<value>
					<label>%4</label>
				</value>
			</values>
		</parameter>
		<parameter id="order" name="order">
			<values>
				<value>
					<label>%5</label>
				</value>
			</values>
		</parameter>
		<parameter id="use_color" name="Colors in the plots">
			<values>
				<value>
					<label>%6</label>
				</value>
			</values>
		</parameter>
		<parameter id="selected_color" name="Selected color">
			<values>
				<value>
					<label>%7</label>
				</value>
			</values>
		</parameter>
	</programParameters>


where:

- **%1** is a parameter named "Use preference directions?". It can have the following values:

  - ``true``: Yes

  - ``false``: No

  The default value is false.

- **%2** is a parameter named "Unique or multiple plot(s)?". It can have the following values:

  - ``true``: Unique

  - ``false``: Multiple

  The default value is true.

- **%3** is a parameter named "Plots arrangement". It can have the following values:

  - ``column``: Column

  - ``line``: Line

  - ``grid``: Grid

  The default value is column.

- **%4** is a parameter named "Order abscissa by:". It can have the following values:

  - ``name``: name

  - ``id``: id

  The default value is order_by_id.

- **%5** is a parameter named "order". It can have the following values:

  - ``increasing``: Ascending order

  - ``decreasing``: Descending order

  The default value is increasing.

- **%6** is a parameter named "Use Colors?". It can have the following values:

  - ``true``: Yes

  - ``false``: No

  The default value is false.

- **%7** is a parameter named "Choose color:". It can have the following values:

  - ``black``: Black

  - ``red``: Red

  - ``blue``: Blue

  - ``green``: Green

  - ``yellow``: Yellow

  - ``magenta``: Magenta

  - ``cyan``: Cyan

  The default value is black.


------------------------



.. _plotPerformanceTable_starGraph-ITTB_outputs:

Outputs
-------


- :ref:`starGraph.png <plotPerformanceTable_starGraph-ITTB-png>`
- :ref:`messages <plotPerformanceTable_starGraph-ITTB-messages>`


.. _plotPerformanceTable_starGraph-ITTB-png:

starGraph.png
~~~~~~~~~~~~~

The star graph representing the performance table, as a PNG image.

The returned value is a XMCDA document whose main tag is ``<alternativeValue>``.


------------------------


.. _plotPerformanceTable_starGraph-ITTB-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/plotPerformanceTable_starGraph-ITTB/description-wsDD.xml>`
