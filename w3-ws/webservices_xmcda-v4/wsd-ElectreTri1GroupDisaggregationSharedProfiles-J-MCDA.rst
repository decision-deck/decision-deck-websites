.. _ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA:

ElectreTri1GroupDisaggregationSharedProfiles
============================================

:Version: 0.5.3
:Provider: J-MCDA
:SOAP service's name: ``ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Finds electre tri like models with shared profiles and individual weights that matches given group assignments.

**Contact:** Olivier Cailloux <olivier.cailloux@ecp.fr>

**Web page:** http://sourceforge.net/projects/j-mcda/

**Reference:** Cailloux, O., Meyer, P. & Mousseau, V., 2012. Eliciting ELECTRE TRI category limits for a group of decision makers. European Journal of Operational Research. Available at: http://www.sciencedirect.com/science/article/pii/S0377221712003906?v=s5.


Inputs
------
(For outputs, see :ref:`below <ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA_outputs>`)


- :ref:`criteria <ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-criteria>`
- :ref:`alternatives <ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-alternatives>` *(optional)*
- :ref:`performances_alternatives <ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-performances_alternatives>`
- :ref:`set_profiles <ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-set_profiles>` *(optional)*
- :ref:`examples <ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-examples>`
- :ref:`categories_profiles <ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-categories_profiles>`
- :ref:`use_vetoes <ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-use_vetoes>` *(optional)*
- :ref:`min_weights <ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-min_weights>` *(optional)*


.. _ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-criteria:

criteria
~~~~~~~~

The criteria to consider. Each one must have a preference direction. No preference or indifference thresholds may be included. Set some criteria as inactive (or remove them) to ignore them. Veto thresholds may be included: these will be considered as constraints on the preference model.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-alternatives:

alternatives *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~

The alternatives to consider. Set some alternatives as inactive (or remove them) to ignore them.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-performances_alternatives:

performances_alternatives
~~~~~~~~~~~~~~~~~~~~~~~~~

The performances of the example alternatives.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-set_profiles:

set_profiles *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~

Some performances for the profiles of the resulting model may be constrained here. The values will not necessarily be considered as is but the resulting model will satisfy the ordering implied by the constraints.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-examples:

examples
~~~~~~~~

The example assignments. Use one tag per decision maker. Use the name attribute to indicate the corresponding decision maker.

The input value should be a valid XMCDA document whose main tag is ``<alternativesAffectations>``.


------------------------


.. _ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-categories_profiles:

categories_profiles
~~~~~~~~~~~~~~~~~~~

The profiles and the categories names in which alternatives are to be sorted.

The input value should be a valid XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-use_vetoes:

use_vetoes *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~

Whether to accept using veto thresholds in the resulting preference model. Must be true when veto threshold values are set as input.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   

      <methodParameters>
         <parameter>
               <value>
                  <boolean>%1</boolean>
               </value>
         </parameter>
      </methodParameters>

			

where:

- **%1** is a parameter named "use vetoes". This is a boolean.

------------------------


.. _ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-min_weights:

min_weights *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~

The minimum value accepted for a weight. Default is zero.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   

      <methodParameters>
         <parameter>
               <value>
                  <real>%1</real>
               </value>
         </parameter>
      </methodParameters>

			

where:

- **%1** is a parameter named "minimum weights". This is a float.

------------------------



.. _ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA_outputs:

Outputs
-------


- :ref:`veto_thresholds <ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-veto_thresholds>`
- :ref:`coalitions <ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-coalitions>`
- :ref:`performances_profiles <ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-performances_profiles>`
- :ref:`messages <ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-messages>`
- :ref:`result_status <ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-result_status>`


.. _ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-veto_thresholds:

veto_thresholds
~~~~~~~~~~~~~~~

The resulting veto thresholds.

The returned value is a XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-coalitions:

coalitions
~~~~~~~~~~

The resulting group coalitions.

The returned value is a XMCDA document whose main tag is ``<criteriaSets>``.


------------------------


.. _ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-performances_profiles:

performances_profiles
~~~~~~~~~~~~~~~~~~~~~

The performances of the shared profiles.

The returned value is a XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-messages:

messages
~~~~~~~~

A status message to indicate if everything went correctly or if an error happened during the execution. Note that if the service finds that the problem is infeasible, this is not considered as an error.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------


.. _ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA-result_status:

result_status
~~~~~~~~~~~~~

The result status: if feasible, then one solution was found, otherwise the status indicates what happened (e.g. the program is infeasible).

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA/description-wsDD.xml>`
