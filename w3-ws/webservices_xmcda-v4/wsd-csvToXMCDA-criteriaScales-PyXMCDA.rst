:orphan:

.. _csvToXMCDA-criteriaScales-PyXMCDA:

csvToXMCDA-criteriaScales
=========================

:Version: 1.0
:Provider: PyXMCDA
:SOAP service's name: ``csvToXMCDA-criteriaScales-PyXMCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Transforms a file containing criteria scales from a comma-separated values (CSV) file to two XMCDA compliant files, containing the corresponding criteria ids and their criteriaScales.

**Contact:** Nicolas Duminy (nicolas.duminy@telecom-bretagne.eu)

**Web page:** https://gitlab.com/nduminy/ws-pyxmcda


Inputs
------
(For outputs, see :ref:`below <csvToXMCDA-criteriaScales-PyXMCDA_outputs>`)


- :ref:`criteriaScales.csv <csvToXMCDA-criteriaScales-PyXMCDA-csv>`
- :ref:`parameters <csvToXMCDA-criteriaScales-PyXMCDA-parameters>`


.. _csvToXMCDA-criteriaScales-PyXMCDA-csv:

criteriaScales.csv
~~~~~~~~~~~~~~~~~~

The criteria and their criteria scales defined, as a CSV file.
The first line contains the columns titles and is purely informative
(i.e criterion,scales id,pref dir,scale label,scale def).
Then for the following lines, the first cell contains a criterion id (and name, see below),
the second cell contains the associated scale id, the following cells are either
empty if the scale was already defined before or the scale definition (see below).
The scale definition can extend on multiple rows if necessary, those extra rows
have their first two cells empty.

Example::

    criterion, scales id, pref dir, scale label, scale definition
    c01 (risk),regular qualitative scale,min,Critical,3
    ,,,Serious,2
    ,,,Minor,1
    c02 (issue),regular qualitative scale,,,

The first column contains the criteria' ids. Additionally, the criteria' names are also extracted when the cells are formatted like `id (name)`.  Set the parameter "First column" to "id" to deactivate the extraction of criteria' names.

The third column contains the preference direction of the scale (if available):
    - `min` for a minimal preference direction
    - `max` for a maximal preference direction

The fourth column contains the scale labels. Each row can only contains one label, so additional labels
are given by extending the scale definition on extra rows.

The fifth column contains the additional scale definition, its formatting depends on the type of the scale being defined.

Three types of scales can be defined: nominative, quantitative, qualitative.

A nominal scale is defined only by its list of labels, which are given in the relevant fourth column.
The listing of labels can either begin on a new row or on the row where the scale id is present.

Example::

    c01,nominal scale,,Poor,
    ,,,Standard,
    ,,,Top,
    c02,nominal scale 2,,,
    ,,,Poor,
    ,,,Standard,
    ,,,Top,

A quantitative scale is defined by its preference direction (in column three) and its boundaries (defined in column five).
Those boundaries are a pair of numeric values, lower first, in the following format: (`lower_limit`, `upper_limit`)
Those boundaries can also be contained in double quotes `"`.
The whole definition of a quantitative scale is on one row and cannot be extended.

Example::

    c01,quantitative scale,max,,"(0, 1)"
    c02,quantitative scale 2,max,,(0, 1)

A qualitative scale is defined by its preference direction (column three) and its list of valued labels (columns four and five).
As for the nominative scale, the listing of valued labels can either begin on a new row or on the row where the scale id is present.
The labels (column four) are associated to their value (column five).
Those values could either be a ranking, defined by single numeric values, or fuzzy numbers for a fuzzy scale,
defined by a list of points (between 2 and 4). Each point formatted (`abscissa`, `ordinate`) and separated from the others by ` - `.
Each fuzzy number can also be contained in double quotes `"`.
Each scale cannot mix up those types of values.

Example::

    c01,fuzzy qualitative scale, max,,
    ,,,Very Low,"(0, 0) - (0.2, 1) - (0.5, 0)"
    ,,,Low,(0.0, 0) - (0.2, 1) - (0.5, 0)
    ,,,Medium,"(0.2, 0) - (0.5, 1) - (0.6, 0)"
    ,,,Almost High,"(0.5, 0) - (0.6, 1) - (0.7, 0)"
    ,,,High,"(0.6, 0) - (0.7, 1) - (0.8, 1) - (0.9, 0)"
    ,,,Very High,"(0.8, 0) - (0.9, 1) - (1.0, 0)"
    ,,,Perfect,"(0.9, 0) - (1.0, 1)"
    c02,ranked qualitative scale,min,Deadly,3
    ,,,Serious,2
    ,,,Minor,1

By default the values in column five are considered as float numbers.  This can be changed using the parameter "Default content".  It is possible to specify the type of a value by prepending it with a prefix:
    - `float:` or `f:` for floats (ex.: `1`, `1.2`, `1.2e3`)
    - `integer:` or `i:` for integers (decimal representation: `127`, hexadecimal: `0x7f`, octal: `0o177`, binary: `0b1111111`)

Complete example::

    criterion, scales id, pref dir, scale label, scale definition
    c01,fuzzy scales, max,,
    ,,,Very Low,"(0, 0) - (0.2, 1) - (0.5, 0)"
    ,,,Low,"(0.0, 0) - (0.2, 1) - (0.5, 0)"
    ,,,Medium,"(0.2, 0) - (0.5, 1) - (0.6, 0)"
    ,,,Almost High,"(0.5, 0) - (0.6, 1) - (0.7, 0)"
    ,,,High,"(0.6, 0) - (0.7, 1) - (0.8, 1) - (0.9, 0)"
    ,,,Very High,"(0.8, 0) - (0.9, 1) - (1.0, 0)"
    ,,,Perfect,"(0.9, float:0) - (1.0, 1)"
    c02,fuzzy scales,,,
    c03,nominal scales,,,
    ,,,Poor,
    ,,,Standard,
    ,,,Top,
    c04,nominal scales,,,
    c05,quantitative scales,max,,"(0, 1)"
    c06,regular qualitative scale,min,Deadly,3
    ,,,Serious,integer:2
    ,,,Minor,1

The input value should be a valid XMCDA document whose main tag is ``<other>``.


------------------------


.. _csvToXMCDA-criteriaScales-PyXMCDA-parameters:

parameters
~~~~~~~~~~

Parameters of the method

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
    <programParameters>
        <programParameter id="csv_delimiter">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="default_prefix">
            <values>
                <value>
                    <label>%2</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="name_in_id">
            <values>
                <value>
                    <boolean>%3</boolean>
                </value>
            </values>
        </programParameter>
    </programParameters>


where:

- **%1** is a parameter named "CSV delimiter". This is a string, and the value should conform to the following constraint: One character maximum.  More formally, the constraint is::

     %1.length() < 2
- **%2** is a parameter named "Default content". It can have the following values:

  - ``float``: float

  - ``label``: string

  - ``integer``: integer

  - ``boolean``: boolean

  The default value is float.

- **%3** is a parameter named "First column". It can have the following values:

  - ``false``: id

  - ``true``: id (name)

  The default value is id_and_name.


------------------------



.. _csvToXMCDA-criteriaScales-PyXMCDA_outputs:

Outputs
-------


- :ref:`criteria <csvToXMCDA-criteriaScales-PyXMCDA-criteria>`
- :ref:`criteriaScales <csvToXMCDA-criteriaScales-PyXMCDA-criteriaScales>`
- :ref:`messages <csvToXMCDA-criteriaScales-PyXMCDA-messages>`


.. _csvToXMCDA-criteriaScales-PyXMCDA-criteria:

criteria
~~~~~~~~

The equivalent criteria ids.

The returned value is a XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _csvToXMCDA-criteriaScales-PyXMCDA-criteriaScales:

criteriaScales
~~~~~~~~~~~~~~

The equivalent criteria scales.

The returned value is a XMCDA document whose main tag is ``<criteriaScales>``.


------------------------


.. _csvToXMCDA-criteriaScales-PyXMCDA-messages:

messages
~~~~~~~~

Status messages.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/csvToXMCDA-criteriaScales-PyXMCDA/description-wsDD.xml>`
