.. _MRSort-LargePerfDiff_inference_exact-R-MCDA:

Exact inference of MRSort with large performance differences
============================================================

:Version: 1.0
:Provider: R-MCDA
:Name: MRSort-LargePerfDiff_inference_exact
:SOAP service's name: ``MRSort-LargePerfDiff_inference_exact-R-MCDA`` (see :ref:`soap-requests` for details)

Description
-----------

MRSort is a simplified ELECTRE TRI sorting method, where alternatives are assigned to an ordered set of categories. In this case, we also take into account large performance differences, both negative (vetoes) and positive (dictators). The identification of the profiles, weights and majority threshold are done by taking into account assignment examples.

**Contact:** Alexandru Olteanu (alexandru.olteanu@univ-ubs.fr)

**Reference:** P. MEYER, A-L. OLTEANU, Integrating large positive and negative performance differences into multicriteria majority-rule sorting models, Computers and Operations Research, 81, pp. 216 - 230, 2017.


Inputs
------
(For outputs, see :ref:`below <MRSort-LargePerfDiff_inference_exact-R-MCDA_outputs>`)


- :ref:`alternatives <MRSort-LargePerfDiff_inference_exact-R-MCDA-inalt>`
- :ref:`performanceTable <MRSort-LargePerfDiff_inference_exact-R-MCDA-inperf>`
- :ref:`criteria <MRSort-LargePerfDiff_inference_exact-R-MCDA-incrit>`
- :ref:`alternativesAssignments <MRSort-LargePerfDiff_inference_exact-R-MCDA-assignments>`
- :ref:`categoriesRanks <MRSort-LargePerfDiff_inference_exact-R-MCDA-incategval>`
- :ref:`parameters <MRSort-LargePerfDiff_inference_exact-R-MCDA-parameters>` *(optional)*


.. _MRSort-LargePerfDiff_inference_exact-R-MCDA-inalt:

alternatives
~~~~~~~~~~~~

A complete list of alternatives to be considered when inferring the MR-Sort model.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
			


------------------------


.. _MRSort-LargePerfDiff_inference_exact-R-MCDA-inperf:

performanceTable
~~~~~~~~~~~~~~~~

The evaluations of the alternatives on the set of criteria.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _MRSort-LargePerfDiff_inference_exact-R-MCDA-incrit:

criteria
~~~~~~~~

A list of criteria on which the alternatives are evaluated. For each criterion, the preference direction should be provided.

The input value should be a valid XMCDA document whose main tag is ``<criteriaScales>``.


------------------------


.. _MRSort-LargePerfDiff_inference_exact-R-MCDA-assignments:

alternativesAssignments
~~~~~~~~~~~~~~~~~~~~~~~

The alternatives assignments to categories.

The input value should be a valid XMCDA document whose main tag is ``<alternativesAssignments>``.


------------------------


.. _MRSort-LargePerfDiff_inference_exact-R-MCDA-incategval:

categoriesRanks
~~~~~~~~~~~~~~~

A list of categories ranks, 1 stands for the most preferred category and the higher the number the lower the preference for that category.

The input value should be a valid XMCDA document whose main tag is ``<categoriesValues>``.


------------------------


.. _MRSort-LargePerfDiff_inference_exact-R-MCDA-parameters:

parameters *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~

The program parameters.

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
			   
    <programParameters>
        <parameter id="assignmentRule">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </parameter>
        <parameter id="readableWeights">
            <values>
                <value>
                    <boolean>%2</boolean>
                </value>
            </values>
        </parameter>
        <parameter id="readableProfiles">
            <values>
                <value>
                    <boolean>%3</boolean>
                </value>
            </values>
        </parameter>
        <parameter id="minmaxLPDProfiles">
            <values>
                <value>
                    <boolean>%4</boolean>
                </value>
            </values>
        </parameter>
    </programParameters>

			

where:

- **%1** is a parameter named "Include vetoes". It can have the following values:

  - ``M``: Majority rule

  - ``V``: Veto

  - ``D``: Dictator

  - ``v``: Veto weakened by dictator

  - ``d``: Dictator weakened by veto

  - ``dV``: Dominating Veto and weakened Dictator

  - ``Dv``: Dominating Dictator and weakened veto

  - ``dv``: Conflicting Veto and Dictator

  The default value is M.

- **%2** is a parameter named "Readable weights". This is a boolean.
  The default value is false.

- **%3** is a parameter named "Readable profiles". This is a boolean.
  The default value is false.

- **%4** is a parameter named "Limit the use of large performance differences". This is a boolean.
  The default value is false.


------------------------



.. _MRSort-LargePerfDiff_inference_exact-R-MCDA_outputs:

Outputs
-------


- :ref:`categoriesProfilesPerformanceTable <MRSort-LargePerfDiff_inference_exact-R-MCDA-outcatprofpt>`
- :ref:`vetoProfilesPerformanceTable <MRSort-LargePerfDiff_inference_exact-R-MCDA-outcatvetopt>`
- :ref:`dictatorProfilesPerformanceTable <MRSort-LargePerfDiff_inference_exact-R-MCDA-outcatdictatorpt>`
- :ref:`criteriaWeights <MRSort-LargePerfDiff_inference_exact-R-MCDA-weights>`
- :ref:`categoriesProfiles <MRSort-LargePerfDiff_inference_exact-R-MCDA-outcatprof>`
- :ref:`vetoProfiles <MRSort-LargePerfDiff_inference_exact-R-MCDA-outcatveto>`
- :ref:`dictatorProfiles <MRSort-LargePerfDiff_inference_exact-R-MCDA-outcatdictator>`
- :ref:`majorityThreshold <MRSort-LargePerfDiff_inference_exact-R-MCDA-majority>`
- :ref:`messages <MRSort-LargePerfDiff_inference_exact-R-MCDA-msg>`


.. _MRSort-LargePerfDiff_inference_exact-R-MCDA-outcatprofpt:

categoriesProfilesPerformanceTable
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The evaluations of the category profiles.

The returned value is a XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _MRSort-LargePerfDiff_inference_exact-R-MCDA-outcatvetopt:

vetoProfilesPerformanceTable
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The evaluations of the veto profiles.

The returned value is a XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _MRSort-LargePerfDiff_inference_exact-R-MCDA-outcatdictatorpt:

dictatorProfilesPerformanceTable
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The evaluations of the dictator profiles.

The returned value is a XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _MRSort-LargePerfDiff_inference_exact-R-MCDA-weights:

criteriaWeights
~~~~~~~~~~~~~~~

The criteria weights.

The returned value is a XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _MRSort-LargePerfDiff_inference_exact-R-MCDA-outcatprof:

categoriesProfiles
~~~~~~~~~~~~~~~~~~

The categories delimiting profiles.

The returned value is a XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _MRSort-LargePerfDiff_inference_exact-R-MCDA-outcatveto:

vetoProfiles
~~~~~~~~~~~~

The categories veto profiles.

The returned value is a XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _MRSort-LargePerfDiff_inference_exact-R-MCDA-outcatdictator:

dictatorProfiles
~~~~~~~~~~~~~~~~

The categories dictator profiles.

The returned value is a XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _MRSort-LargePerfDiff_inference_exact-R-MCDA-majority:

majorityThreshold
~~~~~~~~~~~~~~~~~

The majority threshold.

The returned value is a XMCDA document whose main tag is ``<programParameters>``.


------------------------


.. _MRSort-LargePerfDiff_inference_exact-R-MCDA-msg:

messages
~~~~~~~~

Messages from the execution of the webservice. Possible errors in the input data will be given here.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/MRSort-LargePerfDiff_inference_exact-R-MCDA/description-wsDD.xml>`
