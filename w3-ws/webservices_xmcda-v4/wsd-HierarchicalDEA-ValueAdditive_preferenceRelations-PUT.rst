.. _HierarchicalDEA-ValueAdditive_preferenceRelations-PUT:

HierarchicalDEA-ValueAdditive_preferenceRelations
=================================================

:Version: 1.0
:Provider: PUT
:SOAP service's name: ``HierarchicalDEA-ValueAdditive_preferenceRelations-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Computes necessary and possible preference relations for the pairs of DMUs (alternatives) using Additive Data Envelopment Analysis Model with hierarchical structure of inputs and outputs.

**Contact:** 
            Anna Labijak <anna.labijak@cs.put.poznan.pl>
        


Inputs
------
(For outputs, see :ref:`below <HierarchicalDEA-ValueAdditive_preferenceRelations-PUT_outputs>`)


- :ref:`units <HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-units>`
- :ref:`inputsOutputsScales <HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-inputsOutputsScales>`
- :ref:`performanceTable <HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-performanceTable>`
- :ref:`hierarchy <HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-hierarchy>`
- :ref:`weightsLinearConstraints <HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-methodParameters>`


.. _HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-units:

units
~~~~~

A list of alternatives (DMUs).

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
                <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
            


------------------------


.. _HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-inputsOutputsScales:

inputsOutputsScales
~~~~~~~~~~~~~~~~~~~

Information about inputs and outpus (leaf criteria) scales (preference directions) and optionally about boundaries

The input value should be a valid XMCDA document whose main tag is ``<criteriaScales>``.
It must have the following form::

   
                
<criteriaScales>
    <criterionScale>
      <criterionID>[...]</criterionID>
      <scales>
        <scale>
          [...]
        </scale>
      </scales>
    </criterionScale>
    [...]
</criteriaScales>
            


------------------------


.. _HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A list of alternatives (DMUs) performances (on leaf hierarchy criteria).

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   
                <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<values><value>[...]</value></values>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>
            


------------------------


.. _HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-hierarchy:

hierarchy
~~~~~~~~~

The hierarchical structure of criteria.

The input value should be a valid XMCDA document whose main tag is ``<criteriaHierarchy>``.
It must have the following form::

   
                <criteriaHierarchy>
						<nodes>
                            <node>
                                <criterionID>[...]</criterionID>
                                <nodes>
                                    <node>
                                        <criterionID>[...]</criterionID>
                                        [...]
                                    </node>
                                    [...]
                                </nodes>
                            </node>
                        <nodes>
					</criteriaHierarchy>
            


------------------------


.. _HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-weightsLinearConstraints:

weightsLinearConstraints *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of hierarchy criteria linear constraints.

The input value should be a valid XMCDA document whose main tag is ``<criteriaLinearConstraints>``.
It must have the following form::

   
                
            <criteriaLinearConstraints>
                <constraints>
                    <constraint>
                        <elements>
                            <element>
                                <criterionID> [...] </criterionID>
                                <coefficient>
                                    [...]
                                </coefficient>
                            </element>
                            [...]
                        </elements>
                        <operator>[...]</operator>
                        <rhs>
                            [...]
                        </rhs>
                    </constraint>
                    [...]
                </constraints>
            </criteriaLinearConstraints>
            


------------------------


.. _HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~

Represents parameters.
                "hierarchy node" is the ID of the hierarchy criterion for which the analysis should be performed;
                "transformToUtilities" means if data should be tranformed into values from range [0-1];
                "boundariesProvided" means if inputsOutputs file contains information about min and max data for each factor.

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
                
    <programParameters>
        <parameter id="hierarchyNode">
            <values>
                <value><label>%1</label></value>
            </values>
        </parameter>
        <parameter id="transformToUtilities">
      <values>
        <value><boolean>%2</boolean></value>
      </values>
		</parameter>
		<parameter id="boundariesProvided">
      <values>
        <value><boolean>%3</boolean></value>
      </values>
		</parameter>
    </programParameters>
            

where:

- **%1** is a parameter named "hierarchy node". This is a string.
  The default value is root.

- **%2** is a parameter named "transform to utilities". This is a boolean.
  The default value is true.

- **%3** is a parameter named "boundaries provided". This is a boolean.
  The default value is false.


------------------------



.. _HierarchicalDEA-ValueAdditive_preferenceRelations-PUT_outputs:

Outputs
-------


- :ref:`necessaryDominance <HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-necessaryDominance>`
- :ref:`possibleDominance <HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-possibleDominance>`
- :ref:`messages <HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-messages>`


.. _HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-necessaryDominance:

necessaryDominance
~~~~~~~~~~~~~~~~~~

A matrix containing necessary preference relation between pairs of DMUs.

The returned value is a XMCDA document whose main tag is ``<alternativesMatrix>``.
It has the following form::

   
                
                    <alternativesMatrix>
						<row>
                            <alternativeID>[...]</alternativeID>
                            <column>
                                <alternativeID>[...]</alternativeID>
                                <values>
                                    <value><integer>1</integer></value>
                                </values>
                            </column>
                            [...]
                        </row>
                        [...]
					</alternativesMatrix>
                    
            


------------------------


.. _HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-possibleDominance:

possibleDominance
~~~~~~~~~~~~~~~~~

A matrix containing possible preference relation between pairs of DMUs.

The returned value is a XMCDA document whose main tag is ``<alternativesMatrix>``.
It has the following form::

   
                
                    <alternativesMatrix>
						<row>
                            <alternativeID>[...]</alternativeID>
                            <column>
                                <alternativeID>[...]</alternativeID>
                                <values>
                                    <value><integer>1</integer></value>
                                </values>
                            </column>
                            [...]
                        </row>
                        [...]
					</alternativesMatrix>
                    
            


------------------------


.. _HierarchicalDEA-ValueAdditive_preferenceRelations-PUT-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/HierarchicalDEA-ValueAdditive_preferenceRelations-PUT/description-wsDD.xml>`
