.. _ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT:

ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations
===================================================

:Version: 1.0
:Provider: PUT
:SOAP service's name: ``ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Determines dominance relations for the given DMUs (alternatives) using SMAA-D method and Additive Data Envelopment Analysis Model with imprecise data. For given number of samples  returns a matrix with alternatives in each row and column. Single cell indicates how many samples of alternative in a row dominates alternative in a column.

**Contact:** Anna Labijak <support@decision-deck.org>


Inputs
------
(For outputs, see :ref:`below <ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT_outputs>`)


- :ref:`units <ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-units>`
- :ref:`inputsOutputs <ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-inputsOutputs>`
- :ref:`inputsOutputsScales <ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-inputsOutputsScales>`
- :ref:`inputsOutputsFunctions <ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-inputsOutputsFunctions>` *(optional)*
- :ref:`performanceTable <ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-performanceTable>`
- :ref:`maxPerformanceTable <ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-maxPerformanceTable>` *(optional)*
- :ref:`weightsLinearConstraints <ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-methodParameters>`


.. _ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-units:

units
~~~~~

A list of alternatives (DMUs).

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
         <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>
       


------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-inputsOutputs:

inputsOutputs
~~~~~~~~~~~~~

A list of criteria with specified preference direction. List has to contains at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output).

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
         <criteria>
                        <criterion>[...]</criterion>
                        [...]
                    </criteria>
       


------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-inputsOutputsScales:

inputsOutputsScales
~~~~~~~~~~~~~~~~~~~

Informations about inputs and outputs scales and optionally about boundaries

The input value should be a valid XMCDA document whose main tag is ``<criteriaScales>``.
It must have the following form::

   
         
<criteriaScales>
    <criterionScale>
      <criterionID>[...]</criterionID>
      <scales>
        <scale>
          [...]
        </scale>
      </scales>
    </criterionScale>
    [...]
</criteriaScales>
       


------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-inputsOutputsFunctions:

inputsOutputsFunctions *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Shapes of functions assigned to inputs and outputs (if other than linear)

The input value should be a valid XMCDA document whose main tag is ``<criteriaFunctions>``.
It must have the following form::

   
         
<criteriaFunctions>
    <criterionFunction>
      <criterionID>[...]</criterionID>
      <functions>
        <function>
          <piecewiseLinear>
            <segment>
              <head>
                [...]
              </head>
              <tail>
                [...]
              </tail>
            </segment>
          </piecewiseLinear>
        </function>
        [...]
      </functions>
    </criterionFunction>
    [...]
</criteriaFunctions>
       


------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A list of alternatives (DMUs) minimal performances (or exact performances if intervals should be created by tolerance).

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   
         <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<values><value>[...]</value></values>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>
       


------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-maxPerformanceTable:

maxPerformanceTable *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of alternatives (DMUs) maximal performances.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   
         <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<values><value>[...]</value></values>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>
       


------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-weightsLinearConstraints:

weightsLinearConstraints *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of criteria linear constraints.

The input value should be a valid XMCDA document whose main tag is ``<criteriaLinearConstraints>``.
It must have the following form::

   
         
            <criteriaLinearConstraints>
                <constraints>
                    <constraint>
                        <elements>
                            <element>
                                <criterionID> [...] </criterionID>
                                <coefficient>
                                    [...]
                                </coefficient>
                            </element>
                            [...]
                        </elements>
                        <operator>[...]</operator>
                        <rhs>
                            [...]
                        </rhs>
                    </constraint>
                    [...]
                </constraints>
            </criteriaLinearConstraints>
       


------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~

Represents method parameters.
            "tolerance" represents the fraction for creating interval data (created interval: [data*(1-tolerance), data*(1+tolerance)]),
            "transformToUtilities" means if data should be tranformed into values from range [0-1],
            "boundariesProvided" means if inputsOutputs file contains information about min and max data for each factor,
            "functionShapeProvided" means if inputsOutputs file contains information about the shapes of value function for given factor,
            "samplesNb" determines number of samples used to calculate results.

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
      
	<programParameters>
		<parameter id="samplesNb">
			<values>
				<value><integer>%1</integer></value>
			</values>
		</parameter>
		<parameter id="tolerance">
			<values>
				<value><real>%2</real></value>
			</values>
		</parameter>
		<parameter id="transformToUtilities">
			<values>
				<value><boolean>%3</boolean></value>
			</values>
		</parameter>
		<parameter id="boundariesProvided">
			<values>
				<value><boolean>%4</boolean></value>
			</values>
		</parameter>
		<parameter id="functionShapeProvided">
			<values>
				<value><boolean>%5</boolean></value>
			</values>
		</parameter>
	</programParameters>


where:

- **%1** is a parameter named "number of samples". This is a int, and the value should conform to the following constraint: The value should be a positive integer..  More formally, the constraint is::

     %1 > 0
  The default value is 100.

- **%2** is a parameter named "tolerance". This is a float, and the value should conform to the following constraint: The value should be a non-negative..  More formally, the constraint is::

     %2 >= 0
  The default value is 0.0.

- **%3** is a parameter named "transform to utilities". This is a boolean.
  The default value is true.

- **%4** is a parameter named "boundaries provided". This is a boolean.
  The default value is false.

- **%5** is a parameter named "function shapes provided". This is a boolean.
  The default value is false.


------------------------



.. _ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT_outputs:

Outputs
-------


- :ref:`pairwiseOutrankingIndices <ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-pairwiseOutrankingIndices>`
- :ref:`messages <ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-messages>`


.. _ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-pairwiseOutrankingIndices:

pairwiseOutrankingIndices
~~~~~~~~~~~~~~~~~~~~~~~~~

A performance table for given alternatives. Single performance consists of attribute criterionID representing dominated alternative, and a value representing ratio of samples dominating this alternative.

The returned value is a XMCDA document whose main tag is ``<performanceTable>``.
It has the following form::

   
         <performanceTable>
						<alternativePerformances>
							<alternativeID> [...] </alternativeID>
							<performance>
								<criterionID> geq [...]</criterionID>
								<values>
                    <value>[...]</value>
                </values>
							</performance>
							[...]
						</alternativePerformances>
            [...]
					</performanceTable>
       


------------------------


.. _ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT/description-wsDD.xml>`
