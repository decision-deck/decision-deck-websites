.. _MRSort_inference_approx-R-MCDA:

Approximative inference of MRSort
=================================

:Version: 1.0
:Provider: R-MCDA
:Name: MRSort_inference_approx
:SOAP service's name: ``MRSort_inference_approx-R-MCDA`` (see :ref:`soap-requests` for details)

Description
-----------

MRSort is a simplification of the Electre TRI method that uses the pessimistic assignment rule, without indifference or preference thresholds attached to criteria. Only a binary discordance condition is considered, i.e. a veto forbids an outranking in any possible concordance situation, or not. The identification of the profiles, weights, majority threshold and veto thresholds are done by taking into account assignment examples.

**Contact:** Alexandru Olteanu (alexandru.olteanu@univ-ubs.fr)


Inputs
------
(For outputs, see :ref:`below <MRSort_inference_approx-R-MCDA_outputs>`)


- :ref:`alternatives <MRSort_inference_approx-R-MCDA-inalt>`
- :ref:`performanceTable <MRSort_inference_approx-R-MCDA-inperf>`
- :ref:`criteria <MRSort_inference_approx-R-MCDA-incrit>`
- :ref:`alternativesAssignments <MRSort_inference_approx-R-MCDA-assignments>`
- :ref:`categoriesRanks <MRSort_inference_approx-R-MCDA-incategval>`
- :ref:`parameters <MRSort_inference_approx-R-MCDA-parameters>` *(optional)*


.. _MRSort_inference_approx-R-MCDA-inalt:

alternatives
~~~~~~~~~~~~

A complete list of alternatives to be considered when inferring the MR-Sort model.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _MRSort_inference_approx-R-MCDA-inperf:

performanceTable
~~~~~~~~~~~~~~~~

The evaluations of the alternatives on the set of criteria.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _MRSort_inference_approx-R-MCDA-incrit:

criteria
~~~~~~~~

A list of criteria on which the alternatives are evaluated. For each criterion, the preference direction should be provided.

The input value should be a valid XMCDA document whose main tag is ``<criteriaScales>``.


------------------------


.. _MRSort_inference_approx-R-MCDA-assignments:

alternativesAssignments
~~~~~~~~~~~~~~~~~~~~~~~

The alternatives assignments to categories.

The input value should be a valid XMCDA document whose main tag is ``<alternativesAssignments>``.


------------------------


.. _MRSort_inference_approx-R-MCDA-incategval:

categoriesRanks
~~~~~~~~~~~~~~~

A list of categories ranks, 1 stands for the most preferred category and the higher the number the lower the preference for that category.

The input value should be a valid XMCDA document whose main tag is ``<categoriesValues>``.


------------------------


.. _MRSort_inference_approx-R-MCDA-parameters:

parameters *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~

The method parameters.

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   

    <programParameters>
        <parameter id="veto">
            <values>
                <value>
                    <boolean>%1</boolean>
                </value>
            </values>
        </parameter>
        <parameter id="time">
            <values>
                <value>
                    <integer>%2</integer>
                </value>
            </values>
        </parameter>
        <parameter id="population">
            <values>
                <value>
                    <integer>%3</integer>
                </value>
            </values>
        </parameter>
        <parameter id="mutation">
            <values>
                <value>
                    <real>%4</real>
                </value>
            </values>
        </parameter>
        <parameter id="test">
            <values>
                <value>
                    <boolean>%5</boolean>
                </value>
            </values>
        </parameter>
    </programParameters>

			

where:

- **%1** is a parameter named "Include vetoes". This is a boolean.
  The default value is false.

- **%2** is a parameter named "Time limit". This is a int, and the value should conform to the following constraint: An integer value (minimum 1).  More formally, the constraint is::

      %2 > 0 
  The default value is 60.

- **%3** is a parameter named "Population size". This is a int, and the value should conform to the following constraint: An integer value (minimum 10).  More formally, the constraint is::

      %3 >= 10 
  The default value is 20.

- **%4** is a parameter named "Mutation probability". This is a float, and the value should conform to the following constraint: A value between 0 and 1.  More formally, the constraint is::

      %4 >= 0 && %4 <= 1 
  The default value is 0.1.

- **%5** is a parameter named "Test". This is a boolean.
  The default value is false.


------------------------



.. _MRSort_inference_approx-R-MCDA_outputs:

Outputs
-------


- :ref:`categoriesProfilesPerformanceTable <MRSort_inference_approx-R-MCDA-outcatprofpt>`
- :ref:`vetoProfilesPerformanceTable <MRSort_inference_approx-R-MCDA-outcatvetopt>`
- :ref:`criteriaWeights <MRSort_inference_approx-R-MCDA-weights>`
- :ref:`categoriesProfiles <MRSort_inference_approx-R-MCDA-outcatprof>`
- :ref:`vetoProfiles <MRSort_inference_approx-R-MCDA-outcatveto>`
- :ref:`fitness <MRSort_inference_approx-R-MCDA-fitness>`
- :ref:`majorityThreshold <MRSort_inference_approx-R-MCDA-majority>`
- :ref:`messages <MRSort_inference_approx-R-MCDA-msg>`


.. _MRSort_inference_approx-R-MCDA-outcatprofpt:

categoriesProfilesPerformanceTable
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The evaluations of the category profiles.

The returned value is a XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _MRSort_inference_approx-R-MCDA-outcatvetopt:

vetoProfilesPerformanceTable
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The evaluations of the veto profiles.

The returned value is a XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _MRSort_inference_approx-R-MCDA-weights:

criteriaWeights
~~~~~~~~~~~~~~~

The criteria weights.

The returned value is a XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _MRSort_inference_approx-R-MCDA-outcatprof:

categoriesProfiles
~~~~~~~~~~~~~~~~~~

The categories delimiting profiles.

The returned value is a XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _MRSort_inference_approx-R-MCDA-outcatveto:

vetoProfiles
~~~~~~~~~~~~

The categories veto profiles.

The returned value is a XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _MRSort_inference_approx-R-MCDA-fitness:

fitness
~~~~~~~

The model fitness.

The returned value is a XMCDA document whose main tag is ``<programParameters>``.


------------------------


.. _MRSort_inference_approx-R-MCDA-majority:

majorityThreshold
~~~~~~~~~~~~~~~~~

The majority threshold.

The returned value is a XMCDA document whose main tag is ``<programParameters>``.


------------------------


.. _MRSort_inference_approx-R-MCDA-msg:

messages
~~~~~~~~

Messages from the execution of the webservice. Possible errors in the input data will be given here.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/MRSort_inference_approx-R-MCDA/description-wsDD.xml>`
