.. _plotCriteriaFunctions-PyXMCDA:

plotCriteriaFunctions
=====================

:Version: 1.3
:Provider: PyXMCDA
:SOAP service's name: ``plotCriteriaFunctions-PyXMCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Generate plots from provided criteriaFunctions as well as the scripts generating these plots.

**Contact:** Nicolas Duminy (nicolas.duminy@telecom-bretagne.eu)

**Web page:** https://gitlab.com/nduminy/ws-pyxmcda


Inputs
------
(For outputs, see :ref:`below <plotCriteriaFunctions-PyXMCDA_outputs>`)


- :ref:`criteria <plotCriteriaFunctions-PyXMCDA-criteria>` *(optional)*
- :ref:`criteriaFunctions <plotCriteriaFunctions-PyXMCDA-criteriaFunctions>`
- :ref:`parameters <plotCriteriaFunctions-PyXMCDA-parameters>` *(optional)*


.. _plotCriteriaFunctions-PyXMCDA-criteria:

criteria *(optional)*
~~~~~~~~~~~~~~~~~~~~~

The criteria to be plotted. All are plotted if not provided.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _plotCriteriaFunctions-PyXMCDA-criteriaFunctions:

criteriaFunctions
~~~~~~~~~~~~~~~~~

The criteria functions.

The input value should be a valid XMCDA document whose main tag is ``<criteriaFunctions>``.


------------------------


.. _plotCriteriaFunctions-PyXMCDA-parameters:

parameters *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~

Parameters of the method

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
    <programParameters>
        <programParameter id="x_axis" name="X-axis label">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="y_axis" name="Y-axis label">
            <values>
                <value>
                    <label>%2</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="color" name="Color">
            <values>
                <value>
                    <label>%3</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="marker" name="Marker">
            <values>
                <value>
                    <label>%4</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="linestyle" name="Linestyle">
            <values>
                <value>
                    <label>%5</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="show_grid">
            <values>
                <value>
                    <boolean>%6</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="image_file_extension" name="Image file extension">
            <values>
                <value>
                    <label>%7a%7b</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="plotter" name="Plotter">
            <values>
                <value>
                    <label>%8</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="order_by" name="Order by">
            <values>
                <value>
                    <label>%9</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="reverse_order" name="Reverse order">
            <values>
                <value>
                    <boolean>%10</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="linear_interpolation">
            <values>
                <value>
                    <boolean>%11</boolean>
                </value>
            </values>
        </programParameter>
	<programParameter id="naming_conventions" name="Naming conventions">
	        <values>
                <value>
		            <label>%12</label>
                </value>
	        </values>
        </programParameter>
    </programParameters>


where:

- **%1** is a parameter named "X-axis label". This is a string.
  The default value is x.

- **%2** is a parameter named "Y-axis label". This is a string.
  The default value is y.

- **%3** is a parameter named "Color". It can have the following values:

  - ``r``: red

  - ``b``: blue

  - ``g``: green

  - ``c``: cyan

  - ``m``: magenta

  - ``y``: yellow

  - ``k``: black

  - ``w``: white

  The default value is black.

- **%4** is a parameter named "Marker". It can have the following values:

  - ``.``: point

  - ``None``: none

  - ``o``: circle

  - ``v``: triangle down

  - ``^``: triangle up

  - ``&lt;``: triangle left

  - ``&gt;``: triangle right

  - ``s``: square

  - ``p``: pentagon

  - ``*``: star

  - ``h``: hexagon 1

  - ``H``: hexagon 2

  - ``+``: plus

  - ``x``: x

  - ``d``: thin diamond

  - ``D``: diamond

  - ``|``: vline

  - ``_``: hline

  - ``P``: plus filled

  - ``X``: x (filled)

  The default value is circle.

- **%5** is a parameter named "Linestyle". It can have the following values:

  - ``solid``: solid line

  - ``dashed``: dashed line

  - ``dashdot``: dashed and dotted line

  - ``dotted``: dotted line

  - ``None``: no line

  The default value is dashed.

- **%6** is a parameter named "Show grid". This is a boolean.
  The default value is false.

- **%7a** is a parameter named "Image file extension". It can have the following values:

  - ``eps``: .eps (Encapsulated PostScript)

  - ``jpg``: .jpg (Joint Photographic Experts Group)

  - ``jpeg``: .jpeg (Joint Photographic Experts Group)

  - ``pdf``: .pdf (Portable Document Format)

  - ``pgf``: .pgf (Progressive Graphics File)

  - ``png``: .png (Portable Network Graphics)

  - ``ps``: .ps (PostScript)

  - ``raw``: .raw (Raw RGBA bitmap)

  - ``rgba``: .rgba (Silicon Graphics RGB)

  - ``svg``: .svg (Scalable Vector Graphics)

  - ``svgz``: .svgz (Compressed Scalable Vector Graphics)

  - ``tif``: .tif (Tagged Image File Format)

  - ``tiff``: .tiff (Tagged Image File Format)

  The default value is png.

- **%7b** is a parameter named "Image file extension". It can have the following values:

  - ``eps``: .eps (Encapsulated PostScript)

  - ``jpg``: .jpg (Joint Photographic Experts Group)

  - ``jpeg``: .jpeg (Joint Photographic Experts Group)

  - ``pdf``: .pdf (Portable Document Format)

  - ``png``: .png (Portable Network Graphics)

  - ``svg``: .svg (Scalable Vector Graphics)

  The default value is png.

- **%8** is a parameter named "Plotter". It can have the following values:

  - ``matplotlib``: Matplotlib

  - ``gnuplot``: Gnuplot

  The default value is matplotlib.

- **%9** is a parameter named "Order by". It can have the following values:

  - ``id``: ids

  - ``name``: names

  The default value is id.

- **%10** is a parameter named "Reverse order". This is a boolean.
  The default value is false.

- **%11** is a parameter named "Linear interpolation". This is a boolean.
  The default value is false.

- **%12** is a parameter named "Naming conventions". It can have the following values:

  - ``id``: Only ids are shown

  - ``name``: Only names are shown (disambiguated by appending the ids, if needed)

  - ``name (id)``: Names and ids are shown in that order

  - ``id (name)``: Ids and names are shown in that order

  The default value is name.


------------------------



.. _plotCriteriaFunctions-PyXMCDA_outputs:

Outputs
-------


- :ref:`glob:criteriaFunctions.{bmp,dia,fig,gif,hpgl,ico,jpg,jpeg,jpe,pdf,png,ps,ps2,svg,svgz,tif,tiff} <plotCriteriaFunctions-PyXMCDA-criteriaFunctionsPlot>`
- :ref:`glob:plot_criteriaFunctions.{py,plt} <plotCriteriaFunctions-PyXMCDA-criteriaFunctionsPlotScript>`
- :ref:`messages <plotCriteriaFunctions-PyXMCDA-messages>`


.. _plotCriteriaFunctions-PyXMCDA-criteriaFunctionsPlot:

glob:criteriaFunctions.{bmp,dia,fig,gif,hpgl,ico,jpg,jpeg,jpe,pdf,png,ps,ps2,svg,svgz,tif,tiff}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Image containing all selected criteriaFunctions plots. Format corresponds to the one given in parameters (default is .png).

The returned value is a XMCDA document whose main tag is ``<None>``.


------------------------


.. _plotCriteriaFunctions-PyXMCDA-criteriaFunctionsPlotScript:

glob:plot_criteriaFunctions.{py,plt}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Generated Python script that made the image. Given to enable users to later customize the appearance of the plots. Extension is .py if matplotlib is used, .plt if gnuplot.

The returned value is a XMCDA document whose main tag is ``<None>``.


------------------------


.. _plotCriteriaFunctions-PyXMCDA-messages:

messages
~~~~~~~~

Status messages.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/plotCriteriaFunctions-PyXMCDA/description-wsDD.xml>`
