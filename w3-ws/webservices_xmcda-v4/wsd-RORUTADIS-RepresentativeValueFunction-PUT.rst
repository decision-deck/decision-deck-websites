.. _RORUTADIS-RepresentativeValueFunction-PUT:

RORUTADIS-RepresentativeValueFunction
=====================================

:Version: 0.3
:Provider: PUT
:SOAP service's name: ``RORUTADIS-RepresentativeValueFunction-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Robust Ordinal Regression for value-based sorting: RORUTADIS-RepresentativeValueFunction service finds representative function. It is possible to provide an additional optional preference information: example alternatives assignments, assignment pairwise comparisons and desired class cardinalities. Service developed by Krzysztof Ciomek (Poznan University of Technology, under supervision of Milosz Kadzinski).

**Contact:** 
			Krzysztof Ciomek (k.ciomek@gmail.com),
			Milosz Kadzinski (milosz.kadzinski@cs.put.poznan.pl)
		

**Web page:** https://github.com/kciomek/rorutadis

**Reference:** None


Inputs
------
(For outputs, see :ref:`below <RORUTADIS-RepresentativeValueFunction-PUT_outputs>`)


- :ref:`criteria <RORUTADIS-RepresentativeValueFunction-PUT-criteria>`
- :ref:`alternatives <RORUTADIS-RepresentativeValueFunction-PUT-alternatives>`
- :ref:`categories <RORUTADIS-RepresentativeValueFunction-PUT-categories>`
- :ref:`performanceTable <RORUTADIS-RepresentativeValueFunction-PUT-performanceTable>`
- :ref:`assignmentExamples <RORUTADIS-RepresentativeValueFunction-PUT-assignmentExamples>` *(optional)*
- :ref:`assignmentComparisons <RORUTADIS-RepresentativeValueFunction-PUT-assignmentComparisons>` *(optional)*
- :ref:`preferenceRelation <RORUTADIS-RepresentativeValueFunction-PUT-preferenceRelation>` *(optional)*
- :ref:`categoriesCardinalities <RORUTADIS-RepresentativeValueFunction-PUT-categoriesCardinalities>` *(optional)*
- :ref:`methodParameters <RORUTADIS-RepresentativeValueFunction-PUT-methodParameters>`


.. _RORUTADIS-RepresentativeValueFunction-PUT-criteria:

criteria
~~~~~~~~

A list of criteria (<criteria> tag) with information about preference direction (<criteriaValues mcdaConcept="preferenceDirection">, 0 - gain, 1 - cost) and number of characteristic points (<criteriaValues mcdaConcept="numberOfCharacteristicPoints">, 0 for the most general marginal utility function or integer grater or equal to 2) of each criterion.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
<criteria>
	<criterion id="[...]" />
	[...]
</criteria>

<criteriaValues mcdaConcept="preferenceDirection">
	<criterionValue>
		<criterionID>[...]</criterionID>
		<value><integer>[...]</integer></value>
	</criterionValue>
	[...]
</criteriaValues>

<criteriaValues mcdaConcept="numberOfCharacteristicPoints">
	<criterionValue>
		<criterionID>[...]</criterionID>
		<value><integer>[0|integer greater or equal to 2]</integer></value>
	</criterionValue>
	[...]
</criteriaValues>



------------------------


.. _RORUTADIS-RepresentativeValueFunction-PUT-alternatives:

alternatives
~~~~~~~~~~~~

A list of alternatives.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
<alternatives>
	<alternative id="...">
		<active>[...]</active>
	</alternative>
</alternatives>



------------------------


.. _RORUTADIS-RepresentativeValueFunction-PUT-categories:

categories
~~~~~~~~~~

A list of categories (classes). List must be sorted from the worst category to the best.

The input value should be a valid XMCDA document whose main tag is ``<categories>``.
It must have the following form::

   
<categories>
	<category id="[...]" />
	[...]
</categories>



------------------------


.. _RORUTADIS-RepresentativeValueFunction-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

The performances of the alternatives.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _RORUTADIS-RepresentativeValueFunction-PUT-assignmentExamples:

assignmentExamples *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of assignment examples of alternatives to intervals of categories (classes) or to a specific category (class).

The input value should be a valid XMCDA document whose main tag is ``<alternativesAssignments>``.
It must have the following form::

   
<alternativesAssignments>
	<alternativeAssignment>
		<alternativeID>[...]</alternativeID>
		<categoryID>[...]</categoryID>
	</alternativeAssignment>
	[...]
	<alternativeAssignment>
		<alternativeID>[...]</alternativeID>
		<categoriesInterval>
			<lowerBound>
				<categoryID>[...]</categoryID>
			</lowerBound>
			<upperBound>
				<categoryID>[...]</categoryID>
			</upperBound>
		</categoriesInterval>
	</alternativeAssignment>
	[...]
	<alternativeAssignment>
		<alternativeID>[...]</alternativeID>
		<categoriesSet>
			<categoryID>[...]</categoryID>
			[...]
		</categoriesSet>
	</alternativeAssignment>
	[...]
</alternativesAssignments>



------------------------


.. _RORUTADIS-RepresentativeValueFunction-PUT-assignmentComparisons:

assignmentComparisons *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Two lists of assignment pairwise comparisons. A comparison from list with attribute mcdaConcept="atLeastAsGoodAs" indicates that some alternative should be assigned to class at least as good as class of some other alternative (k = 0) or at least better by k classes (k > 0). A comparison from list with attribute mcdaConcept="atMostAsGoodAs" indicates that some alternative should be assigned to class at most better by k classes (k > 0) then some other alternative.

The input value should be a valid XMCDA document whose main tag is ``<alternativesMatrix>``.
It must have the following form::

   
<alternativesMatrix mcdaConcept="atLeastAsGoodAs">
	<row>
		<alternativeID>[...]</alternativeID>
		<column>
			<alternativeID>[...]</alternativeID>
			<values>
				<value>
					<integer>k</integer>
				</value>
			</values>
		</column>
	</row>
	[...]
</alternativesMatrix>

<alternativesMatrix mcdaConcept="atMostAsGoodAs">
	<row>[...]</row>
	[...]
</alternativesMatrix>



------------------------


.. _RORUTADIS-RepresentativeValueFunction-PUT-preferenceRelation:

preferenceRelation *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Necessary assignment based preference relation between alternatives.

The input value should be a valid XMCDA document whose main tag is ``<alternativesMatrix>``.
It must have the following form::

   
<alternativesMatrix mcdaConcept="atLeastAsGoodAs">
	<row>
		<alternativeID>[...]</alternativeID>
		<column>
			<alternativeID>[...]</alternativeID>
			<values>
				<value>
					<real>...</real>
				</value>
			</values>
		</column>
	</row>
	[...]
</alternativesMatrix>



------------------------


.. _RORUTADIS-RepresentativeValueFunction-PUT-categoriesCardinalities:

categoriesCardinalities *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of category (class) cardinality constraints. It allows to define minimal and/or maximal desired category (class) cardinalities.

The input value should be a valid XMCDA document whose main tag is ``<categoriesValues>``.
It must have the following form::

   
<categoriesValues>
	<categoryValue>
		<categoryID>[...]</categoryID>
		<values>
			<value>
				<interval>
					<lowerBound><integer>[...]</integer></lowerBound>
					<upperBound><integer>[...]</integer></upperBound>
				</interval>
			</value>
		</values>
	</categoryValue>
	[...]
</categoriesValues>



------------------------


.. _RORUTADIS-RepresentativeValueFunction-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~

Method parameters.

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
<programParameters>
	<parameter name="strictlyMonotonicValueFunctions">
		<values>
			<value><boolean>%1</boolean></value>
		</values>
	</parameter>
	<parameter name="mode">
		<values>
			<value><label>%2</label></value>
		</values>
	</parameter>
</programParameters>


where:

- **%1** is a parameter named "strictlyMonotonicValueFunctions". This is a boolean.
  The default value is false.

- **%2** is a parameter named "mode". It can have the following values:

  - ``iterative``: Iterative

  - ``compromise``: Compromise

  The default value is iterative.


------------------------



.. _RORUTADIS-RepresentativeValueFunction-PUT_outputs:

Outputs
-------


- :ref:`marginalValueFunctions <RORUTADIS-RepresentativeValueFunction-PUT-marginalValueFunctions>`
- :ref:`alternativesValuesTable <RORUTADIS-RepresentativeValueFunction-PUT-alternativesValuesTable>`
- :ref:`assignments <RORUTADIS-RepresentativeValueFunction-PUT-assignments>`
- :ref:`thresholds <RORUTADIS-RepresentativeValueFunction-PUT-thresholds>`
- :ref:`messages <RORUTADIS-RepresentativeValueFunction-PUT-messages>`


.. _RORUTADIS-RepresentativeValueFunction-PUT-marginalValueFunctions:

marginalValueFunctions
~~~~~~~~~~~~~~~~~~~~~~

Marginal value functions for found representative function.

The returned value is a XMCDA document whose main tag is ``<criteriaFunctions>``.
It has the following form::

   
<criteriaFunctions>
	<criterionFunction>
		<criterionID>g01</criterionID>
		<functions>
			<function>
				<discrete>
					<point>
						<abscissa><real>[...]</real></abscissa>
						<ordinate><real>[...]</real></ordinate>
					</point>
					[...]
				</discrete>
			</function>
		</functions>
	</criterionFunction>
	<criterionFunction>[...]</criterionFunction>
	[...]
</criteriaFuctions>



------------------------


.. _RORUTADIS-RepresentativeValueFunction-PUT-alternativesValuesTable:

alternativesValuesTable
~~~~~~~~~~~~~~~~~~~~~~~

Marginal utility values of alternatives for found representative function.

The returned value is a XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _RORUTADIS-RepresentativeValueFunction-PUT-assignments:

assignments
~~~~~~~~~~~

Alternative assignments for found representative function.

The returned value is a XMCDA document whose main tag is ``<alternativesAssignments>``.
It has the following form::

   
<alternativesAssignments>
	<alternativeAssignment>
		<alternativeID>[...]</alternativeID>
		<categoryID>[...]</categoryID>
	</alternativeAssignment>
	[...]
	<alternativeAssignment>
		<alternativeID>[...]</alternativeID>
		<categoriesInterval>
			<lowerBound>
				<categoryID>[...]</categoryID>
			</lowerBound>
			<upperBound>
				<categoryID>[...]</categoryID>
			</upperBound>
		</categoriesInterval>
	</alternativeAssignment>
	[...]
	<alternativeAssignment>
		<alternativeID>[...]</alternativeID>
		<categoriesSet>
			<categoryID>[...]</categoryID>
			[...]
		</categoriesSet>
	</alternativeAssignment>
	[...]
</alternativesAssignments>



------------------------


.. _RORUTADIS-RepresentativeValueFunction-PUT-thresholds:

thresholds
~~~~~~~~~~

Lower threshold for each category (class) for found representative function.

The returned value is a XMCDA document whose main tag is ``<categoriesValues>``.
It has the following form::

   
<categoriesValues mcdaConcept="thresholds">
	<categoryValue>
		<categoryID>[...]</categoryID>
		<value>
			<real>[...]</real>
		</value>
	</categoryValue>
	[...]
</categoriesValues>



------------------------


.. _RORUTADIS-RepresentativeValueFunction-PUT-messages:

messages
~~~~~~~~

Messages generated by the program.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/RORUTADIS-RepresentativeValueFunction-PUT/description-wsDD.xml>`
