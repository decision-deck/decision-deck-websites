.. _ElectreConcordance-J-MCDA:

ElectreConcordance
==================

:Version: 0.5.5
:Provider: J-MCDA
:SOAP service's name: ``ElectreConcordance-J-MCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Computes a concordance relation.

**Contact:** Olivier Cailloux <olivier.cailloux@ecp.fr>

**Web page:** http://sourceforge.net/projects/j-mcda/

**Reference:** Cailloux, Olivier. Electre and Promethee MCDA methods as reusable software components. In Proceedings of the 25th Mini-EURO Conference on Uncertainty and Robustness in Planning and Decision Making (URPDM 2010). Coimbra, Portugal, 2010.


Inputs
------
(For outputs, see :ref:`below <ElectreConcordance-J-MCDA_outputs>`)


- :ref:`criteria <ElectreConcordance-J-MCDA-input1>`
- :ref:`alternatives <ElectreConcordance-J-MCDA-input0>` *(optional)*
- :ref:`criteriaScales <ElectreConcordance-J-MCDA-criteriaScales>`
- :ref:`criteriaThresholds <ElectreConcordance-J-MCDA-criteriaThresholds>` *(optional)*
- :ref:`performances <ElectreConcordance-J-MCDA-input3>`
- :ref:`weights <ElectreConcordance-J-MCDA-input2>`


.. _ElectreConcordance-J-MCDA-input1:

criteria
~~~~~~~~

The criteria to consider, possibly with preference and veto thresholds (provided separated via the input 'criteriaThresholds'). Each one must have a preference direction (see input 'criteriaScales'). Set some criteria as inactive (or remove them) to ignore them.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _ElectreConcordance-J-MCDA-input0:

alternatives *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~

The alternatives to consider. Set some alternatives as inactive (or remove them) to ignore them.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _ElectreConcordance-J-MCDA-criteriaScales:

criteriaScales
~~~~~~~~~~~~~~

The scales of the criteria to consider.

The input value should be a valid XMCDA document whose main tag is ``<criteriaScales>``.


------------------------


.. _ElectreConcordance-J-MCDA-criteriaThresholds:

criteriaThresholds *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The criteria' preference and indifference thresholds.

The input value should be a valid XMCDA document whose main tag is ``<criteriaThresholds>``.


------------------------


.. _ElectreConcordance-J-MCDA-input3:

performances
~~~~~~~~~~~~

The performances of the alternatives on the criteria to consider.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _ElectreConcordance-J-MCDA-input2:

weights
~~~~~~~

The weights of the criteria to consider.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.


------------------------



.. _ElectreConcordance-J-MCDA_outputs:

Outputs
-------


- :ref:`concordance <ElectreConcordance-J-MCDA-output0>`
- :ref:`messages <ElectreConcordance-J-MCDA-output1>`


.. _ElectreConcordance-J-MCDA-output0:

concordance
~~~~~~~~~~~

The concordance relation computed from the given input data.

The returned value is a XMCDA document whose main tag is ``<alternativesMatrix>``.


------------------------


.. _ElectreConcordance-J-MCDA-output1:

messages
~~~~~~~~

A status message.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/ElectreConcordance-J-MCDA/description-wsDD.xml>`
