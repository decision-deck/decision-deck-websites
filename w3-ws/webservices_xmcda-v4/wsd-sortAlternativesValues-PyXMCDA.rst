.. _sortAlternativesValues-PyXMCDA:

sortAlternativesValues
======================

:Version: 1.0
:Provider: PyXMCDA
:SOAP service's name: ``sortAlternativesValues-PyXMCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Sort alternatives into categories per values.
The categories are delimited by one interval of numeric values each.

**Contact:** Nicolas Duminy (nicolas.duminy@telecom-bretagne.eu)

**Web page:** https://gitlab.com/nduminy/ws-pyxmcda


Inputs
------
(For outputs, see :ref:`below <sortAlternativesValues-PyXMCDA_outputs>`)


- :ref:`alternatives <sortAlternativesValues-PyXMCDA-alternatives>` *(optional)*
- :ref:`categories <sortAlternativesValues-PyXMCDA-categories>` *(optional)*
- :ref:`alternativesValues <sortAlternativesValues-PyXMCDA-alternativesValues>`
- :ref:`categoriesLimits <sortAlternativesValues-PyXMCDA-categorieslimits>`


.. _sortAlternativesValues-PyXMCDA-alternatives:

alternatives *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~

The alternatives to assign (only the active ones). All are considered if not provided.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _sortAlternativesValues-PyXMCDA-categories:

categories *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~

The categories to assign to (only the active ones). All are considered if not provided.

The input value should be a valid XMCDA document whose main tag is ``<categories>``.


------------------------


.. _sortAlternativesValues-PyXMCDA-alternativesValues:

alternativesValues
~~~~~~~~~~~~~~~~~~

The alternatives values.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _sortAlternativesValues-PyXMCDA-categorieslimits:

categoriesLimits
~~~~~~~~~~~~~~~~

Categories limits represented by intervals of numeric values.

The input value should be a valid XMCDA document whose main tag is ``<categoriesValues>``.


------------------------



.. _sortAlternativesValues-PyXMCDA_outputs:

Outputs
-------


- :ref:`alternativesAssignments <sortAlternativesValues-PyXMCDA-alternativesAssignments>`
- :ref:`messages <sortAlternativesValues-PyXMCDA-messages>`


.. _sortAlternativesValues-PyXMCDA-alternativesAssignments:

alternativesAssignments
~~~~~~~~~~~~~~~~~~~~~~~

The computed alternatives assignments.

The returned value is a XMCDA document whose main tag is ``<alternativesAssignments>``.


------------------------


.. _sortAlternativesValues-PyXMCDA-messages:

messages
~~~~~~~~

Status messages.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/sortAlternativesValues-PyXMCDA/description-wsDD.xml>`
