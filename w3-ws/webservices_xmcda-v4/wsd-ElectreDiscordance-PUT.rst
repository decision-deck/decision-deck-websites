.. _ElectreDiscordance-PUT:

ElectreDiscordance
==================

:Version: 0.2.0
:Provider: PUT
:SOAP service's name: ``ElectreDiscordance-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

ElectreDiscordance - computes partial (i.e. per-criterion) discordance matrix using procedure which is common to the most methods from the Electre family.

The key feature of this module is its flexibility in terms of the types of elements allowed to compare, i.e. alternatives vs alternatives, alternatives vs boundary profiles and alternatives vs central (characteristic) profiles.

It also brings two new concepts: a 'counter-veto' threshold (cv) and 'pre-veto' threshold (pv) such as: cv >= v >= pv >= p (where 'v' is 'veto' threshold, and 'p' is 'preference' threshold).

**Web page:** http://github.com/xor-xor/electre_diviz


Inputs
------
(For outputs, see :ref:`below <ElectreDiscordance-PUT_outputs>`)


- :ref:`alternatives <ElectreDiscordance-PUT-input1>`
- :ref:`criteria <ElectreDiscordance-PUT-input3>`
- :ref:`criteriaScales <ElectreDiscordance-PUT-criteriaScales>`
- :ref:`criteriaThresholds <ElectreDiscordance-PUT-criteriaThresholds>` *(optional)*
- :ref:`performance_table <ElectreDiscordance-PUT-input4>`
- :ref:`profiles_performance_table <ElectreDiscordance-PUT-input5>` *(optional)*
- :ref:`classes_profiles <ElectreDiscordance-PUT-input2>` *(optional)*
- :ref:`method_parameters <ElectreDiscordance-PUT-input6>`


.. _ElectreDiscordance-PUT-input1:

alternatives
~~~~~~~~~~~~

Alternatives to consider.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _ElectreDiscordance-PUT-input3:

criteria
~~~~~~~~

Criteria to consider, possibly with preference, veto, pre-veto and counter-veto thresholds. Each criterion must have a preference direction specified (min or max). It is worth mentioning that this module allows to define thresholds as constants as well as linear functions.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _ElectreDiscordance-PUT-criteriaScales:

criteriaScales
~~~~~~~~~~~~~~

The scales of the criteria to consider.

The input value should be a valid XMCDA document whose main tag is ``<criteriaScales>``.


------------------------


.. _ElectreDiscordance-PUT-criteriaThresholds:

criteriaThresholds *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The criteria' preference and indifference thresholds.

The input value should be a valid XMCDA document whose main tag is ``<criteriaThresholds>``.


------------------------


.. _ElectreDiscordance-PUT-input4:

performance_table
~~~~~~~~~~~~~~~~~

The performance of alternatives.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _ElectreDiscordance-PUT-input5:

profiles_performance_table *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The performance of profiles (boundary or central).

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _ElectreDiscordance-PUT-input2:

classes_profiles *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Definitions of profiles (boundary or central) which should be used for classes (categories) representation.

The input value should be a valid XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _ElectreDiscordance-PUT-input6:

method_parameters
~~~~~~~~~~~~~~~~~

A set of parameters provided to tune up the module's operation.

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
<programParameters>
	<parameter name="comparison_with">
		<values>
			<value>
				<label>%1</label>
			</value>
		</values>
	</parameter>
	<parameter name="use_pre_veto">
		<values>
			<value>
				<boolean>%2</boolean>
			</value>
		</values>
	</parameter>
</programParameters>
      

where:

- **%1** is a parameter named "comparison_with". It can have the following values:

  - ``alternatives``: alternatives vs alternatives

  - ``boundary_profiles``: alternatives vs boundary profiles

  - ``central_profiles``: alternatives vs central (characteristic) profiles

  The default value is item0.

- **%2** is a parameter named "use_pre_veto". This is a boolean.
  The default value is false.


------------------------



.. _ElectreDiscordance-PUT_outputs:

Outputs
-------


- :ref:`discordance <ElectreDiscordance-PUT-output1>`
- :ref:`counter_veto_crossed <ElectreDiscordance-PUT-output2>`
- :ref:`messages <ElectreDiscordance-PUT-output3>`


.. _ElectreDiscordance-PUT-output1:

discordance
~~~~~~~~~~~

Partial (i.e. per-criterion) discordance indices computed from the given data.

The returned value is a XMCDA document whose main tag is ``<alternativesMatrix>``.


------------------------


.. _ElectreDiscordance-PUT-output2:

counter_veto_crossed
~~~~~~~~~~~~~~~~~~~~

This input contains information regarding the number of criteria per pair of the alternatives/profiles on which the 'counter-veto' threshold has been crossed. Please note that this output will be created even if 'counter-veto' threshold has not been defined - in such case, all the values will be set to 'false'.

The returned value is a XMCDA document whose main tag is ``<alternativesMatrix>``.


------------------------


.. _ElectreDiscordance-PUT-output3:

messages
~~~~~~~~

Messages or errors generated by this module.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/ElectreDiscordance-PUT/description-wsDD.xml>`
