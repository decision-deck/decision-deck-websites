.. _csvToXMCDA-criteriaSetsValues-PyXMCDA:

csvToXMCDA-criteriaSetsValues
=============================

:Version: 1.0
:Provider: PyXMCDA
:SOAP service's name: ``csvToXMCDA-criteriaSetsValues-PyXMCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Transforms a file containing criteria sets values from a comma-separated values (CSV) file to three XMCDA compliant files, containing the corresponding criteria/criteriaSets ids and their criteriaSetsValues.

**Contact:** Nicolas Duminy (nicolas.duminy@telecom-bretagne.eu)

**Web page:** https://gitlab.com/nduminy/ws-pyxmcda


Inputs
------
(For outputs, see :ref:`below <csvToXMCDA-criteriaSetsValues-PyXMCDA_outputs>`)


- :ref:`criteriaSetsValues.csv <csvToXMCDA-criteriaSetsValues-PyXMCDA-csv>`
- :ref:`parameters <csvToXMCDA-criteriaSetsValues-PyXMCDA-parameters>`


.. _csvToXMCDA-criteriaSetsValues-PyXMCDA-csv:

criteriaSetsValues.csv
~~~~~~~~~~~~~~~~~~~~~~

The criteria and their criteria values as a CSV file.  The first line is made of two cells, the first one being empty, and the second one will be the content of the attribute "mcdaConcept" in the tag "<criteriaValues>", if supplied.  The following lines are made of at least two cells, with the first cell being an criterion' id (and name, see below), and the remaining cells their associated values.

The CSV is formatted by pairs of lines, each describing a criteriaSetValues.

Example::

    set1 (performance),1.8,19
    ,g1,g2 (time),g3

The first line contains the criteriaSet id on the first cell. Additionally, the criteriaSet name is also extracted when the cell is formatted like `id (name)`.  Set the parameter "First column" to "id" to deactivate the extraction of criteriaSet name.

The following cells on the first line is the enumeration of all values associated to this criteriaSet.
By default the values are considered as float numbers.  This can be changed using the parameter "Default content".  It is possible to specify the type of a value by prepending it with a prefix:

- `float:` for floats (ex.: `1`, `1.2`, `1.2e3`)
- `integer:` for integers (decimal representation: `127`, hexadecimal: `0x7f`, octal: `0o177`, binary: `0b1111111`)
- `string:` for strings (note that a string with a colon should always be prefixed by `string:`, no matter what the default prefix is).
- `boolean:` for booleans: 1 or 'true' (case insensitive) are True values, everything else is false.
- `na:` for N/A (everything after the colon is ignored)

The second line contains the criteria' ids. Additionally, the criteria' names are also extracted when the cells are formatted like `id (name)`.  Set the parameter "First column" to "id" to deactivate the extraction of criteria' names.

Complete example::

    performance,1.8
    ,g1,g2,g3
    safety,3.8,s:Perfect,b:false
    ,g3,g4 (maintenance),g5 (risk)

The input value should be a valid XMCDA document whose main tag is ``<other>``.


------------------------


.. _csvToXMCDA-criteriaSetsValues-PyXMCDA-parameters:

parameters
~~~~~~~~~~

Parameters of the method

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
    <programParameters>
        <programParameter id="csv_delimiter">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="default_prefix">
            <values>
                <value>
                    <label>%2</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="name_in_id">
            <values>
                <value>
                    <boolean>%3</boolean>
                </value>
            </values>
        </programParameter>
    </programParameters>


where:

- **%1** is a parameter named "CSV delimiter". This is a string, and the value should conform to the following constraint: One character maximum.  More formally, the constraint is::

     %1.length() < 2
- **%2** is a parameter named "Default content". It can have the following values:

  - ``float``: float

  - ``label``: string

  - ``integer``: integer

  - ``boolean``: boolean

  The default value is float.

- **%3** is a parameter named "First column". It can have the following values:

  - ``false``: id

  - ``true``: id (name)

  The default value is id_and_name.


------------------------



.. _csvToXMCDA-criteriaSetsValues-PyXMCDA_outputs:

Outputs
-------


- :ref:`criteria <csvToXMCDA-criteriaSetsValues-PyXMCDA-criteria>`
- :ref:`criteriaSets <csvToXMCDA-criteriaSetsValues-PyXMCDA-criteriaSets>`
- :ref:`criteriaSetsValues <csvToXMCDA-criteriaSetsValues-PyXMCDA-criteriaSetsValues>`
- :ref:`messages <csvToXMCDA-criteriaSetsValues-PyXMCDA-messages>`


.. _csvToXMCDA-criteriaSetsValues-PyXMCDA-criteria:

criteria
~~~~~~~~

The contained criteria.

The returned value is a XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _csvToXMCDA-criteriaSetsValues-PyXMCDA-criteriaSets:

criteriaSets
~~~~~~~~~~~~

The contained criteria sets.

The returned value is a XMCDA document whose main tag is ``<criteriaSets>``.


------------------------


.. _csvToXMCDA-criteriaSetsValues-PyXMCDA-criteriaSetsValues:

criteriaSetsValues
~~~~~~~~~~~~~~~~~~

The equivalent criteria sets values.

The returned value is a XMCDA document whose main tag is ``<criteriaSetsValues>``.


------------------------


.. _csvToXMCDA-criteriaSetsValues-PyXMCDA-messages:

messages
~~~~~~~~

Status messages.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/csvToXMCDA-criteriaSetsValues-PyXMCDA/description-wsDD.xml>`
