.. _SRMP_ranking-R-MCDA:

SRMP_ranking
============

:Version: 1.0
:Provider: R-MCDA
:SOAP service's name: ``SRMP_ranking-R-MCDA`` (see :ref:`soap-requests` for details)

Description
-----------

SRMP is a ranking method that uses dominating reference profiles, in a given lexicographic ordering, in order to output a total preorder of a set of alternatives.

**Contact:** Alexandru Olteanu (alexandru.olteanu@univ-ubs.fr)

**Reference:** A. Rolland. Procédures d’agrégation ordinale de préférences avec points de référence pour l’aide a la décision. PhD thesis, Université Paris VI, 2008.


Inputs
------
(For outputs, see :ref:`below <SRMP_ranking-R-MCDA_outputs>`)


- :ref:`alternatives <SRMP_ranking-R-MCDA-inalt>`
- :ref:`referenceProfiles <SRMP_ranking-R-MCDA-inprof>`
- :ref:`performanceTable <SRMP_ranking-R-MCDA-inperf>`
- :ref:`referenceProfilesPerformanceTable <SRMP_ranking-R-MCDA-inprofpt>`
- :ref:`criteria <SRMP_ranking-R-MCDA-incrit>`
- :ref:`criteriaWeights <SRMP_ranking-R-MCDA-weights>`
- :ref:`lexicographicOrder <SRMP_ranking-R-MCDA-inprofval>`


.. _SRMP_ranking-R-MCDA-inalt:

alternatives
~~~~~~~~~~~~

A complete list of alternatives to be considered by the method.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
			


------------------------


.. _SRMP_ranking-R-MCDA-inprof:

referenceProfiles
~~~~~~~~~~~~~~~~~

A list of the reference profiles.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _SRMP_ranking-R-MCDA-inperf:

performanceTable
~~~~~~~~~~~~~~~~

The evaluations of the alternatives on the set of criteria.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _SRMP_ranking-R-MCDA-inprofpt:

referenceProfilesPerformanceTable
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The evaluations of the category profiles.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _SRMP_ranking-R-MCDA-incrit:

criteria
~~~~~~~~

A list of criteria on which the alternatives are evaluated. For each criterion, the preference direction should be provided.

The input value should be a valid XMCDA document whose main tag is ``<criteriaScales>``.


------------------------


.. _SRMP_ranking-R-MCDA-weights:

criteriaWeights
~~~~~~~~~~~~~~~

The criteria weights.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _SRMP_ranking-R-MCDA-inprofval:

lexicographicOrder
~~~~~~~~~~~~~~~~~~

A list of the order of the reference profiles, 1 being for the first profile that will be used when comparing two alternatives together.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------



.. _SRMP_ranking-R-MCDA_outputs:

Outputs
-------


- :ref:`alternativesValues <SRMP_ranking-R-MCDA-outvalues>`
- :ref:`messages <SRMP_ranking-R-MCDA-msg>`


.. _SRMP_ranking-R-MCDA-outvalues:

alternativesValues
~~~~~~~~~~~~~~~~~~

The order of the alternatives given in ranks, or values, 1 being the top alternative.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _SRMP_ranking-R-MCDA-msg:

messages
~~~~~~~~

Messages from the execution of the webservice. Possible errors in the input data will be given here.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/SRMP_ranking-R-MCDA/description-wsDD.xml>`
