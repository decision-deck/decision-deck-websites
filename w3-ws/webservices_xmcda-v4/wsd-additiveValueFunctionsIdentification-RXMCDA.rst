.. _additiveValueFunctionsIdentification-RXMCDA:

additiveValueFunctionsIdentification
====================================

:Version: 1.1
:Provider: RXMCDA
:SOAP service's name: ``additiveValueFunctionsIdentification-RXMCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Identifies an set of piecewise linear additive value functions according to a ranking of the alternatives. If the number of segments is not given, a general additive value function is looked for.

**Contact:** Helene Schmitz and Patrick Meyer (patrick.meyer@telecom-bretagne.eu)

**Web page:** None


Inputs
------
(For outputs, see :ref:`below <additiveValueFunctionsIdentification-RXMCDA_outputs>`)


- :ref:`criteria <additiveValueFunctionsIdentification-RXMCDA-criteria>`
- :ref:`alternatives <additiveValueFunctionsIdentification-RXMCDA-alternatives>`
- :ref:`performanceTable <additiveValueFunctionsIdentification-RXMCDA-performanceTable>`
- :ref:`alternativesRanks <additiveValueFunctionsIdentification-RXMCDA-alternativesRanks>`
- :ref:`separationThreshold <additiveValueFunctionsIdentification-RXMCDA-separationThreshold>`
- :ref:`segments <additiveValueFunctionsIdentification-RXMCDA-segments>` *(optional)*


.. _additiveValueFunctionsIdentification-RXMCDA-criteria:

criteria
~~~~~~~~

A list of criteria. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
	<criteria>
		<criterion>
			<active>[...]</active>
			[...]
		</criterion>
		[...]
	</criteria>



------------------------


.. _additiveValueFunctionsIdentification-RXMCDA-alternatives:

alternatives
~~~~~~~~~~~~

A list of alternatives. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
	<alternatives>
		<alternative id="..." [...]>
			<active>[...]</active>
		</alternative>
		[...]
	</alternatives>



------------------------


.. _additiveValueFunctionsIdentification-RXMCDA-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A performance table. The evaluations should be numeric values, i.e. <real>, <integer> or <rational>.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _additiveValueFunctionsIdentification-RXMCDA-alternativesRanks:

alternativesRanks
~~~~~~~~~~~~~~~~~

The ranking of the alternatives, the best alternative having the lowest rank.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _additiveValueFunctionsIdentification-RXMCDA-separationThreshold:

separationThreshold
~~~~~~~~~~~~~~~~~~~

Threshold value indicating the minimal difference in terms of the overall value between two neighbor alternatives in the given ranking.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
	<programParameters>
		<parameter name="separationThreshold"> <!-- attribute 'name' REQUIRED  -->
			<values>
				<value>
					<real>%1</real>
				</value>
			</values>
		</parameter>
	</programParameters>


where:

- **%1** is a parameter named "separationThreshold". This is a float, and the value should conform to the following constraint: The value should be a strictly positive float, less than the highest possible overall value..  More formally, the constraint is::

      %1 > 0 

------------------------


.. _additiveValueFunctionsIdentification-RXMCDA-segments:

segments *(optional)*
~~~~~~~~~~~~~~~~~~~~~

The number of segments for the additive value functions. If it is not given, then a general additive value function is searched for.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
		<parameter name="criteriaSegments"> <!-- attribute 'name' REQUIRED  -->
			<values>
				<value>
					<integer>%1</integer>
			</values>
		</parameter>
	</programParameters>


where:

- **%1** is a parameter named "numberOfSegments". This is a int, and the value should conform to the following constraint: The value should be a strictly positive integer..  More formally, the constraint is::

      %1 > 0 
  The default value is 1.


------------------------



.. _additiveValueFunctionsIdentification-RXMCDA_outputs:

Outputs
-------


- :ref:`valueFunctions <additiveValueFunctionsIdentification-RXMCDA-valueFunctions>`
- :ref:`messages <additiveValueFunctionsIdentification-RXMCDA-messages>`


.. _additiveValueFunctionsIdentification-RXMCDA-valueFunctions:

valueFunctions
~~~~~~~~~~~~~~

The value functions of the selected criteria.

The returned value is a XMCDA document whose main tag is ``<criteria>``.
It has the following form::

   
	<criteriaFunctions>
		<criterionFunction>
			<criterionID>g1</criterionID>
			<functions>
				<function>
					<discrete>
						<point>
							<abscissa><real>[...]</real></abscissa>
							<ordinate><real>[...]</real></ordinate>
						</point>
						[...]
					</discrete>
				</function>
			</functions>
		</criterionFunction>
	</criteriaFunctions>



------------------------


.. _additiveValueFunctionsIdentification-RXMCDA-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/additiveValueFunctionsIdentification-RXMCDA/description-wsDD.xml>`
