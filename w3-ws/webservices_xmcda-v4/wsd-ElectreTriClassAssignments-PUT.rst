.. _ElectreTriClassAssignments-PUT:

ElectreTriClassAssignments
==========================

:Version: 0.2.0
:Provider: PUT
:SOAP service's name: ``ElectreTriClassAssignments-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

ElectreTriClassAssignments - computes assignments according to the Electre TRI method. It generates separate outputs for the conjuctive ('pessimistic') and disjunctive ('optimistic') assignments.

**Web page:** http://github.com/xor-xor/electre_diviz


Inputs
------
(For outputs, see :ref:`below <ElectreTriClassAssignments-PUT_outputs>`)


- :ref:`alternatives <ElectreTriClassAssignments-PUT-input1>`
- :ref:`classes <ElectreTriClassAssignments-PUT-input2>`
- :ref:`outranking <ElectreTriClassAssignments-PUT-input4>`
- :ref:`classes_profiles <ElectreTriClassAssignments-PUT-input3>`


.. _ElectreTriClassAssignments-PUT-input1:

alternatives
~~~~~~~~~~~~

Alternatives to consider.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _ElectreTriClassAssignments-PUT-input2:

classes
~~~~~~~

Definitions of the classes (categories) to be considered for the assignments. Each class must have its rank provided as an integer, where the lowest number defines the most preferred class.

The input value should be a valid XMCDA document whose main tag is ``<categories>``.


------------------------


.. _ElectreTriClassAssignments-PUT-input4:

outranking
~~~~~~~~~~

Outranking relations for pairs of alternatives and boundary profiles, which should be used in exploitation procedure. This input should be provided in a 'crisp' form, i.e. it should contain only pairs where outranking occurs, but without assigning any explicit value (e.g. '1.0') to them. For example:

	<alternativesMatrix mcdaConcept="alternativesProfilesComparisons">
		<row>
			<alternativeID>a01</alternativeID>
			<column>
				<alternativeID>a05</alternativeID>
				<values><value><NA/></value></values>
			</column>
			<column>
				<alternativeID>a06</alternativeID>
				<values><value><NA/></value></values>
			</column>
			[...]
		</row>
		[...]
	</alternativesMatrix>

The input value should be a valid XMCDA document whose main tag is ``<alternativesMatrix>``.


------------------------


.. _ElectreTriClassAssignments-PUT-input3:

classes_profiles
~~~~~~~~~~~~~~~~

Definitions of the boundary actions (profiles).

The input value should be a valid XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------



.. _ElectreTriClassAssignments-PUT_outputs:

Outputs
-------


- :ref:`assignments_conjuctive <ElectreTriClassAssignments-PUT-output1>`
- :ref:`assignments_disjunctive <ElectreTriClassAssignments-PUT-output2>`
- :ref:`messages <ElectreTriClassAssignments-PUT-output3>`


.. _ElectreTriClassAssignments-PUT-output1:

assignments_conjuctive
~~~~~~~~~~~~~~~~~~~~~~

Resulting class assignments in the conjuctive ('pessimistic') order.

The returned value is a XMCDA document whose main tag is ``<alternativesAssignments>``.


------------------------


.. _ElectreTriClassAssignments-PUT-output2:

assignments_disjunctive
~~~~~~~~~~~~~~~~~~~~~~~

Resulting class assignments in the disjunctive ('optimistic') order.

The returned value is a XMCDA document whose main tag is ``<alternativesAssignments>``.


------------------------


.. _ElectreTriClassAssignments-PUT-output3:

messages
~~~~~~~~

Messages or errors generated by this module.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/ElectreTriClassAssignments-PUT/description-wsDD.xml>`
