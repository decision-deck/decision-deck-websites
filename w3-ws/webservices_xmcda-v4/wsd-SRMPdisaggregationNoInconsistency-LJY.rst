.. _SRMPdisaggregationNoInconsistency-LJY:

S-RMP Disaggregation without inconsistency
==========================================

:Version: 1.2
:Provider: LJY
:Name: SRMPdisaggregationNoInconsistency
:SOAP service's name: ``SRMPdisaggregationNoInconsistency-LJY`` (see :ref:`soap-requests` for details)

Description
-----------

Elicit a S-RMP model from a set of pairwise comparisons without inconsistency. The algorithm will find out a S-RMP model which is able to exactly represent all the input pairwise comparisons (if it exists). The elicitation may be infeasible due to the unknown input inconsistency.

**Contact:** Jinyan Liu (jinyan.liu@ecp.fr)

**Web page:** http://www.lgi.ecp.fr/pmwiki.php/PagesPerso/JinyanLiu

**Reference:** Zheng, J., Rolland, A. & Mousseau, V., 2012. Inferring a reference based multicriteria ranking model from pairwise comparisons. European Journal of Operational Research.

**Reference:** Jun Zheng. Preference elicitation for aggregation models based on reference points: algorithms and procedures. These PhD. May, 2012.


Inputs
------
(For outputs, see :ref:`below <SRMPdisaggregationNoInconsistency-LJY_outputs>`)


- :ref:`criteria <SRMPdisaggregationNoInconsistency-LJY-inc>`
- :ref:`alternatives <SRMPdisaggregationNoInconsistency-LJY-ina>`
- :ref:`performanceTable <SRMPdisaggregationNoInconsistency-LJY-inperf>`
- :ref:`alternativesComparisons <SRMPdisaggregationNoInconsistency-LJY-inac>`


.. _SRMPdisaggregationNoInconsistency-LJY-inc:

criteria
~~~~~~~~

A list of criteria involved in the ranking problem. For each criterion, the preference direction should be provided. Besides, the maximum and minimum should also be included.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _SRMPdisaggregationNoInconsistency-LJY-ina:

alternatives
~~~~~~~~~~~~

A complete list of alternatives to be considered in the ranking problem. At least, all the alternatives appear in the provided pairwise comparisons should be included.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
			


------------------------


.. _SRMPdisaggregationNoInconsistency-LJY-inperf:

performanceTable
~~~~~~~~~~~~~~~~

Values of criteria for each alternatives defined in the alternatives list.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _SRMPdisaggregationNoInconsistency-LJY-inac:

alternativesComparisons
~~~~~~~~~~~~~~~~~~~~~~~

The pairwise comparisons of alternatives provided by the decision maker.

The input value should be a valid XMCDA document whose main tag is ``<alternativesComparisons>``.


------------------------



.. _SRMPdisaggregationNoInconsistency-LJY_outputs:

Outputs
-------


- :ref:`profileConfigs <SRMPdisaggregationNoInconsistency-LJY-outrp>`
- :ref:`weights <SRMPdisaggregationNoInconsistency-LJY-outw>`
- :ref:`lexicography <SRMPdisaggregationNoInconsistency-LJY-outlexi>`
- :ref:`messages <SRMPdisaggregationNoInconsistency-LJY-msg>`


.. _SRMPdisaggregationNoInconsistency-LJY-outrp:

profileConfigs
~~~~~~~~~~~~~~

A set of reference points (special alternatives) with their performance values on each criteria. For each reference point, the id number indicates its lexicographic order.

The returned value is a XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _SRMPdisaggregationNoInconsistency-LJY-outw:

weights
~~~~~~~

The weights of criteria. For each criteria, the value of weight is strictly positive and less than 0.49. The sum of weights is normalized to 1.

The returned value is a XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _SRMPdisaggregationNoInconsistency-LJY-outlexi:

lexicography
~~~~~~~~~~~~

the lexicographic order of the reference points we use during aggregation.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _SRMPdisaggregationNoInconsistency-LJY-msg:

messages
~~~~~~~~

Some status messages.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/SRMPdisaggregationNoInconsistency-LJY/description-wsDD.xml>`
