.. _SRMPaggregation-LJY:

S-RMP Aggregation
=================

:Version: 1.2
:Provider: LJY
:Name: SRMPaggregation
:SOAP service's name: ``SRMPaggregation-LJY`` (see :ref:`soap-requests` for details)

Description
-----------

Execute the S-RMP model based aggregation process for a series of alternatives with a given set of model parameters and create the ranking list of alternatives.

**Contact:** Jinyan Liu (jinyan.liu@ecp.fr)

**Web page:** http://www.lgi.ecp.fr/pmwiki.php/PagesPerso/JinyanLiu

**Reference:** Antoine Rolland. Reference-based preferences aggregation procedures in multi-criteria decision making. European Journal of Operational Research, pages 479–486, 2013.
		

**Reference:** Jun Zheng. Preference elicitation for aggregation models based on reference points: algorithms and procedures. These PhD. May, 2012.


Inputs
------
(For outputs, see :ref:`below <SRMPaggregation-LJY_outputs>`)


- :ref:`criteria <SRMPaggregation-LJY-inc>`
- :ref:`alternatives <SRMPaggregation-LJY-ina>`
- :ref:`performanceTable <SRMPaggregation-LJY-inperf>`
- :ref:`profileConfigs <SRMPaggregation-LJY-inrp>`
- :ref:`weights <SRMPaggregation-LJY-inw>`
- :ref:`lexicography <SRMPaggregation-LJY-inlexi>`


.. _SRMPaggregation-LJY-inc:

criteria
~~~~~~~~

A list of criteria involved in the ranking problem. For each criterion, the preference direction should be provided. Besides, the maximum and minimum should also be included.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   <criteria> <criterion id="[...]" name="[...]"> </criterion> </criteria>


------------------------


.. _SRMPaggregation-LJY-ina:

alternatives
~~~~~~~~~~~~

A complete list of alternatives to be considered in the ranking problem.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
				<alternatives> <alternative id="[...]" name="[...]"/> </alternatives>
			


------------------------


.. _SRMPaggregation-LJY-inperf:

performanceTable
~~~~~~~~~~~~~~~~

Values of criteria for each alternatives defined in the alternatives list.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _SRMPaggregation-LJY-inrp:

profileConfigs
~~~~~~~~~~~~~~

A set of reference points (special alternatives) with their performance values on each criteria.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _SRMPaggregation-LJY-inw:

weights
~~~~~~~

The weights of criteria. For each criteria, the value of weight should strictly positive. The sum of all criteria weights should be normalized to 1.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _SRMPaggregation-LJY-inlexi:

lexicography
~~~~~~~~~~~~

the lexicographic order of the reference points we use during aggregation.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------



.. _SRMPaggregation-LJY_outputs:

Outputs
-------


- :ref:`preorder <SRMPaggregation-LJY-out>`
- :ref:`messages <SRMPaggregation-LJY-msg>`


.. _SRMPaggregation-LJY-out:

preorder
~~~~~~~~

A ranking list of alternatives.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   

			


------------------------


.. _SRMPaggregation-LJY-msg:

messages
~~~~~~~~

Some status messages.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/SRMPaggregation-LJY/description-wsDD.xml>`
