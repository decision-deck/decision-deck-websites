.. _DEACCREfficiencyBounds-PUT:

DEACCREfficiencyBounds
======================

:Version: 1.0
:Provider: PUT
:SOAP service's name: ``DEACCREfficiencyBounds-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Computes efficiency bounds scores for the given DMUs (alternatives) using CCR Data Envelopment Analysis Model.

**Contact:** Malgorzata Napieraj (napieraj.malgorzata@gmail.com)

**Reference:** Cooper W., Seiford L., Tone K., Data Envelopment Analysis: A Comprehensive Text with Models, Applications, References and DEA-Solver (2007).


Inputs
------
(For outputs, see :ref:`below <DEACCREfficiencyBounds-PUT_outputs>`)


- :ref:`inputsOutputs <DEACCREfficiencyBounds-PUT-inputsOutputs>`
- :ref:`units <DEACCREfficiencyBounds-PUT-units>`
- :ref:`performanceTable <DEACCREfficiencyBounds-PUT-performanceTable>`
- :ref:`weightsLinearConstraints <DEACCREfficiencyBounds-PUT-weightsLinearConstraints>` *(optional)*
- :ref:`methodParameters <DEACCREfficiencyBounds-PUT-methodParameters>`


.. _DEACCREfficiencyBounds-PUT-inputsOutputs:

inputsOutputs
~~~~~~~~~~~~~

A list of criteria with specified preference direction. List has to contains at least one criterion that will be minimized (input) and at least one criterion that will be maximized (output).

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   <criteria>
                        <criterion>
							<scale>
								<quantitative>
									<preferenceDirection>
										[...]
									</preferenceDirection>
								</quantitative>
							</scale>
                            [...]
                        </criterion>
                        [...]
                    </criteria>


------------------------


.. _DEACCREfficiencyBounds-PUT-units:

units
~~~~~

A list of alternatives (DMUs).

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   <alternatives>
                        <alternative>
                            [...]
                        </alternative>
                        [...]
                    </alternatives>


------------------------


.. _DEACCREfficiencyBounds-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

A list of alternatives (DMUs) performances.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   <performanceTable>
						<alternativePerformances>
							<alternativeID>[...]</alternativeID>
							<performance>
								<criterionID>
									[...]
								</criterionID>
								<value>[...]</value>
							</performance>
							[...]
						</alternativePerformances>
						[...]
					</performanceTable>


------------------------


.. _DEACCREfficiencyBounds-PUT-weightsLinearConstraints:

weightsLinearConstraints *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of criteria linear constraints.

The input value should be a valid XMCDA document whose main tag is ``<criteriaLinearConstraints>``.
It must have the following form::

   <criteriaLinearConstraints>
						<constraint>
							<element>
								<criterionID>[...]</criterionID>
								<coefficient>
									[...]
								</coefficient>
							</element>
							[...]
							<operator>[...]</operator>
							<rhs>
								[...]
							</rhs>
						</constraint>
						[...]
					</criteriaLinearConstraints>


------------------------


.. _DEACCREfficiencyBounds-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~

Determines if the subject alternative should be in the relative group.

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   <methodParameters>
							<parameter name="includeSubject">
								<value><boolean>%1</boolean></value>
							</parameter>
					</methodParameters>

where:

- **%1** is a parameter named "includeSubject". This is a boolean.
  The default value is false.


------------------------



.. _DEACCREfficiencyBounds-PUT_outputs:

Outputs
-------


- :ref:`minEfficiencyOverLeastEfficient <DEACCREfficiencyBounds-PUT-minEfficiencyOverLeastEfficient>`
- :ref:`maxEfficiencyOverLeastEfficient <DEACCREfficiencyBounds-PUT-maxEfficiencyOverLeastEfficient>`
- :ref:`minEfficiencyOverMostEfficient <DEACCREfficiencyBounds-PUT-minEfficiencyOverMostEfficient>`
- :ref:`maxEfficiencyOverMostEfficient <DEACCREfficiencyBounds-PUT-maxEfficiencyOverMostEfficient>`
- :ref:`messages <DEACCREfficiencyBounds-PUT-messages>`


.. _DEACCREfficiencyBounds-PUT-minEfficiencyOverLeastEfficient:

minEfficiencyOverLeastEfficient
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of alternatives with computed minimum efficiency scores relative to the least efficient alternative in the group.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   <alternativesValues>
						<alternativeValue>
						  <alternativeID>[...]</alternativeID>
						  <value>
							[...]
						  </value>
						</alternativeValue>
						[...]
					</alternativesValues>


------------------------


.. _DEACCREfficiencyBounds-PUT-maxEfficiencyOverLeastEfficient:

maxEfficiencyOverLeastEfficient
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of alternatives with computed maximum efficiency scores relative to the least efficient alternative in the group.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   <alternativesValues>
						<alternativeValue>
						  <alternativeID>[...]</alternativeID>
						  <value>
							[...]
						  </value>
						</alternativeValue>
						[...]
					</alternativesValues>


------------------------


.. _DEACCREfficiencyBounds-PUT-minEfficiencyOverMostEfficient:

minEfficiencyOverMostEfficient
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of alternatives with computed minimum efficiency scores relative to the most efficient alternative in the group.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   <alternativesValues>
						<alternativeValue>
						  <alternativeID>[...]</alternativeID>
						  <value>
							[...]
						  </value>
						</alternativeValue>
						[...]
					</alternativesValues>


------------------------


.. _DEACCREfficiencyBounds-PUT-maxEfficiencyOverMostEfficient:

maxEfficiencyOverMostEfficient
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of alternatives with computed maximum efficiency scores relative to the most efficient alternative in the group.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   <alternativesValues>
						<alternativeValue>
						  <alternativeID>[...]</alternativeID>
						  <value>
							[...]
						  </value>
						</alternativeValue>
						[...]
					</alternativesValues>


------------------------


.. _DEACCREfficiencyBounds-PUT-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/DEACCREfficiencyBounds-PUT/description-wsDD.xml>`
