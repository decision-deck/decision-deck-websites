.. _MRSortInferenceApprox-R-MCDA:

Approximative inference of MRSort
=================================

:Version: 1.0
:Provider: R-MCDA
:Name: MRSortInferenceApprox
:SOAP service's name: ``MRSortInferenceApprox-R-MCDA`` (see :ref:`soap-requests` for details)

Description
-----------

MRSort is a simplification of the Electre TRI method that uses the pessimistic assignment rule, without indifference or preference thresholds attached to criteria. Only a binary discordance condition is considered, i.e. a veto forbids an outranking in any possible concordance situation, or not. The identification of the profiles, weights, majority threshold and veto thresholds are done by taking into account assignment examples.

**Contact:** Alexandru Olteanu (al.olteanu@telecom-bretagne.eu)


Inputs
------
(For outputs, see :ref:`below <MRSortInferenceApprox-R-MCDA_outputs>`)


- :ref:`alternatives <MRSortInferenceApprox-R-MCDA-inalt>`
- :ref:`criteria <MRSortInferenceApprox-R-MCDA-incrit>`
- :ref:`categoriesRanks <MRSortInferenceApprox-R-MCDA-incategval>`
- :ref:`performanceTable <MRSortInferenceApprox-R-MCDA-inperf>`
- :ref:`alternativesAssignments <MRSortInferenceApprox-R-MCDA-assignments>`
- :ref:`veto <MRSortInferenceApprox-R-MCDA-veto>` *(optional)*
- :ref:`time <MRSortInferenceApprox-R-MCDA-time>` *(optional)*
- :ref:`population <MRSortInferenceApprox-R-MCDA-population>` *(optional)*
- :ref:`mutation <MRSortInferenceApprox-R-MCDA-mutation>` *(optional)*


.. _MRSortInferenceApprox-R-MCDA-inalt:

alternatives
~~~~~~~~~~~~

A complete list of alternatives to be considered when inferring the MR-Sort model.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
			


------------------------


.. _MRSortInferenceApprox-R-MCDA-incrit:

criteria
~~~~~~~~

A list of criteria on which the alternatives are evaluated. For each criterion, the preference direction should be provided.

The input value should be a valid XMCDA document whose main tag is ``<criteriaScales>``.


------------------------


.. _MRSortInferenceApprox-R-MCDA-incategval:

categoriesRanks
~~~~~~~~~~~~~~~

A list of categories ranks, 1 stands for the most preferred category and the higher the number the lower the preference for that category.

The input value should be a valid XMCDA document whose main tag is ``<categoriesValues>``.


------------------------


.. _MRSortInferenceApprox-R-MCDA-inperf:

performanceTable
~~~~~~~~~~~~~~~~

The evaluations of the alternatives on the set of criteria.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _MRSortInferenceApprox-R-MCDA-assignments:

alternativesAssignments
~~~~~~~~~~~~~~~~~~~~~~~

The alternatives assignments to categories.

The input value should be a valid XMCDA document whose main tag is ``<alternativesAssignments>``.


------------------------


.. _MRSortInferenceApprox-R-MCDA-veto:

veto *(optional)*
~~~~~~~~~~~~~~~~~

An indicator for whether vetoes should be included in the model or not.

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.

where:

- **%1** is a parameter named "Include vetoes". This is a boolean.

------------------------


.. _MRSortInferenceApprox-R-MCDA-time:

time *(optional)*
~~~~~~~~~~~~~~~~~

The execution time limit in seconds.

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.

where:

- **%1** is a parameter named "Time limit". This is a int, and the value should conform to the following constraint: An integer value (minimum 1).  More formally, the constraint is::

      %1 > 0 

------------------------


.. _MRSortInferenceApprox-R-MCDA-population:

population *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~

The algorithm population size.

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.

where:

- **%1** is a parameter named "Population size". This is a int, and the value should conform to the following constraint: An integer value (minimum 10).  More formally, the constraint is::

      %1 >= 10 

------------------------


.. _MRSortInferenceApprox-R-MCDA-mutation:

mutation *(optional)*
~~~~~~~~~~~~~~~~~~~~~

The algorithm mutation probability.

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.

where:

- **%1** is a parameter named "Mutation probability". This is a float, and the value should conform to the following constraint: A value between 0 and 1.  More formally, the constraint is::

      %1 >= 0 & %1 <= 1 

------------------------



.. _MRSortInferenceApprox-R-MCDA_outputs:

Outputs
-------


- :ref:`fitness <MRSortInferenceApprox-R-MCDA-fitness>`
- :ref:`majorityThreshold <MRSortInferenceApprox-R-MCDA-majority>`
- :ref:`criteriaWeights <MRSortInferenceApprox-R-MCDA-weights>`
- :ref:`categoriesProfiles <MRSortInferenceApprox-R-MCDA-outcatprof>`
- :ref:`categoriesProfilesPerformanceTable <MRSortInferenceApprox-R-MCDA-outcatprofpt>`
- :ref:`categoriesVetoes <MRSortInferenceApprox-R-MCDA-outcatveto>`
- :ref:`categoriesVetoesPerformanceTable <MRSortInferenceApprox-R-MCDA-outcatvetopt>`
- :ref:`messages <MRSortInferenceApprox-R-MCDA-msg>`


.. _MRSortInferenceApprox-R-MCDA-fitness:

fitness
~~~~~~~

The model fitness.

The returned value is a XMCDA document whose main tag is ``<programParameters>``.


------------------------


.. _MRSortInferenceApprox-R-MCDA-majority:

majorityThreshold
~~~~~~~~~~~~~~~~~

The majority threshold.

The returned value is a XMCDA document whose main tag is ``<programParameters>``.


------------------------


.. _MRSortInferenceApprox-R-MCDA-weights:

criteriaWeights
~~~~~~~~~~~~~~~

The criteria weights.

The returned value is a XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _MRSortInferenceApprox-R-MCDA-outcatprof:

categoriesProfiles
~~~~~~~~~~~~~~~~~~

The categories delimiting profiles.

The returned value is a XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _MRSortInferenceApprox-R-MCDA-outcatprofpt:

categoriesProfilesPerformanceTable
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The evaluations of the category profiles.

The returned value is a XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _MRSortInferenceApprox-R-MCDA-outcatveto:

categoriesVetoes
~~~~~~~~~~~~~~~~

The categories veto profiles.

The returned value is a XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _MRSortInferenceApprox-R-MCDA-outcatvetopt:

categoriesVetoesPerformanceTable
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The evaluations of the veto profiles.

The returned value is a XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _MRSortInferenceApprox-R-MCDA-msg:

messages
~~~~~~~~

Messages from the execution of the webservice. Possible errors in the input data will be given here.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/MRSortInferenceApprox-R-MCDA/description-wsDD.xml>`
