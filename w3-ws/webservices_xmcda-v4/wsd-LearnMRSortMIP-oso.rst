.. _LearnMRSortMIP-oso:

LearnMRSortMIP
==============

:Version: 1.0
:Provider: oso
:SOAP service's name: ``LearnMRSortMIP-oso`` (see :ref:`soap-requests` for details)

Description
-----------

The purpose of this webservice is to infer parameters of an ELECTRE TRI Bouyssou-Marchant model. It takes as input a set of learning alternatives evaluated on the criteria and their affectations to the different categories of the model. It is possible to perform a global inference of the parameters of the model (i.e. inferring the profiles, the weights and the credibility threshold) or to perform partial inference (i.e. only inferring the weights and the credibility threshold or only inferring the profiles delimiting the categories). The webservice will try to find a model that maximize the number of learning alternatives compatible with it. It means that it accepts in input an inconsistent set of learning alternatives that cannot be represented by an ELECTRE TRI model.

**Contact:** Olivier Sobrie (olivier.sobrie@gmail.com)

**Reference:** Agnès Leroy: Apprentissage des paramètres d'une mÃ©thode multicritère de tri ordonné


Inputs
------
(For outputs, see :ref:`below <LearnMRSortMIP-oso_outputs>`)


- :ref:`criteria <LearnMRSortMIP-oso-criteria>`
- :ref:`alternatives <LearnMRSortMIP-oso-alternatives>`
- :ref:`categories <LearnMRSortMIP-oso-categories>`
- :ref:`perfs_table <LearnMRSortMIP-oso-perfs_table>`
- :ref:`reference_alts <LearnMRSortMIP-oso-reference_alts>` *(optional)*
- :ref:`crit_weights <LearnMRSortMIP-oso-crit_weights>` *(optional)*
- :ref:`assign <LearnMRSortMIP-oso-assign>`
- :ref:`cat_profiles <LearnMRSortMIP-oso-cat_profiles>` *(optional)*
- :ref:`lambda <LearnMRSortMIP-oso-lambda>` *(optional)*
- :ref:`solver <LearnMRSortMIP-oso-solver>` *(optional)*


.. _LearnMRSortMIP-oso-criteria:

criteria
~~~~~~~~

The list of criteria to be considerated in the model. Criteria can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), criteria are considered as active. Preference Direction of the criteria can also be set.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _LearnMRSortMIP-oso-alternatives:

alternatives
~~~~~~~~~~~~

The list of learning alternatives to use to compute the ELECTRE TRI model. Alternatives can be activated or desactivated via the <active> tag (true or false). By default (no <active> tag), alternatives are considered as active.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _LearnMRSortMIP-oso-categories:

categories
~~~~~~~~~~

The list of categories of the model and their rank. The rank is required because the webservice need to know this to build the profiles delimiting the categories.

The input value should be a valid XMCDA document whose main tag is ``<categories>``.


------------------------


.. _LearnMRSortMIP-oso-perfs_table:

perfs_table
~~~~~~~~~~~

The performance table of the learning alternatives. All the learning alternatives are evaluated on all the criteria of the ELECTRE TRI model. The evaluations should be numeric values, i.e. <real>, <integer> or <rational>.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _LearnMRSortMIP-oso-reference_alts:

reference_alts *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~

The performance table of containing the reference alternatives to use (optional). It contains the performance of the alternatives associated to the profiles of the model. It is combined with the cat_profiles input when wanting to infer only the weights and the credibility threshold.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _LearnMRSortMIP-oso-crit_weights:

crit_weights *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~

The set of criteria weights to use (optional). This input is set if the user only wants to infer the profiles of the model. It should be combined with the majorityThreshold input. If set, this input should contain the weights of the criteria defined in the criteria input.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _LearnMRSortMIP-oso-assign:

assign
~~~~~~

The list of the learning alternatives affectations. This inputs represents the affectations of all the learning alternatives to a category. The categorie name used should be the same as the one given in the category input.

The input value should be a valid XMCDA document whose main tag is ``<alternativesAssignments>``.


------------------------


.. _LearnMRSortMIP-oso-cat_profiles:

cat_profiles *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~

The categories profiles to use (optional). This input defines the profiles delimiting the categories of the model. It is optional and set when wanting to infer only the weights and the credibility threshold.

The input value should be a valid XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _LearnMRSortMIP-oso-lambda:

lambda *(optional)*
~~~~~~~~~~~~~~~~~~~

The credibility threshold to use (optional). This input is combined with the crit_weights inputs when wanting to perform only the inference of the profiles.

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.


------------------------


.. _LearnMRSortMIP-oso-solver:

solver *(optional)*
~~~~~~~~~~~~~~~~~~~

The solver to use. Currently CPLEX and GLPK are supported. In none specified, GLPK is used.

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.


------------------------



.. _LearnMRSortMIP-oso_outputs:

Outputs
-------


- :ref:`compatible_alts <LearnMRSortMIP-oso-compatible_alts_out>`
- :ref:`profiles_perfs <LearnMRSortMIP-oso-profiles_perfs_out>`
- :ref:`crit_weights <LearnMRSortMIP-oso-crit_weights_out>`
- :ref:`cat_profiles <LearnMRSortMIP-oso-cat_profiles_out>`
- :ref:`lambda <LearnMRSortMIP-oso-lambda_out>`
- :ref:`messages <LearnMRSortMIP-oso-message>`


.. _LearnMRSortMIP-oso-compatible_alts_out:

compatible_alts
~~~~~~~~~~~~~~~

The reference alternatives that are compatible with the profiles, weights and majority threshold computed by the webservice.

The returned value is a XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _LearnMRSortMIP-oso-profiles_perfs_out:

profiles_perfs
~~~~~~~~~~~~~~

The profiles performance table of the profiles computed by the webservice.

The returned value is a XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _LearnMRSortMIP-oso-crit_weights_out:

crit_weights
~~~~~~~~~~~~

The set of criteria weights found by the webservice.

The returned value is a XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _LearnMRSortMIP-oso-cat_profiles_out:

cat_profiles
~~~~~~~~~~~~

The category profiles computed by the webservice.

The returned value is a XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _LearnMRSortMIP-oso-lambda_out:

lambda
~~~~~~

The credibility threshold computed by the webservice.

The returned value is a XMCDA document whose main tag is ``<programParameters>``.


------------------------


.. _LearnMRSortMIP-oso-message:

messages
~~~~~~~~

A list of messages generated by the webservice. In this output the result of the inference will be given. It gives informations on what might be wrong in the inputs.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/LearnMRSortMIP-oso/description-wsDD.xml>`
