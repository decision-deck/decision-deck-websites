.. _Fuzzy-Labels_defuzzification-URV:

Fuzzy-Labels_defuzzification
============================

:Version: 4.0
:Provider: URV
:SOAP service's name: ``Fuzzy-Labels_defuzzification-URV`` (see :ref:`soap-requests` for details)

Description
-----------

This module implements three different methods to defuzzificate the result of a ULOWA operation (Unbalanced Linguistic Ordered Weighted Average). Aggregation operators for linguistic variables usually assume uniform and symmetrical distribution of the linguistic terms that define the variable. However, there are some problems where an unbalanced set of linguistic terms is more appropriate to describe the objects. ULOWA accepts a set of linguistic labels defined with unbalanced fuzzy sets. The fuzzy sets must define a fuzzy partition on the set of reference values. They can be defined by trapezoidal or triangular membership functions.
For this method we apply three different operations for every ULOWA alternative result: COG, COM and Ordinal.

**Contact:** Aida Valls <aida.valls@urv.cat>

**Reference:** A. Valls, The Unbalanced Linguistic Ordered Weighted Averaging Operator, In: Proc. IEEE International Conference on Fuzzy Systems, FUZZ-IEEE 2010, IEEE Computer Society, Barcelona, Catalonia, 2010, pp. 3063-3070.


Inputs
------
(For outputs, see :ref:`below <Fuzzy-Labels_defuzzification-URV_outputs>`)


- :ref:`alternativesValues <Fuzzy-Labels_defuzzification-URV-input0>`
- :ref:`fuzzyNumbers <Fuzzy-Labels_defuzzification-URV-input1>`


.. _Fuzzy-Labels_defuzzification-URV-input0:

alternativesValues
~~~~~~~~~~~~~~~~~~

A list of alternativesValue. Normally it will be the result of the ULOWA or other linguistic aggregation operation.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.
It must have the following form::

   
				
				<alternativesValues>
					<alternativeValue>
					<alternativeID>...</alternativeID>
					<values>
						<value>
							<label>...</label>
						</value>
					</values>
					</alternativeValue>
					[...]
				</alternativesValues>
				
			


------------------------


.. _Fuzzy-Labels_defuzzification-URV-input1:

fuzzyNumbers
~~~~~~~~~~~~

Definition of the fuzzy sets associated to the linguistic variable used for all the criteria. The semantics of the linguistic labels are given by a trapezoidal membership function, each membership  function is represented as a group of four consecutive segments, each segment is descrived with two 2D points (head, tail). Into this version exists the possibility to define a segment using the tail of the previous segment (only in the same label) as the head point of the new one, avoiding the possibility of continuity errors of the segments. If the second segment is equal to third segment, the fuzzy is triangular. The values must be ordered increasingly. For each label in the linguistic domain (categoriesValues list), a fuzzy set must be defined. The labels must be ordered from the worst to the best performance (fi. Low, Medium, High, Perfect).

The input value should be a valid XMCDA document whose main tag is ``<criteriaScales>``.
It must have the following form::

   
		  				
			<criteriaScales id="fuzzy-numbers">
				<criterionScales>
					<criterionID>...<\criterionID>
					<scales>
						<scale id="fuzzy-scales">
							<qualitative>
								<preferenceDirection>min</preferenceDirection>
								<valuedLabels>
									<valuedLabel>
										<label>...</label>
										<value>
											<fuzzyNumber>
												<piecewiseLinear>
													<segment>
														<head>
															<abscissa>
																<real>...<\real>
															<\abscissa>
															<ordinate>
																<real>...<\real>
															<\ordinate>
														<\head>
														<tail>
															<abscissa>
																<real>...<\real>
															<\abscissa>
															<ordinate>
																<real>...<\real>
															<\ordinate>
														<\tail>
													<\segment>
													<segment>
														<\head>
														<tail>
															<abscissa>
																<real>...<\real>
															<\abscissa>
															<ordinate>
																<real>...<\real>
															<\ordinate>
														<\tail>
													<\segment>
													<segment>
													[...]
													<\segment>
													<segment>
													[...]
													<\segment>
												<\piecewiseLinear>
											<\fuzzyNumber>
										<\value>
									<\valuedLabel>
									<valuedLabel>
										[...]
									</valuedLabel>
									[...]
								</valuedLabels>
							<\qualitative>
						<\scale>
					<\scales>
				<\criterionScales>
			<\criteriaScales>
		  	
			


------------------------



.. _Fuzzy-Labels_defuzzification-URV_outputs:

Outputs
-------


- :ref:`defuzzificationCOG <Fuzzy-Labels_defuzzification-URV-output0>`
- :ref:`defuzzificationCOM <Fuzzy-Labels_defuzzification-URV-output1>`
- :ref:`defuzzificationORD <Fuzzy-Labels_defuzzification-URV-output2>`
- :ref:`messages <Fuzzy-Labels_defuzzification-URV-output3>`


.. _Fuzzy-Labels_defuzzification-URV-output0:

defuzzificationCOG
~~~~~~~~~~~~~~~~~~

Result obtained from apply COG calculation for each alternative and their fuzzy label result.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _Fuzzy-Labels_defuzzification-URV-output1:

defuzzificationCOM
~~~~~~~~~~~~~~~~~~

Result obtained from apply COM calculation for each alternative and their fuzzy label result.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _Fuzzy-Labels_defuzzification-URV-output2:

defuzzificationORD
~~~~~~~~~~~~~~~~~~

Result obtained from apply Ordinal calculation for each alternative and their fuzzy label result.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _Fuzzy-Labels_defuzzification-URV-output3:

messages
~~~~~~~~

A status message.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/Fuzzy-Labels_defuzzification-URV/description-wsDD.xml>`
