.. _Outranking-ScoreBin-WithPreferenceInformation_scores-PUT:

Outranking-ScoreBin-WithPreferenceInformation_scores
====================================================

:Version: 1.0.0
:Provider: PUT
:SOAP service's name: ``Outranking-ScoreBin-WithPreferenceInformation_scores-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Module for calculation ScoreBin scores using preference information given by decision maker.

**Contact:** Krzysztof Martyn <krzysztof.martyn@wp.pl>

**Web page:** https://bitbucket.org/Krzysztof_Martyn/prefrank


Inputs
------
(For outputs, see :ref:`below <Outranking-ScoreBin-WithPreferenceInformation_scores-PUT_outputs>`)


- :ref:`alternatives <Outranking-ScoreBin-WithPreferenceInformation_scores-PUT-input1>`
- :ref:`preferences <Outranking-ScoreBin-WithPreferenceInformation_scores-PUT-input2>`
- :ref:`preference_information <Outranking-ScoreBin-WithPreferenceInformation_scores-PUT-input3>`
- :ref:`parameters <Outranking-ScoreBin-WithPreferenceInformation_scores-PUT-input4>`


.. _Outranking-ScoreBin-WithPreferenceInformation_scores-PUT-input1:

alternatives
~~~~~~~~~~~~

Alternatives to consider.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _Outranking-ScoreBin-WithPreferenceInformation_scores-PUT-input2:

preferences
~~~~~~~~~~~

Aggregated preferences binary matrix or pairs for which the outrank relationship occurs.

The input value should be a valid XMCDA document whose main tag is ``<alternativesMatrix>``.


------------------------


.. _Outranking-ScoreBin-WithPreferenceInformation_scores-PUT-input3:

preference_information
~~~~~~~~~~~~~~~~~~~~~~

Preference information about alternatives. Strength for the alternative is given to the value of true, weakness for false, when no value is given, the alternative does not gain strength or weakness.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _Outranking-ScoreBin-WithPreferenceInformation_scores-PUT-input4:

parameters
~~~~~~~~~~

First parameter specifies if preference are given by matrix or pairs. Second parameter specifies the algorithm to calculate ranking. There are three algorithms to choose from: PageRank, HITS and Salsa.

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
        
		<programParameters>
			<parameter id="input_type" name="input_type">
				<values>
					<value>
						<label>%1</label>
					</value>
				</values>
			</parameter>	
			<parameter id="algorithm_type" name="algorithm_type">
				<values>
					<value>
						<label>%2</label>
					</value>
				</values>
			</parameter>
			<parameter id="q" name="q">
				<values>
					<value>
						<real>%3</real>
					</value>
				</values>
			</parameter>
			<parameter id="number_of_iteration" name="number_of_iteration">
				<values>
					<value>
						<integer>%4</integer>
					</value>
				</values>
			</parameter>
			<parameter id="check_convergence" name="check_convergence">
				<values>
					<value>
						<boolean>%5</boolean>
					</value>
				</values>
			</parameter>
			<parameter id="early_stopping" name="early_stopping">
				<values>
					<value>
						<boolean>%6</boolean>
					</value>
				</values>
			</parameter>
		</programParameters>
        
      

where:

- **%1** is a parameter named "input type". It can have the following values:

  - ``matrix``: Preferences given by whole matrix 1-0 valued.

  - ``pair``: Preferences given by pairs for whom the outranking relationship occurs (crisp).

  The default value is item0.

- **%2** is a parameter named "algorithm type". It can have the following values:

  - ``scorebin_with_preference_information_1``: ScoreBin with preference information I

  - ``scorebin_with_preference_information_2``: ScoreBin with preference information II

  - ``scorebin_with_preference_information_3``: ScoreBin with preference information III

  The default value is item0.

- **%3** is a parameter named "damping factor". This is a float, and the value should conform to the following constraint: The real value must be between 0 and 1.  More formally, the constraint is::

      %3 >= 0 && %3 <= 1 
  The default value is 0.15.

- **%4** is a parameter named "number of iteration". This is a int, and the value should conform to the following constraint: The value should be greater than 1.  More formally, the constraint is::

      %4 > 1
  The default value is 100.

- **%5** is a parameter named "Run averaging if not converge?". This is a boolean.
  The default value is false.

- **%6** is a parameter named "Earlier stop if converge?". This is a boolean.
  The default value is true.


------------------------



.. _Outranking-ScoreBin-WithPreferenceInformation_scores-PUT_outputs:

Outputs
-------


- :ref:`positive_flows <Outranking-ScoreBin-WithPreferenceInformation_scores-PUT-output1>`
- :ref:`negative_flows <Outranking-ScoreBin-WithPreferenceInformation_scores-PUT-output2>`
- :ref:`total_flows <Outranking-ScoreBin-WithPreferenceInformation_scores-PUT-output3>`
- :ref:`ranking <Outranking-ScoreBin-WithPreferenceInformation_scores-PUT-output4>`
- :ref:`messages <Outranking-ScoreBin-WithPreferenceInformation_scores-PUT-output5>`


.. _Outranking-ScoreBin-WithPreferenceInformation_scores-PUT-output1:

positive_flows
~~~~~~~~~~~~~~

Positive outranking flows.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _Outranking-ScoreBin-WithPreferenceInformation_scores-PUT-output2:

negative_flows
~~~~~~~~~~~~~~

Negative outranking flows.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _Outranking-ScoreBin-WithPreferenceInformation_scores-PUT-output3:

total_flows
~~~~~~~~~~~

Final flows computed from the given data.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _Outranking-ScoreBin-WithPreferenceInformation_scores-PUT-output4:

ranking
~~~~~~~

ScoreBin computed from the given data.

The returned value is a XMCDA document whose main tag is ``<alternativesMatrix>``.


------------------------


.. _Outranking-ScoreBin-WithPreferenceInformation_scores-PUT-output5:

messages
~~~~~~~~

Messages or errors generated by this module.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/Outranking-ScoreBin-WithPreferenceInformation_scores-PUT/description-wsDD.xml>`
