.. _RORUTA-RankRelatedPreferentialReducts-PUT:

RORUTA-RankRelatedPreferentialReducts
=====================================

:Version: 1.0
:Provider: PUT
:SOAP service's name: ``RORUTA-RankRelatedPreferentialReducts-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Finds all rank related preferential reducts. In other words, for each alternative, it finds all minimal sets of pairwise comparisons that has an impact on its range of possible ranking positions.

**Contact:** Pawel Rychly (pawelrychly@gmail.com).


Inputs
------
(For outputs, see :ref:`below <RORUTA-RankRelatedPreferentialReducts-PUT_outputs>`)


- :ref:`criteria <RORUTA-RankRelatedPreferentialReducts-PUT-criteria>`
- :ref:`alternatives <RORUTA-RankRelatedPreferentialReducts-PUT-alternatives>`
- :ref:`performances <RORUTA-RankRelatedPreferentialReducts-PUT-performances>`
- :ref:`characteristic-points <RORUTA-RankRelatedPreferentialReducts-PUT-characteristic-points>` *(optional)*
- :ref:`criteria-preference-directions <RORUTA-RankRelatedPreferentialReducts-PUT-criteria-preference-directions>` *(optional)*
- :ref:`preferences <RORUTA-RankRelatedPreferentialReducts-PUT-preferences>` *(optional)*
- :ref:`worst-ranking <RORUTA-RankRelatedPreferentialReducts-PUT-worst-ranking>`
- :ref:`best-ranking <RORUTA-RankRelatedPreferentialReducts-PUT-best-ranking>`
- :ref:`parameters <RORUTA-RankRelatedPreferentialReducts-PUT-parameters>`


.. _RORUTA-RankRelatedPreferentialReducts-PUT-criteria:

criteria
~~~~~~~~

A list of all considered criteria. The input value should be a valid XMCDA document whose main tag is criteria.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
                
                    <criteria>
                        <criterion id="%1" name="%1"></criterion>
                        [...]
                    </criteria>
                    
            


------------------------


.. _RORUTA-RankRelatedPreferentialReducts-PUT-alternatives:

alternatives
~~~~~~~~~~~~

The list of all considered alternatives. The input value should be a valid XMCDA document whose main tag is alternatives. Each alternative may be described using two attributes: id and name. While the first one denotes a machine readable name, the second represents a human readable name.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
                
                    <alternatives>
                        <alternative id="%1" name="%2" />
                        [...]
                    </alternatives>
                    
            


------------------------


.. _RORUTA-RankRelatedPreferentialReducts-PUT-performances:

performances
~~~~~~~~~~~~

Description of evaluation of alternatives on different criteria. It is required to provide the IDs of both criteria and alternatives described previously. The input value should be provided as a valid XMCDA document whose main tag is performanceTable

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.
It must have the following form::

   
                
                    <performanceTable>
                        <alternativePerformances>
                            <alternativeID>%1</alternativeID>
                            <performance>
                                <criterionID>%2</criterionID>
                                <value>
                                    <real>%3</real>
                                </value>
                            </performance>
                            [...]
                        </alternativePerformances>
                        [...]
                    </performanceTable>
                    
            


------------------------


.. _RORUTA-RankRelatedPreferentialReducts-PUT-characteristic-points:

characteristic-points *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A set of values associated with the criteria. This input allows to determine what type of value function should be used for the particular criterion. For each criterion that has an associated greater than one value, a piecewise linear value function is used. In this case, the mentioned value denotes a number of characteristic points of this value function. For the criteria that are not listed in this file, or for these for which the provided values are lower than two uses a general value function. The input value should be provided as a valid XMCDA document whose main tag is criteriaValues. Each element should contain both an id of the criterion, and value tag.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.
It must have the following form::

   
                
                     <criteriaValues>
                        <criterionValue>
                            <criterionID>%1</criterionID>
                            <value>
                                <integer>%2</integer>
                            </value>
                        </criterionValue>
                        [...]
                    </criteriaValues>
                    
            


------------------------


.. _RORUTA-RankRelatedPreferentialReducts-PUT-criteria-preference-directions:

criteria-preference-directions *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A set of values associated with criteria that determine their preference direction (0 - gain, 1 - cost).

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.
It must have the following form::

   
                
                     <criteriaValues mcdaConcept="preferenceDirection">
                        <criterionValue>
                            <criterionID>%1</criterionID>
                            <value>
                                <integer>%2</integer>
                            </value>
                        </criterionValue>
                        [...]
                    </criteriaValues>
                    
            


------------------------


.. _RORUTA-RankRelatedPreferentialReducts-PUT-preferences:

preferences *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~

Set of pairwise comparisons of reference alternatives. For a pair of alternatives three types of comparisons are supported. These are the strict preference, weak preference, and indifference. The input value should be provided as a valid XMCDA document whose main tag is alternativesComparisons. For each type of comparison, a separate alternativesComparisons tag should be used. Within these groups a mentioned types are denoted using a comparisonType tag by respectively strict, weak, and indif label. Comparisons should be provided as pairs of alternatives ids.

The input value should be a valid XMCDA document whose main tag is ``<alternativesComparisons>``.
It must have the following form::

   
                
                    <alternativesComparisons>
                        <comparisonType>
                            %1<!-- type of preference: strong, weak, or indif -->
                        </comparisonType>
                        <pairs>
                            <pair>
                                <initial>
                                    <alternativeID>%2</alternativeID>
                                </initial>
                                <terminal>
                                    <alternativeID>%3</alternativeID>
                                </terminal>
                            </pair>
                            [...]
                        </pairs>
                    </alternativesComparisons>
                    [...]
                    
            


------------------------


.. _RORUTA-RankRelatedPreferentialReducts-PUT-worst-ranking:

worst-ranking
~~~~~~~~~~~~~

The worst possible ranking positions that may be achieved by the alternatives taking into consideration all value functions that are compatible with the preference information.The input value should be provided as a valid XMCDA document whose main tag is alternativesValues.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.
It must have the following form::

   
                
                    <alternativesValues>
                        <alternativeValue>
                            <alternativeID>[...]</alternativeID>
                            <value>
                                <real>[...]</real>
                            </value>
                         </alternativeValue>
                        [...]
                    </alternativesValues>
                    
            


------------------------


.. _RORUTA-RankRelatedPreferentialReducts-PUT-best-ranking:

best-ranking
~~~~~~~~~~~~

The best possible ranking positions that may be achieved by the alternatives taking into consideration all value functions that are compatible with the preference information.The input value should be provided as a valid XMCDA document whose main tag is alternativesValues.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.
It must have the following form::

   
                
                    <alternativesValues>
                        <alternativeValue>
                            <alternativeID>[...]</alternativeID>
                            <value>
                                <real>[...]</real>
                            </value>
                         </alternativeValue>
                        [...]
                    </alternativesValues>
                    
            


------------------------


.. _RORUTA-RankRelatedPreferentialReducts-PUT-parameters:

parameters
~~~~~~~~~~

Single boolean value. Determines whether to use sctrictly increasing (true) or monotonously increasing (false) value functions

The input value should be a valid XMCDA document whose main tag is ``<methodParameters>``.
It must have the following form::

   
                
                     <methodParameters>
                        <parameter name="strict">
                            <value>
                                <boolean>%1</boolean>
                            </value>
                        </parameter>
                    </methodParameters>
                    
            

where:

- **%1** is a parameter named "Use strictly increasing value functions?". This is a boolean.
  The default value is false.


------------------------



.. _RORUTA-RankRelatedPreferentialReducts-PUT_outputs:

Outputs
-------


- :ref:`reducts-by-alternatives <RORUTA-RankRelatedPreferentialReducts-PUT-reducts-by-alternatives>`
- :ref:`messages <RORUTA-RankRelatedPreferentialReducts-PUT-messages>`


.. _RORUTA-RankRelatedPreferentialReducts-PUT-reducts-by-alternatives:

reducts-by-alternatives
~~~~~~~~~~~~~~~~~~~~~~~

Description of preferential reducts that are found for each alternative. The returned value is a valid XMCDA document whose main tag is the alternativesValues. Each element of this data contains an id of the alternative and a set of string values that describe different possible minimal sets of pairwise comparisons which imply this result. Each of these values is a comma separated list of relations. Each comparison is described as a pair of ids separated by one of the three possible labels: strong, weak, and indif. These abbreviations denotes respectively a strong preference, weak preference, or indifference between mentioned alternatives.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   
                
                    <alternativesValues>
                        <alternativeValue>
                          <alternativeID>%1</alternativeID>
                          <values>
                            <value>
                              <label>%2</label>
                            </value>
                            [...]
                          </values>
                        </alternativeValue>
                        [...]
                    </alternativesValues>
                    
            


------------------------


.. _RORUTA-RankRelatedPreferentialReducts-PUT-messages:

messages
~~~~~~~~

A list of messages generated by the algorithm.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/RORUTA-RankRelatedPreferentialReducts-PUT/description-wsDD.xml>`
