.. _PrometheeProfiles-J-MCDA:

PrometheeProfiles
=================

:Version: 0.5.5
:Provider: J-MCDA
:SOAP service's name: ``PrometheeProfiles-J-MCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Computes the profiles of the given alternatives on the given criteria, a la Promethee.

**Contact:** Olivier Cailloux <olivier.cailloux@ecp.fr>

**Web page:** http://sourceforge.net/projects/j-mcda/

**Reference:** Promethee methods, Brans & Mareschal, in Multiple Criteria Decision Analysis: State of the Art Surveys.


Inputs
------
(For outputs, see :ref:`below <PrometheeProfiles-J-MCDA_outputs>`)


- :ref:`criteria <PrometheeProfiles-J-MCDA-input1>`
- :ref:`alternatives <PrometheeProfiles-J-MCDA-input0>` *(optional)*
- :ref:`criteriaScales <PrometheeProfiles-J-MCDA-criteriaScales>`
- :ref:`criteriaThresholds <PrometheeProfiles-J-MCDA-criteriaThresholds>` *(optional)*
- :ref:`performances <PrometheeProfiles-J-MCDA-input2>`


.. _PrometheeProfiles-J-MCDA-input1:

criteria
~~~~~~~~

The criteria to consider, possibly with preference and veto thresholds (provided separated via the input 'criteriaThresholds'). Each one must have a preference direction (see input 'criteriaScales'). Set some criteria as inactive (or remove them) to ignore them.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _PrometheeProfiles-J-MCDA-input0:

alternatives *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~

The alternatives to consider. Set some alternatives as inactive (or remove them) to ignore them.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _PrometheeProfiles-J-MCDA-criteriaScales:

criteriaScales
~~~~~~~~~~~~~~

The scales of the criteria to consider.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _PrometheeProfiles-J-MCDA-criteriaThresholds:

criteriaThresholds *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The criteria' preference and indifference thresholds.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _PrometheeProfiles-J-MCDA-input2:

performances
~~~~~~~~~~~~

The performances of the alternatives on the criteria to consider.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------



.. _PrometheeProfiles-J-MCDA_outputs:

Outputs
-------


- :ref:`promethee_profiles <PrometheeProfiles-J-MCDA-output0>`
- :ref:`messages <PrometheeProfiles-J-MCDA-output1>`


.. _PrometheeProfiles-J-MCDA-output0:

promethee_profiles
~~~~~~~~~~~~~~~~~~

The profiles of the alternatives computed from the given input data.

The returned value is a XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _PrometheeProfiles-J-MCDA-output1:

messages
~~~~~~~~

A status message.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/PrometheeProfiles-J-MCDA/description-wsDD.xml>`
