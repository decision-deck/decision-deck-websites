.. _RORUTADIS-PostFactum-InvestigateValueChange-PUT:

RORUTADIS-PostFactum-InvestigateValueChange
===========================================

:Version: 0.3
:Provider: PUT
:SOAP service's name: ``RORUTADIS-PostFactum-InvestigateValueChange-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

Robust Ordinal Regression for value-based sorting: RORUTADIS-PostFactum-InvestigateValueChange service calculates missing value of an alternative utility for that alternative to be possibly (or necessarily) assigned to at least some specific class. It is possible to provide an additional optional preference information: example alternatives assignments, assignment pairwise comparisons and desired class cardinalities. Service developed by Krzysztof Ciomek (Poznan University of Technology, under supervision of Milosz Kadzinski).

**Contact:** 
			Krzysztof Ciomek (k.ciomek@gmail.com),
			Milosz Kadzinski (milosz.kadzinski@cs.put.poznan.pl)
		

**Web page:** https://github.com/kciomek/rorutadis

**Reference:** None


Inputs
------
(For outputs, see :ref:`below <RORUTADIS-PostFactum-InvestigateValueChange-PUT_outputs>`)


- :ref:`criteria <RORUTADIS-PostFactum-InvestigateValueChange-PUT-criteria>`
- :ref:`alternatives <RORUTADIS-PostFactum-InvestigateValueChange-PUT-alternatives>`
- :ref:`categories <RORUTADIS-PostFactum-InvestigateValueChange-PUT-categories>`
- :ref:`performanceTable <RORUTADIS-PostFactum-InvestigateValueChange-PUT-performanceTable>`
- :ref:`assignmentExamples <RORUTADIS-PostFactum-InvestigateValueChange-PUT-assignmentExamples>` *(optional)*
- :ref:`assignmentComparisons <RORUTADIS-PostFactum-InvestigateValueChange-PUT-assignmentComparisons>` *(optional)*
- :ref:`categoriesCardinalities <RORUTADIS-PostFactum-InvestigateValueChange-PUT-categoriesCardinalities>` *(optional)*
- :ref:`methodParameters <RORUTADIS-PostFactum-InvestigateValueChange-PUT-methodParameters>`


.. _RORUTADIS-PostFactum-InvestigateValueChange-PUT-criteria:

criteria
~~~~~~~~

A list of criteria (<criteria> tag) with information about preference direction (<criteriaValues mcdaConcept="preferenceDirection">, 0 - gain, 1 - cost) and number of characteristic points (<criteriaValues mcdaConcept="numberOfCharacteristicPoints">, 0 for the most general marginal utility function or integer grater or equal to 2) of each criterion.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.
It must have the following form::

   
<criteria>
	<criterion id="[...]" />
	[...]
</criteria>

<criteriaValues mcdaConcept="preferenceDirection">
	<criterionValue>
		<criterionID>[...]</criterionID>
		<value><integer>[...]</integer></value>
	</criterionValue>
	[...]
</criteriaValues>

<criteriaValues mcdaConcept="numberOfCharacteristicPoints">
	<criterionValue>
		<criterionID>[...]</criterionID>
		<value><integer>[0|integer greater or equal to 2]</integer></value>
	</criterionValue>
	[...]
</criteriaValues>



------------------------


.. _RORUTADIS-PostFactum-InvestigateValueChange-PUT-alternatives:

alternatives
~~~~~~~~~~~~

A list of alternatives.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
<alternatives>
	<alternative id="...">
		<active>[...]</active>
	</alternative>
</alternatives>



------------------------


.. _RORUTADIS-PostFactum-InvestigateValueChange-PUT-categories:

categories
~~~~~~~~~~

A list of categories (classes). List must be sorted from the worst category to the best.

The input value should be a valid XMCDA document whose main tag is ``<categories>``.
It must have the following form::

   
<categories>
	<category id="[...]" />
	[...]
</categories>



------------------------


.. _RORUTADIS-PostFactum-InvestigateValueChange-PUT-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

The performances of the alternatives.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _RORUTADIS-PostFactum-InvestigateValueChange-PUT-assignmentExamples:

assignmentExamples *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of assignment examples of alternatives to intervals of categories (classes) or to a specific category (class).

The input value should be a valid XMCDA document whose main tag is ``<alternativesAssignments>``.
It must have the following form::

   
<alternativesAssignments>
	<alternativeAssignment>
		<alternativeID>[...]</alternativeID>
		<categoryID>[...]</categoryID>
	</alternativeAssignment>
	[...]
	<alternativeAssignment>
		<alternativeID>[...]</alternativeID>
		<categoriesInterval>
			<lowerBound>
				<categoryID>[...]</categoryID>
			</lowerBound>
			<upperBound>
				<categoryID>[...]</categoryID>
			</upperBound>
		</categoriesInterval>
	</alternativeAssignment>
	[...]
	<alternativeAssignment>
		<alternativeID>[...]</alternativeID>
		<categoriesSet>
			<categoryID>[...]</categoryID>
			[...]
		</categoriesSet>
	</alternativeAssignment>
	[...]
</alternativesAssignments>



------------------------


.. _RORUTADIS-PostFactum-InvestigateValueChange-PUT-assignmentComparisons:

assignmentComparisons *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Two lists of assignment pairwise comparisons. A comparison from list with attribute mcdaConcept="atLeastAsGoodAs" indicates that some alternative should be assigned to class at least as good as class of some other alternative (k = 0) or at least better by k classes (k > 0). A comparison from list with attribute mcdaConcept="atMostAsGoodAs" indicates that some alternative should be assigned to class at most better by k classes (k > 0) then some other alternative.

The input value should be a valid XMCDA document whose main tag is ``<alternativesMatrix>``.
It must have the following form::

   
<alternativesMatrix mcdaConcept="atLeastAsGoodAs">
	<row>
		<alternativeID>[...]</alternativeID>
		<column>
			<alternativeID>[...]</alternativeID>
			<values>
				<value>
					<integer>k</integer>
				</value>
			</values>
		</column>
	</row>
	[...]
</alternativesMatrix>

<alternativesMatrix mcdaConcept="atMostAsGoodAs">
	<row>[...]</row>
	[...]
</alternativesMatrix>



------------------------


.. _RORUTADIS-PostFactum-InvestigateValueChange-PUT-categoriesCardinalities:

categoriesCardinalities *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A list of category (class) cardinality constraints. It allows to define minimal and/or maximal desired category (class) cardinalities.

The input value should be a valid XMCDA document whose main tag is ``<categoriesValues>``.
It must have the following form::

   
<categoriesValues>
	<categoryValue>
		<categoryID>[...]</categoryID>
		<values>
			<value>
				<interval>
					<lowerBound><integer>[...]</integer></lowerBound>
					<upperBound><integer>[...]</integer></upperBound>
				</interval>
			</value>
		</values>
	</categoryValue>
	[...]
</categoriesValues>



------------------------


.. _RORUTADIS-PostFactum-InvestigateValueChange-PUT-methodParameters:

methodParameters
~~~~~~~~~~~~~~~~

Method parameters.

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
<programParameters>
	<parameter name="strictlyMonotonicValueFunctions">
		<values>
			<value><boolean>%1</boolean></value>
		</values>
	</parameter>
	<parameter name="alternative">
		<values>
			<value><label>%2</label></value>
		</values>
	</parameter>
	<parameter name="necessary">
		<values>
			<value><boolean>%3</boolean></value>
		</values>
	</parameter>
	<parameter name="atLeastToClass">
		<values>
			<value><label>%4</label></value>
		</values>
	</parameter>
</programParameters>


where:

- **%1** is a parameter named "strictlyMonotonicValueFunctions". This is a boolean.
  The default value is false.

- **%2** is a parameter named "alternative". This is a string, and the value should conform to the following constraint: This identifier cannot be empty.  More formally, the constraint is::

      ! %2.isEmpty() 
- **%3** is a parameter named "necessary". This is a boolean.
  The default value is false.

- **%4** is a parameter named "atLeastToClass". This is a string, and the value should conform to the following constraint: This identifier cannot be empty.  More formally, the constraint is::

      ! %4.isEmpty() 

------------------------



.. _RORUTADIS-PostFactum-InvestigateValueChange-PUT_outputs:

Outputs
-------


- :ref:`marginalValueFunctions <RORUTADIS-PostFactum-InvestigateValueChange-PUT-marginalValueFunctions>`
- :ref:`alternativesValuesTable <RORUTADIS-PostFactum-InvestigateValueChange-PUT-alternativesValuesTable>`
- :ref:`assignments <RORUTADIS-PostFactum-InvestigateValueChange-PUT-assignments>`
- :ref:`values <RORUTADIS-PostFactum-InvestigateValueChange-PUT-values>`
- :ref:`thresholds <RORUTADIS-PostFactum-InvestigateValueChange-PUT-thresholds>`
- :ref:`messages <RORUTADIS-PostFactum-InvestigateValueChange-PUT-messages>`


.. _RORUTADIS-PostFactum-InvestigateValueChange-PUT-marginalValueFunctions:

marginalValueFunctions
~~~~~~~~~~~~~~~~~~~~~~

Marginal value functions.

The returned value is a XMCDA document whose main tag is ``<criteriaFunctions>``.
It has the following form::

   
<criteriaFunctions>
	<criterionFunction>
		<criterionID>g01</criterionID>
		<functions>
			<function>
				<discrete>
					<point>
						<abscissa><real>[...]</real></abscissa>
						<ordinate><real>[...]</real></ordinate>
					</point>
					[...]
				</discrete>
			</function>
		</functions>
	</criterionFunction>
	<criterionFunction>[...]</criterionFunction>
	[...]
</criteriaFuctions>



------------------------


.. _RORUTADIS-PostFactum-InvestigateValueChange-PUT-alternativesValuesTable:

alternativesValuesTable
~~~~~~~~~~~~~~~~~~~~~~~

Marginal utility values of alternatives (for result function where missing utility is the smallest).

The returned value is a XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _RORUTADIS-PostFactum-InvestigateValueChange-PUT-assignments:

assignments
~~~~~~~~~~~

Alternative assignments (for result function where missing utility is the smallest).

The returned value is a XMCDA document whose main tag is ``<alternativesAssignments>``.
It has the following form::

   
<alternativesAffectations>
	<alternativeAssignment>
		<alternativeID>[...]</alternativeID>
		<categoryID>[...]</categoryID>
	</alternativeAssignment>
	[...]
	<alternativeAssignment>
		<alternativeID>[...]</alternativeID>
		<categoriesInterval>
			<lowerBound>
				<categoryID>[...]</categoryID>
			</lowerBound>
			<upperBound>
				<categoryID>[...]</categoryID>
			</upperBound>
		</categoriesInterval>
	</alternativeAssignment>
	[...]
	<alternativeAssignment>
		<alternativeID>[...]</alternativeID>
		<categoriesSet>
			<categoryID>[...]</categoryID>
			[...]
		</categoriesSet>
	</alternativeAssignment>
	[...]
</alternativesAssignments>



------------------------


.. _RORUTADIS-PostFactum-InvestigateValueChange-PUT-values:

values
~~~~~~

Value of missing utility.

The returned value is a XMCDA document whose main tag is ``<alternativesValues>``.
It has the following form::

   
<alternativesValues mcdaConcept="investigationResultValue">
	<alternativeValue>
		<alternativeID>[...]</alternativeID>
		<values>
			<value>
				<real>[...]</real>
			</value>
		</values>
	</alternativeValue>
</alternativesValues>



------------------------


.. _RORUTADIS-PostFactum-InvestigateValueChange-PUT-thresholds:

thresholds
~~~~~~~~~~

Lower threshold for each category/class (for result function where missing utility is the smallest).

The returned value is a XMCDA document whose main tag is ``<categoriesValues>``.
It has the following form::

   
<categoriesValues mcdaConcept="thresholds">
	<categoryValue>
		<categoryID>[...]</categoryID>
		<values>
			<value>
				<real>[...]</real>
			</value>
		</values>
	</categoryValue>
	[...]
</categoriesValues>
			


------------------------


.. _RORUTADIS-PostFactum-InvestigateValueChange-PUT-messages:

messages
~~~~~~~~

Messages generated by the program.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/RORUTADIS-PostFactum-InvestigateValueChange-PUT/description-wsDD.xml>`
