:orphan:

.. _plotNumericPerformanceTable-PyXMCDA:

plotNumericPerformanceTable
===========================

:Version: 1.4
:Provider: PyXMCDA
:SOAP service's name: ``plotNumericPerformanceTable-PyXMCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Generate plots from provided performance tables as well as the scripts generating these plots.

Colormap can be defined by the user, by giving a list of colors in the parameters.xml file.
The number of colors is not restrained, and the colormap will linearly distribute the color in their provided order and interpolate between them.
If only one is provided, it will be used for all data plot.
Each color is either one of the color names predefined in matplotlib (See https://matplotlib.org/stable/gallery/color/named_colors.html#sphx-glr-gallery-color-named-colors-py) or a RGB color defined in hexadecimal '#RRGGBB'.

N.B.: This service can handle missing data by not representing them except for starCharts which centre values represent missing data.
Also, when plotting pie charts, negative and null values are not represented!

**Contact:** Nicolas Duminy (nicolas.duminy@telecom-bretagne.eu)

**Web page:** https://gitlab.com/nduminy/ws-pyxmcda


Inputs
------
(For outputs, see :ref:`below <plotNumericPerformanceTable-PyXMCDA_outputs>`)


- :ref:`alternatives <plotNumericPerformanceTable-PyXMCDA-alternatives>` *(optional)*
- :ref:`criteria <plotNumericPerformanceTable-PyXMCDA-criteria>` *(optional)*
- :ref:`performanceTable <plotNumericPerformanceTable-PyXMCDA-performanceTable>`
- :ref:`parameters <plotNumericPerformanceTable-PyXMCDA-parameters>` *(optional)*


.. _plotNumericPerformanceTable-PyXMCDA-alternatives:

alternatives *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~

The alternatives to be plotted. All are plotted if not provided.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _plotNumericPerformanceTable-PyXMCDA-criteria:

criteria *(optional)*
~~~~~~~~~~~~~~~~~~~~~

The criteria to be plotted. All are plotted if not provided.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _plotNumericPerformanceTable-PyXMCDA-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

The performance table.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _plotNumericPerformanceTable-PyXMCDA-parameters:

parameters *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~

Parameters of the method

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
    <programParameters>
        <programParameter id="x_axis" name="X-axis label">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="y_axis" name="Y-axis label">
            <values>
                <value>
                    <label>%2</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="chart_type" name="Chart type">
            <values>
                <value>
                    <label>%3</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="colors" name="Colors">
            <!-- colormap: %colormap / nb colors: %nb_colors -->
            <values>%color-mono%color-A%color-B%color-a%color-b%color-c%color-d%color-e%color-f%color-g%color-h%color-i%color-j
            </values>
        </programParameter>
        <programParameter id="image_file_extension" name="Image file extension">
            <values>
                <value>
                    <label>%7a%7b</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="plotter" name="Plotter">
            <values>
                <value>
                    <label>%8</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="order_alternatives_by" name="Order alternatives by">
            <values>
                <value>
                    <label>%9</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="order_criteria_by" name="Order criteria by">
            <values>
                <value>
                    <label>%10</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="reverse_order" name="Reverse order">
            <values>
                <value>
                    <boolean>%11</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="mix_colors" name="Mix colors">
            <values>
                <value>
                    <boolean>%12</boolean>
                </value>
            </values>
        </programParameter>
        <programParameter id="superposed_plot_title" name="Superposed plot title">
            <values>
                <value>
                    <label>%13</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="naming_conventions" name="Naming conventions">
            <values>
                <value>
                    <label>%14</label>
                </value>
            </values>
        </programParameter>
    </programParameters>


where:

- **%1** is a parameter named "X-axis label". This is a string.
- **%2** is a parameter named "Y-axis label". This is a string.
- **%3** is a parameter named "Chart type". It can have the following values:

  - ``barChart``: bar chart

  - ``pieChart``: pie chart

  - ``starChart``: star chart

  The default value is barChart.

- **%colormap** is a parameter named "Colormap". It can have the following values:

  - ``monochrome``: Monochrome

  - ``bicolor``: Bicolor

  - ``multicolor (advanced)``: Multi-color (advanced)

  The default value is monochrome.

- **%nb_colors** is a parameter named "Nb of colors". This is a int, and the value should conform to the following constraint: The value should be between 1 and 10..  More formally, the constraint is::

     %nb_colors > 0 && %nb_colors <= 10
  The default value is 2.

- **%color-mono** is a parameter named "Color". It can have the following values:

  - ``
                <value>
                    <label>black</label>
                </value>``: black

  - ``
                <value>
                    <label>red</label>
                </value>``: red

  - ``
                <value>
                    <label>green</label>
                </value>``: green

  - ``
                <value>
                    <label>blue</label>
                </value>``: blue

  - ``
                <value>
                    <label>orange</label>
                </value>``: orange

  - ``
                <value>
                    <label>green</label>
                </value>``: green

  - ``
                <value>
                    <label>purple</label>
                </value>``: purple

  - ``
                <value>
                    <label>cyan</label>
                </value>``: cyan

  - ``
                <value>
                    <label>magenta</label>
                </value>``: magenta

  - ``
                <value>
                    <label>yellow</label>
                </value>``: yellow

  - ``
                <value>
                    <label>salmon</label>
                </value>``: salmon

  - ``
                <value>
                    <label>orangered</label>
                </value>``: orangered

  - ``
                <value>
                    <label>chocolate</label>
                </value>``: chocolate

  - ``
                <value>
                    <label>greenyellow</label>
                </value>``: greenyellow

  - ``
                <value>
                    <label>aquamarine</label>
                </value>``: aquamarine

  - ``
                <value>
                    <label>pink</label>
                </value>``: pink

  - ``
                <value>
                    <label>grey</label>
                </value>``: grey

  - ``
                <value>
                    <label>whitesmoke</label>
                </value>``: whitesmoke

  The default value is black.

- **%color-A** is a parameter named "1st color". It can have the following values:

  - ``
                <value>
                    <label>black</label>
                </value>``: black

  - ``
                <value>
                    <label>red</label>
                </value>``: red

  - ``
                <value>
                    <label>green</label>
                </value>``: green

  - ``
                <value>
                    <label>blue</label>
                </value>``: blue

  - ``
                <value>
                    <label>orange</label>
                </value>``: orange

  - ``
                <value>
                    <label>green</label>
                </value>``: green

  - ``
                <value>
                    <label>purple</label>
                </value>``: purple

  - ``
                <value>
                    <label>cyan</label>
                </value>``: cyan

  - ``
                <value>
                    <label>magenta</label>
                </value>``: magenta

  - ``
                <value>
                    <label>yellow</label>
                </value>``: yellow

  - ``
                <value>
                    <label>salmon</label>
                </value>``: salmon

  - ``
                <value>
                    <label>orangered</label>
                </value>``: orange-red

  - ``
                <value>
                    <label>chocolate</label>
                </value>``: chocolate

  - ``
                <value>
                    <label>greenyellow</label>
                </value>``: green-yellow

  - ``
                <value>
                    <label>aquamarine</label>
                </value>``: aquamarine

  - ``
                <value>
                    <label>pink</label>
                </value>``: pink

  - ``
                <value>
                    <label>grey</label>
                </value>``: grey

  - ``
                <value>
                    <label>whitesmoke</label>
                </value>``: whitesmoke

  - ``
                <value>
                    <label>white</label>
                </value>``: white

  The default value is black.

- **%color-B** is a parameter named "2nd color". It can have the following values:

  - ``
                <value>
                    <label>black</label>
                </value>``: black

  - ``
                <value>
                    <label>red</label>
                </value>``: red

  - ``
                <value>
                    <label>green</label>
                </value>``: green

  - ``
                <value>
                    <label>blue</label>
                </value>``: blue

  - ``
                <value>
                    <label>orange</label>
                </value>``: orange

  - ``
                <value>
                    <label>green</label>
                </value>``: green

  - ``
                <value>
                    <label>purple</label>
                </value>``: purple

  - ``
                <value>
                    <label>cyan</label>
                </value>``: cyan

  - ``
                <value>
                    <label>magenta</label>
                </value>``: magenta

  - ``
                <value>
                    <label>yellow</label>
                </value>``: yellow

  - ``
                <value>
                    <label>salmon</label>
                </value>``: salmon

  - ``
                <value>
                    <label>orangered</label>
                </value>``: orangered

  - ``
                <value>
                    <label>chocolate</label>
                </value>``: chocolate

  - ``
                <value>
                    <label>greenyellow</label>
                </value>``: greenyellow

  - ``
                <value>
                    <label>aquamarine</label>
                </value>``: aquamarine

  - ``
                <value>
                    <label>pink</label>
                </value>``: pink

  - ``
                <value>
                    <label>grey</label>
                </value>``: grey

  - ``
                <value>
                    <label>whitesmoke</label>
                </value>``: whitesmoke

  The default value is black.

- **%color-a** is a parameter named "Color 1 (ex.: cyan or #00FFFF)". This is a string.
  The default value is black.

- **%color-b** is a parameter named "Color 2". This is a string.
  The default value is black.

- **%color-c** is a parameter named "Color 3". This is a string.
  The default value is black.

- **%color-d** is a parameter named "Color 4". This is a string.
  The default value is black.

- **%color-e** is a parameter named "Color 5". This is a string.
  The default value is black.

- **%color-f** is a parameter named "Color 6". This is a string.
  The default value is black.

- **%color-g** is a parameter named "Color 7". This is a string.
  The default value is black.

- **%color-h** is a parameter named "Color 8". This is a string.
  The default value is black.

- **%color-i** is a parameter named "Color 9". This is a string.
  The default value is black.

- **%color-j** is a parameter named "Color 10". This is a string.
  The default value is black.

- **%7a** is a parameter named "Image file extension". It can have the following values:

  - ``eps``: .eps (Encapsulated PostScript)

  - ``jpg``: .jpg (Joint Photographic Experts Group)

  - ``pdf``: .pdf (Portable Document Format)

  - ``pgf``: .pgf (Progressive Graphics File)

  - ``png``: .png (Portable Network Graphics)

  - ``ps``: .ps (PostScript)

  - ``raw``: .raw (Raw RGBA bitmap)

  - ``rgba``: .rgba (Silicon Graphics RGB)

  - ``svg``: .svg (Scalable Vector Graphics)

  - ``svgz``: .svgz (Compressed Scalable Vector Graphics)

  - ``tif``: .tif (Tagged Image File Format)

  The default value is png.

- **%7b** is a parameter named "Image file extension". It can have the following values:

  - ``eps``: .eps (Encapsulated PostScript)

  - ``jpg``: .jpg (Joint Photographic Experts Group)

  - ``pdf``: .pdf (Portable Document Format)

  - ``png``: .png (Portable Network Graphics)

  - ``svg``: .svg (Scalable Vector Graphics)

  The default value is png.

- **%8** is a parameter named "Plotter". It can have the following values:

  - ``matplotlib``: Matplotlib

  - ``gnuplot``: Gnuplot

  The default value is matplotlib.

- **%9** is a parameter named "Order alternatives by". It can have the following values:

  - ``id``: ids

  - ``name``: names

  - ``value``: values

  The default value is id.

- **%10** is a parameter named "Order criteria by". It can have the following values:

  - ``id``: ids

  - ``name``: names

  The default value is id.

- **%11** is a parameter named "Reverse order". This is a boolean.
  The default value is false.

- **%12** is a parameter named "Mix colors". This is a boolean.
  The default value is false.

- **%13** is a parameter named "Superposed plot title". This is a string.
- **%14** is a parameter named "Naming conventions". It can have the following values:

  - ``id``: Only ids are shown

  - ``name``: Only names are shown (disambiguated by appending the ids, if needed)

  - ``name (id)``: Names and ids are shown in that order

  - ``id (name)``: Ids and names are shown in that order

  The default value is name.


------------------------



.. _plotNumericPerformanceTable-PyXMCDA_outputs:

Outputs
-------


- :ref:`glob:numericPerformanceTable.{bmp,dia,fig,gif,hpgl,ico,jpg,jpe,pdf,png,ps,ps2,svg,svgz,tif} <plotNumericPerformanceTable-PyXMCDA-numericPerformanceTablePlot>`
- :ref:`glob:plot_numericPerformanceTable.{py,plt} <plotNumericPerformanceTable-PyXMCDA-numericPerformanceTablePlotScript>`
- :ref:`messages <plotNumericPerformanceTable-PyXMCDA-messages>`


.. _plotNumericPerformanceTable-PyXMCDA-numericPerformanceTablePlot:

glob:numericPerformanceTable.{bmp,dia,fig,gif,hpgl,ico,jpg,jpe,pdf,png,ps,ps2,svg,svgz,tif}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Image containing all selected values for performance table plots. Format corresponds to the one given in parameters (default is .png).

The returned value is a XMCDA document whose main tag is ``<None>``.


------------------------


.. _plotNumericPerformanceTable-PyXMCDA-numericPerformanceTablePlotScript:

glob:plot_numericPerformanceTable.{py,plt}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Generated Python or Gnuplot script that made the image.
                Given to enable users to later customize the appearance of the plots.
                Extension is .py if matplotlib is used, .plt for gnuplot.

The returned value is a XMCDA document whose main tag is ``<None>``.


------------------------


.. _plotNumericPerformanceTable-PyXMCDA-messages:

messages
~~~~~~~~

Status messages.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/plotNumericPerformanceTable-PyXMCDA/description-wsDD.xml>`
