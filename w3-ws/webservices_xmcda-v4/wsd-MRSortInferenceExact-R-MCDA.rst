.. _MRSortInferenceExact-R-MCDA:

Exact inference of MRSort
=========================

:Version: 1.0
:Provider: R-MCDA
:Name: MRSortInferenceExact
:SOAP service's name: ``MRSortInferenceExact-R-MCDA`` (see :ref:`soap-requests` for details)

Description
-----------

The MRSort method, a simplification of the Electre TRI method, uses the pessimistic assignment rule, without indifference or preference thresholds attached to criteria. Only a binary discordance condition is considered, i.e. a veto forbids an outranking in any possible concordance situation, or not. The identification of the profiles, weights and majority threshold are done by taking into account assignment examples.

**Contact:** Alexandru Olteanu (al.olteanu@telecom-bretagne.eu)


Inputs
------
(For outputs, see :ref:`below <MRSortInferenceExact-R-MCDA_outputs>`)


- :ref:`alternatives <MRSortInferenceExact-R-MCDA-inalt>`
- :ref:`criteria <MRSortInferenceExact-R-MCDA-incrit>`
- :ref:`categoriesRanks <MRSortInferenceExact-R-MCDA-incategval>`
- :ref:`performanceTable <MRSortInferenceExact-R-MCDA-inperf>`
- :ref:`alternativesAssignments <MRSortInferenceExact-R-MCDA-assignments>`
- :ref:`parameters <MRSortInferenceExact-R-MCDA-parameters>` *(optional)*


.. _MRSortInferenceExact-R-MCDA-inalt:

alternatives
~~~~~~~~~~~~

A complete list of alternatives to be considered when inferring the MR-Sort model.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.
It must have the following form::

   
			


------------------------


.. _MRSortInferenceExact-R-MCDA-incrit:

criteria
~~~~~~~~

A list of criteria on which the alternatives are evaluated. For each criterion, the preference direction should be provided.

The input value should be a valid XMCDA document whose main tag is ``<criteriaScales>``.


------------------------


.. _MRSortInferenceExact-R-MCDA-incategval:

categoriesRanks
~~~~~~~~~~~~~~~

A list of categories ranks, 1 stands for the most preferred category and the higher the number the lower the preference for that category.

The input value should be a valid XMCDA document whose main tag is ``<categoriesValues>``.


------------------------


.. _MRSortInferenceExact-R-MCDA-inperf:

performanceTable
~~~~~~~~~~~~~~~~

The evaluations of the alternatives on the set of criteria.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _MRSortInferenceExact-R-MCDA-assignments:

alternativesAssignments
~~~~~~~~~~~~~~~~~~~~~~~

The alternatives assignments to categories.

The input value should be a valid XMCDA document whose main tag is ``<alternativesAssignments>``.


------------------------


.. _MRSortInferenceExact-R-MCDA-parameters:

parameters *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~

An indicator for whether vetoes should be included in the model or not.

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
			   
    <programParameters>
        <parameter id="veto">
            <values>
                <value>
                    <boolean>%1</boolean>
                </value>
            </values>
        </parameter>
        <parameter id="readableWeights">
            <values>
                <value>
                    <boolean>%2</boolean>
                </value>
            </values>
        </parameter>
        <parameter id="readableProfiles">
            <values>
                <value>
                    <boolean>%3</boolean>
                </value>
            </values>
        </parameter>
    </programParameters>

			

where:

- **%1** is a parameter named "Include vetoes". This is a boolean.
  The default value is false.

- **%2** is a parameter named "Readable weights". This is a boolean.
  The default value is false.

- **%3** is a parameter named "Readable profiles". This is a boolean.
  The default value is false.


------------------------



.. _MRSortInferenceExact-R-MCDA_outputs:

Outputs
-------


- :ref:`majorityThreshold <MRSortInferenceExact-R-MCDA-majority>`
- :ref:`criteriaWeights <MRSortInferenceExact-R-MCDA-weights>`
- :ref:`categoriesProfiles <MRSortInferenceExact-R-MCDA-outcatprof>`
- :ref:`categoriesProfilesPerformanceTable <MRSortInferenceExact-R-MCDA-outcatprofpt>`
- :ref:`vetoProfiles <MRSortInferenceExact-R-MCDA-outcatveto>`
- :ref:`vetoProfilesPerformanceTable <MRSortInferenceExact-R-MCDA-outcatvetopt>`
- :ref:`messages <MRSortInferenceExact-R-MCDA-msg>`


.. _MRSortInferenceExact-R-MCDA-majority:

majorityThreshold
~~~~~~~~~~~~~~~~~

The majority threshold.

The returned value is a XMCDA document whose main tag is ``<programParameters>``.


------------------------


.. _MRSortInferenceExact-R-MCDA-weights:

criteriaWeights
~~~~~~~~~~~~~~~

The criteria weights.

The returned value is a XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _MRSortInferenceExact-R-MCDA-outcatprof:

categoriesProfiles
~~~~~~~~~~~~~~~~~~

The categories delimiting profiles.

The returned value is a XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _MRSortInferenceExact-R-MCDA-outcatprofpt:

categoriesProfilesPerformanceTable
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The evaluations of the category profiles.

The returned value is a XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _MRSortInferenceExact-R-MCDA-outcatveto:

vetoProfiles
~~~~~~~~~~~~

The categories veto profiles.

The returned value is a XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _MRSortInferenceExact-R-MCDA-outcatvetopt:

vetoProfilesPerformanceTable
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The evaluations of the veto profiles.

The returned value is a XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _MRSortInferenceExact-R-MCDA-msg:

messages
~~~~~~~~

Messages from the execution of the webservice. Possible errors in the input data will be given here.

The returned value is a XMCDA document whose main tag is ``<methodMessages>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/MRSortInferenceExact-R-MCDA/description-wsDD.xml>`
