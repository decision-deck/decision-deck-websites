.. _applyCriteriaFunctionsToPerformanceTable-PyXMCDA:

applyCriteriaFunctionsToPerformanceTable
========================================

:Version: 1.0
:Provider: PyXMCDA
:SOAP service's name: ``applyCriteriaFunctionsToPerformanceTable-PyXMCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Apply criteria functions to input performance table.

**Contact:** Nicolas Duminy (nicolas.duminy@telecom-bretagne.eu)

**Web page:** https://gitlab.com/nduminy/ws-pyxmcda


Inputs
------
(For outputs, see :ref:`below <applyCriteriaFunctionsToPerformanceTable-PyXMCDA_outputs>`)


- :ref:`alternatives <applyCriteriaFunctionsToPerformanceTable-PyXMCDA-alternatives>` *(optional)*
- :ref:`criteria <applyCriteriaFunctionsToPerformanceTable-PyXMCDA-criteria>` *(optional)*
- :ref:`performanceTable <applyCriteriaFunctionsToPerformanceTable-PyXMCDA-performanceTable>`
- :ref:`criteriaFunctions <applyCriteriaFunctionsToPerformanceTable-PyXMCDA-criteriaFunctions>`


.. _applyCriteriaFunctionsToPerformanceTable-PyXMCDA-alternatives:

alternatives *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~

The alternatives. Only used to exclude inactive alternatives from computations (all are considered if not provided).

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _applyCriteriaFunctionsToPerformanceTable-PyXMCDA-criteria:

criteria *(optional)*
~~~~~~~~~~~~~~~~~~~~~

The criteria. Only used to exclude inactive criteria from computations (all are considered if not provided).

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _applyCriteriaFunctionsToPerformanceTable-PyXMCDA-performanceTable:

performanceTable
~~~~~~~~~~~~~~~~

The performance table.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _applyCriteriaFunctionsToPerformanceTable-PyXMCDA-criteriaFunctions:

criteriaFunctions
~~~~~~~~~~~~~~~~~

Criteria functions to apply.

The input value should be a valid XMCDA document whose main tag is ``<criteriaFunctions>``.


------------------------



.. _applyCriteriaFunctionsToPerformanceTable-PyXMCDA_outputs:

Outputs
-------


- :ref:`performanceTable <applyCriteriaFunctionsToPerformanceTable-PyXMCDA-performanceTable_out>`
- :ref:`messages <applyCriteriaFunctionsToPerformanceTable-PyXMCDA-messages>`


.. _applyCriteriaFunctionsToPerformanceTable-PyXMCDA-performanceTable_out:

performanceTable
~~~~~~~~~~~~~~~~

The output performance table.

The returned value is a XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _applyCriteriaFunctionsToPerformanceTable-PyXMCDA-messages:

messages
~~~~~~~~

Status messages.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/applyCriteriaFunctionsToPerformanceTable-PyXMCDA/description-wsDD.xml>`
