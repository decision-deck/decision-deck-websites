.. _ElectreCredibilityWithCounterVeto-PUT:

ElectreCredibilityWithCounterVeto
=================================

:Version: 0.1.0
:Provider: PUT
:SOAP service's name: ``ElectreCredibilityWithCounterVeto-PUT`` (see :ref:`soap-requests` for details)

Description
-----------

ElectreCredibilityWithCounterVeto - computes credibility matrix using procedure which is common to the most methods from the Electre family.

This module is an extended version of 'ElectreCredibility' in that it is designed to work with the 'counter-veto' concept - i.e. it requires an additional input file ('counter_veto_crossed.xml') produced by 'ElectreDiscordance' module, which contains the information for which pairs and on which criteria the 'counter-veto' threshold has been crossed.

Please note that unlike 'ElectreCredibility', this module can accept discordance indices only in non-aggregated form (i.e. one index per criterion).

**Web page:** http://github.com/xor-xor/electre_diviz

**Reference:** Bernard Roy and Roman Słowiński; Handling effects of reinforced preference and counter-veto in credibility of outranking; European Journal of Operational Research 188(1):185–190; 2008; doi:10.1016/j.ejor.2007.04.005


Inputs
------
(For outputs, see :ref:`below <ElectreCredibilityWithCounterVeto-PUT_outputs>`)


- :ref:`alternatives <ElectreCredibilityWithCounterVeto-PUT-input1>`
- :ref:`concordance <ElectreCredibilityWithCounterVeto-PUT-input3>`
- :ref:`counter_veto_crossed <ElectreCredibilityWithCounterVeto-PUT-input4>`
- :ref:`discordance <ElectreCredibilityWithCounterVeto-PUT-input5>`
- :ref:`classes_profiles <ElectreCredibilityWithCounterVeto-PUT-input2>` *(optional)*
- :ref:`method_parameters <ElectreCredibilityWithCounterVeto-PUT-input6>`


.. _ElectreCredibilityWithCounterVeto-PUT-input1:

alternatives
~~~~~~~~~~~~

Alternatives to consider.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _ElectreCredibilityWithCounterVeto-PUT-input3:

concordance
~~~~~~~~~~~

Concordance matrix with aggregated concordance indices (i.e. one index per pair of alternatives or alternatives/profiles).

The input value should be a valid XMCDA document whose main tag is ``<alternativesMatrix>``.


------------------------


.. _ElectreCredibilityWithCounterVeto-PUT-input4:

counter_veto_crossed
~~~~~~~~~~~~~~~~~~~~

This input contains the information (boolean values) for which pairs of variants and on which criteria the 'counter-veto' threshold has been crossed.

The input value should be a valid XMCDA document whose main tag is ``<alternativesMatrix>``.


------------------------


.. _ElectreCredibilityWithCounterVeto-PUT-input5:

discordance
~~~~~~~~~~~

Discordance indices provided in non-aggregated ('partial' or 'per-criterion') form.

The input value should be a valid XMCDA document whose main tag is ``<alternativesMatrix>``.


------------------------


.. _ElectreCredibilityWithCounterVeto-PUT-input2:

classes_profiles *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Definitions of profiles (boundary or central) which should be used for classes (categories) representation.

The input value should be a valid XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _ElectreCredibilityWithCounterVeto-PUT-input6:

method_parameters
~~~~~~~~~~~~~~~~~

A set of parameters provided to tune up the module's operation.

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
<programParameters>
	<parameter name="comparison_with">
		<values>
			<value>
				<label>%1</label>
			</value>
		</values>
	</parameter>
	<parameter name="with_denominator">
		<values>
			<value>
				<boolean>%2</boolean>
			</value>
		</values>
	</parameter>
	<parameter name="only_max_discordance">
		<values>
			<value>
				<boolean>%3</boolean>
			</value>
		</values>
	</parameter>
</programParameters>


where:

- **%1** is a parameter named "comparison_with". It can have the following values:

  - ``alternatives``: alternatives vs alternatives

  - ``boundary_profiles``: alternatives vs boundary profiles

  - ``central_profiles``: alternatives vs central (characteristic) profiles

  The default value is item0.

- **%2** is a parameter named "with_denominator". This is a boolean.
  The default value is true.

- **%3** is a parameter named "only_max_discordance". This is a boolean.
  The default value is false.


------------------------



.. _ElectreCredibilityWithCounterVeto-PUT_outputs:

Outputs
-------


- :ref:`credibility <ElectreCredibilityWithCounterVeto-PUT-output1>`
- :ref:`messages <ElectreCredibilityWithCounterVeto-PUT-output2>`


.. _ElectreCredibilityWithCounterVeto-PUT-output1:

credibility
~~~~~~~~~~~

Credibility matrix computed from the given data.

The returned value is a XMCDA document whose main tag is ``<alternativesMatrix>``.


------------------------


.. _ElectreCredibilityWithCounterVeto-PUT-output2:

messages
~~~~~~~~

Messages or errors generated by this module.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/ElectreCredibilityWithCounterVeto-PUT/description-wsDD.xml>`
