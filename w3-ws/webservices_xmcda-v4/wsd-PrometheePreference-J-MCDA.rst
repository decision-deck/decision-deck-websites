.. _PrometheePreference-J-MCDA:

PrometheePreference
===================

:Version: 0.5.5
:Provider: J-MCDA
:SOAP service's name: ``PrometheePreference-J-MCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Computes a preference relation, a la Promethee.

**Contact:** Olivier Cailloux <olivier.cailloux@ecp.fr>

**Web page:** http://sourceforge.net/projects/j-mcda/

**Reference:** Cailloux, Olivier. Electre and Promethee MCDA methods as reusable software components. In Proceedings of the 25th Mini-EURO Conference on Uncertainty and Robustness in Planning and Decision Making (URPDM 2010). Coimbra, Portugal, 2010.


Inputs
------
(For outputs, see :ref:`below <PrometheePreference-J-MCDA_outputs>`)


- :ref:`criteria <PrometheePreference-J-MCDA-input1>`
- :ref:`alternatives <PrometheePreference-J-MCDA-input0>` *(optional)*
- :ref:`criteriaScales <PrometheePreference-J-MCDA-criteriaScales>`
- :ref:`criteriaThresholds <PrometheePreference-J-MCDA-criteriaThresholds>` *(optional)*
- :ref:`performances <PrometheePreference-J-MCDA-input3>`
- :ref:`weights <PrometheePreference-J-MCDA-input2>`


.. _PrometheePreference-J-MCDA-input1:

criteria
~~~~~~~~

The criteria to consider, possibly with preference and veto thresholds (provided separated via the input 'criteriaThresholds'). Each one must have a preference direction (see input 'criteriaScales'). Set some criteria as inactive (or remove them) to ignore them.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _PrometheePreference-J-MCDA-input0:

alternatives *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~

The alternatives to consider. Set some alternatives as inactive (or remove them) to ignore them.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _PrometheePreference-J-MCDA-criteriaScales:

criteriaScales
~~~~~~~~~~~~~~

The scale of the criteria to consider.

The input value should be a valid XMCDA document whose main tag is ``<criteriaScales>``.


------------------------


.. _PrometheePreference-J-MCDA-criteriaThresholds:

criteriaThresholds *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The criteria' preference and indifference thresholds.

The input value should be a valid XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _PrometheePreference-J-MCDA-input3:

performances
~~~~~~~~~~~~

The performances of the alternatives on the criteria to consider.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _PrometheePreference-J-MCDA-input2:

weights
~~~~~~~

The weights of the criteria to consider.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.


------------------------



.. _PrometheePreference-J-MCDA_outputs:

Outputs
-------


- :ref:`preference <PrometheePreference-J-MCDA-output0>`
- :ref:`messages <PrometheePreference-J-MCDA-output1>`


.. _PrometheePreference-J-MCDA-output0:

preference
~~~~~~~~~~

The preference relation computed from the given input data.

The returned value is a XMCDA document whose main tag is ``<alternativesMatrix>``.


------------------------


.. _PrometheePreference-J-MCDA-output1:

messages
~~~~~~~~

A status message.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/PrometheePreference-J-MCDA/description-wsDD.xml>`
