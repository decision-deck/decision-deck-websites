.. _csvToXMCDA-performanceTable-PyXMCDA:

csvToXMCDA-performanceTable
===========================

:Version: 2.0
:Provider: PyXMCDA
:SOAP service's name: ``csvToXMCDA-performanceTable-PyXMCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Transforms a file containing a performance table from a comma-separated values (CSV) file to three XMCDA compliant files, containing the corresponding criteria ids, alternatives' ids and the performance table.

**Contact:** Sébastien Bigaret (sebastien.bigaret@telecom-bretagne.eu)

**Web page:** https://gitlab.com/sbigaret/ws-pyxmcda


Inputs
------
(For outputs, see :ref:`below <csvToXMCDA-performanceTable-PyXMCDA_outputs>`)


- :ref:`performanceTable.csv <csvToXMCDA-performanceTable-PyXMCDA-input0>`
- :ref:`parameters <csvToXMCDA-performanceTable-PyXMCDA-parameters>`


.. _csvToXMCDA-performanceTable-PyXMCDA-input0:

performanceTable.csv
~~~~~~~~~~~~~~~~~~~~

The performance table as a CSV file.

Example:

  ,cost,risk,employment,connection
  a11,17537,28.3,34.8,2.33
  a03,16973,29,34.9,2.66

The first column contains the criteria' ids. Additionally, the alternatives' names are also extracted when the cells are formatted like `id (name)`.  Set the parameter "First column" to "id" to deactivate the extraction of alternatives' names.

Then one line per alternative, with their ids and names derived from the 1st cell (just like criteria' ids, see above) and the rest being the performance of each alternative on the criteria.

By default the values are supposed to be float numbers.  This can be changed using the parameter "Default content".  It is possible to specify the type of a value by prepending it with a prefix:

- `float`: for floats (ex.: `1`, `1.2`, `1.2e3`)
- `integer`: for integers (decimal representation: `127`, hexadecimal: `0x7f`, octal: `0o177`, binary: `0b1111111`)
- `string`: for strings (note that a string with a colon should always be prefixed by `string:`, no matter what the default prefix is).
- `boolean`: for booleans: 1 or 'true' (case insensitive) are True values, everything else is false.
- `na`: for N/A (everything after the colon is ignored)

Example::

  ,cost,risky,color,connection
  a11,17537,boolean:true,string:red,2.33
  a03,16973,boolean:false,string:blue.na:

The input value should be a valid XMCDA document whose main tag is ``<other>``.


------------------------


.. _csvToXMCDA-performanceTable-PyXMCDA-parameters:

parameters
~~~~~~~~~~

Parameters of the method

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
    <programParameters>
        <parameter id="csv_delimiter">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </parameter>
        <parameter id="default_prefix">
            <values>
                <value>
                    <label>%2</label>
                </value>
            </values>
        </parameter>
        <parameter id="name_in_id">
            <values>
                <value>
                    <boolean>%3</boolean>
                </value>
            </values>
        </parameter>
    </programParameters>


where:

- **%1** is a parameter named "CSV delimiter". This is a string, and the value should conform to the following constraint: One character maximum.  More formally, the constraint is::

     %1.length() < 2
- **%2** is a parameter named "Default content". It can have the following values:

  - ``float``: float

  - ``label``: string

  - ``integer``: integer

  - ``boolean``: boolean

  The default value is float.

- **%3** is a parameter named "First column". It can have the following values:

  - ``false``: id

  - ``true``: id (name)

  The default value is id_and_name.


------------------------



.. _csvToXMCDA-performanceTable-PyXMCDA_outputs:

Outputs
-------


- :ref:`alternatives <csvToXMCDA-performanceTable-PyXMCDA-output0>`
- :ref:`criteria <csvToXMCDA-performanceTable-PyXMCDA-output1>`
- :ref:`performanceTable <csvToXMCDA-performanceTable-PyXMCDA-output2>`
- :ref:`messages <csvToXMCDA-performanceTable-PyXMCDA-output3>`


.. _csvToXMCDA-performanceTable-PyXMCDA-output0:

alternatives
~~~~~~~~~~~~

The equivalent alternative ids.

The returned value is a XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _csvToXMCDA-performanceTable-PyXMCDA-output1:

criteria
~~~~~~~~

The equivalent criteria ids.

The returned value is a XMCDA document whose main tag is ``<criteria>``.


------------------------


.. _csvToXMCDA-performanceTable-PyXMCDA-output2:

performanceTable
~~~~~~~~~~~~~~~~

The equivalent performances.

The returned value is a XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _csvToXMCDA-performanceTable-PyXMCDA-output3:

messages
~~~~~~~~

Status messages.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/csvToXMCDA-performanceTable-PyXMCDA/description-wsDD.xml>`
