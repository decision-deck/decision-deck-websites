:orphan:

.. _plotXYAlternativesValues-PyXMCDA:

plotXYAlternativesValues
========================

:Version: 1.3
:Provider: PyXMCDA
:SOAP service's name: ``plotXYAlternativesValues-PyXMCDA`` (see :ref:`soap-requests` for details)

Description
-----------

Generate plot from provided alternativesValues as well as the scripts generating these plots.
The plot compares both alternativesValues by plotting them as 2D points with first set of values as abscissa and second one as ordinates.

**Contact:** Nicolas Duminy (nicolas.duminy@telecom-bretagne.eu)

**Web page:** https://gitlab.com/nduminy/ws-pyxmcda


Inputs
------
(For outputs, see :ref:`below <plotXYAlternativesValues-PyXMCDA_outputs>`)


- :ref:`alternatives <plotXYAlternativesValues-PyXMCDA-alternatives>` *(optional)*
- :ref:`alternativesValues1 <plotXYAlternativesValues-PyXMCDA-alternativesValues1>`
- :ref:`alternativesValues2 <plotXYAlternativesValues-PyXMCDA-alternativesValues2>`
- :ref:`parameters <plotXYAlternativesValues-PyXMCDA-parameters>` *(optional)*


.. _plotXYAlternativesValues-PyXMCDA-alternatives:

alternatives *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~

The alternatives to be plotted. All are plotted if not provided.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _plotXYAlternativesValues-PyXMCDA-alternativesValues1:

alternativesValues1
~~~~~~~~~~~~~~~~~~~

The reference alternatives values to compare to.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _plotXYAlternativesValues-PyXMCDA-alternativesValues2:

alternativesValues2
~~~~~~~~~~~~~~~~~~~

The alternatives values subject to comparison.

The input value should be a valid XMCDA document whose main tag is ``<alternativesValues>``.


------------------------


.. _plotXYAlternativesValues-PyXMCDA-parameters:

parameters *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~

Parameters of the method

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.
It must have the following form::

   
    <programParameters>
        <programParameter id="x_axis" name="X-axis label">
            <values>
                <value>
                    <label>%1</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="y_axis" name="Y-axis label">
            <values>
                <value>
                    <label>%2</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="chart_title" name="Chart title">
            <values>
                <value>
                    <label>%3</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="color" name="Color">
            <values>
                <value>
                    <label>%4</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="marker" name="Marker">
            <values>
                <value>
                    <label>%5</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="linestyle" name="Linestyle">
            <values>
                <value>
                    <label>%6</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="image_file_extension" name="Image file extension">
            <values>
                <value>
                    <label>%7a%7b</label>
                </value>
            </values>
        </programParameter>
        <programParameter id="plotter" name="Plotter">
            <values>
                <value>
                    <label>%8</label>
                </value>
            </values>
        </programParameter>
    </programParameters>


where:

- **%1** is a parameter named "X-axis label". This is a string.
  The default value is alternativesValues 1.

- **%2** is a parameter named "Y-axis label". This is a string.
  The default value is alternativesValues 2.

- **%3** is a parameter named "Chart title". This is a string.
  The default value is Alternatives Values XY.

- **%4** is a parameter named "Color". It can have the following values:

  - ``r``: red

  - ``b``: blue

  - ``g``: green

  - ``c``: cyan

  - ``m``: magenta

  - ``y``: yellow

  - ``k``: black

  - ``w``: white

  The default value is black.

- **%5** is a parameter named "Marker". It can have the following values:

  - ``.``: point

  - ``None``: none

  - ``o``: circle

  - ``v``: triangle down

  - ``^``: triangle up

  - ``&lt;``: triangle left

  - ``&gt;``: triangle right

  - ``s``: square

  - ``p``: pentagon

  - ``*``: star

  - ``h``: hexagon 1

  - ``H``: hexagon 2

  - ``+``: plus

  - ``x``: x

  - ``d``: thin diamond

  - ``D``: diamond

  - ``|``: vline

  - ``_``: hline

  - ``P``: plus filled

  - ``X``: x (filled)

  The default value is circle.

- **%6** is a parameter named "Linestyle". It can have the following values:

  - ``solid``: solid line

  - ``dashed``: dashed line

  - ``dashdot``: dashed and dotted line

  - ``dotted``: dotted line

  - ``None``: no line

  The default value is dashed.

- **%7a** is a parameter named "Image file extension". It can have the following values:

  - ``eps``: .eps (Encapsulated PostScript)

  - ``jpg``: .jpg (Joint Photographic Experts Group)

  - ``pdf``: .pdf (Portable Document Format)

  - ``pgf``: .pgf (Progressive Graphics File)

  - ``png``: .png (Portable Network Graphics)

  - ``ps``: .ps (PostScript)

  - ``raw``: .raw (Raw RGBA bitmap)

  - ``rgba``: .rgba (Silicon Graphics RGB)

  - ``svg``: .svg (Scalable Vector Graphics)

  - ``svgz``: .svgz (Compressed Scalable Vector Graphics)

  - ``tif``: .tif (Tagged Image File Format)

  The default value is png.

- **%7b** is a parameter named "Image file extension". It can have the following values:

  - ``eps``: .eps (Encapsulated PostScript)

  - ``jpg``: .jpg (Joint Photographic Experts Group)

  - ``pdf``: .pdf (Portable Document Format)

  - ``png``: .png (Portable Network Graphics)

  - ``svg``: .svg (Scalable Vector Graphics)

  The default value is png.

- **%8** is a parameter named "Plotter". It can have the following values:

  - ``matplotlib``: Matplotlib

  - ``gnuplot``: Gnuplot

  The default value is matplotlib.


------------------------



.. _plotXYAlternativesValues-PyXMCDA_outputs:

Outputs
-------


- :ref:`glob:XYalternativesValues.{bmp,dia,fig,gif,hpgl,ico,jpg,jpe,pdf,png,ps,ps2,svg,svgz,tif} <plotXYAlternativesValues-PyXMCDA-XYAlternativesValues>`
- :ref:`glob:plot_XYalternativesValues.{py,plt} <plotXYAlternativesValues-PyXMCDA-XYAlternativesValuesPlotScript>`
- :ref:`messages <plotXYAlternativesValues-PyXMCDA-messages>`


.. _plotXYAlternativesValues-PyXMCDA-XYAlternativesValues:

glob:XYalternativesValues.{bmp,dia,fig,gif,hpgl,ico,jpg,jpe,pdf,png,ps,ps2,svg,svgz,tif}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Image containing all selected values for XY alternativesValues plots. Format corresponds to the one given in parameters (default is .png).

The returned value is a XMCDA document whose main tag is ``<None>``.


------------------------


.. _plotXYAlternativesValues-PyXMCDA-XYAlternativesValuesPlotScript:

glob:plot_XYalternativesValues.{py,plt}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Generated Python or Gnuplot script that made the image.
                Given to enable users to later customize the appearance of the plots.
                Extension is .py if matplotlib is used, .plt for gnuplot.

The returned value is a XMCDA document whose main tag is ``<None>``.


------------------------


.. _plotXYAlternativesValues-PyXMCDA-messages:

messages
~~~~~~~~

Status messages.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/plotXYAlternativesValues-PyXMCDA/description-wsDD.xml>`
