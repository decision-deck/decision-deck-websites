.. _MRSort-LargePerfDiff_assignment-R-MCDA:

MRSort with large performance differences
=========================================

:Version: 1.0
:Provider: R-MCDA
:Name: MRSort-LargePerfDiff_assignment
:SOAP service's name: ``MRSort-LargePerfDiff_assignment-R-MCDA`` (see :ref:`soap-requests` for details)

Description
-----------

MRSort is a simplified ELECTRE TRI sorting method, where alternatives are assigned to an ordered set of categories. In this case, we also take into account large performance differences, both negative (vetoes) and positive (dictators).

**Contact:** Alexandru Olteanu (alexandru.olteanu@univ-ubs.fr)

**Reference:** P. MEYER, A-L. OLTEANU, Integrating large positive and negative performance differences into multicriteria majority-rule sorting models, Computers and Operations Research, 81, pp. 216 - 230, 2017.


Inputs
------
(For outputs, see :ref:`below <MRSort-LargePerfDiff_assignment-R-MCDA_outputs>`)


- :ref:`alternatives <MRSort-LargePerfDiff_assignment-R-MCDA-inalt>`
- :ref:`categories <MRSort-LargePerfDiff_assignment-R-MCDA-incateg>`
- :ref:`performanceTable <MRSort-LargePerfDiff_assignment-R-MCDA-inperf>`
- :ref:`categoriesProfilesPerformanceTable <MRSort-LargePerfDiff_assignment-R-MCDA-incatprofpt>`
- :ref:`vetoProfilesPerformanceTable <MRSort-LargePerfDiff_assignment-R-MCDA-invetoprofpt>` *(optional)*
- :ref:`dictatorProfilesPerformanceTable <MRSort-LargePerfDiff_assignment-R-MCDA-indictprofpt>` *(optional)*
- :ref:`criteria <MRSort-LargePerfDiff_assignment-R-MCDA-incrit>`
- :ref:`criteriaWeights <MRSort-LargePerfDiff_assignment-R-MCDA-weights>`
- :ref:`categoriesProfiles <MRSort-LargePerfDiff_assignment-R-MCDA-incatprof>`
- :ref:`vetoProfiles <MRSort-LargePerfDiff_assignment-R-MCDA-invetoprof>` *(optional)*
- :ref:`dictatorProfiles <MRSort-LargePerfDiff_assignment-R-MCDA-indictprof>` *(optional)*
- :ref:`categoriesRanks <MRSort-LargePerfDiff_assignment-R-MCDA-incategval>`
- :ref:`majorityThreshold <MRSort-LargePerfDiff_assignment-R-MCDA-majorityThreshold>`
- :ref:`assignmentRule <MRSort-LargePerfDiff_assignment-R-MCDA-assignmentRule>`


.. _MRSort-LargePerfDiff_assignment-R-MCDA-inalt:

alternatives
~~~~~~~~~~~~

A complete list of alternatives to be considered by the MR-Sort method.

The input value should be a valid XMCDA document whose main tag is ``<alternatives>``.


------------------------


.. _MRSort-LargePerfDiff_assignment-R-MCDA-incateg:

categories
~~~~~~~~~~

A list of categories to which the alternatives will be assigned.

The input value should be a valid XMCDA document whose main tag is ``<categories>``.


------------------------


.. _MRSort-LargePerfDiff_assignment-R-MCDA-inperf:

performanceTable
~~~~~~~~~~~~~~~~

The evaluations of the alternatives on the set of criteria.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _MRSort-LargePerfDiff_assignment-R-MCDA-incatprofpt:

categoriesProfilesPerformanceTable
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The evaluations of the category profiles.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _MRSort-LargePerfDiff_assignment-R-MCDA-invetoprofpt:

vetoProfilesPerformanceTable *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The evaluations of the veto profiles.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _MRSort-LargePerfDiff_assignment-R-MCDA-indictprofpt:

dictatorProfilesPerformanceTable *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The evaluations of the dictator profiles.

The input value should be a valid XMCDA document whose main tag is ``<performanceTable>``.


------------------------


.. _MRSort-LargePerfDiff_assignment-R-MCDA-incrit:

criteria
~~~~~~~~

A list of criteria on which the alternatives are evaluated. For each criterion, the preference direction should be provided.

The input value should be a valid XMCDA document whose main tag is ``<criteriaScales>``.


------------------------


.. _MRSort-LargePerfDiff_assignment-R-MCDA-weights:

criteriaWeights
~~~~~~~~~~~~~~~

The criteria weights.

The input value should be a valid XMCDA document whose main tag is ``<criteriaValues>``.


------------------------


.. _MRSort-LargePerfDiff_assignment-R-MCDA-incatprof:

categoriesProfiles
~~~~~~~~~~~~~~~~~~

The categories delimiting profiles.

The input value should be a valid XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _MRSort-LargePerfDiff_assignment-R-MCDA-invetoprof:

vetoProfiles *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~

The categories veto profiles.

The input value should be a valid XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _MRSort-LargePerfDiff_assignment-R-MCDA-indictprof:

dictatorProfiles *(optional)*
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The categories dictator profiles.

The input value should be a valid XMCDA document whose main tag is ``<categoriesProfiles>``.


------------------------


.. _MRSort-LargePerfDiff_assignment-R-MCDA-incategval:

categoriesRanks
~~~~~~~~~~~~~~~

A list of categories ranks, 1 stands for the most preferred category and the higher the number the lower the preference for that category.

The input value should be a valid XMCDA document whose main tag is ``<categoriesValues>``.


------------------------


.. _MRSort-LargePerfDiff_assignment-R-MCDA-majorityThreshold:

majorityThreshold
~~~~~~~~~~~~~~~~~

The majority threshold.

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.


------------------------


.. _MRSort-LargePerfDiff_assignment-R-MCDA-assignmentRule:

assignmentRule
~~~~~~~~~~~~~~

The type of assignment rule. Can be anything from the list M (majority rule), V (veto), D (dictator), v (veto weakened by dictator), d (dictator weakened by veto), dV (dominating veto and weakened dictator), Dv (dominating dictator and weakened veto) and dv (conflicting veto and dictator).

The input value should be a valid XMCDA document whose main tag is ``<programParameters>``.

where:

- **%1** is a parameter named "Assignment rule". It can have the following values:

  - ``M``: Majority rule

  - ``V``: Veto

  - ``D``: Dictator

  - ``v``: Veto weakened by dictator

  - ``d``: Dictator weakened by veto

  - ``dV``: Dominating Veto and weakened Dictator

  - ``Dv``: Dominating Dictator and weakened veto

  - ``dv``: Conflicting Veto and Dictator

  The default value is M.


------------------------



.. _MRSort-LargePerfDiff_assignment-R-MCDA_outputs:

Outputs
-------


- :ref:`alternativesAssignments <MRSort-LargePerfDiff_assignment-R-MCDA-outaffect>`
- :ref:`messages <MRSort-LargePerfDiff_assignment-R-MCDA-msg>`


.. _MRSort-LargePerfDiff_assignment-R-MCDA-outaffect:

alternativesAssignments
~~~~~~~~~~~~~~~~~~~~~~~

The alternatives assignments to categories.

The returned value is a XMCDA document whose main tag is ``<alternativesAssignments>``.


------------------------


.. _MRSort-LargePerfDiff_assignment-R-MCDA-msg:

messages
~~~~~~~~

Messages from the execution of the webservice. Possible errors in the input data will be given here.

The returned value is a XMCDA document whose main tag is ``<programExecutionResult>``.


------------------------



Original xml description
------------------------

 - :download:`description.xml<files/descriptions/MRSort-LargePerfDiff_assignment-R-MCDA/description-wsDD.xml>`
