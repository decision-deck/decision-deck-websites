.. _howto.ws:

How to create an XMCDA *web service-able* program?
==================================================

.. toctree::
   :maxdepth: 1

The Decision Deck project offers an architecture dedicated to web services.
Its primary goal is to offer a place where MCDA algorithms, methods etc.
are hosted and made freely available to anyone.

One basic way to access these web services is to use standard `SOAP requests <https://en.wikipedia.org/wiki/SOAP>`_. Moreover, you can use `diviz <//www.diviz.org/>`_, with offers a graphical interface to build algorithmic workflows based on thse web-services.

Now you have a program and you would like to publish it inside the  Decision Deck web services' architecture.

This guide demonstrates the necessary steps, and you will find:

- what is required for your program to be integrated,
- the additional informations you need to provide to us.

  .. note:: We do our best to be as understandable and comprehensive as possible.  However, you may
   experience difficulties following the guidelines; maybe you're more a
   mathematician than a computer-scientist, or maybe we failed to be clear
   enough in this document.  Don't worry!  Whatever the reason, please do not hesitate to :ref:`contact <contact>` us: requests for advices and help are always welcome and
   we are happy to collaborate with anyone willing to integrate one's program
   within the Decision Deck's web services infrastructure or the `diviz`_ software.

   We do **not** require that you understand every single thing in this document before contacting us: simply follow the guidelines you understand, and we will work together to fill in the remaining parts.


Elements of context
-------------------

**Open-source**:

  The Decision Deck project is about sharing MCDA researches, results, algorithms and programs: when it comes to programs, it means that they must be released under an open-source licence.  Any open-source license is accepted, it is usually one of the OSI-approved open-source licenses (https://www.opensource.org/).

**Inter-operability**:

  The integration within the Decision Deck infrastructure means that your program should be able to interoperate with all the others (and vice versa). To ensure that the programs can cooperate, the Decision Deck consortium proposes a common data exchange standard called `XMCDA <//www.decision-deck.org/xmcda>`_; your program should be able to take its data inputs from XMCDA-formatted files and produce such XMCDA compliant files.

**Ease of integration**

  One of our main goals when designing the web-service environment was to be able to integrate most programs or scripts, so that anyone may participate by simply using the tools he/she works with.

  This means two important things:

  1. we do not require any particular language: you use the one you are used to;

  2. there is nothing you need to know about web-services: you just supply your program and we provide the necessary environment to transform it into a web-service.

To achieve this, we only have a few requirements on the program you submit, we will explain that in details.

**Documentation, GUI:**

  The integration of a program into the set of Decision Deck web-services is not only a technical point.  First, as it becomes available to anyone, we want it to be sufficiently documented (see :ref:`the documentation for existing webservices <webServices>`).  It also means that it is very likely to be integrated in other softwares, such as `diviz`_, which present a more user-friendly interface to access the web-services.

  This is the reason why we need you to provide a documentation in addition to the program itself.  As you will see, we already have a specific framework for that, making it possible to generate a comprehensive documentation and to make it possible for third-party software to automatically use this documentation for accessing the services.


Requirements for programs
-------------------------

In order to prepare a program for being deployed as a web service in the Decision Deck environment, some requirements must be met.  They are described in this separate document: :ref:`Requirements on programs<ws.program_requirements>`.


Description of the program
--------------------------

As already mentioned, a program needs to be documented, and the information provided in the documentation may also be used by third-party software using the web-services.  To fulfil these two pre-requisites, we propose that a single documentation file is provided, following a precise schema.

The format of the documentation is exposed in a separate document: :ref:`Documenting the program / web-service <specification>`.


Submitting a service: the expected structure for the archive
------------------------------------------------------------

The archive you send to us when submitting (or updating) a web-service should have a precise structure ; please conform to these recommandations, since this really helps us to manage submissions.

1. The archive should have **only one top-directory**, named after the program's name and the provider's name given in the ``description-wsDD.xml``, which contains all the files, **without any exception**.  This top-directory may obviously contain other sub-directories.

   For example, the archive for the program *weightedSum* whose provider is
   *PyXMCDA* should have only one top-directory: ``weightedSum-PyXMCDA/``
   which contains all files **without any exception**.

2. Test files:

   - there is at least one set of files to test the program,
   - all sets of test files are organized in sub-directories named ``tests/in1``, ``tests/in2`` etc.
   - For each test set ``tests/in<x>/`` there is a corresponding ``test/out<x>/`` directory containing the expected output files.
   - For each test set ``tests/in<x>/``, there is a corresponding script ``launch.test<x>`` demonstrating how to execute the program on the test files.


This is ok::

    weightedSum-PyXMCDA/
      +- description-wsDD.xml
      +- execute1
      +- tests/
         + launch.test1.sh
         + in1/
              +- test_file1.xml
         +- out1/
              +- output1.xml

This is **NOT** ok::

    weightedSum-PyXMCDA/
      +- description-wsDD.xml
    in1/
      +- test_file1.xml

because:

- there are files/directories that are not under ``weightedSum-PyXMCDA/``
- the expected outputs for the test set ``in1`` are not provided,
- the file containing the command-line to execute the examples is missing.


Checklist
---------

Before submitting us a program for its integration into Decision Deck's web services architecture (and maybe into diviz), we suggest to check the following elements we discussed above:

- Your program:

  - is released under an open-source license,
  - understands the XMCDA format,
  - can be run entirely from the command-line (no graphical user interface),
  - can be run on a linux platform,
  - is available as an archive on the internet.

- The documentation file ``description-wsDD.xml`` exists.

- You have at least one complete set of input files for testing the program, and for every test set, you provide the expected output files.

- The archive you submit is organized as expected.

- The source code and the installation instructions are available on the internet, and you supplied the URL where they can be obtained.

  (this last point is optional: if you have nowhere to host the code, you can sant it to us by email e.g.)

  The installation procedure contains the list of the required libraries, how to build the program, how to install it, and any additional informations you can think about that will be necessary for the installation of the program.


Once you have collected all these items, please create an archive with all the elements and send it to submission@diviz.org with your deployment request. We will contact you as soon as the integration has been successfully completed, or if we need some further details from you.

Again, remember that this is a just a guide and that this is **not** a problem if you are not sure about some points, if you have trouble making the description file, or whatever: please contact us with the first elements you have, and we will be glad to work with you to fill in the gaps and eventually cover the missing elements.

.. index:: web service, XMCDA, specification.xml, program submission
