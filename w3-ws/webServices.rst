List of available XMCDA web services
------------------------------------

.. raw:: html

    <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for...">
    <table id="myTable">
      <tr class="header">
        <th style="width:30%;">Name</th>
        <th style="width:10%;">Version</th>
        <th style="width:15%;">Provider</th>
        <th style="width:45%;">Description</th>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ACUTA-UTAR.html">ACUTA</a></div>
        </td>
        <td>
          <div>2.0</div>
        </td>
        <td>
          <div>UTAR</div>
        </td>
        <td>
          <div>Computes ACUTA method - analytic center for UTA - which provides a set of additive value functio ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-additiveValueFunctionsIdentification-RXMCDA.html">additiveValueFunctionsIdentification</a></div>
        </td>
        <td>
          <div>1.1</div>
        </td>
        <td>
          <div>RXMCDA</div>
        </td>
        <td>
          <div>Identifies an set of piecewise linear additive value functions according to a ranking of the alt ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-AHP_criteriaWeights-PUT.html">AHP_criteriaWeights</a></div>
        </td>
        <td>
          <div>1.1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes criteria weights using AHP method.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-AHP_priorities-PUT.html">AHP_priorities</a></div>
        </td>
        <td>
          <div>1.1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes ranking for given alternatives and criteria using AHP method.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-alternativesRankingViaQualificationDistillation-ITTB.html">alternativesRankingViaQualificationDistillation</a></div>
        </td>
        <td>
          <div>1.1</div>
        </td>
        <td>
          <div>ITTB</div>
        </td>
        <td>
          <div>This web service computes rankings on the alternatives by distillation of alternatives' qualific ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-alternativesValuesKendall-RXMCDA.html">alternativesValuesKendall</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>RXMCDA</div>
        </td>
        <td>
          <div>Calculates the correlation index between two rankings of alternatives (represented by lists of r ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-BWM-weights-PUT.html">BWM-weights</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes criteria weights using BWM method.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-capacityEntropy-kappalab.html">capacityEntropy</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>kappalab</div>
        </td>
        <td>
          <div>Computes the normalized entropy of a capacity. The capacity is  given either as its Mobius trans ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-capacityFavor-kappalab.html">capacityFavor</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>kappalab</div>
        </td>
        <td>
          <div>Computes the favor indices of a Choquet integral from the underlying normalised Mobius transform ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-capacityInteraction-kappalab.html">capacityInteraction</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>kappalab</div>
        </td>
        <td>
          <div>Computes the Shapley interaction index for pairs of elements with respect to the Mobius transfor ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-capacityOrness-kappalab.html">capacityOrness</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>kappalab</div>
        </td>
        <td>
          <div>Computes the orness degree of a Choquet integral from the underlying normalized Mobius transform ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-capacityShapley-kappalab.html">capacityShapley</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>kappalab</div>
        </td>
        <td>
          <div>Computes the Shapley value (n indices) of the Mobius transform of a capacity.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-capacityVariance-kappalab.html">capacityVariance</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>kappalab</div>
        </td>
        <td>
          <div>Computes the normalized variance of the Mobius transform of a capacity.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-capacityVeto-kappalab.html">capacityVeto</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>kappalab</div>
        </td>
        <td>
          <div>Computes the veto indices of a Choquet integral from the underlying normalised Mobius transform  ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-choquetIntegral-kappalab.html">choquetIntegral</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>kappalab</div>
        </td>
        <td>
          <div>Computes the Choquet integral from a normalised Mobius transform of a capacity.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-COMET-ZUT.html">COMET</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>ZUT</div>
        </td>
        <td>
          <div>This web service allows to compute preference value of a given alternatives set based on COMET m ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-computeAlternativesQualification-RXMCDA.html">computeAlternativesQualification</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>RXMCDA</div>
        </td>
        <td>
          <div>Computes the strength, weakness and qualification of the alternatives. Requires a 0-1 valued out ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-computeNormalisedPerformanceTable-UTAR.html">computeNormalisedPerformanceTable</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>UTAR</div>
        </td>
        <td>
          <div>Transforms a performance table via value functions.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-computeNumericPerformanceTable-ITTB.html">computeNumericPerformanceTable</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>ITTB</div>
        </td>
        <td>
          <div>This web service generates a transformed numeric performance table. The performance table before ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-CondorcetRobustnessRelation-PyXMCDA.html">CondorcetRobustnessRelation</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PyXMCDA</div>
        </td>
        <td>
          <div>This web service allows to compute the Condorcet robustness denotation associated to the concord ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-consistencyTest-ITTB.html">consistencyTest</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>ITTB</div>
        </td>
        <td>
          <div>This web service aims at testing the consistency of the judgements provided by an individual or  ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-convertAlternativesRanksToAlternativesComparisons-RXMCDA.html">convertAlternativesRanksToAlternativesComparisons</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>RXMCDA</div>
        </td>
        <td>
          <div>Converts ranks of alternatives into alternatives comparisons.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-criteriaDescriptiveStatistics-ITTB.html">criteriaDescriptiveStatistics</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>ITTB</div>
        </td>
        <td>
          <div>This web service provides statistics about the criteria. It computes for every criterion the ave ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-csvToXMCDA-alternativesValues-PyXMCDA.html">csvToXMCDA-alternativesValues</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PyXMCDA</div>
        </td>
        <td>
          <div>Transforms a file containing alternatives values from a comma-separated values (CSV) file to two ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-csvToXMCDA-categoriesProfiles-PyXMCDA.html">csvToXMCDA-categoriesProfiles</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PyXMCDA</div>
        </td>
        <td>
          <div>Transforms a file containing categories profiles from a comma-separated values (CSV) file to XMC ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-csvToXMCDA-criteriaThresholds-PyXMCDA.html">csvToXMCDA-criteriaThresholds</a></div>
        </td>
        <td>
          <div>1.2</div>
        </td>
        <td>
          <div>PyXMCDA</div>
        </td>
        <td>
          <div>Transforms a file containing criteria discrimination thresholds and preference directions from a ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-csvToXMCDA-criteriaValues-PyXMCDA.html">csvToXMCDA-criteriaValues</a></div>
        </td>
        <td>
          <div>1.2</div>
        </td>
        <td>
          <div>PyXMCDA</div>
        </td>
        <td>
          <div>Transforms a file containing criteria values from a comma-separated values (CSV) file to two XMC ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-csvToXMCDA-performanceTable-PyXMCDA.html">csvToXMCDA-performanceTable</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PyXMCDA</div>
        </td>
        <td>
          <div>Transforms a file containing a performance table from a comma-separated values (CSV) file to thr ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-csvToXMCDA-valueFunctions-PyXMCDA.html">csvToXMCDA-valueFunctions</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PyXMCDA</div>
        </td>
        <td>
          <div>Transforms a file containing value functions from a comma-separated values (CSV) file to a XMCDA ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-cutRelation-ITTB.html">cutRelation</a></div>
        </td>
        <td>
          <div>1.1</div>
        </td>
        <td>
          <div>ITTB</div>
        </td>
        <td>
          <div>This web service cuts a fuzzy relation (on alternatives) at a given threshold and produces a rel ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-cutRelation-J-MCDA.html">cutRelation-J</a></div>
        </td>
        <td>
          <div>0.5.5</div>
        </td>
        <td>
          <div>J-MCDA</div>
        </td>
        <td>
          <div>Cuts a fuzzy relation (on alternatives) at a given threshold and produces a binary relation.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-cutRelationCrisp-PUT.html">cutRelationCrisp</a></div>
        </td>
        <td>
          <div>0.1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>cutRelationCrisp - this module applies cut threshold to the credibility matrix provided as input ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-DEACCRCrossEfficiency-PUT.html">DEACCRCrossEfficiency</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes cross efficiency scores for the given DMUs (alternatives) using CCR Data Envelopment An ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-DEACCREfficiency-PUT.html">DEACCREfficiency</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes efficiency scores for the given DMUs (alternatives) using CCR Data Envelopment Analysis ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-DEACCREfficiencyBounds-PUT.html">DEACCREfficiencyBounds</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes efficiency bounds scores for the given DMUs (alternatives) using CCR Data Envelopment A ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-DEACCRExtremeRanks-PUT.html">DEACCRExtremeRanks</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Determines ranking intervals for the given DMUs (alternatives) using CCR Data Envelopment Analys ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-DEACCRPreferenceRelations-PUT.html">DEACCRPreferenceRelations</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Determines necessary and possible dominance relations for the given DMUs (alternatives) using CC ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-DEACCRSuperEfficiency-PUT.html">DEACCRSuperEfficiency</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes super-efficiency scores for the given DMUs (alternatives) using CCR Data Envelopment An ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-DEASMAACCREfficiencies-PUT.html">DEASMAACCREfficiencies</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes efficiency scores for the given DMUs (alternatives) using SMAA-D method and CCR Data En ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-DEASMAACCRPreferenceRelations-PUT.html">DEASMAACCRPreferenceRelations</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Determines dominance relations for the given DMUs (alternatives) using SMAA-D method and CCR Dat ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-DEASMAACCRRanks-PUT.html">DEASMAACCRRanks</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes rankings for the given DMUs (alternatives) using SMAA-D method and CCR Data Envelopment ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-DEASMAAvalueADDEfficiencies-PUT.html">DEASMAAvalueADDEfficiencies</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes efficiency scores for the given DMUs (alternatives) using SMAA-D method and additive Da ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-DEASMAAvalueADDPreferenceRelations-PUT.html">DEASMAAvalueADDPreferenceRelations</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Determines dominance relations for the given DMUs (alternatives) using SMAA-D method and additiv ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-DEASMAAvalueADDRanks-PUT.html">DEASMAAvalueADDRanks</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes rankings for the given DMUs (alternatives) using SMAA-D method and additive Data Envelo ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-DEAvalueADDCrossEfficiency-PUT.html">DEAvalueADDCrossEfficiency</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes cross efficiency scores for the given DMUs (alternatives) using additive Data Envelopme ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-DEAvalueADDEfficiency-PUT.html">DEAvalueADDEfficiency</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes efficiency scores for the given DMUs (alternatives) using additive Data Envelopment Ana ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-DEAvalueADDEfficiencyBounds-PUT.html">DEAvalueADDEfficiencyBounds</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes efficiency bounds for the given DMUs (alternatives) using additive Data Envelopment Ana ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-DEAvalueADDExtremeRanks-PUT.html">DEAvalueADDExtremeRanks</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Determines ranking intervals for the given DMUs (alternatives) using additive Data Envelopment A ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-DEAvalueADDNormalizedPerformanceMatrix-PUT.html">DEAvalueADDNormalizedPerformanceMatrix</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes utility values for decision making units based on given functions for inputs and outputs.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-DEAvalueADDPreferenceRelations-PUT.html">DEAvalueADDPreferenceRelations</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Determines necessary and possible dominance relations for the given DMUs (alternatives) using ad ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-DEAvalueADDSuperEfficiency-PUT.html">DEAvalueADDSuperEfficiency</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes super-efficiency scores for the given DMUs (alternatives) using additive Data Envelopme ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-defuzzificationCOG-URV.html">defuzzificationCOG</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>URV</div>
        </td>
        <td>
          <div>Implementation of a defuzzification of set of fuzzy labels Java implementation of a defuzzificat ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-defuzzificationCOM-URV.html">defuzzificationCOM</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>URV</div>
        </td>
        <td>
          <div>Implementation of a defuzzification of set of fuzzy labels according to the COM method (Center o ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-defuzzificationOrdinal-URV.html">defuzzificationOrdinal</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>URV</div>
        </td>
        <td>
          <div>Implementation of a defuzzification of set of fuzzy labels according to their position. The firs ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-DEMATEL_influences-PUT.html">DEMATEL_influences</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes effects (in and out) and relations for given alternatives used in DEMATEL method.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-DEMATEL_relation-PUT.html">DEMATEL_relation</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes prominences and relations for given alternatives and effects (in and out) using DEMATEL ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ElectreComprehensiveDiscordanceIndex-PUT.html">ElectreComprehensiveDiscordanceIndex</a></div>
        </td>
        <td>
          <div>0.2.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes comprehensive discordance index from given discordance relation</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ElectreConcordance-PUT.html">ElectreConcordance</a></div>
        </td>
        <td>
          <div>0.2.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes concordance matrix using procedure which is common to the most methods from the Electre ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ElectreConcordance-J-MCDA.html">ElectreConcordance-J</a></div>
        </td>
        <td>
          <div>0.5.5</div>
        </td>
        <td>
          <div>J-MCDA</div>
        </td>
        <td>
          <div>Computes a concordance relation.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ElectreConcordanceReinforcedPreference-PUT.html">ElectreConcordanceReinforcedPreference</a></div>
        </td>
        <td>
          <div>0.1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes concordance matrix using procedure which is common to the most methods from the Electre ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ElectreConcordanceWithInteractions-PUT.html">ElectreConcordanceWithInteractions</a></div>
        </td>
        <td>
          <div>0.2.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes concordance matrix taking into account interactions between criteria. Possible interact ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ElectreCredibility-PUT.html">ElectreCredibility</a></div>
        </td>
        <td>
          <div>0.2.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>ElectreCredibility - computes credibility matrix using procedure which is common to the most met ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ElectreCredibilityWithCounterVeto-PUT.html">ElectreCredibilityWithCounterVeto</a></div>
        </td>
        <td>
          <div>0.1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>ElectreCredibilityWithCounterVeto - computes credibility matrix using procedure which is common  ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ElectreCrispOutrankingAggregation-PUT.html">ElectreCrispOutrankingAggregation</a></div>
        </td>
        <td>
          <div>0.2.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes outranking relation as an aggregation of concordance and discordance binary relations.  ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ElectreDiscordance-PUT.html">ElectreDiscordance</a></div>
        </td>
        <td>
          <div>0.2.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>ElectreDiscordance - computes partial (i.e. per-criterion) discordance matrix using procedure wh ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ElectreDiscordances-J-MCDA.html">ElectreDiscordances-J</a></div>
        </td>
        <td>
          <div>0.5.5</div>
        </td>
        <td>
          <div>J-MCDA</div>
        </td>
        <td>
          <div>Computes a discordance relation per criteria.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ElectreDistillation-PUT.html">ElectreDistillation</a></div>
        </td>
        <td>
          <div>0.2.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>This module performs distillation (upwards/downwards)</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ElectreDistillationRank-PUT.html">ElectreDistillationRank</a></div>
        </td>
        <td>
          <div>0.1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>This module provides three types of resultes performed on downwards and upwards distillation: in ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ElectreIIPreorder-PUT.html">ElectreIIPreorder</a></div>
        </td>
        <td>
          <div>0.2.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>This module constructs a complete pre-order using Electre II procedure based on the provided out ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ElectreIsDiscordanceBinary-PUT.html">ElectreIsDiscordanceBinary</a></div>
        </td>
        <td>
          <div>0.1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes discordance matrix as in Electre Is method. Resulting discordance indices are from rang ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ElectreIsFindKernel-PUT.html">ElectreIsFindKernel</a></div>
        </td>
        <td>
          <div>0.2.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>ElectreIsFindKernel - finds kernel of a graph (i.e. subset of best alternatives) according to th ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ElectreIVCredibility-PUT.html">ElectreIVCredibility</a></div>
        </td>
        <td>
          <div>0.2.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>ElectreIVCredibility - computes credibility matrix as presented in Electre IV method.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ElectreNFSOutranking-PUT.html">ElectreNFSOutranking</a></div>
        </td>
        <td>
          <div>0.2.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>This module computes Net Flow Score from given outranking (additionally with non_outranking). Ra ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ElectreOutranking-J-MCDA.html">ElectreOutranking-J</a></div>
        </td>
        <td>
          <div>0.5.5</div>
        </td>
        <td>
          <div>J-MCDA</div>
        </td>
        <td>
          <div>Computes an outranking relation.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ElectreTri-CClassAssignments-PUT.html">ElectreTri-CClassAssignments</a></div>
        </td>
        <td>
          <div>0.2.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes class assignments according to the Electre TRI-C method.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ElectreTri-rCClassAssignments-PUT.html">ElectreTri-rCClassAssignments</a></div>
        </td>
        <td>
          <div>0.2.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes class assignments according to the Electre TRI-rC method.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ElectreTri1GroupDisaggregationSharedProfiles-J-MCDA.html">ElectreTri1GroupDisaggregationSharedProfiles-J</a></div>
        </td>
        <td>
          <div>0.5.5</div>
        </td>
        <td>
          <div>J-MCDA</div>
        </td>
        <td>
          <div>Finds electre tri like models with shared profiles and individual weights that matches given gro ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ElectreTriBMInference-PyXMCDA.html">ElectreTriBMInference</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PyXMCDA</div>
        </td>
        <td>
          <div>This web service allows to infer parameters of a Bouyssou-Marchant Electre Tri relation on basis ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ElectreTriClassAssignments-PUT.html">ElectreTriClassAssignments</a></div>
        </td>
        <td>
          <div>0.2.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>ElectreTriClassAssignments - computes assignments according to the Electre TRI method. It genera ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ElectreTriExploitation-J-MCDA.html">ElectreTriExploitation-J</a></div>
        </td>
        <td>
          <div>0.5.5</div>
        </td>
        <td>
          <div>J-MCDA</div>
        </td>
        <td>
          <div>Computes assignments according to the Electre TRI procedure.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-fuzzyLabelsDescriptors-URV.html">fuzzyLabelsDescriptors</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>URV</div>
        </td>
        <td>
          <div>Two types of uncertainty in fuzzy sets are recognized: (1) specificity, related to the measureme ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-generalWeightedSum-UTAR.html">generalWeightedSum</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>UTAR</div>
        </td>
        <td>
          <div> -**Contact:** Boris Leistedt (boris.leistedt@gmail.com)</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-groupClassAcceptabilities-PUT.html">groupClassAcceptabilities</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Module for calculation group class acceptabilities basing on the assignments to classes of each  ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-HierarchicalDEA-CCR-SMAA_efficiencies-PUT.html">HierarchicalDEA-CCR-SMAA_efficiencies</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes efficiency scores for the given DMUs (alternatives) using SMAA-D method and CCR Data En ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-HierarchicalDEA-CCR-SMAA_preferenceRelations-PUT.html">HierarchicalDEA-CCR-SMAA_preferenceRelations</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Determines dominance relations for the given DMUs (alternatives) using SMAA-D method and CCR Dat ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-HierarchicalDEA-CCR-SMAA_ranks-PUT.html">HierarchicalDEA-CCR-SMAA_ranks</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes ranks for the given DMUs (alternatives) using SMAA-D method and CCR Data Envelopment An ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-HierarchicalDEA-CCR_efficiencies-PUT.html">HierarchicalDEA-CCR_efficiencies</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes efficiency scores for the given DMUs (alternatives) using CCR Data Envelopment Analysis ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-HierarchicalDEA-CCR_extremeRanks-PUT.html">HierarchicalDEA-CCR_extremeRanks</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes the extreme efficiency ranks for the given DMUs (alternatives) using CCR Data Envelopme ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-HierarchicalDEA-CCR_preferenceRelations-PUT.html">HierarchicalDEA-CCR_preferenceRelations</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes necessary and possible preference relations for the pairs of DMUs (alternatives) using  ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-HierarchicalDEA-ValueAdditive-SMAA_efficiencies-PUT.html">HierarchicalDEA-ValueAdditive-SMAA_efficiencies</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes efficiency scores for the given DMUs (alternatives) using SMAA-D method and Additive Da ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-HierarchicalDEA-ValueAdditive-SMAA_preferenceRelations-PUT.html">HierarchicalDEA-ValueAdditive-SMAA_preferenceRelations</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Determines dominance relations for the given DMUs (alternatives) using SMAA-D method and Additiv ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-HierarchicalDEA-ValueAdditive-SMAA_ranks-PUT.html">HierarchicalDEA-ValueAdditive-SMAA_ranks</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes ranks for the given DMUs (alternatives) using SMAA-D method and Additive Data Envelopme ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-HierarchicalDEA-ValueAdditive_efficiencies-PUT.html">HierarchicalDEA-ValueAdditive_efficiencies</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes efficiency scores for the given DMUs (alternatives) using Additive Data Envelopment Ana ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-HierarchicalDEA-ValueAdditive_extremeRanks-PUT.html">HierarchicalDEA-ValueAdditive_extremeRanks</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes extreme efficiency ranks for the given DMUs (alternatives) using Additive Data Envelopm ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-HierarchicalDEA-ValueAdditive_preferenceRelations-PUT.html">HierarchicalDEA-ValueAdditive_preferenceRelations</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes necessary and possible preference relations for the pairs of DMUs (alternatives) using  ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ImpreciseDEA-CCR-SMAA_efficiencies-PUT.html">ImpreciseDEA-CCR-SMAA_efficiencies</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes efficiency scores for the given DMUs (alternatives) using SMAA-D method and CCR Data En ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ImpreciseDEA-CCR-SMAA_preferenceRelations-PUT.html">ImpreciseDEA-CCR-SMAA_preferenceRelations</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Determines dominance relations for the given DMUs (alternatives) using SMAA-D method and CCR Dat ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ImpreciseDEA-CCR-SMAA_ranks-PUT.html">ImpreciseDEA-CCR-SMAA_ranks</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes ranks for the given DMUs (alternatives) using SMAA-D method and CCR Data Envelopment An ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ImpreciseDEA-CCR_efficiencies-PUT.html">ImpreciseDEA-CCR_efficiencies</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes efficiency scores for the given DMUs (alternatives)  using CCR Data Envelopment Analysi ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ImpreciseDEA-CCR_extremeRanks-PUT.html">ImpreciseDEA-CCR_extremeRanks</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes extrene efficiency ranks for the given DMUs (alternatives) using CCR Data Envelopment A ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ImpreciseDEA-CCR_preferenceRelations-PUT.html">ImpreciseDEA-CCR_preferenceRelations</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes extrene efficiency ranks for the given DMUs (alternatives) using CCR Data Envelopment A ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ImpreciseDEA-ValueAdditive-SMAA_efficiencies-PUT.html">ImpreciseDEA-ValueAdditive-SMAA_efficiencies</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes efficiency scores for the given DMUs (alternatives) using SMAA-D method and Additive Da ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations-PUT.html">ImpreciseDEA-ValueAdditive-SMAA_preferenceRelations</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Determines dominance relations for the given DMUs (alternatives) using SMAA-D method and Additiv ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ImpreciseDEA-ValueAdditive-SMAA_ranks-PUT.html">ImpreciseDEA-ValueAdditive-SMAA_ranks</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes ranks for the given DMUs (alternatives) using SMAA-D method and Additive Data Envelopme ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ImpreciseDEA-ValueAdditive_efficiencies-PUT.html">ImpreciseDEA-ValueAdditive_efficiencies</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes efficiency scores for the given DMUs (alternatives)  using Additive Data Envelopment An ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ImpreciseDEA-ValueAdditive_extremeRanks-PUT.html">ImpreciseDEA-ValueAdditive_extremeRanks</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes efficiency scores for the given DMUs (alternatives)  using Additive Data Envelopment An ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ImpreciseDEA-ValueAdditive_preferenceRelations-PUT.html">ImpreciseDEA-ValueAdditive_preferenceRelations</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes efficiency scores for the given DMUs (alternatives)  using Additive Data Envelopment An ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-invertAlternativesRanks-RXMCDA.html">invertAlternativesRanks</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>RXMCDA</div>
        </td>
        <td>
          <div>Inverts ranks of alternatives (the highest rank becomes the lowest, etc.).</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-LearnMRSortMeta-oso.html">LearnMRSortMeta</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>oso</div>
        </td>
        <td>
          <div>The purpose of this webservice is to infer parameters of an ELECTRE TRI Bouyssou-Marchant model, ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-LearnMRSortMIP-oso.html">LearnMRSortMIP</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>oso</div>
        </td>
        <td>
          <div>The purpose of this webservice is to infer parameters of an ELECTRE TRI Bouyssou-Marchant model. ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-leastSquaresCapaIdent-kappalab.html">leastSquaresCapaIdent</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>kappalab</div>
        </td>
        <td>
          <div>Identifies a Mobius capacity by means of an approach grounded on least squares optimization. Mor ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-linProgCapaIdent-kappalab.html">linProgCapaIdent</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>kappalab</div>
        </td>
        <td>
          <div>Identifies a Mobius capacity by means of an approach using the linear programming approach propo ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-lsRankingCapaIdent-kappalab.html">lsRankingCapaIdent</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>kappalab</div>
        </td>
        <td>
          <div>Identifies a Mobius capacity by means of an approach called TOMASO in the particular ranking fra ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-mccClusters-ws-Mcc.html">mccClusters-ws</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>ws-Mcc</div>
        </td>
        <td>
          <div>This web service computes clusters of alternatives based on the following clustering typology: n ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-mccClustersRelationSummary-ws-Mcc.html">mccClustersRelationSummary-ws</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>ws-Mcc</div>
        </td>
        <td>
          <div>This web service gives a summary of the preference relations between a set of clusters of altern ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-mccEvaluateClusters-ws-Mcc.html">mccEvaluateClusters-ws</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>ws-Mcc</div>
        </td>
        <td>
          <div>This web service evaluates clusters of alternatives based on the following clustering typology:  ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-mccPlotClusters-ws-Mcc.html">mccPlotClusters-ws</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>ws-Mcc</div>
        </td>
        <td>
          <div>This web service gives 4 different representations for a set of clusters of alternatives. There  ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-mccPreferenceRelation-ws-Mcc.html">mccPreferenceRelation-ws</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>ws-Mcc</div>
        </td>
        <td>
          <div>This web service allows the generation of a preference relation between a set of alternatives fr ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-miniVarCapaIdent-kappalab.html">miniVarCapaIdent</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>kappalab</div>
        </td>
        <td>
          <div>Identifies a Mobius capacity by means of an approach using a maximum like quadratic entropy prin ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-multipleAlternativesValuesKendalls-RXMCDA.html">multipleAlternativesValuesKendalls</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>RXMCDA</div>
        </td>
        <td>
          <div>Calculates the correlation index between multiple rankings of alternatives (represented by lists ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-NetFlow-Iterative_ranking-PUT.html">NetFlow-Iterative_ranking</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Module for calculation flows of Net Flow Score in iterative way using chosen function and direct ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-NetFlow_scores-PUT.html">NetFlow_scores</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Module for calculation flows of Net Flow Score using chosen function and direction in parameters.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-orderedClustering-PUT.html">orderedClustering</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>OrderedClustering - clustering method using graphs and flows</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-Outranking-PrefRank-WithPreferenceInformation_scores-PUT.html">Outranking-PrefRank-WithPreferenceInformation_scores</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Module for calculation PrefRank scores using preference information given by decision maker.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-Outranking-PrefRank_scores-PUT.html">Outranking-PrefRank_scores</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Module for calculation outranking PrefRank scores.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-Outranking-ScoreBin-WithPreferenceInformation_scores-PUT.html">Outranking-ScoreBin-WithPreferenceInformation_scores</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Module for calculation ScoreBin scores using preference information given by decision maker. The ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-Outranking-ScoreBin_scores-PUT.html">Outranking-ScoreBin_scores</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Module for calculation ScoreBin scores. The preferences can be given as a complete pairwise comp ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-OWA-URV.html">OWA</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>URV</div>
        </td>
        <td>
          <div>The Ordered Weighted Averaging operators, commonly called OWA operators, provide a parameterized ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-OWAWeightsBalance-URV.html">OWAWeightsBalance</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>URV</div>
        </td>
        <td>
          <div>Balance of the weights given to the Ordered Weighted Average operator (OWA)</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-OWAWeightsDivergence-URV.html">OWAWeightsDivergence</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>URV</div>
        </td>
        <td>
          <div>Divergence of the weights given to the Ordered Weighted Average operator (OWA)</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-OWAWeightsEntropy-URV.html">OWAWeightsEntropy</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>URV</div>
        </td>
        <td>
          <div>Entropy of the weights given to the Ordered Weighted Average operator (OWA)</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-OWAWeightsOrness-URV.html">OWAWeightsOrness</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>URV</div>
        </td>
        <td>
          <div>Orness of the weights given to the Ordered Weighted Average operator (OWA)</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-PairwiseComparisons-HeuristicRatingEstimation-AGH.html">PairwiseComparisons-HeuristicRatingEstimation</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>AGH</div>
        </td>
        <td>
          <div>Computes HRE (Heuristic rating estimation) method - approach proposes a new way of using pairwis ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-performanceTableFilter-ITTB.html">performanceTableFilter</a></div>
        </td>
        <td>
          <div>1.1</div>
        </td>
        <td>
          <div>ITTB</div>
        </td>
        <td>
          <div>This web service generates a filtered performance table. A file containing methodParameters tag  ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-plotAlternativesAssignments-ITTB.html">plotAlternativesAssignments</a></div>
        </td>
        <td>
          <div>1.1</div>
        </td>
        <td>
          <div>ITTB</div>
        </td>
        <td>
          <div>This web service generates a plot representing the alternatives assignments. Colors can be used. ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-plotAlternativesComparisons-ITTB.html">plotAlternativesComparisons</a></div>
        </td>
        <td>
          <div>1.1</div>
        </td>
        <td>
          <div>ITTB</div>
        </td>
        <td>
          <div>This web service generates a graph representing a partial preorder on the alternatives. Compared ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-plotAlternativesHasseDiagram-PUT.html">plotAlternativesHasseDiagram</a></div>
        </td>
        <td>
          <div>0.2</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Draws Hasse diagram - visualization of transitive reduction of a finite partially ordered set. S ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-plotAlternativesValues-ITTB.html">plotAlternativesValues</a></div>
        </td>
        <td>
          <div>1.1</div>
        </td>
        <td>
          <div>ITTB</div>
        </td>
        <td>
          <div>This web service generates a barplot or a pie plot representing a numeric quantity for each alte ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-plotAlternativesValuesPreorder-ITTB.html">plotAlternativesValuesPreorder</a></div>
        </td>
        <td>
          <div>1.1</div>
        </td>
        <td>
          <div>ITTB</div>
        </td>
        <td>
          <div>This web service generates a graph representing a preorder on the alternatives, according to num ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-plotClassAssignments-LaTeX-PUT.html">plotClassAssignments-LaTeX</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Visualizes class assignments for given alternatives as a single latex table.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-plotClassAssignments-png-PUT.html">plotClassAssignments-png</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Visualizes class assignments for given alternatives as an image.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-plotCriteriaComparisons-ITTB.html">plotCriteriaComparisons</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>ITTB</div>
        </td>
        <td>
          <div>This web service generates a graph representing a partial preorder on the criteria. Compared to  ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-plotCriteriaValues-ITTB.html">plotCriteriaValues</a></div>
        </td>
        <td>
          <div>1.1</div>
        </td>
        <td>
          <div>ITTB</div>
        </td>
        <td>
          <div>This web service generates a barplot or a pie plot representing a numeric quantity for each crit ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-plotCriteriaValuesPreorder-ITTB.html">plotCriteriaValuesPreorder</a></div>
        </td>
        <td>
          <div>1.1</div>
        </td>
        <td>
          <div>ITTB</div>
        </td>
        <td>
          <div>This web service generates a graph representing a preorder on the criteria, according to numeric ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-plotFuzzyCategoriesValues-ITTB.html">plotFuzzyCategoriesValues</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>ITTB</div>
        </td>
        <td>
          <div>This web service allows to plot fuzzy sets. There are some options to take into account: title,  ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-plotGaiaPlane-RXMCDA.html">plotGaiaPlane</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>RXMCDA</div>
        </td>
        <td>
          <div>Generates the Gaia Plane from decomposed Promethee flows (a.k.a Promethee profiles).</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-plotNumericPerformanceTable-ITTB.html">plotNumericPerformanceTable</a></div>
        </td>
        <td>
          <div>1.1</div>
        </td>
        <td>
          <div>ITTB</div>
        </td>
        <td>
          <div>This web service generates a plot representing the performance table (abscissa: alternatives). C ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-plotStarGraphPerformanceTable-ITTB.html">plotStarGraphPerformanceTable</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>ITTB</div>
        </td>
        <td>
          <div>This web service generates, for each alternative, a plot representing the performance table as a ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-plotValueFunctions-ITTB.html">plotValueFunctions</a></div>
        </td>
        <td>
          <div>1.1</div>
        </td>
        <td>
          <div>ITTB</div>
        </td>
        <td>
          <div>This web service allows to plot utility functions. Compared to the web service plotValueFunction ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-PROMETHEE-Cluster-PUT.html">PROMETHEE-Cluster</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Clustering using Promethee Tri and k-means algorithm</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-PROMETHEE-FlowSort-GDSS_assignments-PUT.html">PROMETHEE-FlowSort-GDSS_assignments</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes group class assignment for given data using FlowSortGDSS method.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-PROMETHEE-I-FlowSort_assignments-PUT.html">PROMETHEE-I-FlowSort_assignments</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes class assignment for given alternatives using FlowSort method based on Promethee I.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-PROMETHEE-I_ranking-PUT.html">PROMETHEE-I_ranking</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>This module is calculating ranking using Promethee I method. The result is shown as the table of ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-PROMETHEE-II-FlowSort_assignments-PUT.html">PROMETHEE-II-FlowSort_assignments</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes class assignment for given alternatives using FlowSort method based on Promethee II.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-PROMETHEE-II-GDSS_flows-PUT.html">PROMETHEE-II-GDSS_flows</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Module for calculation PROMETHEE group ranking as a weighted sum of flows from every decision ma ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-PROMETHEE-II_flows-PUT.html">PROMETHEE-II_flows</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Final flows calculated with Promethee II method as a difference of positive and negative outrank ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-PROMETHEE-II_orderedClustering-PUT.html">PROMETHEE-II_orderedClustering</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Clustering using Promethee II and k-means algorithm</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-PROMETHEE-III_flows-PUT.html">PROMETHEE-III_flows</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>This module is calculating ranking using Promethee III method. The result is shown as the table  ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-PROMETHEE-PROMSORT_assignments-PUT.html">PROMETHEE-PROMSORT_assignments</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes class assignment for given alternatives using PromSort method. This method consists of  ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-PROMETHEE-TRI_assignments-PUT.html">PROMETHEE-TRI_assignments</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes class assignment for given alternatives using Promethee Tri method.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-PROMETHEE-V-PUT.html">PROMETHEE-V</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Promethee V - choose alternatives using linear constraints.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-Promethee1Ranking-RXMCDA.html">Promethee1Ranking</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>RXMCDA</div>
        </td>
        <td>
          <div>Calculates the Promethee 1 ranking from the positive and the negative flows.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-PROMETHEE_alternativesProfiles-PUT.html">PROMETHEE_alternativesProfiles</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes the profiles of the alternatives (the single criterion net flow for each alternative an ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-PROMETHEE_discordance-PUT.html">PROMETHEE_discordance</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes aggregated discordance indices and partial discordance indices.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-PROMETHEE_outrankingFlows-PUT.html">PROMETHEE_outrankingFlows</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes positive and negative outranking flows using aggregated preference indices</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-PROMETHEE_preference-PUT.html">PROMETHEE_preference</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes aggregated preference indices</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-PROMETHEE_preference-interactions-PUT.html">PROMETHEE_preference-interactions</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes aggregated preference indices with interactions between criteria</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-PROMETHEE_preference-reinforcedPreference-PUT.html">PROMETHEE_preference-reinforcedPreference</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes aggregated preference indices with reinforced preference effect</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-PROMETHEE_preferenceDiscordance-PUT.html">PROMETHEE_preferenceDiscordance</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Aggregate preferences (concordance) and discordances(or veto) into one preferences file.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-PROMETHEE_veto-PUT.html">PROMETHEE_veto</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes veto indices which</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-PrometheeFlows-J-MCDA.html">PrometheeFlows-J</a></div>
        </td>
        <td>
          <div>0.5.5</div>
        </td>
        <td>
          <div>J-MCDA</div>
        </td>
        <td>
          <div>Computes Promethee flows (net flows, positive flows, or negative flows).</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-PrometheePreference-J-MCDA.html">PrometheePreference-J</a></div>
        </td>
        <td>
          <div>0.5.5</div>
        </td>
        <td>
          <div>J-MCDA</div>
        </td>
        <td>
          <div>Computes a preference relation, a la Promethee.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-PrometheeProfiles-J-MCDA.html">PrometheeProfiles-J</a></div>
        </td>
        <td>
          <div>0.5.5</div>
        </td>
        <td>
          <div>J-MCDA</div>
        </td>
        <td>
          <div>Computes the profiles of the given alternatives on the given criteria, a la Promethee.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-randomAlternatives-PyXMCDA.html">randomAlternatives</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PyXMCDA</div>
        </td>
        <td>
          <div>This web service allows to create a simple list of alternative by simply providing the desired n ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-randomAlternativesRanks-RXMCDA.html">randomAlternativesRanks</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>RXMCDA</div>
        </td>
        <td>
          <div>Generates random (uniformly distributed) ranks of alternatives.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-randomCriteria-PyXMCDA.html">randomCriteria</a></div>
        </td>
        <td>
          <div>1.1</div>
        </td>
        <td>
          <div>PyXMCDA</div>
        </td>
        <td>
          <div>This web service allows to create a simple list of criteria by providing the desired number of c ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-randomCriteriaWeights-RXMCDA.html">randomCriteriaWeights</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>RXMCDA</div>
        </td>
        <td>
          <div>Generates random (uniformly distributed) and normalized (sum = 1) weights of criteria.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-randomNormalizedPerformanceTable-RXMCDA.html">randomNormalizedPerformanceTable</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>RXMCDA</div>
        </td>
        <td>
          <div>Generates a performance table with numeric values in the real unit interval, from a uniform dist ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-randomPerformanceTable-PyXMCDA.html">randomPerformanceTable</a></div>
        </td>
        <td>
          <div>1.1</div>
        </td>
        <td>
          <div>PyXMCDA</div>
        </td>
        <td>
          <div>This web service allows to create a simple performance table by providing a list of alternatives ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-rankAlternativesValues-RXMCDA.html">rankAlternativesValues</a></div>
        </td>
        <td>
          <div>1.1</div>
        </td>
        <td>
          <div>RXMCDA</div>
        </td>
        <td>
          <div>Calculates the rank of alternatives via their overall values. A parameter named maxMin allows to ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-RORUTA-ExtremeRanks-PUT.html">RORUTA-ExtremeRanks</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Finds the best and worst possible positions of alternatives in the final ranking taking into con ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-RORUTA-ExtremeRanksHierarchical-PUT.html">RORUTA-ExtremeRanksHierarchical</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Finds the best and worst possible positions of alternatives in the final ranking taking into con ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-RORUTA-NecessaryAndPossiblePreferenceRelations-PUT.html">RORUTA-NecessaryAndPossiblePreferenceRelations</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Finds all possible and necessary weak preference relations on the set of alternatives.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-RORUTA-NecessaryAndPossiblePreferenceRelationsHierarchical-PUT.html">RORUTA-NecessaryAndPossiblePreferenceRelationsHierarchical</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Finds all possible and necessary weak preference relations on the set of alternatives. It suppor ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-RORUTA-PairwiseOutrankingIndices-PUT.html">RORUTA-PairwiseOutrankingIndices</a></div>
        </td>
        <td>
          <div>1.1</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>The procedure finds pairwise outranking indices matrix for a given problem.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-RORUTA-PairwiseOutrankingIndicesHierarchical-PUT.html">RORUTA-PairwiseOutrankingIndicesHierarchical</a></div>
        </td>
        <td>
          <div>1.1</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Procedure finds a set of matrices of the pair-wise outranking indices  for a given problem and a ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-RORUTA-PostFactum-PreferenceRelatedDeteriorationOrSurplusValue-PUT.html">RORUTA-PostFactum-PreferenceRelatedDeteriorationOrSurplusValue</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Depending on the need, for a given already fulfilled preference relation between two alternative ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-RORUTA-PostFactum-PreferenceRelatedDeteriorationOrSurplusValueHierarchical-PUT.html">RORUTA-PostFactum-PreferenceRelatedDeteriorationOrSurplusValueHierarchical</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Depending on the need, for a given already fulfilled preference relation between two alternative ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue-PUT.html">RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValue</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Depending on the need, for a given not satisified preference relation between two alternatives:  ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValueHierarchical-PUT.html">RORUTA-PostFactum-PreferenceRelatedImprovementOrMissingValueHierarchical</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Depending on the need, for a given not satisfied preference relation between two alternatives: " ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue-PUT.html">RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValue</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>For a given ranking position n and the alternative a that already attains at least n-th position ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValueHierarchical-PUT.html">RORUTA-PostFactum-RankRelatedDeteriorationOrSurplusValueHierarchical</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>For a given ranking position n and the alternative a that already attains at least n-th position ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-RORUTA-PostFactum-RankRelatedImprovementOrMissingValue-PUT.html">RORUTA-PostFactum-RankRelatedImprovementOrMissingValue</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>For a given ranking position n and the alternative a which is worse than n-th, depending on the  ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical-PUT.html">RORUTA-PostFactum-RankRelatedImprovementOrMissingValueHierarchical</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>For a given ranking position n and the alternative a which is worse than n-th, depending on the  ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-RORUTA-PreferentialReductsForNecessaryRelations-PUT.html">RORUTA-PreferentialReductsForNecessaryRelations</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Finds all preferential reducts for the necessary relations. In other words, for each necessary w ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-RORUTA-PreferentialReductsForNecessaryRelationsHierarchical-PUT.html">RORUTA-PreferentialReductsForNecessaryRelationsHierarchical</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>The module finds all preferential reducts for the necessary relations. In other words, for each  ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-RORUTA-RankAcceptabilityIndices-PUT.html">RORUTA-RankAcceptabilityIndices</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Finds rank acceptability indices matrix for the given problem.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-RORUTA-RankAcceptabilityIndicesHierarchical-PUT.html">RORUTA-RankAcceptabilityIndicesHierarchical</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>The module finds rank acceptability indices matrix for a given problem. it supports a hierarchic ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-RORUTA-RankRelatedPreferentialReducts-PUT.html">RORUTA-RankRelatedPreferentialReducts</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Finds all rank related preferential reducts. In other words, for each alternative, it finds all  ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-RORUTA-RankRelatedPreferentialReductsHierarchical-PUT.html">RORUTA-RankRelatedPreferentialReductsHierarchical</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Finds all rank related preferential reducts. In other words, for each alternative, it finds all  ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-RORUTA-RepresentativeValueFunction-PUT.html">RORUTA-RepresentativeValueFunction</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>The procedure finds a representative value function for a given set of necessary relations.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-RORUTA-RepresentativeValueFunctionHierarchical-PUT.html">RORUTA-RepresentativeValueFunctionHierarchical</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>The procedure finds a representative value function for a given set of necessary relations. It s ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-RORUTADIS-ExtremeClassCardinalities-PUT.html">RORUTADIS-ExtremeClassCardinalities</a></div>
        </td>
        <td>
          <div>0.1</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Robust Ordinal Regression for value-based sorting: RORUTADIS-ExtremeClassCardinalities calculate ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-RORUTADIS-GroupAssignments-PUT.html">RORUTADIS-GroupAssignments</a></div>
        </td>
        <td>
          <div>0.1</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>This service allows for merging different assignments, e.g. from various decision makers (group  ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-RORUTADIS-NecessaryAssignment-basedPreferenceRelation-PUT.html">RORUTADIS-NecessaryAssignment-basedPreferenceRelation</a></div>
        </td>
        <td>
          <div>0.1</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Compares necessary assignments of alternatives using Robust Ordinal Regression for value-based s ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-RORUTADIS-PossibleAndNecessaryAssignments-PUT.html">RORUTADIS-PossibleAndNecessaryAssignments</a></div>
        </td>
        <td>
          <div>0.1</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Calculates possible and necessary assignments of alternatives to classes (categories) using Robu ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-RORUTADIS-PostFactum-InvestigatePerformanceDeterioration-PUT.html">RORUTADIS-PostFactum-InvestigatePerformanceDeterioration</a></div>
        </td>
        <td>
          <div>0.3</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Robust Ordinal Regression for value-based sorting: RORUTADIS-PostFactum-InvestigatePerformanceDe ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-RORUTADIS-PostFactum-InvestigatePerformanceImprovement-PUT.html">RORUTADIS-PostFactum-InvestigatePerformanceImprovement</a></div>
        </td>
        <td>
          <div>0.3</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Robust Ordinal Regression for value-based sorting: RORUTADIS-PostFactum-InvestigatePerformanceIm ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-RORUTADIS-PostFactum-InvestigateValueChange-PUT.html">RORUTADIS-PostFactum-InvestigateValueChange</a></div>
        </td>
        <td>
          <div>0.3</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Robust Ordinal Regression for value-based sorting: RORUTADIS-PostFactum-InvestigateValueChange s ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-RORUTADIS-PreferentialReducts-PUT.html">RORUTADIS-PreferentialReducts</a></div>
        </td>
        <td>
          <div>0.1</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Robust Ordinal Regression for value-based sorting: RORUTADIS-PreferentialReducts service allows  ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-RORUTADIS-RepresentativeValueFunction-PUT.html">RORUTADIS-RepresentativeValueFunction</a></div>
        </td>
        <td>
          <div>0.3</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Robust Ordinal Regression for value-based sorting: RORUTADIS-RepresentativeValueFunction service ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-RORUTADIS-StochasticResults-PUT.html">RORUTADIS-StochasticResults</a></div>
        </td>
        <td>
          <div>0.1</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Calculates stochastic results for alternative assignments, assignment-based preference relation  ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-RubisConcordanceRelation-PyXMCDA.html">RubisConcordanceRelation</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PyXMCDA</div>
        </td>
        <td>
          <div>This web service allows to compute a concordance relation as defined in the Rubis methodology.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-RubisOutrankingRelation-PyXMCDA.html">RubisOutrankingRelation</a></div>
        </td>
        <td>
          <div>1.1</div>
        </td>
        <td>
          <div>PyXMCDA</div>
        </td>
        <td>
          <div>This web service allows to compute an outranking relation as defined in the Rubis methodology.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-smaa2-jsmaa.html">smaa2</a></div>
        </td>
        <td>
          <div>0.2</div>
        </td>
        <td>
          <div>jsmaa</div>
        </td>
        <td>
          <div>JSMAA adapter for DD. Currently implements only SMAA-2 without preference information and only c ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-sortAlternativesValues-ITTB.html">sortAlternativesValues</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>ITTB</div>
        </td>
        <td>
          <div>This web service computes affectations of the alternatives from the given input data (3 inputs:  ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-SRF_weights-PUT.html">SRF_weights</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>SRFWeights - computes weights of criteria using the revised Simos procedure</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-SRMPaggregation-LJY.html">SRMPaggregation</a></div>
        </td>
        <td>
          <div>1.2</div>
        </td>
        <td>
          <div>LJY</div>
        </td>
        <td>
          <div>Execute the S-RMP model based aggregation process for a series of alternatives with a given set  ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-SRMPdisaggregationNoInconsistency-LJY.html">SRMPdisaggregationNoInconsistency</a></div>
        </td>
        <td>
          <div>1.2</div>
        </td>
        <td>
          <div>LJY</div>
        </td>
        <td>
          <div>Elicit a S-RMP model from a set of pairwise comparisons without inconsistency. The algorithm wil ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-stableSorting-PyXMCDA.html">stableSorting</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PyXMCDA</div>
        </td>
        <td>
          <div>This web service allows to compute the affectations of some alternatives in predefined categorie ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-surrogateWeights-PUT.html">surrogateWeights</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>SurrogateWeights - computes weights of criteria using ranking specified by user. Module allows t ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-thresholdsSensitivityAnalysis-PyXMCDA.html">thresholdsSensitivityAnalysis</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PyXMCDA</div>
        </td>
        <td>
          <div>This web service allows to compute intervals of stability on indifference and preference thresho ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-TOPSIS_extremeAlternatives-PUT.html">TOPSIS_extremeAlternatives</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes positive and negative ideal alternatives from given alternatives.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-TOPSIS_normalizationAndWeighting-PUT.html">TOPSIS_normalizationAndWeighting</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes normalized and weighted performance for each alternative.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-TOPSIS_ranking-PUT.html">TOPSIS_ranking</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes ranking by distance ratio (nDist/(nDist+pDist) to ideal alternatives</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-transitiveReductionAlternativesComparisons-ITTB.html">transitiveReductionAlternativesComparisons</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>ITTB</div>
        </td>
        <td>
          <div>This web service applies a transitive reduction on the relation relative to comparisons of the a ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-transitiveReductionCriteriaComparisons-ITTB.html">transitiveReductionCriteriaComparisons</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>ITTB</div>
        </td>
        <td>
          <div>This web service applies a transitive reduction on the relation relative to comparisons of the c ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-ULOWA-URV.html">ULOWA</a></div>
        </td>
        <td>
          <div>1.1</div>
        </td>
        <td>
          <div>URV</div>
        </td>
        <td>
          <div>This module implements the ULOWA aggregation operator: Unbalanced Linguistic Ordered Weighted Av ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-UTA-ITTB.html">UTA</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>ITTB</div>
        </td>
        <td>
          <div>Computes UTA method and if necessary uses post-optimality analysis.</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-UTASTAR-UTAR.html">UTASTAR</a></div>
        </td>
        <td>
          <div>1.1</div>
        </td>
        <td>
          <div>UTAR</div>
        </td>
        <td>
          <div>Computes UTASTAR method and if necessary uses post-optimality analysis among three well-known po ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-VIKOR_ranking-PUT.html">VIKOR_ranking</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes compromise alternatives and vector `q` for given alternatives, according to 2 indexes - ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-VIKOR_SRVectors-PUT.html">VIKOR_SRVectors</a></div>
        </td>
        <td>
          <div>1.0.0</div>
        </td>
        <td>
          <div>PUT</div>
        </td>
        <td>
          <div>Computes Manhatan and Chebyshev distances for given alternatives, which i.e. may be later used i ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-weightedSum-PyXMCDA.html">weightedSum</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>PyXMCDA</div>
        </td>
        <td>
          <div>This web service allows to compute the weighted sum of alternatives's evaluations through criter ...</div>
        </td>
      </tr>
      <tr>
        <td>
          <div><a href="wsd-XYPlotAlternativesValues-UTAR.html">XYPlotAlternativesValues</a></div>
        </td>
        <td>
          <div>1.0</div>
        </td>
        <td>
          <div>UTAR</div>
        </td>
        <td>
          <div>Give a graphical XY reprentation of two alternativesValues (ex: ranking), providing an efficient ...</div>
        </td>
      </tr>
    </table>
    <script>
    function myFunction() {
      var input, filter, table, tr, td, i;
      input = document.getElementById("myInput");
      filter = input.value.toUpperCase();
      table = document.getElementById("myTable");
      tr = table.getElementsByTagName("tr");
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
          if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }
      }
    }
    </script>
