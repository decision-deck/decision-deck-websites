.. _contact:
.. index:: contact, bug report, feature request, information

Contact details
===============

The XMCDA web services are an initiative within the Decision Deck project. 

The contact person for anything related to the web services is:

**Sébastien Bigaret**

| Institut Télécom; Télécom Bretagne
| LUSSI Department
| Technopôle Brest-Iroise CS 83818
| F-29238 Brest Cedex 3
| France

sebastien.bigaret@telecom-bretagne.eu
