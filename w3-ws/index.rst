.. index:: MCDA, Decision Deck
.. _index:

.. raw:: html

    <div id="title" class="text-center">
      <img class="img-responsive" src="_static/xmcdawslogo.png" style="max-height:70px;display:block;margin: 20px auto;">
      <h2>Distributed MCDA calculation resources</h2>
      <p>Decision Deck's XMCDA web services are algorithmic components or complete Multicriteria Decision Aid (MCDA) methods which are made available online.</p>
    </div>


The XMCDA web services use the `XMCDA <//www.decision-deck.org/xmcda/index.html>`_ data standard to be interoperable.

The XMCDA web services can be accessed via:

 - Decision Deck's `diviz <//www.diviz.org/index.html>`_ software;
 - standard `SOAP <https://en.wikipedia.org/wiki/SOAP>`_ requests;

The XMCDA web services are an initiative  within the `Decision Deck <//www.decision-deck.org/>`_ project.

XMCDA web services
------------------

.. toctree::
   :maxdepth: 1

   webservices_xmcda-v2/wsindex-1
   webservices_xmcda-v4/wsindex-1
   accessing

General information
-------------------

.. toctree::
   :maxdepth: 1

   features
   contact

Developer's corner
------------------
.. toctree::
   :maxdepth: 1

   howtos
   downloads
