.. _howto.ws.RXMCDA:

How to create an *web service-able* program in R with the RXMCDA parsing library?
=================================================================================

.. toctree::
   :maxdepth: 1

We detail here a quick recipe to help creating an R script which can easily be added in the Decision Deck web services framework (and also in diviz). 

  .. note:: Most of the details can be found in the source files available below. Do not hesitate to :ref:`contact <contact>` us for further assistance!

Download
--------

The complete source files of this dummy R script can be found `here <httpshttps://gitlab.com/decision-deck/decision-deck-webservices-infrastructure/tree/master/XMCDAWebServicesData/dummyRXMCDAService>`_. If you do not wish to use git to download these sources, you can click on "Download" in the upper right part of the page to obtain an archive containing all the necessary files. 

Quick guide
-----------

Most of the information given in this quick tutorial can also be found in the source files. The R scripts have been tested on Linux.

The R script which is described here generates a random vector of ranks of alternatives. The IDs of the alternatives are specified in an input file called **alternatives.xml**. It is possible to specify a *seed* in a file called **seed.xml**. The generated ranks are stored in a file called **alternativesRanks.xml** and some information about the execution is given in **messages.xml**.

In order to test the R script, you must first install the RXMCDA parsing library which is available from `github <https://github.com/paterijk/RXMCDA>`_ (we assume that you have already installed R). 

Then you can run the script (from the directory that you downloaded) via the following command:

  ::
    
    R --slave --vanilla --args "${PWD}/in" "${PWD}/out" < dummy.R

The script then reads the previously mentionned input data files from the "in" directory and writes its output data files to the "out" directory. 

The **seed.xml** file is an optional file. If it is not found, then the random number generator of R is initialized via the current time.

The loaded files are then checked if they are valid according to the (current) XMCDA schema.

If everything works without errors up to here, the data is extracted from the loaded XMCDA trees; in this case, the alternatives' IDs, the seed (if necessary) and the number of alternatives.

Then the calculations are performed to generate the random ranks of the alternatives. 

Finally, the output files are written. Four possibilities appear at this stage:

 - If everything went right with the calculation, then we write the output data into the right files: 
   **alternativesRanks.xml** containing the generated ranks, and **messages.xml** containing an execution status.
 - If there was an error during the calculation, we write an error message in **messages.xml**.
 - If there was an error during the extraction of the data from the XMCDA trees, we write an error in **messages.xml**.
 - If there was an error during the loading of the files, we write an error in **messages.xml**.

You can find the output files in the "out" directory. 

.. index:: web service example, XMCDA, RXMCDA
