.. index:: HOWTO

Howtos
======

On this page you can find howto's for various topics. 

First some documentation on how to write and submit a program which is to be integrated in Decision Deck's web services framework. 

.. toctree::
   :maxdepth: 1
   :glob:

   howto.createAWebService

Then some documentation on how to write programs written in specific programming languages. 

.. toctree::
   :maxdepth: 1
   :glob:

   example.*

And more:

- How to create an XMCDA web service in Java? Please see the `J-MCDA project <https://sourceforge.net/apps/mediawiki/j-mcda/>`_ page (follow the link to the relevant how to).
 
