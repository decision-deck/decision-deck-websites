.. index:: download

Downloads
=========

The XMCDA web services can be accessed via various techniques (see :ref:`tutorials<accessing>`).

However, they can also be downloaded for local testing or if you wish to reuse them to setup your own web services server. 

Note that these downloads can also be of some interest to web services developpers who wish to have a look at what has already been done!

.. toctree::
   :maxdepth: 1
   :glob:

   download.*


