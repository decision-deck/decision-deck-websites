.. index:: specification.xml
.. _specification:

How to document your program / web service? 
===========================================

.. toctree::
   :maxdepth: 1

Summary
*******

All technical details concerning the program which you are submitting for inclusion into Decision Deck's web services infrastructure works is stored in a file called **description-wsDD.xml**. It also contains information on how it should be made accessible from within diviz. This guide explains how to write such a descriptive file. 

  **Note:** this file contains the necessary informations to generate a documentation of the web service and to generate any graphical user interface (GUI) in third-party software offering an access to the web services.  We here use the diviz software as an example to illustrate the aspects linked to the generation of GUI.

Introduction
************

The ``description-wsDD.xml`` file allows to describe your program which you wish to integrate into Decision Deck's web-services framework (and into the diviz software). We describe here its entries. 

The details on how to create and submit a program for its integration into the web-services framework are given :ref:`here<howto.ws>`. 

Recall however that a successful integration of your program into Decision Deck's web services framework requires that it is capable of reading and writing files according to the `XMCDA <//www.decision-deck.org/xmcda>`_ standard.

Once your program has been made available as a web service, it can be integrated into diviz. However, in diviz like in any graphical program, it makes sense to have the possibility to modify manually the values of certain input elements via a dialog box related to your program, instead of having to modify XMCDA files directly: the input data sent to the web service always conforms to the XMCDA format, but it may be either loaded from a file or generated from values entered by a user in a more user-friendly manner.

This leads us to the notion of file inputs and editable inputs, that can be found in the `diviz <//www.diviz.org>`_ software, as shown on the picture hereafter. The picture shows an excerpt from a workflow in diviz, and more precisely the program called **linProgCapaIdent**.

    .. figure:: images/parameterWindow.png
      :align: left

      *Editable and file inputs in diviz*

The dialog box which is shown on the picture shows that the parameter **kAdditivity** of this program can be entered by hand, whereas the input parameters **criteria**, **alternatives**, **performanceTable**, etc. need to be loaded from an input file (represented by colored bullets on the left side of the program icon). 

Very similarly, such a GUI could be done in a standard web browser via a web page. The picture hereafter shows such a page which would prepare the inputs of the program **linProgCapaIdent** in the same manner as in the diviz software. 

    .. figure:: images/parametersInBrowser.png
      :align: left

      *Editable and file inputs in a browser*

In general, we consider that the input elements which are editable by hand are *tuning* parameters or *options* of your underlying algorithm. 

Consequently, the easy integration of your program within any GUI requires some additional information concerning details of these editable input parameters (their *type*, constraints on their values, ...).

It is therefore important to distinguish between 3 types of input and output data elements:

- editable input parameters (which can be modified in a GUI);

- input data (files) (using the `XMCDA <//www.decision-deck.org/xmcda>`_ standard), represented as an input connection point in diviz;

- output data (files) (using the `XMCDA <//www.decision-deck.org/xmcda>`_ standard), represented as an output connection point in diviz.

Note that the grammar of ``description-wsDD.xml`` allows to specify that an editable input parameter should optionally become a standard input connection point (and vice-versa). 

Finally remember that it is up to you to decide if you wish to allow diviz users to edit certain parameters via a dialog box, or let them submit these data in a standard way as input data files to your program. This choice has no influence on the web service, on its call via a `SOAP request <https://en.wikipedia.org/wiki/SOAP>`_ or on your program.

Let us now dive into the detailed documentation of the ``description-wsDD.xml`` file. It is written in XML and structured as follows.

General document structure
**************************

The root tag of ``description-wsDD.xml`` is given below:

  ::

   <program_description>

     [...]

   </program_description>

``<program_description>`` has a certain number of children which are described hereafter.

Program name and technical information:
---------------------------------------

  ::

   <program provider="DecisionDeck"
        name="lsRankingCapaIdent"
        displayName="lsRankingCapaIdent"
        version="1.0" />

  - The ``provider`` attribute specifies who is providing the web service;

  - The ``name`` attribute specifies the name of the web service on the distant server;

  - The ``displayName`` attribute specifies the name which should be displayed for the user (in a software using the web service).

  - The ``version`` attribute sepcifies which version of the software is available as a web service.

.. _documentationTag:

Description of the program
--------------------------

  ::

    <documentation>

      <description>Identifies a Mobius capacity [...] </description>

      <contact>John Doe (john.doe@somewhere.com)</contact>

      <url>https://www.this-is-an-url.com</url>

      <reference>J. Doe, J. Smith (2009) [...]</reference>

    </documentation>

  - The ``<description>`` tag describes the purpose of the web service;

  - The ``<contact>`` tag specifies a contact person which can help in case of problems with the web service (optional);

  - The ``<url>`` tag specifies an url where further information can be found on the web service (optional);

  - The ``<reference>`` tag gives a bibliographical reference for the description of the web service (optional).

A list of input and output parameters: 
--------------------------------------

They specify the contents of the input data, output data and provide information on the editable input parameters in diviz (or any other GUI).

  ::

    <parameters>

      <input [...]>
         [...]
      </input>

      [...]

      <output [...]>
         [...]
      </output>

      [...]

    </parameters>

Detailed description of ``<input>`` and its children
****************************************************

  ::

   <input id="numIt"
          name="numberIter"
          displayName="numberOfIterations"
          isoptional="0">

  - The ``id`` attribute defines a unique identifier for this input element;

  - The ``name`` attribute indicates the name of the parameter in the soap request (and consequently, in the Decision Deck web service framework, also the name of the xml file which is read by your program; in this case ``numberIter.xml``);

  - The ``displayName`` attribute indicates how this parameter should be displayed in diviz;

  - The ``isoptional`` attribute indicates if this parameter is optional or if it is mandatory. Note that if it is optional, your program should be able to handle a request where that file is not present in the input directory.  When an input is optional, a GUI should present a way to enable or disable the input (for example, by adding a check box the user can toggle to enable or disable the input).

    Valid values are:

    - ``0`` (default): if ``isoptional`` is absent or equal to ``0``,
      the input is not optional;

    - ``defaultTrue`` or ``1``: the input is optional, and the GUI
      should enable the input by default;

    - ``defaultFalse``: the input is optional, and the GUI should
      disable the input by default.

Let us now describe the children of ``<input>``.

.. _xmcdaTag:

The technical description of the XMCDA code associated with this parameter:
---------------------------------------------------------------------------

The ``<xmcda>`` tag allows to specify what XMCDA code is associated with this parameter. This tag has two purposes: 

 - for documentation purposes: if you wish to describe the data required by this parameter for the users of your web service;
 
 - for diviz: if this parameter should be manually editable in a dialog window of diviz, we need to know what XMCDA code should be generated before the call of the service.

In the example below we describe a ``<methodParameters>`` entry containing two parameters (which as we will see later should be editable in a dialog window in diviz). Note however that this is not restricted to this XMCDA tag, and that the detailed description of the XMCDA code is not mandatory.

  ::

    <xmcda tag="methodParameters">

       <![CDATA[
       
          <methodParameters>

             <parameter
                <!-- REQUIRED  --> name="kAdditivity">
                   <value>
                      <int>%1</int>
                   </value>
             </parameter>
                
             <parameter
                <!-- REQUIRED  --> name="separationThreshold">
                   <value>
                      <int>%2</int>
                   </value>
              </parameter>

           </methodParameters>

       ]]>

    </xmcda>

  - The ``tag`` attribute indicates what main XMCDA tag is associated with this parameter (in the example above, it is a ``methodParameters``); this attribute is mandatory as it is the minimal information a user need to know if he submits data to your web service;

  - The content of ``<xmcda>`` describes the data which the web service is waiting for in case of this input parameter. To allow writing XML code inside this tag, we suggest to use the ``<![CDATA[ [...] ]]>`` tag, which is used for text data (and thus XML tags) that should not be parsed by the XML parser;

  - If this code should be generated by diviz, then at least one *placeholder* needs to be defined. A placeholder indicates at which place the value typed by the user should be inserted in the XMCDA code. If there are multiple such values, there need to be multiple placeholders;

    In the example above, two such placeholders are defined (by the codes ``%1`` and ``%2``). They indicate where the values typed by the user for the kAdditivity parameter and the separationThreshold parameter should be inserted;

    The placeholders are numbered from ``%1`` to ``%n``, where ``n`` is the number of necessary placeholders;

  - Note that any XML comments can be inserted in this XMCDA code to clarify things for the user; 

    In the example above we have added the comment ``<!-- REQUIRED -->`` to express that the ``name`` attributes of both ``<parameter>`` tags need to be correctly filled in for the web service to work properly.

The documentation of the input parameter:
-----------------------------------------

  ::

    <documentation>

       <description>
          A set of method parameters to tune the algorithm: [...]
       </description>    

    </documentation>

The documentation tag has the same children as the documentation tag used to describe the program, detailed :ref:`earlier<documentationTag>` in this document. 


Description of the GUI associated with the input parameter (optional):
----------------------------------------------------------------------

The optional ``<gui>`` tag describes the behaviour and the structure of a GUI associated with the input of the value(s) of this particular parameter of the web service. In general, we suggest to use such an input panel for quite simple types. The ``<methodParameters>`` tag associated with several ``<parameter>`` tags is appropriate for such GUIs. The following example refers to the :ref:`previous<xmcdaTag>` one introduced with the ``<xmcda>`` tag.

  ::

    <gui status="preferFile">

       <entry id="%1" type="int" displayName="kAdditivity">

          <constraint>
             <description>The value should be a positive integer.</description>
             <code>
                <![CDATA[
                   %1 > 0
                ]]>
             </code>
          </constraint>

          <defaultValue>1</defaultValue>

       </entry>

       <entry id="%2" type="float" displayName="separationThreshold">

          <constraint>
             <description>The value should be a strictly positive float.</description>
             <code>
                <![CDATA[
                   %2 > 0
                ]]>
             </code>
          </constraint>        

       </entry>

    </gui>

  - The ``status`` attribute indicates if it should be possible to switch between the GUI input panel and the standard input data (file) representation for this parameter (represented as an input connection point in diviz); 

    The value "alwaysGUI" indicates that this switch is impossible and that the only way to enter this input parameter is via a GUI.

    The value "preferGUI" indicates that this switch is possible, but that by default, the input parameter is entered via a GUI.

    The value "preferFile" indicates that this switch is possible, but that by default, the input parameter is loaded from an XMCDA file. 

  - Each ``<entry>`` is related to a :ref:`placeholder<xmcdaTag>` in the ``<xmcda>`` tag;

     The ``id`` attribute is related to the number of the placeholder (``%1`` to ``%n``);

     The ``type`` attribute indicates what data type is expected for this placeholder value (``int``, ``float``, ``boolean``);

     The ``displayName`` attribute contains the name which should be displayed for this placeholder value;

  - The ``<constraint>`` tag contains the information related to logical constraints related to the value of this placeholder;

     The ``<description>`` tag contains a message explaining literally the constraint;

     The ``<code>`` tag contains the *mathematical* expression of the constraint on this placeholder. To allow writing easily such a constraint inside this tag, we suggest to use the ``<![CDATA[ [...] ]]>`` tag, which is used for text data that should not be parsed by the XML parser;

     The constraint should respect the Java syntax for logical expressions (``&&`` for logical *and*, ``||`` for logical *or*, ``!`` for logical *not*);

Detailed description of ``<output>`` and its children
*****************************************************

As no GUI is necessary for output elements, their description is much easier. 

  ::

   <output id="mobiusCapacity"
           name="mobiusCapacity"
           displayName="mobiusCapacity">


  - The ``id`` attribute defines a unique identifier for this output element;

  - The ``name`` attribute indicates the name of the parameter in the soap request (and consequently, in the Decision Deck web service framework, also the name of the xml file which is written by your program; in this case ``mobiusCapacity.xml``);

  - The ``displayName`` attribute indicates how this parameter should be displayed in diviz;

Let us now describe the children of ``<output>``.
    
The technical description of the XMCDA code associated with this output parameter:
----------------------------------------------------------------------------------

The ``<xmcda>`` tag allows to specify what XMCDA code is associated with this parameter. For this output parameter, the only purpose of this tag is to document the data returned by the web service for the users. The syntax is the same as for :ref:`input<xmcdaTag>` parameters.
 
  ::

   <xmcda tag="criteriaValues">
      <![CDATA[
         <criteriaValues mcdaConcept="mobiusCapacity">
            <criterionValue>
               [...]
            </criterionValue>
            [...]
         </criteriaValues>
      ]]>
   </xmcda>

The documentation of the output parameter:
------------------------------------------

  ::

    <documentation>

       <description>
          The Mobius transform of a capacity.
       </description>    

    </documentation>

The documentation tag has the same children as the documentation tag used to describe the program, detailed :ref:`earlier<documentationTag>` in this document. 

Final observations
******************

The ``description-wsDD.xml`` file has 3 main purposes:

 - information for the deployement of your program as a web service;

 - documentation of the web service and its use;

 - information elements for the generation of a GUI for the input of certain parameters.

To help you writing your specification file, you can have a look at the specification files of the web services currently deployed in the Decision Deck project :ref:`here<webServices>`. 

The syntax of the ``description-wsDD.xml`` file can be found in an XML schema which is available :download:`here</specificationData/description.xsd>`.








   
