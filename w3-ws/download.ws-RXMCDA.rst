.. _download.ws-RXMCDA:

ws-RXMCDA suite
===============

ws-RXMCDA is a suite of XMCDA web services (programs) written in GNU R, and using the RXMCDA parsing library. 

Each program of ws-RXMCDA requires, among other packages, the RXMCDA parsing library for R (see `www.decision-deck.org/xmcda <//www.decision-deck.org/xmcda>`_).

The ws-RXMCDA suite is available for download from `github <https://github.com/paterijk/ws-RXMCDA>`_.

.. index:: web service, RXMCDA, ws-RXMCDA, GNU R
