===========================
 Decision Deck's web-sites
===========================

This is the source for the whole websites www.decision-deck.org and www.diviz.org.

See ``INSTALL`` for details on how to transform the source into a website.
