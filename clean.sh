clean ()
{
  cd "$1"
  make clean
  cd "${OLDPWD}"
}
clean w3-dd
clean w3-d2
clean w3-d3
clean w3-diviz
clean w3-gis
clean w3-news
clean w3-r
clean w3-ws
clean w3-xmcda
