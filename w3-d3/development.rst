d3 Development Environment Installation Guide
=============================================

Certified versions
------------------

d3 is known to work against these versions:

    * Eclipse 3.3.1.1
    * Java 6
    * Mysql 5
    * Tomcat 5.5

Install the prerequisites
-------------------------

    * Download and Install Tomcat

      http://tomcat.apache.org/download-55.cgi (select the binary core distribution)
    * Download Eclipse

      http://www.eclipse.org/downloads/

Install Eclipse plugins
-----------------------

Developing d3 is made easier with WTP (Web Tools Platform). You will also need AJDT (AspectJ Development Tool). To install those plugins follow the steps outlined below.

    * Open the update manager dialog [Help > Software update > Find and Install]
    * Select "Find new features to install"
    * Select "Europa Discovery Site" and press finish
    * Select "AspectJ Development Tools" under "Programming Languages"
    * Select Web and JEE Development
    * Press "Select required"
    * Press Next
    * Accept the license and press Next, then Next again, then Finish
    * Grab a coffee and wait until all plugins are downloaded
    * Press "Install all"
    * Press OK when you're asked to restart Eclipse

Fetch Projects from CVS
-----------------------

    * Download the Project Set File
    * Import the project list into Eclipse

      File > Import > Team > Team Project Set

      If you don't check "Run import in background", you won't be able to use the platform until the import ends
    * When prompted, enter "anonymous" as user name and left the password blank

First execution
---------------

    * Create a Tomcat server definition
          - Open the Server/Installed runtimes in the preferences dialog

            Window > Preferences > Server > Installed Runtimes

          - Press Add..
          - Select Apache / Tomcat 5.5
          - Fill in the installation directory (where you have previously installed Tomcat)
          - Press OK
          - Create the datasource configuration file. Datasource definition is fetched from the user home folder. So copy d3/resources/META-INF/spring/jdbc.properties into ${user.home}/.d3 and edit its content

            Under Linux: ${user.home} = ~/

            Under Windows: ${user.home} = C:\Document And Settings<USER NAME>\
          - Open the server view [Window > Show view > Other]
          - In the server View, right click and press "New > server"
          - Select Tomcat v5.5
          - Press Next
          - Select the d3 project and add it to the configured projects list
          - Press Finish
    * Start the newly declared server
    * When the server is started point your browser to

      http://localhost:[port]/d3 where [port] is the port your server listens to (8080 on a default Tomcat install)

      You should see d3 index page

Build the project
-----------------

    * Run Ant script build.xml.

      You need to set the eclipse.home property in the Properties tab of the launcher dialog (Open external tools...)

      If all went well, a war file named d3.war should be created in d3/target (you may need to refresh the projects to reflect the changes).


