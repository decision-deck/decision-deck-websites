d3
==

.. Important:: This initiative is no longer maintained.

If you already have developed an implementation of a MCDA method, whatever language is used, you may want to reuse it directly without having to rewrite it to fit d2 expectations. d3 aims to help to integrate such existing methods. It is a web application that allows you to interact with remote services exposing MCDA methods. Data exchanged between d3 and your remote service is done trough XML files.

Documentation
-------------

.. toctree::
	:maxdepth: 1

	overview
	

Developers
----------

.. toctree::
	:maxdepth: 1

	development
	install
	d2-integration
	developer-guide
