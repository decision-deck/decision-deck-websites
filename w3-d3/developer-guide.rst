Technical Overview
==================

Kernel
------

Overview
^^^^^^^^

The Figure 1 below provides an overview of the kernel layers.

Figure 1 Decision Deck Layers

Core Model
^^^^^^^^^^

The core model (Figure 2) is composed of basic yet generic entities in the decision aid field:

Figure 2 Core Model Layer

    * Alternative and Profile
    * Criterium
    * Criterium has a type that can take one of the following values:
          - Ranking
          - Label
          - Cotation 
    * Evaluation represents a ternary association between User, Criterium and Alternative

Data Access
^^^^^^^^^^^

A few DAO are provided to query the basic elements of the model (see Figure 3). Concrete DAO implementations reside in the org.decisiondeck.dao.sql package. They should be accessed through the DaoFactory. Dao implementation are declared in the /org/decisiondeck/dao/dao.properties file. This properties file contains key/value pairs such as: ::

	#DAO interface name = DAO implementation name
	org.decisiondeck.dao.IAlternativeDao=org.decisiondeck.sql.AlternativeSqlDao

Figure 3 DAO Layer

The provided DAO access the database through native JDBC calls. Query are constructed thanks to a simple SQL Query builder, whose implementation reside in the d2-sql project.

Service Layer
^^^^^^^^^^^^^

Although it is possible to access the DAO directly it is recommended to only access them through the service layer (see diagram). Service are singleton.

Plugin
^^^^^^

The plugin guide describes the plugin mechanism extensively. The Figure 4 below describe in a visual way the plugin descriptor. Plugin descriptor related classes are generated using Modello. Modello generates Reader and Writer (PluginDescriptorXpp3Reader and PluginDescriptorXpp3Writer) as well as xsd schemas.
Figure 4 Plugin Descriptor

The Figure 5 describes the plugin manager architecture. GlobalPluginManager is used to query global plugin registry and to initialize plugins, whereas ProjectPluginManager is used to retrieve project specific plugins and exposed services. They both implement IPluginManager and must be retrieved through the PluginManagerRegistry singleton.

Figure 5 Plugin Manager

View Layer
^^^^^^^^^^

The view Layer aggregates the core interface and shared basic components (tree, tabel, editors, etc.). You can gain access to the MainFrame through the Workbench singleton: ::

	Workbench.getKernel().getWindow()

Figure 5 Common UI Components

Launcher
--------

We use Classworld to launch the application. This requires a thin bootstrap layer : this is the role of the d2-launcher project. It passes arguments to the MainFrame static main(String[], ClassWorld) method.

Building
--------

Prerequisites
^^^^^^^^^^^^^

You need to have Maven 2 installed to build Decision-Deck.

Command Line
^^^^^^^^^^^^

To build the whole application alongside with the base plugins provided with the kernel, go into d2-root and type: ::

	D:\worskpaces\decision-deck\d2-root> build_exploded.bat

The resultant build artifacts should be generated into d2-launcher/target. Alternatively you can build a zip archive using the build_zip.bat script. Please note that there's yet no script for linux.

Building from Eclipse
^^^^^^^^^^^^^^^^^^^^^

It is recommended to build first from the command line, so that Maven can download required libraries.

Then, to build from Eclipse, you will need to declare a Classpath Variable. You will name it M2_REPO and make it point to your m2 repository (defaults to ~/.m2/repository). Once done, you can rebuild all projects.

Versioning
----------

Version numbering follows a classical 3 numbers convention: X.Y.Z. Where X represents a major version, eventually including non compatible changes and API break; Y represents minor version, with functionnality addition (backward compatible), and Z represents minor updates, patch, bug fixing, etc..

