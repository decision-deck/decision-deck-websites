d3 Overview
===========

If you already have developed an implementation of a MCDA method, whatever language is used, you may want to reuse it directly without having to rewrite it to fit d2 expectations. d3 aims to help to integrate such existing methods. It is a web application that allows you to interact with remote services exposing MCDA methods. Data exchanged between d3 and your remote service is done trough XML files.

Only a few core concepts are used :

    * Method - a Method represents a formal MCDA Method definition
    * Service - a Service represents a remote concretization (web service, soap) of a MCDA Method
    * Job - a Job represents a suite of interaction steps between d3 and your service, it encapsulates a problem that is submitted to a remote service
    * UserFile - a UserFile is a file owned by a d3 user (a file that a d3 use has previously uploaded to d3)

Job can have four distinct state :

    * PENDING - just after the job has been created
    * WAITING - the problem has been submitted
    * SOLVED - the problem has been successfully solved
    * ERRORS - if an error occured while submitting the job

Below is a simple class diagram to help to vizualize those concepts.

.. figure:: images/class_diagram.png
   :align: center

The integration process is pretty straightforward :

    * register your method within d3 (screenshot)
    * declare one or more services that implement the method (screenshot)
    * create a job (screenshot)
    * Interact with remote service (screenshot)
    * Vizualize solution (screenshot) 

When registering a Method, the user must provide the method input schema and the method output schema. Also XML stylesheets can be specified to allow to visualize both problem input and problem output.

The remote service must expose at least three ports whose name can configured when registering the service:

    * problem submission port (default=submitProblem)
    * solution request port (default=requestSolution)
    * hello port (not used yet ; default=hello)

Note that your SOAP service must use a rpc binding and literal encoding. You can find a WSDL sample here.

Finally d3 can be used to facilitate d2 development: d3 exposes a few interfaces that allows d2 (especially) to communicate with it. See d2 integration for a more thorough insight of that integration.

