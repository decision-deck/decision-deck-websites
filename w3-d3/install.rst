d3 Quick Installation Guide
===========================

Certified versions
------------------

d3 is known to work against these versions:

    * Java 6
    * Mysql 5
    * Tomcat 5.5

Installation steps
------------------

We assume here that you have successfully installed Tomcat in folder TOMCAT_HOME and that you have already downloaded d3 (or built it yourself). Let's call USER_HOME the Tomcat process owner home

    * Configure Tomcat.

      This is a simple installation, for more complex configuration options (f.i. configuring virtual hosts, integrating with apache, etc.) please refer to Tomcat documentation.
          - Open TOMCAT_HOME/conf/server.xml
                + Locate the non ssl http connector declaration :

                  <!-- Define a non-SSL HTTP/1.1 Connector on port 8080 --> 
          - Edit the connector port to your convenience
          - Configure compression: Add the following attributes to the Connector element ::

			compression="on" 
			compressionMinSize="2048" 
			noCompressionUserAgents="gozilla, traviata" 
			compressableMimeType="text/html,text/xml,text/javascript,application/x-javascript,application/javascript"

          + Once done the Connector declaration will somewhat look similar to: ::

		          <Connector port="8080" 
		                     maxHttpHeaderSize="8192" 
		                     maxThreads="150" 
		                     minSpareThreads="25" 
		                     maxSpareThreads="75" 
		                     enableLookups="false" 
		                     redirectPort="8443" 
		                     acceptCount="100" 
		                     connectionTimeout="20000" 
		                     disableUploadTimeout="true" 
		                     compression="on" 
		                     compressionMinSize="2048" 
		                     noCompressionUserAgents="gozilla, traviata" 
		                     compressableMimeType="text/html,text/xml,text/javascript,application/x-javascript,application/javascript"
		                  /> 

            You may need to restart Tomcat for the changes to be applied.
    * Create a jdbc.properties file in USER_HOME/.d3. This file defines d3 datasource. It should contain the following lines: ::

	      jdbc.driver=com.mysql.jdbc.Driver
	      jdbc.host=<<edit to your convenience>>
	      jdbc.port=<<edit to your convenience>>
	      jdbc.database=<<edit to your convenience>>
	      jdbc.username=<<edit to your convenience>>
	      jdbc.password=<<edit to your convenience>>

      Note that the database must exist prior to starting d3.
    * Copy d3.war to TOMCAT_HOME/webapps/d3.war
    * Tomcat will then deploy d3, and once done you will be able to access d3 through http://localhost:8080/d3


