d2/d3 Integration
=================

Overview
--------

d3 is a web application that can be used to reuse a method that you had previously written (whatever language you used).

Some d3 features are exposed over HTTP to allow to easily use them from d2. From d2 you can:

    * submit a problem to d3
    * view a file (problem input, problem solution)
    * delete a submitted problem

Those features are demonstrated in Rubis plugin.

It is assumed here that you have previously registered your method on d3, and have declared a service that implements it. Then to make use of the features outline above, you need essentially to be able to serialize/deserialize a d2 problem that fits your remote method expectation (in other words, that means that the serialized problem must be valid in regards to the method input schema as declared on d3).

Once you have a valid XML description of the problem, you can submit it to d3 using plain HTTP request. The following resources are exposed:

    * Submit action, exposed at D3_ROOT/integration/createJob.do
    * View a file, exposed at D3_ROOT/integration/viewJobFile.do
    * Delete a job, exposed at D3_ROOT/integration/deleteJob.do

In addition problems can also be submitted through a SOAP call

    * SOAP based Submit action, exposed at D3_ROOT/api/service/createJob.do

Remote methods description
--------------------------

Submit action
^^^^^^^^^^^^^

The submit action expects the following parameters:

    * description: String
    * methodId: int
    * serviceId: int
    * problemFile: File
    * userName: String
    * password: String

Where methodId, serviceId are the identifiers (stored on d3) of the method and service to be used, and userName and password are d3 login information. The problem file is passed as an attachment (form/multipart request).

The result is an xml fragment such as: ::

	<result>
		<success>true|false</success>
		<message>Some information message</message>
		<jobOid>The remote identifier of the job if the call was successful</jobOid>
	</result>

View file action
^^^^^^^^^^^^^^^^

The View File action expects the following parameters:

    * d: String
    * transfo: boolean
    * id: int
    * userName: String
    * password: String

The parameter d (for direction) indicates wether we want to view the problem input or output. It can take the following values: in | out. The transfo paramater indicates wether we want to try transforming the xml using the stylesheet specified in the method declaration. The parameter id is the remote job identifier, while userName and password are the d3 login information. Note that job identified by id must be owned by the user identified by (userName, password). The result is an html file or an xml stream - depending on the transfo parameter.

Delete job action
^^^^^^^^^^^^^^^^^

The delete job action expects the following parameters:

    * userName
    * password
    * jobId

Where userName and password are the login information of the d3 user, and jobId the identifier of the job to be deleted. Not that job must be owned by the user identified by (userName, password).

The result is an xml fragment such as: ::

	<result>
		<success>true|false</success>
		<message>Some information message</message>
	</result>


