.. raw:: html

    <!-- Container -->
    <div class="container">


Contact details
===============

Announcement mailing list
-------------------------

This **low traffic** mailing list is intended to announce general news on the Decision Deck project and inform you of upcoming events.

If you wish to subscribe, note that you will receive at most 1 or 2 announcements per month from this list. You can subscribe to the mailing list by pointing your web browser to the following address:

https://listes.imt-atlantique.fr/wws/subscribe/decisiondeck-announcements

and by entering your email address. You will then be sent a password which you have to enter on the website to confirm your subscription.

Decision Deck Consortium
------------------------

Patrick Meyer

| IMT Atlantique
| LUSSI Department
| Technopôle Brest-Iroise CS 83818
| F-29238 Brest Cedex 3
| France

patrick.meyer@imt-atlantique.fr

XMCDA
-----

Patrick Meyer

| IMT Atlantique
| LUSSI Department
| Technopôle Brest-Iroise CS 83818
| F-29238 Brest Cedex 3
| France

patrick.meyer@imt-atlantique.fr

diviz
-----

Sébastien Bigaret and Patrick Meyer

| IMT Atlantique
| LUSSI Department
| Technopôle Brest-Iroise CS 83818
| F-29238 Brest Cedex 3
| France

info@diviz.org

.. raw:: html

    </div>
