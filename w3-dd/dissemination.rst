.. dissemination:

.. raw:: html

    <!-- Container -->
    <div class="container">


Scientific dissemination
========================

On this page we list the articles and presentations (other than Decision Deck workshops) involving the Decision Deck project or one of its initiatives.

Articles
--------

- Sébastien Bigaret, Patrick Meyer, diviz: an MCDA workflow design, execution and sharing tool, Newsletter of the EURO Working Group Multicriteria Aid for Decisions (MCDA), Series 3, Number 21, Spring 2010.

- Raymond Bisdorff, Patrick Meyer, The Decision-Deck Project - Developing a Multiple Criteria Decision Analysis Software Platform, ERCIM News 72, January 2008, (`pdf <https://ercim-news.ercim.eu/images/stories/EN72/EN72-web.pdf>`_).

Presentations at conferences
----------------------------

- Sébastien Bigaret, Patrick Meyer, diviz: an MCDA workflow design, execution and sharing tool, 25th Mini-EURO Conference Uncertainty and Robustness in Planning and Decision Making (URPDM 2010), Coimbra, Portugal, 15-17 April 2010.

- Thomas Veneziano, Sébastien Bigaret, Patrick Meyer, Diviz : an MCDA components workflow execution engine. EURO XXIII : 23rd European Conference on Operational Research, 05-08 july 2009, Bonn, Germany, 2009.

- Sébastien Bigaret, Patrick Meyer, Vincent Mousseau, The Decision Project: Towards Open Source Software Tools Implementing Multiple Criteria Decision Aid, Journées de l'Optimisation 2009, Montréal, Canada, 4-6 May 2009.

    The Decision Deck project aims at collaboratively developing open source software tools implementing Multiple Criteria Decision Aid (MCDA). Its purpose is to provide effective tools for three types of users:

    - practitioners who use MCDA tools to support actual decision makers involved in real world decision problems;

    - teachers who present MCDA methods in courses, for didactic purposes;

    - researchers who want to test and compare methods or to develop new ones.

    In this talk, we present the Decision Deck project and detail the different research initiatives which have emerged inside it. In particular, we focus on the diviz software platform which is a tool for designing, executing and deploying MCDA methods.

- Patrick Meyer, Raymond Bisdorff, Vincent Mousseau, M. Pirlot : Latest news on the Decision Deck Project and focus on diviz, MCDA69, 69th Meeting of the Euro Working Group "Multiple Criteria Decision Aiding", Brussels, Belgium, April 2-3, 2009.

.. raw:: html

    </div>
