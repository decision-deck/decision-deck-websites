.. raw:: html

    <!-- Container -->
    <div class="container">


Downloads
=========

The Decision Deck project has given birth to various initiatives which aim at the same global objective : propose software solutions which support the MCDA process. The following tools are such productions of the project.

diviz
-----

diviz is a software for designing, executing and sharing Multicriteria Decision Aid (MCDA) methods, algorithms and experiments. On the diviz `download <//www.diviz.org/download.html>`_ page you can find the latest version of the diviz client.


XMCDA
-----

You will find `here <//www.decision-deck.org/xmcda/current.html>`_ downloads for the current version of XMCDA standard, including the XML Schema, a quick tutorial, a complete documentation, sample XMCDA files and cutomisable XSL and CSS stylesheets.

R
-

The R package MCDA contains multiple functions to support the MCDA process within the R statistical environment. The download page may be found `here <//www.decision-deck.org/r/index.html>`_ .

d2 and d3
---------

Both d2 and d3 are older initiatives of the Decision Deck project. They are currently not actively developed, however they are fully functional. The latest versions for both d2 and d3 can be downloaded from `sourceforge <https://sourceforge.net/projects/decision-deck/files/>`_.

.. raw:: html

    </div>
