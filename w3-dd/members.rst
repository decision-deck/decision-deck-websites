.. raw:: html

    <!-- Container -->
    <div class="container">


Members List
============

Administration Board
--------------------

* Patrick Meyer (president)
* Miłosz Kadziński (vice-president)
* Vincent Mousseau (secretary)
* Brice Mayag (treasurer)
* Sébastien Bigaret (software coordinator)
* Alexandru-Liviu Olteanu (communications coordinator)
* Olivier Cailloux (deputy software coordinator)

All members (situation: October 2016)
-------------------------------------

* Khaled Belahcene
* Sébastien Bigaret
* Olivier Cailloux
* Miłosz Kadziński
* Rémy Le Boennec
* Brice Mayag
* Patrick Meyer
* Vincent Mousseau
* Alexandru-Liviu Olteanu
* Marc Pirlot
* Olivier Sobrie

.. raw:: html

    </div>
