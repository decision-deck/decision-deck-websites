.. role:: html(raw)
   :format: html

.. raw:: html

    <!-- Container -->
    <div class="container">


Decision Deck events
====================

On this page you can find past and future events organised by the Decision Deck consortium or involving initiatives of Decision Deck.

Future events
-------------

- fall 2019: 16th Decision Deck Workshop at IMT Atlantique, Brest, France

Past events
-----------

- 26 September 2018: 15th Decision Deck Workshop at Instituto Superior Técnico, Universidade de Lisboa, Lisbon, Portugal, http://ddws2018.idsswh.sysresearch.org, :html:`<a data-toggle="collapse" href="#slides15ddw">View slides</a>`

.. raw:: html

    <div id="slides15ddw" class="collapse"><ul>


- `Latest news and developments <_static/slides15ddw1.pdf>`_, *Patrick Meyer*
- `Robust efficiency analysis methods on diviz <_static/slides15ddw2.pdf>`_, *Milosz Kadzinski, Anna Labijak*
- `How modelling assistants can be integrated in the SW tools to improve the effectiveness of decision aiding <_static/slides15ddw3.ppt>`_, *Maria Franca Norese, Andrea Profili, Antonino Scarelli*
- `Using PROMETHEE to rank the EU countries on Environmental Health and Ecosystem Vitality (EPI's indicators) from 2006 to 2016 <_static/slides15ddw4.pptx>`_, *Panagiota Digkoglou, Jason Papathanasiou*
- `Learning MCDA with the help of diviz <_static/slides15ddw5.pptx>`_, *Maria De Vicente y Oliva, Vincent Cliville, Jaime Manera Bassa*
- `Criteria weights assessment through prioritizations (WAP) software tool <_static/slides15ddw6.pdf>`_, *Athanasios Spyridakos, Nikos Tsotsolas, Isaak Vryzidis*
- `DECSPACE: A user-friendly web-based platform to explore MCDA methods <_static/slides15ddw7.pdf>`_, *Ana Sara Costa, Joao Amador, Ruben Rodrigues, Jose Rui Figueira, Jose Borbinha*
- `Integrating time into majority-rule sorting models: application to the cyber-defense context <_static/slides15ddw8.pdf>`_, *Arthur Valko, Alexandru-Liviu Olteanu, David Brosset, Patrick Meyer*

.. raw:: html

    </ul></div>


- 20 September 2017: 14th Decision Deck Workshop at Université Paris-Dauphine, Paris, France, http://www.lamsade.dauphine.fr/d2-workshop/

- 14 October 2016: 13th Decision Deck Workshop at École centrale Paris, http://www.lgi.ecp.fr/~mousseau/D2Workshop/pmwiki-2.2.7/pmwiki.php

- 30-31 November 2014: 12nd Decision Deck Workshop at Télécom Bretagne, Brest, https://conferences.telecom-bretagne.eu/ddws12/

- September 24th, 2013: 3rd Developers' Camp at École Centrale Paris, http://www.lgi.ecp.fr/DevCamp

- September 23rd, 2013: 11th Decision Deck Workshop at École Centrale Paris, http://www.lgi.ecp.fr/DevCamp

- 8-11 July 2012: Stream at the EURO XXV conference in Vilnius : Innovative Software Tools for MCDA, http://www.euro-2012.lt/welcome

- 11 April 2012: 10th Decision Deck Workshop in Tarragona, Spain, http://deim.urv.cat/~itaka/CMS4/index.php?option=com_content&view=article&id=10&Itemid=14

- 5-6 December 2011: Developper's camp at Centrale Paris, France, http://www.lgi.ecp.fr/DevCamp

- 17-18 October 2011: 9th Decision Deck Workshop at University of Luxembourg, Luxembourg.

- 14-16 April 2011: 8th Decision Deck Workshop in Corte, France, http://mcda.univ-corse.fr/Welcome-Bienvenue-_a8.html

- 6 October 2010: 7th Decision Deck workshop at Université Paris Dauphine, http://www.lgi.ecp.fr/~mousseau/D2Workshop/pmwiki-2.2.7/pmwiki.php/Main/HomePage?setlang=en


- 14-15 April 2010: 6th Decision Deck workshop at INESC Coimbra, Portugal, http://www.inescc.pt/ddws6/

- 27 June - 8 July 2010: 10th MCDA Summer School at Ecole Centrale Paris, France, http://www.gi.ecp.fr/mcda-ss

- 17-18 September 2009: 5th Decision Deck workshop at Télécom Bretagne, Brest, France, https://conferences.telecom-bretagne.eu/ddws5/

- 30-31 March 2009: 4th Decision Deck workshop at the University of Mons, Belgium

- 3-4 December 2008, 2nd D{2+3} Developers' Days, Ecole Centrale de Paris, France, http://www.gi.ecp.fr/mousseau/d2develop/

- 16-17 June 2008: 3rd Decision Deck workshop at INESC Coimbra, Portugal, http://www.inescc.pt/d2-workshop/

- 6-7 May 2008:  1st D{2+3} Developers' Days, University of Luxembourg, Luxembourg, http://sma.uni.lu/d2develop/

- 21-22 February 2008: 2nd Decision Deck Workshop at Université Paris Dauphine, France, http://www.lamsade.dauphine.fr/d2-workshop/

- 15-16 November 2007: Plugin Developer Meeting, Université Paris Dauphine

- 8-9 March 2007: 1st Decision Deck Workshop at the University of Luxembourg, Luxembourg, http://charles-sanders-peirce.uni.lu/d2workshop/

.. raw:: html

    </div>
