.. _index:

.. _index_dd:

.. raw:: html

    <!-- Container (Decision Deck Section) -->
    <div id="deck" class="container text-center">
      <img class="img-responsive" src="_static/ddlogo.png" style="max-height:70px;display:block;margin: 40px auto;">
      <h2>Decision Deck</h2>
      <p>The Decision Deck project collaboratively develops Open Source software tools to support the Multi-Criteria Decision Aiding (MCDA) process.</p>
    </div>
    <div id="latestnews" class = "text-center">
        <hr>
        <div id="myCarousel" class="carousel slide text-center" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
          </ol>
          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <h4>diviz v20</h4>
              <p>diviz v20 has been released. <a href="//www.diviz.org/download.html">Download it</a>!</p>
            </div>
            <div class="item">
              <h4>16th Decision Deck Workshop</h4>
              <p>The 16th Decision Deck Workshop took place at IMT Atlantique, Brest, France on September 25th 2019. Go to the official <a href="http://conferences.imt-atlantique.fr/ddws16">website</a> for more details.</p>
            </div>
            <div class="item">
            <h4>The first Decision Deck newsletter</h4>
            <p>We have just released our first newsletter. Be sure to check it out <a href="../news/newsletter1.html">here</a>.</p>
            </div>
            <div class="item">
            <h4>XMCDA v2/v3 Java reference library</h4>
            <p>First release of the XMCDA v2/v3 Java reference library. Follow <a href="//www.decision-deck.org/xmcda/developers.html#xmcda-java-reference-library">this link</a>.</p>
            </div>
          </div>
          <!-- Left and right controls -->
          <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
    </div>
    <div id="consortium" class="bg-1">
      <div class="container">
        <div class="row text-center">
          <div class="col-sm-3">
            <p><strong>Info</strong></p>
            <a data-toggle="collapse" data-parent="#accordion" href="#info">
            <span class="glyphicon glyphicon-info-sign icons"></span>
            </a>
          </div>
          <div class="col-sm-3">
            <p><strong>Board</strong></p>
            <a data-toggle="collapse" data-parent="#accordion" href="#committee">
            <span class="glyphicon glyphicon-user icons"></span>
            </a>
          </div>
          <div class="col-sm-3">
            <p><strong>Manifesto</strong></p>
            <a data-toggle="collapse" data-parent="#accordion" href="#manifesto">
            <span class="glyphicon glyphicon-list-alt icons"></span>
            </a>
          </div>
          <div class="col-sm-3">
            <p><strong>Contact</strong></p>
            <a data-toggle="collapse" data-parent="#accordion" href="#contact">
            <span class="glyphicon glyphicon-envelope icons"></span>
            </a>
          </div>
          <div class="col-sm-12" id="accordion">
            <div class = "panel">
              <div id="info" class="collapse" style="text-align: left;">
                <h3>Decision Deck Consortium</h3>


Association loi 1901. Address: 445, Route de Quilihouarn, 29280 Plouzané, France

**Purpose**

The literature in the field of Multi-Criteria Decision Aiding (MCDA) proposes algorithms and methods that are acknowledged and used in real world decision aiding processes. These techniques provide useful solutions to complex decision problems which involve several, often conflicting evaluation criteria. Several such methods and algorithms have been implemented and are available as separate software solutions. However, as they were developed in an uncoordinated way, they lack fundamental interoperability features.

The Decision Deck project therefore aims at collaboratively developing Open Source software tools implementing Multi-Criteria Decision Aiding techniques which are meant to support complex decision aiding processes. One of the main features of these software solutions are that they are interoperable in order to create a coherent ecosystem.

The Decision Deck Consortium, a french non profit association, steers and manages the project along the lines of a manifesto. It is headed by an Administration Board.

The Decision Deck Consortium was officially born on November 25, 2008 as it appears the official declaration (#1390) published in the French Journal Officielle de la République Française p.6301 (see `Extract <//www.decision-deck.org/project/_static/extraitJournOff27Dec08Small.jpeg>`_ (280.36 Kb)).

**Bylaws (In French):** `Page 1 <//www.decision-deck.org/project/_static/statut_dd_1.jpeg>`_ (246.40 Kb) `Page 2 <//www.decision-deck.org/project/_static/statut_dd_2.jpeg>`_ (103.16 Kb)

.. raw:: html

              </div>
              <div id="committee" class="collapse">
                <table style="margin-left:auto;margin-right:auto;">
                  <tr>
                    <td><strong>President</strong></td>
                    <td>Patrick Meyer</td>
                  </tr>
                  <tr>
                    <td><strong>Vice-president</strong></td>
                    <td>Miłosz Kadziński</td>
                  </tr>
                  <tr>
                    <td><strong>Secretary</strong></td>
                    <td>Vincent Mousseau</td>
                  </tr>
                  <tr>
                    <td><strong>Treasurer</strong></td>
                    <td>Brice Mayag</td>
                  </tr>
                  <tr>
                    <td><strong>Software coordinator</strong></td>
                    <td>Sébastien Bigaret</td>
                  </tr>
                  <tr>
                    <td><strong>Communications coordinator</strong></td>
                    <td>Alexandru Olteanu</td>
                  </tr>
                  <tr>
                    <td><strong>Deputy software coordinator</strong></td>
                    <td>Olivier Cailloux</td>
                  </tr>
                </table>

.. raw:: html

              </div>
              <div id="manifesto" class="collapse" style="text-align: left;">


The Decision Deck project collaboratively develops Open Source software tools to support the Multi-Criteria Decision Aiding (MCDA) process. Its purpose is to provide effective tools for three types of users:

* consultants who use MCDA tools to support decision makers involved in real world decision problems;
* teachers who present MCDA methods in courses, for didactic purposes;
* researchers who want to test and compare methods, or to develop new ones.

From a practical point of view, the Decision Deck project works on developing multiple software resources that are able to interact. Several complementary **initiatives** focusing on different perspectives contribute to:

* global architecture of MCDA systems;
* implementations of MCDA algorithms;
* data models of MCDA objects;
* decision process modelling and management;
* graphical user interface.

The original manifesto as approved by the consortium at its creation data is available `here <_static/D2manifesto.pdf>`_.

.. raw:: html

              </div>
              <div id="contact" class="collapse">
                  <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#home">Decision Deck</a></li>
                    <li><a data-toggle="tab" href="#menu1">diviz</a></li>
                    <li><a data-toggle="tab" href="#menu2">Communication</a></li>
                    <li><a data-toggle="tab" href="#menu3">Mailing list</a></li>
                  </ul>
                  <div class="tab-content">
                    <div id="home" class="tab-pane fade in active">


**Patrick Meyer**

| IMT Atlantique
| LUSSI Department
| Technopôle Brest-Iroise CS 83818
| F-29238 Brest Cedex 3
| France

patrick.meyer@imt-atlantique.fr

.. raw:: html

                    </div>
                    <div id="menu1" class="tab-pane fade">


**Sébastien Bigaret** and **Patrick Meyer**

| IMT Atlantique
| LUSSI Department
| Technopôle Brest-Iroise CS 83818
| F-29238 Brest Cedex 3
| France

info@diviz.org

.. raw:: html

                    </div>
                    <div id="menu2" class="tab-pane fade">


**Alexandru Olteanu**

| Université Bretagne Sud
| ENSIBS, Lab-STICC
| Bur 155, Centre de Recherche
| Rue Saint-Maudé, 56100
| Lorient, France

alexandru.olteanu@univ-ubs.fr

.. raw:: html

                    </div>
                    <div id="menu3" class="tab-pane fade">


This **low traffic** mailing list is intended to announce general news on the Decision Deck project and inform you of upcoming events.

If you wish to subscribe, note that you will receive at most 1 or 2 announcements per month from this list. You can subscribe to the mailing list by pointing your web browser to the following address:

https://listes.imt-atlantique.fr/wws/subscribe/decisiondeck-announcements

and by entering your email address. You will then receive an email with a link to confirm your subscription.

.. raw:: html

                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="initiatives" class="container text-center">
      <h3>Initiatives</h3>
      <br>
      <div class="row">
        <div class="col-md-3 col-sm-4 col-xs-6">
          <a href = "//www.diviz.org">
          <img class="img-responsive" src="_static/divizlogo2.png" style="max-height:40px;display:block;margin: 0px auto">
          <h4>diviz</h4>
          </a>
          <p>A software for designing, executing and sharing MCDA methods, algorithms and experiments</p>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6">
          <a href = "//www.decision-deck.org/xmcda">
          <img class="img-responsive" src="_static/xmcdalogo.png" style="max-height:40px;display:block;margin: 0px auto">
          <h4>XMCDA</h4>
          </a>
          <p>A standardized XML recommendation to represent objects and data from the MCDA field which allows MCDA software to be interoperable</p>
        </div>
        <div class="clearfix visible-xs"><br></div>
        <div class="col-md-3 col-sm-4 col-xs-6">
          <a href = "//www.decision-deck.org/ws">
          <img class="img-responsive" src="_static/xmcdawslogo.png" style="max-height:40px;display:block;margin: 0px auto">
          <h4>XMCDA web services</h4>
          </a>
          <p>Distributed computational MCDA resources, using the XMCDA standard</p>
        </div>
        <div class="clearfix visible-sm"><br></div>
        <div class="col-md-3 col-sm-4 col-xs-6">
          <a href = "//www.decision-deck.org/gis">
          <img class="img-responsive" src="_static/gislogo2.png" style="max-height:40px;display:block;margin: 0px auto">
          <h4>GIS</h4>
          </a>
          <p>An initiative linking Geographical Information Systems and MCDA</p>
        </div>
        <div class="clearfix visible-xs visible-md visible-lg"><br></div>
        <div class="col-md-3 col-sm-4 col-xs-6">
          <a href = "//www.decision-deck.org/r">
          <img class="img-responsive" src="_static/rlogo2.png" style="max-height:40px;display:block;margin: 0px auto">
          <h4>R</h4>
          </a>
          <p>An initiative linking the statistical software and programming environment R and MCDA</p>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-6">
          <a href = "//www.decision-deck.org/d2">
          <img class="img-responsive" src="_static/frozenlogo.png" style="max-height:40px;display:block;margin: 0px auto">
          <h4>d2</h4>
          </a>
          <p>A software containing several MCDA methods</p>
          <p>(FROZEN)</p>
        </div>
        <div class="clearfix visible-sm visible-xs"></div>
        <div class="col-md-3 col-sm-4 col-xs-6">
          <a href = "//www.decision-deck.org/d3">
          <img class="img-responsive" src="_static/frozenlogo.png" style="max-height:40px;display:block;margin: 0px auto">
          <h4>d3</h4>
          </a>
          <p>A rich internet application for calling XMCDA web services</p>
          <p>(FROZEN)</p>
        </div>
      </div>
    </div>
    <hr>
    <div id="sponsors" class="container text-center">
      <h3>Our Sponsors</h3>
      <br>
      <div class="row">
        <div class="col-sm-6 col-md-4">
          <a href = "https://www.labsticc.fr">
          <img class="img-responsive sponsor" src="_static/labsticc.png">
          </a>
        </div>
        <div class="col-sm-6 col-md-4">
          <a href = "http://www.cs.put.poznan.pl/ewgmcda/">
          <img class="img-responsive sponsor" src="_static/EWG_MCDA.png">
          </a>
        </div>
        <div class="col-sm-offset-3 col-md-offset-0 col-sm-6 col-md-4">
          <a href = "https://www.imt-atlantique.fr">
          <img class="img-responsive sponsor" src="_static/imta.png">
          </a>
        </div>
      </div>
    </div>
    <hr>
    <div id="icons" class="text-center">
        <p>Follow diviz on <a href="https://twitter.com/divizMCDA"><i class="fa fa-twitter icons"></i></a></p>
    </div>
