.. raw:: html

    <div id="title" class="text-center">
      <img class="img-responsive" src="_static/gislogo2.png" style="max-height:70px;display:block;margin: 20px auto;">
      <h2>The GIS initiative</h2>
      <p>The GIS initiative of Decision Deck focuses on tools that help with the integration of Geographical Information Systems and Multi-Criteria Decision Aiding</p>
    </div>


This initiative is currently in the process of expading. Stay tuned for more details.

MODEL (Multi-criteria OrDinal EvaLuation)
-----------------------------------------

.. Important:: This plug-in is no longer maintained. It will be shortly replaced by a newer version.

.. figure:: pics/model.png
    :width: 100%
    :alt: The MODEL plug-in.

This is a QuantumGIS plug-in which provides an easy to use interface to model and solve decision problems which aim at oradinally evaluating geographical areas by taking into account multiple criteria and multiple actors. The official plug-in page may be found `here <https://formations.telecom-bretagne.eu/or/software/model/index.html>`_.


