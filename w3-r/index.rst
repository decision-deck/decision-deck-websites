.. raw:: html

    <div id="title" class="text-center">
      <img class="img-responsive" src="_static/rlogo2.png" style="max-height:70px;display:block;margin: 20px auto;">
      <h2>The R initiative</h2>
      <p>The R initiative of Decision Deck proposes to use the R statistical environment, coupled with the MCDA package, to support all the steps of the MCDA process</p>
    </div>


The MCDA package for R contains functions which can be useful to support the analyst in this complex process, and which should facilitate his dialoge with the decision maker(s).

How to install ?
----------------

After having installed the R environment, install the R package by typing 

::

	install.packages("MCDA")

in the command prompt of R. This will download the latest stable version from the CRAN repositories. 

How to use ?
------------

We have written a :ref:`tutorial<tutorial_R_MCDA>` which details the use of the MCDA R package in the various steps of a decision aiding process. 

How to contribute ? 
-------------------

To contribute to the package, please clone the source of the latest version of the package available here : https://github.com/paterijk/MCDA. We would really appreciate if contributors would contact the authors of the package before implementing any new functions. 

