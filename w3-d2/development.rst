d2 Development Environment Installation Guide
=============================================

Certified versions
------------------

d2 is known to work against these versions:

    * Eclipse 3.3.1.1
    * Java 5+
    * Mysql 5

Install the prerequisites
-------------------------

    * Download Eclipse

      http://www.eclipse.org/downloads/

Install Eclipse plugins
-----------------------

    * Open the update manager dialog [Help > Software update > Find and Install]
    * Select "Find new features to install"
    * Select "Europa Discovery Site" and press finish
    * Select "AspectJ Development Tools" under "Programming Languages"
    * Press "Select required"
    * Press Next
    * Accept the license and press Next, then Next again, then Finish
    * Press "Install all"
    * Press OK when you're asked to restart Eclipse

New AspectJ site
................

    * For more recent eclipse installations, AspectJ web site has changed. You have to add the following update site: http://download.eclipse.org/tools/ajdt/35/update. For more recent informations see http://www.eclipse.org/ajdt/downloads/.

Install d2 plugin
-----------------

    * Open the update manager dialog
    * Select "Find new features to install"
    * Deselect all sites
    * Create a "New Remote site"

      > Name: Decision Deck (d2) Updates

      > URL : http://decision-deck.sourceforge.net/tools/
    * Press Finish
    * Select "Decision Deck (d2) Updates"
    * Click Next
    * Accept the license, click Next then Finish
    * Press "Install all"
    * Restart Eclipse

Fetch Projects from CVS
-----------------------

    * Download the `Project Set File <http://sourceforge.net/projects/decision-deck/files/d2%20tools/d2.psf/download>`_
    * Import the project list into Eclipse

      File > Import > Team > Team Project Set

      If you don't check "Run import in background", you won't be able to use the platform until the import ends
    * When prompted, enter "anonymous" as user name and left the password blank
    * After all projects have been imported, configure the Package Explorer view to use Working sets as Top Level Elements in Package Explorer

First execution
---------------

    * Open Run dialog
    * Select OSGi Platform
    * Click New
    * In the plugins tab
          * Deselect Platform
          * Deselect Workspace/org.decisiondeck.eclipse.tools
          * Press Add Required
          * Press Run
          * Decision Deck should run without any further issue

Build Decision Deck
-------------------

    * Run Ant script org.decisiondeck.dist/build.xml with the parameters: ::

      	eclipse.home
      	target.version
      	target.platform [linux-gtk|linux-motif|win32]
      	export.source [true|false]

    * d2 archive should be copied to org.decisiondeck.dist/dist/decisiondeck-$target.version-$target.platform.zip
    * If export.source is true, a qualifier is added to the archive name, f.i: decisiondeck-1.1-win32-src.zip

Build the documentation
-----------------------

    * You need to have Maven2 installed
    * Open a shell console
    * cd to org.decisiondeck.doc
    * run ::

      	mvn site:site  

    * Maven will download all the required libraries and transform the apt documents into valid html ones. You will find them in org.decisiondeck.doc/target/site


