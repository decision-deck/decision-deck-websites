Installation
============

Server side
-----------

Prerequisites
^^^^^^^^^^^^^

d2 requires MySql. Minimum version is 4.1.x

Install
^^^^^^^

To install d2 on the server side, you don't need to do anything specific. You just need to ensure you have a running MySql server and an accessible database (even empty).

Client Side
-----------

Prerequisites
^^^^^^^^^^^^^

To run d2 you will need a Java 5 JRE installed on your machine. If don't already have one you can download it on `Java <http://java.sun.com/j2se/1.5.0/>`_ site.

Install and Run
^^^^^^^^^^^^^^^

No special action is required to install d2 on the client side. Just unzip the distribution in the folder of your choice. To launch d2, run the decision-deck executable (decision-deck.exe on Windows, decision-deck on Linux).
