Core overview
=============

Decision Deck core entities
---------------------------

Below is an overview of the entities composing the core of Decision Deck. Please refer to :doc:`plugin guide <plugin-guide>` for more insight about the platform itself.

.. figure:: images/d2_core.jpg
   :scale: 60
   :align: center
