decision desktop
================

.. Important:: This initiative is no longer maintained.

The Decision Desktop software, or d2 for short, was the first software to be developed in the Decision Deck project.

It is a desktop, client/server application, meaning that it is designed to be installed locally (it is not a web application), and uses a database to store application data, thereby enabling a multiple user usage.

One of its usage patterns is that several experts may enter evaluations in a decentralized manner, then these evaluations be analyzed by a coordinator, this analysis being then reviewed by one or several decision-maker.

Historically the software has also been named EVAL, from the name of the originating project, and Decision Deck. It is now recommended to name it decision desktop to differentiate it from the other softwares produced in the Decision Deck project.

Download and technical information
----------------------------------

All downloads, as well as some technical information (bug tracking, developer and user help forums) can be found on the SourceForge `website <https://sourceforge.net/project/showfiles.php?group_id=172756>`_.

Usage informations
------------------

.. toctree::
	:maxdepth: 1

	install
	user-guide
	plugins
	
The decision desktop software is published under the `GPL, v3 <http://www.gnu.org/licenses/gpl-3.0.html>`_ licence.

For user help, please post a message to the SourceForge `help forum <http://sourceforge.net/projects/decision-deck/forums/forum/592750>`_.

Developer informations
----------------------

.. toctree::
	:maxdepth: 1

	development
	core
	plugin-guide
	migration-guide

For developer questions, please see the SourceForge `developer forum <http://sourceforge.net/projects/decision-deck/forums/forum/592751>`_.

Contributors
------------
The following people have contributed to this project through the way of code, suggestions, or documentation.

.. raw:: html

	<BR>
	<CENTER>
	<TABLE BORDER=1>
		<TR><TD>Raymond Bisdorff </TD><TD>University of Luxembourg</TD></TR>
		<TR><TD>Olivier Cailloux</TD><TD>Université Libre de Bruxelles</TD></TR>
		<TR><TD>Joao Costa</TD><TD>INESC</TD></TR>
		<TR><TD>Gilles Dodinet</TD><TD>Karmicsoft</TD></TR>
		<TR><TD>Antonio Fiordaliso</TD><TD>Faculte Polytechnique de Mons</TD></TR>
		<TR><TD>Vincent Mousseau</TD><TD>LAMSADE</TD></TR>
		<TR><TD>Piotr Zielniewicz</TD><TD>Poznan University of Technology</TD></TR>
	</TABLE>
	</CENTER>

