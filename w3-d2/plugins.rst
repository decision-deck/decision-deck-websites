Decision Deck plugins list
==========================

Below is a list of Decision Deck plugins (MCDA methods and utilities) bundled with the platform.

.. See also http://sma.uni.lu/d2cms/tiki-index.php?page=Implemented+Methods
.. IRIS: Outranking based sorting of alternatives into ordered classes
.. RUBIS: Outranking based methodology for the decision choice problem.
.. VIP: Choosing on the basis of an additive aggregation model , accepting imprecise information on the scaling coefficients
.. UTA-GMS/GRIP: Ranking alternatives with a set of value functions
.. MAVT-Kappalab: MAVT Ranking with Choquet-Integral based aggregation

Being listed
------------

You wrote a Decision Deck plugin and want to be listed here? Drop us a note with the following information:

    * Name
    * Description
    * License type
    * Site URL
    * Download URL
    * Dependencies (which plugins your plugin depends on, which version of the platform, etc.)
    * Documentation URL
    * Source repository (if relevant)

Iris
----
IRIS is an interactive method that aim at sorting alternatives into ordered classes. The assignment of alternatives to classes corresponds to Electre Tri. The specificity of the IRIS method is that the DM does not have to specify the weights of criteria, but intead provides assignment examples, i.e., typical elements of the classes.

IRIS documents
    * IRIS `input file in a flat text format <http://www.lamsade.dauphine.fr/d2-workshop/documents/data-tri.tri>`_.
    * IRIS `input file in a XML format <http://www.lamsade.dauphine.fr/d2-workshop/documents/IRISinput.xml>`_.
    * IRIS `file in a XML format <http://www.lamsade.dauphine.fr/d2-workshop/documents/IRISsmall.xml>`_ (containing both input and output information). 

Relevant references
    * L.C. Dias, V. Mousseau, J. Figueira, and J.N. Clìmaco. An aggregation/disaggregation approach to obtain robust conclusions with ELECTRE TRI. European Journal of Operational Research, 138(2):332-348, April 2002. `PDF <http://www.lamsade.dauphine.fr/mcda/biblio/PDF/dias4ejor02.pdf>`_.
    * L.C. Dias and V. Mousseau. IRIS: A DSS for Multiple Criteria Sorting Problems. Journal of Multi-Criteria Decision Analysis, 12:285-298, 2003. `PDF <http://www.lamsade.dauphine.fr/mcda/biblio/PDF/DiasMousseauJMCDA2003.pdf>`_.
    * S. Damart, L. Dias, and V. Mousseau. Supporting groups in sorting decisions: Methodology and use of a multi-criteria aggregation/disaggregation DSS, Decision Support Systems, 43(4):1464-1475, August 2007. `PDF <http://www.lamsade.dauphine.fr/mcda/biblio/PDF/DamartDiasMousseauDSS.pdf>`_.

Informations about the original implementation of IRIS for Windows can be found `here <http://www4.fe.uc.pt/lmcdias/iris.htm>`_.

Rubis
-----
Rubis is a decision aid method for tackling the choice problem in the context of multiple criteria decision analysis (1).

Its genuine purpose is to help a decision maker to determine a single best decision alternative. Methodologically we focus on pairwise comparisons of these alternatives which lead to the concept of bipolar-valued outranking digraph (2). The work is centred around a set of five pragmatic principles which are required in the context of a progressive decision aiding methodology (3). `Learn more <http://charles-sanders-peirce.uni.lu/bisdorff/documents/RubisHandouts.pdf>`_.

Here is an example of an outranking digraph with best (yellow) and worst (blue) choices (`Learn more <http://charles-sanders-peirce.uni.lu/bisdorff/EJORPaper103352.pdf>`_):

.. figure:: images/rubis_outrank.png
   :align: center

.. TODO broken link to EJORPaper103352.pdf (nor there nor in documents), and Meyer thesis.

Rubis documents
    * Rubis choice method presentation (`PDF <http://charles-sanders-peirce.uni.lu/bisdorff/documents/RubisHandouts.pdf>`_).
    * Rubis Web Service Design and Implementation (`More informations <http://ernst-schroeder.uni.lu/RubisServerDoc/>`_).
    * d2 Rubis plugin performance tableau input specification `file <http://sma.uni.lu/rubisServer/Schemas/rubisPerformanceTableau-1.0/rubisPerformanceTableau.xsd.html>`_.
    * Example of Rubis performance tableau XML specification `file <http://sma.uni.lu/rubisServer/Schemas/rubisPerformanceTableau-1.0/rubisPerformanceTableau.xml>`_.
    * d2 Rubis plugin outranking digraph output specification `file <http://sma.uni.lu/rubisServer/Schemas/rubisOutrankingDigraph-1.0/rubisOutrankingDigraph.xsd.html>`_.
    * Example of Rubis outranking digraph (with choice recommendation) XML specification `file <http://sma.uni.lu/rubisServer/Schemas/rubisOutrankingDigraph-1.0/rubisOutrankingDigraph.xml>`_.


Bibliography
	1. R. Bisdorff, P. Meyer and M. Roubens (2007), RUBIS: a bipolar-valued outranking method for the choice problem. 4OR, A Quarterly Journal of Operations Research, Springer - Verlag. (Online)] Electronic version: DOI: 10.1007/s10288-007-0045-5, pp 1 - 27. (`Preprint version <http://charles-sanders-peirce.uni.lu/bisdorff/documents/HyperKernels.pdf>`_).
	2. R. Bisdorff, M. Pirlot and M. Roubens (2006). Choices and kernels from bipolar valued digraphs. European Journal of Operational Research, 175 (2006) 155-170. (Online) Electronic version: DOI:10.1016/j.ejor.2005.05.004. (`Preprint version <http://charles-sanders-peirce.uni.lu/bisdorff/documents/BisdorffPirlotRoubens05.pdf>`_).
	3. P. Meyer (2007), Progressive Methods in Multiple Criteria Decision Analysis, PhD thesis, University of Luxembourg (`PDF <http://sma.uni.lu/meyer/articles/pdf/these.pdf>`_).

VIP
---

The VIP (Variable Interdependent Parameters) Analysis software has been built to support the selection of the most preferred alternative among a list, considering the impacts of each alternative on multiple evaluation criteria. It is based on an additive aggregation model (value function), accepting imprecise information on the value of the scaling coefficients (a.k.a. scaling constants, which indirectly reflect the relative importance of the each criterion).

Rather than precise values, the scaling coefficients are considered Variable Interdependent Parameters subject to a set of constraints (e.g. bounds, order relations, or any linear constraints). This amounts to consider multiple acceptable combinations of values for these parameters, which is particularly relevant in spite of the simplicity of the model. Indeed, fixing precise values for the scaling constants is often difficult because these values reflect the judgments of the decision makers, which may evolve through time and may be hard to elicit in a precise way. The number of (arbitrary) options in the process of building the criteria plus the possibility of divergence among several decision makers may further hinder the requirement of precise numerical figures.

The VIP Analysis software offers its users an easy to use tool to analyze a choice problem using multiple approaches at several levels of detail, when imprecise information is accepted. It computes the best and worst overall value that each alternative may attain (given the multiple acceptable inputs), the pairwise confrontation table (differences of global value between pairs of alternatives) - which allows to discover dominated or quasi-dominated alternatives -, as well as the maximum loss of opportunity associated with choosing each alternative, plus the domain where each alternative is optimal or quasi-optimal (if the problem dimension allows it).

See also the VIP Analysis `web page <http://www4.fe.uc.pt/lmcdias/english/vipa.htm>`_.

UTADISGMS and GRIP
------------------
The UTAGMS and GRIP methods aim at solving the multiple criteria ranking problem. The underlying preference model is a set of monotone additive value function compatible with preference statements expressed by the decision maker. The main result of these methods is :
    * a necessary (robust) ranking that contains comparisons of alternatives that remain valid for all compatible value function,
    * a possible ranking that contains comparisons of alternatives that are valid for at least one compatible value function. 

For a detailled descriptions of the methods, see:
    * `UTAGMS <http://www.lamsade.dauphine.fr/mcda/biblio/PDF/UTAGMS-Ejor.pdf>`_,
    * `GRIP <http://www.lamsade.dauphine.fr/sites/default/IMG/pdf/cahierLamsade253.pdf>`_. 

Weighted Sum
------------
A simple method to demonstrate some of the platform features.

GLPK
----
A wrapper around the GLPK library.

Concordance/Discordance computation
-----------------------------------
Generic concordance and discordance computation.
