#! /bin/bash
# rsync per directory instead of trying to mirror the root
# This mitigates the risk to touch either downloads.diviz.org or downloads/

for dir in d2 d3 gis news project r ws xmcda; do
    echo "Syncing ${dir}..." >&2
    # use ${dir:-DIR NOT SET} as an extra precaution
    rsync -e='ssh -x' -axXHS --info=PROGRESS2 "www-decision-deck/${dir:-DIR NOT SET}/" "decision@ssh.cluster014.ovh.net:~/www/${dir:-DIR NOT SET}/"
done

# diviz.org
dir=diviz
echo "Syncing ${dir}..." >&2
# use ${dir:-DIR NOT SET} as an extra precaution
rsync -e='ssh -x' -axXHS --info=PROGRESS2 "www-decision-deck/${dir:-DIR NOT SET}/" "diviz@ssh.cluster014.ovh.net:~/web/www/"

scp -p w3-diviz/.htaccess.decision-deck.org \
       decision@ssh.cluster014.ovh.net:~/www/diviz/.htaccess
scp -p w3-diviz/.htaccess.diviz.org \
       diviz@ssh.cluster014.ovh.net:~/web/www/.htaccess

#  --allow-chown   --allow-suid
